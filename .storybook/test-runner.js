/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 */

import { getStoryContext, waitForPageReady } from '@storybook/test-runner';
import assert from 'node:assert';
import { execSync } from 'node:child_process';

import { toMatchImageSnapshot } from 'jest-image-snapshot';

assert(
  process.env.SNAPSHOT_OUTPUT,
  'Missing SNAPSHOT_OUTPUT environmental variable',
);

// Throw an error when the snapshot server is not running.
// We do this because the snapshots will still work without the docker snapshot server (they will run against your locally installed chromium).
assert(
  process.env.NX_TASK_TARGET_CONFIGURATION === 'ci' ||
    isSnapshotServerRunning(),
  'Snapshot server should run before creating snapshots. Make sure to execute "npm run start-snapshot-server" before running snapshots',
);

const customSnapshotsDir = `${process.cwd()}/${process.env.SNAPSHOT_OUTPUT}`;

const viewport = { width: 1920, height: 1080 };

/** @type {import('@storybook/test-runner').TestRunnerConfig} */
const config = {
  tags: {
    include: ['snapshot'],
  },
  setup() {
    expect.extend({ toMatchImageSnapshot });
  },
  async preVisit(page) {
    await page.setViewportSize(viewport);

    await page.evaluate(() => {
      document.location.hash = '#snapshot';
    });
  },
  async postVisit(page, context) {
    const storyContext = await getStoryContext(page, context);

    page.addStyleTag({
      content: `
            *,
            *::before,
            *::after {
                transition: none !important;
                animation: none !important;
            }
        `,
    });

    page.mouse.move(viewport.width, viewport.height);

    await new Promise((resolve) => setTimeout(resolve, 1000));

    // Waits for the page to be ready before taking a screenshot to ensure consistent results
    await waitForPageReady(page);

    let image;
    if (storyContext.tags?.includes('fullPageSnapshot')) {
      image = await page.screenshot();
    } else {
      const component = await page.$('#storybook-root > *, [role=dialog]');
      if (!component) {
        throw new Error('No root component found');
      }

      const scrollHeight = await page.evaluate(() => {
        return document.documentElement.scrollHeight;
      });

      // component.screenshot() takes screenshots outside of viewport but does not modify the viewport.
      // this causes some stories to look broken so that's why we make sure to align the viewport with the height of the actual page.
      if (scrollHeight > viewport.height) {
        await page.setViewportSize({
          width: viewport.width,
          height: Math.min(scrollHeight, 15000), // the 15000 cap is to prevent ENOBUFS errors
        });
      }

      image = await component.screenshot();
    }

    expect(image).toMatchImageSnapshot({
      customSnapshotsDir,
      customSnapshotIdentifier: context.id,
    });
  },
};
export default config;

function isSnapshotServerRunning() {
  const cmd = `docker ps --filter "name=snapshot-server" --filter="status=running" -q`;
  try {
    return execSync(cmd).toString('utf8') !== '';
  } catch (e) {
    return false;
  }
}
