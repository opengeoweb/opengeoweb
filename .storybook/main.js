const path = require('path');
const toPath = (filePath) => path.join(process.cwd(), filePath);

module.exports = {
  stories: [],
  addons: [
    {
      name: '@storybook/addon-essentials',
      options: {
        actions: false,
        backgrounds: false,
        controls: false,
        docs: false,
      },
    },
    'storybook-zeplin/register',
    '@storybook/preset-scss', // currently only used for frappe-gantt
    '@storybook/addon-webpack5-compiler-babel',
    { name: '@storybook/addon-docs', options: { transcludeMarkdown: true } },
  ],
  features: {
    postcss: false,
  },
  core: {
    disableTelemetry: true,
  },
  webpackFinal: async (config, { configType }) => {
    config.resolve.alias['@opengeoweb/api'] = path.resolve(
      __dirname,
      '../libs/api/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/authentication'] = path.resolve(
      __dirname,
      '../libs/authentication/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/shared'] = path.resolve(
      __dirname,
      '../libs/shared/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/form-fields'] = path.resolve(
      __dirname,
      '../libs/form-fields/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/theme'] = path.resolve(
      __dirname,
      '../libs/theme/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/core'] = path.resolve(
      __dirname,
      '../libs/core/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/store'] = path.resolve(
      __dirname,
      '../libs/store/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/webmap'] = path.resolve(
      __dirname,
      '../libs/webmap/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/webmap-react'] = path.resolve(
      __dirname,
      '../libs/webmap-react/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/timeseries'] = path.resolve(
      __dirname,
      '../libs/timeseries/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/taf'] = path.resolve(
      __dirname,
      '../libs/taf/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/sigmet-airmet'] = path.resolve(
      __dirname,
      '../libs/sigmet-airmet/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/theme'] = path.resolve(
      __dirname,
      '../libs/theme/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/cap'] = path.resolve(
      __dirname,
      '../libs/cap/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/warnings'] = path.resolve(
      __dirname,
      '../libs/warnings/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/timeslider'] = path.resolve(
      __dirname,
      '../libs/timeslider/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/timesliderlite'] = path.resolve(
      __dirname,
      '../libs/timesliderlite/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/snackbar'] = path.resolve(
      __dirname,
      '../libs/snackbar/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/layer-select'] = path.resolve(
      __dirname,
      '../libs/layer-select/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/webmap-redux'] = path.resolve(
      __dirname,
      '../libs/webmap-redux/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/metronome'] = path.resolve(
      __dirname,
      '../libs/metronome/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/spaceweather'] = path.resolve(
      __dirname,
      '../libs/spaceweather/src/index.ts',
    );
    config.optimization = {
      minimize: false,
      minimizer: [],
    };
    return {
      ...config,
      resolve: {
        ...config.resolve,
        alias: {
          ...config.resolve.alias,
          '@emotion/core': toPath('node_modules/@emotion/react'),
          'emotion-theming': toPath('node_modules/@emotion/react'),
        },
      },
    };
  },
  env: (config) => {
    /*
      To to prevent error "Failed to execute 'postMessage' on 'DOMWindow'".

      We have to set the STORYBOOK_ZEPLIN_PARENT_ORIGIN parameter.

      This is done by reading the port number for this storybook project,
      and use it to set the correct parent origin for the Zeplin plugin.
    */
    try {
      const nxTarget = process.env['NX_TASK_TARGET_PROJECT'];
      const projectFile = toPath('./libs/' + nxTarget + '/project.json');
      const projectJson = require(projectFile);
      const portNumber = projectJson.targets.storybook.options.port;
      const parentOrigin = 'http://localhost:' + portNumber;
      return {
        ...config,
        STORYBOOK_ZEPLIN_PARENT_ORIGIN: parentOrigin,
      };
    } catch (e) {
      console.error(e);
    }
    return config;
  },
  framework: {
    name: '@storybook/react-webpack5',
    options: {
      fastRefresh: true,
      strictMode: true,
    },
  },
  staticDirs: [toPath('.storybook/public')],
};
