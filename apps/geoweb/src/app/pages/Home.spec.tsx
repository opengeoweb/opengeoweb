/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { uiTypes } from '@opengeoweb/store';
import { workspaceRoutes } from '@opengeoweb/workspace';
import { Route } from 'react-router-dom';
import { ConfigType } from '@opengeoweb/shared';
import { TestWrapperWithRouter } from '../components/Providers';
import { AppStore } from '../store';
import { createFakeApi } from '../utils/api';
import Home from './Home';
import { createMockStore } from '../utils/testUtils';
import { translateKeyOutsideComponents } from '../../i18n';

describe('Home', () => {
  const testConfig: ConfigType = {
    GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
    GW_SW_BASE_URL: 'spaceweather',
    GW_SIGMET_BASE_URL: 'sigmet',
    GW_AIRMET_BASE_URL: 'airmet',
    GW_TAF_BASE_URL: 'taf',
    GW_PRESET_BACKEND_URL: 'presets',
  };

  it('should not render homepage when no config is loading', async () => {
    const props = {
      config: null!,
      auth: null,
      onSetAuth: jest.fn(),
    };
    const store = createMockStore();
    render(
      <TestWrapperWithRouter store={store}>
        <Route
          path={workspaceRoutes.root}
          element={<Home {...props} createApi={createFakeApi} />}
        />
      </TestWrapperWithRouter>,
    );
    await waitFor(() => expect(screen.queryByTestId('appLayout')).toBeFalsy());
  });

  it('should render successfully with default empty preset', async () => {
    const store = createMockStore();

    render(
      <TestWrapperWithRouter store={store}>
        <Route
          path={workspaceRoutes.root}
          element={<Home config={testConfig} createApi={createFakeApi} />}
        />
      </TestWrapperWithRouter>,
    );
    await waitFor(() => {
      expect(screen.getByTestId('WorkspacePage')).toBeTruthy();
    });

    await waitFor(() => {
      expect(screen.getByTestId('workspaceTab').textContent).toEqual(
        translateKeyOutsideComponents('workspace-new'),
      );
    });

    await screen.findAllByText(
      translateKeyOutsideComponents('workspace-mappreset-new'),
    );
  });

  it('should open Sync Groups', async () => {
    const store = createMockStore();
    render(
      <TestWrapperWithRouter store={store}>
        <Route
          path={workspaceRoutes.root}
          element={<Home config={testConfig} createApi={createFakeApi} />}
        />
      </TestWrapperWithRouter>,
    );

    await screen.findByTestId('menuButton');
    fireEvent.click(screen.getByTestId('menuButton'));
    fireEvent.click(screen.getByText('Sync Groups'));

    await screen.findByTestId('syncGroupViewer');
    await screen.findByText('Sync'); // dialog title
  });

  it('should show the viewpreset button', async () => {
    const mockState: AppStore = {
      workspace: {
        id: 'screenConfigA',
        title: 'Radar',
        views: {
          byId: {
            screenA: {
              title: 'Precipitation Radar',
              componentType: 'Map',
              initialProps: {
                mapPreset: {
                  layers: [
                    {
                      service:
                        'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserver?dataset=RADAR',
                      name: 'precipitation',
                      format: 'image/png',
                      enabled: true,
                      style: 'radar/nearest',
                      id: 'test_layerid_active',
                      layerType: 'mapLayer',
                    },
                  ],
                  activeLayerId: 'test_layerid_active',
                },
                syncGroupsIds: ['Area_A', 'Time_A'],
              },
            },
          },
          allIds: ['screenA'],
        },
        mosaicNode: 'screenA',
      },
      ui: {
        order: [uiTypes.DialogTypes.LayerManager],
        dialogs: {
          layerManager: {
            activeMapId: 'screenA',
            isOpen: true,
            type: uiTypes.DialogTypes.LayerManager,
            source: 'app',
          },
        },
      },
    };
    const store = createMockStore(mockState);

    render(
      <TestWrapperWithRouter store={store}>
        <Route
          path={workspaceRoutes.root}
          element={<Home config={testConfig} createApi={createFakeApi} />}
        />
      </TestWrapperWithRouter>,
    );
    expect(await screen.findByTestId('layerManagerButton')).toBeTruthy();
    expect(await screen.findAllByTestId('layerManagerWindow')).toHaveLength(1);
    expect(
      await screen.findAllByTestId('viewpreset-options-toolbutton'),
    ).toBeTruthy();
  });
});
