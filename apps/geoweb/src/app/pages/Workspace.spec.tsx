/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, screen } from '@testing-library/react';
import { Route } from 'react-router-dom';

import {
  AuthenticationProvider,
  getSessionStorageProvider,
  Login,
} from '@opengeoweb/authentication';
import { ConfigType } from '@opengeoweb/shared';
import { workspaceRoutes } from '@opengeoweb/workspace';
import Workspace from './Workspace';
import { TestWrapperWithRouter } from '../components/Providers';
import { createFakeApi } from '../utils/api';
import { createMockStore } from '../utils/testUtils';

describe('Workspace', () => {
  const testConfig: ConfigType = {
    GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
    GW_FEATURE_MODULE_SPACE_WEATHER: true,
    GW_SW_BASE_URL: 'spaceweather',
    GW_SIGMET_BASE_URL: 'sigmet',
    GW_AIRMET_BASE_URL: 'airmet',
    GW_TAF_BASE_URL: 'taf',
    GW_PRESET_BACKEND_URL: 'presets',
  };

  const storedWindowLocation = window.location;

  beforeEach(() => {
    // mock window.location
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    delete window.location;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = {
      assign: jest.fn(),
      origin: 'http://localhost',
      href: 'http://localhost/some-path',
    };
  });

  afterEach(() => {
    window.location = storedWindowLocation;
  });

  it('should render correctly', async () => {
    const store = createMockStore();
    const props = {
      onSetAuth: jest.fn(),
      isLoggedIn: true,
      onLogin: jest.fn(),
      auth: null,
      sessionStorageProvider: getSessionStorageProvider(),
    };

    render(
      <AuthenticationProvider value={props}>
        <TestWrapperWithRouter store={store}>
          <Route
            path={workspaceRoutes.root}
            element={
              <Workspace config={testConfig} createApi={createFakeApi} />
            }
          />
        </TestWrapperWithRouter>
      </AuthenticationProvider>,
    );
    expect(await screen.findByTestId('appLayout')).toBeTruthy();
  });

  it('should redirect to login if authenticated and not logged in', async () => {
    const store = createMockStore();
    const sessionStorageProvider = getSessionStorageProvider();
    const props = {
      onSetAuth: jest.fn(),
      isLoggedIn: false,
      onLogin: jest.fn(),
      auth: null,
      sessionStorageProvider,
    };

    // mock session storage
    jest.spyOn(Storage.prototype, 'getItem').mockReturnValue('true');
    render(
      <AuthenticationProvider value={props}>
        <TestWrapperWithRouter store={store}>
          <Route
            path={workspaceRoutes.root}
            element={<Workspace config={testConfig} />}
          />
          <Route path="/login" element={<Login />} />
        </TestWrapperWithRouter>
      </AuthenticationProvider>,
    );

    expect(window.location.assign).toHaveBeenCalledWith('/login');
    expect(screen.queryByTestId('workspace')).toBeFalsy();
  });

  it('should redirect to login if not authenticated and has GW_FEATURE_FORCE_AUTHENTICATION set', async () => {
    const store = createMockStore();
    const sessionStorageProvider = getSessionStorageProvider();
    const testConfigWithForceAuth: ConfigType = {
      GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
      GW_SW_BASE_URL: 'spaceweather',
      GW_SIGMET_BASE_URL: 'sigmet',
      GW_AIRMET_BASE_URL: 'airmet',
      GW_TAF_BASE_URL: 'taf',
      GW_PRESET_BACKEND_URL: 'presets',
      GW_FEATURE_FORCE_AUTHENTICATION: true,
    };

    const props = {
      onSetAuth: jest.fn(),
      isLoggedIn: false,
      onLogin: jest.fn(),
      auth: {
        username: 'Michael Jackson',
        token: '1223344',
        refresh_token: '33455214',
      },
      sessionStorageProvider,
    };

    // mock session storage
    jest.spyOn(Storage.prototype, 'getItem').mockReturnValue('false');
    render(
      <AuthenticationProvider value={props}>
        <TestWrapperWithRouter store={store}>
          <Route
            path={workspaceRoutes.root}
            element={<Workspace config={testConfigWithForceAuth} />}
          />
          <Route path="/login" element={<Login />} />
        </TestWrapperWithRouter>
      </AuthenticationProvider>,
    );

    expect(window.location.assign).toHaveBeenCalledWith('/login');
    expect(screen.queryByTestId('workspace')).toBeFalsy();
  });

  it('should not redirect to login if logged in', async () => {
    const store = createMockStore();
    const props = {
      onSetAuth: jest.fn(),
      isLoggedIn: true,
      onLogin: jest.fn(),
      auth: null,
      sessionStorageProvider: getSessionStorageProvider(),
    };

    // mock session storage
    jest.spyOn(Storage.prototype, 'getItem').mockReturnValue('true');

    render(
      <AuthenticationProvider value={props}>
        <TestWrapperWithRouter store={store}>
          <Route
            path={workspaceRoutes.root}
            element={
              <Workspace config={testConfig} createApi={createFakeApi} />
            }
          />
          <Route path="/login" element={<Login />} />
        </TestWrapperWithRouter>
      </AuthenticationProvider>,
    );

    expect(window.location.assign).not.toHaveBeenCalledWith('/login');
    expect(await screen.findByTestId('workspace')).toBeTruthy();
  });
});
