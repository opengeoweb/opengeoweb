/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { Route } from 'react-router-dom';
import { fireEvent, screen, render, waitFor } from '@testing-library/react';
import { routerActions } from '@opengeoweb/store';
import { ConfigType } from '@opengeoweb/shared';
import { workspaceRoutes } from '@opengeoweb/workspace';
import Header from './Header';
import { createMockStore } from '../../utils/testUtils';
import { TestWrapperWithRouter } from '../../components/Providers';

describe('components/Header', () => {
  it('should have the configured header text', async () => {
    const titleText = 'Test title';
    const store = createMockStore();
    render(
      <TestWrapperWithRouter store={store}>
        <Route
          path={workspaceRoutes.root}
          element={
            <Header
              config={
                {
                  GW_FEATURE_APP_TITLE: titleText,
                } as unknown as ConfigType
              }
            />
          }
        />
      </TestWrapperWithRouter>,
    );

    expect(await screen.findByText(titleText)).toBeTruthy();
  });

  it('should have the the default header', async () => {
    const store = createMockStore();
    render(
      <TestWrapperWithRouter store={store}>
        <Route
          path={workspaceRoutes.root}
          element={<Header config={{} as unknown as ConfigType} />}
        />
      </TestWrapperWithRouter>,
    );
    expect(await screen.findByText('GeoWeb')).toBeTruthy();
  });

  it('should switch to default workspace when logo clicked', async () => {
    const store = createMockStore();
    const spy = jest.spyOn(routerActions, 'navigateToUrl');

    render(
      <TestWrapperWithRouter store={store}>
        <Route
          path={workspaceRoutes.root}
          element={<Header config={{} as unknown as ConfigType} />}
        />
      </TestWrapperWithRouter>,
    );

    fireEvent.click(screen.getByText('GeoWeb'));

    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith({ url: workspaceRoutes.root });
    });
  });

  it('should switch to predefined workspace if GW_INITIAL_WORKSPACE_PRESET is defined in config.json', async () => {
    const store = createMockStore();
    const spy = jest.spyOn(routerActions, 'navigateToUrl');

    const workspaceId = 'radarSingleMapScreenConfig';
    const config = {
      GW_INITIAL_WORKSPACE_PRESET: workspaceId,
    };

    render(
      <TestWrapperWithRouter store={store}>
        <Route
          path={workspaceRoutes.root}
          element={<Header config={config as unknown as ConfigType} />}
        />
      </TestWrapperWithRouter>,
    );

    await waitFor(() => {
      // eslint-disable-next-line testing-library/no-wait-for-side-effects
      fireEvent.click(screen.getByText('GeoWeb'));
    });
    expect(spy).toHaveBeenCalledWith({ url: `/workspace/${workspaceId}` });
  });
});
