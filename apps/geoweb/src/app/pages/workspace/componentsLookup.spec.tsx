/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import {
  ObjectManagerMapButtonConnect,
  DrawingToolMapButtonConnect,
} from '@opengeoweb/warnings';

import { ServiceInterface } from '@opengeoweb/shared';
import { componentsLookUp } from './componentsLookUp';

describe('apps/geoweb/src/app/pages/workspace/componentsLookUp', () => {
  it('should return the HarmonieTempAndPrecipPreset component', () => {
    const payload = {
      title: 'title',
      componentType: 'HarmonieTempAndPrecipPreset' as const,
      id: 'viewid-1',
      initialProps: {
        layers: [],
        syncGroupsIds: [],
      },
    };
    const component = componentsLookUp(payload);
    expect(component).toBeDefined();
    expect(component.props['data-testid']).toEqual(
      'coreHarmonieTempAndPrecipPreset',
    );
  });
  it('should return the MultiMapView component when using a mapPreset', () => {
    const payload = {
      title: 'title',
      componentType: 'MultiMap' as const,
      id: 'viewid-1',
      initialProps: {
        mapPreset: [{}],
        shouldShowZoomControls: false,
        syncGroupsIds: [],
      },
    };
    const component = componentsLookUp(payload);
    expect(component).toBeDefined();
    expect(component.props['data-testid']).toEqual('coreMultiMapViewConnect');
  });
  it('should not return the MultiMapView component when not using a mapPreset', () => {
    const payload = {
      title: 'title',
      componentType: 'MultiMap' as const,
      id: 'viewid-1',
      initialProps: {
        shouldShowZoomControls: false,
        mapPreset: undefined,
        syncGroupsIds: [],
      },
    };
    const component = componentsLookUp(payload);
    expect(component).toBeNull();
  });
  it('should return the Map component', () => {
    const payload = {
      title: 'title',
      componentType: 'Map' as const,
      id: 'viewid-1',
      initialProps: {
        mapPreset: undefined,
        syncGroupsIds: [],
      },
    };
    const component = componentsLookUp(payload);
    expect(component).toBeDefined();
    expect(component.props.id).toEqual('viewid-1');
    expect(component.props['data-testid']).toEqual(
      'coreConfigurableMapConnect',
    );
    expect(component.props.mapControls).toBeFalsy();
    expect(component.props.shouldDisplayDrawControls).toBeFalsy();
  });

  it('should return the Map component with mapControls and shouldDisplayDrawControls when GW_DRAWINGS_BASE_URL is given', () => {
    const payload = {
      title: 'title',
      componentType: 'Map' as const,
      id: 'viewid-1',
      initialProps: {
        mapPreset: undefined,
        syncGroupsIds: [],
      },
    };
    const component = componentsLookUp(payload, {
      GW_DRAWINGS_BASE_URL: 'test-url',
    });
    expect(component).toBeDefined();
    expect(component.props.id).toEqual('viewid-1');
    expect(component.props['data-testid']).toEqual(
      'coreConfigurableMapConnect',
    );
    expect(component.props.mapControls).toEqual(
      <>
        <DrawingToolMapButtonConnect mapId="viewid-1" />
        <ObjectManagerMapButtonConnect mapId="viewid-1" />
      </>,
    );
    expect(component.props.shouldDisplayDrawControls).toBeTruthy();
  });

  it('should return the TimeSlider component', () => {
    const payload = {
      title: 'title',
      componentType: 'TimeSlider' as const,
      id: 'viewid-1',
      initialProps: {
        sliderPreset: {
          mapId: 'test-1',
        },
        syncGroupsIds: [],
      },
    };
    const component = componentsLookUp(payload);
    expect(component).toBeDefined();
    expect(component.props.sourceId).toEqual('viewid-1');
    expect(component.props['data-testid']).toEqual('coreTimeSliderConnect');
  });
  it('should return the TimeSeries component', () => {
    const payload = {
      title: 'title',
      componentType: 'TimeSeries' as const,
      id: 'viewid-1',
      initialProps: {
        plotPreset: {
          mapId: 'map1',
          plots: [],
          parameters: [],
        },
        services: [
          {
            id: 'serviceId',
            description: 'desc',
            type: 'EDR' as ServiceInterface,
            url: 'this_url',
            name: 'EDRserivce',
          },
        ],
      },
    };
    const component = componentsLookUp(payload);
    expect(component).toBeDefined();
    expect(component.props['data-testid']).toEqual('timeseriesPlot');
  });

  it('should return the WarningList component', () => {
    const payload = {
      title: 'title',
      componentType: 'WarningList' as const,
      id: 'viewid-1',
      initialProps: {},
    };
    const component = componentsLookUp(payload);
    expect(component).toBeDefined();

    expect(component.props['data-testid']).toEqual('publicWarningModule');
  });

  it('should return the null if not recognized', () => {
    const payload = {
      title: 'title',
      componentType: undefined!,
      id: 'viewid-1',
      initialProps: undefined!,
    };
    const component = componentsLookUp(payload);
    expect(component).toBeNull();
  });

  it('should return the TimeSlider component', () => {
    const payload = {
      title: 'title',
      componentType: 'TimeSlider' as const,
      id: 'viewid-1',
      initialProps: {
        sliderPreset: {
          mapId: 'test-1',
        },
        syncGroupsIds: [],
      },
    };
    const component = componentsLookUp(payload);
    expect(component).toBeDefined();
    expect(component.props.sourceId).toEqual('viewid-1');
    expect(component.props['data-testid']).toEqual('coreTimeSliderConnect');
  });

  it('should return the Taf component', () => {
    const payload = {
      title: 'title',
      componentType: 'TafModule' as const,
      id: 'viewid-1',
      initialProps: {},
    };
    const component = componentsLookUp(payload);
    expect(
      component.props.payload.initialProps.isAutoTafEnabled,
    ).toBeUndefined();

    expect(component).toBeDefined();
    expect(component.props['data-testid']).toEqual('tafmodule');
  });

  it('should return the Taf component with AutoTaf', () => {
    const payload = {
      title: 'title',
      componentType: 'TafModule' as const,
      id: 'viewid-1',
      initialProps: {
        isAutoTafEnabled: true,
      },
    };
    const component = componentsLookUp(payload);
    expect(component).toBeDefined();

    expect(component.props.payload.initialProps.isAutoTafEnabled).toBeTruthy();
    expect(component.props['data-testid']).toEqual('tafmodule');
  });
});
