/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import {
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { routerActions, uiActions } from '@opengeoweb/store';
import { AppHeader } from '@opengeoweb/shared';
import Menu, {
  MenuProps,
  PUBLIC_WARNINGS,
  SPACEWEATHER_WORKSPACE_ID,
  WARNINGS_WORKSPACE_ID,
  publicWarningDialogType,
} from './Menu';
import { AppStore } from '../../store';
import { TestWrapper } from '../../components/Providers';
import { createMockStore } from '../../utils/testUtils';

describe('/components/Menu', () => {
  const props: MenuProps = {
    config: {
      GW_INITIAL_PRESETS_FILENAME: '',
      GW_FEATURE_APP_TITLE: 'TestTitle',
      GW_FEATURE_FORCE_AUTHENTICATION: false,
      GW_FEATURE_MODULE_SPACE_WEATHER: true,
    },
  };

  const user = userEvent.setup();

  it('should toggle the preset menu', async () => {
    const store = createMockStore();
    render(
      <TestWrapper store={store}>
        <AppHeader menu={<Menu {...props} />} />
      </TestWrapper>,
    );

    expect(screen.queryByTestId('drawer')).toBeFalsy();

    fireEvent.click(screen.getByTestId('menuButton'));

    await waitFor(() => {
      expect(screen.getByTestId('drawer')).toBeTruthy();
    });

    expect(screen.queryByText('Aviation')).toBeFalsy();
  });

  it('should support keyboard navigation', async () => {
    const store = createMockStore();
    render(
      <TestWrapper store={store}>
        <AppHeader menu={<Menu {...props} />} />
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('menuButton'));

    await user.tab();
    const testTabIndex = (tabIndex = 0, expectedText = ''): void => {
      const element = within(screen.getByTestId('drawer')).getAllByRole(
        'button',
      )[tabIndex];
      expect(element.matches(':focus')).toBeTruthy();
      expect(within(element).getByText(expectedText));
    };

    testTabIndex(0, 'Auto Arrange Windows');
    await user.tab();
    testTabIndex(1, 'Sync Groups');
    await user.tab();
    testTabIndex(2, 'Space Weather Forecast');
    await user.tab({ shift: true });
    testTabIndex(1, 'Sync Groups');
    await user.tab({ shift: true });
    testTabIndex(0, 'Auto Arrange Windows');
  });

  it('should auto arrange windows', async () => {
    const screenConfig = {
      id: 'preset1',
      title: 'Preset 1',
      views: {
        allIds: ['screen1', 'screen2', 'screen3', 'screen4'],
        byId: {
          screen1: {
            title: 'screen 1',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
          screen2: {
            title: 'screen 2',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
          screen3: {
            title: 'screen 3',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
          screen4: {
            title: 'screen 4',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
        },
      },
      mosaicNode: {
        direction: 'row' as const,
        second: {
          direction: 'column' as const,
          first: {
            direction: 'row' as const,
            second: 'screen1',
            first: 'screen2',
          },
          second: 'screen3',
        },
        first: 'screen4',
      },
    };

    const mockState: AppStore = {
      workspace: screenConfig,
    };

    const store = createMockStore(mockState);
    render(
      <TestWrapper store={store}>
        <AppHeader menu={<Menu {...props} />} />
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('menuButton'));

    fireEvent.click(screen.getByText('Auto Arrange Windows'));

    const expectedState = {
      direction: 'row',
      first: {
        direction: 'column',
        first: 'screen4',
        second: 'screen2',
      },
      second: {
        direction: 'column',
        first: 'screen1',
        second: 'screen3',
      },
    };
    await waitFor(() => {
      expect(store.getState().workspace.mosaicNode).toEqual(expectedState);
    });
  });

  it('should show the aviation products when GW_PRESET_BACKEND_URL has set and fire correct actions', async () => {
    const spy = jest.spyOn(routerActions, 'navigateToUrl');
    const store = createMockStore();
    const testProps = {
      ...props,
      config: {
        ...props.config,
        GW_PRESET_BACKEND_URL: 'test',
      },
    };
    render(
      <TestWrapper store={store}>
        <Menu {...testProps} />
      </TestWrapper>,
    );

    expect(screen.getByText('Aviation')).toBeTruthy();

    fireEvent.click(screen.getByText('TAF'));
    expect(spy).toHaveBeenLastCalledWith({ url: '/workspace/screenConfigTaf' });

    fireEvent.click(screen.getByText('SIGMET'));
    expect(spy).toHaveBeenLastCalledWith({
      url: '/workspace/screenConfigSigmet',
    });

    fireEvent.click(screen.getByText('AIRMET'));
    expect(spy).toHaveBeenLastCalledWith({
      url: '/workspace/screenConfigAirmet',
    });
  });

  it('should show the warnings workspace when GW_DRAWINGS_BASE_URL and GW_PRESET_BACKEND_URL has been set and fire correct actions', async () => {
    const spy = jest.spyOn(routerActions, 'navigateToUrl');
    const store = createMockStore();
    const testProps = {
      ...props,
      config: {
        ...props.config,
        GW_DRAWINGS_BASE_URL: 'test',
        GW_PRESET_BACKEND_URL: 'test',
      },
    };

    store.dispatch(
      uiActions.registerDialog({
        type: publicWarningDialogType,
        setOpen: false,
        source: 'app',
      }),
    );

    render(
      <TestWrapper store={store}>
        <Menu {...testProps} />
      </TestWrapper>,
    );

    expect(screen.getByText(PUBLIC_WARNINGS)).toBeTruthy();
    fireEvent.click(screen.getByText(PUBLIC_WARNINGS));
    expect(spy).toHaveBeenLastCalledWith({
      url: `/workspace/${WARNINGS_WORKSPACE_ID}`,
    });
  });

  it('should show the spaceweather workspace when GW_FEATURE_MODULE_SPACE_WEATHER has been set and fire correct actions', async () => {
    const spy = jest.spyOn(routerActions, 'navigateToUrl');
    const store = createMockStore();
    const testProps = {
      ...props,
      config: {
        ...props.config,
        GW_FEATURE_MODULE_SPACE_WEATHER: true,
      },
    };

    render(
      <TestWrapper store={store}>
        <Menu {...testProps} />
      </TestWrapper>,
    );

    expect(screen.getByText('Space Weather Forecast')).toBeTruthy();
    fireEvent.click(screen.getByText('Space Weather Forecast'));
    expect(spy).toHaveBeenLastCalledWith({
      url: `/workspace/${SPACEWEATHER_WORKSPACE_ID}`,
    });
  });
});
