/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { render, screen } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import { ThemeWrapper } from '@opengeoweb/theme';
import { I18nextProvider } from 'react-i18next';
import NotFound from './404';
import i18n, {
  initGeowebI18n,
  translateKeyOutsideComponents,
} from '../../i18n';

beforeAll(() => {
  initGeowebI18n();
});

describe('pages/404', () => {
  it('should render correctly', async () => {
    const { baseElement } = render(
      <I18nextProvider i18n={i18n}>
        <ThemeWrapper>
          <Router>
            <NotFound />
          </Router>
        </ThemeWrapper>
      </I18nextProvider>,
    );

    expect(baseElement).toBeTruthy();
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('geoweb-404-error-message'),
      ),
    ).toBeTruthy();
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('geoweb-info-go-back'),
      ),
    ).toBeTruthy();
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('geoweb-info-go-back-to-app'),
      ),
    ).toBeTruthy();
  });
});
