/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';

import { I18nextProvider } from 'react-i18next';
import ReleaseNotes from './ReleaseNotes';
import i18n, {
  initGeowebI18n,
  translateKeyOutsideComponents,
} from '../../i18n';

beforeAll(() => {
  initGeowebI18n();
});

describe('ReleaseNotes', () => {
  it('should show the version', async () => {
    const props = {
      version: '1.0.1',
      releasePage: '',
      versionProgressNotesPage: '',
      handleClose: jest.fn(),
    };

    render(
      <I18nextProvider i18n={i18n}>
        <ReleaseNotes {...props} />
      </I18nextProvider>,
    );

    expect(
      screen.getByText(
        translateKeyOutsideComponents('geoweb-release-notes-version', {
          version: props.version,
        }),
      ),
    ).toBeTruthy();
  });
  it('should close the dialog', async () => {
    const props = {
      version: '1.0.1',
      releasePage: '',
      versionProgressNotesPage: '',
      handleClose: jest.fn(),
    };

    render(
      <I18nextProvider i18n={i18n}>
        <ReleaseNotes {...props} />
      </I18nextProvider>,
    );

    fireEvent.click(screen.getByTestId('closeReleaseNotes'));
    expect(props.handleClose).toHaveBeenCalled();
  });
  it('should open the release notes page in a new tab', async () => {
    const storedWindowOpen = window.open;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    delete window.open;
    window.open = jest.fn();

    const props = {
      version: '1.0.1',
      releasePage: '',
      versionProgressNotesPage:
        'http://confluence.knmi.nl/display/GW/Release+notes',
      handleClose: jest.fn(),
    };
    render(
      <I18nextProvider i18n={i18n}>
        <ReleaseNotes {...props} />
      </I18nextProvider>,
    );

    fireEvent.click(screen.getByTestId('openReleasePage'));
    await waitFor(() =>
      expect(window.open).toHaveBeenCalledWith(
        'http://confluence.knmi.nl/display/GW/Release+notes',
        '_blank',
      ),
    );
    window.open = storedWindowOpen;
  });
});
