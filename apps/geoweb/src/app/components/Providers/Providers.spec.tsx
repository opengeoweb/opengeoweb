/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { useConfirmationDialog } from '@opengeoweb/shared';
import { initGeowebI18n } from '../../../i18n';
import { AppWrapper } from './Providers';
import { createMockStore } from '../../utils/testUtils';

describe('components/Providers', () => {
  describe('AppWrapper', () => {
    it('be able to use the confirmation dialog', async () => {
      initGeowebI18n();
      const store = createMockStore();
      const options = {
        title: 'test title',
        description: 'test description',
      };
      const confirm = 'simple confirm';
      const TestComponent: React.FC = () => {
        const confirmDialog = useConfirmationDialog();
        return (
          <button
            type="button"
            onClick={async (): Promise<void> => {
              await confirmDialog(options);
            }}
          >
            {confirm}
          </button>
        );
      };

      render(
        <AppWrapper store={store}>
          <TestComponent />
        </AppWrapper>,
      );
      expect(screen.queryByText(options.title)).toBeFalsy();
      const button = screen.queryByRole('button')!;

      fireEvent.click(button);
      expect(screen.getByText(options.title)).toBeTruthy();
      expect(screen.getByText(options.description)).toBeTruthy();

      // close it again
      fireEvent.click(screen.getByRole('button', { name: /Close/i }));

      await waitFor(() =>
        expect(screen.queryByText(options.title)).toBeFalsy(),
      );
    });
  });
});
