/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import BrowserIssueDialog from './BrowserIssueDialog';
import { TestWrapper } from './Providers';
import { createMockStore } from '../utils/testUtils';

describe('components/BrowserIssueDialog', () => {
  let browserGetter: jest.SpyInstance<string, [], unknown>;
  beforeEach(() => {
    browserGetter = jest.spyOn(window.navigator, 'userAgent', 'get');
  });

  it('should open and close the dialog', async () => {
    const store = createMockStore();
    render(
      <TestWrapper store={store}>
        <BrowserIssueDialog />
      </TestWrapper>,
    );
    expect(screen.getByText('Browser issue')).toBeTruthy();
    fireEvent.click(screen.getByTestId('customDialog-close'));
    await waitFor(() =>
      expect(screen.queryByText('Browser issue')).toBeFalsy(),
    );
  });

  it('should display the correct message when using firefox', async () => {
    browserGetter.mockReturnValue('firefox');
    const store = createMockStore();
    render(
      <TestWrapper store={store}>
        <BrowserIssueDialog />
      </TestWrapper>,
    );
    expect(screen.getByText('Browser issue')).toBeTruthy();
    expect(screen.getByTestId('firefoxMessage')).toBeTruthy();
  });

  it('should display the correct message when not using firefox', async () => {
    browserGetter.mockReturnValue('chrome');
    const store = createMockStore();
    render(
      <TestWrapper store={store}>
        <BrowserIssueDialog />
      </TestWrapper>,
    );
    expect(screen.getByText('Browser issue')).toBeTruthy();
    expect(screen.queryByTestId('firefoxMessage')).toBeFalsy();
  });
});
