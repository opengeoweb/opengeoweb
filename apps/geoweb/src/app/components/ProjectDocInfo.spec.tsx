/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import {
  ProjectDocInfo,
  technicalDocLinks,
  userDocLinks,
} from './ProjectDocInfo';
import { initGeowebI18n, translateKeyOutsideComponents } from '../../i18n';

beforeAll(() => {
  initGeowebI18n();
});

describe('ProjectDocInfo', () => {
  it('should open the project doc page', async () => {
    render(<ProjectDocInfo />);
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('geoweb-project-documentation'),
      ),
    );
    await waitFor(() => {
      expect(
        screen.getByText(
          translateKeyOutsideComponents('geoweb-user-doc-title'),
        ),
      ).toBeTruthy();
    });
  });

  it('should render all user and technical doc links', async () => {
    render(<ProjectDocInfo />);
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('geoweb-project-documentation'),
      ),
    );
    await waitFor(() => {
      [...userDocLinks, ...technicalDocLinks].forEach((docLink) => {
        expect(
          screen.getByText(translateKeyOutsideComponents(docLink.title)),
        ).toBeTruthy();
        expect(
          screen.getByText(translateKeyOutsideComponents(docLink.subTitle)),
        ).toBeTruthy();
      });
    });
  });
});
