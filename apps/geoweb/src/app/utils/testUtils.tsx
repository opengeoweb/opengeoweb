/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { createToolkitMockStore } from '@opengeoweb/shared';
import { snackbarListener, snackbarReducer } from '@opengeoweb/snackbar';
import {
  syncGroupsListener,
  syncGroupsReducer,
  uiReducer,
} from '@opengeoweb/store';
import { workspaceReducer, workspaceListener } from '@opengeoweb/workspace';
import { Store } from '@reduxjs/toolkit';
import { AppStore } from '../store';

const reducer = {
  snackbar: snackbarReducer,
  syncGroups: syncGroupsReducer,
  workspace: workspaceReducer,
  ui: uiReducer,
};

export const createMockStore = (mockState?: AppStore | undefined): Store =>
  createToolkitMockStore({
    reducer,
    preloadedState: mockState,
    middleware: [
      snackbarListener.middleware,
      syncGroupsListener.middleware,
      workspaceListener.middleware,
    ],
  });
