/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

describe('LayerManager', () => {
  it('should duplicate and remove a layer', () => {
    cy.mockExampleConfigNoFeatures();
    cy.mockRadarGetCapabilities();
    cy.visit('/');
    cy.findByTestId('workspaceMenuButton').click();

    // select preset radar preset
    cy.findAllByText('Radar').first().click({ force: true });
    cy.findByText('Workspace menu').should('not.exist');

    // open layermanager
    cy.waitOnRadarGetCapabilities();
    cy.findByTestId('layerManagerButton').click();
    cy.get('.layerRow').then(($elements) => {
      const listLength = $elements.length;

      // duplicate a layer
      cy.findAllByTestId('openMenuButton').last().click();
      cy.findByText('Duplicate layer').click();
      cy.findAllByRole('button', { name: 'Leading layer' }).should(
        'have.length',
        listLength + 1,
      );

      // open legend
      cy.findByTestId('open-Legend').click();
      cy.findAllByTestId('legend').should('have.length', listLength + 1);

      // // remove a layer
      cy.findAllByTestId('deleteButton').last().click();
      cy.findAllByRole('button', { name: 'Leading layer' }).should(
        'have.length',
        listLength,
      );

      cy.findAllByTestId('legend').should('have.length', listLength);

      // close the layermanager
      cy.findByTestId('layerManagerWindow').findByTestId('closeBtn').click();
      cy.findByTestId('layerManagerWindow').should('not.exist');
    });
  });

  it('should drag and drop layers', () => {
    cy.mockExampleConfigNoFeatures();
    cy.mockRadarGetCapabilities();
    cy.visit('/');
    cy.findByTestId('workspaceMenuButton').click();

    // select preset radar preset
    cy.findAllByText('Radar').first().click({ force: true });
    cy.findByText('Workspace menu').should('not.exist');

    // open layermanager
    cy.waitOnRadarGetCapabilities();
    cy.findByTestId('layerManagerButton').click();
    // Check that Layer Manager is shown
    cy.findAllByTestId('selectOpacity').should('have.length', 6);

    // change the opacity of the first layer so you can check after dragging this layer has moved down
    cy.findAllByTestId('selectOpacity').eq(1).click();
    cy.findByText('50 %', { timeout: 40_000 }).click({ force: true });
    cy.findAllByTestId('selectOpacity').eq(1).click();
    cy.findAllByTestId('selectOpacity').eq(1).contains('50');
    cy.findAllByTestId('selectOpacity').eq(3).contains('100'); // second
    cy.findAllByTestId('selectOpacity').eq(5).contains('100');

    // Move the second layer up
    cy.get('.column-1 [data-testid=dragHandle]')
      .eq(1)
      .dragTo(cy.get('.column-1 [data-testid=dragHandle]').eq(0));

    cy.findAllByTestId('selectOpacity')
      .first()
      .contains('100', { timeout: 5000 });
    cy.findAllByTestId('selectOpacity').eq(3).contains('50'); // second
    cy.findAllByTestId('selectOpacity').last().contains('100');

    // close the layermanager
    cy.findByTestId('layerManagerWindow').findByTestId('closeBtn').click();
    cy.findByTestId('layerManagerWindow').should('not.exist');
  });

  it('should not show the Map presets when there is no GW_PRESET_BACKEND_URL', () => {
    cy.mockExampleConfigNoFeatures();
    cy.visit('/');
    cy.findByTestId('layerManagerWindow').should('be.visible');
    cy.findByText('New map preset').should('not.exist');
  });

  it('should show the Map presets when there is a GW_PRESET_BACKEND_URL', () => {
    cy.mockExampleConfig();
    cy.mockCognitoLoginSuccess();
    cy.findByTestId('layerManagerWindow').should('be.visible');
    cy.findAllByText('New map preset').first().should('be.visible');
    cy.mockCognitoLogoutSuccess();
  });

  it('should not resize legend outside the view', () => {
    cy.mockExampleConfigNoFeatures();
    cy.visit('/');

    // close layermanager
    cy.findByTestId('layerManagerButton').click();

    // open legend
    cy.findByTestId('open-Legend').click();
    cy.findByTestId('moveable-legend')
      .findByLabelText('Close')
      .should('be.visible');

    // split screen
    cy.findByLabelText('Split window vertically').click();

    // resize legend
    cy.findByTestId('moveable-legend')
      .findAllByTestId('resizeHandle')
      .first()
      .parent()
      .as('resizeHandle')
      .should('exist');

    cy.get('@resizeHandle').trigger('mousedown', { force: true });
    cy.get('@resizeHandle').trigger('mousemove', {
      force: true,
      clientX: 1200,
    });
    cy.get('@resizeHandle').trigger('mouseup', { force: true });

    // check that the legend close button is still visible to make sure you cannot resize outside of the view
    cy.findByTestId('moveable-legend')
      .findByLabelText('Close')
      .should('be.visible');
  });
});
