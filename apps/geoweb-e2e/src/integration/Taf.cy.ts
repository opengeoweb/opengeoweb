/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

describe('Taf', () => {
  beforeEach(() => {
    cy.mockExampleConfig();
    cy.mockWorkspaces();
    cy.mockCognitoLoginSuccess();
    cy.mockFetchTAFList();
    cy.mockGetTac();
  });

  afterEach(() => {
    cy.mockCognitoLogoutSuccess();
  });

  it('should drag and drop a changegroup', () => {
    cy.findByTestId('workspaceMenuButton').click();
    cy.wait('@workspaceList');

    // Open the TAF module
    cy.findByText('TAF').click();
    // Check that menu is closed
    cy.findByText('Workspace menu').should('not.exist');

    cy.wait('@workspaceTaf');
    cy.wait('@viewpresetTaf');
    // Check that the TAF module is shown
    cy.get('#tafmodule').should('be.visible');
    cy.wait('@taflist');

    // The first TAF should be active
    cy.findByTestId('location-tabs')
      .get('li:visible')
      .first()
      .should('have.class', 'Mui-selected');
    // Make sure we are in editor mode
    cy.findByTestId('switchMode').should('have.class', 'Mui-checked');
    // Add a new changegroup at the bottom
    cy.get('[data-testid="tafFormOptions[4]"]').click();
    cy.findByText('Insert 1 row below').click();
    cy.get('[name="changeGroups[5].change"]').should('have.value', '');
    // Add data to the second to last changegroup
    cy.get('[name="changeGroups[4].change"]').type('f');
    cy.get('[name="changeGroups[4].change"]').should('have.value', 'FM');
    // Click publish to trigger validations
    cy.get('[data-testid="publishtaf"]').click();
    // Make sure the correct number of issues is shown
    cy.findByTestId('issuesButton').invoke('text').should('equal', '9 issues');
    // Save the number of issues shown to check later
    cy.findByTestId('issuesButton')
      .invoke('text')
      .then((issuesText) => {
        // Move the new changegroup up
        cy.get('[data-testid=dragHandle-5]').dragTo(
          cy.get('[data-testid=dragHandle-4]'),
        );
        cy.get('[name="changeGroups[4].change"]').should('have.value', '');
        cy.get('[name="changeGroups[5].change"]').should('have.value', 'FM');
        // Check that the number of issues is still the same
        cy.findByTestId('issuesButton')
          .invoke('text')
          .should('equal', issuesText);
      });
  });

  it('should import a TAF from TAC Overview', () => {
    cy.findByTestId('workspaceMenuButton').click();
    cy.wait('@workspaceList');
    // Open the TAF module
    cy.findByText('TAF').click();
    // Check that menu is closed
    cy.findByText('Workspace menu').should('not.exist');
    // Check that the TAF module is shown
    cy.get('#tafmodule').should('be.visible');
    cy.wait('@taflist');
    // The first TAF should be active
    cy.findByTestId('location-tabs')
      .get('li:visible')
      .first()
      .should('have.class', 'Mui-selected');
    // Make sure we are in editor mode
    cy.findByTestId('switchMode').should('have.class', 'Mui-checked');
    // Delete all changegroups
    cy.get('[data-testid="tafFormOptions[4]"]').click();
    cy.findByText('Delete row').click();
    cy.findByText('Yes').click();
    cy.get('[data-testid="tafFormOptions[3]"]').click();
    cy.findByText('Delete row').click();
    cy.findByText('Yes').click();
    cy.get('[data-testid="tafFormOptions[2]"]').click();
    cy.findByText('Delete row').click();
    cy.findByText('Yes').click();
    cy.get('[data-testid="tafFormOptions[1]"]').click();
    cy.findByText('Delete row').click();
    cy.findByText('Yes').click();
    cy.get('[data-testid="tafFormOptions[0]"]').click();
    cy.findByText('Delete row').click();
    cy.findByText('Yes').click();
    // There should be no changegroups
    cy.findByTestId('row-changeGroups[0]').should('not.exist');
    cy.findByTestId('row-changeGroups[1]').should('not.exist');
    cy.findByTestId('row-changeGroups[2]').should('not.exist');
    cy.findByTestId('row-changeGroups[3]').should('not.exist');
    cy.findByTestId('row-changeGroups[4]').should('not.exist');
    // Import TAF from TAC Overview
    cy.findByText('Current & Expired').click();
    cy.wait('@getTac');
    cy.findByTestId('export-tac-button').click();
    // Form values should be imported
    cy.get('[name="baseForecast.wind"]').should('have.value', '18010');
    cy.get('[name="baseForecast.visibility"]').should('have.value', 'CAVOK');
    // There should be two changegroups
    cy.findByTestId('row-changeGroups[0]').should('exist');
    cy.findByTestId('row-changeGroups[1]').should('exist');
    // Changegroup values should also be imported
    cy.get('[name="changeGroups[0].change"]').should('have.value', 'BECMG');
    cy.get('[name="changeGroups[1].change"]').should('have.value', 'FM');
    // There should be two validation errors
    cy.findByText('2 issues').should('be.visible');
    // add a new row
    cy.get('[data-testid="tafFormOptions[1]"]').click();

    cy.findByText('Insert 1 row below').click();
    cy.get('[name="changeGroups[2].change"]').should('have.value', '');
    cy.get('[name="changeGroups[2].change"]').type('test');

    // Click publish to trigger validations
    cy.get('[data-testid="publishtaf"]').click();
    // Make sure the correct number of issues is shown
    cy.findByTestId('issuesButton').invoke('text').should('equal', '5 issues');
  });

  it('should be able to switch location and clear current taf values', () => {
    cy.findByTestId('workspaceMenuButton').click();
    cy.wait('@workspaceList');

    // Open the TAF module
    cy.findByText('TAF').click();
    // Check that menu is closed
    cy.findByText('Workspace menu').should('not.exist');

    cy.wait('@workspaceTaf');
    cy.wait('@viewpresetTaf');
    // Check that the TAF module is shown
    cy.get('#tafmodule').should('be.visible');
    cy.wait('@taflist');

    // The first TAF should be active
    cy.findByTestId('location-tabs')
      .get('li:visible')
      .first()
      .should('have.class', 'Mui-selected');

    cy.findByTestId('location-tabs').get('li:visible').eq(1).click();

    // Make sure we are in editor mode
    cy.findByTestId('switchMode').should('have.class', 'Mui-checked');

    // Add some data
    cy.get('[name="changeGroups[0].change"]').type('TEMPO');
    cy.get('[name="changeGroups[1].change"]').type('TEMPO');
    cy.get('[name="changeGroups[2].change"]').type('TEMPO');
    cy.get('[name="changeGroups[3].change"]').type('TEMPO');
    cy.get('[name="changeGroups[4].change"]').type('TEMPO');
    cy.get('[data-testid="tafFormOptions[4]"]').click();
    cy.findByText('Insert 1 row below').click();
    cy.get('[name="changeGroups[5].change"]').type('TEMPO');

    // click first location
    cy.findByTestId('location-tabs').get('li:visible').eq(0).click();
    cy.wait('@taflist');

    cy.get('[name="changeGroups[0].change"]').should('have.value', '');
    cy.get('[name="changeGroups[1].change"]').should('have.value', '');
    cy.get('[name="changeGroups[2].change"]').should('have.value', '');
    cy.get('[name="changeGroups[3].change"]').should('have.value', '');
    cy.get('[name="changeGroups[4].change"]').should('have.value', '');
    cy.get('[data-testid="tafFormOptions[4]"]').click();
    cy.findByText('Insert 1 row below').click();
    cy.get('[name="changeGroups[5].change"]').should('have.value', '');
  });
});
