/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

const convertToUTCString = (date: string): string => {
  const day = Number(date.slice(0, 2));
  const month = Number(date.slice(3, 5));
  const year = Number(date.slice(6, 10));
  const hours = Number(date.slice(11, 13));
  const minutes = Number(date.slice(14, 16));

  return new Date(Date.UTC(year, month - 1, day, hours, minutes)).toISOString();
};

/** helper function to convert date to a later time */
export const getLaterDate = (
  date: string,
  amountToAdd: number,
  unitOfTime: 'hours' | 'minutes' = 'hours',
  convertToUTC = true, // If input date string is in format "dd/MM/yyyy HH:mm"
  timepickerFormat = false, // If output needs to be in datepicker format "dd/MM/yyyy HH:mm"
  roundToNextHalfHour = true, // Round to next 30 minutes
  addOffset = false, // Apply the 15-minute offset logic
): string => {
  const currentDateUTC = convertToUTC ? convertToUTCString(date) : date;
  const currentDate = new Date(currentDateUTC);

  // Add time to the current date
  if (unitOfTime === 'hours') {
    currentDate.setUTCHours(currentDate.getUTCHours() + amountToAdd);
  } else {
    currentDate.setUTCMinutes(currentDate.getUTCMinutes() + amountToAdd);
  }

  // Apply the 15-minute offset logic before rounding
  if (addOffset) {
    currentDate.setUTCMinutes(currentDate.getUTCMinutes() + 15);
  }

  // Round to the next half-hour
  if (roundToNextHalfHour) {
    const minutes = currentDate.getUTCMinutes();

    if (minutes > 0 && minutes <= 30) {
      currentDate.setUTCMinutes(30);
    } else if (minutes > 30) {
      currentDate.setUTCMinutes(0);
      currentDate.setUTCHours(currentDate.getUTCHours() + 1);
    } else {
      currentDate.setUTCMinutes(0);
    }
  }

  // Convert to ISO string if not timepicker format
  const dateString = currentDate.toISOString();

  if (!timepickerFormat) {
    return dateString;
  }

  // Construct 'dd/MM/yyyy HH:mm' format
  return `${getDayFromDate(dateString)}/${getMonthFromDate(dateString)}/${currentDate.getUTCFullYear()} ${getHoursFromDate(dateString)}:${getMinutesFromDate(dateString)}`;
};

export const getMinutesFromDate = (date: string): string => {
  const newDate = new Date(date).getUTCMinutes();
  return newDate < 10 ? `0${newDate.toString()}` : newDate.toString();
};

// Pass passedUTC = false in case you are passing date string formatted as used in the date/time picker ('dd/MM/yyyy HH:mm')
export const getHoursFromDate = (date: string, passedUTC = true): string => {
  if (!passedUTC) {
    const time = date.split(' ');
    const hours = time[1].split(':');
    return hours[0];
  }
  const hours = new Date(date).getUTCHours();
  return hours < 10 ? `0${hours.toString()}` : hours.toString();
};

// Pass passedUTC = false in case you are passing date string formatted as used in the date/time picker ('dd/MM/yyyy HH:mm')
export const getDayFromDate = (date: string, passedUTC = true): string => {
  if (!passedUTC) {
    const d1 = date.split(' ');
    const day = d1[0].split('/');
    return day[0];
  }
  const newDate = new Date(date).getUTCDate();
  return newDate < 10 ? `0${newDate.toString()}` : newDate.toString();
};

// Pass passedUTC = false in case you are passing date string formatted as used in the date/time picker ('dd/MM/yyyy HH:mm')
export const getMonthFromDate = (date: string, passedUTC = true): string => {
  if (!passedUTC) {
    const d1 = date.split(' ');
    const month = d1[0].split('/');
    return month[1];
  }
  const newDate = new Date(date).getUTCMonth() + 1;
  return newDate < 10 ? `0${newDate.toString()}` : newDate.toString();
};
