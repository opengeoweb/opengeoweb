/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
'use strict';

module.exports = {
  'no-moment': {
    meta: {
      docs: {
        description: 'no moment usage',
        category: 'Deprecated',
        recommended: false,
      },
      schema: [],
    },
    create(context) {
      return {
        Identifier: function (node) {
          if (node.name === 'moment' || node.name === 'Moment')
            context.report({
              node,
              message:
                "Moment is deprecated in Geoweb. Use date library 'dateUtils' from '@opengeoweb/shared' instead",
            });
        },
      };
    },
  },
};
