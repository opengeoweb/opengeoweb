/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

// ---> Sentry related code
// This file is used to replace "webpackConfig": "@nx/react/plugins/webpack" inside apps/geoweb.project.json

const { composePlugins, withNx } = require('@nx/webpack');
const { withReact } = require('@nx/react');
const { sentryWebpackPlugin } = require('@sentry/webpack-plugin');

module.exports = composePlugins(withNx(), withReact(), (config) => {
  config.plugins = config.plugins || [];

  // During development there is no SENTRY_AUTH_TOKEN and we don´t want to upload source maps to Sentry
  const isPipelineEnvironemnt = !!process.env.SENTRY_AUTH_TOKEN;
  if (!isPipelineEnvironemnt) {
    return config;
  }

  // In pipeline, we have SENTRY_AUTH_TOKEN, and we do want to upload source maps to Sentry
  config.devtool = 'source-map';
  config.plugins.push(
    sentryWebpackPlugin({
      authToken: process.env.SENTRY_AUTH_TOKEN,
      org: 'finnish-meteorological-inst-cb',
      project: 'geoweb',
      include: '.',
      ignore: ['node_modules', 'webpack.config.js'],
      sourceMap: true,
    }),
  );

  return config;
});
// Sentry related code <--
