/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { MapPreset } from '@opengeoweb/store';

export interface AllowedUnits {
  unit_type: string;
  allowed_units: string[];
}

// config types
export type FIRLocationGeoJson = GeoJSON.FeatureCollection<
  GeoJSON.Geometry,
  { selectionType?: string }
>;

export interface LevelMinMaxType {
  FT?: number;
  FL?: number;
  M?: number;
}

export interface MovementMinMaxType {
  KT?: number;
  KMH?: number;
}

export interface CloudLevelMinMaxType {
  FT?: number;
  M?: number;
}

export interface SurfaceWindMinMaxType {
  KT?: number;
  MPS?: number;
}

export interface FIRArea {
  fir_name: string;
  fir_location: FIRLocationGeoJson;
  location_indicator_atsr: string;
  location_indicator_atsu: string;
  max_hours_of_validity: number;
  hours_before_validity: number;
  level_min?: LevelMinMaxType;
  level_max?: LevelMinMaxType;
  level_rounding_FL?: number;
  level_rounding_FT?: number;
  level_rounding_M?: number;
  movement_min?: MovementMinMaxType;
  movement_max?: MovementMinMaxType;
  movement_rounding_kt?: number;
  movement_rounding_kmh?: number;
  max_polygon_points?: number;
  units: AllowedUnits[];
}

export interface FIRConfigSigmet extends FIRArea {
  tc_max_hours_of_validity: number;
  tc_hours_before_validity: number;
  va_max_hours_of_validity: number;
  va_hours_before_validity: number;
  adjacent_firs: string[];
  area_preset: string;
}

export interface FIRConfigAirmet extends FIRArea {
  cloud_level_min?: CloudLevelMinMaxType;
  cloud_level_max?: CloudLevelMinMaxType;
  cloud_lower_level_min?: CloudLevelMinMaxType;
  cloud_lower_level_max?: CloudLevelMinMaxType;
  cloud_level_rounding_ft?: number;
  cloud_level_rounding_m_below?: number;
  cloud_level_rounding_m_above?: number;
  wind_direction_rounding?: number;
  wind_speed_max?: SurfaceWindMinMaxType;
  wind_speed_min?: SurfaceWindMinMaxType;
  visibility_max?: number;
  visibility_min?: number;
  visibility_rounding_below?: number;
  visibility_rounding_above?: number;
}

type SigmetFIRArea = Record<string, FIRConfigSigmet>;
type AirmetFIRArea = Record<string, FIRConfigAirmet>;

export type Firareas = SigmetFIRArea | AirmetFIRArea;

export interface ProductConfig {
  location_indicator_mwo: string;
  active_firs: string[];
  valid_from_delay_minutes: number;
  default_validity_minutes: number;
  fir_areas: Firareas;
  // uses mapPreset key (and no snake_case) to keep it in line with TS type
  mapPreset?: MapPreset;
  omit_change_va_and_rdoact_cloud?: boolean;
}

export interface SigmetConfig extends ProductConfig {
  fir_areas: SigmetFIRArea;
}

export interface AirmetConfig extends ProductConfig {
  fir_areas: AirmetFIRArea;
}
