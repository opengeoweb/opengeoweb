/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  AirmetMovementType,
  AirmetPhenomena,
  ATSRLocation,
  ATSULocation,
  Change,
  CloudLevelInfoMode,
  CloudLevelUnits,
  DateType,
  Direction,
  ICAOLocation,
  LevelInfoMode,
  LevelUnits,
  MovementUnit,
  MWOLocation,
  ObservationOrForcast,
  ProductCanbe,
  ProductSequenceId,
  ProductStatus,
  SigmetMovementType,
  SigmetPhenomena,
  VisibilityCause,
  VisibilityUnit,
  WindUnit,
} from './definitions';

export interface LevelType {
  value: number; //  height value
  unit: LevelUnits;
}

export interface CloudLevelType {
  value: number; //  height value
  unit: CloudLevelUnits;
}

/* Point coordinate in lat/lon system */
export interface Coordinate {
  latitude: number;
  longitude: number;
}

// This is the base sigmet/airmet structure, used for both the Sigmet, Airmet and CancelSigmet, CancelAirmet
export interface BaseProduct {
  /* Start of validity time for SIGMET/AIRMET */
  validDateStart: DateType;

  validDateEnd: DateType;
  firName: ICAOLocation;

  /* WMO name of Meteorological Watch Office (issuer): "EHDB", */
  locationIndicatorMWO: MWOLocation;
  locationIndicatorATSU: ATSULocation;
  locationIndicatorATSR: ATSRLocation;

  /* --- The following properties are controlled by the backend: --- */

  uuid?: string;
  sequence?: ProductSequenceId;

  /* Date of issuing (publishing) of SIGMET/AIRMET in ISO8601: "2020-09-17T12:03:00Z"
   * issueDate is needed in case of status 'PUBLISHED' */
  issueDate?: DateType;
}

interface Product extends BaseProduct {
  /* Current validity status of this airmet */
  status: ProductStatus;

  /* Whether this product is based on an observation or on a forecast */
  isObservationOrForecast?: ObservationOrForcast;
  observationOrForecastTime?: DateType; // Datetime of observation or forecast in ISO8601:

  change?: Change;

  /* Description of the vertical extent of the product phenomenon, described by 1 or 2 levels and a level mode. */
  levelInfoMode?: LevelInfoMode;

  /* In case lowerlevel is specified, this acts as the upper level, otherwise it is the main level */
  level?: LevelType;

  /* Lowerlevel is only used for between and is not surface */
  lowerLevel?: LevelType;

  /* A GeoJSON Feature object describing the fir as used to calculate the intersection between drawn start/end and fir   */
  firGeometry?: GeoJSON.FeatureCollection;

  /* A GeoJSON Feature object describing the startposition of the product area as the forecaster specified it.
   * This has a property "selectionType" describing the type of the geometry ("point", box", "poly" or "fir")   */
  startGeometry: GeoJSON.FeatureCollection;

  /* A GeoJSON Feature object describing the intersection of the start geometry and the FIR area.
   * This field will be generated either in the frontend or in the backend. */
  startGeometryIntersect: GeoJSON.FeatureCollection;

  /* If movementType is MOVEMENT, a movement speed and direction should be given */
  movementSpeed?: number;
  movementUnit?: MovementUnit;
  movementDirection?: Direction;
}

export interface Sigmet extends Product {
  phenomenon: SigmetPhenomena;

  /* Type of Sigmet (e.g normal/test/excercise)
   * SHORT_TEST and SHORT_VA_TEST are only used in the frontend and converted to TEST for API calls */
  type: 'NORMAL' | 'TEST' | 'EXERCISE' | 'SHORT_TEST' | 'SHORT_VA_TEST';

  /* (optional) A GeoJSON Feature object describing the startposition of the SIGMET area as the forecaster specified it.
   * This has a property "selectionType" describing the type of the geometry ("point", box", "poly" or "fir")
   * Rule: When an endGeometry is specified, you should not specify a movement speed and direction */
  endGeometry?: GeoJSON.FeatureCollection;

  /* (optional) A GeoJSON Feature object describing the intersection of the start geometry and the FIR area.
   * This field will be generated either in the frontend or in the backend. */
  endGeometryIntersect?: GeoJSON.FeatureCollection;

  movementType?: SigmetMovementType;

  /** Extra properties in case of va SIGMET */
  vaSigmetVolcanoName?: string;
  vaSigmetVolcanoCoordinates?: Coordinate;

  /** Extra properties in case of cancel va SIGMET, if set to unknown, property not set */
  vaSigmetMoveToFIR?: ICAOLocation;
}

/**
 * To cancel a published sigmet
 * To cancel a published sigmet, we send a cancel sigmet with all the mandatory properties from the basesigmet. */
export interface CancelSigmet extends BaseProduct {
  /* The sigmet sequence id to cancel */
  cancelsSigmetSequenceId: ProductSequenceId;

  /* Start of validity time for the SIGMET to cancel */
  validDateStartOfSigmetToCancel: DateType;

  /* End of validity time for the SIGMET to cancel in ISO8601: "2020-09-17T12:03:00Z" */
  validDateEndOfSigmetToCancel: DateType;

  // The status of the CancelSigmet, should only be set to PUBLISHED
  // You cannot save a draft a cancelsigmet, can only be 'PUBLISHED'
  status: ProductStatus.PUBLISHED;

  /** Extra properties in case of cancel va SIGMET, if set to unknown, property not set */
  vaSigmetMoveToFIR?: ICAOLocation;
}

export interface Airmet extends Product {
  phenomenon: AirmetPhenomena;

  /* Current validity status of this airmet */
  status: ProductStatus;

  /* Type of airmet (e.g normal/test/excercise) */
  type: 'NORMAL' | 'TEST' | 'EXERCISE';

  movementType: AirmetMovementType;

  /* Only for phenomenon surface wind */
  windSpeed?: number;
  windUnit?: WindUnit;
  windDirection?: number;

  /* Only for phenomenon surface visibility */
  visibilityValue?: number;
  visibilityCause?: VisibilityCause;
  visibilityUnit?: VisibilityUnit;

  /* Only for Cloud phenomena */
  cloudLevelInfoMode?: CloudLevelInfoMode;
  cloudLevel?: CloudLevelType;
  cloudLowerLevel?: CloudLevelType;
}

// To cancel a published airmet, we send a cancel airmet with all the mandatory properties from the baseairmet.
export interface CancelAirmet extends BaseProduct {
  /* The airmet sequence id to cancel */
  cancelsAirmetSequenceId: ProductSequenceId;

  /* Start of validity time for the Airmet to cancel */
  validDateStartOfAirmetToCancel: DateType;

  /* End of validity time for the Airmet to cancel in ISO8601: "2020-09-17T12:03:00Z" */
  validDateEndOfAirmetToCancel: DateType;

  /* The status of the CancelAirmet, should only be set to PUBLISHED
   * You cannot save a draft a cancelairmet, can only be 'PUBLISHED' */
  status: ProductStatus.PUBLISHED;
}

// Type with sigmet info from the backend
export interface SigmetFromBackend {
  sigmet: Sigmet | CancelSigmet;
  creationDate: DateType;
  lastUpdateDate: DateType;
  uuid: string;
  canbe: ProductCanbe[];
}

// Type with airmet info from the backend
export interface AirmetFromBackend {
  airmet: Airmet | CancelAirmet;
  creationDate: DateType;
  lastUpdateDate: DateType;
  uuid: string;
  canbe: ProductCanbe[];
}
