/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as previewHelpers from '../../../.storybook/preview';
import { initialize as mswInitialize, mswLoader } from 'msw-storybook-addon';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { StoryWrapper } from '../src/lib/utils/storybookUtils';
import { requestHandlers } from '../src/lib/utils/spaceweather-api/fakeApi';

export const decorators = [
  (Story, params) => {
    const { parameters, tags } = params;
    const isDark = tags.indexOf('dark') !== -1;
    const theme = isDark ? darkTheme : lightTheme;

    return (
      <StoryWrapper theme={theme} createApiFunc={parameters.createApiFunc}>
        <Story />
      </StoryWrapper>
    );
  },
];

const parameters = {
  ...previewHelpers.parameters,
  options: {
    storySort: (a, b) => {
      const result = a.title.localeCompare(b.title, undefined, {
        numeric: true,
      });

      if (a.type === 'docs') {
        return -1;
      }

      return a.title === b.title ? 0 : result;
    },
  },
  tags: ['autodocs'],
  msw: {
    handlers: {
      spaceweather: requestHandlers,
    },
  },
};

mswInitialize({
  serviceWorker: {
    url: './mockServiceWorker.js',
  },
});

const loaders = [mswLoader];

export { loaders, parameters };
export default parameters;
