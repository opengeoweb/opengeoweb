/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import swTranslations from '../locales/spaceweather.json';

export { Bulletin } from './lib/components/Bulletin';
export { Layout } from './lib/components/Layout';
export { Notifications } from './lib/components/Notifications';
export {
  NotificationTrigger,
  NotificationTriggerProvider,
  useNotificationTriggerContext,
} from './lib/components/NotificationTrigger';
export { createApi } from './lib/utils/api';
export { createFakeApiFixedDates as createFakeApi } from './lib/utils/fakeApi';
export { TimeSeries } from './lib/components/TimeSeries';
export { SPACEWEATHER_NAMESPACE } from './lib/utils/i18n';
export { swTranslations };

export * from './lib/components/ComponentsLookUp';
