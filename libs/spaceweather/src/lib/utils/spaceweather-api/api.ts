/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Credentials, getConfig } from '@opengeoweb/authentication';
import { getHeaders, handleResponse } from '@opengeoweb/api';
import { Bulletin } from '../../types';

const config = getConfig();

const spaceweatherBaseUrl = config.GW_SW_BASE_URL || '';

// API
export const getBulletinHistoryList = (
  auth: Credentials,
): Promise<Bulletin[]> =>
  fetch(`${spaceweatherBaseUrl}/bulletin/getBulletinHistory`, {
    headers: getHeaders(auth),
  }).then(handleResponse);

export const getBulletin = (auth: Credentials): Promise<Bulletin> =>
  fetch(`${spaceweatherBaseUrl}/bulletin/getBulletin`, {
    headers: getHeaders(auth),
  }).then(handleResponse);
