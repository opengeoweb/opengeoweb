/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import {
  ThemeProviderProps,
  ThemeTypes,
  ThemeWrapper,
  useThemeContext,
} from '@opengeoweb/theme';
import {
  ApiProvider,
  AuthenticationProvider,
  createFakeApiInstance,
} from '@opengeoweb/api';
import { ConfirmationServiceProvider } from '@opengeoweb/shared';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { SWEvent } from '../types';
import {
  createApi as createFakeApi,
  fetchEvent,
  fetchFakeNewNotifications,
} from './fakeApi';
import { fakeBackendError } from './testUtils';
import { SpaceweatherI18nProvider } from '../components/Providers/Providers';
import { MOCK_USERNAME } from './spaceweather-api/fakeApi';

const fakeAxiosInstance = createFakeApiInstance();

interface ExtWrapperProps extends ThemeProviderProps {
  createApiFunc?: () => void;
}

export const StoryWrapper: React.FC<ExtWrapperProps> = ({
  children,
  createApiFunc = null!,
  theme,
}: ExtWrapperProps) => {
  const defaultThemeName =
    theme?.palette.mode === 'dark'
      ? ThemeTypes.DARK_THEME
      : ThemeTypes.LIGHT_THEME;
  const queryClient = new QueryClient({
    defaultOptions: { queries: { retry: false } },
  });

  return (
    <SpaceweatherI18nProvider>
      <ThemeWrapper theme={theme} defaultThemeName={defaultThemeName}>
        <AuthenticationProvider
          value={{
            isLoggedIn: true,
            auth: {
              username: MOCK_USERNAME,
              token: '1223344',
              refresh_token: '33455214',
            },
            onLogin: (): void => null!,
            onSetAuth: (): void => null!,
            sessionStorageProvider: null!,
          }}
        >
          <ApiProvider
            createApi={createApiFunc !== null ? createApiFunc : createFakeApi}
          >
            <QueryClientProvider client={queryClient}>
              <ConfirmationServiceProvider>
                {children}
              </ConfirmationServiceProvider>
            </QueryClientProvider>
          </ApiProvider>
        </AuthenticationProvider>
      </ThemeWrapper>
    </SpaceweatherI18nProvider>
  );
};

export const StoryWrapperWithErrorOnSave: React.FC<ThemeProviderProps> = ({
  children,
  theme,
}) => {
  return (
    <ThemeWrapper theme={theme}>
      <ApiProvider
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        createApi={(): // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any => {
          return {
            // dummy calls
            issueNotification: (): Promise<void> => {
              return Promise.reject(fakeBackendError);
            },
            discardDraftNotification: (): Promise<void> => {
              return Promise.reject(fakeBackendError);
            },
            getNewNotifications: (): Promise<{ data: SWEvent[] }> =>
              fakeAxiosInstance
                .get('/notification/newNotifications')
                .then(() => ({
                  data: fetchFakeNewNotifications(),
                })),
            getEvent: (eventid: string): Promise<{ data: SWEvent }> =>
              fakeAxiosInstance.get(`/event/${eventid}`).then(() => ({
                data: fetchEvent(eventid),
              })),
            getTimeSeriesMultiple: (): void => {
              throw new Error('Dummy error');
            },
          };
        }}
      >
        <ConfirmationServiceProvider>{children}</ConfirmationServiceProvider>
      </ApiProvider>
    </ThemeWrapper>
  );
};

export const ToggleThemeButton: React.FC = () => {
  const { namedTheme, setThemeName } = useThemeContext();
  const toggleTheme = (): void => {
    setThemeName?.(
      namedTheme?.name === ThemeTypes.LIGHT_THEME
        ? ThemeTypes.DARK_THEME
        : ThemeTypes.LIGHT_THEME,
    );
  };
  const isDark = namedTheme?.theme.palette.mode === 'dark';

  return (
    <FormControlLabel
      control={<Switch checked={isDark} onChange={toggleTheme} />}
      label={isDark ? 'Dark theme' : 'Light theme'}
    />
  );
};
