/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { render, waitFor, fireEvent, screen } from '@testing-library/react';
import * as utils from '@opengeoweb/api';
import LifeCycleDisplay, { getActionModeForDraft } from './LifeCycleDisplay';
import { TestWrapper } from '../../utils/testUtils';
import { createApi as createFakeApi } from '../../utils/fakeApi';

describe('src/components/LifeCycleDialog/LifeCycleDisplay', () => {
  beforeEach(() => {
    jest.spyOn(console, 'log').mockImplementation();
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('should display the lifecycle with an external and internal column for an external event', async () => {
    const props = {
      eventId: 'METRB1123454',
      toggleDialogOpen: jest.fn,
    };
    render(
      <TestWrapper createApi={createFakeApi}>
        <LifeCycleDisplay {...props} />
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(
        screen.getByTestId('display-lifecycle-externalcolumn'),
      ).toBeTruthy();
    });
    expect(screen.getByTestId('display-lifecycle-internalcolumn')).toBeTruthy();
    expect(screen.getAllByTestId('lifecycle-column').length).toEqual(2);
    expect(screen.queryByTestId('display-lifecycle-error')).toBeFalsy();
    expect(screen.queryByTestId('display-lifecycle-loading')).toBeFalsy();
  });
  it('should display an error if error returned by api', async () => {
    jest.spyOn(utils, 'useApi').mockReturnValue({
      isLoading: false,
      error: new Error('error'),
      result: null,
    });
    const props = {
      eventId: 'METRB1123454',
      toggleDialogOpen: jest.fn,
    };
    render(
      <TestWrapper>
        <LifeCycleDisplay {...props} />
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(screen.getByTestId('display-lifecycle-error')).toBeTruthy();
    });
    expect(screen.queryByTestId('display-lifecycle-loading')).toBeFalsy();
    expect(
      screen.queryByTestId('display-lifecycle-internalcolumn'),
    ).toBeFalsy();
    expect(
      screen.queryByTestId('display-lifecycle-externalcolumn'),
    ).toBeFalsy();
  });
  it('should display the loading screen while api is still loading', async () => {
    jest.spyOn(utils, 'useApi').mockReturnValue({
      isLoading: true,
      error: null!,
      result: null,
    });
    const props = {
      eventId: 'METRB1123454',
      toggleDialogOpen: jest.fn,
    };
    render(
      <TestWrapper>
        <LifeCycleDisplay {...props} />
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(screen.getByTestId('display-lifecycle-loading')).toBeTruthy();
    });
    expect(screen.queryByTestId('display-lifecycle-error')).toBeFalsy();
    expect(
      screen.queryByTestId('display-lifecycle-internalcolumn'),
    ).toBeFalsy();
    expect(
      screen.queryByTestId('display-lifecycle-externalcolumn'),
    ).toBeFalsy();
  });
  it('should display the action buttons (update/extend and cancelled) for an external WARNING for which internal notifications have already been issued', async () => {
    const props = {
      eventId: 'METRB122',
      toggleDialogOpen: jest.fn,
    };
    render(
      <TestWrapper createApi={createFakeApi}>
        <LifeCycleDisplay {...props} />
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(
        screen.getByTestId('display-lifecycle-externalcolumn'),
      ).toBeTruthy();
    });
    expect(screen.getAllByTestId('lifecycle-column').length).toEqual(2);
    expect(screen.getByTestId('lifecycle-actions-displaybuttons')).toBeTruthy();
    expect(screen.getByTestId('cancel')).toBeTruthy();
    expect(screen.getByTestId('updateextend')).toBeTruthy();
    expect(screen.queryByTestId('summarise')).toBeFalsy();
    expect(screen.queryByTestId('edit-lifecycle')).toBeFalsy();

    // Then, when pressing the cancel button the edit fields should be displayed with the right tag
    fireEvent.click(screen.getByTestId('cancel'));
    await waitFor(() => {
      expect(
        screen.getByTestId('display-lifecycle-externalcolumn'),
      ).toBeTruthy();
    });
    expect(screen.getAllByTestId('lifecycle-column').length).toEqual(2);
    expect(screen.getByTestId('edit-lifecycle')).toBeTruthy();
    expect(
      screen.queryByTestId('lifecycle-actions-displaybuttons'),
    ).toBeFalsy();
    expect(
      screen.getByTestId('lifecycle-display-internaleditaction'),
    ).toBeTruthy();
    expect(
      screen.queryByTestId('lifecycle-display-internaldraftedit'),
    ).toBeFalsy();
    expect(
      screen.queryByTestId('lifecycle-actions-displaybuttons'),
    ).toBeFalsy();
    expect(screen.getByTestId('notification-tag').textContent).toEqual(
      'Cancelled',
    );

    // Check that the title and message have been retrieved
    expect(
      await screen.findByText('Template message text for you'),
    ).toBeTruthy();
    expect(screen.getByTestId('notification-title').getAttribute('value')).toBe(
      'Template title for you',
    );
  });
  it('should display the edit section for an external alert for which no internal notifications have been issued', async () => {
    const props = {
      eventId: 'METRB345',
      toggleDialogOpen: jest.fn,
    };
    render(
      <TestWrapper createApi={createFakeApi}>
        <LifeCycleDisplay {...props} />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(
        screen.getByTestId('display-lifecycle-externalcolumn'),
      ).toBeTruthy();
    });
    expect(screen.getAllByTestId('lifecycle-column').length).toEqual(1);
    expect(screen.getByTestId('edit-lifecycle')).toBeTruthy();
    expect(
      screen.queryByTestId('lifecycle-actions-displaybuttons'),
    ).toBeFalsy();
    expect(
      screen.queryByTestId('lifecycle-display-internaleditaction'),
    ).toBeFalsy();
    expect(
      screen.queryByTestId('lifecycle-display-internaldraftedit'),
    ).toBeFalsy();

    // Check that the title and message have been retrieved
    expect(
      await screen.findByText('Template message text for you'),
    ).toBeTruthy();
    expect(screen.getByTestId('notification-title').getAttribute('value')).toBe(
      'Template title for you',
    );
  });
  it('should display no external column if it is a internally issued event', async () => {
    const props = {
      eventId: 'KNMI002',
      toggleDialogOpen: jest.fn,
    };
    render(
      <TestWrapper createApi={createFakeApi}>
        <LifeCycleDisplay {...props} />
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(
        screen.queryByTestId('display-lifecycle-externalcolumn'),
      ).toBeFalsy();
    });
    await waitFor(() => {
      expect(screen.getAllByTestId('lifecycle-column').length).toEqual(1);
    });
    expect(screen.getByTestId('lifecycle-actions-displaybuttons')).toBeTruthy();
    expect(screen.queryByTestId('summarise')).toBeFalsy();
    expect(screen.getByTestId('updateextend')).toBeTruthy();
    expect(screen.getByTestId('cancel')).toBeTruthy();
    expect(screen.queryByTestId('edit-lifecycle')).toBeFalsy();
  });
  it('should display the edit section for an external alert for which a draft has been saved', async () => {
    const props = {
      eventId: 'METRB1123453453454',
      toggleDialogOpen: jest.fn,
    };
    render(
      <TestWrapper createApi={createFakeApi}>
        <LifeCycleDisplay {...props} />
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(
        screen.getByTestId('display-lifecycle-externalcolumn'),
      ).toBeTruthy();
    });
    expect(screen.getAllByTestId('lifecycle-column').length).toEqual(2);
    expect(screen.getByTestId('edit-lifecycle')).toBeTruthy();
    expect(
      screen.getByTestId('lifecycle-display-internaldraftedit'),
    ).toBeTruthy();
    expect(
      screen.queryByTestId('lifecycle-display-internaleditaction'),
    ).toBeFalsy();
    expect(
      screen.queryByTestId('lifecycle-actions-displaybuttons'),
    ).toBeFalsy();

    // Summary so peak flux fields and end date should be preset
    expect(screen.getByTestId('peakfluxtime-picker')).toBeTruthy();
    expect(screen.getByTestId('end-date-picker')).toBeTruthy();

    // Check that the title and message have been retrieved
    expect(await screen.findByText('Geen impact')).toBeTruthy();
    expect(screen.getByTestId('notification-title').getAttribute('value')).toBe(
      'Proton flux 10 alert summary',
    );
  });

  describe('getActionModeForDraft', () => {
    it('should return actionMode if passed and not none', () => {
      expect(getActionModeForDraft('Updateextend', 'ALERT', 'issued')).toEqual(
        'Updateextend',
      );
    });
    it('should return actionMode if changestate to is not ended', () => {
      expect(getActionModeForDraft('none', 'ALERT', 'issued')).toEqual('none');
      expect(getActionModeForDraft('none', 'WARNING', 'expired')).toEqual(
        'none',
      );
      expect(
        getActionModeForDraft('Updateextend', 'WARNING', 'expired'),
      ).toEqual('Updateextend');
    });
    it('should return Cancel or Summarise if changestateto is ended', () => {
      expect(getActionModeForDraft('none', 'ALERT', 'ended')).toEqual(
        'Summarise',
      );
      expect(getActionModeForDraft('none', 'WARNING', 'ended')).toEqual(
        'Cancel',
      );
    });
  });
});
