/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import {
  render,
  fireEvent,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';

import { errorMessages } from '@opengeoweb/form-fields';

import LifeCycleEditOptions from './LifeCycleEditOptions';
import {
  fakeEventList,
  mockEvent,
  mockEventInternal,
  mockEventWarning,
} from '../../utils/fakedata';
import { constructBaseNotification } from './utils';
import {
  EventCategory,
  EventCategoryDetail,
  SWEvent,
  ThresholdUnits,
} from '../../types';
import FormWrapper from './FormWrapper';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { TestWrapper } from '../../utils/testUtils';

interface WrapperProps {
  baseNotificationData?: SWEvent;
  baseNotificationType: string;
  children: React.ReactNode;
}

const Wrapper: React.FC<WrapperProps> = ({
  baseNotificationData,
  baseNotificationType,
  children,
}: WrapperProps) => {
  return (
    <TestWrapper>
      <FormWrapper
        baseNotificationData={baseNotificationData}
        baseNotificationType={baseNotificationType}
      >
        {children}
      </FormWrapper>
    </TestWrapper>
  );
};

describe('src/components/LifeCycleDialog/LifeCycleEditOptions', () => {
  it('should display the correct date and time provided', () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
      actionMode: 'Summarise',
    };
    render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const startDateInput = within(
      screen.getByTestId('start-date-picker'),
    ).getByRole('textbox');
    expect((startDateInput as HTMLInputElement).value).toEqual(
      dateUtils.dateToString(
        dateUtils.utc(
          mockEventWarning.lifecycles!.internalprovider!.eventstart,
        ),
        dateUtils.DATE_FORMAT_DATEPICKER,
      ),
    );

    const endDateInput = within(
      screen.getByTestId('end-date-picker'),
    ).getByRole('textbox');
    expect((endDateInput as HTMLInputElement).value).toEqual(
      dateUtils.dateToString(
        dateUtils.utc(mockEventWarning.lifecycles!.internalprovider!.eventend),
        dateUtils.DATE_FORMAT_DATEPICKER,
      ),
    );
  });

  it('should give an error message when end date is before start date', async () => {
    const start = '2020-07-01T10:00:00Z';
    const end = dateUtils.dateToString(
      dateUtils.utc('2020-07-01T09:59:00Z'),
      dateUtils.DATE_FORMAT_DATEPICKER,
    );
    mockEventWarning.lifecycles!.internalprovider!.eventstart = start;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
      actionMode: 'Summarise',
    };

    render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const endDatePicker = screen.getByTestId('end-date-picker');

    fireEvent.change(within(endDatePicker).getByRole('textbox'), {
      target: { value: end },
    });

    await screen.findByRole('alert');
    expect(
      screen.getByText(
        translateKeyOutsideComponents('notification-end-time-after-start'),
      ),
    ).toBeTruthy();
  });

  it('should give an error message when end date is equal to start date', async () => {
    const start = '2020-07-01T10:00:00Z';
    const end = dateUtils.dateToString(
      dateUtils.utc(start),
      dateUtils.DATE_FORMAT_DATEPICKER,
    );
    mockEventWarning.lifecycles!.internalprovider!.eventstart = start;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
      actionMode: 'Summarise',
    };

    render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );
    const endDatePicker = screen.getByTestId('end-date-picker');

    fireEvent.change(within(endDatePicker).getByRole('textbox')!, {
      target: { value: end },
    });

    await screen.findByRole('alert');
    expect(
      screen.getByText(
        translateKeyOutsideComponents('notification-end-time-after-start'),
      ),
    ).toBeTruthy();
  });

  it('should not give an error message when end date is valid', () => {
    const start = '2020-07-01T10:00:00Z';
    const end = dateUtils.dateToString(
      dateUtils.add(dateUtils.utc(), { hours: 1 }),
    );
    mockEventWarning.lifecycles!.internalprovider!.eventstart = start;
    mockEventWarning.lifecycles!.internalprovider!.eventend = end;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
      actionMode: 'Summarise',
    };
    render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.queryByRole('alert')).toBeFalsy();
  });

  it('should give an error message when end date is in the past and its not allowed', async () => {
    const start = '2020-07-01T10:00:00Z';

    const end = dateUtils.dateToString(
      dateUtils.utc('2020-07-01T11:00:00Z'),
      dateUtils.DATE_FORMAT_DATEPICKER,
    );
    mockEventWarning.lifecycles!.internalprovider!.eventstart = start;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };

    render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const endDatePicker = screen.getByTestId('end-date-picker');

    fireEvent.change(within(endDatePicker).getByRole('textbox'), {
      target: { value: end },
    });

    await screen.findByRole('alert');
    expect(
      screen.getByText(
        translateKeyOutsideComponents('notification-end-time-future'),
      ),
    ).toBeTruthy();
  });

  it('should give an error message when end date is current datetime and end date in the past is not allowed', async () => {
    const start = '2020-07-01T10:00:00Z';
    const end = dateUtils.dateToString(dateUtils.utc());
    mockEventWarning.lifecycles!.internalprovider!.eventstart = start;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };

    render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const endDatePicker = screen.getByTestId('end-date-picker');

    fireEvent.change(within(endDatePicker).getByRole('textbox'), {
      target: { value: end },
    });

    await screen.findByRole('alert');
  });

  it('should not display end date when label is Alert and not summarising', async () => {
    const start = '2020-07-01T10:00:00Z';
    mockEvent.lifecycles!.internalprovider!.eventstart = start;
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };

    render(
      <Wrapper
        baseNotificationData={mockEvent}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.queryByTestId('end-date-picker')).toBeFalsy();
  });

  it('should give an error message when end date is empty and label is Warning', async () => {
    const start = '2020-07-01T10:00:00Z';
    mockEventWarning.lifecycles!.internalprovider!.eventstart = start;
    const props = {
      statusTagContent: 'Warning',
      eventTypeDisabled: false,
    };

    render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const endDatePicker = screen.getByTestId('end-date-picker');
    fireEvent.change(within(endDatePicker).getByRole('textbox'), {
      target: { value: '' },
    });

    await screen.findByRole('alert');

    // Expect end date to be removed when changing to label = alert
    fireEvent.mouseDown(screen.getByTestId('label-select'));
    const menuItem = await screen.findByText('Alert');
    fireEvent.click(menuItem);
    await waitFor(() =>
      expect(screen.queryByTestId('end-date-picker')).toBeFalsy(),
    );
  });

  it('should not give an error message when end date is in the past and it is allowed', async () => {
    const start = dateUtils.dateToString(
      dateUtils.sub(dateUtils.utc(), { days: 4 }),
    )!;

    const end = dateUtils.dateToString(
      dateUtils.sub(dateUtils.utc(), { days: 1 }),
      dateUtils.DATE_FORMAT_DATEPICKER,
    )!;

    mockEventWarning.lifecycles!.internalprovider!.eventstart = start;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
      actionMode: 'Summarise',
    };
    render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const endDatePicker = screen.getByTestId('end-date-picker');
    fireEvent.change(within(endDatePicker).getByRole('textbox'), {
      target: { value: end },
    });

    expect(screen.queryByRole('alert')).toBeFalsy();
  });

  it('should give an error message when start date is empty', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };

    render(
      <Wrapper
        baseNotificationData={mockEvent}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const startDatePicker = screen.getByTestId('start-date-picker');

    fireEvent.change(within(startDatePicker).getByRole('textbox'), {
      target: { value: '' },
    });
    await screen.findByRole('alert');
    expect(
      screen.getByText(translateKeyOutsideComponents(errorMessages.required)),
    ).toBeTruthy();
  });
  it('should give an error message when start date is more than 1 week in the past', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };

    render(
      <Wrapper
        baseNotificationData={mockEvent}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const startDatePicker = screen.getByTestId('start-date-picker');

    const newStartDate = dateUtils.dateToString(
      dateUtils.sub(dateUtils.utc(), { days: 7, hours: 1 }),
      dateUtils.DATE_FORMAT_DATEPICKER,
    );

    fireEvent.change(within(startDatePicker).getByRole('textbox'), {
      target: { value: newStartDate },
    });

    await screen.findByRole('alert');
    expect(
      screen.getByText(
        translateKeyOutsideComponents('notification-recent-date'),
      ),
    ).toBeTruthy();
  });

  it('should update the tag when changing the label', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={mockEvent}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );
    expect(screen.getByTestId('notification-tag').textContent).toEqual('Alert');
    fireEvent.mouseDown(screen.getByTestId('label-select'));
    const menuItem = await screen.findByText('Warning');
    fireEvent.click(menuItem);
    await waitFor(() =>
      expect(screen.getByTestId('notification-tag').textContent).toEqual(
        'Warning',
      ),
    );
  });

  it('should be possible to select a different category and should automatically update the event level options and label', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={constructBaseNotification(null, 'new') as SWEvent}
        baseNotificationType="new"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );
    expect(screen.getByTestId('category-select').textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );
    expect(screen.getByTestId('statusTag').textContent).toEqual('Alert');
    fireEvent.mouseDown(screen.getByTestId('eventlevel-select'));
    const menuItemLevelR = await screen.findByText('R1');
    fireEvent.click(menuItemLevelR);
    await waitFor(() => {
      expect(screen.getByTestId('eventlevel-select').textContent).toEqual('R1');
    });

    fireEvent.mouseDown(screen.getByTestId('category-select'));
    const menuItemCategory = await screen.findByText(EventCategory.GEOMAGNETIC);
    fireEvent.click(menuItemCategory);
    await waitFor(() => {
      expect(screen.getByTestId('category-select').textContent).toEqual(
        EventCategory.GEOMAGNETIC,
      );
    });
    expect(screen.getByTestId('statusTag').textContent).toEqual('Watch');
    fireEvent.mouseDown(screen.getByTestId('categorydetail-select'));
    const menuItemCategoryDetail = await screen.findByText(
      EventCategoryDetail.KP_INDEX,
    );
    fireEvent.click(menuItemCategoryDetail);
    await waitFor(() => {
      expect(screen.getByTestId('categorydetail-select').textContent).toEqual(
        EventCategoryDetail.KP_INDEX,
      );
    });
    expect(screen.getByTestId('statusTag').textContent).toEqual('Alert');
    fireEvent.mouseDown(screen.getByTestId('eventlevel-select'));
    const menuItemLevelG = await screen.findByText('G1');
    fireEvent.click(menuItemLevelG);
    await waitFor(() => {
      expect(screen.getByTestId('eventlevel-select').textContent).toEqual('G1');
    });
  });

  it('should hide category, categorydetail and label input for user when eventType is disabled', async () => {
    const start = '2020-07-01T10:00:00Z';
    const end = '2020-07-01T11:00:00Z';
    mockEvent.lifecycles!.internalprovider!.eventstart = start;
    mockEvent.lifecycles!.internalprovider!.eventend = end;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: true,
    };
    render(
      <Wrapper
        baseNotificationData={mockEvent}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const categoryHiddenInput = screen.getByTestId('category');
    expect(categoryHiddenInput).toBeTruthy();
    expect(categoryHiddenInput.getAttribute('value')).toEqual(
      mockEvent.category,
    );
    const categorydetailHiddenInput = screen.getByTestId('categorydetail');
    expect(categorydetailHiddenInput).toBeTruthy();
    expect(categorydetailHiddenInput.getAttribute('value')).toEqual(
      mockEvent.categorydetail,
    );
    const labelHiddenInput = screen.getByTestId('label');
    expect(labelHiddenInput).toBeTruthy();
    expect(labelHiddenInput.getAttribute('value')).toEqual(
      mockEvent.lifecycles!.internalprovider!.label,
    );
    expect(screen.queryByTestId('category-select')).toBeFalsy();
    expect(screen.queryByTestId('categorydetail-select')).toBeFalsy();
    expect(screen.queryByTestId('label-select')).toBeFalsy();
    expect(screen.getByTestId('statusTag')).toBeTruthy();
    expect(screen.getByTestId('eventlevel-select')).toBeTruthy();
    expect(screen.getByTestId('threshold-select')).toBeTruthy();
    expect(screen.getByTestId('start-date-picker')).toBeTruthy();
    expect(screen.queryByTestId('end-date-picker')).toBeFalsy();
  });

  it('should be possible to select an event level and should automatically update threshold for KP_INDEX', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={mockEvent}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );
    expect(screen.getByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.KP_INDEX,
    );
    expect(screen.getByTestId('eventlevel-select').textContent).toEqual('G1');
    expect(screen.getByTestId('threshold-select').textContent).toEqual('5');
    fireEvent.mouseDown(screen.getByTestId('eventlevel-select'));
    const menuItem = await screen.findByText('G2');
    fireEvent.click(menuItem);
    await waitFor(() => {
      expect(screen.getByTestId('eventlevel-select').textContent).toEqual('G2');
    });
    expect(screen.getByTestId('threshold-select').textContent).toEqual('6');
  });

  it('should be possible to select a threshold and should automatically update eventlevel for KP_INDEX', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={mockEvent}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );
    expect(screen.getByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.KP_INDEX,
    );
    expect(screen.getByTestId('eventlevel-select').textContent).toEqual('G1');
    expect(screen.getByTestId('threshold-select').textContent).toEqual('5');
    fireEvent.mouseDown(screen.getByTestId('threshold-select'));
    const menuItem = await screen.findByText('7');
    fireEvent.click(menuItem);
    await waitFor(() => {
      expect(screen.getByTestId('threshold-select').textContent).toEqual('7');
    });
    expect(screen.getByTestId('eventlevel-select').textContent).toEqual('G3');
  });

  it('should be possible to change the startdate', async () => {
    const start = '2020-07-01T10:00:00Z';
    mockEvent.lifecycles!.internalprovider!.eventstart = start;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={mockEvent}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const startDateInput = within(
      screen.getByTestId('start-date-picker'),
    ).getByRole('textbox');
    expect((startDateInput as HTMLInputElement).value).toEqual(
      dateUtils.dateToString(
        dateUtils.utc(start),
        dateUtils.DATE_FORMAT_DATEPICKER,
      ),
    );
    const newDate = '28/06/2020 09:00';
    fireEvent.change(startDateInput, {
      target: { value: newDate },
    });
    await waitFor(() =>
      expect((startDateInput as HTMLInputElement).value).toEqual(newDate),
    );
  });

  it('should be possible to change the enddate', async () => {
    const start = '2020-07-01T10:00:00Z';
    const end = '2020-07-01T11:00:00Z';
    mockEventWarning.lifecycles!.internalprovider!.eventstart = start;
    mockEventWarning.lifecycles!.internalprovider!.eventend = end;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };

    render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const endDateInput = within(
      screen.getByTestId('end-date-picker'),
    ).getByRole('textbox');
    expect((endDateInput as HTMLInputElement).value).toEqual(
      dateUtils.dateToString(
        dateUtils.utc(end),
        dateUtils.DATE_FORMAT_DATEPICKER,
      ),
    );
    const newDate = '28/07/2020 09:00';
    fireEvent.change(endDateInput, {
      target: { value: newDate },
    });

    await waitFor(() =>
      expect((endDateInput as HTMLInputElement).value).toEqual(newDate),
    );
  });

  it('should hide category, categorydetail and label fields when summarizing', async () => {
    const props = {
      statusTagContent: 'Summary',
      eventTypeDisabled: true,

      actionMode: 'Summarise',
    };
    render(
      <Wrapper
        baseNotificationData={mockEventInternal}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    // Hidden inputs should be present
    expect(screen.getByTestId('category')).toBeTruthy();
    expect(screen.getByTestId('categorydetail')).toBeTruthy();
    expect(screen.getByTestId('label')).toBeTruthy();

    // But visible inputs should not be there
    expect(screen.queryByTestId('category-select')).toBeFalsy();
    expect(screen.queryByTestId('categorydetail-select')).toBeFalsy();
    expect(screen.queryByTestId('label-select')).toBeFalsy();

    // Visible inputs for other fields should be there
    expect(screen.getByTestId('threshold-select')).toBeTruthy();
    expect(screen.getByTestId('thresholdunit-input')).toBeTruthy();
    expect(screen.getByTestId('eventlevel-select')).toBeTruthy();
    expect(screen.getByTestId('datasource-input')).toBeTruthy();
  });

  it('should hide category, categorydetail and label fields when cancelling', async () => {
    const props = {
      statusTagContent: 'Cancelled',
      eventTypeDisabled: true,

      actionMode: 'Cancel',
    };
    render(
      <Wrapper
        baseNotificationData={mockEventInternal}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    // Hidden inputs should be present
    expect(screen.getByTestId('category')).toBeTruthy();
    expect(screen.getByTestId('categorydetail')).toBeTruthy();
    expect(screen.getByTestId('label')).toBeTruthy();

    // But visible inputs should not be there
    expect(screen.queryByTestId('category-select')).toBeFalsy();
    expect(screen.queryByTestId('categorydetail-select')).toBeFalsy();
    expect(screen.queryByTestId('label-select')).toBeFalsy();

    // Visible inputs for other fields should be there
    expect(screen.getByTestId('threshold-select')).toBeTruthy();
    expect(screen.getByTestId('thresholdunit-input')).toBeTruthy();
    expect(screen.getByTestId('eventlevel-select')).toBeTruthy();
    expect(screen.getByTestId('datasource-input')).toBeTruthy();
  });

  it('should give an error message when end date has an invalid format', async () => {
    const start = '2020-07-01T10:00:00Z';
    const end = '2020-07-0233 11:00';
    mockEventWarning.lifecycles!.internalprovider!.eventstart = start;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };

    render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const endDatePicker = screen.getByTestId('end-date-picker');

    fireEvent.change(within(endDatePicker).getByRole('textbox'), {
      target: { value: end },
    });

    await screen.findByRole('alert');
  });

  it('should update the categorydetail, label and statustag when changing category', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={constructBaseNotification(null, 'new') as SWEvent}
        baseNotificationType="new"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );
    // New notification opens with default values for XRAY_RADIO_BLACKOUT
    expect(screen.getByTestId('category-select').textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );
    // Category detail should be empty, disabled and invisible for category XRAY_RADIO_BLACKOUT
    expect(screen.getByTestId('categorydetail-select').textContent).toMatch('');
    expect(screen.getByTestId('categorydetail-select').classList).toContain(
      'Mui-disabled',
    );
    const style = getComputedStyle(
      // eslint-disable-next-line testing-library/no-node-access
      screen.getByTestId('categorydetail-select').parentElement!.parentElement!,
    );
    expect(style.opacity).toEqual('0');
    expect(screen.getByTestId('label-select').textContent).toEqual('Alert');
    expect(screen.getByTestId('statusTag').textContent).toEqual('Alert');

    // Change category to GEOMAGNETIC
    fireEvent.mouseDown(screen.getByTestId('category-select'));
    const geomagnetic = await screen.findByText(EventCategory.GEOMAGNETIC);
    fireEvent.click(geomagnetic);
    await waitFor(() => {
      expect(screen.getByTestId('category-select').textContent).toEqual(
        EventCategory.GEOMAGNETIC,
      );
    });
    // Categorydetail, label and statustag should be updated
    expect(screen.getByTestId('categorydetail-select')).toBeTruthy();
    expect(screen.getByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.GEOMAGNETIC_STORM,
    );
    expect(screen.getByTestId('label-select').textContent).toEqual('Watch');
    expect(screen.getByTestId('statusTag').textContent).toEqual('Watch');
  });
  it('should update the label and statustag when changing categorydetail', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={constructBaseNotification(null, 'new') as SWEvent}
        baseNotificationType="new"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    // Change category to GEOMAGNETIC so we have a categorydetail field
    fireEvent.mouseDown(screen.getByTestId('category-select'));
    const geomagnetic = await screen.findByText(EventCategory.GEOMAGNETIC);
    fireEvent.click(geomagnetic);
    await waitFor(() => {
      expect(screen.getByTestId('category-select').textContent).toEqual(
        EventCategory.GEOMAGNETIC,
      );
    });
    // Categorydetail, label and statustag should be updated
    expect(screen.getByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.GEOMAGNETIC_STORM,
    );
    expect(screen.getByTestId('label-select').textContent).toEqual('Watch');
    expect(screen.getByTestId('statusTag').textContent).toEqual('Watch');
    // change categorydetail to KP_INDEX
    fireEvent.mouseDown(screen.getByTestId('categorydetail-select'));
    const kpindex = await screen.findByText(EventCategoryDetail.KP_INDEX);
    fireEvent.click(kpindex);
    await waitFor(() => {
      expect(screen.getByTestId('categorydetail-select').textContent).toEqual(
        EventCategoryDetail.KP_INDEX,
      );
    });
    // label and statustag should be updated
    expect(screen.getByTestId('label-select').textContent).toEqual('Alert');
    expect(screen.getByTestId('statusTag').textContent).toEqual('Alert');
  });
  it('should update threshold and xrayclass when changing eventlevel for XRAY_RADIO_BLACKOUT', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={constructBaseNotification(null, 'new') as SWEvent}
        baseNotificationType="new"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.getByTestId('category-select').textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );
    // check eventlevel, threshold, xrayclass values
    expect(screen.getByTestId('eventlevel-select').textContent).toMatch('');
    expect(screen.getByTestId('threshold-select').textContent).toMatch('');
    expect(screen.getByTestId('xrayclass-select').textContent).toMatch('');

    // change eventlevel
    fireEvent.mouseDown(screen.getByTestId('eventlevel-select'));
    const newlevel = await screen.findByText('R2');
    fireEvent.click(newlevel);
    await waitFor(() =>
      expect(screen.getByTestId('eventlevel-select').textContent).toEqual('R2'),
    );

    // threshold and xrayclass should be updated
    expect(screen.getByTestId('threshold-select').textContent).toEqual(
      '0.00005',
    );
    expect(screen.getByTestId('xrayclass-select').textContent).toEqual('M5');
  });
  it('should update eventlevel and xrayclass when changing threshold for XRAY_RADIO_BLACKOUT', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={constructBaseNotification(null, 'new') as SWEvent}
        baseNotificationType="new"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.getByTestId('category-select').textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );
    // check eventlevel, threshold, xrayclass values
    expect(screen.getByTestId('eventlevel-select').textContent).toMatch('');
    expect(screen.getByTestId('threshold-select').textContent).toMatch('');
    expect(screen.getByTestId('xrayclass-select').textContent).toMatch('');

    // change threshold
    fireEvent.mouseDown(screen.getByTestId('threshold-select'));
    const newlevel = await screen.findByText('0.00005');
    fireEvent.click(newlevel);
    await waitFor(() =>
      expect(screen.getByTestId('threshold-select').textContent).toEqual(
        '0.00005',
      ),
    );

    // eventlevel and xrayclass should be updated
    expect(screen.getByTestId('eventlevel-select').textContent).toEqual('R2');
    expect(screen.getByTestId('xrayclass-select').textContent).toEqual('M5');
  });
  it('should update eventlevel and threshold when changing xrayclass for XRAY_RADIO_BLACKOUT', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={constructBaseNotification(null, 'new') as SWEvent}
        baseNotificationType="new"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.getByTestId('category-select').textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );
    // check eventlevel, threshold, xrayclass values
    expect(screen.getByTestId('eventlevel-select').textContent).toMatch('');
    expect(screen.getByTestId('threshold-select').textContent).toMatch('');
    expect(screen.getByTestId('xrayclass-select').textContent).toMatch('');

    // change xrayclass
    fireEvent.mouseDown(screen.getByTestId('xrayclass-select'));
    const newlevel = await screen.findByText('M5');
    fireEvent.click(newlevel);
    await waitFor(() =>
      expect(screen.getByTestId('xrayclass-select').textContent).toEqual('M5'),
    );

    // eventlevel and threshold should be updated
    expect(screen.getByTestId('eventlevel-select').textContent).toEqual('R2');
    expect(screen.getByTestId('threshold-select').textContent).toEqual(
      '0.00005',
    );
  });
  it('should show xray class, peak class, peak flux, peak flux time and end time when summarising XRAY_RADIO_BLACKOUT', () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,

      actionMode: 'Summarise',
    };
    render(
      <Wrapper
        baseNotificationData={mockEventInternal}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.queryByTestId('category-select')!.textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );
    expect(screen.getByTestId('xrayclass-select')).toBeTruthy();
    expect(screen.getByTestId('peakclass-input')).toBeTruthy();
    expect(screen.getByTestId('peakflux-input')).toBeTruthy();
    expect(screen.getByTestId('peakfluxtime-picker')).toBeTruthy();
    expect(screen.getByTestId('end-date-picker')).toBeTruthy();
  });
  it('should show peak flux, peak flux time and end time when summarising Electron Flux', () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
      actionMode: 'Summarise',
    };
    render(
      <Wrapper
        baseNotificationData={fakeEventList[17]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.queryByTestId('category-select')!.textContent).toEqual(
      EventCategory.ELECTRON_FLUX,
    );
    expect(screen.queryByTestId('categorydetail-select')!.textContent).toEqual(
      EventCategoryDetail.ELECTRON_FLUX_2,
    );
    expect(screen.queryByTestId('xrayclass-select')).toBeFalsy();
    expect(screen.queryByTestId('peakclass-input')).toBeFalsy();
    expect(screen.getByTestId('peakflux-input')).toBeTruthy();
    expect(screen.getByTestId('peakfluxtime-picker')).toBeTruthy();
    expect(screen.getByTestId('end-date-picker')).toBeTruthy();
  });
  it('should show correct fields for Geomagnetic Storm Watch', () => {
    const props = {
      statusTagContent: 'Watch',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={fakeEventList[14]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.queryByTestId('category-select')!.textContent).toEqual(
      EventCategory.GEOMAGNETIC,
    );
    expect(screen.queryByTestId('categorydetail-select')!.textContent).toEqual(
      EventCategoryDetail.GEOMAGNETIC_STORM,
    );
    expect(screen.getByTestId('initialgscale-input')).toBeTruthy();
    expect(screen.getByTestId('start-date-picker')).toBeTruthy();
    expect(screen.getByTestId('end-date-picker')).toBeTruthy();
    expect(screen.queryByTestId('statusTag')!.textContent).toEqual('Watch');

    expect(screen.queryByTestId('shocktime-picker')).toBeFalsy();
    expect(screen.queryByTestId('observedpolaritybz-input')).toBeFalsy();
    expect(screen.queryByTestId('observedsolarwind-input')).toBeFalsy();
    expect(screen.queryByTestId('impulsetime-picker')).toBeFalsy();
    expect(screen.queryByTestId('magnetometerdeflection-input')).toBeFalsy();
    expect(screen.queryByTestId('eventlevel-select')).toBeFalsy();
    expect(screen.queryByTestId('datasource-input')).toBeFalsy();
    expect(screen.queryByTestId('threshold-select')).toBeFalsy();
    expect(screen.queryByTestId('thresholdunit-input')).toBeFalsy();
    expect(screen.queryByTestId('xrayclass-select')).toBeFalsy();
  });
  it('should show correct fields for Geomagnetic Sudden Impuls Alert', () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={fakeEventList[1]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.queryByTestId('category-select')!.textContent).toEqual(
      EventCategory.GEOMAGNETIC,
    );
    expect(screen.queryByTestId('categorydetail-select')!.textContent).toEqual(
      EventCategoryDetail.GEOMAGNETIC_SUDDEN_IMPULSE,
    );
    expect(screen.queryByTestId('statusTag')!.textContent).toEqual('Alert');
    expect(screen.getByTestId('datasource-input')).toBeTruthy();
    expect(screen.getByTestId('start-date-picker')).toBeTruthy();
    expect(screen.getByTestId('impulsetime-picker')).toBeTruthy();
    expect(screen.getByTestId('magnetometerdeflection-input')).toBeTruthy();

    expect(screen.queryByTestId('shocktime-picker')).toBeFalsy();
    expect(screen.queryByTestId('observedpolaritybz-input')).toBeFalsy();
    expect(screen.queryByTestId('observedsolarwind-input')).toBeFalsy();
    expect(screen.queryByTestId('initialgscale-input')).toBeFalsy();
    expect(screen.queryByTestId('end-date-picker')).toBeFalsy();
    expect(screen.queryByTestId('eventlevel-select')).toBeFalsy();
    expect(screen.queryByTestId('threshold-select')).toBeFalsy();
    expect(screen.queryByTestId('thresholdunit-input')).toBeFalsy();
    expect(screen.queryByTestId('xrayclass-select')).toBeFalsy();
  });
  it('should show correct fields for Geomagnetic Sudden Impuls Warning', () => {
    const props = {
      statusTagContent: 'Warning',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={fakeEventList[8]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.queryByTestId('category-select')!.textContent).toEqual(
      EventCategory.GEOMAGNETIC,
    );
    expect(screen.queryByTestId('categorydetail-select')!.textContent).toEqual(
      EventCategoryDetail.GEOMAGNETIC_SUDDEN_IMPULSE,
    );
    expect(screen.queryByTestId('statusTag')!.textContent).toEqual('Warning');
    expect(screen.getByTestId('datasource-input')).toBeTruthy();
    expect(screen.getByTestId('start-date-picker')).toBeTruthy();
    expect(screen.getByTestId('end-date-picker')).toBeTruthy();
    expect(screen.getByTestId('shocktime-picker')).toBeTruthy();
    expect(screen.getByTestId('observedpolaritybz-input')).toBeTruthy();
    expect(screen.getByTestId('observedsolarwind-input')).toBeTruthy();

    expect(screen.queryByTestId('impulsetime-picker')).toBeFalsy();
    expect(screen.queryByTestId('magnetometerdeflection-input')).toBeFalsy();
    expect(screen.queryByTestId('initialgscale-input')).toBeFalsy();
    expect(screen.queryByTestId('eventlevel-select')).toBeFalsy();
    expect(screen.queryByTestId('threshold-select')).toBeFalsy();
    expect(screen.queryByTestId('thresholdunit-input')).toBeFalsy();
    expect(screen.queryByTestId('xrayclass-select')).toBeFalsy();
  });
  it('should show correct fields for Geomagnetic KP Index Alert', () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={fakeEventList[11]}
        baseNotificationType="externalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.queryByTestId('category-select')!.textContent).toEqual(
      EventCategory.GEOMAGNETIC,
    );
    expect(screen.queryByTestId('categorydetail-select')!.textContent).toEqual(
      EventCategoryDetail.KP_INDEX,
    );
    expect(screen.queryByTestId('statusTag')!.textContent).toEqual('Alert');
    expect(screen.getByTestId('start-date-picker')).toBeTruthy();
    expect(screen.getByTestId('eventlevel-select')).toBeTruthy();
    expect(screen.getByTestId('datasource-input')).toBeTruthy();
    expect(screen.getByTestId('threshold-select')).toBeTruthy();

    expect(screen.queryByTestId('thresholdunit-input')).toBeFalsy();
    expect(screen.queryByTestId('shocktime-picker')).toBeFalsy();
    expect(screen.queryByTestId('observedpolaritybz-input')).toBeFalsy();
    expect(screen.queryByTestId('observedsolarwind-input')).toBeFalsy();
    expect(screen.queryByTestId('end-date-picker')).toBeFalsy();
    expect(screen.queryByTestId('impulsetime-picker')).toBeFalsy();
    expect(screen.queryByTestId('magnetometerdeflection-input')).toBeFalsy();
    expect(screen.queryByTestId('initialgscale-input')).toBeFalsy();
    expect(screen.queryByTestId('xrayclass-select')).toBeFalsy();
  });
  it('should show correct fields for Proton Flux 100', () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={fakeEventList[15]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.queryByTestId('category-select')!.textContent).toEqual(
      EventCategory.PROTON_FLUX,
    );
    expect(screen.queryByTestId('categorydetail-select')!.textContent).toEqual(
      EventCategoryDetail.PROTON_FLUX_100,
    );
    expect(screen.queryByTestId('statusTag')!.textContent).toEqual('Alert');
    expect(screen.getByTestId('start-date-picker')).toBeTruthy();
    expect(screen.getByTestId('datasource-input')).toBeTruthy();
    expect(screen.getByTestId('threshold-select')).toBeTruthy();
    expect(screen.getByTestId('thresholdunit-input')).toBeTruthy();

    expect(screen.queryByTestId('eventlevel-select')).toBeFalsy();
    expect(screen.queryByTestId('shocktime-picker')).toBeFalsy();
    expect(screen.queryByTestId('observedpolaritybz-input')).toBeFalsy();
    expect(screen.queryByTestId('observedsolarwind-input')).toBeFalsy();
    expect(screen.queryByTestId('end-date-picker')).toBeFalsy();
    expect(screen.queryByTestId('impulsetime-picker')).toBeFalsy();
    expect(screen.queryByTestId('magnetometerdeflection-input')).toBeFalsy();
    expect(screen.queryByTestId('initialgscale-input')).toBeFalsy();
    expect(screen.queryByTestId('xrayclass-select')).toBeFalsy();
  });
  it('should show correct fields for Proton Flux 10', () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={fakeEventList[16]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.queryByTestId('category-select')!.textContent).toEqual(
      EventCategory.PROTON_FLUX,
    );
    expect(screen.queryByTestId('categorydetail-select')!.textContent).toEqual(
      EventCategoryDetail.PROTON_FLUX_10,
    );
    expect(screen.queryByTestId('statusTag')!.textContent).toEqual('Alert');
    expect(screen.getByTestId('start-date-picker')).toBeTruthy();
    expect(screen.getByTestId('datasource-input')).toBeTruthy();
    expect(screen.getByTestId('eventlevel-select')).toBeTruthy();
    expect(screen.getByTestId('threshold-select')).toBeTruthy();
    expect(screen.getByTestId('thresholdunit-input')).toBeTruthy();

    expect(screen.queryByTestId('shocktime-picker')).toBeFalsy();
    expect(screen.queryByTestId('observedpolaritybz-input')).toBeFalsy();
    expect(screen.queryByTestId('observedsolarwind-input')).toBeFalsy();
    expect(screen.queryByTestId('end-date-picker')).toBeFalsy();
    expect(screen.queryByTestId('impulsetime-picker')).toBeFalsy();
    expect(screen.queryByTestId('magnetometerdeflection-input')).toBeFalsy();
    expect(screen.queryByTestId('initialgscale-input')).toBeFalsy();
    expect(screen.queryByTestId('xrayclass-select')).toBeFalsy();
  });
  it('should update threshold when changing eventlevel for Proton Flux 10', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={fakeEventList[16]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.queryByTestId('category-select')!.textContent).toEqual(
      EventCategory.PROTON_FLUX,
    );
    expect(screen.queryByTestId('categorydetail-select')!.textContent).toEqual(
      EventCategoryDetail.PROTON_FLUX_10,
    );
    expect(screen.getByTestId('eventlevel-select')).toBeTruthy();
    expect(screen.getByTestId('threshold-select')).toBeTruthy();

    // check eventlevel and threshold values
    expect(screen.getByTestId('eventlevel-select').textContent).toMatch('');
    expect(screen.getByTestId('threshold-select').textContent).toMatch('');

    // change eventlevel
    fireEvent.mouseDown(screen.getByTestId('eventlevel-select'));
    const newlevel = await screen.findByText('S2');
    fireEvent.click(newlevel);
    await waitFor(() =>
      expect(screen.getByTestId('eventlevel-select').textContent).toEqual('S2'),
    );

    // threshold should be updated
    expect(screen.getByTestId('threshold-select').textContent).toEqual('100');
  });

  it('should show correct fields for Electron Flux 2', () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={fakeEventList[17]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.queryByTestId('category-select')!.textContent).toEqual(
      EventCategory.ELECTRON_FLUX,
    );
    expect(screen.queryByTestId('categorydetail-select')!.textContent).toEqual(
      EventCategoryDetail.ELECTRON_FLUX_2,
    );
    expect(screen.queryByTestId('statusTag')!.textContent).toEqual('Alert');
    expect(screen.getByTestId('start-date-picker')).toBeTruthy();
    expect(screen.getByTestId('datasource-input')).toBeTruthy();
    expect(screen.getByTestId('threshold-input')).toBeTruthy();
    expect(screen.getByTestId('thresholdunit-input')).toBeTruthy();

    expect(screen.queryByTestId('eventlevel-select')).toBeFalsy();
    expect(screen.queryByTestId('threshold-select')).toBeFalsy();
    expect(screen.queryByTestId('shocktime-picker')).toBeFalsy();
    expect(screen.queryByTestId('observedpolaritybz-input')).toBeFalsy();
    expect(screen.queryByTestId('observedsolarwind-input')).toBeFalsy();
    expect(screen.queryByTestId('end-date-picker')).toBeFalsy();
    expect(screen.queryByTestId('impulsetime-picker')).toBeFalsy();
    expect(screen.queryByTestId('magnetometerdeflection-input')).toBeFalsy();
    expect(screen.queryByTestId('initialgscale-input')).toBeFalsy();
    expect(screen.queryByTestId('xrayclass-select')).toBeFalsy();
  });
  it('should show correct fields for Electron Fluence', () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={fakeEventList[9]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.queryByTestId('category-select')!.textContent).toEqual(
      EventCategory.ELECTRON_FLUX,
    );
    expect(screen.queryByTestId('categorydetail-select')!.textContent).toEqual(
      EventCategoryDetail.ELECTRON_FLUENCE,
    );
    expect(screen.queryByTestId('statusTag')!.textContent).toEqual('Alert');
    expect(screen.getByTestId('start-date-picker')).toBeTruthy();
    expect(screen.getByTestId('end-date-picker')).toBeTruthy();
    expect(screen.getByTestId('datasource-input')).toBeTruthy();
    expect(screen.getByTestId('threshold-input')).toBeTruthy();
    expect(screen.getByTestId('thresholdunit-input')).toBeTruthy();

    expect(screen.queryByTestId('eventlevel-select')).toBeFalsy();
    expect(screen.queryByTestId('threshold-select')).toBeFalsy();
    expect(screen.queryByTestId('shocktime-picker')).toBeFalsy();
    expect(screen.queryByTestId('observedpolaritybz-input')).toBeFalsy();
    expect(screen.queryByTestId('observedsolarwind-input')).toBeFalsy();
    expect(screen.queryByTestId('impulsetime-picker')).toBeFalsy();
    expect(screen.queryByTestId('magnetometerdeflection-input')).toBeFalsy();
    expect(screen.queryByTestId('initialgscale-input')).toBeFalsy();
    expect(screen.queryByTestId('xrayclass-select')).toBeFalsy();
  });
  it('should update the threshold unit when switching category detail for ELECTRON_FLUX', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={fakeEventList[17]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.queryByTestId('category-select')!.textContent).toEqual(
      EventCategory.ELECTRON_FLUX,
    );
    expect(screen.queryByTestId('categorydetail-select')!.textContent).toEqual(
      EventCategoryDetail.ELECTRON_FLUX_2,
    );
    expect(
      screen.queryByTestId('thresholdunit-input')!.getAttribute('value'),
    ).toEqual(ThresholdUnits['ELECTRON_FLUX_2']);
    fireEvent.mouseDown(screen.getByTestId('categorydetail-select'));
    const menuItemCategoryDetail = await screen.findByText(
      EventCategoryDetail.ELECTRON_FLUENCE,
    );
    fireEvent.click(menuItemCategoryDetail);
    await waitFor(() =>
      expect(
        screen.queryByTestId('thresholdunit-input')!.getAttribute('value'),
      ).toEqual(ThresholdUnits['ELECTRON_FLUENCE']),
    );
    fireEvent.mouseDown(screen.getByTestId('categorydetail-select'));
    const menuItemCategoryDetail2 = await screen.findByText(
      EventCategoryDetail.ELECTRON_FLUX_2,
    );
    fireEvent.click(menuItemCategoryDetail2);
    await waitFor(() =>
      expect(
        screen.queryByTestId('thresholdunit-input')!.getAttribute('value'),
      ).toEqual(ThresholdUnits['ELECTRON_FLUX_2']),
    );
  });
  it('should update the threshold unit when switching category', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={constructBaseNotification(null, 'new') as SWEvent}
        baseNotificationType="new"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.queryByTestId('category-select')!.textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );
    expect(
      screen.queryByTestId('thresholdunit-input')!.getAttribute('value'),
    ).toEqual(ThresholdUnits['XRAY_RADIO_BLACKOUT']);
    fireEvent.mouseDown(screen.getByTestId('category-select'));
    const menuItemCategory = await screen.findByText(EventCategory.PROTON_FLUX);
    fireEvent.click(menuItemCategory);
    await waitFor(() =>
      expect(
        screen.queryByTestId('thresholdunit-input')!.getAttribute('value'),
      ).toEqual(ThresholdUnits['PROTON_FLUX']),
    );
  });
  it('should show an error that startdate should be in the past for an Alert but not for a Warning', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    render(
      <Wrapper
        baseNotificationData={constructBaseNotification(null, 'new') as SWEvent}
        baseNotificationType="new"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(screen.queryByTestId('category-select')!.textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );

    fireEvent.mouseDown(screen.getByTestId('category-select'));
    const menuItemCategory = await screen.findByText(EventCategory.PROTON_FLUX);
    fireEvent.click(menuItemCategory);

    await waitFor(() =>
      expect(screen.queryByTestId('category-select')!.textContent).toEqual(
        EventCategory.PROTON_FLUX,
      ),
    );

    const startDatePicker = screen.getByTestId('start-date-picker');

    // Set start date to future
    const startDateInput = within(startDatePicker).getByRole('textbox');
    const newDate = dateUtils.dateToString(
      dateUtils.add(dateUtils.utc(), { hours: 1 }),
      dateUtils.DATE_FORMAT_DATEPICKER,
    );

    fireEvent.change(startDateInput, {
      target: { value: newDate },
    });
    expect((startDateInput as HTMLInputElement).value).toEqual(newDate);

    await screen.findByRole('alert');
    expect(
      screen.getByText(
        translateKeyOutsideComponents('notification-date-in-past'),
      ),
    ).toBeTruthy();

    const labelDropdown = screen.getByTestId('label-select');

    // change to Warning
    fireEvent.mouseDown(labelDropdown);
    const menuItemWarning = await screen.findByText('Warning');
    fireEvent.click(menuItemWarning);
    await waitFor(() => expect(screen.queryByRole('alert')).toBeFalsy());
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('notification-date-in-past'),
      ),
    ).toBeFalsy();

    // change to Alert
    fireEvent.mouseDown(labelDropdown);
    const menuItemAlert = await screen.findByText('Alert');
    fireEvent.click(menuItemAlert);

    await screen.findByRole('alert');
    expect(
      screen.getByText(
        translateKeyOutsideComponents('notification-date-in-past'),
      ),
    ).toBeTruthy();
  });
  it('should show an error for enddate when its more than a week in the past', async () => {
    const props = {
      statusTagContent: 'Warning',
      eventTypeDisabled: false,
      actionMode: 'Cancel',
    };
    const event = fakeEventList[2];

    render(
      <Wrapper
        baseNotificationData={event}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );
    expect(screen.getByTestId('category-select').textContent).toEqual(
      EventCategory.PROTON_FLUX,
    );

    const startDatePicker = screen.getByTestId('start-date-picker');
    const newStartDate = dateUtils.dateToString(
      dateUtils.sub(dateUtils.utc(), { days: 10 }),
      dateUtils.DATE_FORMAT_DATEPICKER,
    );

    fireEvent.change(within(startDatePicker).getByRole('textbox'), {
      target: { value: newStartDate },
    });

    await waitFor(() => {
      expect(
        (within(startDatePicker).getByRole('textbox') as HTMLInputElement)
          .value,
      ).toEqual(newStartDate);
    });

    const endDatePicker = screen.getByTestId('end-date-picker');
    const newEndDate = dateUtils.dateToString(
      dateUtils.sub(dateUtils.utc(), { days: 8 }),
      dateUtils.DATE_FORMAT_DATEPICKER,
    );
    fireEvent.change(within(endDatePicker).getByRole('textbox'), {
      target: { value: newEndDate },
    });

    await waitFor(() => {
      expect(
        (within(endDatePicker).getByRole('textbox') as HTMLInputElement).value,
      ).toEqual(newEndDate);
    });

    expect(await screen.findAllByRole('alert')).toHaveLength(2);
    expect(
      screen.queryAllByText(
        translateKeyOutsideComponents('notification-recent-date'),
      ),
    ).toHaveLength(2);
  });
});
