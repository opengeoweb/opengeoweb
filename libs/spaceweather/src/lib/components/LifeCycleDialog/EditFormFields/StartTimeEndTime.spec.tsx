/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import {
  defaultFormOptions,
  ReactHookFormHiddenInput,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';
import {
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';
import StartTimeEndTime, {
  isAfterStart,
  isEndDateRequired,
  isInFuture,
} from './StartTimeEndTime';
import { TestWrapper } from '../../../utils/testUtils';
import {
  i18n,
  initSpaceweatherI18n,
  translateKeyOutsideComponents,
} from '../../../utils/i18n';

beforeAll(() => {
  initSpaceweatherI18n();
});

describe('components/LifeCycleDialog/EditFormFields/StartTimeEndTime', () => {
  const now = dateUtils.utc();
  const currentTime = dateUtils.dateToString(now);
  const currentTimeDisplayed = dateUtils.dateToString(
    now,
    dateUtils.DATE_FORMAT_DATEPICKER,
  );
  const pastTime = dateUtils.dateToString(
    dateUtils.sub(now, { hours: 1 }),
    dateUtils.DATE_FORMAT_DATEPICKER,
  );
  const futureTime = dateUtils.dateToString(
    dateUtils.add(now, { hours: 1 }),
    dateUtils.DATE_FORMAT_DATEPICKER,
  );
  it('should show an error message when setting start date to a date in the future for an Alert', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              label: 'ALERT',
              neweventstart: currentTime,
            },
          }}
        >
          <StartTimeEndTime actionMode="none" />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const startDatePicker = screen.getByTestId('start-date-picker');
    const startDateInput = within(startDatePicker).getByRole('textbox');

    expect(startDateInput.getAttribute('value')!).toEqual(currentTimeDisplayed);
    expect(screen.queryByRole('alert')).toBeFalsy();

    // set date in future
    fireEvent.change(startDateInput, {
      target: { value: futureTime },
    });
    await waitFor(() => {
      expect(startDateInput.getAttribute('value')!).toEqual(futureTime);
    });
    expect(screen.getByRole('alert')).toBeTruthy();

    expect(
      await screen.findByText(
        translateKeyOutsideComponents('notification-date-in-past'),
      ),
    ).toBeTruthy();

    // set date in past
    fireEvent.change(startDateInput, {
      target: { value: pastTime },
    });
    await waitFor(async () => {
      expect(startDateInput.getAttribute('value')!).toEqual(pastTime);
    });
    expect(screen.queryByRole('alert')).toBeFalsy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('notification-date-in-past'),
      ),
    ).toBeFalsy();
  });

  it('should not show an error message when setting start date to a date in the future for a Warning', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              label: 'WARNING',
              neweventstart: currentTime,
            },
          }}
        >
          <ReactHookFormHiddenInput name="label" />
          <StartTimeEndTime actionMode="none" />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const startDatePicker = screen.getByTestId('start-date-picker');
    const startDateInput = within(startDatePicker).getByRole('textbox');
    expect(startDateInput.getAttribute('value')!).toEqual(currentTimeDisplayed);
    expect(screen.queryByRole('alert')).toBeFalsy();

    // set date in future
    fireEvent.change(startDateInput, {
      target: { value: futureTime },
    });
    await waitFor(async () => {
      expect(startDateInput.getAttribute('value')!).toEqual(futureTime);
    });
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('notification-date-in-past'),
      ),
    ).toBeFalsy();
  });

  it('should not show an error message when setting end date to a date in the past when Summarising', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              label: 'ALERT',
              neweventstart: dateUtils.dateToString(
                dateUtils.sub(dateUtils.utc(), { days: 1 }),
              ),
              neweventend: currentTime,
            },
          }}
        >
          <StartTimeEndTime actionMode="Summarise" />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const endDatePicker = screen.getByTestId('end-date-picker');
    const endDateInput = within(endDatePicker).getByRole('textbox');
    expect(endDateInput.getAttribute('value')!).toEqual(currentTimeDisplayed);
    expect(screen.queryByRole('alert')).toBeFalsy();

    // set date in past
    fireEvent.change(endDateInput, {
      target: { value: pastTime },
    });
    await waitFor(async () => {
      expect(endDateInput.getAttribute('value')!).toEqual(pastTime);
    });
    expect(screen.queryByRole('alert')).toBeFalsy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('notification-end-time-future'),
      ),
    ).toBeFalsy();
  });

  it('should not show an error message when setting end date to a date in the past when Cancelling', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              label: 'WARNING',
              neweventstart: dateUtils.dateToString(
                dateUtils.sub(dateUtils.utc(), { days: 1 }),
              ),
              neweventend: currentTime,
            },
          }}
        >
          <StartTimeEndTime actionMode="Cancel" />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const endDatePicker = screen.getByTestId('end-date-picker');
    const endDateInput = within(endDatePicker).getByRole('textbox');
    expect(endDateInput.getAttribute('value')!).toEqual(currentTimeDisplayed);
    expect(screen.queryByRole('alert')).toBeFalsy();

    // set date in past
    fireEvent.change(endDateInput, {
      target: { value: pastTime },
    });
    await waitFor(() => {
      expect(endDateInput.getAttribute('value')!).toEqual(pastTime);
    });
    expect(screen.queryByRole('alert')).toBeFalsy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('notification-end-time-future'),
      ),
    ).toBeFalsy();
  });

  it('should show an error message when setting end date to a date in the past', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              label: 'WARNING',
              neweventstart: dateUtils.dateToString(
                dateUtils.sub(dateUtils.utc(), { days: 1 }),
              ),
              neweventend: currentTime,
            },
          }}
        >
          <StartTimeEndTime actionMode="none" />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const endDatePicker = screen.getByTestId('end-date-picker');
    const endDateInput = within(endDatePicker).getByRole('textbox');
    expect(endDateInput.getAttribute('value')!).toEqual(currentTimeDisplayed);
    expect(screen.queryByRole('alert')).toBeFalsy();

    // set date in past
    fireEvent.change(endDateInput, {
      target: { value: pastTime },
    });
    await waitFor(async () => {
      expect(endDateInput.getAttribute('value')!).toEqual(pastTime);
    });
    expect(screen.getByRole('alert')).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('notification-end-time-future'),
      ),
    ).toBeTruthy();

    // set date in future
    fireEvent.change(endDateInput, {
      target: { value: futureTime },
    });
    await waitFor(async () => {
      expect(endDateInput.getAttribute('value')!).toEqual(futureTime);
    });
    expect(screen.queryByRole('alert')).toBeFalsy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('notification-end-time-future'),
      ),
    ).toBeFalsy();
  });

  describe('isInFuture', () => {
    it('should validate if date is in future', () => {
      expect(
        isInFuture(
          i18n.t,
          dateUtils.dateToString(dateUtils.add(dateUtils.utc(), { hours: 1 }))!,
          false,
        ),
      ).toBeTruthy();
      expect(
        isInFuture(
          i18n.t,
          dateUtils.dateToString(dateUtils.add(dateUtils.utc(), { years: 1 }))!,
          false,
        ),
      ).toBeTruthy();
      expect(
        isInFuture(i18n.t, dateUtils.dateToString(dateUtils.utc())!, false),
      ).toEqual(translateKeyOutsideComponents('notification-end-time-future'));
      expect(
        isInFuture(
          i18n.t,
          dateUtils.dateToString(dateUtils.sub(dateUtils.utc(), { hours: 1 }))!,
          false,
        ),
      ).toEqual(translateKeyOutsideComponents('notification-end-time-future'));
    });

    it('should allow past date if needed', () => {
      expect(
        isInFuture(
          i18n.t,
          dateUtils.dateToString(dateUtils.sub(dateUtils.utc(), { hours: 1 }))!,
          true,
        ),
      ).toBeTruthy();
    });
  });
  describe('isAfterStart', () => {
    it('should validate date is after supplied date', () => {
      const pastTime = dateUtils.dateToString(
        dateUtils.sub(dateUtils.utc(), { hours: 1 }),
      )!;

      const futureTime = dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 1 }),
      )!;

      expect(isAfterStart(i18n.t, pastTime, futureTime)).toEqual(
        translateKeyOutsideComponents('notification-end-time-after-start'),
      );
      expect(isAfterStart(i18n.t, futureTime, pastTime)).toBeTruthy();
    });
  });
  describe('isEndDateRequired', () => {
    it('should give an error when value is empty and label is WARNING or actionmode is SUMMARISE', () => {
      expect(isEndDateRequired(i18n.t, '', 'WARNING')).toEqual(
        'This field is required',
      );
      expect(isEndDateRequired(i18n.t, '', 'ALERT', 'Summarise')).toEqual(
        'This field is required',
      );
      expect(isEndDateRequired(i18n.t, '', 'ALERT')).toEqual(true);
      expect(isEndDateRequired(i18n.t, '2021-07-05 12:00', 'WARNING')).toEqual(
        true,
      );
      expect(
        isEndDateRequired(i18n.t, '2021-07-05 12:00', 'ALERT', 'Summarise'),
      ).toEqual(true);
    });
  });
});
