/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';

import XrayClassPeakClass, { isValidPeakClass } from './XrayClassPeakClass';
import {
  i18n,
  initSpaceweatherI18n,
  translateKeyOutsideComponents,
} from '../../../utils/i18n';
import { TestWrapper } from '../../../utils/testUtils';

beforeAll(() => {
  initSpaceweatherI18n();
});

describe('components/LifeCycleDialog/EditFormFields/XrayClassPeakClass', () => {
  it('should show an error message when invalid peak class is entered', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              category: 'XRAY_RADIO_BLACKOUT',
            },
          }}
        >
          <XrayClassPeakClass actionMode="Summarise" />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    const peakClassInput = screen.getByTestId('peakclass-input');
    fireEvent.change(peakClassInput, { target: { value: '5.5' } });
    await screen.findByText(
      translateKeyOutsideComponents('notification-peak-class-error'),
    );
    fireEvent.change(peakClassInput, { target: { value: 'm5.5' } });
    await waitFor(() =>
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('notification-peak-class-error'),
        ),
      ).toBeFalsy(),
    );
    // should convert to uppercase
    expect(peakClassInput.getAttribute('value')).toEqual('M5.5');
    fireEvent.change(peakClassInput, { target: { value: 'X5.10' } });
    await screen.findByText(
      translateKeyOutsideComponents('notification-peak-class-error'),
    );
    fireEvent.change(peakClassInput, { target: { value: 'X10.7' } });
    await waitFor(() =>
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('notification-peak-class-error'),
        ),
      ).toBeFalsy(),
    );
    fireEvent.change(peakClassInput, { target: { value: 'X10' } });
    await screen.findByText(
      translateKeyOutsideComponents('notification-peak-class-error'),
    );
  });
  describe('isValidPeakClass', () => {
    it('should return an error message for invalid peak class value', () => {
      expect(isValidPeakClass(i18n.t, '1')).toEqual(
        translateKeyOutsideComponents('notification-peak-class-error'),
      );
      expect(isValidPeakClass(i18n.t, 'M5')).toEqual(
        translateKeyOutsideComponents('notification-peak-class-error'),
      );
      expect(isValidPeakClass(i18n.t, 'x10.2')).toEqual(
        translateKeyOutsideComponents('notification-peak-class-error'),
      );
      expect(isValidPeakClass(i18n.t, 'm5.5')).toEqual(
        translateKeyOutsideComponents('notification-peak-class-error'),
      );
      expect(isValidPeakClass(i18n.t, 'M.4')).toEqual(
        translateKeyOutsideComponents('notification-peak-class-error'),
      );
      expect(isValidPeakClass(i18n.t, 'X10')).toEqual(
        translateKeyOutsideComponents('notification-peak-class-error'),
      );
      expect(isValidPeakClass(i18n.t, 'X10,2')).toEqual(
        translateKeyOutsideComponents('notification-peak-class-error'),
      );
      expect(isValidPeakClass(i18n.t, 'X1.20')).toEqual(
        translateKeyOutsideComponents('notification-peak-class-error'),
      );
    });
    it('should return true for a valid peak class value', () => {
      expect(isValidPeakClass(i18n.t, 'M5.5')).toEqual(true);
      expect(isValidPeakClass(i18n.t, 'X10.2')).toEqual(true);
    });
  });
});
