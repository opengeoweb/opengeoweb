/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';

import PeakFlux from './PeakFlux';
import { translateKeyOutsideComponents } from '../../../utils/i18n';
import { TestWrapper } from '../../../utils/testUtils';

describe('components/LifeCycleDialog/EditFormFields/PeakFlux', () => {
  it('should show an error message when invalid peak flux is entered', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              label: 'Warning',
            },
          }}
        >
          <PeakFlux />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    const peakFluxInput = screen.getByTestId('peakflux-input');
    fireEvent.change(peakFluxInput, { target: { value: '-1' } });
    await screen.findByText(
      translateKeyOutsideComponents('notification-peak-flux-error'),
    );
    fireEvent.change(peakFluxInput, { target: { value: '0' } });
    await waitFor(() =>
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('notification-peak-flux-error'),
        ),
      ).toBeFalsy(),
    );
  });
});
