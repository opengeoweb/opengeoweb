/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';

import SuddenImpulse from './SuddenImpulse';
import { translateKeyOutsideComponents } from '../../../utils/i18n';
import { TestWrapper } from '../../../utils/testUtils';

describe('components/LifeCycleDialog/EditFormFields/SuddenImpulse', () => {
  it('should show an error message when invalid polarity is entered', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              label: 'Warning',
            },
          }}
        >
          <SuddenImpulse />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    const polarityInput = screen.getByTestId('observedpolaritybz-input');
    fireEvent.change(polarityInput, { target: { value: '-1001' } });
    await screen.findByText(
      translateKeyOutsideComponents('notification-polarity-error'),
    );
    fireEvent.change(polarityInput, { target: { value: '-1000' } });
    await waitFor(() =>
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('notification-polarity-error'),
        ),
      ).toBeFalsy(),
    );
    fireEvent.change(polarityInput, { target: { value: '1001' } });
    await screen.findByText(
      translateKeyOutsideComponents('notification-polarity-error'),
    );
    fireEvent.change(polarityInput, { target: { value: '1000' } });
    await waitFor(() =>
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('notification-polarity-error'),
        ),
      ).toBeFalsy(),
    );
  });
  it('should show an error message when invalid solar wind is entered', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              label: 'Warning',
            },
          }}
        >
          <SuddenImpulse />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    const solarWindInput = screen.getByTestId('observedsolarwind-input');
    fireEvent.change(solarWindInput, { target: { value: '99' } });
    await screen.findByText(
      translateKeyOutsideComponents('notification-solar-wind-error'),
    );
    fireEvent.change(solarWindInput, { target: { value: '100' } });
    await waitFor(() =>
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('notification-solar-wind-error'),
        ),
      ).toBeFalsy(),
    );
    fireEvent.change(solarWindInput, { target: { value: '10001' } });
    await screen.findByText(
      translateKeyOutsideComponents('notification-solar-wind-error'),
    );
    fireEvent.change(solarWindInput, { target: { value: '10000' } });
    await waitFor(() =>
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('notification-solar-wind-error'),
        ),
      ).toBeFalsy(),
    );
  });
});
