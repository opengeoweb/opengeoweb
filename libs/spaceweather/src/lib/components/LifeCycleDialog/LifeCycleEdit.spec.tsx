/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import LifeCycleEdit from './LifeCycleEdit';
import {
  fakeEventList,
  mockDraftEvent,
  mockEventInternal,
  mockEventMissingData,
} from '../../utils/fakedata';
import { TestWrapper } from '../../utils/testUtils';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { SpaceWeatherApi } from '../../utils/api';

describe('src/components/LifeCycleDialog/LifeCycleEdit', () => {
  it('should handle saving a new notification as draft', async () => {
    jest.spyOn(console, 'log').mockImplementation();
    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'internalprovider',
      baseNotificationData: fakeEventList[17],
    };
    render(
      <TestWrapper>
        <LifeCycleEdit {...props} />
      </TestWrapper>,
    );

    // Wait till title and message have been retrieved
    expect(
      await screen.findByText('Template message text for you'),
    ).toBeTruthy();
    expect(screen.getByTestId('notification-title').getAttribute('value')).toBe(
      'Template title for you',
    );

    fireEvent.click(screen.getByTestId('draft'));

    // wait for the api response, loader should be gone
    await waitFor(() =>
      expect(screen.getByTestId('loader').getAttribute('style')).toContain(
        'opacity: 0;',
      ),
    );
    await waitFor(() => {
      expect(props.toggleDialogOpen).toHaveBeenCalled();
    });
  });
  it('should set error message when issue notification fails', async () => {
    const spy = jest.fn().mockRejectedValue(new Error('Saving failed'));
    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      issueNotification: spy,
    });
    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'internalprovider',
      baseNotificationData: mockEventInternal,
      setErrorStoreMessage: jest.fn(),
    };
    render(
      <TestWrapper createApi={fakeApi}>
        <LifeCycleEdit {...props} />
      </TestWrapper>,
    );

    // Wait till title and message have been retrieved
    expect(
      await screen.findByText('Template message text for you'),
    ).toBeTruthy();
    expect(screen.getByTestId('notification-title').getAttribute('value')).toBe(
      'Template title for you',
    );

    // enter eventlevel
    fireEvent.mouseDown(screen.getByTestId('eventlevel-select'));
    const menuItemLevelR = await screen.findByText('R1');
    fireEvent.click(menuItemLevelR);

    fireEvent.click(screen.getByTestId('issue'));

    // wait for the api response, loader should be gone
    await waitFor(() => {
      expect(screen.getByTestId('loader').getAttribute('style')).toContain(
        'opacity: 0;',
      );
    });
    expect(spy).toHaveBeenCalledTimes(1);
    expect(props.toggleDialogOpen).not.toHaveBeenCalled();
    expect(props.setErrorStoreMessage).toHaveBeenCalled();
  });

  it('should handle discarding a draft notification and show confirmation dialog', async () => {
    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'draft',
      baseNotificationData: mockDraftEvent,
    };
    render(
      <ThemeWrapper>
        <TestWrapper>
          <LifeCycleEdit {...props} />
        </TestWrapper>
      </ThemeWrapper>,
    );

    // Check that the draft title and message are used
    expect(await screen.findByText('This is a draft message')).toBeTruthy();
    expect(screen.getByTestId('notification-title').getAttribute('value')).toBe(
      'Geomagnetic Kp index alert',
    );

    // Click on the discard button and expect the confirm dialog to be visible
    fireEvent.click(screen.getByTestId('discard'));
    await waitFor(() => {
      expect(screen.getByTestId('confirmationDialog')).toBeTruthy();
    });
    expect(screen.getByTestId('customDialog-close')).toBeTruthy();

    // Click on Cancel and expect the confirm dialog to close but the draft to still be visible
    const cancelButton = screen.getByTestId('confirmationDialog-cancel');
    fireEvent.click(cancelButton);
    await waitFor(() => {
      expect(screen.queryByTestId('confirmationDialog')).toBeFalsy();
    });
    expect(screen.getByText('This is a draft message')).toBeTruthy();

    // Click on back button and expect the confirm dialog to be visible
    fireEvent.click(screen.getByTestId('discard'));
    await waitFor(() => {
      expect(screen.getByTestId('confirmationDialog')).toBeTruthy();
    });

    // Click on Discard and Close and expect the confirm dialog to close and also the draft to close
    const confirmButton = screen.getByTestId('confirmationDialog-confirm');
    fireEvent.click(confirmButton!);

    // wait for the api response, loader should be gone
    await waitFor(() =>
      expect(screen.getByTestId('loader').getAttribute('style')).toContain(
        'opacity: 0;',
      ),
    );
    await waitFor(() => expect(props.toggleDialogOpen).toHaveBeenCalled());
  });

  it('should handle discarding a new notification', async () => {
    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'new',
      baseNotificationData: null!,
    };
    render(
      <TestWrapper>
        <LifeCycleEdit {...props} />
      </TestWrapper>,
    );
    // Expect title to be empty
    expect(screen.getByTestId('notification-title').getAttribute('value')).toBe(
      '',
    );
    expect(screen.getByTestId('notification-text').textContent).toBe('');

    fireEvent.click(screen.getByTestId('discard'));

    expect(props.toggleDialogOpen).toHaveBeenCalled();
    // loader should not be visible
    expect(screen.getByTestId('loader').getAttribute('style')).toContain(
      'opacity: 0;',
    );
  });
  it('should not call the template populate function in case of a new notification', async () => {
    const fakePromiseFunction = jest.fn();
    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getRePopulateTemplateContent: fakePromiseFunction,
    });

    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'new',
      setErrorRetrievePopulate: jest.fn(),
    };
    render(
      <TestWrapper createApi={fakeApi}>
        <LifeCycleEdit {...props} />
      </TestWrapper>,
    );

    await waitFor(() => {
      // Ensure no templates have been added
      expect(screen.getByTestId('notification-text').textContent).toBe('');
    });
    expect(screen.getByTestId('notification-title').getAttribute('value')).toBe(
      '',
    );

    expect(fakePromiseFunction).not.toHaveBeenCalled();
  });
  it('should not call the template populate function in case of a draft notification', async () => {
    const fakePromiseFunction = jest.fn();
    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getRePopulateTemplateContent: fakePromiseFunction,
    });

    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'draft',
      baseNotificationData: mockDraftEvent,
      setErrorRetrievePopulate: jest.fn(),
    };
    render(
      <TestWrapper createApi={fakeApi}>
        <LifeCycleEdit {...props} />
      </TestWrapper>,
    );

    await waitFor(() => {
      // Check that the draft title and message are used
      expect(screen.getByText('This is a draft message')).toBeTruthy();
    });
    expect(screen.getByTestId('notification-title').getAttribute('value')).toBe(
      'Geomagnetic Kp index alert',
    );

    expect(fakePromiseFunction).not.toHaveBeenCalled();
  });
  it('should call the template populate function in case there are no validation errors', async () => {
    const fakePromiseFunction = jest.fn().mockResolvedValue({
      data: {
        message: 'fake template returned message',
        title: 'fake template returned title',
      },
    });

    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getRePopulateTemplateContent: fakePromiseFunction,
    });

    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'internalprovider',
      baseNotificationData: fakeEventList[17],
      setErrorRetrievePopulate: jest.fn(),
    };
    render(
      <TestWrapper createApi={fakeApi}>
        <LifeCycleEdit {...props} />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(fakePromiseFunction).toHaveBeenCalled();
    });

    // Check that returned message and title are inserted into the form
    expect(
      await screen.findByText('fake template returned message'),
    ).toBeTruthy();
    expect(screen.getByTestId('notification-title').getAttribute('value')).toBe(
      'fake template returned title',
    );
  });
  it('should not call the template populate function in case there are validation errors', async () => {
    const fakePromiseFunction = jest.fn().mockResolvedValue({
      data: {
        message: 'fake template returned message',
        title: 'fake template returned title',
      },
    });

    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getRePopulateTemplateContent: fakePromiseFunction,
    });

    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'internalprovider',
      baseNotificationData: mockEventMissingData,
    };
    render(
      <TestWrapper createApi={fakeApi}>
        <LifeCycleEdit {...props} />
      </TestWrapper>,
    );

    await screen.findByRole('alert');
    expect(fakePromiseFunction).not.toHaveBeenCalled();

    // Ensure no templates have been added
    expect(screen.getByTestId('notification-text').textContent).toBe('');
    expect(screen.getByTestId('notification-title').getAttribute('value')).toBe(
      '',
    );
  });
  it('should call the template populate function when pressing the button and there are no validation errors', async () => {
    const fakePromiseFunction = jest.fn().mockResolvedValue({
      data: {
        message: 'fake template returned message',
        title: 'fake template returned title',
      },
    });

    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getRePopulateTemplateContent: fakePromiseFunction,
    });

    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'internalprovider',
      baseNotificationData: mockEventMissingData,
    };

    render(
      <TestWrapper createApi={fakeApi}>
        <LifeCycleEdit {...props} />
      </TestWrapper>,
    );

    await screen.findByRole('alert');
    expect(fakePromiseFunction).not.toHaveBeenCalled();

    // Ensure no templates have been added
    expect(screen.getByTestId('notification-text').textContent).toBe('');
    expect(screen.getByTestId('notification-title').getAttribute('value')).toBe(
      '',
    );

    fireEvent.change(screen.getByTestId('datasource-input'), {
      target: { value: 'GOES' },
    });

    fireEvent.click(screen.getByTestId('notification-repopulate'));

    await waitFor(() => {
      expect(fakePromiseFunction).toHaveBeenCalled();
    });

    // Check that returned message and title are inserted into the form
    expect(
      await screen.findByText('fake template returned message'),
    ).toBeTruthy();
    expect(screen.getByTestId('notification-title').getAttribute('value')).toBe(
      'fake template returned title',
    );
  });
  it('should pass an error message to be shown in the dialog alert when retrieval of templates fails', async () => {
    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getRePopulateTemplateContent: (): Promise<{
        data: { title: string; message: string };
      }> => Promise.reject(new Error('test error message for templates')),
    });

    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'internalprovider',
      baseNotificationData: fakeEventList[17],
      setErrorRetrievePopulate: jest.fn(),
    };
    render(
      <TestWrapper createApi={fakeApi}>
        <LifeCycleEdit {...props} />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(props.setErrorRetrievePopulate).toHaveBeenCalled();
    });
    expect(props.setErrorRetrievePopulate).toHaveBeenCalledWith(
      'test error message for templates',
    );

    // Ensure no templates have been added
    expect(screen.getByTestId('notification-text').textContent).toBe('');
    expect(screen.getByTestId('notification-title').getAttribute('value')).toBe(
      '',
    );
  });
  it('should pass an error message to be shown in the dialog alert when retrieval of templates fails in case AxiosError is detected', async () => {
    const fakeAxiosError = {
      isAxiosError: true,
      config: undefined,
      toJSON: undefined,
      name: 'API error',
      message: 'Some other message',
      response: {
        data: 'Axios error message',
        status: 400,
        statusText: '',
        config: undefined,
        headers: [],
      },
    } as Error;

    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getRePopulateTemplateContent: (): Promise<{
        data: { title: string; message: string };
      }> => Promise.reject(fakeAxiosError),
    });

    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'internalprovider',
      baseNotificationData: fakeEventList[17],
      setErrorRetrievePopulate: jest.fn(),
    };
    render(
      <TestWrapper createApi={fakeApi}>
        <LifeCycleEdit {...props} />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(props.setErrorRetrievePopulate).toHaveBeenCalled();
    });
    expect(props.setErrorRetrievePopulate).toHaveBeenCalledWith(
      'Axios error message',
    );

    // Ensure no templates have been added
    expect(screen.getByTestId('notification-text').textContent).toBe('');
    expect(screen.getByTestId('notification-title').getAttribute('value')).toBe(
      '',
    );
  });
});
