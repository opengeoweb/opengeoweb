/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import {
  defaultFormOptions,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';
import { Meta, StoryObj } from '@storybook/react/*';
import LifeCycleDialog, {
  getDialogTitle,
  LifeCycleDialogProps,
} from './LifeCycleDialog';
import { StoryWrapperWithErrorOnSave } from '../../utils/storybookUtils';
import { fakeEventList, fakeEventListFixedDates } from '../../utils/fakedata';
import { NotificationTriggerProvider } from '../NotificationTrigger';
import { ContentDialog } from '../ContentDialog';
import { LifeCycleEditForm } from './LifeCycleEdit';
import { constructBaseNotification } from './utils';
import {
  EventCategory,
  EventCategoryDetail,
  NotificationLabel,
} from '../../types';
import { createFakeApiFixedDates } from '../../utils/fakeApi';
import { i18n } from '../../utils/i18n';

const meta: Meta<typeof LifeCycleDialog> = {
  title: 'components/LifeCycleDialog',
  component: LifeCycleDialog,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the LifeCycleDialog',
      },
    },
  },
  tags: ['!autodocs'],
};
export default meta;

type Story = StoryObj<typeof LifeCycleDialog>;

const defaultProps: LifeCycleDialogProps = {
  open: true,
  toggleStatus: (): void => {},
};

export const NewNotification: Story = {
  args: {
    ...defaultProps,
    dialogMode: 'new',
  },
};

export const MetOfficeRadioBlackoutEvent: Story = {
  args: {
    ...defaultProps,
    dialogMode: 'METRB1',
    event: fakeEventList[3],
  },
};

export const MetOfficeSummarisedEvent: Story = {
  args: {
    ...defaultProps,
    dialogMode: 'METRB3',
    event: fakeEventListFixedDates[6],
  },
  parameters: {
    createApiFunc: createFakeApiFixedDates,
  },
  tags: ['snapshot'],
};

MetOfficeSummarisedEvent.storyName = 'Met Office Summarised Event';

export const MetOfficeCancelledEvent: Story = {
  args: {
    ...defaultProps,
    dialogMode: 'METRB122',
    event: fakeEventListFixedDates[2],
  },
  parameters: {
    createApiFunc: createFakeApiFixedDates,
  },
  tags: ['snapshot'],
};

MetOfficeCancelledEvent.storyName = 'Met Office Cancelled Event';

export const MetOfficeGeomagneticSuddenImpulseAlert: Story = {
  args: {
    ...defaultProps,
    dialogMode: 'METRB2',
    event: fakeEventList[1],
  },
};

export const MetOfficeGeomagneticSuddenImpulseWarning: Story = {
  args: {
    ...defaultProps,
    dialogMode: 'METRB4',
    event: fakeEventList[8],
  },
};

const useBlurFieldForSnapshot = (): void => {
  React.useEffect(() => {
    // blur field for snapshot
    setTimeout(() => {
      (document.activeElement as HTMLElement).blur();
    }, 1000);
  }, []);
};

export const MetOfficeGeomagneticKpIndexAlert: Story = {
  args: {
    ...defaultProps,
    dialogMode: 'METRB345',
    event: fakeEventListFixedDates[11],
  },
  render: (props) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useBlurFieldForSnapshot();
    return <LifeCycleDialog {...props} />;
  },
  parameters: {
    createApiFunc: createFakeApiFixedDates,
  },
  tags: ['snapshot'],
};

MetOfficeGeomagneticKpIndexAlert.storyName =
  'Met Office Geomagnetic KpIndex Alert';

export const KNMIGeomagneticKpIndexWarning: Story = {
  args: {
    ...defaultProps,
    dialogMode: 'KNMI002',
    event: fakeEventListFixedDates[12],
  },
  parameters: {
    createApiFunc: createFakeApiFixedDates,
  },
  tags: ['snapshot'],
};

KNMIGeomagneticKpIndexWarning.storyName = 'KNMI Geomagnetic KpIndex Warning';

export const MetOfficeGeomagneticStormWatch: Story = {
  args: {
    ...defaultProps,
    dialogMode: 'METRWATCH121',
    event: fakeEventList[14],
  },
};

export const MetOfficeProtonFlux100AlertSummary: Story = {
  args: {
    ...defaultProps,
    dialogMode: 'METRB1123454',
    event: fakeEventList[0],
  },
};

export const MetOfficeProtonFlux100Warning: Story = {
  args: {
    ...defaultProps,
    dialogMode: 'METRB122',
    event: fakeEventList[2],
  },
};

export const MetOfficeProtonFlux10Warning: Story = {
  args: {
    ...defaultProps,
    dialogMode: 'METRBWARNPRO1084847',
    event: fakeEventListFixedDates[13],
  },
  render: (props) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useBlurFieldForSnapshot();
    return <LifeCycleDialog {...props} />;
  },
  parameters: {
    createApiFunc: createFakeApiFixedDates,
  },
  tags: ['snapshot'],
};

MetOfficeProtonFlux10Warning.storyName = 'Met Office Proton Flux 10 Warning';

export const MetOfficeProtonFlux10AlertSummary: Story = {
  args: {
    ...defaultProps,
    dialogMode: 'METRB1123453453454',
    event: fakeEventList[10],
  },
};

export const MetOfficeElectronFlux2AlertSummary: Story = {
  args: {
    ...defaultProps,
    dialogMode: 'METRB3',
    event: fakeEventList[6],
  },
};

const MetOfficeElectronFluenceWarningComponent = (): React.ReactElement => {
  return (
    <LifeCycleDialog
      open
      toggleStatus={(): void => {
        /* Do nothing */
      }}
      dialogMode="METRB25"
      event={fakeEventList[9]}
    />
  );
};

export const MetOfficeElectronFluenceWarning = (): React.ReactElement => {
  return <MetOfficeElectronFluenceWarningComponent />;
};

export const NewNotificationWithTrigger = (): React.ReactElement => {
  return (
    <NotificationTriggerProvider>
      <LifeCycleDialog {...defaultProps} dialogMode="new" />
    </NotificationTriggerProvider>
  );
};

export const WithErrorOnSaveNewNotification = (): React.ReactElement => {
  return (
    <StoryWrapperWithErrorOnSave>
      <NotificationTriggerProvider>
        <LifeCycleDialog {...defaultProps} dialogMode="new" />
      </NotificationTriggerProvider>
    </StoryWrapperWithErrorOnSave>
  );
};

export const WithErrorOnSaveElectronFluenceWarning = (): React.ReactElement => {
  return (
    <StoryWrapperWithErrorOnSave>
      <NotificationTriggerProvider>
        <MetOfficeElectronFluenceWarningComponent />
      </NotificationTriggerProvider>
    </StoryWrapperWithErrorOnSave>
  );
};

export const NewNotificationGeomagneticSnap = (): React.ReactElement => {
  return (
    <ContentDialog
      open
      title={getDialogTitle(i18n.t, 'new', null!)}
      toggleStatus={(): void => {}}
      maxWidth="sm"
    >
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            ...constructBaseNotification(null, 'new'),
            neweventstart: '2022-01-01T12:00:00Z',
            category: Object.keys(EventCategory)[1],
            categorydetail: Object.keys(EventCategoryDetail)[0],
            label: Object.keys(NotificationLabel)[1],
          },
        }}
      >
        <LifeCycleEditForm
          toggleDialogOpen={(): void => {}}
          baseNotificationType="new"
          statusTagContent="Watch"
        />
      </ReactHookFormProvider>
    </ContentDialog>
  );
};

NewNotificationGeomagneticSnap.storyName = 'New Notification Geomagnetic Storm';
NewNotificationGeomagneticSnap.tags = ['snapshot'];

export const NewNotificationWithTriggerSnap = (): React.ReactElement => {
  return (
    <NotificationTriggerProvider>
      <ContentDialog
        open
        title={getDialogTitle(i18n.t, 'new', null!)}
        toggleStatus={(): void => {}}
        maxWidth="sm"
      >
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...constructBaseNotification(null, 'new'),
              neweventstart: '2022-01-01T12:00:00Z',
            },
          }}
        >
          <LifeCycleEditForm
            toggleDialogOpen={(): void => {}}
            baseNotificationType="new"
          />
        </ReactHookFormProvider>
      </ContentDialog>
    </NotificationTriggerProvider>
  );
};

NewNotificationWithTriggerSnap.storyName = 'New Notification With Trigger';
NewNotificationWithTriggerSnap.tags = ['snapshot'];
