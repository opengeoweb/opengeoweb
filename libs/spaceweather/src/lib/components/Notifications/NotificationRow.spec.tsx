/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { dateUtils } from '@opengeoweb/shared';

import { render, fireEvent, screen } from '@testing-library/react';
import NotificationRow from './NotificationRow';
import {
  mockEventUnacknowledgedExternal,
  mockEventAcknowledgedExternal,
  mockEventAcknowledgedExternalDraft,
} from '../../utils/fakedata';
import { EventCategory } from '../../types';
import { SizeWrapper, TestWrapper } from '../../utils/testUtils';

describe('src/components/NotificationRow/NotificationRow', () => {
  it('should call onNotificationRowClick clicking on a row', () => {
    const props = {
      event: mockEventUnacknowledgedExternal,
      onNotificationRowClick: jest.fn(),
    };
    render(
      <TestWrapper>
        <NotificationRow {...props} />
      </TestWrapper>,
    );

    fireEvent.click(screen.getByRole('button'));

    expect(props.onNotificationRowClick).toHaveBeenCalledWith(
      mockEventUnacknowledgedExternal,
    );
  });

  it('should show unacknowledged external event correctly (on lg size screens)', () => {
    const props = {
      event: mockEventUnacknowledgedExternal,
      onNotificationRowClick: jest.fn(),
    };
    render(
      <SizeWrapper>
        <NotificationRow {...props} />
      </SizeWrapper>,
    );

    // Show right event category and level and external issue time
    expect(
      screen.getByText(
        `${
          mockEventUnacknowledgedExternal.lifecycles!.externalprovider!
            .eventlevel
        } ${
          EventCategory[
            mockEventUnacknowledgedExternal.category as keyof typeof EventCategory
          ]
        }: ${dateUtils.dateToString(
          dateUtils.utc(
            mockEventUnacknowledgedExternal.lifecycles!.externalprovider!
              .lastissuetime!,
          ),
          `${dateUtils.DATE_FORMAT_DATEPICKER}`,
        )!}`,
      ),
    ).toBeTruthy();

    // Show the right tag
    expect(
      screen.getByTestId('notificationRow-externalStatusTag').textContent,
    ).toEqual('Alert');

    // Show the new notification icon (lg size screen so large should show)
    expect(
      screen.getByTestId('newNotificationRowIcon-large'),
    ).not.toHaveAttribute('aria-hidden', 'false');
    expect(
      screen.queryByTestId('newNotificationRowIcon-small'),
    ).toHaveAttribute('aria-hidden', 'true');

    // internal last issue time is empty
    expect(
      screen.queryByTestId('notificationRow-internalLastIssueTime'),
    ).toBeFalsy();

    // no edit draft button to be present
    expect(screen.queryByTestId('notificationRow-draft')).toBeFalsy();

    // internal notification tag should not be present
    expect(
      screen.getByTestId('notificationRow-internalStatusTagDraft').textContent,
    ).toEqual('');
  });

  it('should show unacknowledged external event correctly on >lg size screen', () => {
    const props = {
      event: mockEventUnacknowledgedExternal,
      onNotificationRowClick: jest.fn(),
    };
    render(
      <SizeWrapper width="xl">
        <NotificationRow {...props} />
      </SizeWrapper>,
    );

    // Show the new notification icon (xl size screen so large should show)
    expect(
      screen.getByTestId('newNotificationRowIcon-large'),
    ).not.toHaveAttribute('aria-hidden', 'false');
    expect(
      screen.queryByTestId('newNotificationRowIcon-small'),
    ).toHaveAttribute('aria-hidden', 'true');
  });

  it('should show unacknowledged external event correctly on md size screen', () => {
    const props = {
      event: mockEventUnacknowledgedExternal,
      onNotificationRowClick: jest.fn(),
    };
    render(
      <SizeWrapper width="md">
        <NotificationRow {...props} />
      </SizeWrapper>,
    );

    // Show the new notification icon (md size screen so small should show)
    expect(
      screen.getByTestId('newNotificationRowIcon-small'),
    ).not.toHaveAttribute('aria-hidden', 'false');
    expect(screen.getByTestId('newNotificationRowIcon-large')).toHaveAttribute(
      'aria-hidden',
      'true',
    );
  });

  it('should show acknowledged external event with internal issued notifications correctly', () => {
    const props = {
      event: mockEventAcknowledgedExternal,
      onNotificationRowClick: jest.fn(),
    };
    render(
      <TestWrapper>
        <NotificationRow {...props} />
      </TestWrapper>,
    );

    // Show right event category and level
    expect(
      screen.getByText(
        `${
          mockEventAcknowledgedExternal.lifecycles!.externalprovider!.eventlevel
        } ${
          EventCategory[
            mockEventAcknowledgedExternal.category as keyof typeof EventCategory
          ]
        }: ${dateUtils.dateToString(
          dateUtils.utc(
            mockEventAcknowledgedExternal.lifecycles!.externalprovider!
              .lastissuetime!,
          ),
          `${dateUtils.DATE_FORMAT_DATEPICKER}`,
        )!}`,
      ),
    ).toBeTruthy();

    // Show the right tag
    expect(
      screen.getByTestId('notificationRow-externalStatusTag').textContent,
    ).toEqual('Alert');

    // Not show the new notification icon
    expect(screen.queryByTestId('newNotificationRowIcon')).toBeFalsy();

    // internal last issue time
    expect(
      screen.getByTestId('notificationRow-internalLastIssueTime').textContent,
    ).toEqual(
      dateUtils.dateToString(
        dateUtils.utc(
          mockEventAcknowledgedExternal.lifecycles!.internalprovider!
            .lastissuetime!,
        ),
        `${dateUtils.DATE_FORMAT_DATEPICKER}`,
      ),
    );

    // no edit draft button to be present
    expect(screen.queryByTestId('notificationRow-draft')).toBeFalsy();

    // internal notification tag should be present
    expect(
      screen.getByTestId('notificationRow-internalStatusTagDraft').textContent,
    ).toEqual('Alert');
  });

  it('should show draft notification correctly', () => {
    const props = {
      event: mockEventAcknowledgedExternalDraft,
      onNotificationRowClick: jest.fn(),
    };
    render(
      <TestWrapper>
        <NotificationRow {...props} />
      </TestWrapper>,
    );

    // Show right event category and no level abd last issue time (external event)

    const date = dateUtils.dateToString(
      dateUtils.utc(
        mockEventAcknowledgedExternal.lifecycles!.externalprovider!
          .lastissuetime!,
      ),
      `${dateUtils.DATE_FORMAT_DATEPICKER}`,
    )!;
    expect(
      screen.getByText(
        `${
          EventCategory[
            mockEventAcknowledgedExternalDraft.category as keyof typeof EventCategory
          ]
        }: ${date}`,
      ),
    ).toBeTruthy();

    // Show the right tag
    expect(
      screen.getByTestId('notificationRow-externalStatusTag').textContent,
    ).toEqual('Alert');

    // Not show the new notification icon
    expect(screen.queryByTestId('newNotificationRowIcon')).toBeFalsy();

    // edit draft button to be present
    expect(screen.getByTestId('notificationRow-draft')).toBeTruthy();

    // no internal notification tag should be present
    expect(
      screen.queryByTestId('notificationRow-internalStatusTagDraft')
        ?.textContent,
    ).toBe('Edit');
  });
});
