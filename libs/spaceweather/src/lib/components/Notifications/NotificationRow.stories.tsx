/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Meta, StoryObj } from '@storybook/react/*';
import NotificationRow from './NotificationRow';

const fakeEvent = {
  eventid: 'METRB1',
  category: 'ELECTRON_FLUX',
  categorydetail: 'ELECTRON_FLUX_2',
  originator: 'METOffice',
  lifecycles: {
    externalprovider: {
      eventid: 'METRB1',
      owner: 'METOffice',
      label: 'ALERT',
      eventstart: '2020-07-13 13:00',
      eventend: '2020-07-13 16:12',
      firstissuetime: '2020-07-13 12:00',
      lastissuetime: '2020-07-13 12:00',
      state: 'ended',
      canbe: [],
    },
    internalprovider: {
      eventid: 'METRB1',
      owner: 'KNMI',
      label: 'ALERT',
      eventstart: '2020-07-13 13:00',
      eventend: '2020-07-13 16:12',
      firstissuetime: '2020-07-13 12:10',
      lastissuetime: '2020-07-13 12:10',
      state: 'ended',
      canbe: [],
    },
  },
};

const fakeCancelledEvent = {
  eventid: 'METRB2',
  category: 'XRAY_RADIO_BLACKOUT',
  categorydetail: '',
  originator: 'METOffice',
  notacknowledged: false,
  lifecycles: {
    externalprovider: {
      eventid: 'METRB2',
      owner: 'METOffice',
      label: 'WARNING',
      eventstart: '2020-07-13 13:00',
      eventend: '2020-07-13 16:12',
      firstissuetime: '2020-07-13 12:00',
      lastissuetime: '2020-07-13 12:00',
      state: 'ended',
      canbe: [],
    },
    internalprovider: {
      eventid: 'METRB2',
      owner: 'KNMI',
      label: 'WARNING',
      eventstart: '2020-07-13 13:00',
      eventend: '',
      firstissuetime: '2020-07-13 12:10',
      lastissuetime: '2020-07-13 12:10',
      state: 'issued',
      canbe: [],
    },
  },
};

const fakeEventWithBell = {
  eventid: 'METRB3',
  category: 'XRAY_RADIO_BLACKOUT',
  categorydetail: '',
  originator: 'METOffice',
  notacknowledged: true,
  lifecycles: {
    externalprovider: {
      eventid: 'METRB3',
      owner: 'METOffice',
      label: 'ALERT',
      eventstart: '2020-07-13 13:00',
      eventend: '',
      firstissuetime: '2020-07-13 12:00',
      lastissuetime: '2020-07-13 12:00',
      state: 'issued',
      canbe: [],
    },
  },
};

const fakeEventWithDraft = {
  eventid: 'METRB4',
  category: 'XRAY_RADIO_BLACKOUT',
  categorydetail: '',
  originator: 'METOffice',
  lifecycles: {
    externalprovider: {
      eventid: 'METRB4',
      owner: 'METOffice',
      label: 'ALERT',
      eventstart: '2020-07-13 13:00',
      eventend: '',
      firstissuetime: '2020-07-13 12:00',
      lastissuetime: '2020-07-13 12:00',
      state: 'issued',
      canbe: [],
    },
    internalprovider: {
      draft: true,
      eventid: 'METRB4',
      owner: 'KNMI',
      label: 'ALERT',
      eventstart: '2020-07-13 13:00',
      eventend: '',
      firstissuetime: '2020-07-13 12:10',
      lastissuetime: '2020-07-13 12:10',
      state: 'draft',
      canbe: [],
    },
  },
};

const meta: Meta<typeof NotificationRow> = {
  title: 'components/Notification Section/NotificationRow',
  component: NotificationRow,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the NotificationRow',
      },
    },
  },
  argTypes: {
    event: {
      control: 'select',
      options: ['summary', 'cancel', 'alert', 'draft'],
      description: 'Size of icon, can be small or large',
      mapping: {
        summary: fakeEvent,
        cancel: fakeCancelledEvent,
        alert: fakeEventWithBell,
        draft: fakeEventWithDraft,
      },
    },
  },
};
export default meta;
type Story = StoryObj<typeof NotificationRow>;

export const Component: Story = {
  args: {
    event: fakeEvent,
    onNotificationRowClick: () => {},
  },
};

export const NotificationRowDummyDemo = (): React.ReactElement => {
  return (
    <>
      <NotificationRow
        event={fakeEvent}
        onNotificationRowClick={(): void => {
          // eslint-disable-next-line no-console
          console.log('hello');
        }}
      />
      <NotificationRow
        event={fakeCancelledEvent}
        onNotificationRowClick={(): void => {
          // eslint-disable-next-line no-console
          console.log('hello cancel');
        }}
      />
      <NotificationRow
        event={fakeEventWithBell}
        onNotificationRowClick={(): void => {
          // eslint-disable-next-line no-console
          console.log('hello bell');
        }}
      />
      <NotificationRow
        event={fakeEventWithDraft}
        onNotificationRowClick={(): void => {
          // eslint-disable-next-line no-console
          console.log('hello draft');
        }}
      />
    </>
  );
};
