/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';

import Notifications from './Notifications';
import { TestWrapper } from '../../utils/testUtils';
import { fakeEventList } from '../../utils/fakedata';

describe('src/components/Notifications/Notifications', () => {
  it('should retrieve the eventlist on load', async () => {
    render(
      <TestWrapper>
        <Notifications />
      </TestWrapper>,
    );

    await waitFor(() =>
      expect(
        screen.queryAllByTestId('notificationRow-listitem').length,
      ).toEqual(fakeEventList.length),
    );
  });

  it('should set the selected tab when a new tab header is clicked and load the right list', async () => {
    render(
      <TestWrapper>
        <Notifications />
      </TestWrapper>,
    );

    // Should first load the whole list
    await waitFor(() =>
      expect(
        screen.queryAllByTestId('notificationRow-listitem').length,
      ).toEqual(fakeEventList.length),
    );

    const KNMITab = screen.getByTestId('newNotificationTabKNMI');
    fireEvent.click(KNMITab);

    // Should only load events with originator KNMI and have KNMI tab selected
    await waitFor(() => {
      expect(screen.getByTestId('newNotificationTabKNMI').classList).toContain(
        'Mui-selected',
      );
    });
    await waitFor(
      () => {
        expect(
          screen.queryAllByTestId('notificationRow-listitem').length,
        ).toEqual(6);
      },
      { timeout: 3000 },
    );

    const ALLTab = screen.getByTestId('newNotificationTabAll');
    fireEvent.click(ALLTab);

    // Should load all events and have all tab selected
    await waitFor(() => {
      expect(screen.getByTestId('newNotificationTabAll').classList).toContain(
        'Mui-selected',
      );
    });

    await waitFor(() => {
      expect(
        screen.queryAllByTestId('notificationRow-listitem').length,
      ).toEqual(fakeEventList.length);
    });
  });

  it('should open dialog when eventrow is clicked', async () => {
    render(
      <TestWrapper>
        <Notifications />
      </TestWrapper>,
    );
    expect(screen.queryByTestId('lifecycle-dialog')).toBeFalsy();
    await waitFor(() =>
      expect(
        screen.queryAllByTestId('notificationRow-listitem').length,
      ).toEqual(fakeEventList.length),
    );
    fireEvent.click(screen.getAllByTestId('notificationRow-listitem')[1]);
    expect(screen.getByTestId('lifecycle-dialog')).toBeTruthy();
  });

  it('should open dialog when clicking the new notification button', async () => {
    render(
      <TestWrapper>
        <Notifications />
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('notifications-newnotification'));
    await screen.findByTestId('lifecycle-dialog');
  });
});
