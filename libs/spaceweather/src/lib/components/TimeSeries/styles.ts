/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

export const curveColors = {
  kpIndex: '#417505',
  kpIndexForecast: '#8db04e',
  magneticFieldBt: '#d3d3d3',
  magneticFieldBz: '#e28f95',
  lineSolarXray: '#f29df1',
  lineSolarWindDensity: '#f73f49',
  lineSolarWindSpeed: '#f9ba52',
  areaSolarWindSpeed: '#feffe3',
  lineSolarWindPressure: '#9795ff',
  areaSolarWindPressure: '#9795ff',
  markLines: '#979797',
  currentTime: '#186dff',
};
