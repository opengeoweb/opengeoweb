/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { BulletinTabs } from './BulletinTabs';
import { BulletinType } from '../../types';
import { TestWrapper } from '../../utils/testUtils';

describe('src/components/Bulletin/BulletinTabs', () => {
  it('should have selected technical tab if that is the one selected', () => {
    const props = {
      selected: BulletinType.technical,
      onSetBulletinType: jest.fn(),
    };
    render(
      <TestWrapper>
        <BulletinTabs {...props} />
      </TestWrapper>,
    );
    expect(screen.getByTestId('technical').getAttribute('class')).toContain(
      'Mui-selected',
    );
  });

  it('update to plain if plain tab selected', () => {
    const props = {
      selected: BulletinType.technical,
      onSetBulletinType: jest.fn(),
    };
    render(
      <TestWrapper>
        <BulletinTabs {...props} />
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('plain'));

    expect(props.onSetBulletinType).toHaveBeenCalledWith('PLAIN');
  });

  it('updates to plain if technical tab selected', () => {
    const props = {
      selected: BulletinType.technical,
      onSetBulletinType: jest.fn(),
    };
    const { rerender } = render(<BulletinTabs {...props} />);

    fireEvent.click(screen.getByTestId('plain'));
    expect(props.onSetBulletinType).toHaveBeenCalledWith('PLAIN');

    // rerender with new selected value to mock state change
    rerender(
      <TestWrapper>
        <BulletinTabs {...props} selected={BulletinType.plain} />
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('technical'));
    expect(props.onSetBulletinType).toHaveBeenCalledWith('TECHNICAL');
  });
});
