/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { Box } from '@mui/material';

import type { Meta, StoryObj } from '@storybook/react';
import { Bulletin } from '.';

import { requestHandlers } from '../../utils/spaceweather-api/fakeApi';

const meta: Meta<typeof Bulletin> = {
  title: 'components/Bulletin Section',
  component: Bulletin,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the Bulletin',
      },
    },
    msw: {
      handlers: {
        spaceweather: requestHandlers,
      },
    },
  },
  render: (props) => (
    <div style={{ paddingBottom: '30px' }}>
      <Bulletin {...props} />
    </div>
  ),
};
export default meta;

type Story = StoryObj<typeof Bulletin>;

export const Component: Story = {
  tags: ['!dev'],
};

export const BulletinStory: Story = {
  render: () => (
    <Box
      sx={{
        width: '800px',
        padding: '16px',
        display: 'flex',
        backgroundColor: 'geowebColors.background.surface',
      }}
    >
      <Bulletin />
    </Box>
  ),
  tags: ['snapshot'],
};

BulletinStory.storyName = 'Bulletin Section';

export const BulletinDarkStory: Story = {
  ...BulletinStory,
  tags: ['snapshot', 'dark'],
};

BulletinDarkStory.storyName = 'Bulletin Section Dark';
