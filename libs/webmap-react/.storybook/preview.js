/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { darkTheme, lightTheme } from '@opengeoweb/theme';
import * as previewHelpers from '../../../.storybook/preview';
import '../../../libs/theme/src/lib/components/Theme/styles.css';
import { WebmapReactThemeProvider } from '../src/lib/components/Providers';
import { wmsQueryClient } from '@opengeoweb/webmap';
import { QueryClientProvider } from '@tanstack/react-query';

export const decorators = [
  (Story, params) => {
    const theme = params.tags.indexOf('dark') !== -1 ? darkTheme : lightTheme;
    return (
      <WebmapReactThemeProvider theme={theme}>
        <QueryClientProvider client={wmsQueryClient}>
          <Story />
        </QueryClientProvider>
      </WebmapReactThemeProvider>
    );
  },
];

const parameters = {
  ...previewHelpers.parameters,
  tags: ['autodocs'],
};
export { parameters };
export default parameters;
