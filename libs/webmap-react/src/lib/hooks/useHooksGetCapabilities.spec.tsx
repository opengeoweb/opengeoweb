/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React, { act } from 'react';
import { QueryClientProvider } from '@tanstack/react-query';
import {
  setWMSGetCapabilitiesFetcher,
  mockGetCapabilities,
  getWMLayerById,
  webmapUtils,
  wmsQueryClient,
} from '@opengeoweb/webmap';
import { renderHook, waitFor } from '@testing-library/react';
import {
  useGetWMLayerInstance,
  useGetWMSLayerStyleList,
  useQueryGetWMSGetCapabilities,
  useQueryGetWMSLayer,
  useQueryGetWMSLayers,
  useQueryGetWMSLayersTree,
  useQueryGetWMSServiceInfo,
} from './useHooksGetCapabilities';

describe('useHooksGetCapabilities.spec', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });
  describe('useGetWMLayerInstance', () => {
    it('should return same WMLayer instance when using same arguments and id again', async () => {
      const layer = {
        service: mockGetCapabilities.MOCK_URL_HARMONIE,
        id: 'multiDimensionLayerMock',
        name: 'dew_point_temperature__at_2m',
      };
      const { result: resultA } = renderHook(() =>
        useGetWMLayerInstance(layer.service, layer.name, layer.id),
      );
      const { result: resultB } = renderHook(() =>
        useGetWMLayerInstance(layer.service, layer.name, layer.id),
      );
      await act(async () => {
        expect(resultA).toBeTruthy();
      });
      await act(async () => {
        expect(resultB).toBeTruthy();
      });

      expect(resultA.current).toBe(resultB.current);
    });

    it('should return same WMLayer instance when only name changes', async () => {
      const layer = {
        service: mockGetCapabilities.MOCK_URL_HARMONIE,
        id: 'multiDimensionLayerMock',
      };
      const { result: resultA } = renderHook(() =>
        useGetWMLayerInstance(
          layer.service,
          'dew_point_temperature__at_2m',
          layer.id,
        ),
      );
      const { result: resultB } = renderHook(() =>
        useGetWMLayerInstance(
          layer.service,
          'air_temperature__at_2m',
          layer.id,
        ),
      );
      await act(async () => {
        expect(resultA).toBeTruthy();
      });
      await act(async () => {
        expect(resultB).toBeTruthy();
      });

      expect(resultA.current).toBe(resultB.current);
    });

    it('should return new WMLayer instance when service changes', async () => {
      const layer = {
        id: 'multiDimensionLayerMock',
        name: 'dew_point_temperature__at_2m',
      };
      const { result: resultA } = renderHook(() =>
        useGetWMLayerInstance(
          mockGetCapabilities.MOCK_URL_HARMONIE,
          'dew_point_temperature__at_2m',
          layer.id,
        ),
      );
      const { result: resultB } = renderHook(() =>
        useGetWMLayerInstance(
          'WMS111GetCapabilitiesGeoServicesRADAR',
          'air_temperature__at_2m',
          layer.id,
        ),
      );
      await act(async () => {
        expect(resultA).toBeTruthy();
      });
      await act(async () => {
        expect(resultB).toBeTruthy();
      });

      expect(resultA.current).not.toBe(resultB.current);

      expect(resultA.current?.name).toBe('dew_point_temperature__at_2m');
      expect(resultA.current?.service).toBe(
        mockGetCapabilities.MOCK_URL_HARMONIE,
      );

      expect(resultB.current?.name).toBe('air_temperature__at_2m');
      expect(resultB.current?.service).toBe(
        'WMS111GetCapabilitiesGeoServicesRADAR',
      );
      expect(resultA.current?.isConfigured).toBeTruthy();
      expect(resultB.current?.isConfigured).toBeFalsy();
      expect(resultA.current?.hasError).toBeFalsy();
      expect(resultB.current?.hasError).toBeTruthy();
    });

    it('should not return same WMLayer instance when no id is provided', async () => {
      const layer = {
        service: mockGetCapabilities.MOCK_URL_HARMONIE,
        name: 'dew_point_temperature__at_2m',
      };
      const { result: resultA } = renderHook(() =>
        useGetWMLayerInstance(layer.service, layer.name),
      );
      const { result: resultB } = renderHook(() =>
        useGetWMLayerInstance(layer.service, layer.name),
      );
      await act(async () => {
        expect(resultA).toBeTruthy();
      });
      await act(async () => {
        expect(resultB).toBeTruthy();
      });

      expect(resultA.current).not.toBe(resultB.current);
    });

    it('should generate an id if not provided', async () => {
      const layer = {
        service: mockGetCapabilities.MOCK_URL_HARMONIE,
        name: 'dew_point_temperature__at_2m',
      };
      jest
        .spyOn(webmapUtils, 'generateLayerId')
        .mockReturnValueOnce('layerid_usegetwmlayerinstance_1');

      const { result: resultA } = renderHook(() =>
        useGetWMLayerInstance(layer.service, layer.name),
      );
      await act(async () => {
        expect(resultA).toBeTruthy();
      });
      expect(resultA.current?.id).toBe('layerid_usegetwmlayerinstance_1');
    });

    it('should return WMLayer instance when mounted', async () => {
      const layer = {
        service: mockGetCapabilities.MOCK_URL_HARMONIE,
        id: 'multiDimensionLayerMock',
        name: 'dew_point_temperature__at_2m',
      };
      const { result } = renderHook(() =>
        useGetWMLayerInstance(layer.service, layer.name, layer.id),
      );
      await act(async () => {
        expect(result).toBeTruthy();
      });

      const { result: result2 } = renderHook(() =>
        useGetWMLayerInstance(layer.service, layer.name, layer.id),
      );
      await act(async () => {
        expect(result2).toBeTruthy();
      });

      const wmLayerInstance = result.current;
      expect(getWMLayerById('multiDimensionLayerMock')).toBe(result2.current);
      expect(getWMLayerById('multiDimensionLayerMock')).toBe(wmLayerInstance);
      expect(wmLayerInstance?.id).toBe('multiDimensionLayerMock');
      expect(wmLayerInstance?.title).toBe('Dew Point 2m');
      expect(wmLayerInstance?.name).toBe('dew_point_temperature__at_2m');

      const timeDim = wmLayerInstance?.getDimension('time');
      expect(timeDim?.getValue()).toBe('2021-05-17T03:00:00Z');
      expect(timeDim?.getFirstValue()).toBe('2021-05-17T03:00:00Z');
      expect(timeDim?.getLastValue()).toBe('2021-05-19T03:00:00Z');

      const refTimeDim = wmLayerInstance?.getDimension('reference_time');
      expect(refTimeDim?.getValue()).toBe('2021-05-17T03:00:00Z');
      expect(refTimeDim?.getFirstValue()).toBe('2021-05-10T00:00:00Z');
      expect(refTimeDim?.getLastValue()).toBe('2021-05-17T03:00:00Z');
    });
  });

  describe('useQueryGetWMSGetCapabilities', () => {
    it('Should return getCapabilities document', async () => {
      const wrapper = ({
        children,
      }: React.PropsWithChildren): React.ReactElement => (
        <QueryClientProvider client={wmsQueryClient}>
          {children}
        </QueryClientProvider>
      );
      const { result } = renderHook(
        () =>
          useQueryGetWMSGetCapabilities(mockGetCapabilities.MOCK_URL_HARMONIE),
        { wrapper },
      );
      await act(async () => {
        expect(result).toBeTruthy();
      });

      await waitFor(() => expect(result.current.isFetching).toBeFalsy());

      expect(result.current.data?.WMS_Capabilities).toBeDefined();
    });
  });

  describe('useQueryGetWMSServiceInfo', () => {
    it('Should return WMS serviceInfo document', async () => {
      const wrapper = ({
        children,
      }: React.PropsWithChildren): React.ReactElement => (
        <QueryClientProvider client={wmsQueryClient}>
          {children}
        </QueryClientProvider>
      );
      const { result } = renderHook(
        () => useQueryGetWMSServiceInfo(mockGetCapabilities.MOCK_URL_HARMONIE),
        { wrapper },
      );
      await act(async () => {
        expect(result).toBeTruthy();
      });
      await waitFor(() => expect(result?.current?.title).toBeDefined());
      expect(result?.current?.title).toBe('HARM_N25');
    });
  });

  describe('useQueryGetWMSLayersTree', () => {
    it('Should return WMS layertree document', async () => {
      const wrapper = ({
        children,
      }: React.PropsWithChildren): React.ReactElement => (
        <QueryClientProvider client={wmsQueryClient}>
          {children}
        </QueryClientProvider>
      );
      const { result } = renderHook(
        () => useQueryGetWMSLayersTree(mockGetCapabilities.MOCK_URL_HARMONIE),
        { wrapper },
      );
      await act(async () => {
        expect(result).toBeTruthy();
      });
      await waitFor(() => expect(result?.current?.title).toBeDefined());
      expect(result?.current?.title).toBe('WMS of  HARM_N25');
      // eslint-disable-next-line testing-library/no-node-access
      expect(result?.current?.children).toHaveLength(21);
    });
  });

  describe('useQueryGetWMSLayers', () => {
    it('Should return flat list of layers from service', async () => {
      const wrapper = ({
        children,
      }: React.PropsWithChildren): React.ReactElement => (
        <QueryClientProvider client={wmsQueryClient}>
          {children}
        </QueryClientProvider>
      );
      const { result } = renderHook(
        () => useQueryGetWMSLayers(mockGetCapabilities.MOCK_URL_HARMONIE),
        { wrapper },
      );
      await act(async () => {
        expect(result).toBeTruthy();
      });
      await waitFor(() => expect(result?.current).toBeDefined());

      expect(result?.current?.length).toBe(21);
    });
  });

  describe('useQueryGetWMSLayer', () => {
    it('Should return one layer from service selected by its name', async () => {
      const wrapper = ({
        children,
      }: React.PropsWithChildren): React.ReactElement => (
        <QueryClientProvider client={wmsQueryClient}>
          {children}
        </QueryClientProvider>
      );
      const { result } = renderHook(
        () =>
          useQueryGetWMSLayer(
            mockGetCapabilities.MOCK_URL_HARMONIE,
            'dew_point_temperature__at_2m',
          ),
        { wrapper },
      );
      await act(async () => {
        expect(result).toBeTruthy();
      });
      await waitFor(() => expect(result?.current).toBeDefined());

      expect(result?.current?.title).toBe('Dew Point 2m');
    });
  });

  describe('useGetWMSLayerStyleList', () => {
    it('Should return stylelist for selected layer from the service', async () => {
      const wrapper = ({
        children,
      }: React.PropsWithChildren): React.ReactElement => (
        <QueryClientProvider client={wmsQueryClient}>
          {children}
        </QueryClientProvider>
      );
      const { result } = renderHook(
        () =>
          useGetWMSLayerStyleList(
            mockGetCapabilities.MOCK_URL_HARMONIE,
            'dew_point_temperature__at_2m',
          ),
        { wrapper },
      );
      await act(async () => {
        expect(result).toBeTruthy();
      });
      await waitFor(() => expect(result?.current).toBeDefined());

      expect(result?.current).toEqual([
        {
          abstract: 'Contours with shaded background',
          legendURL:
            'https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=dew_point_temperature__at_2m&format=image/png&STYLE=temperature/shadedcontour',
          name: 'temperature/shadedcontour',
          title: 'Temperature shaded + contours',
        },
        {
          abstract: 'Contours with shaded background',
          legendURL:
            'https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=dew_point_temperature__at_2m&format=image/png&STYLE=temperature-hi/shadedcontour',
          name: 'temperature-hi/shadedcontour',
          title: 'Temperature (Hi) shaded + contours',
        },
        {
          abstract:
            'Automatic min/max rendered with nearest neighbour interpolation',
          legendURL:
            'https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=dew_point_temperature__at_2m&format=image/png&STYLE=auto/nearest',
          name: 'auto/nearest',
          title: 'Automatic minimum and maximum',
        },
        {
          abstract: 'Automatic min/max rendered with bilinear interpolation',
          legendURL:
            'https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=dew_point_temperature__at_2m&format=image/png&STYLE=auto/bilinear',
          name: 'auto/bilinear',
          title: 'Automatic minimum and maximum bilinear',
        },
      ]);
    });
  });
});
