/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import i18n from 'i18next';
import {
  UseTranslationResponse,
  initReactI18next,
  useTranslation,
} from 'react-i18next';
import { WEBMAP_NAMESPACE, webmapTranslations } from '@opengeoweb/webmap';
import webmapReactTranslations from '../../../locales/webmap-react.json';

export const WEBMAP_REACT_NAMESPACE = 'webmapreact';

export const initWebmapReactTestI18n = (): void => {
  void i18n.use(initReactI18next).init({
    lng: 'en',
    ns: WEBMAP_REACT_NAMESPACE,
    interpolation: {
      escapeValue: false,
    },
    resources: {
      en: {
        [WEBMAP_REACT_NAMESPACE]: webmapReactTranslations.en,
        [WEBMAP_NAMESPACE]: webmapTranslations.en,
      },
      fi: {
        [WEBMAP_REACT_NAMESPACE]: webmapReactTranslations.fi,
        [WEBMAP_NAMESPACE]: webmapTranslations.fi,
      },
      no: {
        [WEBMAP_REACT_NAMESPACE]: webmapReactTranslations.no,
        [WEBMAP_NAMESPACE]: webmapTranslations.no,
      },
      nl: {
        [WEBMAP_REACT_NAMESPACE]: webmapReactTranslations.nl,
        [WEBMAP_NAMESPACE]: webmapTranslations.nl,
      },
    },
  });
};

export const translateKeyOutsideComponents = (
  key: string,
  params: Record<string, string | number> | undefined = undefined,
): string => i18n.t(key, { ns: WEBMAP_REACT_NAMESPACE, ...params });

export const useWebmapReactTranslation = (): UseTranslationResponse<
  typeof WEBMAP_REACT_NAMESPACE,
  typeof i18n
> => useTranslation(WEBMAP_REACT_NAMESPACE);

export { i18n };
