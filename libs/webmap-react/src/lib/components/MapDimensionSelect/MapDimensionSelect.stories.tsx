/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { getWMJSMapById, getWMLayerById } from '@opengeoweb/webmap';
import type { StoryObj } from '@storybook/react';
import { MapView, MapViewLayer } from '../MapView';
import { publicLayers, defaultLayers } from '../../layers';
import MapDimensionSelect, {
  MapDimensionSelectProps,
} from './MapDimensionSelect';
import DimensionSelectDialog from './DimensionSelectDialog';

export default {
  title: 'components/MapDimensionSelect',
  component: MapView,
};

const mapId = 'map-1';
const layerId = 'layer-1';
const multiDimensionLayer = {
  ...publicLayers.harmonieRelativeHumidityPl,
  id: layerId,
  dimensions: [
    {
      name: 'pressure_level_in_hpa',
      units: 'hPa',
      currentValue: undefined!,
      synced: false,
      validSyncSelection: true,
    },
  ],
};

const getMapDimensionSelectProps = (index = 0): MapDimensionSelectProps => ({
  layerId,
  dimensionName: multiDimensionLayer.dimensions[index].name,
  layerDimension: multiDimensionLayer.dimensions[index],
  handleDimensionValueChanged: (layerId, dimensionName, value): void => {
    getWMLayerById(layerId).setDimension(dimensionName, value);
    getWMJSMapById(mapId)?.draw();
  },
  handleSyncChanged: (): void => {},
});

const Demo = (): React.ReactElement => {
  const [isLayerReady, setLayerReady] = React.useState(false);
  const [props, setProps] = React.useState<MapDimensionSelectProps | undefined>(
    undefined,
  );

  return (
    <>
      <div style={{ height: '100vh' }}>
        <MapView mapId={mapId}>
          <MapViewLayer {...publicLayers.baseLayer} />
          <MapViewLayer {...defaultLayers.overLayer} />
          <MapViewLayer
            {...publicLayers.harmonieRelativeHumidityPl}
            onLayerReady={(layer) => {
              setLayerReady(true);
              const newProps = {
                ...getMapDimensionSelectProps(),
                layerId: layer.id,
              };

              setProps(newProps);
            }}
          />
        </MapView>
      </div>
      <div
        style={{
          position: 'absolute',
          top: '20px',
          left: '50px',
          zIndex: 1000,
        }}
      >
        {isLayerReady && props && (
          <DimensionSelectDialog
            isOpen
            dimensionName={multiDimensionLayer.name}
          >
            <MapDimensionSelect {...props} />
          </DimensionSelectDialog>
        )}
      </div>
    </>
  );
};

type Story = StoryObj<typeof MapDimensionSelect>;

export const MapDimensionOnMap: Story = {
  render: Demo,
};
