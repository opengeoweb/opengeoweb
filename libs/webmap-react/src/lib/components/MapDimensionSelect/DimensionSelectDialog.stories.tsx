/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { LayerType, WMLayer, webmapUtils } from '@opengeoweb/webmap';

import type { Meta, StoryObj } from '@storybook/react';
import { StorybookDocsWrapper } from '@opengeoweb/shared';
import DimensionSelectDialog, {
  DimensionSelectProps,
} from './DimensionSelectDialog';
import MapDimensionSelect, {
  MapDimensionSelectProps,
} from './MapDimensionSelect';

const layerId = 'layer-1';
const multiDimensionLayer = {
  service: 'https://testservice',
  id: layerId,
  name: 'air_temperature__at_pl',
  title: 'air_temperature__at_pl',
  layerType: LayerType.mapLayer,
  enabled: true,
  dimensions: [
    {
      name: 'elevation',
      units: 'hPa',
      currentValue: '850',
      values: '850,925',
      synced: false,
      validSyncSelection: true,
    },
    {
      name: 'ensemble_member',
      units: 'member',
      currentValue: '7',
      values:
        '0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29',
      synced: true,
      validSyncSelection: true,
    },
    {
      name: 'modellevel',
      units: '',
      currentValue: '',
      values: '_0_,_1_,_2_',
      synced: true,
      validSyncSelection: false,
    },
  ],
};

const wmMultiDimensionLayer = new WMLayer(multiDimensionLayer);
webmapUtils.registerWMLayer(wmMultiDimensionLayer, multiDimensionLayer.id);

const getDimensionSelectProps = (index: number): DimensionSelectProps => ({
  isOpen: true,
  dimensionName: multiDimensionLayer.dimensions[index].name,
});

const getDimensionSelectWrapperProps = (
  index: number,
): MapDimensionSelectProps => ({
  layerId,
  dimensionName: multiDimensionLayer.dimensions[index].name,
  layerDimension: multiDimensionLayer.dimensions[index],
  handleDimensionValueChanged: (): void => {},
  handleSyncChanged: (): void => {},
});

const meta: Meta<typeof MapDimensionSelect> = {
  title: 'components/MapDimensionSelect',
  component: MapDimensionSelect,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the user MapDimensionSelect',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof MapDimensionSelect>;

export const Component: Story = {
  args: getDimensionSelectWrapperProps(0),
  render: (props) => (
    <div>
      <MapDimensionSelect {...props} />
    </div>
  ),
};

export const ComponentDark: Story = {
  args: getDimensionSelectWrapperProps(0),
  render: (props) => (
    <StorybookDocsWrapper isDark>
      <MapDimensionSelect {...props} />
    </StorybookDocsWrapper>
  ),
  tags: ['dark'],
};

// elevation
const elevationProps = getDimensionSelectProps(0);
const elevationWrapperProps = getDimensionSelectWrapperProps(0);

export const DimensionSelectElevationLight: Story = {
  args: elevationWrapperProps,
  render: (props) => (
    <DimensionSelectDialog {...elevationProps}>
      <MapDimensionSelect {...props} />
    </DimensionSelectDialog>
  ),
  tags: ['snapshot', '!autodocs'],
};

export const DimensionSelectElevationDark: Story = {
  args: elevationWrapperProps,
  render: (props) => (
    <DimensionSelectDialog {...elevationProps}>
      <MapDimensionSelect {...props} />
    </DimensionSelectDialog>
  ),
  tags: ['snapshot', 'dark', '!autodocs'],
};

// ensemble
const ensembleMemberProps = getDimensionSelectProps(1);
const ensembleMemberWrapperProps = getDimensionSelectWrapperProps(1);

export const DimensionSelectEnsembleMemberLight: Story = {
  args: ensembleMemberWrapperProps,
  render: (props) => (
    <DimensionSelectDialog {...ensembleMemberProps}>
      <MapDimensionSelect {...props} />
    </DimensionSelectDialog>
  ),
  tags: ['snapshot', '!autodocs'],
};

export const DimensionSelectEnsembleMemberDark: Story = {
  args: ensembleMemberWrapperProps,
  render: (props) => (
    <DimensionSelectDialog {...ensembleMemberProps}>
      <MapDimensionSelect {...props} />
    </DimensionSelectDialog>
  ),
  tags: ['snapshot', 'dark', '!autodocs'],
};

// invalid custom
const customProps = getDimensionSelectProps(2);
const customWrapperProps = getDimensionSelectWrapperProps(2);

export const DimensionSelectInvalidSyncLight: Story = {
  args: customWrapperProps,
  render: (props) => (
    <DimensionSelectDialog {...customProps}>
      <MapDimensionSelect {...props} />
    </DimensionSelectDialog>
  ),
  tags: ['snapshot', '!autodocs'],
};

export const DimensionSelectInvalidSyncDark: Story = {
  args: customWrapperProps,
  render: (props) => (
    <DimensionSelectDialog {...customProps}>
      <MapDimensionSelect {...props} />
    </DimensionSelectDialog>
  ),
  tags: ['snapshot', 'dark', '!autodocs'],
};
