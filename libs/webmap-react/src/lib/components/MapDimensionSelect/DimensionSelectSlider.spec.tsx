/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

import DimensionSelectSlider from './DimensionSelectSlider';

const mockProps = {
  marks: [
    {
      value: 1000,
      label: '1000',
    },
    {
      value: 5000,
      label: '5000',
    },
    {
      value: 9000,
      label: '9000',
    },
  ],
  layerTitle: 'elevation',
  onChangeDimensionValue: jest.fn(),
};

describe('src/components/MultiMapDimensionSelect/DimensionSelectSlider', () => {
  it('should render a slider', () => {
    render(<DimensionSelectSlider {...mockProps} />);
    expect(screen.getByRole('slider')).toBeTruthy();

    // slider should be enabled
    const slider = screen.getByTestId('verticalSlider');
    expect(slider.classList.contains('Mui-disabled')).toBeFalsy();
  });

  it('should render the provided marks in an ascending order', () => {
    const { baseElement } = render(<DimensionSelectSlider {...mockProps} />);
    // eslint-disable-next-line testing-library/no-node-access
    const markers = baseElement.getElementsByClassName('MuiSlider-markLabel');
    expect(markers[0].innerHTML).toBe('1000');
    expect(markers[2].innerHTML).toBe('9000');
  });

  it('should render the provided marks in a descending (reverse) order', () => {
    const { baseElement } = render(
      <DimensionSelectSlider {...mockProps} reverse />,
    );
    // eslint-disable-next-line testing-library/no-node-access
    const markers = baseElement.getElementsByClassName('MuiSlider-markLabel');
    expect(markers[0].innerHTML).toBe('9000');
    expect(markers[2].innerHTML).toBe('1000');
  });

  it('should set the slider to first value by default', () => {
    const { baseElement } = render(<DimensionSelectSlider {...mockProps} />);
    // eslint-disable-next-line testing-library/no-node-access
    const activeLabel = baseElement.getElementsByClassName(
      'MuiSlider-markLabelActive',
    );
    expect(activeLabel.length).toBe(1);
    expect(activeLabel[0].textContent === '1000').toBeTruthy();
  });

  it('should not show a slider when there are no marks provided', () => {
    const props = {
      ...mockProps,
      marks: [],
    };
    render(<DimensionSelectSlider {...props} />);
    expect(screen.queryByRole('slider')).toBeFalsy();
  });

  it('should be disabled if isDisabled is true', () => {
    const props = {
      ...mockProps,
      isDisabled: true,
    };
    render(<DimensionSelectSlider {...props} />);

    expect(
      screen.getByTestId('verticalSlider').classList.contains('Mui-disabled'),
    ).toBeTruthy();
  });

  it('should handle string marks', () => {
    const mockPropsString = {
      marks: [
        {
          label: '_000_ ',
          value: '_000_',
        },
        {
          label: '_001_ ',
          value: '_001_',
        },
        {
          label: '_002_ ',
          value: '_002_',
        },
      ],
      layerTitle: 'cloud base',
      onChangeDimensionValue: jest.fn(),
    };

    const { baseElement } = render(
      <DimensionSelectSlider {...mockPropsString} />,
    );
    // eslint-disable-next-line testing-library/no-node-access
    const markers = baseElement.getElementsByClassName('MuiSlider-markLabel');
    expect(markers[0].innerHTML).toBe(mockPropsString.marks[0].label);
    expect(markers[1].innerHTML).toBe(mockPropsString.marks[1].label);
    expect(markers[2].innerHTML).toBe(mockPropsString.marks[2].label);
  });

  it('should not be navigable with left arrow key', async () => {
    render(<DimensionSelectSlider {...mockProps} />);
    const eventPassed = fireEvent.keyDown(screen.getByRole('slider'), {
      key: 'ArrowLeft',
      code: 'ArrowLeft',
    });
    expect(eventPassed).toBe(false);
  });

  it('should not be navigable with right arrow key', async () => {
    render(<DimensionSelectSlider {...mockProps} />);
    const eventPassed = fireEvent.keyDown(screen.getByRole('slider'), {
      key: 'ArrowRight',
      code: 'ArrowRight',
    });
    expect(eventPassed).toBe(false);
  });

  it('should not be navigable while ctrl pressed', async () => {
    render(<DimensionSelectSlider {...mockProps} />);
    const eventPassed = fireEvent.keyDown(screen.getByRole('slider'), {
      key: 'ArrowDown',
      code: 'ArrowDown',
      ctrlKey: true,
    });
    expect(eventPassed).toBe(false);
  });

  it('should not be navigable while meta pressed', async () => {
    render(<DimensionSelectSlider {...mockProps} />);
    const eventPassed = fireEvent.keyDown(screen.getByRole('slider'), {
      key: 'ArrowDown',
      code: 'ArrowDown',
      metaKey: true,
    });
    expect(eventPassed).toBe(false);
  });

  it('should be navigable with down arrow key', async () => {
    render(<DimensionSelectSlider {...mockProps} />);
    const eventPassed = fireEvent.keyDown(screen.getByRole('slider'), {
      code: 'ArrowDown',
    });
    expect(eventPassed).toBe(true);
  });
});
