/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { Grid2 as Grid, Divider } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import { MapDrawToolOptions, useMapDrawTool } from './useMapDrawTool';
import {
  FeatureLayers,
  GeoJSONTextField,
  SelectField,
} from '../MapDraw/storyComponents';
import { StoryLayout } from '../MapDraw/storyComponents/StoryLayout';
import { ToolButton } from '../MapDraw/storyComponents/ToolButton';
import { EditModeButton } from '../MapDraw/storyComponents/EditModeButton';
import { DrawModeExitCallback } from '../MapDraw/MapDraw';
import {
  basicExampleDrawOptions,
  basicExampleMultipleShapeDrawOptions,
  basicExampleMultipleShapeWithValuesDrawOptions,
  fillOptions,
  getToolIcon,
  opacityOptions,
  strokeWidthOptions,
} from './storyExamplesMapDrawTool';
import { getGeoJSONPropertyValue } from './utils';

const meta: Meta<typeof useMapDrawTool> = {
  title: 'components/MapDrawTool',
  tags: ['!autodocs'],
};
export default meta;

type Story = StoryObj<typeof useMapDrawTool>;

interface BasicMapDrawToolProps {
  mapDrawOptions: MapDrawToolOptions;
}

const BasicMapDrawToolComponent: React.FC<BasicMapDrawToolProps> = ({
  mapDrawOptions = {},
}: BasicMapDrawToolProps) => {
  const {
    drawModes,
    isInEditMode,
    geoJSON,
    setGeoJSON,
    changeDrawMode,
    setEditMode,
    featureLayerIndex,
    setFeatureLayerIndex,
    activeTool,
    changeActiveTool,
    layers,
    deactivateTool,
    changeProperties,
    getProperties,
    setActiveTool,
  } = useMapDrawTool(mapDrawOptions);

  const featureLayerGeoJSONProperties = getProperties();
  // use polygon tool shape to get default style fallback when no shapes are drawn
  const defaultPolygonShape = drawModes.find(
    (mode) => mode.value === 'POLYGON',
  );
  // geoJSON properties
  const opacity = getGeoJSONPropertyValue(
    'fill-opacity',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as number;

  const opacityString = (opacity * 100).toString();

  const fill = getGeoJSONPropertyValue(
    'fill',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as string;

  const strokeOpacity = getGeoJSONPropertyValue(
    'stroke-opacity',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as number;

  const strokeOpacityString = (strokeOpacity * 100).toString();

  const strokeColor = getGeoJSONPropertyValue(
    'stroke',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as string;

  const strokeWidth = getGeoJSONPropertyValue(
    'stroke-width',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as string;

  const onExitDrawMode = (reason: DrawModeExitCallback): void => {
    if (
      reason === 'escaped' ||
      (reason === 'doubleClicked' && !mapDrawOptions.shouldAllowMultipleShapes)
    ) {
      deactivateTool();
    }
  };

  const onChangeFeatureLayer = (
    featureNr: number,
    newFeature: GeoJSON.Feature,
  ): void => {
    setFeatureLayerIndex(featureNr);
    setEditMode(true);

    const newSelectionType = newFeature.properties?.selectionType;
    const newDrawTool = drawModes.find(
      (mode) => mode.selectionType === newSelectionType,
    );
    if (newDrawTool && newDrawTool.isSelectable) {
      setActiveTool(newDrawTool?.drawModeId);
      changeDrawMode(newDrawTool?.value);
    } else {
      // fallback for if it can not find properties.selectionType in the geoJSON: set draw mode based on type of geoJSON
      const geometryType = newFeature.geometry.type;
      const fallbackDrawTool = drawModes.find(
        ({ shape }) =>
          shape.type === 'Feature' && shape.geometry.type === geometryType,
      );
      if (fallbackDrawTool) {
        changeDrawMode(fallbackDrawTool.value);
      }
    }
  };

  // change geoJSON properties
  const onChangeOpacity = (value: string): void => {
    const parseValue = parseInt(value, 10);
    const newOpacity = parseValue / 100;
    changeProperties({ 'fill-opacity': newOpacity });
  };

  const onChangeFill = (value: string): void => {
    changeProperties({ fill: value });
  };

  const onChangeStrokeOpacity = (value: string): void => {
    const parseValue = parseInt(value, 10);
    const newOpacity = parseValue / 100;
    changeProperties({ 'stroke-opacity': newOpacity });
  };

  const onChangeStroke = (value: string): void => {
    changeProperties({ stroke: value });
  };

  const onChangeStrokeWidth = (value: string): void => {
    changeProperties({ 'stroke-width': value });
  };

  return (
    <StoryLayout layers={layers} onExitDrawMode={onExitDrawMode}>
      <>
        <Grid size={{ xs: 12 }}>
          {drawModes.map((mode) => (
            <ToolButton
              key={mode.drawModeId}
              mode={mode}
              onClick={(): void => {
                changeActiveTool(mode);
              }}
              isSelected={activeTool === mode.drawModeId}
              icon={getToolIcon(mode.selectionType)}
            />
          ))}

          <Divider />
        </Grid>

        <Grid size={{ xs: 12 }}>
          <SelectField
            label="Fill opacity"
            value={opacityString}
            onChangeValue={onChangeOpacity}
            options={opacityOptions}
            labelSuffix=" %"
          />

          <SelectField
            label="Fill color"
            value={fill}
            onChangeValue={onChangeFill}
            options={fillOptions}
            showValueAsColor
          />

          <SelectField
            label="Stroke opacity"
            value={strokeOpacityString}
            onChangeValue={onChangeStrokeOpacity}
            options={opacityOptions.filter((option) => option !== '0')}
            width="33.3%"
            labelSuffix=" %"
          />

          <SelectField
            label="Stroke color"
            value={strokeColor}
            onChangeValue={onChangeStroke}
            options={fillOptions}
            width="33.3%"
            showValueAsColor
          />

          <SelectField
            label="Stroke width"
            value={strokeWidth}
            onChangeValue={onChangeStrokeWidth}
            options={strokeWidthOptions}
            width="33.3%"
          />
        </Grid>

        <EditModeButton
          isInEditMode={isInEditMode}
          onToggleEditMode={setEditMode}
          drawMode={activeTool}
        />

        <FeatureLayers
          geojson={geoJSON}
          onChangeLayerIndex={onChangeFeatureLayer}
          activeFeatureLayerIndex={featureLayerIndex}
          getToolIcon={getToolIcon}
        />

        <GeoJSONTextField onChangeGeoJSON={setGeoJSON} geoJSON={geoJSON} />
      </>
    </StoryLayout>
  );
};

export const BasicMapDrawTool: Story = {
  render: () => <BasicMapDrawToolComponent mapDrawOptions={{}} />,
};

export const BasicMapDrawToolShape: Story = {
  render: () => (
    <BasicMapDrawToolComponent mapDrawOptions={basicExampleDrawOptions} />
  ),
};

export const BasicMapDrawToolWithMultipleShapes: Story = {
  render: () => (
    <BasicMapDrawToolComponent
      mapDrawOptions={basicExampleMultipleShapeDrawOptions}
    />
  ),
};

export const BasicMapDrawToolWithMultipleShapesValues: Story = {
  render: () => (
    <BasicMapDrawToolComponent
      mapDrawOptions={basicExampleMultipleShapeWithValuesDrawOptions}
    />
  ),
};
