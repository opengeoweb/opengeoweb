/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { Grid2 as Grid, Divider, Typography } from '@mui/material';

import type { Meta, StoryObj } from '@storybook/react';
import { useMapDrawTool } from './useMapDrawTool';
import { DrawMode } from './types';
import { DrawModeExitCallback } from '../MapDraw/MapDraw';
import { GeoJSONTextField } from '../MapDraw/storyComponents';
import { StoryLayout } from '../MapDraw/storyComponents/StoryLayout';
import { ToolButton } from '../MapDraw/storyComponents/ToolButton';
import { IntersectionSelect } from '../MapDraw/storyComponents/IntersectionSelect';
import {
  startToolExampleConfig,
  endToolExampleConfig,
  getDoubleControlToolIcon,
  updateEditModeButtonsWithFir,
  exampleIntersectionsMultiDrawTool,
} from './storyExamplesMapDrawToolMultipleDrawTools';
import { WebmapReactThemeProvider } from '../Providers/Providers';

const meta: Meta<typeof useMapDrawTool> = {
  title: 'components/MapDrawTool/multiple drawtools',
  tags: ['!autodocs'],
};
export default meta;

type Story = StoryObj<typeof useMapDrawTool>;

const MultipleDrawTools = (): React.ReactElement => {
  const {
    geoJSON: startGeoJSON,
    geoJSONIntersection: startGeoJSONIntersection,
    drawModes: startEditModes,
    activeTool: startActiveTool,
    changeActiveTool: startChangeActiveTool,
    getLayer: startGetLayer,
    deactivateTool: startDeactivateTool,
    setGeoJSONIntersectionBounds: startSetGeoJSONIntersectionBounds,
    setDrawModes: startSetEditModes,
  } = useMapDrawTool(startToolExampleConfig);

  const {
    geoJSON: endGeoJSON,
    geoJSONIntersection: endGeoJSONIntersection,
    drawModes: endEditModes,
    activeTool: endActiveTool,
    changeActiveTool: endChangeActiveTool,
    getLayer: endGetLayer,
    deactivateTool: endDeactivateTool,
    setGeoJSONIntersectionBounds: endSetGeoJSONIntersectionBounds,
    setDrawModes: endSetEditModes,
  } = useMapDrawTool(endToolExampleConfig);

  const layers = [
    startGetLayer('geoJSONIntersectionBounds', 'example-static-layer'),
    endGetLayer('geoJSON', 'example-draw-layer-end'),
    endGetLayer('geoJSONIntersection', 'example-intersection-layer-end'),
    startGetLayer('geoJSON', 'example-draw-layer'),
    startGetLayer('geoJSONIntersection', 'example-intersection-layer'),
  ];

  const onExitDrawMode = (reason: DrawModeExitCallback): void => {
    if (reason === 'escaped') {
      startDeactivateTool();
      endDeactivateTool();
    }
  };

  const onChangeIntersection = (
    newGeoJSON: GeoJSON.FeatureCollection,
  ): void => {
    // deactivate tool modes
    startDeactivateTool();
    endDeactivateTool();
    // update intersection bounds for both shapes
    startSetGeoJSONIntersectionBounds(newGeoJSON);
    endSetGeoJSONIntersectionBounds(newGeoJSON);
    // update controls with new shape for fir
    startSetEditModes(updateEditModeButtonsWithFir(startEditModes, newGeoJSON));
    endSetEditModes(updateEditModeButtonsWithFir(endEditModes, newGeoJSON));
  };

  const onChangeStartTool = (mode: DrawMode): void => {
    endDeactivateTool(); // deactivate end tool
    startChangeActiveTool(mode);
  };

  const onChangeEndTool = (mode: DrawMode): void => {
    startDeactivateTool(); // deactivate start tool
    endChangeActiveTool(mode);
  };

  return (
    <WebmapReactThemeProvider>
      <StoryLayout layers={layers} onExitDrawMode={onExitDrawMode}>
        <>
          <IntersectionSelect
            intersections={exampleIntersectionsMultiDrawTool}
            onChangeIntersection={onChangeIntersection}
            isDisabled={startActiveTool !== '' || endActiveTool !== ''}
          />

          <Grid size={{ xs: 12 }}>
            <Typography sx={{ margin: 1, marginLeft: 0 }}>
              Start geoJSON
            </Typography>
            {startEditModes.map((mode) => (
              <ToolButton
                key={mode.drawModeId}
                mode={mode}
                onClick={(): void => onChangeStartTool(mode)}
                isSelected={startActiveTool === mode.drawModeId}
                icon={getDoubleControlToolIcon(mode)}
              />
            ))}
            <Divider sx={{ marginBottom: 1 }} />

            <GeoJSONTextField
              title="start geoJSON result"
              geoJSON={startGeoJSON}
              sx={{ maxHeight: 200, height: 200, overflow: 'hidden' }}
              maxRows={7}
            />

            <GeoJSONTextField
              title="start intersection geoJSON result"
              geoJSON={startGeoJSONIntersection as GeoJSON.FeatureCollection}
              sx={{ maxHeight: 200, height: 200, overflow: 'hidden' }}
              maxRows={7}
            />
          </Grid>

          <Grid size={{ xs: 12 }}>
            <Typography sx={{ margin: 1, marginLeft: 0 }}>
              End geoJSON
            </Typography>
            {endEditModes.map((mode) => (
              <ToolButton
                key={mode.drawModeId}
                mode={mode}
                onClick={(): void => onChangeEndTool(mode)}
                isSelected={
                  mode.isSelectable && endActiveTool === mode.drawModeId
                }
                icon={getDoubleControlToolIcon(mode)}
              />
            ))}

            <Divider sx={{ marginBottom: 1 }} />

            <GeoJSONTextField
              title="end geoJSON result"
              geoJSON={endGeoJSON}
              sx={{ maxHeight: 200, height: 200, overflow: 'hidden' }}
              maxRows={7}
            />

            <GeoJSONTextField
              title="end intersection geoJSON result"
              geoJSON={endGeoJSONIntersection as GeoJSON.FeatureCollection}
              sx={{ maxHeight: 200, height: 200, overflow: 'hidden' }}
              maxRows={7}
            />
          </Grid>
        </>
      </StoryLayout>
    </WebmapReactThemeProvider>
  );
};

export const MultipleDrawToolsDemo: Story = {
  render: MultipleDrawTools,
};
