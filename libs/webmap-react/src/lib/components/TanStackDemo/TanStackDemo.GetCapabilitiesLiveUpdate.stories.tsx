/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  invalidateWMSGetCapabilities,
  LayerProps,
  queryWMSLayer,
  wmsQueryClient,
} from '@opengeoweb/webmap';
import { QueryClientProvider } from '@tanstack/react-query';
import { Paper } from '@mui/material';
import { WebmapReactThemeProvider } from '../Providers/Providers';
import { useQueryGetWMSLayers } from '../../hooks/useHooksGetCapabilities';

export default {
  title: 'components/TanStackDemo',
};

const serviceUrl =
  'https://geoservices.knmi.nl/adagucserver?DATASET=liveupdate&SERVICE=WMS&';
const layerName = 'live_update_every_second';

const TanStackDemoQueryWMSLayer = (): React.ReactElement => {
  const layers = useQueryGetWMSLayers(serviceUrl);
  const [layer, setLayer] = React.useState<LayerProps>();

  // Demonstrate how to query a WMS layer with the new TanStack implementation
  const fetchLayer = async (): Promise<void> => {
    const layerPropsFromWMS = await queryWMSLayer(serviceUrl, layerName);
    setLayer(layerPropsFromWMS);
  };

  React.useEffect(() => {
    // Call fetchLayer 10 times, to demonstrate that fetching is only done once. TanStack 'debounces' this because it has a cache.
    for (let j = 0; j < 10; j += 1) {
      void fetchLayer();
    }

    // This will invalidate the GetCapabilities every second. This will force TanStack to fetch new documents from the server.
    const intervalId = setInterval(async () => {
      await invalidateWMSGetCapabilities(serviceUrl);
      void fetchLayer();
    }, 1000);

    return (): void => clearInterval(intervalId);
  }, []);

  return (
    <div style={{ height: '100vh' }}>
      <Paper style={{ padding: '10px' }}>
        <h1>Demonstration of useQueryGetWMSLayers and queryWMSLayer</h1>
        <p>
          The data below should update every second, displaying latest time
          values from WMS services.
        </p>
        <p>
          live_update_every_second should update every second. The solar
          terminator layer updates every 10 minutes.
        </p>
        <p>Only one request per second should be made.</p>
        <hr />

        <h2>Latest time values for each layer in the WMS Service:</h2>
        <table>
          <thead>
            <tr>
              <th>Layer name</th>
              <th>time</th>
            </tr>
          </thead>
          <tbody>
            {layers.map((l) => {
              return (
                <tr key={`D${l.name}`}>
                  <td>
                    <i>{l.name}</i>
                  </td>
                  <td>
                    <b>
                      <code>
                        {l.dimensions && l.dimensions[0].currentValue}
                      </code>
                    </b>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <hr />
        <h2>All properties for live_update_every_second:</h2>
        <div style={{ background: '#EEE', width: '400px', padding: '10px' }}>
          <pre style={{ fontSize: '10px' }}>
            {JSON.stringify(layer, null, 2)}
          </pre>
        </div>
      </Paper>
    </div>
  );
};

export const MapViewRadar = (): React.ReactElement => {
  return (
    <WebmapReactThemeProvider>
      <QueryClientProvider client={wmsQueryClient}>
        <TanStackDemoQueryWMSLayer />
      </QueryClientProvider>
    </WebmapReactThemeProvider>
  );
};
MapViewRadar.storyName = 'GetCapabilities every second';
