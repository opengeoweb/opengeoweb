/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, waitFor, screen } from '@testing-library/react';

import { webmapUtils } from '@opengeoweb/webmap';
import { ThemeWrapper } from '@opengeoweb/theme';
import { WebmapReactThemeProvider } from '../Providers/Providers';
import { MapView } from './MapView';
import { defaultLayers } from '../../layers';
import { MapViewLayer } from './MapViewLayer';

describe('src/components/MapView', () => {
  const mapViewProps = {
    mapId: webmapUtils.generateMapId(),
    onWMJSMount: (): void => {},
  };

  it('should show map', () => {
    render(
      <WebmapReactThemeProvider>
        <MapView {...mapViewProps} />
      </WebmapReactThemeProvider>,
    );
    expect(screen.getByRole('application', { name: 'map' })).toBeTruthy();
  });

  it('should show the zoomControls', () => {
    render(
      <ThemeWrapper>
        <MapView {...mapViewProps} />
      </ThemeWrapper>,
    );
    expect(screen.getByTestId('zoom-reset')).toBeTruthy();
    expect(screen.getByTestId('zoom-in')).toBeTruthy();
    expect(screen.getByTestId('zoom-out')).toBeTruthy();
  });

  it('should hide the zoomControls', () => {
    render(
      <ThemeWrapper>
        <MapView {...mapViewProps} controls={{}} />
      </ThemeWrapper>,
    );
    expect(screen.queryByTestId('zoom-reset')).toBeFalsy();
    expect(screen.queryByTestId('zoom-in')).toBeFalsy();
    expect(screen.queryByTestId('zoom-out')).toBeFalsy();
  });

  it('should render map with layers when layers exist', () => {
    render(
      <ThemeWrapper>
        <MapView {...mapViewProps}>
          <MapViewLayer {...defaultLayers.baseLayerGrey} />
          <MapViewLayer {...defaultLayers.overLayer} />
        </MapView>
      </ThemeWrapper>,
    );

    const layerList = screen.queryAllByTestId('mapViewLayer');

    expect(layerList.length).toEqual(2);
    expect(layerList[0].textContent).toContain(defaultLayers.baseLayerGrey.id);
    expect(layerList[1].textContent).toContain(defaultLayers.overLayer.id);
  });

  it('should show the time display by default', () => {
    render(
      <ThemeWrapper>
        <MapView
          {...mapViewProps}
          dimensions={[
            { name: 'time', currentValue: new Date().toISOString() },
          ]}
        />
      </ThemeWrapper>,
    );
    expect(screen.getByTestId('map-time')).toBeTruthy();
  });

  it('should hide the time display', () => {
    render(
      <ThemeWrapper>
        <MapView
          {...mapViewProps}
          displayTimeInMap={false}
          dimensions={[
            { name: 'time', currentValue: new Date().toISOString() },
          ]}
        />
      </ThemeWrapper>,
    );
    expect(screen.queryByTestId('map-time')).toBeFalsy();
  });

  it('should support null objects as children', () => {
    render(
      <ThemeWrapper>
        <MapView mapId={webmapUtils.generateMapId()}>
          <MapViewLayer {...defaultLayers.baseLayerGrey} />
        </MapView>
      </ThemeWrapper>,
    );

    expect(screen.getByRole('application', { name: 'map' })).toBeTruthy();
    const layerList = screen.queryAllByTestId('mapViewLayer');
    expect(layerList.length).toEqual(1);
    expect(layerList[0].textContent).toContain(defaultLayers.baseLayerGrey.id);
  });

  it('should observe resizing', async () => {
    const observeSpy = jest.fn();
    global.ResizeObserver = jest.fn().mockImplementation(() => ({
      observe: observeSpy,
      unobserve: jest.fn(),
      disconnect: jest.fn(),
    }));

    render(
      <ThemeWrapper>
        <MapView {...mapViewProps} />
      </ThemeWrapper>,
    );
    expect(screen.getByRole('application', { name: 'map' })).toBeTruthy();

    await waitFor(() => {
      expect(observeSpy).toHaveBeenCalled();
    });
  });

  it('should call onWMJSMount after map mount', () => {
    const onWMJSMount = jest.fn();
    render(
      <ThemeWrapper>
        <MapView {...mapViewProps} onWMJSMount={onWMJSMount} />
      </ThemeWrapper>,
    );

    expect(onWMJSMount).toHaveBeenCalled();
  });
});
