/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { generateLayerId, webmapUtils } from '@opengeoweb/webmap';
import type { Meta, StoryObj } from '@storybook/react';
import { MapView, MapViewLayer } from '.';
import { defaultLayers, publicLayers } from '../../layers';

const meta: Meta<typeof MapView> = {
  title: 'components/MapView',
  component: MapView,
  parameters: {
    docs: {
      description: {
        component: 'MapView component',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof MapView>;

// main story doc
export const Component: Story = {
  args: {
    controls: { zoomControls: true },
  },
  tags: ['!autodocs', '!dev'],
  render: (props) => {
    const baseLayer = { ...publicLayers.baseLayer, id: generateLayerId() };
    const overLayer = { ...defaultLayers.overLayer, id: generateLayerId() };
    return (
      <div style={{ height: '600px' }}>
        <MapView {...props}>
          <MapViewLayer {...baseLayer} />
          <MapViewLayer {...overLayer} />
        </MapView>
      </div>
    );
  },
};

export const MapError: Story = {
  args: {
    mapId: webmapUtils.generateMapId(),
  },
  render: (props) => (
    <>
      <div
        style={{
          top: '10px',
          left: '50px',
          position: 'absolute',
          padding: '20px',
          background: '#DDD',
          zIndex: 10000,
        }}
      >
        The console panel of your browser shows the error produced by this
        layer.
      </div>
      <div style={{ height: '100vh' }}>
        <MapView {...props}>
          <MapViewLayer {...publicLayers.baseLayer} />
          <MapViewLayer
            {...publicLayers.radarLayerWithError}
            onLayerError={(_, error): void => {
              console.warn('Layer with error', error);
            }}
          />
          <MapViewLayer {...defaultLayers.overLayer} />
        </MapView>
      </div>
    </>
  ),
  parameters: {
    docs: {
      description: {
        story:
          'The console panel of your browser shows the error produced by this layer.',
      },
    },
  },
};

MapError.storyName = 'Map with a layer which has an error';

export const MapWithDuplicateLayerIdsError: Story = {
  args: {
    mapId: webmapUtils.generateMapId(),
  },
  render: (props) => (
    <>
      <div
        style={{
          top: '10px',
          left: '50px',
          position: 'absolute',
          padding: '20px',
          background: '#DDD',
          zIndex: 10000,
        }}
      >
        The console panel of your browser shows the error produced by this
        layer.
      </div>
      <div style={{ height: '100vh' }}>
        <MapView {...props}>
          <MapViewLayer {...publicLayers.baseLayer} />
          <MapViewLayer
            {...publicLayers.obsAirTemperature}
            onLayerError={(_, error): void => {
              console.warn('Layer with error', error);
            }}
          />
          <MapViewLayer
            {...publicLayers.obsAirTemperature}
            onLayerError={(_, error): void => {
              console.warn('Layer with error', error);
            }}
          />
          <MapViewLayer {...defaultLayers.overLayer} />
        </MapView>
      </div>
    </>
  ),
  parameters: {
    docs: {
      description: {
        story:
          'The console panel of your browser shows the error produced by this layer.',
      },
    },
  },
};
MapWithDuplicateLayerIdsError.storyName =
  "Map with duplicate layers id's has an error";
