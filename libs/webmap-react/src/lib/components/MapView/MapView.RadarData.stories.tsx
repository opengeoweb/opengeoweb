/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';

import { webmapUtils } from '@opengeoweb/webmap';
import type { Meta, StoryObj } from '@storybook/react';
import { defaultLayers, publicLayers } from '../../layers';
import { MapView } from './MapView';
import { LegendDialog } from '../Legend';
import { MapViewLayer } from './MapViewLayer';

const meta: Meta<typeof MapView> = {
  title: 'components/MapView',
  component: MapView,
};
export default meta;

type Story = StoryObj<typeof MapView>;

export const MapViewRadar: Story = {
  args: {
    mapId: webmapUtils.generateMapId(),
  },
  render: (props) => (
    <div style={{ height: '100vh' }}>
      <LegendDialog
        layers={[publicLayers.radarLayer]}
        isOpen={true}
        mapId=""
        onClose={(): void => {}}
      />
      <MapView {...props}>
        <MapViewLayer {...publicLayers.baseLayer} />
        <MapViewLayer
          {...publicLayers.radarLayer}
          onLayerReady={(layer): void => {
            layer.zoomToLayer();
          }}
        />
        <MapViewLayer {...defaultLayers.overLayer} />
      </MapView>
    </div>
  ),
};

MapViewRadar.storyName = 'MapView with radar data';
