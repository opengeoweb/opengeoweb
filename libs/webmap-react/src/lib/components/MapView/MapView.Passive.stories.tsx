/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { webmapUtils } from '@opengeoweb/webmap';
import type { Meta, StoryObj } from '@storybook/react';
import { MapView, MapViewLayer } from '.';
import { publicLayers, defaultLayers } from '../../layers';

const meta: Meta<typeof MapView> = {
  title: 'components/MapView',
  component: MapView,
};
export default meta;

type Story = StoryObj<typeof MapView>;
export const PassiveMap: Story = {
  args: {
    mapId: webmapUtils.generateMapId(),
    passiveMap: true,
    showScaleBar: false,
    controls: {},
    // eslint-disable-next-line no-console
    onClick: (): void => console.log('Passive map clicked'),
  },
  render: (props) => (
    <div style={{ height: '100vh' }}>
      <MapView {...props}>
        <MapViewLayer {...publicLayers.baseLayer} />
        <MapViewLayer
          {...publicLayers.radarLayer}
          onLayerReady={(layer): void => {
            layer.zoomToLayer();
          }}
        />
        <MapViewLayer {...defaultLayers.overLayer} />
      </MapView>
    </div>
  ),
};

PassiveMap.storyName = 'Passive MapView without controls';
