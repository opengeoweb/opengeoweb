/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  Paper,
  FormControl,
  InputLabel,
  Select,
  Box,
  MenuItem,
} from '@mui/material';

import { webmapUtils } from '@opengeoweb/webmap';
import { EPSGCode, PROJECTION } from '@opengeoweb/shared';
import type { Meta, StoryObj } from '@storybook/react';
import { MapView, MapViewLayer } from '.';
import { publicLayers, defaultLayers } from '../../layers';

const meta: Meta<typeof MapView> = {
  title: 'components/MapView',
  component: MapView,
};
export default meta;

type Story = StoryObj<typeof MapView>;

export const SetProjectionBBoxLocalState: Story = {
  args: {
    mapId: webmapUtils.generateMapId(),
  },
  render: (props) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [srsAndBBOX, setSrsAndBBOX] = React.useState({
      srs: PROJECTION.EPSG_3575.value as EPSGCode,
      bbox: {
        left: -13000000,
        bottom: -13000000,
        right: 13000000,
        top: 13000000,
      },
    });

    const BBOXES: Record<
      string,
      { left: number; bottom: number; right: number; top: number }
    > = {
      [PROJECTION.EPSG_32661.value]: {
        left: -2776118.977564746,
        bottom: -6499490.259201691,
        right: 9187990.785775745,
        top: 971675.53185069,
      },
      [PROJECTION.EPSG_3412.value]: {
        left: -4589984.273212382,
        bottom: -2752857.546211313,
        right: 5425154.657417289,
        top: 2986705.2537886878,
      },
      [PROJECTION.EPSG_3411.value]: {
        left: -5661541.927991125,
        bottom: -3634073.745615984,
        right: 5795287.923063262,
        top: 2679445.334384017,
      },
      [PROJECTION.EPSG_3857.value]: {
        left: -19000000,
        bottom: -19000000,
        right: 19000000,
        top: 19000000,
      },
      [PROJECTION.EPSG_54030.value]: {
        left: -17036744.451383516,
        bottom: -10711364.114367772,
        right: 16912038.081015453,
        top: 10488456.659686875,
      },
      [PROJECTION.EPSG_28992.value]: {
        left: -350000,
        bottom: 125000,
        right: 700000,
        top: 900000,
      },
      [PROJECTION.EPSG_3067.value]: {
        left: -1197402.0,
        bottom: 6439686.0,
        right: 2090705,
        top: 7975202,
      },
    };

    const handleChange = (srsValue: string): void => {
      const bbox = BBOXES[srsValue];

      if (bbox) {
        setSrsAndBBOX({
          srs: srsValue as EPSGCode,
          bbox,
        });
      } else {
        // Default case if SRS code is not found in BBOXES
        setSrsAndBBOX({
          srs: PROJECTION.EPSG_3575.value,
          bbox: {
            left: -13000000,
            bottom: -13000000,
            right: 13000000,
            top: 13000000,
          },
        });
      }
    };

    return (
      <>
        <div style={{ height: '100vh' }}>
          {/* 'Europe North Pole' */}
          <MapView
            srs={srsAndBBOX.srs}
            bbox={srsAndBBOX.bbox}
            onMapZoomEnd={(bboxPayload): void => {
              // eslint-disable-next-line no-console
              console.log('onMapZoomEnd', JSON.stringify(bboxPayload));
            }}
            {...props}
          >
            <MapViewLayer {...publicLayers.baseLayer} />
            <MapViewLayer {...defaultLayers.overLayer} />
          </MapView>
        </div>
        <div
          style={{
            position: 'absolute',
            top: '20px',
            left: '50px',
            zIndex: 1000,
          }}
        >
          <Box>
            <Paper>
              <FormControl variant="filled">
                <InputLabel id="demo-select-SRS-label">Select SRS</InputLabel>
                <Select
                  labelId="demo-select-SRS-label"
                  id="demo-select-SRS"
                  value={srsAndBBOX.srs}
                  onChange={(event): void => {
                    handleChange(event.target.value);
                  }}
                >
                  <MenuItem value={PROJECTION.EPSG_3575.value}>
                    {PROJECTION.EPSG_3575.name}
                  </MenuItem>
                  <MenuItem value={PROJECTION.EPSG_3411.value}>
                    {PROJECTION.EPSG_3411.name}
                  </MenuItem>
                  <MenuItem value={PROJECTION.EPSG_3412.value}>
                    {PROJECTION.EPSG_3412.name}
                  </MenuItem>
                  <MenuItem value={PROJECTION.EPSG_32661.value}>
                    {PROJECTION.EPSG_32661.name}
                  </MenuItem>
                  <MenuItem value={PROJECTION.EPSG_3857.value}>
                    {PROJECTION.EPSG_3857.name}
                  </MenuItem>
                  <MenuItem value={PROJECTION.EPSG_54030.value}>
                    {PROJECTION.EPSG_54030.name}
                  </MenuItem>
                  <MenuItem value={PROJECTION.EPSG_28992.value}>
                    {PROJECTION.EPSG_28992.name}
                  </MenuItem>
                  <MenuItem value={PROJECTION.EPSG_3067.value}>
                    {PROJECTION.EPSG_3067.name}
                  </MenuItem>
                </Select>
              </FormControl>
            </Paper>
          </Box>
        </div>
      </>
    );
  },
};

SetProjectionBBoxLocalState.storyName =
  'Set projection and BBOX via local state';
