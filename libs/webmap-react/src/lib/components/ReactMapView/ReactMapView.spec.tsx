/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import {
  LayerType,
  LayerOptions,
  webmapUtils,
  registerWMLayer,
  WMLayer,
  mockGetCapabilities,
  setWMSGetCapabilitiesFetcher,
  webmapTestSettings,
} from '@opengeoweb/webmap';

import RadarGetCapabilities from './radarGetCapabilities.spec.json';
import { MINUTE_TO_MILLISECOND, ReactMapView } from './ReactMapView';
import {
  isAMapLayer,
  getFeatureLayers,
  isAGeoJSONLayer,
  parseWMJSLayer,
} from './utils';
import { ReactMapViewLayer } from './ReactMapViewLayer';
import { defaultLayers } from '../../layers';
import { getLayerUpdateInfo } from './ReactMapViewParseLayer';

export const SECOND_TO_MILLISECOND = 1000;

export const mockGeoJSON: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [],
};

jest.mock('./utils', () => ({
  ...jest.requireActual('./utils'),
  parseWMJSLayer: jest.fn(),
}));

jest.mock('./ReactMapViewParseLayer', () => ({
  ...jest.requireActual('./ReactMapViewParseLayer'),
  getLayerUpdateInfo: jest.fn(),
}));

describe('src/components/ReactMapView/ReactMapView', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });
  it('should trigger onClick when the passive map is clicked', async () => {
    const props = {
      mapId: 'map1',
      passiveMap: true,
      onClick: jest.fn(),
    };
    render(<ReactMapView {...props} />);

    fireEvent.click(screen.getByRole('button'));

    expect(props.onClick).toHaveBeenCalledTimes(1);
  });

  it('should return false for a baselayer without geojson', () => {
    expect(isAMapLayer(defaultLayers.baseLayerGrey)).toBeFalsy();
  });

  it('should return false for an overlayer without geojson', () => {
    expect(isAMapLayer(defaultLayers.overLayer)).toBeFalsy();
  });

  it('should return true for a maplayer without geojson', () => {
    expect(
      isAMapLayer(webmapTestSettings.defaultReduxLayerRadarKNMI),
    ).toBeTruthy();
  });

  it('should return false for a baselayer with geojson', () => {
    const mockLayer = {
      ...defaultLayers.baseLayerGrey,
      geojson: mockGeoJSON,
    };
    expect(isAMapLayer(mockLayer)).toBeFalsy();
  });

  it('should detect geojson layer for a baselayer with geojson', () => {
    const mockLayer = {
      ...defaultLayers.baseLayerGrey,
      geojson: mockGeoJSON,
    };
    expect(isAGeoJSONLayer(mockLayer)).toBeTruthy();
  });

  it('should return false for an overlayer with geojson', () => {
    const mockLayer = {
      ...defaultLayers.overLayer,
      geojson: mockGeoJSON,
    };
    expect(isAMapLayer(mockLayer)).toBeFalsy();
  });

  it('should detect geojson layer for an overlayer with geojson', () => {
    const mockLayer = {
      ...defaultLayers.overLayer,
      geojson: mockGeoJSON,
    };
    expect(isAGeoJSONLayer(mockLayer)).toBeTruthy();
  });

  it('should return true for a maplayer with geojson', () => {
    const mockLayer = {
      ...webmapTestSettings.defaultReduxLayerRadarKNMI,
      geojson: mockGeoJSON,
    };
    expect(isAMapLayer(mockLayer)).toBeTruthy();
  });

  it('should return a list of props if it has geojson', () => {
    const mockLayer = {
      ...webmapTestSettings.defaultReduxLayerRadarKNMI,
      props: {
        geojson: 'string',
      },
    };
    const mockLayer2 = {
      ...defaultLayers.baseLayerGrey,
      props: {
        geojson: 'another-string',
      },
    };
    expect(
      getFeatureLayers([mockLayer, mockLayer2] as React.ReactNode),
    ).toEqual([mockLayer2.props, mockLayer.props]);
  });

  it('should return an empty list if it has no geojson', () => {
    const mockLayer = {
      ...webmapTestSettings.defaultReduxLayerRadarKNMI,
      props: {
        otherProp: 'string',
      },
    };
    const mockLayer2 = {
      ...defaultLayers.baseLayerGrey,
      props: {
        anotherProp: 'another-string',
      },
    };
    expect(
      getFeatureLayers([mockLayer, mockLayer2] as React.ReactNode),
    ).toEqual([]);
  });

  it('should trigger onMapPinChangeLocation when the map is clicked at the same location', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
      disableMapPin: false,
      onMapPinChangeLocation: jest.fn(),
    };
    render(<ReactMapView {...props} />);

    /* Click at same location should trigger onMapPinChangeLocation */
    webmapUtils.getWMJSMapById('map1')!.mouseDown(10, 10, null!);
    webmapUtils.getWMJSMapById('map1')!.mouseUp(10, 10, null!);
    expect(props.onMapPinChangeLocation).toHaveBeenCalledTimes(1);

    /* Click at almost the same location should trigger onMapPinChangeLocation */
    webmapUtils.getWMJSMapById('map1')!.mouseDown(10, 10, null!);
    webmapUtils.getWMJSMapById('map1')!.mouseUp(8, 8, null!);
    expect(props.onMapPinChangeLocation).toHaveBeenCalledTimes(2);

    /* Click at different location should NOT trigger onMapPinChangeLocation */
    webmapUtils.getWMJSMapById('map1')!.mouseDown(100, 100, null!);
    webmapUtils.getWMJSMapById('map1')!.mouseUp(3, 3, null!);
    expect(props.onMapPinChangeLocation).toHaveBeenCalledTimes(2);
  });

  it('should not trigger onMapPinChangeLocation when the mapPin is disabled', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
      disableMapPin: true,
      onMapPinChangeLocation: jest.fn(),
    };
    render(<ReactMapView {...props} />);

    webmapUtils.getWMJSMapById('map1')!.mouseDown(10, 10, null!);
    webmapUtils.getWMJSMapById('map1')!.mouseUp(10, 10, null!);
    expect(props.onMapPinChangeLocation).not.toHaveBeenCalled();
  });

  it('should not trigger onMapPinChangeLocation when the mapPin is not visible', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: false,
      disableMapPin: false,
      onMapPinChangeLocation: jest.fn(),
    };
    render(<ReactMapView {...props} />);

    webmapUtils.getWMJSMapById('map1')!.mouseDown(10, 10, null!);
    webmapUtils.getWMJSMapById('map1')!.mouseUp(10, 10, null!);
    expect(props.onMapPinChangeLocation).not.toHaveBeenCalled();
  });

  it('should call onUpdateLayerInformation when a layer is removed', async () => {
    const mockOnUpdateLayerInformation = jest.fn();
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
      onUpdateLayerInformation: mockOnUpdateLayerInformation,
      services: { serviceid_1: { name: 'somename' } },
    };

    const layerDef: LayerOptions = {
      id: 'testlayer',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
    };
    const wmLayer = new WMLayer(layerDef);

    registerWMLayer(wmLayer, layerDef.id);
    const { rerender } = render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDef} />
      </ReactMapView>,
    );

    /* Check if IWMJSMap instance was made */
    const map = webmapUtils.getWMJSMapById('map1')!;

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(1);

    /* Check if onUpdateLayerInformation was called */
    await waitFor(() =>
      expect(props.onUpdateLayerInformation).toHaveBeenCalledTimes(1),
    );

    /* Remove the layer */
    rerender(<ReactMapView {...props} />);

    /* Check if it has no layers anymore */
    expect(map.getLayers().length).toBe(0);

    /* Check if onUpdateLayerInformation was called again */
    await waitFor(() =>
      expect(props.onUpdateLayerInformation).toHaveBeenCalledTimes(2),
    );
  });

  it('should call onUpdateLayerInformation when the layername is changed', async () => {
    const mockOnUpdateLayerInformation = jest.fn();
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
      onUpdateLayerInformation: mockOnUpdateLayerInformation,
      services: { serviceid_1: { name: 'somename' } },
    };

    /* Create a ReactMapView component without a time dimension and add a layer */

    const layerDef: LayerOptions = {
      id: 'testlayer',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
    };

    const { rerender } = render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDef} />
      </ReactMapView>,
    );

    /* Check if IWMJSMap instance was made */
    const map = webmapUtils.getWMJSMapById('map1')!;

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(1);

    /* Check its name */
    expect(map.getLayers()[0].name).toBe('RAD_NL25_PCP_CM');

    /* Check if onUpdateLayerInformation was called */
    await waitFor(() =>
      expect(props.onUpdateLayerInformation).toHaveBeenCalledTimes(1),
    );

    /* Change layerName */
    const newLayerDef: LayerOptions = {
      ...layerDef,
      name: 'Reflectivity',
    };
    rerender(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...newLayerDef} />
      </ReactMapView>,
    );

    /* Check if layer is still there */
    expect(map.getLayers().length).toBe(1);

    /* Check its name */
    expect(map.getLayers()[0].name).toBe('Reflectivity');

    /* Check if onUpdateLayerInformation was called */
    await waitFor(() =>
      expect(props.onUpdateLayerInformation).toHaveBeenCalledTimes(2),
    );
  });

  it('should call onLayerError for duplicate layer id', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
    };

    /* Create a ReactMapView component without a time dimension and add a layer */
    const layerDefA: LayerOptions = {
      id: 'testlayer',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
      onLayerError: jest.fn(),
    };

    const layerDefB: LayerOptions = {
      id: 'testlayer', // <== Same id as layerDefA
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
      onLayerError: jest.fn(),
    };

    const { rerender } = render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );

    /* Check if IWMJSMap instance was made */
    const map = webmapUtils.getWMJSMapById('map1')!;

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(1);

    expect(layerDefA.onLayerError).toHaveBeenCalledTimes(0);
    expect(layerDefB.onLayerError).toHaveBeenCalledTimes(0);

    jest.spyOn(console, 'warn').mockImplementation();
    /* Add an extra layer with the same id */
    rerender(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDefA} />
        <ReactMapViewLayer {...layerDefB} />
      </ReactMapView>,
    );
    expect(console.warn).toHaveBeenCalled();

    /* Check if it has still one layer */
    expect(map.getLayers().length).toBe(1);

    /* Check if onLayerError was called for layerDefB */
    expect(layerDefA.onLayerError).toHaveBeenCalledTimes(0);
    expect(layerDefB.onLayerError).toHaveBeenCalledTimes(1);
  });

  it('updateWMJSMapProps should call correct IWMJSMap functions', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
    };

    /* Create a ReactMapView component without a time dimension and add a layer */

    const layerDefA: LayerOptions = {
      id: 'testlayer',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
      onLayerError: jest.fn(),
    };

    const { rerender } = render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );

    /* Check if IWMJSMap instance was made */
    const map = webmapUtils.getWMJSMapById('map1')!;

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(1);

    /* Check if showLegend works */
    const displayLegendInMapSpy = jest
      .spyOn(map, 'displayLegendInMap')
      .mockImplementation();
    /* Add an extra layer with the same id */
    rerender(
      <ReactMapView {...{ ...props, showLegend: true }}>
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );
    expect(displayLegendInMapSpy).toHaveBeenCalledWith(true);

    /* Check if displayScaleBarInMap works */
    const displayScaleBarInMap = jest
      .spyOn(map, 'displayLegendInMap')
      .mockImplementation();
    /* Add an extra layer with the same id */
    rerender(
      <ReactMapView {...{ ...props, showScaleBar: true }}>
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );
    expect(displayScaleBarInMap).toHaveBeenCalledWith(true);

    /* Check if map dimensions work */
    const setMapDimensionsSpy = jest
      .spyOn(map, 'setDimension')
      .mockImplementation();
    /* Add an extra layer with the same id */
    rerender(
      <ReactMapView
        {...{
          ...props,
          dimensions: [{ name: 'time', currentValue: '2020-01-01T00:00:00Z' }],
        }}
      >
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );
    expect(setMapDimensionsSpy).toHaveBeenCalledWith(
      'time',
      '2020-01-01T00:00:00Z',
      false,
      false,
    );

    /* Check if map setProjection work */
    const setProjectionSpy = jest
      .spyOn(map, 'setProjection')
      .mockImplementation();
    /* Add an extra layer with the same id */
    rerender(
      <ReactMapView
        {...{
          ...props,
          bbox: { left: 1, right: 2, bottom: 3, top: 4 },
        }}
      >
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );
    expect(setProjectionSpy).toHaveBeenCalledWith(
      'EPSG:4326',
      expect.objectContaining({ left: 1, right: 2, bottom: 3, top: 4 }),
    );

    /* Check if map setProjection for change of srs works */
    /* Add an extra layer with the same id */
    rerender(
      <ReactMapView
        {...{
          ...props,
          srs: 'EPSG:54030',
          bbox: { left: 1, right: 2, bottom: 3, top: 4 },
        }}
      >
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );
    expect(setProjectionSpy).toHaveBeenCalledWith(
      'EPSG:54030',
      expect.objectContaining({ left: 1, right: 2, bottom: 3, top: 4 }),
    );

    /* Check if layer dimension works */
    const layer = webmapUtils.getWMLayerById(layerDefA.id);
    await layer.parseLayer();
    const setLayerSetDimensionSpy = jest.spyOn(layer, 'setDimension');
    /* Add an extra layer with the same id */
    rerender(
      <ReactMapView {...props}>
        <ReactMapViewLayer
          {...{
            ...layerDefA,
            dimensions: [
              { name: 'time', currentValue: '2020-01-01T00:00:00Z' },
            ],
          }}
        />
      </ReactMapView>,
    );
    expect(setLayerSetDimensionSpy).toHaveBeenCalledWith(
      'time',
      '2020-01-01T00:00:00Z',
      false,
    );

    /* Check if change layer works */
    const layerDefB: LayerOptions = {
      id: 'testlayerB', // <== Same id as layerDefA
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
      onLayerError: jest.fn(),
    };
    const setLayerSpy = jest.spyOn(map, 'addLayer');
    /* Add an extra layer with the same id */
    rerender(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDefB} />
      </ReactMapView>,
    );
    expect(setLayerSpy).toHaveBeenCalledWith(
      expect.objectContaining({ ReactWMJSLayerId: 'testlayerB' }),
    );
  });

  it('updateWMJSMapProps should call IWMJSMap setDimension function on a new map without layers', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      dimensions: [{ name: 'time', currentValue: '2020-01-01T00:00:00Z' }],
      displayMapPin: true,
    };

    /* Create a map without layers */
    render(<ReactMapView {...props} />);

    /* Check if IWMJSMap instance was made */
    const map = webmapUtils.getWMJSMapById('map1')!;

    expect(map.getDimension('time')?.currentValue).toEqual(
      '2020-01-01T00:00:00Z',
    );
    webmapUtils.unRegisterWMJSMap(props.mapId);
  });

  it('updateWMJSMapProps should call IWMJSMap setDimension function on an existing map without layers', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
    };

    /* Create a map without layers */
    const { rerender } = render(<ReactMapView {...props} />);

    /* Check if IWMJSMap instance was made */
    const map = webmapUtils.getWMJSMapById('map1')!;

    /* Check if map dimensions work */
    const setMapDimensionsSpy = jest
      .spyOn(map, 'setDimension')
      .mockImplementation();

    /* Set map dimensions, only for the map (no layers added) */
    rerender(
      <ReactMapView
        {...{
          ...props,
          dimensions: [{ name: 'time', currentValue: '2020-01-01T00:00:00Z' }],
        }}
      />,
    );
    expect(setMapDimensionsSpy).toHaveBeenCalledWith(
      'time',
      '2020-01-01T00:00:00Z',
      false,
      false,
    );
  });

  it('a layer should get its own default value when the map has no time dimension', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
    };
    /* Create a ReactMapView component without a time dimension and add a layer */

    const layerDefA: LayerOptions = {
      id: 'testlayer',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
      onLayerError: jest.fn(),
    };

    render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );

    /* Check if IWMJSMap instance was made */
    const map = webmapUtils.getWMJSMapById('map1')!;

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(1);

    /* Check if the layer has the default value for time */
    const layer = webmapUtils.getWMLayerById(layerDefA.id);

    await layer.parseLayer();

    expect(layer.getDimension('time')!.getValue()).toBe(
      RadarGetCapabilities.WMS_Capabilities.Capability.Layer.Layer[0].Dimension
        .attr.default,
    );
  });

  it('a layer should get the map time dimension value when the map has a time dimension', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
      dimensions: [{ name: 'time', currentValue: '2020-01-01T00:00:00Z' }],
    };
    /* Create a ReactMapView component with a time dimension and add a layer */

    const layerDefA: LayerOptions = {
      id: 'testlayer',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
      onLayerError: jest.fn(),
    };

    render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );

    /* Check if IWMJSMap instance was made */
    const map = webmapUtils.getWMJSMapById('map1')!;

    /* Check if the map time dimension has the correct value */
    expect(map.getDimension('time')?.getValue()).toBe(
      props.dimensions[0].currentValue,
    );

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(1);

    /* Check if the layer has the closest value possible for its time dimension */
    const layer = webmapUtils.getWMLayerById(layerDefA.id);
    await layer.parseLayer();
    expect(layer.getDimension('time')!.getValue()).toBe(
      RadarGetCapabilities.WMS_Capabilities.Capability.Layer.Layer[0].Dimension.value.split(
        '/',
      )[0],
    );
  });
  it('should call onUpdateLayerInformation and onLayerReady when services object is not defined', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
      onUpdateLayerInformation: jest.fn(),
    };

    /* Create a ReactMapView component without a time dimension and add a layer */

    const layerDef: LayerOptions = {
      id: 'testlayer',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
      onLayerReady: jest.fn(),
    };

    render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDef} />
      </ReactMapView>,
    );

    /* Check if IWMJSMap instance was made */
    const map = webmapUtils.getWMJSMapById('map1')!;

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(1);

    /* Check its name */
    expect(map.getLayers()[0].name).toBe('RAD_NL25_PCP_CM');

    await waitFor(() => {
      /* Make sure onLayerReady of the layer was called */
      expect(layerDef.onLayerReady).toHaveBeenCalledWith(
        map.getLayers()[0],
        expect.objectContaining(map),
      );
    });
  });
  it('should order layers when swapped', () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
    };

    /* Create a ReactMapView component without a time dimension and add a layer */

    const layerDefA: LayerOptions = {
      id: 'testlayerA',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
    };

    const layerDefB: LayerOptions = {
      id: 'testlayerB',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
    };

    const layerDefC: LayerOptions = {
      id: 'testlayerC',
      name: 'Reflectivity',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
    };

    const { rerender } = render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDefA} />
        <ReactMapViewLayer {...layerDefB} />
        <ReactMapViewLayer {...layerDefC} />
      </ReactMapView>,
    );

    /* Check if IWMJSMap instance was made */
    const map = webmapUtils.getWMJSMapById('map1')!;

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(3);

    expect(
      map.getLayers().map((layer) => {
        return { id: layer.id, name: layer.name };
      }),
    ).toEqual([
      { id: 'testlayerA', name: 'RAD_NL25_PCP_CM' },
      { id: 'testlayerB', name: 'RAD_NL25_PCP_CM' },
      { id: 'testlayerC', name: 'Reflectivity' },
    ]);

    // Re-order
    rerender(
      <ReactMapView {...{ ...props, showLegend: true }}>
        <ReactMapViewLayer {...layerDefC} />
        <ReactMapViewLayer {...layerDefB} />
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );

    expect(
      map.getLayers().map((layer) => {
        return { id: layer.id, name: layer.name };
      }),
    ).toEqual([
      { id: 'testlayerC', name: 'Reflectivity' },
      { id: 'testlayerB', name: 'RAD_NL25_PCP_CM' },
      { id: 'testlayerA', name: 'RAD_NL25_PCP_CM' },
    ]);

    // Re-order
    rerender(
      <ReactMapView {...{ ...props, showLegend: true }}>
        <ReactMapViewLayer {...layerDefB} />
        <ReactMapViewLayer {...layerDefC} />
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );

    expect(
      map.getLayers().map((layer) => {
        return { id: layer.id, name: layer.name };
      }),
    ).toEqual([
      { id: 'testlayerB', name: 'RAD_NL25_PCP_CM' },
      { id: 'testlayerC', name: 'Reflectivity' },
      { id: 'testlayerA', name: 'RAD_NL25_PCP_CM' },
    ]);

    // Remove one layer
    rerender(
      <ReactMapView {...{ ...props, showLegend: true }}>
        <ReactMapViewLayer {...layerDefB} />
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );

    expect(
      map.getLayers().map((layer) => {
        return { id: layer.id, name: layer.name };
      }),
    ).toEqual([
      { id: 'testlayerB', name: 'RAD_NL25_PCP_CM' },
      { id: 'testlayerA', name: 'RAD_NL25_PCP_CM' },
    ]);

    // Add one layer
    rerender(
      <ReactMapView {...{ ...props, showLegend: true }}>
        <ReactMapViewLayer {...layerDefC} />
        <ReactMapViewLayer {...layerDefB} />
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );

    expect(
      map.getLayers().map((layer) => {
        return { id: layer.id, name: layer.name };
      }),
    ).toEqual([
      { id: 'testlayerC', name: 'Reflectivity' },
      { id: 'testlayerB', name: 'RAD_NL25_PCP_CM' },
      { id: 'testlayerA', name: 'RAD_NL25_PCP_CM' },
    ]);

    // Order back
    rerender(
      <ReactMapView {...{ ...props, showLegend: true }}>
        <ReactMapViewLayer {...layerDefA} />
        <ReactMapViewLayer {...layerDefB} />
        <ReactMapViewLayer {...layerDefC} />
      </ReactMapView>,
    );

    expect(
      map.getLayers().map((layer) => {
        return { id: layer.id, name: layer.name };
      }),
    ).toEqual([
      { id: 'testlayerA', name: 'RAD_NL25_PCP_CM' },
      { id: 'testlayerB', name: 'RAD_NL25_PCP_CM' },
      { id: 'testlayerC', name: 'Reflectivity' },
    ]);
  });

  describe('shouldAutoFetch', () => {
    it('should not set an interval if false', () => {
      jest.useFakeTimers();
      global.setInterval = jest.fn() as unknown as typeof setInterval;

      const props = {
        mapId: 'map1',
        srs: 'EPSG:4326',
        bbox: { left: -180, right: 180, top: 90, bottom: -90 },
        shouldAutoFetch: false,
      };

      render(<ReactMapView {...props} />);

      expect(global.setInterval).toHaveBeenCalledTimes(0);
    });

    it('should set an 1 min interval if true', () => {
      jest.useFakeTimers();
      global.setInterval = jest.fn() as unknown as typeof setInterval;

      const props = {
        mapId: 'map1',
        srs: 'EPSG:4326',
        bbox: { left: -180, right: 180, top: 90, bottom: -90 },
        shouldAutoFetch: true,
      };

      render(<ReactMapView {...props} />);

      const mockedSetInterval = global.setInterval as unknown as jest.Mock<
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any
      >;
      expect(mockedSetInterval).toHaveBeenCalledTimes(1);
      expect(mockedSetInterval.mock.calls[0][1]).toBe(60 * 1000);
    });

    it('should set an 5 min interval if 5', () => {
      jest.useFakeTimers();
      const shouldAutoFetch = 5;
      global.setInterval = jest.fn() as unknown as typeof setInterval;

      const props = {
        mapId: 'map1',
        srs: 'EPSG:4326',
        bbox: { left: -180, right: 180, top: 90, bottom: -90 },
        shouldAutoFetch,
      };

      render(<ReactMapView {...props} />);

      const mockedSetInterval = global.setInterval as unknown as jest.Mock<
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any
      >;
      expect(mockedSetInterval).toHaveBeenCalledTimes(1);
      expect(mockedSetInterval.mock.calls[0][1]).toBe(
        shouldAutoFetch * 60 * 1000,
      );
    });

    it('should restart auto update if set to visible', () => {
      global.setInterval = jest
        .fn()
        .mockReturnValue('refetch timer') as unknown as typeof setInterval;
      global.clearInterval = jest.fn() as unknown as typeof clearInterval;

      const props = {
        mapId: 'map1',
        srs: 'EPSG:4326',
        bbox: { left: -180, right: 180, top: 90, bottom: -90 },
        shouldAutoFetch: true,
      };

      render(<ReactMapView {...props} />);

      const mockedClearInterval = global.clearInterval as unknown as jest.Mock<
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any
      >;

      Object.defineProperty(document, 'visibilityState', {
        value: 'visible',
        writable: true,
      });
      global.document.dispatchEvent(new Event('visibilitychange'));

      expect(mockedClearInterval).toHaveBeenCalledTimes(1);
    });

    it('should force refetch if set to visible', async () => {
      const mockParseWMJSLayer = parseWMJSLayer as jest.Mock;
      mockParseWMJSLayer.mockImplementation(() => Promise.resolve({}));
      const mockGetLayerUpdateInfo = getLayerUpdateInfo as jest.Mock;
      mockGetLayerUpdateInfo.mockImplementation(() => Promise.resolve({}));
      const mockOnUpdateLayerInformation = jest.fn();
      const props = {
        mapId: 'map1',
        onUpdateLayerInformation: mockOnUpdateLayerInformation,
        shouldAutoFetch: true,
      };

      const layerDef: LayerOptions = {
        id: 'testlayer',
        name: 'RAD_NL25_PCP_CM',
        service: 'testservice',
        layerType: LayerType.mapLayer,
        ReactWMJSLayerId: 'test',
      };

      render(
        <ReactMapView {...props}>
          <ReactMapViewLayer {...layerDef} />
        </ReactMapView>,
      );

      /* Check if onUpdateLayerInformation was called */
      await waitFor(() =>
        expect(props.onUpdateLayerInformation).toHaveBeenCalledTimes(1),
      );

      /* Simulate visibility change to trigger refetch */
      Object.defineProperty(document, 'visibilityState', {
        value: 'visible',
        writable: true,
      });
      global.document.dispatchEvent(new Event('visibilitychange'));

      /* Check if onUpdateLayerInformation was called (force refetch) */
      await waitFor(() =>
        expect(props.onUpdateLayerInformation).toHaveBeenCalledTimes(2),
      );
    });

    it('should restart auto update if visible after few seconds', () => {
      jest.useFakeTimers();
      global.setInterval = jest.fn() as unknown as typeof setInterval;

      const props = {
        mapId: 'map1',
        srs: 'EPSG:4326',
        bbox: { left: -180, right: 180, top: 90, bottom: -90 },
        shouldAutoFetch: true,
      };

      render(<ReactMapView {...props} />);
      Object.defineProperty(document, 'visibilityState', {
        value: 'hidden',
        writable: true,
      });
      global.document.dispatchEvent(new Event('visibilitychange'));

      const fewSeconds = 5 * SECOND_TO_MILLISECOND;
      jest.advanceTimersByTime(fewSeconds);

      Object.defineProperty(document, 'visibilityState', {
        value: 'visible',
        writable: true,
      });
      global.document.dispatchEvent(new Event('visibilitychange'));

      const mockedSetInterval = global.setInterval as unknown as jest.Mock<
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any
      >;
      expect(mockedSetInterval).toHaveBeenCalledTimes(2);
    });

    it('should restart auto update if visible after auto fetch interval', () => {
      jest.useFakeTimers();
      global.setInterval = jest.fn() as unknown as typeof setInterval;
      const shouldAutoFetch = 5;

      const props = {
        mapId: 'map1',
        srs: 'EPSG:4326',
        bbox: { left: -180, right: 180, top: 90, bottom: -90 },
        shouldAutoFetch,
      };

      render(<ReactMapView {...props} />);
      Object.defineProperty(document, 'visibilityState', {
        value: 'hidden',
        writable: true,
      });
      global.document.dispatchEvent(new Event('visibilitychange'));

      const afterShouldAutoFetch = shouldAutoFetch * MINUTE_TO_MILLISECOND + 1;
      jest.advanceTimersByTime(afterShouldAutoFetch);

      Object.defineProperty(document, 'visibilityState', {
        value: 'visible',
        writable: true,
      });
      global.document.dispatchEvent(new Event('visibilitychange'));

      const mockedSetInterval = global.setInterval as unknown as jest.Mock<
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any
      >;
      expect(mockedSetInterval).toHaveBeenCalledTimes(2);
    });
  });
  describe('should enable/disable wheel events', () => {
    it('should disable wheel events when isTimeScrollingEnabled is true', async () => {
      const props = {
        mapId: 'map1',
        isTimeScrollingEnabled: true,
      };

      const { rerender } = render(<ReactMapView {...props} />);

      const wmjsMap = webmapUtils.getWMJSMapById(props.mapId);
      if (wmjsMap) {
        const detachWheelEventSpy = jest.spyOn(wmjsMap, 'detachWheelEvent');
        rerender(<ReactMapView {...props} />);
        expect(detachWheelEventSpy).toHaveBeenCalledTimes(1);
      }
    });

    it('should enable wheel events when isTimeScrollingEnabled is false', () => {
      const props = {
        mapId: 'map1',
        isTimeScrollingEnabled: false,
      };

      const { rerender } = render(<ReactMapView {...props} />);

      const wmjsMap = webmapUtils.getWMJSMapById(props.mapId);
      if (wmjsMap) {
        const attachWheelEventSpy = jest.spyOn(wmjsMap, 'attachWheelEvent');
        rerender(<ReactMapView {...props} />);
        expect(attachWheelEventSpy).toHaveBeenCalledTimes(1);
      }
    });
  });
});
