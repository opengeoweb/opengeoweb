/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  LayerOptions,
  WMJSDimension,
  WMJSMap,
  WMLayer,
  mockGetCapabilities,
  setWMSGetCapabilitiesFetcher,
  webmapTestSettings,
  webmapUtils,
} from '@opengeoweb/webmap';

import {
  getCurrentDimensionValue,
  getLayerUpdateInfo,
} from './ReactMapViewParseLayer';

export const mockGeoJSON: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [],
};

describe('src/components/ReactMapView/ReactMapViewParseLayer', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });
  it('should get current dimension value', () => {
    expect(getCurrentDimensionValue('', [])).toBeUndefined();
    expect(
      getCurrentDimensionValue('', [
        { name: 'humid', currentValue: '800' },
      ] as WMJSDimension[]),
    ).toBeUndefined();
    expect(
      getCurrentDimensionValue('humid', [
        { name: 'humid', currentValue: '800' },
      ] as WMJSDimension[]),
    ).toEqual('800');
    expect(
      getCurrentDimensionValue('humid', [
        { name: 'humid', currentValue: '800' },
        { name: 'time', currentValue: '2022-08-10T09:00:00Z' },
      ] as WMJSDimension[]),
    ).toEqual('800');
    expect(
      getCurrentDimensionValue('time', [
        { name: 'humid', currentValue: '800' },
        { name: 'time', currentValue: '2022-08-10T09:00:00Z' },
      ] as WMJSDimension[]),
    ).toEqual('2022-08-10T09:00:00Z');

    expect(
      getCurrentDimensionValue('2022-08-10T09:00:00Z', [
        { name: 'humid', currentValue: '800' },
        { name: 'time' },
      ] as WMJSDimension[]),
    ).toBeUndefined();
  });
  describe('getLayerUpdateInfo', () => {
    it('should get call onUpdateLayerInformation', () => {
      const mapId = 'map1';
      const testLayer = {
        ...webmapTestSettings.multiDimensionLayer,
        id: 'test-multi',
      } as LayerOptions;
      const onUpdateLayerInformation = jest.fn();
      const baseElement = document.createElement('div');
      const wmjsmap = new WMJSMap(baseElement);
      webmapUtils.registerWMJSMap(wmjsmap, mapId);
      wmjsmap.setDimension('time', '2020-03-13T14:40:00Z');

      const wmLayer = new WMLayer(testLayer);
      void wmjsmap.addLayer(wmLayer);

      webmapUtils.registerWMLayer(wmLayer, testLayer.id);

      wmLayer.ReactWMJSLayerId = testLayer.id;
      onUpdateLayerInformation(getLayerUpdateInfo(wmLayer, mapId));

      const result = {
        layerDimensions: {
          dimensions: [
            {
              currentValue: '625',
              maxValue: '625',
              minValue: '25',
              name: 'flight level',
              synced: false,
              timeInterval: undefined,
              units: 'hft',
              values: '25,325,625',
            },
            {
              currentValue: '9000',
              maxValue: '9000',
              minValue: '1000',
              name: 'elevation',
              synced: false,
              timeInterval: undefined,
              units: 'meters',
              values: '1000,5000,9000',
            },
            {
              currentValue: '2020-03-13T14:40:00Z',
              maxValue: '2020-03-13T14:40:00Z',
              minValue: '2020-03-13T14:40:00Z',
              name: 'time',
              synced: false,
              timeInterval: undefined,
              units: 'ISO8601',
              values: undefined,
            },
          ],
          layerId: 'test-multi',
          origin: 'ReactMapViewParseLayer',
        },
        layerStyle: {
          layerId: 'test-multi',
          origin: 'ReactMapViewParseLayer',
          style: '',
        },
        mapDimensions: {
          dimensions: [
            {
              currentValue: '2020-03-13T14:40:00Z',
              name: 'time',
              units: undefined,
            },
          ],
          mapId: 'map1',
          origin: 'ReactMapViewParseLayer',
        },
        origin: 'ReactMapViewParseLayer',
      };

      expect(onUpdateLayerInformation).toHaveBeenCalledWith(result);
    });
  });
});
