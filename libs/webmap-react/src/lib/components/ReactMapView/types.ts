/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import {
  Bbox,
  Dimension,
  IWMJSMap,
  TileServerSettings,
} from '@opengeoweb/webmap';
import { TFunction } from 'i18next';

export interface ReactMapViewProps {
  listeners?: {
    name?: string;
    data: string;
    keep: boolean;
    callbackfunction: (webMap: IWMJSMap, value: string) => void;
  }[];
  isTimeScrollingEnabled?: boolean;
  srs?: string;
  bbox?: Bbox;
  children?: React.ReactNode;
  mapId: string;
  activeLayerId?: string;
  showScaleBar?: boolean;
  showLegend?: boolean;
  tileServerSettings?: TileServerSettings;
  passiveMap?: boolean;
  displayTimeInMap?: boolean;
  animationDelay?: number;
  timestep?: number;
  dimensions?: Dimension[];
  onClick?: () => void;
  displayMapPin?: boolean;
  mapPinLocation?: MapLocation;
  shouldAutoFetch?: number | boolean; // Auto fetch interval in minutes or boolean
  showLayerInfo?: boolean;
  disableMapPin?: boolean;
  holdShiftToScroll?: boolean;
  shouldDisablePrefetching?: boolean;
  t?: TFunction;
  linkedFeatures?: GeoJSON.FeatureCollection;
  /* Callback actions */
  onWMJSMount?: (mapId: string) => void;
  onWMJSUnMount?: (mapId: string) => void;
  onMapChangeDimension?: (payload: SetMapDimensionPayload) => void;
  onMapZoomEnd?: (payload: SetBboxPayload) => void;
  onMapPinChangeLocation?: (payload: MapPinLocationPayload) => void;
  onUpdateLayerInformation?: (payload: UpdateLayerInfoPayload) => void;
}

export interface MapLocation {
  lat: number;
  lon: number;
  screenOffsetX?: number;
  screenOffsetY?: number;
  projectionX?: number;
  projectionY?: number;
  srs?: string;
  id?: string;
  serviceId?: string;
  collectionId?: string;
}

export interface MapPinLocationPayload {
  mapId: string;
  mapPinLocation: MapLocation;
}

export interface SetMapDimensionPayload {
  origin: string;
  mapId: string;
  dimension: Dimension;
}

export interface SetBboxPayload {
  mapId: string;
  bbox: Bbox;
  srs?: string;
  origin?: string;
}

export interface SetStepBackWardOrForward {
  mapId: string;
  isForwardStep: boolean;
}
export interface SetLayerDimensionsPayload {
  layerId: string;
  origin: string;
  dimensions: Dimension[];
}

export interface UpdateAllMapDimensionsPayload {
  origin: string;
  mapId: string;
  dimensions: Dimension[];
}

export interface SetLayerStylePayload {
  layerId: string;
  style: string; // TODO: (Sander de Snaijer, 2020-03-19) Change to name as well
  mapId?: string;
  origin?: string;
}

export interface UpdateLayerInfoPayload {
  origin: string;
  mapDimensions?: UpdateAllMapDimensionsPayload;
  layerStyle?: SetLayerStylePayload;
  layerDimensions?: SetLayerDimensionsPayload;
}
