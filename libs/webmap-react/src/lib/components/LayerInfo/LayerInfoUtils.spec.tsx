/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import {
  DimensionsElevation,
  DimensionsOther,
  DimensionsRefTime,
  DimensionsTime,
} from '@opengeoweb/theme';
import {
  getDimensionLabel,
  getDimensionsList,
  getDimensionValue,
  getLayerBbox,
  getLayerStyles,
} from './LayerInfoUtils';
import { i18n, translateKeyOutsideComponents } from '../../utils/i18n';

describe('src/components/LayerInfo/LayerInfoUtils', () => {
  const layer = {
    name: 'hirlam:isobaric:temperature',
    title: 'HIRLAM Temperature Isobaric',
    leaf: true,
    path: ['models', 'hirlam', 'isobaric'],
    keywords: ['model', 'hirlam', 'temperature', 'isobaric'],
    abstract: 'Lorem ipsum.',
    styles: [
      {
        title: 'countours',
        name: 'external-radiation',
        legendURL: 'https://url',
        abstract: 'Ulkoisen säteilyn annosnopeus',
      },
      {
        title: 'isotherms',
        name: 'external-radiation-2',
        legendURL: 'https://url',
        abstract: 'Ulkoisen säteilyn annosnopeus 2',
      },
    ],
    dimensions: [
      {
        name: 'elevation',
        values: '1000,500,100',
        units: 'hPa',
        currentValue: '1000',
      },
    ],
    geographicBoundingBox: {
      north: '64',
      south: '58',
      east: '24',
      west: '-18',
    },
  };

  describe('getLayerStyles', () => {
    it('should return dash if layer has no styles', () => {
      expect(
        getLayerStyles({
          name: 'name',
          title: 'text',
          leaf: true,
          path: [],
        }),
      ).toEqual('-');
      expect(
        getLayerStyles({
          name: 'name',
          title: 'text',
          leaf: true,
          path: [],
          styles: [],
        }),
      ).toEqual('-');
    });
    it('should return single style if layer has 1 style', () => {
      expect(
        getLayerStyles({
          name: 'name',
          title: 'text',
          leaf: true,
          path: [],
          styles: [layer.styles[0]],
        }),
      ).toEqual(layer.styles[0].title);
    });
    it('should return list of styles if layer has multiple styles', () => {
      expect(getLayerStyles(layer)).toEqual('countours, isotherms');
    });
  });

  describe('getDimensionValue', () => {
    it('should return null if layer does not have the given dimension', () => {
      expect(getDimensionValue('dimName', [])).toEqual(null);
      expect(
        getDimensionValue('dimName', [
          {
            name: 'time',
            values: '2022',
            units: '-',
            currentValue: '2022',
          },
        ]),
      ).toEqual(null);
      expect(
        getDimensionValue('dimName', [
          {
            name: 'time',
            values: null!,
            units: '-',
            currentValue: null!,
          },
        ]),
      ).toEqual(null);
    });
    it('should return correct dimension value', () => {
      expect(
        getDimensionValue('time', [
          {
            name: 'time',
            values: '2021/2022',
            units: '-',
            currentValue: '2022',
          },
        ]),
      ).toEqual('2021/ 2022');
      expect(
        getDimensionValue('reference_time', [
          {
            name: 'reference_time',
            values: '2021/2022,2023',
            units: '-',
            currentValue: '2022',
          },
        ]),
      ).toEqual('2021/2022, 2023');
      expect(
        getDimensionValue('elevation', [
          {
            name: 'ELEVATION',
            values: '100,200,300',
            units: 'hPa',
            currentValue: '100',
          },
        ]),
      ).toEqual('100, 200, 300 hPa');
    });
  });

  describe('getLayerBbox', () => {
    it('should return dash if layer does not have a geographic bounding box', () => {
      expect(
        getLayerBbox(i18n.t, {
          name: 'name',
          title: 'text',
          leaf: true,
          path: [],
        }),
      ).toEqual('-');
    });
    it('should return the geographic bounding box', () => {
      expect(getLayerBbox(i18n.t, layer)).toEqual(
        `${translateKeyOutsideComponents('webmap-react-west')} -18, ${translateKeyOutsideComponents('webmap-react-east')} 24, ${translateKeyOutsideComponents('webmap-react-north')} 64, ${translateKeyOutsideComponents('webmap-react-south')} 58`,
      );
    });
  });

  describe('getDimensionLabel', () => {
    it('should return a formatted label for the dimension name', () => {
      expect(getDimensionLabel('REFERENCE_TIME')).toEqual('Reference time');
      expect(getDimensionLabel('time')).toEqual('Time');
    });
  });

  describe('getDimensionsList', () => {
    it('should return a sorted list of dimension with correct values, labels and icons', () => {
      expect(getDimensionsList([])).toEqual([]);
      expect(getDimensionsList(null!)).toEqual([]);
      expect(
        getDimensionsList([
          {
            name: 'reference_time',
            values: '2022-10-05,2022-10-06',
            units: 'ISO8601',
            currentValue: '2022-10-05',
          },
          {
            name: 'TIME',
            values: '2022-10-04T00:00:00Z/PT1H',
            units: 'ISO8601',
            currentValue: '2022-10-04T00:00:00Z',
          },
          {
            name: 'modellevel',
            values: '1,2,3,4,5',
            units: '-',
            currentValue: '1',
          },
          {
            name: 'Elevation',
            values: '1000,500,100',
            units: 'hPa',
            currentValue: '1000',
          },
          {
            name: 'member',
            values: '1,2,3',
            units: '-',
            currentValue: '1',
          },
        ]),
      ).toEqual([
        {
          icon: <DimensionsTime />,
          label: 'Time',
          value: '2022-10-04T00:00:00Z/ PT1H',
        },
        {
          icon: <DimensionsRefTime />,
          label: 'Reference time',
          value: '2022-10-05, 2022-10-06',
        },
        {
          icon: <DimensionsElevation />,
          label: 'Elevation',
          value: '1000, 500, 100 hPa',
        },
        {
          icon: <DimensionsOther />,
          label: 'Member',
          value: '1, 2, 3 -',
        },
        {
          icon: <DimensionsOther />,
          label: 'Modellevel',
          value: '1, 2, 3, 4, 5 -',
        },
      ]);
    });
  });
});
