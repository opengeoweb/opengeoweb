/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import type { Meta, StoryObj } from '@storybook/react';
import { LayerInfoDialog } from './LayerInfoDialog';

const meta: Meta<typeof LayerInfoDialog> = {
  title: 'components/LayerInfo',
  component: LayerInfoDialog,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the LayerInfoDialog',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof LayerInfoDialog>;

const props = {
  onClose: (): void => {},
  dialogHeight: 985,
  startPosition: { top: 10, left: 10 },
  layer: {
    name: 'hirlam:isobaric:temperature',
    title: 'HIRLAM Temperature Isobaric',
    leaf: true,
    path: ['models', 'hirlam', 'isobaric'],
    keywords: ['model', 'hirlam', 'temperature', 'isobaric'],
    abstract: 'Lorem ipsum dolor sit amet.',
    styles: [
      {
        title: 'countours',
        name: 'external-radiation',
        legendURL: '',
        abstract: 'Ulkoisen säteilyn annosnopeus',
      },
      {
        title: 'isotherms',
        name: 'external-radiation-2',
        legendURL: '',
        abstract: 'Ulkoisen säteilyn annosnopeus 2',
      },
    ],
    dimensions: [
      {
        name: 'time',
        values: '2022-10-04T00:00:00Z/PT1H',
        units: 'ISO8601',
        currentValue: '2022-10-04T00:00:00Z',
      },
      {
        name: 'reference_time',
        values: '2022-10-05,2022-10-06',
        units: 'ISO8601',
        currentValue: '2022-10-05',
      },
      {
        name: 'modellevel',
        values: '1,2,3,4,5',
        units: '-',
        currentValue: '1',
      },
      {
        name: 'elevation',
        values: '1000,500,100',
        units: 'hPa',
        currentValue: '1000',
      },
      {
        name: 'member',
        values: '1,2,3',
        units: '-',
        currentValue: '1',
      },
    ],
    geographicBoundingBox: {
      north: '64',
      south: '58',
      east: '24',
      west: '-18',
    },
  },
  serviceName: 'FMI',
  isOpen: true,
};

export const Component: Story = {
  args: { ...props, dialogHeight: 300 },
  render: (props) => {
    return (
      <div style={{ height: '326px' }}>
        <LayerInfoDialog {...props} />
      </div>
    );
  },
  tags: ['!autodocs'],
};

export const LayerInfoDialogLightDoc: Story = {
  render: (props) => (
    <div style={{ height: 1010 }}>
      <LayerInfoDialog {...props} />
    </div>
  ),
  args: props,
  tags: ['!dev'],
};

export const LayerInfoDialogLight: Story = {
  render: (props) => <LayerInfoDialog {...props} />,
  args: props,
  parameters: {
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62c695a1ed42f01adee34e63/version/633551ddd63e4c4d715ab03d',
      },
    ],
  },
  tags: ['snapshot', '!autodocs'],
};

export const LayerInfoDialogDarkDoc: Story = {
  render: (props) => (
    <div style={{ height: 1010 }}>
      <LayerInfoDialog {...props} />
    </div>
  ),
  args: props,
  tags: ['!dev', 'dark'],
};

export const LayerInfoDialogDark: Story = {
  render: (props) => <LayerInfoDialog {...props} />,
  args: props,
  parameters: {
    zeplinLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62d7b06898ff1c11dbcc0d0a/version/63355206daa88e4eda6803bc',
      },
    ],
  },
  tags: ['snapshot', 'dark', '!autodocs'],
};
