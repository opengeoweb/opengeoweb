/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';

import { LayerInfoLegend } from './LayerInfoLegend';
import { WebmapReactThemeProvider } from '../Providers/Providers';

describe('src/components/LayerInfo/LayerInfoLegend', () => {
  it('should not render if legenURL is not provided', () => {
    const props = {
      title: '',
      name: '',
      dimensions: [],
      legendURL: undefined!,
    };

    render(
      <WebmapReactThemeProvider>
        <LayerInfoLegend {...props} />
      </WebmapReactThemeProvider>,
    );

    const legendImage = screen.queryByRole('img');
    expect(legendImage).toBeFalsy();
  });

  it('should render if legenURL is provided', () => {
    const props = {
      title: 'test title',
      name: 'test name',
      dimensions: [],
      legendURL: 'http:fakeUrl',
    };

    render(
      <WebmapReactThemeProvider>
        <LayerInfoLegend {...props} />
      </WebmapReactThemeProvider>,
    );

    const legendImage = screen.queryByRole('img')!;
    expect(legendImage).toBeTruthy();
    expect(legendImage.getAttribute('alt')).toEqual(props.title);
  });
});
