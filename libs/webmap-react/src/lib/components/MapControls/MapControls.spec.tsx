/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import { MapControls } from './MapControls';

describe('src/components/MapControls/MapControls', () => {
  it('work with default props', async () => {
    const buttonContent = 'button content';
    const TestButton: React.FC = () => (
      <button type="button" data-testid="test-button">
        {buttonContent}
      </button>
    );

    render(
      <ThemeWrapper>
        <MapControls>
          <TestButton />
        </MapControls>
      </ThemeWrapper>,
    );

    expect(screen.getByTestId('test-button')).toBeTruthy();
  });
});
