/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';

import { produce } from 'immer';
import { webmapUtils } from '@opengeoweb/webmap';
import { PROJECTION } from '@opengeoweb/shared';
import type { Meta, StoryObj } from '@storybook/react';
import { FeatureEvent } from './MapDraw';

import { Synops } from './storyComponents/Synops';
import { FeatureInfo } from './storyComponents/FeatureInfo';

import { MapDrawGeoJSON } from './storyComponents/MapDrawGeoJSON';
import { FeatureInfoHTML } from './storyComponents/FeatureInfoHTML';
import { MapDrawDrawFunctionArgs, registerDrawFunction } from './mapDrawUtils';
import { MapView, MapViewLayer } from '../MapView';
import { defaultLayers, publicLayers } from '../../layers';
import {
  simpleGeometryCollectionGeoJSON,
  simpleMultiPolygon,
  simplePointsGeojson,
  simplePolygonGeoJSON,
} from './storyComponents/geojsonExamples';
import { MapWarningProperties } from '../MapWarning';

const meta: Meta<typeof MapDrawGeoJSON> = {
  title: 'components/MapDraw',
  tags: ['!autodocs'],
};
export default meta;

type Story = StoryObj<typeof MapDrawGeoJSON>;

const srsAndBboxDefault = {
  bbox: {
    left: -2324980.5498391856,
    bottom: 5890854.775012179,
    right: 6393377.702660825,
    top: 11652109.058827976,
  },
  srs: PROJECTION.EPSG_3857.value,
};

export const Map1: Story = {
  render: () => (
    <div style={{ height: '100vh' }}>
      <MapDrawGeoJSON />
    </div>
  ),
  tags: ['!autodocs'],
};
Map1.storyName = 'Drawing and editing GeoJSON';

// --- example 2: Display GeoJSON Points
const Map2Demo = (): React.ReactElement => {
  const [selectedFeature, setSelectedFeature] = React.useState<
    FeatureEvent | undefined
  >(undefined);

  const circleDrawFunction = (
    args: MapDrawDrawFunctionArgs,
    fillColor = '#88F',
    radius = 12,
  ): void => {
    const { context: ctx, coord } = args;
    ctx.strokeStyle = '#000';
    ctx.fillStyle = fillColor || '#88F';
    ctx.lineWidth = 1;
    ctx.beginPath();
    ctx.arc(coord.x, coord.y, radius || 10, 0, 2 * Math.PI);
    ctx.fill();
    ctx.stroke();
    ctx.fillStyle = '#000';
    ctx.beginPath();
    ctx.arc(coord.x, coord.y, 2, 0, 2 * Math.PI);
    ctx.fill();
  };
  const circleDrawFunctionId = React.useRef(
    registerDrawFunction(circleDrawFunction),
  );
  const selectedCircleDrawFunctionId = React.useRef(
    registerDrawFunction((args): void => {
      circleDrawFunction(args, '#F00', 12);
    }),
  );

  return (
    <div style={{ height: '100vh' }}>
      <MapView
        mapId={React.useRef<string>(webmapUtils.generateMapId()).current}
        srs={srsAndBboxDefault.srs}
        bbox={srsAndBboxDefault.bbox}
      >
        <MapViewLayer {...publicLayers.baseLayer} />
        <MapViewLayer {...defaultLayers.overLayer} />
        <MapViewLayer
          id={webmapUtils.generateLayerId()}
          geojson={produce(simplePointsGeojson, (draft) => {
            /* Set default drawfunction for each feature */
            draft.features.forEach((feature) => {
              // eslint-disable-next-line no-param-reassign
              feature.properties!.drawFunctionId = circleDrawFunctionId.current;
            });

            /* Change drawfunction for selected feature */
            if (selectedFeature) {
              draft.features[
                selectedFeature.featureIndex
              ].properties!.drawFunctionId =
                selectedCircleDrawFunctionId.current;
            }
          })}
          onClickFeature={(featureResult?: FeatureEvent): void => {
            setSelectedFeature(featureResult);
          }}
        />
      </MapView>
      <div
        style={{
          position: 'absolute',
          left: '50px',
          top: '10px',
          zIndex: 10000,
          backgroundColor: '#CCCCCCC0',
          padding: '20px',
          overflow: 'auto',
          width: '80%',
          fontSize: '11px',
        }}
      >
        {selectedFeature && (
          <pre>{JSON.stringify(selectedFeature, null, 2)}</pre>
        )}
      </div>
    </div>
  );
};

export const Map2: Story = {
  render: Map2Demo,
};

Map2.storyName = 'Display Points GeoJSON';

// --- example 3: Synops along buffered route via WFS CQL
export const Map3: Story = {
  render: () => (
    <div style={{ height: '100vh' }}>
      <Synops />
    </div>
  ),
};

Map3.storyName = 'Synops along buffered route via WFS CQL';

// --- example 4: Custom GetFeatureInfo as JSON
export const Map4: Story = {
  render: () => (
    <div style={{ height: '100vh' }}>
      <FeatureInfo />
    </div>
  ),
};

Map4.storyName = 'Custom GetFeatureInfo as JSON';

// --- example 5: Custom GetFeatureInfo as HTML
export const Map5: Story = {
  render: () => (
    <div style={{ height: '100vh' }}>
      <FeatureInfoHTML />
    </div>
  ),
};
Map5.storyName = 'Custom GetFeatureInfo as HTML';

// --- example 6: Display GeoJSON MultiPolygon
const Map6Demo = (): React.ReactElement => {
  const [selectedFeature, setSelectedFeature] = React.useState<
    FeatureEvent | undefined
  >(undefined);
  return (
    <div style={{ height: '100vh' }}>
      <MapView
        mapId={React.useRef<string>(webmapUtils.generateMapId()).current}
        srs={srsAndBboxDefault.srs}
        bbox={srsAndBboxDefault.bbox}
      >
        <MapViewLayer {...publicLayers.baseLayer} />
        <MapViewLayer {...defaultLayers.overLayer} />
        <MapViewLayer
          id={webmapUtils.generateLayerId()}
          geojson={simplePolygonGeoJSON}
          onClickFeature={(featureResult?: FeatureEvent): void => {
            setSelectedFeature(featureResult);
          }}
        />
      </MapView>
      {selectedFeature && (
        <MapWarningProperties
          selectedFeatureProperties={selectedFeature.feature.properties!}
        />
      )}
    </div>
  );
};

export const Map6: Story = {
  render: Map6Demo,
};

Map6.storyName = 'Display Polygon GeoJSON';

// --- example 7: Display GeoJSON MultiPolygon
const Map7Demo = (): React.ReactElement => {
  const [selectedFeature, setSelectedFeature] = React.useState<
    FeatureEvent | undefined
  >(undefined);
  return (
    <div style={{ height: '100vh' }}>
      <MapView
        mapId={React.useRef<string>(webmapUtils.generateMapId()).current}
        srs={srsAndBboxDefault.srs}
        bbox={srsAndBboxDefault.bbox}
      >
        <MapViewLayer {...publicLayers.baseLayer} />
        <MapViewLayer {...defaultLayers.overLayer} />
        <MapViewLayer
          id={webmapUtils.generateLayerId()}
          geojson={simpleMultiPolygon}
          onClickFeature={(featureResult?: FeatureEvent): void => {
            setSelectedFeature(featureResult);
          }}
        />
      </MapView>
      <div
        style={{
          position: 'absolute',
          left: '50px',
          top: '10px',
          zIndex: 10000,
          backgroundColor: '#CCCCCCC0',
          padding: '20px',
          overflow: 'auto',
          width: '80%',
          fontSize: '11px',
          height: '240px',
        }}
      >
        {selectedFeature && (
          <pre>{JSON.stringify(selectedFeature, null, 2)}</pre>
        )}
      </div>
    </div>
  );
};

export const Map7: Story = {
  render: Map7Demo,
};

Map7.storyName = 'Display MultiPolygon GeoJSON';

// --- example 8: Display GeoJSON GeometryCollection containing MultiPolygons
const Map8Demo = (): React.ReactElement => {
  const [selectedFeature, setSelectedFeature] = React.useState<
    FeatureEvent | undefined
  >(undefined);
  return (
    <div style={{ height: '100vh' }}>
      <MapView
        mapId={React.useRef<string>(webmapUtils.generateMapId()).current}
        srs={srsAndBboxDefault.srs}
        bbox={srsAndBboxDefault.bbox}
      >
        <MapViewLayer {...publicLayers.baseLayer} />
        <MapViewLayer {...defaultLayers.overLayer} />
        <MapViewLayer
          id={webmapUtils.generateLayerId()}
          geojson={simpleGeometryCollectionGeoJSON}
          onClickFeature={(featureResult?: FeatureEvent): void => {
            setSelectedFeature(featureResult);
          }}
        />
      </MapView>
      <div
        style={{
          position: 'absolute',
          left: '50px',
          top: '10px',
          zIndex: 10000,
          backgroundColor: '#CCCCCCC0',
          padding: '20px',
          overflow: 'auto',
          width: '80%',
          fontSize: '11px',
          height: '240px',
        }}
      >
        {selectedFeature && (
          <pre>{JSON.stringify(selectedFeature, null, 2)}</pre>
        )}
      </div>
    </div>
  );
};

export const Map8: Story = {
  render: Map8Demo,
};

Map8.storyName = 'Display GeometryCollection of MultiPolygons GeoJSON';
