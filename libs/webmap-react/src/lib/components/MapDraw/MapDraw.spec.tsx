/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { render } from '@testing-library/react';
import * as webmapModule from '@opengeoweb/webmap';
import { cloneDeep } from 'lodash';
import MapDraw, { MapDrawProps, EDITMODE } from './MapDraw';
import {
  simpleBoxGeoJSON,
  simpleBoxGeoJSONWrongOrder,
  simpleMultiPolygon,
  simplePolygonGeoJSON,
  simpleGeometryCollectionGeoJSON,
} from './storyComponents/geojsonExamples';
import { lineStringCollection } from './geojsonShapes';

describe('components/MapDraw/MapDraw', () => {
  afterEach(() => {
    webmapModule.unRegisterAllWMJSLayersAndMaps();
  });
  it('should render with default props', () => {
    const props = {
      isInEditMode: false,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: simplePolygonGeoJSON,
      mapId: 'test-id-1',
    };

    const { baseElement } = render(<MapDraw {...props} />);

    expect(baseElement.firstChild).toBeTruthy();
    expect(baseElement.firstChild?.firstChild).toBeTruthy();
  });

  it('handle edit mode', () => {
    const props = {
      isInEditMode: false,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: simplePolygonGeoJSON,
    };
    const prevProps = {
      ...props,
      drawMode: '',
    } as unknown as MapDrawProps;

    const testComponent = new MapDraw({
      ...(props as unknown as MapDrawProps),
    });
    const spy = jest.spyOn(testComponent, 'handleDrawMode');

    testComponent.componentDidUpdate(prevProps);

    expect(testComponent.myEditMode).toEqual(EDITMODE.EMPTY);
    expect(spy).toHaveBeenCalledWith(props.drawMode);
  });

  it('handle cancel edit', () => {
    const props = {
      isInEditMode: false,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: simplePolygonGeoJSON,
    };

    const prevProps = {
      ...props,
      isInEditMode: true,
    } as unknown as MapDrawProps;

    const testComponent = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);
    testComponent.myEditMode = EDITMODE.DELETE_FEATURES;
    const spy = jest.spyOn(testComponent, 'cancelEdit');
    const drawspy = jest.spyOn(testComponent, 'handleDrawMode');

    testComponent.componentDidUpdate(prevProps);

    expect(testComponent.myEditMode).toEqual(EDITMODE.EMPTY);
    expect(spy).toHaveBeenCalledWith(true);
    expect(drawspy).not.toHaveBeenCalled();
  });

  it('should redraw when isInEditMode changes', () => {
    const props = {
      isInEditMode: false,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: simplePolygonGeoJSON,
      mapId: 'map_for_testing',
    };

    const prevProps = {
      ...props,
      isInEditMode: true,
    } as unknown as MapDrawProps;

    webmapModule.registerWMJSMap(
      new webmapModule.WMJSMap(document.createElement('div')),
      props.mapId,
    );
    const spy = jest.spyOn(webmapModule.getWMJSMapById(props.mapId)!, 'draw');

    const testComponent = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    testComponent.componentDidUpdate(prevProps);

    expect(testComponent.myEditMode).toEqual(EDITMODE.EMPTY);
    expect(spy).toHaveBeenCalledWith('MapDraw::componentDidUpdateIsInEditMode');
  });

  it('should redraw when selectedFeatureIndex changes', () => {
    const props = {
      isInEditMode: false,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: simplePolygonGeoJSON,
      mapId: 'map_for_testing',
    };

    const prevProps = {
      ...props,
      selectedFeatureIndex: 1,
    } as unknown as MapDrawProps;

    webmapModule.registerWMJSMap(
      new webmapModule.WMJSMap(document.createElement('div')),
      props.mapId,
    );
    const spy = jest.spyOn(webmapModule.getWMJSMapById(props.mapId)!, 'draw');

    const testComponent = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    testComponent.componentDidUpdate(prevProps);

    expect(spy).toHaveBeenCalledWith(
      'MapDraw::componentDidUpdateSelectedFeatureIndex',
    );
  });

  it('should delete point Feature correctly', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              stroke: '#f24a00',
              'stroke-width': 1.5,
              'stroke-opacity': 1,
              fill: '#f24a00',
              'fill-opacity': 0.25,
              selectionType: 'point',
            },
            geometry: {
              type: 'Point',
              coordinates: [6.061753373579083, 52.48421727270884],
            },
          },
        ],
      },
      webmapjs: { draw: jest.fn() },
      mapId: 'test1',
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    webmapModule.registerWMJSMap(
      new webmapModule.WMJSMap(document.createElement('div')),
      props.mapId,
    );
    const spy = jest.spyOn(webmapModule.getWMJSMapById(props.mapId)!, 'draw');

    const featureChangeSpy = jest.spyOn(instanceOfMapDraw, 'featureHasChanged');

    instanceOfMapDraw.mouseIsOverVertexNr = 1;
    instanceOfMapDraw.snappedPolygonIndex = 1;
    instanceOfMapDraw.deleteFeature();

    expect(spy).toHaveBeenCalledWith('MapDraw::deletefeatures');
    expect(featureChangeSpy).toHaveBeenCalledWith('deleteFeature');
  });

  it('handle change snappedPolygonIndex nr correctly', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: simpleMultiPolygon,
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    instanceOfMapDraw.snappedPolygonIndex = 1;
    instanceOfMapDraw.handleGeoJSONUpdate(simpleBoxGeoJSON);
    instanceOfMapDraw.componentDidUpdate(props);
    expect(instanceOfMapDraw.snappedPolygonIndex).toEqual(0);
  });

  it('should call exitDrawModeCallback on double click', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: simpleMultiPolygon,
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    instanceOfMapDraw.mouseDoubleClick();

    expect(props.exitDrawModeCallback).toHaveBeenCalledWith(
      'doubleClicked',
      props.geojson,
    );
  });

  it('should call exitDrawModeCallback with default escaped', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: simpleMultiPolygon,
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    instanceOfMapDraw.handleExitDrawMode();

    expect(props.exitDrawModeCallback).toHaveBeenCalledWith(
      'escaped',
      props.geojson,
    );
  });

  it('should call exitDrawModeCallback when pressing escape key', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: simpleMultiPolygon,
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    instanceOfMapDraw.handleKeyDown({ key: 'Escape' } as KeyboardEvent);

    expect(props.exitDrawModeCallback).toHaveBeenCalledWith(
      'escaped',
      props.geojson,
    );
  });

  it('it should ensure box geometry is unchanged when a correct ordered box is given', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: cloneDeep(simpleBoxGeoJSON),
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    const update = jest.spyOn(props, 'updateGeojson');
    instanceOfMapDraw.featureHasChanged('my-trigger');
    expect(update).toHaveBeenCalledWith(simpleBoxGeoJSON, 'my-trigger');
  });
  it('it should ensure box geometry is always the same when reverse ordered box was given', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: cloneDeep(simpleBoxGeoJSONWrongOrder),
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    const update = jest.spyOn(props, 'updateGeojson');
    instanceOfMapDraw.featureHasChanged('my-trigger');
    expect(update).toHaveBeenCalledWith(simpleBoxGeoJSON, 'my-trigger');
  });

  it('it should ensure box can be dragged correctly', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: cloneDeep(simpleBoxGeoJSONWrongOrder),
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    // Drag the map
    const coordinatesToTranspose = (
      instanceOfMapDraw.geojson.features[0] as GeoJSON.Feature<GeoJSON.Polygon>
    ).geometry.coordinates[0];

    instanceOfMapDraw.snappedGeoCoords = { x: 20, y: 20 };
    instanceOfMapDraw.mouseGeoCoord = { x: 30, y: 40 };

    instanceOfMapDraw.transposePolygon(coordinatesToTranspose);

    expect(coordinatesToTranspose).toEqual([
      [13.676469924610156, 83.56469995688167],
      [13.676469924610156, 80.0554711506609],
      [25.801179052423734, 80.0554711506609],
      [25.801179052423734, 83.56469995688167],
      [13.676469924610156, 83.56469995688167],
    ]);
  });

  it('it should ensure map is drawn when a Polygon geojson is updated', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: cloneDeep(simpleBoxGeoJSON),
      mapId: 'test_id',
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    webmapModule.registerWMJSMap(
      new webmapModule.WMJSMap(document.createElement('div')),
      props.mapId,
    );
    const spy = jest.spyOn(webmapModule.getWMJSMapById(props.mapId)!, 'draw');

    instanceOfMapDraw.handleGeoJSONUpdate(cloneDeep(simpleBoxGeoJSON));

    expect(spy).toHaveBeenCalledWith('MapDraw::handleGeoJSONUpdate');
  });

  it('it should ensure map is drawn when a GeometryCollection geojson is updated', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: cloneDeep(simpleGeometryCollectionGeoJSON),
      mapId: 'test_id',
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    webmapModule.registerWMJSMap(
      new webmapModule.WMJSMap(document.createElement('div')),
      props.mapId,
    );
    const spy = jest.spyOn(webmapModule.getWMJSMapById(props.mapId)!, 'draw');

    instanceOfMapDraw.handleGeoJSONUpdate(
      cloneDeep(simpleGeometryCollectionGeoJSON),
    );

    expect(spy).toHaveBeenCalledWith('MapDraw::handleGeoJSONUpdate');
  });

  it('it should ensure second multiline string feature is drawn correctly when mouse is moved', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'LINESTRING',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      selectedFeatureIndex: 0,
      geojson: cloneDeep(lineStringCollection),
      mapId: 'test_id',
    } as unknown as MapDrawProps;

    const webMapJS = new webmapModule.WMJSMap(document.createElement('div'));
    webmapModule.registerWMJSMap(webMapJS, props.mapId);
    (webMapJS as unknown as { _srs: string })._srs = null!;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    const handleNewFeatureIndexSpy = jest.spyOn(
      instanceOfMapDraw,
      'handleNewFeatureIndex',
    );
    (instanceOfMapDraw as { props: MapDrawProps }).props = {
      ...props,
      selectedFeatureIndex: 1,
    };

    instanceOfMapDraw.componentDidUpdate({
      ...props,
    });

    expect(handleNewFeatureIndexSpy).toHaveBeenCalled();

    instanceOfMapDraw.mouseDown({
      mouseX: 0,
      mouseY: 10,
      leftButton: false,
      rightButton: false,
      mouseDown: true,
    });

    instanceOfMapDraw.mouseMove({
      mouseX: 20,
      mouseY: 30,
      leftButton: false,
      rightButton: false,
      mouseDown: true,
    });
    instanceOfMapDraw.mouseUp({
      mouseX: 20,
      mouseY: 30,
      leftButton: false,
      rightButton: false,
      mouseDown: false,
    });
    expect(props.updateGeojson).toHaveBeenCalledTimes(1);

    expect(instanceOfMapDraw.geojson).toEqual({
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            stroke: '#66F',
            'stroke-width': 5,
            'stroke-opacity': '1',
          },
          geometry: { type: 'LineString', coordinates: [[]] },
        },
        {
          type: 'Feature',
          properties: {
            stroke: '#66F',
            'stroke-width': 5,
            'stroke-opacity': '1',
          },
          geometry: {
            coordinates: [
              [0, 10],
              [20, 30],
            ],
            type: 'LineString',
          },
        },
      ],
    });
  });

  it('should not highlight polygon if conditions are not met', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      selectedFeatureIndex: 0,
      mapId: 'test-map',
      linkedFeatures: {
        features: [{ id: 'feature-1' }],
      },
      geojson: {
        type: 'FeatureCollection',
        features: [
          {
            id: 'feature-1',
            type: 'Feature',
            geometry: {
              type: 'Polygon',
              coordinates: [
                [0, 0],
                [1, 1],
                [2, 2],
                [0, 0],
              ],
            },
            properties: {
              selectionType: 'non-fir',
              'fill-opacity': 0.5,
              stroke: '#000000',
              fill: '#ffffff',
            },
          },
        ],
      },
    } as unknown as MapDrawProps;
    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
    instanceOfMapDraw.mouseOverPolygonFeatureIndex = 1;
    instanceOfMapDraw.mouseOverPolygonCoordinates = [
      { x: 0, y: 0 },
      { x: 1, y: 1 },
    ];
    instanceOfMapDraw.getSelectedFeature = jest.fn().mockReturnValue({
      id: 'feature-1',
      properties: {
        selectionType: 'non-fir',
        'fill-opacity': 0.5,
        stroke: '#000000',
        fill: '#ffffff',
      },
      geometry: {
        type: 'Polygon',
        coordinates: [
          [0, 0],
          [1, 1],
          [2, 2],
          [0, 0],
        ],
      },
    });

    instanceOfMapDraw.beforeDraw(ctx);

    expect(instanceOfMapDraw.mouseOverPolygonFeatureIndex).not.toBe(0);
  });
});
