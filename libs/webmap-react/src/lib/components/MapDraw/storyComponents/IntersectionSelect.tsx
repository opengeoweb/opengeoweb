/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import {
  Grid2 as Grid,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  SelectChangeEvent,
} from '@mui/material';

interface IntersectionElement {
  geojson: GeoJSON.FeatureCollection;
  title: string;
}

interface IntersectionSelectProps {
  intersections: IntersectionElement[];
  onChangeIntersection: (
    geojson: GeoJSON.FeatureCollection,
    title: string,
  ) => void;
  isDisabled?: boolean;
}

export const IntersectionSelect: React.FC<IntersectionSelectProps> = ({
  intersections,
  onChangeIntersection,
  isDisabled = false,
}: IntersectionSelectProps) => {
  const [activeIntersection, setActiveIntersection] = React.useState(0);

  return (
    <Grid size={{ sm: 12 }}>
      <FormControl variant="filled" sx={{ width: '100%' }}>
        <InputLabel id="demo-intersection">Intersection shapes</InputLabel>
        <Select
          labelId="demo-intersection"
          value={activeIntersection.toString()}
          onChange={(event: SelectChangeEvent): void => {
            const featureIndex = parseInt(event.target.value, 10);
            setActiveIntersection(featureIndex);
            const { geojson, title } = intersections[featureIndex];
            onChangeIntersection(geojson, title);
          }}
          disabled={isDisabled}
        >
          {intersections.map((listItem, index) => {
            return (
              <MenuItem key={listItem.title} value={index}>
                {listItem.title}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </Grid>
  );
};

export default IntersectionSelect;
