/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2021 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import { LayerProps, LayerFoundation } from '@opengeoweb/webmap';

interface WMSLayerSelectorProps {
  layer: LayerFoundation;
  layers: LayerProps[];
  onSelectLayer: (layer: LayerProps) => void;
}

const WMSLayerSelector = ({
  layer,
  layers,
  onSelectLayer,
}: WMSLayerSelectorProps): React.ReactElement<WMSLayerSelectorProps> => {
  const handleChange = (event: SelectChangeEvent<string>): void => {
    const selectedLayerIndex = layers.findIndex(
      (l) => l.name === event.target.value,
    );
    onSelectLayer(layers[selectedLayerIndex]);
  };
  const title = 'Layer';
  return (
    <Box sx={{ height: '100%' }}>
      <FormControl size="small">
        <InputLabel size="small">{title}</InputLabel>
        <Select
          size="small"
          value={layer.name}
          label="Layer"
          onChange={handleChange}
        >
          {layers.map((l) => (
            <MenuItem key={l.name} value={l.name!}>
              {l.title}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
};

export default WMSLayerSelector;
