/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2021 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Box, Slider } from '@mui/material';
import { getWMLayerById } from '@opengeoweb/webmap';
import { isArray } from 'lodash';

interface WMSTimeSliderProps {
  selectedISOTime: string;
  layerId: string;
  onSelectTime: (time: string) => void;
}

const WMSTimeSlider = ({
  selectedISOTime,
  layerId,
  onSelectTime,
}: WMSTimeSliderProps): React.ReactElement<WMSTimeSliderProps> | null => {
  // Checks
  if (!selectedISOTime) {
    return null;
  }
  const wmLayer = getWMLayerById(layerId);
  if (!wmLayer) {
    console.warn(`No layer exists for ${layerId}`);
    return null;
  }
  const timeDim = wmLayer.getDimension('time');
  if (!timeDim) {
    console.warn(`No time exists for ${layerId}`);
    return null;
  }
  const handleChange = (_event: Event, value: number | number[]): void => {
    let timeIndex = isArray(value) ? value[0] : value;
    if (timeIndex < 0) {
      timeIndex = 0;
    }
    if (timeIndex > timeDim.size() - 1) {
      timeIndex = timeDim.size() - 1;
    }
    onSelectTime(timeDim.getValueForIndex(timeIndex) as string);
  };

  const timeIndex = timeDim.getIndexForValue(selectedISOTime);

  const timeToDisplay = selectedISOTime.substring(0, 10);
  return (
    <Box sx={{ height: '100%', margin: '0 20px' }}>
      <Slider
        value={timeIndex}
        onChange={handleChange}
        size="small"
        min={0}
        max={timeDim.size()}
        orientation="horizontal"
        valueLabelDisplay="on"
        valueLabelFormat={() => {
          return timeToDisplay;
        }}
      />
    </Box>
  );
};

export default WMSTimeSlider;
