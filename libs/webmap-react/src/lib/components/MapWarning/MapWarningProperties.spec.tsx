/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { MapWarningProperties } from './MapWarningProperties';
import { WebmapReactThemeProvider } from '../Providers/Providers';

describe('src/components/MapWarning/MapWarningProperties', () => {
  const props = {
    details: {
      identifier: '2.49.0.1.578.0.220406064209738.1298',
      severity: 'Severe',
      certainty: 'Certain',
      event: 'Gale',
      onset: '2022-04-06T06:42:09+00:00',
      expires: '2022-04-06T14:00:00+00:00',
      languages: [
        {
          areaDesc: 'Uusimaa ja Kymenlaakso',
          language: 'fi-FI',
          event: 'Metsäpalovaroitus',
          senderName: 'Ilmatieteen laitos',
          headline:
            'Oranssi metsäpalovaroitus: Uusimaa ja Kymenlaakso, to 0.00 - 23.53',
          description:
            'Metsäpalojen vaara on erittäin suuri keskiviikon ja torstain välisestä yöstä torstain ja perjantain väliseen yöhön 30 % todennäköisyydellä.',
        },
        {
          areaDesc: 'Nyland och Kymmenedalen',
          language: 'sv-FI',
          event: 'Varning för skogsbrand',
          senderName: 'Meteorologiska institutet',
          headline:
            'Orange varning för skogsbrand: Nyland och Kymmenedalen, to 0.00 - 23.53',
          description:
            'Risken för skogsbränder är mycket hög på från och med natten mellan onsdag och torsdag fram till natten mellan torsdag och fredag med 30 % sannolikhet.',
        },
        {
          areaDesc: 'Uusimaa and Kymenlaakso',
          language: 'en-GB',
          event: 'Forest fire warning',
          senderName: 'Finnish Meteorological Institute',
          headline:
            'Orange forest fire warning: Uusimaa and Kymenlaakso, Thu 0.00 - 23.53',
          description:
            'A risk of forest fires is very high on from the night between Wednesday and Thursday to the night between Thursday and Friday with probability of 30 %.',
        },
      ],
    },
  };

  it('should render successfully', async () => {
    render(
      <WebmapReactThemeProvider>
        <MapWarningProperties selectedFeatureProperties={props.details} />
      </WebmapReactThemeProvider>,
    );

    expect(await screen.findByTestId('map-warning-properties')).toBeTruthy();
  });

  it('should show location as title', () => {
    render(<MapWarningProperties selectedFeatureProperties={props.details} />);

    expect(screen.getByTestId('map-warning-properties-title')).toBeTruthy();

    expect(
      screen.getByTestId('map-warning-properties-title').textContent,
    ).toEqual(props.details.languages[0].areaDesc);
  });

  it('should show title in configured language', () => {
    render(
      <MapWarningProperties
        selectedFeatureProperties={props.details}
        languageIndex={1}
      />,
    );

    expect(screen.getByTestId('map-warning-properties-title')).toBeTruthy();

    expect(
      screen.getByTestId('map-warning-properties-title').textContent,
    ).toEqual(props.details.languages[1].areaDesc);
  });

  it('should show details', () => {
    render(<MapWarningProperties selectedFeatureProperties={props.details} />);

    expect(screen.getAllByTestId('map-warning-properties-row')).toHaveLength(5);
  });

  it('should not show empty fields', () => {
    const propsEithEmptyDetails = {
      details: {
        identifier: '',
        severity: '',
        certainty: '',
        event: '',
        onset: '',
        expires: '',
        languages: [
          {
            areaDesc: '',
            language: '',
            event: '',
            senderName: '',
            headline: '',
            description: '',
          },
          {
            areaDesc: '',
            language: '',
            event: '',
            senderName: '',
            headline: '',
            description: '',
          },
          {
            areaDesc: '',
            language: '',
            event: '',
            senderName: '',
            headline: '',
            description: '',
          },
        ],
      },
    };
    render(
      <MapWarningProperties
        selectedFeatureProperties={propsEithEmptyDetails.details}
      />,
    );

    expect(screen.queryByTestId('map-warning-properties-row')).toBeFalsy();
  });
});
