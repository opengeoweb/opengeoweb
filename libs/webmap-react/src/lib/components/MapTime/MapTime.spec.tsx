/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { dateUtils } from '@opengeoweb/shared';
import { render, screen } from '@testing-library/react';
import { Dimension } from '@opengeoweb/webmap';
import {
  MapTime,
  formatTime,
  getTimeDimension,
  defaultTimeFormat,
} from './MapTime';

describe('src/components/MapTime/MapTime', () => {
  it('should show map time', () => {
    const props = {
      dimensions: [
        {
          currentValue: new Date().toISOString(),
          name: 'time',
        },
      ],
    };
    render(<MapTime {...props} />);

    const time = formatTime(
      getTimeDimension(props.dimensions).currentValue,
      defaultTimeFormat,
    );

    expect(screen.getByTestId('map-time')).toBeTruthy();

    // time should be correctly formatted
    expect(screen.getByText(time).innerHTML).toEqual(time);
  });

  it('should show map time with custom timeFormat', () => {
    const props = {
      dimensions: [
        {
          currentValue: new Date().toISOString(),
          name: 'time',
        },
      ],
      timeFormat: 'dd-MMM-yyyy',
    };
    render(<MapTime {...props} />);

    const time = formatTime(
      getTimeDimension(props.dimensions).currentValue,
      props.timeFormat,
    );

    expect(screen.getByTestId('map-time')).toBeTruthy();

    // time should be correctly formatted
    expect(screen.getByText(time).innerHTML).toEqual(time);
  });

  it('should not render when dimensions are empty', () => {
    const props = {
      dimensions: [],
    };
    const { container } = render(<MapTime {...props} />);

    expect(container.innerHTML).toHaveLength(0);
  });

  it('should return timeDimension', () => {
    const dimensions = [
      { name: 'date', currentValue: new Date().toISOString() },
      {},
      { name: 'time', currentValue: new Date().toISOString() },
    ] as Dimension[];

    expect(getTimeDimension(dimensions)).toEqual(dimensions[2]);

    const noDimensions = [
      { name: 'date', currentValue: new Date().toISOString() },
      {},
      { name: 'time' },
    ] as Dimension[];

    expect(getTimeDimension(noDimensions)).toBeUndefined();
  });

  it('should formatTime', () => {
    const time = new Date().toISOString();
    const expectedResult = dateUtils.dateToString(
      dateUtils.utc(time),
      defaultTimeFormat,
    );

    expect(formatTime(time, defaultTimeFormat)).toEqual(expectedResult);
  });
});
