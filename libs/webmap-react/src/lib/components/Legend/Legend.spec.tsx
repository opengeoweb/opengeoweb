/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import {
  LayerOptions,
  WMEmptyLayerTitle,
  WMLayer,
  webmapTestSettings,
  webmapUtils,
} from '@opengeoweb/webmap';
import { ThemeWrapper } from '@opengeoweb/theme';
import { WebmapReactThemeProvider } from '../Providers/Providers';
import { Legend } from './Legend';

describe('src/components/LegendOverview/Legend', () => {
  it('should not show a legend when there is no layer', () => {
    render(
      <WebmapReactThemeProvider>
        <Legend layer={null!} />
      </WebmapReactThemeProvider>,
    );

    expect(screen.queryByTestId('legend')).toBeFalsy();
  });

  it('should show the layer name, time and all dimensions for a layer', () => {
    const testLayer = {
      ...webmapTestSettings.multiDimensionLayer,
      id: 'test-multi',
    } as LayerOptions;
    const wmLayer = new WMLayer(testLayer);
    webmapUtils.registerWMLayer(wmLayer, testLayer.id);
    render(
      <ThemeWrapper>
        <Legend layer={testLayer} />
      </ThemeWrapper>,
    );

    expect(screen.getByTestId('legend')).toBeTruthy();
    expect(screen.getByTestId('legend-title').textContent).toEqual(
      testLayer.name,
    );

    testLayer.dimensions!.forEach((dimension) => {
      if (dimension.name !== 'time') {
        expect(screen.getByTestId('legend-info').textContent).toContain(
          `${dimension.name}:${dimension.currentValue} ${dimension.units}`,
        );
      } else {
        expect(screen.getByTestId('legend-info').textContent).toContain(
          `${dimension.currentValue}`,
        );
      }
    });
  });

  it('should show the layer graphic when it is available', () => {
    webmapUtils.registerWMLayer(
      webmapTestSettings.WmdefaultReduxLayerRadarKNMI,
      webmapTestSettings.WmdefaultReduxLayerRadarKNMI.id,
    );
    webmapTestSettings.WmdefaultReduxLayerRadarKNMI.legendGraphic =
      'https://SOMEMOCKURLFORLEGEND';
    render(
      <ThemeWrapper>
        <Legend layer={webmapTestSettings.defaultReduxLayerRadarKNMI} />
      </ThemeWrapper>,
    );

    expect(screen.getByRole('button')).toBeTruthy();
  });

  it('should not show a legend when the layer is disabled', () => {
    const testLayer = {
      ...webmapTestSettings.defaultReduxLayerRadarKNMI,
      id: 'test-disabled',
      enabled: false,
    } as LayerOptions;
    const wmLayer = new WMLayer(testLayer);
    webmapUtils.registerWMLayer(wmLayer, testLayer.id);
    render(
      <ThemeWrapper>
        <Legend layer={testLayer} />
      </ThemeWrapper>,
    );

    expect(screen.queryByTestId('legend')).toBeFalsy();
  });

  it('should show the layer title in the tooltip on hover', async () => {
    const testLayer = {
      ...webmapTestSettings.defaultReduxLayerRadarKNMI,
      id: 'test-tooltip',
      enabled: true,
      title: 'TEST_LAYER',
    } as LayerOptions;
    const wmLayer = new WMLayer(testLayer);
    webmapUtils.registerWMLayer(wmLayer, testLayer.id);
    wmLayer.legendGraphic = 'https://mockurl';
    render(
      <ThemeWrapper>
        <Legend layer={testLayer} />
      </ThemeWrapper>,
    );

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.mouseOver(screen.getByRole('button')!);
    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip.textContent).toEqual(testLayer.title);
  });

  it('should show "empty layer" in the tooltip on hover when the layer has no title', async () => {
    const testLayer = {
      ...webmapTestSettings.defaultReduxLayerRadarKNMI,
      id: 'test-no-title',
      enabled: true,
      title: null!,
    } as LayerOptions;
    const wmLayer = new WMLayer(testLayer);
    webmapUtils.registerWMLayer(wmLayer, testLayer.id);
    render(
      <ThemeWrapper>
        <Legend layer={testLayer} />
      </ThemeWrapper>,
    );

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.mouseOver(screen.getByRole('button')!);
    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip.textContent).toEqual(WMEmptyLayerTitle);
  });

  it('should show "loading" in the tooltip on hover when the layer is not registered yet', async () => {
    const testLayer = {
      ...webmapTestSettings.defaultReduxLayerRadarKNMI,
      id: 'test-not-registered',
      enabled: true,
    };
    render(
      <ThemeWrapper>
        <Legend layer={testLayer} />
      </ThemeWrapper>,
    );

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.mouseOver(screen.getByRole('button')!);
    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip.textContent).toEqual('loading');
  });
});
