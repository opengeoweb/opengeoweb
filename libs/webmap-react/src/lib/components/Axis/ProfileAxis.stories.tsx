/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { generateMapId, WMBBOX } from '@opengeoweb/webmap';
import type { Meta, StoryObj } from '@storybook/react';
import { ReactMapView } from '../ReactMapView';
import { ProfileAxis } from './ProfileAxis';

const meta: Meta<typeof ProfileAxis> = {
  title: 'components/ProfileSounding',
  component: ProfileAxis,
  parameters: {
    docs: {
      description: {
        component: 'A component to show ProfileSounding on or next to a map',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof ProfileAxis>;

export const Component: Story = {
  render: () => {
    const bbox = new WMBBOX({
      left: new Date('2023-12-03T00:00:00Z').getTime(),
      bottom: -500,
      right: new Date('2023-12-03T24:00:00Z').getTime(),
      top: 15000,
    });
    const mapId = generateMapId();

    return (
      <div
        style={{
          height: '200px',
          width: '100%',
          border: '1px solid grey',
          margin: 0,
        }}
      >
        <ProfileAxis mapId={mapId} />
        <ReactMapView mapId={mapId} srs="GFI:TIME_ELEVATION" bbox={bbox} />
      </div>
    );
  },
  tags: ['!autodocs'],
};

export const ProfileAxisStory: Story = {
  parameters: {
    docs: {
      description: {
        story: 'Variations of ProfileAxis on a map',
      },
    },
  },
  render: () => {
    const bbox = new WMBBOX({
      left: new Date('2023-12-03T00:00:00Z').getTime(),
      bottom: -500,
      right: new Date('2023-12-03T24:00:00Z').getTime(),
      top: 15000,
    });
    const extraStyle = {
      border: '1px solid grey',
      marginBottom: '8px',
      minHeight: '150px',
    };

    return (
      <div style={{ height: '80vh' }}>
        <div
          style={{
            height: '30%',
            width: '100%',
            ...extraStyle,
          }}
        >
          <ProfileAxis mapId="mapId1" />
          <ReactMapView mapId="mapId1" srs="GFI:TIME_ELEVATION" bbox={bbox} />
        </div>
        <div
          style={{
            height: '50%',
            width: '90%',
            ...extraStyle,
          }}
        >
          <ProfileAxis mapId="mapId2" />
          <ReactMapView mapId="mapId2" srs="GFI:TIME_ELEVATION" bbox={bbox} />
        </div>
        <div
          style={{
            height: '15%',
            width: '25%',
            ...extraStyle,
          }}
        >
          <ProfileAxis mapId="mapId3" />
          <ReactMapView mapId="mapId3" srs="GFI:TIME_ELEVATION" bbox={bbox} />
        </div>
      </div>
    );
  },
};

ProfileAxisStory.storyName = 'MapView with ProfileAxis';
