/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { render, waitFor } from '@testing-library/react';
import 'jest-canvas-mock';

import {
  LayerType,
  WMBBOX,
  WMJSMap,
  WMLayer,
  mockGetCapabilities,
  registerWMJSMap,
  setWMSGetCapabilitiesFetcher,
  unRegisterAllWMJSLayersAndMaps,
} from '@opengeoweb/webmap';
import { ProfileAxis } from './ProfileAxis';

describe('components/Axis/ProfileAxis', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });

  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
    unRegisterAllWMJSLayersAndMaps();
  });
  it('should show empty component', () => {
    const mapId = 'mapid1';
    const { baseElement } = render(<ProfileAxis mapId={mapId} />);
    expect(baseElement.innerHTML).toEqual('<div></div>');
  });
  it('should NOT register to WMJSMap beforecanvasdisplay listener when correct projection is NOT set', () => {
    const mapId = 'mapid1';
    const baseElement = document.createElement('div');
    const wmjsmap = new WMJSMap(baseElement);
    registerWMJSMap(wmjsmap, mapId);
    expect(wmjsmap.getProjection().srs).toBe('EPSG:4326');
    const listener = jest.spyOn(wmjsmap.getListener(), 'addEventListener');
    render(<ProfileAxis mapId={mapId} />);
    expect(listener).not.toHaveBeenCalled();
  });
  it('should register to WMJSMap beforecanvasdisplay listener when correct projection is set', () => {
    const mapId = 'mapid1';
    const baseElement = document.createElement('div');
    const wmjsmap = new WMJSMap(baseElement);
    registerWMJSMap(wmjsmap, mapId);
    wmjsmap.setProjection('GFI:TIME_ELEVATION');
    expect(wmjsmap.getProjection().srs).toBe('GFI:TIME_ELEVATION');
    const addToCallbackListener = jest.spyOn(
      wmjsmap.getListener(),
      'addEventListener',
    );
    const removeFromCallbackListener = jest.spyOn(
      wmjsmap.getListener(),
      'removeEvents',
    );
    const { unmount } = render(<ProfileAxis mapId={mapId} />);
    expect(addToCallbackListener).toHaveBeenCalledWith(
      'beforecanvasdisplay',
      expect.any(Function),
      true,
    );
    unmount();
    expect(removeFromCallbackListener).toHaveBeenCalledWith(
      'beforecanvasdisplay',
      expect.any(Function),
    );
  });

  it('should draw axis based on WMJSMap projection settings', async () => {
    const mapId = 'mapid1';
    const baseElement = document.createElement('div');

    const wmjsmap = new WMJSMap(baseElement);
    wmjsmap.setSize(1000, 1000);
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    registerWMJSMap(wmjsmap, mapId);
    wmjsmap.setProjection({
      srs: 'GFI:TIME_ELEVATION',
      bbox: new WMBBOX({
        left: 0,
        bottom: 0,
        right: 86400,
        top: 50,
      }),
    });

    const setTimeOffset = jest.spyOn(wmjsmap, 'setTimeOffset');
    const setMessage = jest.spyOn(wmjsmap, 'setMessage');
    const fillRectOnCanvas = jest.spyOn(
      ctx as CanvasRenderingContext2D,
      'fillRect',
    );
    expect(setTimeOffset).not.toHaveBeenCalled();
    expect(setMessage).not.toHaveBeenCalled();

    render(<ProfileAxis mapId={mapId} />);

    await waitFor(() => {
      expect(setTimeOffset).toHaveBeenCalledWith('');
    });

    wmjsmap.getListener().triggerEvent('beforecanvasdisplay', ctx);

    await waitFor(() => {
      expect(setMessage).toHaveBeenCalledWith(
        'Thu Jan 01 1970, 0 to 0 hours UTC',
      );
    });
    expect(fillRectOnCanvas).toHaveBeenNthCalledWith(1, 0, 950, 1000, 50);
    expect(fillRectOnCanvas).toHaveBeenNthCalledWith(2, 0, 0, 80, 1000);
    expect(fillRectOnCanvas).toHaveBeenNthCalledWith(3, 0, 940, 80, 60);

    // Test if the layer name is displayed
    const layer = new WMLayer({
      name: 'testlayer',
      id: 'testlayer',
      layerType: LayerType.mapLayer,
      service: 'http://profilestationservice/',
    });
    void wmjsmap.addLayer(layer);

    const fillTextOnCanvas = jest.spyOn(
      ctx as CanvasRenderingContext2D,
      'fillText',
    );
    wmjsmap.getListener().triggerEvent('beforecanvasdisplay', ctx);

    expect(fillTextOnCanvas).toHaveBeenCalledWith('Layer: testlayer', 10, 988);

    // Test if the dashed mousecursor displays
    wmjsmap.mouseMove(100, 100);
    const setLineDashOnCanvas = jest.spyOn(
      ctx as CanvasRenderingContext2D,
      'setLineDash',
    );
    wmjsmap.getListener().triggerEvent('beforecanvasdisplay', ctx);
    expect(setLineDashOnCanvas).toHaveBeenCalledWith([15, 5]);
  });
});
