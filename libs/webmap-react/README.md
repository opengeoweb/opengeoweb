# webmap-react

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test webmap-react` to execute the unit tests via [Jest](https://jestjs.io).

### TypeScript Documentation

- [TypeScript Docs](https://opengeoweb.gitlab.io/opengeoweb/typescript-docs/webmap-react/)
