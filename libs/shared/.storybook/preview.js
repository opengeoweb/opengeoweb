/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { SharedStoryWrapper } from '../src/lib/components/Providers';
import '../../../libs/theme/src/lib/components/Theme/styles.css';
import * as previewHelpers from '../../../.storybook/preview';

export const decorators = [
  (Story, params) => {
    const theme = params.tags.indexOf('dark') !== -1 ? 'dark' : 'light';
    return (
      <SharedStoryWrapper theme={theme}>
        <Story />
      </SharedStoryWrapper>
    );
  },
];

const parameters = {
  parameters: {
    viewport: previewHelpers.parameters.viewport,
    docs: previewHelpers.parameters.docs,
  },
  tags: ['autodocs'],
};

export default parameters;
