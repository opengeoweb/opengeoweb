/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { render, fireEvent, screen } from '@testing-library/react';
import { ConfirmationDialog, ConfirmationDialogProps } from '.';
import { SharedI18nProvider, SharedThemeProvider } from '../Providers';

describe('src/components/ConfirmationDialog/ConfirmationDialog', () => {
  it('should trigger correct callbacks', () => {
    const props: ConfirmationDialogProps = {
      open: true,
      title: 'dialog',
      onSubmit: jest.fn(),
      onClose: jest.fn(),
    };
    render(
      <SharedI18nProvider>
        <SharedThemeProvider>
          <ConfirmationDialog {...props} />
        </SharedThemeProvider>
      </SharedI18nProvider>,
    );

    // confirm
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    expect(props.onSubmit).toHaveBeenCalled();

    // cancel
    fireEvent.click(screen.getByTestId('confirmationDialog-cancel'));
    expect(props.onClose).toHaveBeenCalledWith('CANCELLED');

    // close
    fireEvent.click(screen.getByTestId('customDialog-close'));
    expect(props.onClose).toHaveBeenCalledWith('CLOSED');
  });

  it('should focus on confirm button', () => {
    const props: ConfirmationDialogProps = {
      open: true,
      title: 'dialog',
      onSubmit: jest.fn(),
      onClose: jest.fn(),
    };
    render(
      <SharedThemeProvider>
        <ConfirmationDialog {...props} />
      </SharedThemeProvider>,
    );

    expect(
      screen.getByTestId('confirmationDialog-confirm').matches(':focus'),
    ).toBeTruthy();
  });

  it('should be able to disable autoFocus', () => {
    const props: ConfirmationDialogProps = {
      open: true,
      title: 'dialog',
      onSubmit: jest.fn(),
      onClose: jest.fn(),
      disableAutoFocus: true,
    };
    render(
      <SharedThemeProvider>
        <ConfirmationDialog {...props} />
      </SharedThemeProvider>,
    );

    const confirm = screen.getByTestId('confirmationDialog-confirm');
    expect(confirm.classList.contains('Mui-focusVisible')).toBeFalsy();
  });

  it('should show a spinner instead of confirm button while loading', () => {
    const props: ConfirmationDialogProps = {
      open: true,
      title: 'dialog',
      onSubmit: jest.fn(),
      onClose: jest.fn(),
      isLoading: true,
    };
    render(
      <SharedThemeProvider>
        <ConfirmationDialog {...props} />
      </SharedThemeProvider>,
    );

    expect(screen.getByTestId('confirm-dialog-spinner')).toBeTruthy();
    expect(screen.queryByTestId('confirmationDialog-confirm')).toBeFalsy();
  });

  it('should show confirm button instead of spinner when not loading', () => {
    const props: ConfirmationDialogProps = {
      open: true,
      title: 'dialog',
      onSubmit: jest.fn(),
      onClose: jest.fn(),
      isLoading: false,
    };
    render(
      <SharedThemeProvider>
        <ConfirmationDialog {...props} />
      </SharedThemeProvider>,
    );

    expect(screen.queryByTestId('confirm-dialog-spinner')).toBeFalsy();
    expect(screen.getByTestId('confirmationDialog-confirm')).toBeTruthy();
  });
});
