/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */
import type { Meta, StoryObj } from '@storybook/react';
import { DialogActions } from '@mui/material';
import { ConfirmationButtons } from './ConfirmationButtons';
import { StorybookDocsWrapper } from '../Storybook';

const meta: Meta<typeof ConfirmationButtons> = {
  title: 'components/ConfirmationDialog/ConfirmationButtons',
  component: ConfirmationButtons,
  parameters: {
    docs: {
      description: {
        component:
          'A component for showing the ConfirmationButtons used inside a (custom) dialog',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof ConfirmationButtons>;

export const Component: Story = {
  args: {
    disableAutoFocus: true,
    isLoading: false,
  },
  render: (props) => (
    <DialogActions
      sx={{
        padding: 1,
      }}
    >
      <ConfirmationButtons {...props} />
    </DialogActions>
  ),
};

export const ComponentDark: Story = {
  ...Component,
  tags: ['dark'],
  render: (props) => (
    <StorybookDocsWrapper isDark>
      <DialogActions
        sx={{
          padding: 1,
        }}
      >
        <ConfirmationButtons {...props} />
      </DialogActions>
    </StorybookDocsWrapper>
  ),
};

export const Loading: Story = {
  ...Component,
  args: {
    disableAutoFocus: true,
    isLoading: true,
  },
};

export const CustomLabels: Story = {
  ...Component,
  args: {
    cancelLabel: 'Cancel',
    confirmLabel: 'Confirm',
  },
};
