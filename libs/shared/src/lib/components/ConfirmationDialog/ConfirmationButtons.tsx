/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Button, Box, CircularProgress } from '@mui/material';
import { DialogAction } from '../CustomDialog';
import { useSharedTranslation } from '../../utils/i18n';

const confirmStyle = {
  width: { xs: '100%', sm: 'auto' },
  minWidth: { xs: '100%', sm: '150px' },
  marginLeft: { xs: '0px!important', sm: '8px!important' },
  marginTop: { xs: 2, sm: 0 },
};

export type ConfirmationAction = 'CANCELLED' | DialogAction;

export interface ConfirmationButtonsProps {
  confirmLabel?: string;
  cancelLabel?: string;
  disableAutoFocus?: boolean;
  onSubmit: () => void;
  onClose: (reason: ConfirmationAction) => void;
  isLoading?: boolean;
}

export const ConfirmationButtons: React.FC<ConfirmationButtonsProps> = ({
  onClose,
  cancelLabel,
  isLoading,
  onSubmit,
  disableAutoFocus,
  confirmLabel,
}) => {
  const { t } = useSharedTranslation();

  return (
    <>
      <Button
        data-testid="confirmationDialog-cancel"
        variant="tertiary"
        sx={(theme) => ({
          width: {
            xs: '100%',
            sm: '150px',
          },
          '&:focus': {
            outline: theme.palette.geowebColors.buttons.primary.focus.outline,
            borderColor: 'transparent',
          },
        })}
        onClick={(): void => onClose('CANCELLED')}
      >
        {cancelLabel ?? t('shared-confirmation-cancel')}
      </Button>
      {isLoading ? (
        <Box
          data-testid="confirm-dialog-spinner"
          sx={{
            ...confirmStyle,
            height: '40px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <CircularProgress size={40} color="secondary" />
        </Box>
      ) : (
        <Button
          data-testid="confirmationDialog-confirm"
          variant="primary"
          sx={(theme) => ({
            ...confirmStyle,
            '&:focus': {
              outline: theme.palette.geowebColors.buttons.primary.focus.outline,
              borderColor: 'transparent',
            },
          })}
          onClick={onSubmit}
          autoFocus={!disableAutoFocus}
        >
          {confirmLabel ?? t('shared-confirmation-submit')}
        </Button>
      )}
    </>
  );
};
