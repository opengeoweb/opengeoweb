/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import {
  render,
  fireEvent,
  waitFor,
  renderHook,
  act,
  screen,
} from '@testing-library/react';
import {
  ConfirmationServiceProvider,
  useDialogActions,
  useConfirmationDialog,
} from './ConfirmationService';
import { SharedI18nProvider, SharedThemeProvider } from '../Providers';

interface WrapperProps {
  children: React.ReactNode;
}
const Wrapper: React.FC<WrapperProps> = ({ children }: WrapperProps) => {
  return (
    <SharedI18nProvider>
      <SharedThemeProvider>
        <ConfirmationServiceProvider>{children}</ConfirmationServiceProvider>
      </SharedThemeProvider>
    </SharedI18nProvider>
  );
};

describe('components/ConfirmationDialog/ConfirmationService', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should be able wrap a component with confirmDialog', async () => {
    const options = {
      title: 'test title',
      description: 'test description',
    };
    const TestComp: React.FC = () => {
      const confirmDialog = useConfirmationDialog();
      return (
        <button
          type="button"
          onClick={async (): Promise<void> => {
            await confirmDialog(options);
          }}
        >
          simple confirm
        </button>
      );
    };

    render(
      <Wrapper>
        <TestComp />
      </Wrapper>,
    );

    expect(screen.queryByTestId('confirmationDialog')).toBeFalsy();
    const button = screen.getByRole('button')!;

    fireEvent.click(button);
    await waitFor(() => {
      expect(screen.getByTestId('confirmationDialog')).toBeTruthy();
    });
    expect(
      screen
        .getByTestId('confirmationDialog')!
        .textContent!.includes(options.title),
    ).toBeTruthy();
    expect(
      screen
        .getByTestId('confirmationDialog')!
        .textContent!.includes(options.description),
    ).toBeTruthy();

    // close it again
    fireEvent.click(screen.queryByTestId('customDialog-close')!);
    await waitFor(() => {
      expect(screen.queryByTestId('confirmationDialog')).toBeFalsy();
    });
  });

  describe('useDialogActions', () => {
    it('should handle close', async () => {
      const ref = {
        current: {
          reject: jest.fn(),
          resolve: jest.fn(),
        },
      };
      const { result } = renderHook(() =>
        useDialogActions(ref, { title: 'test', catchOnCancel: true }),
      );
      await act(async () => {
        const { handleClose, confirmationState } = result.current;
        expect(confirmationState).toEqual({
          title: 'test',
          catchOnCancel: true,
        });
        handleClose('CLOSED');
        expect(ref.current.reject).toHaveBeenCalled();
        jest.runAllTimers();
      });

      await act(async () => {
        const { confirmationState } = result.current;
        expect(confirmationState).toBeNull();
      });
    });
    it('should handle submit', async () => {
      const ref = {
        current: {
          reject: jest.fn(),
          resolve: jest.fn(),
        },
      };
      const { result } = renderHook(() =>
        useDialogActions(ref, { title: 'test', catchOnCancel: true }),
      );
      await act(async () => {
        const { handleSubmit, confirmationState } = result.current;
        expect(confirmationState).toEqual({
          title: 'test',
          catchOnCancel: true,
        });
        handleSubmit();
        expect(ref.current.resolve).toHaveBeenCalled();
        jest.runAllTimers();
      });
      await act(async () => {
        const { confirmationState } = result.current;
        expect(confirmationState).toBeNull();
      });
    });
    it('should work without provided ref', async () => {
      const { result } = renderHook(() => useDialogActions({ current: null! }));
      await act(async () => {
        const { handleSubmit, confirmationState } = result.current;
        handleSubmit();
        expect(confirmationState).toBeNull();
      });
    });

    it('should not trigger callback when not mounted', async () => {
      const TestComp: React.FC = () => {
        const ref = {
          current: {
            reject: jest.fn(),
            resolve: jest.fn(),
          },
        };
        const options = { title: 'testing' };
        const { handleClose } = useDialogActions(ref, options);
        return (
          <button
            type="button"
            onClick={(): void => {
              handleClose('CLOSED');
            }}
          >
            simple confirm
          </button>
        );
      };

      const { unmount } = render(
        <Wrapper>
          <TestComp />
        </Wrapper>,
      );
      const button = screen.getByRole('button')!;

      fireEvent.click(button);
      await waitFor(() => {
        expect(screen.getByRole('button')).toBeTruthy();
      });

      unmount();
      expect(screen.queryByRole('button')).toBeNull();

      // run all timers and verify to make sure
      jest.runAllTimers();
      expect(screen.queryByRole('button')).toBeNull();
    });
  });
});
