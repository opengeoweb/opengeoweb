/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import { SearchField } from './SearchField';
import { SharedThemeProvider, SharedStoryWrapper } from '../Providers';

describe('components/LayerManager/LayerSelect/SearchField/SearchField', () => {
  const setSearchFilter = jest.fn();
  const onOpenServicePopup = jest.fn();
  it('should render correctly', () => {
    render(
      <SharedStoryWrapper>
        <SearchField
          searchFilter=""
          setSearchFilter={setSearchFilter}
          onOpenServicePopup={onOpenServicePopup}
        />
      </SharedStoryWrapper>,
    );
    expect(screen.getByRole('textbox')).toBeTruthy();
  });
  it('should be focused by default', () => {
    render(
      <SharedThemeProvider>
        <SearchField
          searchFilter=""
          setSearchFilter={setSearchFilter}
          onOpenServicePopup={onOpenServicePopup}
        />
      </SharedThemeProvider>,
    );
    expect(screen.getByRole('textbox').matches(':focus')).toBeTruthy();
  });
  it('should allow text entry', () => {
    render(
      <SharedThemeProvider>
        <SearchField
          searchFilter=""
          setSearchFilter={setSearchFilter}
          onOpenServicePopup={onOpenServicePopup}
        />
      </SharedThemeProvider>,
    );
    const textField = screen.getByRole('textbox');
    const testString = 'test';
    fireEvent.change(textField, { target: { value: testString } });
    expect((textField as HTMLInputElement).value).toBe(testString);
  });
  it('should clear the input when pressing the cancel button', () => {
    render(
      <SharedThemeProvider>
        <SearchField
          searchFilter=""
          setSearchFilter={setSearchFilter}
          onOpenServicePopup={onOpenServicePopup}
        />
      </SharedThemeProvider>,
    );
    const textField = screen.getByRole('textbox');
    fireEvent.change(textField, { target: { value: 'abc' } });
    const button = screen.getByRole('button');
    fireEvent.click(button);
    expect((textField as HTMLInputElement).value).toBeFalsy();
  });

  it('should debounce dispatch calls', () => {
    jest.useFakeTimers();
    expect(setSearchFilter).not.toHaveBeenCalled();
    render(
      <SharedThemeProvider>
        <SearchField
          searchFilter=""
          setSearchFilter={setSearchFilter}
          onOpenServicePopup={onOpenServicePopup}
        />
      </SharedThemeProvider>,
    );
    const textField = screen.getByRole('textbox');
    fireEvent.change(textField, { target: { value: 'a' } });
    fireEvent.change(textField, { target: { value: 'ab' } });
    fireEvent.change(textField, { target: { value: 'abc' } });
    jest.runOnlyPendingTimers();
    expect(setSearchFilter).toHaveBeenCalledTimes(1);

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should call onOpenServicePopup when pressing save button', async () => {
    render(
      <SharedStoryWrapper>
        <SearchField
          searchFilter=""
          setSearchFilter={setSearchFilter}
          onOpenServicePopup={onOpenServicePopup}
        />
      </SharedStoryWrapper>,
    );

    const textField = screen.getByRole('textbox');
    fireEvent.change(textField, {
      target: { value: 'https://testService' },
    });
    fireEvent.click(screen.getByText('Save'));
    expect(onOpenServicePopup).toHaveBeenCalled();
  });

  it('should call onOpenServicePopup on enter, if service is valid', async () => {
    render(
      <SharedThemeProvider>
        <SearchField
          searchFilter=""
          setSearchFilter={setSearchFilter}
          onOpenServicePopup={onOpenServicePopup}
        />
      </SharedThemeProvider>,
    );

    const textField = screen.getByRole('textbox');
    fireEvent.change(textField, {
      target: { value: 'https://testService' },
    });
    fireEvent.keyDown(textField, { key: 'Enter' });
    expect(onOpenServicePopup).toHaveBeenCalled();
  });

  it('should not to call onOpenServicePopup on enter, when service is not valid', async () => {
    render(
      <SharedThemeProvider>
        <SearchField
          searchFilter=""
          setSearchFilter={setSearchFilter}
          onOpenServicePopup={onOpenServicePopup}
        />
      </SharedThemeProvider>,
    );

    const textField = screen.getByRole('textbox');
    fireEvent.change(textField, {
      target: { value: 'notaValidService' },
    });

    fireEvent.keyDown(textField, { key: 'Enter' });
    expect(onOpenServicePopup).not.toHaveBeenCalled();
  });

  it('should clear the text from searchbar when popup is closed', async () => {
    render(
      <SharedThemeProvider>
        <SearchField
          searchFilter=""
          setSearchFilter={setSearchFilter}
          onOpenServicePopup={onOpenServicePopup}
        />
      </SharedThemeProvider>,
    );
    const textField = screen.getByRole('textbox');
    fireEvent.change(textField, {
      target: { value: 'https://testService' },
    });
    fireEvent.keyDown(textField, { key: 'Enter' });

    await waitFor(() => expect((textField as HTMLInputElement).value).toBe(''));
  });

  it('should initialize to passed in filter', () => {
    const initialText = 'test';
    render(
      <SharedThemeProvider>
        <SearchField
          searchFilter={initialText}
          setSearchFilter={setSearchFilter}
          onOpenServicePopup={onOpenServicePopup}
        />
      </SharedThemeProvider>,
    );
    const textField = screen.getByRole('textbox');
    expect((textField as HTMLInputElement).value).toBe(initialText);
  });
});
