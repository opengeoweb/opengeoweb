/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { Box, useTheme, Typography, TooltipProps } from '@mui/material';
import { Add } from '@opengeoweb/theme';
import type { Meta, StoryObj } from '@storybook/react';
import { CustomIconButton, Variant } from './CustomIconButton';
import { StorybookDocsWrapper } from '../Storybook';

const meta: Meta<typeof CustomIconButton> = {
  title: 'components/CustomIconButton',
  component: CustomIconButton,
  argTypes: {},
  parameters: {
    docs: {
      description: {
        component: 'A custom icon button component',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof CustomIconButton>;

export const Component: Story = {
  args: {
    variant: 'boxed',
    children: <Add />,
    tooltipTitle: 'hover tooltip',
    isSelected: false,
    shouldShowAsDisabled: false,
    tooltipProps: {} as TooltipProps,
    size: 'medium',
  },
};

type IconButtonSize = 'small' | 'medium' | 'large';

const CustomIconDemo = ({
  isDark = false,
}: {
  isDark?: boolean;
}): React.ReactElement => {
  const theme = useTheme();

  const iconButtonVariants = Object.keys(
    theme.palette.geowebColors.iconButtons,
  ) as Variant[];

  const sizes: IconButtonSize[] = ['small', 'medium', 'large'];

  return (
    <StorybookDocsWrapper isDark={isDark} sx={{ button: { margin: 1 } }}>
      <Box>
        <Typography>variants</Typography>
        {iconButtonVariants.map((variant) => (
          <CustomIconButton key={`iconbutton-${variant}`} variant={variant}>
            <Add />
          </CustomIconButton>
        ))}
      </Box>

      <Box>
        <Typography>sizes</Typography>
        {sizes.map((size) =>
          iconButtonVariants.map((variant) => (
            <CustomIconButton
              size={size}
              key={`iconbutton-${variant}`}
              variant={variant}
            >
              <Add />
            </CustomIconButton>
          )),
        )}
      </Box>

      <Box>
        <Typography>tooltip title</Typography>
        {iconButtonVariants.map((variant) => (
          <CustomIconButton
            key={`iconbutton-${variant}`}
            variant={variant}
            tooltipTitle={`This is a tooltip from ${variant} variant`}
          >
            <Add />
          </CustomIconButton>
        ))}
      </Box>

      <Box>
        <Typography>isSelected</Typography>
        {iconButtonVariants.map((variant) => (
          <CustomIconButton
            key={`iconbutton-${variant}`}
            variant={variant}
            isSelected
          >
            <Add />
          </CustomIconButton>
        ))}
      </Box>

      <Box>
        <Typography>shouldShowAsDisabled</Typography>
        {iconButtonVariants.map((variant) => (
          <CustomIconButton
            key={`iconbutton-${variant}`}
            variant={variant}
            shouldShowAsDisabled
          >
            <Add />
          </CustomIconButton>
        ))}
      </Box>

      <Box>
        <Typography>disabled</Typography>
        {iconButtonVariants.map((variant) => (
          <CustomIconButton
            key={`iconbutton-${variant}`}
            variant={variant}
            disabled
          >
            <Add />
          </CustomIconButton>
        ))}
      </Box>

      <Box>
        <Typography>tooltipProps</Typography>
        {iconButtonVariants.map((variant) => (
          <CustomIconButton
            key={`iconbutton-${variant}`}
            variant={variant}
            tooltipProps={{
              title: `Hovering on ${variant}`,
              placement: 'right',
            }}
          >
            <Add />
          </CustomIconButton>
        ))}
      </Box>
    </StorybookDocsWrapper>
  );
};

const parameters = {
  docs: {
    description: {
      story: 'Variations of CustomIcon',
    },
  },
};

export const CustomIconButtonLight: Story = {
  render: () => <CustomIconDemo />,
  tags: ['snapshot'],
  parameters: {
    ...parameters,
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf85c60f301e47ca4eee55/version/6436bb4149f9bd3d3d2e219f',
      },
    ],
  },
};

export const CustomIconButtonDark: Story = {
  render: () => <CustomIconDemo isDark />,
  tags: ['snapshot', 'dark'],
  parameters: {
    ...parameters,
    zeplinLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e68ceb304607400f6d5e/version/6436bb5a6a1b64363b818ea8',
      },
    ],
  },
};
