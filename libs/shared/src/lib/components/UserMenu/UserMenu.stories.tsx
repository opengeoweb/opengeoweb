/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { MenuItem, Paper } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import {
  UserMenuItemTheme,
  useUserMenu,
  UserMenu,
  UserMenuItemSentryRecording,
} from '.';
import UserMenuItemCheatSheet from './UserMenuItemCheatSheet';

const meta: Meta<typeof UserMenu> = {
  title: 'components/UserMenu',
  component: UserMenu,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the user UserMenu',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof UserMenu>;

export const Component: Story = {
  args: {
    initials: 'TU',
    isAdmin: false,
    isOpen: true,
    children: [
      <MenuItem key="index-1" selected>
        Item 1
      </MenuItem>,
      <MenuItem key="index-2">Item 2</MenuItem>,
      <MenuItem key="index-3">Item 3</MenuItem>,
    ],
  },
  render: (props) => (
    <div style={{ height: 300 }}>
      <UserMenu {...props} />
    </div>
  ),
};

const DemoUserMenu: React.FC = () => {
  const { isOpen, onClose, onToggle } = useUserMenu(true);
  const [recordButtonEnabled, setRecordButtonEnabled] = React.useState(false);

  return (
    <UserMenu
      isOpen={isOpen}
      onClose={onClose}
      onToggle={onToggle}
      initials="JD"
    >
      <MenuItem selected onClick={onClose} divider>
        Profile
      </MenuItem>
      <UserMenuItemTheme />
      <UserMenuItemCheatSheet />
      <UserMenuItemSentryRecording
        recordButtonEnabled={recordButtonEnabled}
        setRecordButtonEnabled={setRecordButtonEnabled}
      />
      <MenuItem onClick={onClose}>Logout</MenuItem>
    </UserMenu>
  );
};

const compactParameters = {
  docs: {
    description: {
      story: 'Compact demo of UserMenu',
    },
  },
};

export const ThemeLight: Story = {
  render: () => (
    <Paper>
      <DemoUserMenu />
    </Paper>
  ),
  parameters: compactParameters,
};

export const ThemeDark: Story = {
  render: () => (
    <Paper>
      <DemoUserMenu />
    </Paper>
  ),
  tags: ['dark'],
  parameters: compactParameters,
};

const extendedParameters = {
  docs: {
    description: {
      story: 'Extended demo of UserMenu',
    },
  },
};

export const UserMenuLight: Story = {
  render: () => (
    <div style={{ position: 'relative', height: 600, width: 400 }}>
      <DemoUserMenu />
    </div>
  ),
  tags: ['snapshot'],
  parameters: extendedParameters,
};

export const UserMenuDark: Story = {
  render: () => (
    <div style={{ position: 'relative', height: 600, width: 400 }}>
      <DemoUserMenu />
    </div>
  ),
  tags: ['dark', 'snapshot'],
  parameters: extendedParameters,
};
