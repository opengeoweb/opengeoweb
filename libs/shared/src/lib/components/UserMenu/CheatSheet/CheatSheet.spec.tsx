/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import CheatSheet from './CheatSheet';
import { SharedStoryWrapper } from '../../Providers/Providers';

describe('components/UserMenu/CheatSheet/CheatSheet', () => {
  it('should work with default props', async () => {
    render(
      <SharedStoryWrapper>
        <CheatSheet />
      </SharedStoryWrapper>,
    );

    expect(screen.getByLabelText('platform')).toBeTruthy();
  });

  it('should change tabs', async () => {
    render(
      <SharedStoryWrapper>
        <CheatSheet />
      </SharedStoryWrapper>,
    );
    expect(screen.getByText('Windows').classList).toContain('Mui-selected');
    expect(screen.getByText('Mac').classList).not.toContain('Mui-selected');

    // change to mac
    fireEvent.click(screen.getAllByRole('tab')[1]);

    expect(screen.getByText('Windows').classList).not.toContain('Mui-selected');
    expect(screen.getByText('Mac').classList).toContain('Mui-selected');
  });
});
