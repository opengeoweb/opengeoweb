/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';
import ShortcutList from './ShortcutList';
import { SharedStoryWrapper } from '../../Providers';

describe('components/UserMenu/CheatSheet/ShortcutList', () => {
  it('should work with default props', async () => {
    const { container } = render(
      <SharedStoryWrapper>
        <ShortcutList />
      </SharedStoryWrapper>,
    );

    expect(screen.getByTestId('list-windows')).toBeTruthy();
    expect(screen.queryByTestId('list-mac')).toBeFalsy();
    // eslint-disable-next-line testing-library/no-container, testing-library/no-node-access
    expect(container.querySelectorAll('li')).not.toHaveLength(0);
  });

  it('should list platform mac', async () => {
    const { container } = render(
      <SharedStoryWrapper>
        <ShortcutList platform="mac" />
      </SharedStoryWrapper>,
    );

    expect(screen.getByTestId('list-mac')).toBeTruthy();
    expect(screen.queryByTestId('list-windows')).toBeFalsy();
    // eslint-disable-next-line testing-library/no-container, testing-library/no-node-access
    expect(container.querySelectorAll('li')).not.toHaveLength(0);
  });
});
