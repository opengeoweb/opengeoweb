/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { Paper } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import CheatSheet from './CheatSheet';
import { StorybookDocsWrapper } from '../../Storybook';

const meta: Meta<typeof CheatSheet> = {
  title: 'components/UserMenu/CheatSheet',
  component: CheatSheet,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the user CheatSheet',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof CheatSheet>;

export const Component: Story = {};

export const ThemeLight: Story = {
  render: () => (
    <Paper sx={{ overflow: 'scroll' }}>
      <CheatSheet />
    </Paper>
  ),
  tags: ['!autodocs'],
  parameters: {
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6138a379a11c80b92f4c4694',
      },
    ],
  },
};

export const ThemeDark: Story = {
  render: () => (
    <Paper sx={{ overflow: 'scroll' }}>
      <CheatSheet />
    </Paper>
  ),
  tags: ['dark', '!autodocs'],
  parameters: {
    zeplinLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6138a37a453de5b9c8619572',
      },
    ],
  },
};

const parameters = {
  docs: {
    description: {
      story: 'Example of CheatSheet',
    },
  },
};

export const CheatSheetLight: Story = {
  render: () => (
    <StorybookDocsWrapper
      sx={{ width: 800, '& > div': { maxHeight: 'initial!important' } }}
    >
      <CheatSheet />
    </StorybookDocsWrapper>
  ),
  tags: ['snapshot'],
  parameters,
};

export const CheatSheetDark: Story = {
  render: () => (
    <StorybookDocsWrapper
      isDark
      sx={{ width: 800, '> div': { maxHeight: 'initial!important' } }}
    >
      <CheatSheet />
    </StorybookDocsWrapper>
  ),
  tags: ['snapshot', 'dark'],
  parameters,
};
