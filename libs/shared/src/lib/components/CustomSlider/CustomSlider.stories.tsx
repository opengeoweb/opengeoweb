/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { Card, Box } from '@mui/material';

import type { Meta, StoryObj } from '@storybook/react';
import { CustomSlider, sliderHeaderStyle } from './CustomSlider';

const meta: Meta<typeof CustomSlider> = {
  title: 'components/CustomSlider',
  component: CustomSlider,
  argTypes: {
    value: {
      control: 'number',
      description: 'Current value of slider',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'A custom slider component',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof CustomSlider>;

export const Component: Story = {
  args: {
    orientation: 'vertical',
    // value: 1,
    step: 1,
    max: 10,
    min: 0,
    onChange: () => {},
  },
  render: (props) => (
    <div
      style={{
        height: '200px',
        width: '100px',
        float: 'left',
        marginTop: '10px',
      }}
    >
      <CustomSlider {...props} />
    </div>
  ),
};

const SliderExamples: React.FC = () => {
  const [value, setValue] = React.useState<number | number[]>(1);
  const onChangeSlider = (_event: Event, newValue: number | number[]): void => {
    setValue(newValue);
  };
  const [value2, setValue2] = React.useState<number | number[]>(1);
  const onChangeSlider2 = (
    _event: Event,
    newValue: number | number[],
  ): void => {
    setValue2(newValue);
  };
  const [value3, setValue3] = React.useState<number | number[]>(1);
  const onChangeSlider3 = (
    _event: Event,
    newValue: number | number[],
  ): void => {
    setValue3(newValue);
  };

  return (
    <Card
      style={{
        width: '300px',
        height: '300px',
      }}
    >
      <div
        style={{
          height: '200px',
          width: '100px',
          float: 'left',
          marginTop: '10px',
        }}
      >
        <Box sx={sliderHeaderStyle}>Header 1</Box>
        <CustomSlider
          orientation="vertical"
          value={value}
          step={1}
          max={10}
          min={0}
          onChange={onChangeSlider}
        />
      </div>
      <div
        style={{
          height: '200px',
          width: '100px',
          float: 'left',
          marginTop: '10px',
        }}
      >
        <Box sx={sliderHeaderStyle}>Header 2</Box>
        <CustomSlider
          orientation="vertical"
          value={value2}
          max={3}
          min={0}
          marks={[
            { value: 0, label: '0' },
            { value: 1, label: '1' },
            { value: 2, label: '2' },
            { value: 3, label: '3' },
          ]}
          onChange={onChangeSlider2}
        />
      </div>
      <div
        style={{
          height: '200px',
          width: '100px',
          float: 'left',
          marginTop: '10px',
        }}
      >
        <Box sx={sliderHeaderStyle}>Header 3</Box>
        <CustomSlider
          orientation="vertical"
          value={value3}
          max={3}
          min={0}
          marks={[
            { value: 0, label: '0' },
            { value: 1, label: '1' },
            { value: 2, label: '2' },
            { value: 3, label: '3' },
          ]}
          onChange={onChangeSlider3}
          disabled={true}
        />
      </div>
    </Card>
  );
};

const parameters = {
  docs: {
    description: {
      story: 'Variations of CustomSlider',
    },
  },
};

export const CustomSliderLightTheme: Story = {
  render: () => <SliderExamples />,
  tags: ['snapshot'],
  parameters,
};

export const CustomSliderDarkTheme: Story = {
  render: () => <SliderExamples />,
  tags: ['dark', 'snapshot'],
  parameters,
};
