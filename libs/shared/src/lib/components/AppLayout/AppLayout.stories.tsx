/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { MenuItem, Typography } from '@mui/material';
import { Meta, StoryObj } from '@storybook/react/*';
import AppLayout from './AppLayout';
import { AppHeader } from '../AppHeader';
import { UserMenu, UserMenuItemTheme, useUserMenu } from '../UserMenu';

const meta: Meta<typeof AppLayout> = {
  title: 'components/AppLayout',
  component: AppLayout,
  parameters: {
    docs: {
      description: {
        component: 'The main app layout Geoweb',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof AppLayout>;

const DemoUserMenu: React.FC = () => {
  const { isOpen, onClose, onToggle } = useUserMenu();

  return (
    <UserMenu
      isOpen={isOpen}
      onClose={onClose}
      onToggle={onToggle}
      initials="JD"
    >
      <MenuItem selected onClick={onClose}>
        Profile
      </MenuItem>
      <UserMenuItemTheme />
      <MenuItem onClick={onClose}>Logout</MenuItem>
    </UserMenu>
  );
};

export const Component: Story = {
  args: {
    header: <AppHeader title="Header" userMenu={<DemoUserMenu />} />,
    children: <Typography>Main</Typography>,
  },
  tags: ['!autodocs'],
};

export const ThemeLight: Story = {
  render: () => (
    <AppLayout
      header={<AppHeader title="Header" userMenu={<DemoUserMenu />} />}
    >
      <Typography>Main</Typography>
    </AppLayout>
  ),
};

export const ThemeDark: Story = {
  render: () => (
    <AppLayout
      header={<AppHeader title="Header" userMenu={<DemoUserMenu />} />}
    >
      <Typography>Main</Typography>
    </AppLayout>
  ),
  tags: ['dark'],
};
