/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { Box, Grid2 as Grid, TextField, Typography } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import { ToolContainer } from '.';

const meta: Meta<typeof ToolContainer> = {
  title: 'components/ToolContainer',
  component: ToolContainer,
  parameters: {
    docs: {
      description: {
        component: 'A component that wraps children inside a ToolContainer',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof ToolContainer>;

export const Component: Story = {
  args: {
    children: 'content',
    size: 'large',
    title: 'title',
    isDraggable: true,
    isResizable: true,
  },
};
interface ToolContainerDemoProps {
  isSnapShot?: boolean;
}

export const ToolContainerDemo = ({
  isSnapShot = false,
}: ToolContainerDemoProps): React.ReactElement => {
  // eslint-disable-next-line no-console
  const onClose = (): void => console.log('onClose');

  const maxHeight = isSnapShot ? null : 200;
  return (
    <Grid container>
      <Grid size={12}>
        <div>
          <ToolContainer style={{ marginBottom: 50, width: 500 }} size="xxs">
            <Typography>xxs header</Typography>
          </ToolContainer>

          <ToolContainer
            style={{ marginBottom: 50, width: 500 }}
            size="xs"
            title="xs header"
            onClose={onClose}
            elevation={1}
          >
            <Typography>low elevation</Typography>
          </ToolContainer>

          <ToolContainer
            style={{ marginBottom: 50, width: 600, maxHeight: maxHeight! }}
            size="small"
            title="small header"
            isResizable
            onClose={onClose}
          >
            <div style={{ padding: 12 }}>
              <Typography>padding maxheight content</Typography>
              <Typography>padding maxheight content</Typography>
              <Typography>padding maxheight content</Typography>
              <Typography>padding maxheight content</Typography>
              <Typography>padding maxheight content</Typography>
              <Typography>padding maxheight content</Typography>
              <Typography>padding maxheight content</Typography>
            </div>
          </ToolContainer>

          <ToolContainer
            style={{ marginBottom: 50, width: 700 }}
            size="medium"
            title="medium header"
            onClose={onClose}
            isResizable
            isDraggable
            elevation={10}
          >
            <Typography>with footer and high elevation</Typography>
          </ToolContainer>

          <ToolContainer
            style={{ marginBottom: 50, width: 800 }}
            size="large"
            title="large header"
            onClose={onClose}
          >
            <Typography>no footer</Typography>
          </ToolContainer>

          <ToolContainer
            style={{ marginBottom: 50, width: 700 }}
            size="medium"
            title="medium custom leftHeaderComponent"
            onClose={onClose}
            isResizable
            leftHeaderComponent={
              <TextField
                value="Some dummy text"
                size="small"
                sx={{
                  '& .MuiFilledInput-root': {
                    height: '24px',
                    fontSize: 11,
                  },
                  '& .MuiFilledInput-input': {
                    height: '100%',
                    paddingTop: 0,
                    paddingBottom: 0,
                  },
                }}
                slotProps={{
                  input: { disableUnderline: true },
                }}
              />
            }
            rightHeaderComponent={<div>Some text</div>}
          >
            <Typography>
              with custom leftHeaderComponent and rightHeaderComponent
            </Typography>
          </ToolContainer>
          <ToolContainer
            style={{ marginBottom: 50, width: 319 }}
            size="xs"
            title="title hides on small width"
            onClose={onClose}
            elevation={1}
            rightHeaderComponent={<div>Some text</div>}
          >
            <Typography>title hides on small width</Typography>
          </ToolContainer>
        </div>
      </Grid>
    </Grid>
  );
};
ToolContainerDemo.tags = ['!autodocs'];

const ToolContainerDemoStory = (): React.ReactElement => {
  return (
    <Box sx={{ padding: '20px' }}>
      <Grid container direction="row">
        <ToolContainerDemo />
      </Grid>
    </Box>
  );
};

const parameters = {
  docs: {
    description: {
      story: 'Variations of ToolContainer',
    },
  },
};

export const ThemeLight: Story = {
  render: ToolContainerDemoStory,
  parameters: {
    ...parameters,
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6143189d4518bf5167200f4a',
      },
    ],
  },
};

export const ThemeDark: Story = {
  render: ToolContainerDemoStory,
  tags: ['dark'],
  parameters: {
    ...parameters,
    zeplinLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6143189e3369ae4fe33a4cc3',
      },
    ],
  },
};
