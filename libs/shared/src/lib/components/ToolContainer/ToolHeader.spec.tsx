/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { SharedStoryWrapper } from '../Providers';
import ToolHeader from './ToolHeader';
import { HeaderSize } from './types';

describe('components/ToolHeader', () => {
  it('should render successfully with default props', () => {
    const { baseElement } = render(
      <SharedStoryWrapper>
        <ToolHeader />
      </SharedStoryWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(screen.queryByTestId('closeBtn')).toBeFalsy();

    expect(screen.getByRole('heading', { name: 'toolheader' })).toBeTruthy();
  });

  it('should handle custom className', () => {
    const className = 'header-test';
    const { baseElement } = render(
      <SharedStoryWrapper>
        <ToolHeader className={className} />
      </SharedStoryWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(
      screen
        .getByRole('heading', { name: 'toolheader' })
        .classList.contains(className),
    ).toBeTruthy();
  });

  it('should handle different sizes and onClose with *mouse* click', () => {
    const props = {
      onClose: jest.fn(),
      size: 'small' as HeaderSize,
      isDraggable: true,
      title: 'test',
    };
    const { baseElement } = render(
      <SharedStoryWrapper>
        <ToolHeader {...props} />
      </SharedStoryWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(screen.getByText(props.title)).toBeTruthy();

    fireEvent.click(screen.queryByTestId('closeBtn')!);
    expect(props.onClose).toHaveBeenCalled();
  });

  it('should handle different sizes and onClose with *touch* tap', async () => {
    const props = {
      onClose: jest.fn(),
      size: 'small' as HeaderSize,
      isDraggable: true,
      title: 'test',
    };
    const { baseElement } = render(
      <SharedStoryWrapper>
        <ToolHeader {...props} />
      </SharedStoryWrapper>,
    );
    expect(baseElement).toBeTruthy();

    expect(screen.getByRole('heading', { name: /test/i }).innerHTML).toEqual(
      props.title,
    );

    fireEvent.touchEnd(screen.queryByTestId('closeBtn')!);
    expect(props.onClose).toHaveBeenCalled();
  });

  it('should pass custom leftHeaderComponent', () => {
    const TestComponent = (
      <div data-testid="test-leftHeaderComponent">testing left component</div>
    );
    render(
      <SharedStoryWrapper>
        <ToolHeader leftHeaderComponent={TestComponent} />
      </SharedStoryWrapper>,
    );
    expect(screen.getByTestId('test-leftHeaderComponent')).toBeTruthy();
  });

  it('should pass custom rightHeaderComponent', () => {
    const TestComponent = (
      <div data-testid="test-rightHeaderComponent">testing right component</div>
    );
    render(
      <SharedStoryWrapper>
        <ToolHeader rightHeaderComponent={TestComponent} />
      </SharedStoryWrapper>,
    );

    expect(screen.getByTestId('test-rightHeaderComponent')).toBeTruthy();
  });
  it('should show custom closeIcon', () => {
    const testId = 'testCloseIcon';
    render(
      <SharedStoryWrapper>
        <ToolHeader
          onClose={(): void => {}}
          closeIcon={<span data-testid={testId} />}
        />
      </SharedStoryWrapper>,
    );
    expect(screen.getByTestId(testId)).toBeTruthy();
  });

  it('should change cursor for draggable toolheader', () => {
    const props = {
      isDraggable: true,
      title: 'test',
    };
    render(
      <SharedStoryWrapper>
        <ToolHeader {...props} />
      </SharedStoryWrapper>,
    );
    const title = screen.getByLabelText('toolheader');
    const style = window.getComputedStyle(title);
    expect(style.cursor).toBe('move');
  });
});
