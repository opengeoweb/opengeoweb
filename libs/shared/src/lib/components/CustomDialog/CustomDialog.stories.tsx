/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Box, Button, Link, Typography } from '@mui/material';
import type { StoryObj } from '@storybook/react';
import { CustomDialog } from './CustomDialog';

export default {
  title: 'components/CustomDialog',
  component: CustomDialog,
  tags: ['!autodocs'],
};

type Story = StoryObj<typeof CustomDialog>;

const Message = (): React.ReactElement => (
  <>
    <Typography>
      This browser is not compatible with GeoWeb functionalities.
    </Typography>
    <br />
    <Typography>
      For optimal experience, please upgrade to the latest version of{' '}
      <Link href="https://www.google.com/chrome/" target="_blank">
        Chrome
      </Link>
      ,{' '}
      <Link href="https://support.apple.com/downloads/safari" target="_blank">
        Safari
      </Link>{' '}
      or{' '}
      <Link href="https://www.microsoft.com/edge" target="_blank">
        Microsoft Edge
      </Link>
      .
    </Typography>
  </>
);

export const CustomDialogLight: Story = {
  args: {
    title: 'Browser issue',
    open: true,
    onClose: (): void => {},
    children: <Message />,
  },
  tags: ['snapshot'],
};

export const CustomDialogDark: Story = {
  args: {
    title: 'Browser issue',
    open: true,
    onClose: (): void => {},
    children: <Message />,
  },
  tags: ['snapshot', 'dark'],
};

export const CustomDialogResizable: Story = {
  args: {
    title: 'Resizable dialog',
    open: true,
    onClose: (): void => {},
    children: <Typography>This custom dialog can be resized</Typography>,
    isResizable: true,
  },
};

export const CustomDialogResizableWithInitialSize: Story = {
  args: {
    title: 'Resizable dialog',
    open: true,
    onClose: (): void => {},
    children: (
      <Typography>
        This custom dialog can be resized and has a default resizable default
        size
      </Typography>
    ),
    isResizable: true,
    resizableDefaultSize: { width: 400, height: 300 },
  },
};

const CustomDialogInlineDemo: React.FC<{ label?: string }> = ({
  label = 'Left',
}) => {
  const [isDialogOpen, toggleDialog] = React.useState<boolean>(false);
  return (
    <Box
      sx={{
        flex: 1,
        alignContent: 'center',
        textAlign: 'center',
        border: '1px solid grey',
        // make sure the parent element of the dialogs has a position defined
        position: 'relative',
      }}
    >
      <Button
        variant="contained"
        onClick={() => toggleDialog(true)}
      >{`Open ${label} dialog`}</Button>
      <CustomDialog
        open={isDialogOpen}
        onClose={() => toggleDialog(false)}
        title={`${label} dialog`}
        isResizable
        shouldRenderInline
      >
        <p>This dialog is resizable</p>
      </CustomDialog>
    </Box>
  );
};

export const CustomDialogRenderInline: Story = {
  render: () => (
    <Box sx={{ display: 'flex', height: '100vh', overflow: 'hidden' }}>
      <CustomDialogInlineDemo label="Left dialog" />
      <Box sx={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
        <CustomDialogInlineDemo label="Top right dialog" />
        <CustomDialogInlineDemo label="Bottom right dialog" />
      </Box>
    </Box>
  ),
  parameters: {
    layout: 'fullscreen',
  },
};

export const CustomDialogResizableNoLayout: Story = {
  args: {
    title: 'No layout dialog',
    open: true,
    onClose: (): void => {},
    children: (
      <div
        style={{
          background: 'yellow',
          height: '100%',
          width: '100%',
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <Typography align="center" width="100%">
          This dialog has no layout and is resizable
        </Typography>
      </div>
    ),
    isResizable: true,
    resizableDefaultSize: { width: 100, height: 60 },
    hasLayout: false,
  },
};
