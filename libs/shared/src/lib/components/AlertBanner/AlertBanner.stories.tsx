/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Meta, StoryObj } from '@storybook/react/*';
import AlertBanner from './AlertBanner';
import { StorybookDocsWrapper } from '../Storybook';

const actionButtonProps = {
  clickMeButton: {
    onClick: (): void => {},
    title: 'click me',
  },
};

const meta: Meta<typeof AlertBanner> = {
  title: 'components/AlertBanner',
  component: AlertBanner,
  argTypes: {
    severity: {
      control: 'radio',
      options: ['success', 'info', 'warning', 'error'],
      description: 'Alert severity',
    },
    actionButtonProps: {
      control: 'select',
      options: [undefined, ...Object.keys(actionButtonProps)],
      mapping: {
        undefined,
        ...actionButtonProps,
      },
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'A component that informs the user about requests with different statusses',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof AlertBanner>;

export const Component: Story = {
  args: {
    title: 'Success',
    info: 'Data has been processed',
    severity: 'success',
    isCompact: false,
    shouldClose: true,
    dataTestId: '',
  },
};

const Demo = ({ ...props }): React.ReactElement => {
  return (
    <StorybookDocsWrapper {...props} sx={{ width: '500px' }}>
      <AlertBanner
        title="An error has occurred while saving, please try again"
        info="Unable to store data."
      />
      <br />
      <AlertBanner
        title="There are 2 unhandled notification triggers"
        severity="warning"
      />
      <br />
      <AlertBanner
        title="There are no available bulletins from the last 24 hours"
        severity="info"
        info="For historical bulletins please see the forecast history list"
      />
      <br />
      <AlertBanner
        title="The product was saved successfully"
        severity="success"
      />
      <br />
      <AlertBanner
        title="This layer could not be loaded: does not exist on the server. Long text is clipped."
        isCompact
        severity="error"
      />
      <br />
      <AlertBanner title="Notification" shouldClose />
      <br />
      <AlertBanner
        title="Notification"
        info="Some more lines when addional information is needed."
        shouldClose
      />
      <br />
      <AlertBanner
        title="Notification"
        info="Some more lines when addional information is needed."
        shouldClose
        actionButtonProps={{
          onClick: (): void => {},
          title: 'TAKE ACTION',
        }}
      />
      <br />
      <AlertBanner
        title="An error has occurred while retrieving the list"
        actionButtonProps={{
          onClick: (): void => {},
          title: 'Try again',
        }}
      />
      <br />
      <AlertBanner
        severity="info"
        title="Auto-update in 00:35"
        actionButtonProps={{
          onClick: (): void => {},
          title: 'Auto-update now',
        }}
        shouldClose
      />
    </StorybookDocsWrapper>
  );
};

const parameters = {
  docs: {
    description: {
      story: 'Variations of AlertBanner',
    },
  },
};

export const AlertBannersLight: Story = {
  render: () => <Demo />,
  tags: ['snapshot'],
  parameters,
};

export const AlertBannersDark: Story = {
  render: () => <Demo isDark />,
  tags: ['dark', 'snapshot'],
  parameters,
};
