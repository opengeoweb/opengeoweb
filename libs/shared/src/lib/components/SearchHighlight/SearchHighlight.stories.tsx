/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { Card, Grid2 as Grid } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import SearchHighlight, { HighlightProps } from './SearchHighlight';

const meta: Meta<typeof SearchHighlight> = {
  title: 'components/SearchHighlight',
  component: SearchHighlight,
  argTypes: {
    text: {
      control: 'text',
      description: 'The text to search within.',
    },
    search: {
      control: 'text',
      description: 'The search term to highlight in the text.',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'A component that highlights search terms within a given text.',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof SearchHighlight>;

const args = {
  text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non blandit tellus. Donec dignissim odio vitae ex vestibulum commodo.',
  search: 'adipiscing Donec',
};

export const Component: Story = {
  args,
};

const Demo = (args: HighlightProps): React.ReactElement => (
  <Card sx={{ padding: 1, maxWidth: 800 }}>
    <Grid container spacing={5}>
      <Grid>
        <SearchHighlight {...args} />
        <br />
        <SearchHighlight {...args} search="consectetur adipiscing elit" />
        <br />
        <SearchHighlight {...args} search="Lorem Cras COMMODO" />
      </Grid>
    </Grid>
  </Card>
);
const parameters = {
  docs: {
    description: {
      story: 'Variations of SearchHighlight',
    },
  },
};
export const SearchHighlightLightTheme: Story = {
  args,
  render: Demo,
  tags: ['snapshot'],
  parameters,
};

export const SearchHighlightDarkTheme: Story = {
  args,
  render: Demo,
  tags: ['snapshot', 'dark'],
  parameters,
};
