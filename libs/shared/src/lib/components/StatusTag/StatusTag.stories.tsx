/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { Grid2 as Grid } from '@mui/material';
import { Edit, Export } from '@opengeoweb/theme';
import { Meta, StoryObj } from '@storybook/react/*';
import StatusTag, { bgColors } from './StatusTag';
import { StorybookDocsWrapper } from '../Storybook';

const iconsMap = {
  Warning: <Edit />,
  Cart: <Export />,
};

const meta: Meta<typeof StatusTag> = {
  title: 'components/StatusTag',
  component: StatusTag,
  argTypes: {
    content: {
      control: 'text',
    },
    icon: {
      control: 'select',
      options: [undefined, ...Object.keys(iconsMap)],
      mapping: {
        undefined,
        ...iconsMap,
      },
      description: 'icon',
    },
    color: {
      control: 'select',
      options: [undefined, ...Object.keys(bgColors)],
      description: 'color',
    },
  },
};
export default meta;

type Story = StoryObj<typeof StatusTag>;

export const Component: Story = {
  args: {
    content: 'All good',
    color: 'green',
  },
};

const Demo = ({ ...props }): React.ReactElement => (
  <StorybookDocsWrapper
    {...props}
    sx={{
      width: 900,
      padding: '20px 0px',
    }}
  >
    <Grid container spacing={5}>
      <Grid>
        <StatusTag content="All good" color="green" />
        <br />
        <StatusTag content="Active" color="green" />
      </Grid>
      <Grid>
        <StatusTag content="Alert" color="red" />
        <br />
        <StatusTag content="Cancelled" color="red" />
      </Grid>
      <Grid>
        <StatusTag content="Inactive" color="grey" />
        <br />
        <StatusTag content="Expired" color="grey" />
      </Grid>
      <Grid>
        <StatusTag content="Watch" color="yellow" />
        <br />
        <StatusTag content="Warning" color="yellow" />
      </Grid>
      <Grid>
        <StatusTag content="Summary" color="purple" />
        <br />
        <StatusTag content="Read" color="blue" />
      </Grid>
      <Grid>
        <StatusTag
          content="Pub"
          color="green"
          sx={{
            width: 66,
          }}
          icon={<Edit sx={{ width: 22, height: 22, paddingTop: '1px' }} />}
        />
      </Grid>
    </Grid>
  </StorybookDocsWrapper>
);
const lightThemeLink =
  'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5eea1defcc2c462aa1af2b7a/version/635243ce8ced6b935e144b52';

const parameters = {
  docs: {
    description: {
      story: 'Variations of StatusTag',
    },
  },
};

export const StatusTagsLight: Story = {
  render: () => <Demo />,
  tags: ['snapshot'],
  parameters: {
    ...parameters,
    zeplinLink: [
      {
        name: 'Light theme',
        link: lightThemeLink,
      },
    ],
  },
};

const darkThemeLink =
  'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e69071e103308259492c/version/635243ce677ae791cb436c18';

export const StatusTagsDark: Story = {
  render: () => <Demo isDark />,
  tags: ['dark', 'snapshot'],
  parameters: {
    ...parameters,
    zeplinLink: [
      {
        name: 'Dark theme',
        link: darkThemeLink,
      },
    ],
  },
};
