/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, act, screen } from '@testing-library/react';
import { MenuItem, Box } from '@mui/material';
import { ThemeWrapper } from '@opengeoweb/theme';
import { TooltipSelect } from './TooltipSelect';
import { defaultDelay } from '../CustomTooltip';

describe('components/Manager/TooltipSelect/TooltipSelect', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('work with default props', () => {
    const props = {
      tooltip: 'test tooltip',
    };

    render(
      <ThemeWrapper>
        <TooltipSelect {...props}>
          <MenuItem>empty</MenuItem>
        </TooltipSelect>
      </ThemeWrapper>,
    );

    const button = screen.getByRole('combobox')!;
    fireEvent.focus(button);

    act(() => {
      jest.advanceTimersByTime(defaultDelay);
    });

    expect(screen.getByRole('tooltip')).toBeTruthy();

    expect(screen.getByText(props.tooltip)).toBeTruthy();
  });

  it('should show correct tooltip on focus', () => {
    const props = {
      tooltip: 'test tooltip',
    };

    render(
      <ThemeWrapper>
        <TooltipSelect {...props} data-testid="testing">
          <MenuItem>empty</MenuItem>
        </TooltipSelect>
      </ThemeWrapper>,
    );

    expect(screen.queryByRole('tooltip')).toBeFalsy();
    const button = screen.getByRole('combobox')!;

    fireEvent.focus(button);

    act(() => {
      jest.advanceTimersByTime(defaultDelay);
    });
    expect(screen.getByRole('tooltip')).toBeTruthy();
  });

  it('should call onChangeMouseWheel on wheel scroll', () => {
    const props = {
      tooltip: 'test tooltip',
      list: [{ value: 'test1' }, { value: 'test2' }, { value: 'test3' }],
      currentIndex: 1,
      onChangeMouseWheel: jest.fn(),
    };
    render(
      <ThemeWrapper>
        <TooltipSelect {...props} data-testid="testing">
          <MenuItem>empty</MenuItem>
        </TooltipSelect>
      </ThemeWrapper>,
    );
    const select = screen.getByTestId('testing');

    fireEvent.wheel(select, { deltaY: 1 });
    expect(props.onChangeMouseWheel).toHaveBeenCalledTimes(1);
    expect(props.onChangeMouseWheel).toHaveBeenCalledWith({
      value: 'test3',
    });

    fireEvent.wheel(select, { deltaY: -1 });
    expect(props.onChangeMouseWheel).toHaveBeenCalledTimes(2);
    expect(props.onChangeMouseWheel).toHaveBeenCalledWith({
      value: 'test1',
    });
  });

  it('should hide tooltip, when select component is closed', () => {
    const props = {
      tooltip: 'test tooltip',
    };

    render(
      <ThemeWrapper>
        <Box data-testid="wrapper">
          <TooltipSelect {...props} data-testid="testing">
            <MenuItem>empty</MenuItem>
          </TooltipSelect>
        </Box>
      </ThemeWrapper>,
    );

    expect(screen.queryByRole('tooltip')).toBeFalsy();

    const button = screen.queryByRole('combobox')!;
    fireEvent.focus(button);

    act(() => {
      jest.advanceTimersByTime(defaultDelay);
    });
    expect(screen.getByRole('tooltip')).toBeTruthy();

    fireEvent.mouseDown(button);

    expect(screen.queryByRole('tooltip')).toBeFalsy();

    const wrapperComponent = screen.getByTestId('wrapper');
    fireEvent.mouseDown(wrapperComponent);

    expect(screen.queryByRole('tooltip')).toBeFalsy();
  });
});
