/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React, { FC } from 'react';
import { Add } from '@opengeoweb/theme';
import {
  Grid2 as Grid,
  ListItemIcon,
  ListItemText,
  MenuItem,
  SelectChangeEvent,
} from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import { TooltipSelect } from './TooltipSelect';
import { StorybookDocsWrapper } from '../Storybook';

const meta: Meta<typeof TooltipSelect> = {
  title: 'components/TooltipSelect',
  component: TooltipSelect,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the user TooltipSelect',
      },
    },
  },
  argTypes: {
    list: {
      control: 'select',
      options: [
        undefined,
        ...Object.keys({
          '1': '1',
          '2': '2',
          '3': '3',
        }),
      ],
      description: 'used for scrollwheel',
    },
    variant: {
      control: 'radio',
      description: 'selection variant',
      options: ['outlined', 'standard', 'filled', 'standard'],
    },
    requiresCtrlToChange: {
      control: 'boolean',
      description: 'use ctrl to change',
    },
  },
};
export default meta;

type Story = StoryObj<typeof TooltipSelect>;

export const Component: Story = {
  args: {
    isEnabled: false,
    open: false,
    value: '2',
    tooltip: 'Choose a number',
    children: [
      <MenuItem key="1" disabled>
        Numbers
      </MenuItem>,
      <MenuItem key="2" value="1">
        One
      </MenuItem>,
      <MenuItem key="3" value="2">
        Two
      </MenuItem>,
      <MenuItem key="4" value="3">
        Three
      </MenuItem>,
    ],
    requiresCtrlToChange: false,
    hasBackgroundColor: false,
    currentIndex: -1,
    skipLocalStyling: false,
    additionalStyling: {},
  },
};

const StoryTooltipSelect: FC<{
  isEnabled: boolean;
  defaultOpen: boolean;
  defaultValue: string;
}> = ({ isEnabled, defaultOpen, defaultValue }) => {
  const [isOpen, setIsOpen] = React.useState(defaultOpen);
  const [value, setValue] = React.useState(defaultValue);

  return (
    <Grid container direction="column" sx={{ width: '300px' }}>
      <Grid>{isEnabled ? 'Enabled' : 'Disabled'}</Grid>
      <Grid>
        <TooltipSelect
          open={isOpen}
          value={value}
          tooltip="Choose a number"
          isEnabled={isEnabled}
          onChange={(event: SelectChangeEvent<unknown>): void => {
            setValue((event.target as HTMLInputElement).value);
            setIsOpen(false);
          }}
          onClick={(): void => setIsOpen(!isOpen)}
        >
          <MenuItem disabled>Numbers</MenuItem>
          <MenuItem value="1">One</MenuItem>
          <MenuItem value="2">Two</MenuItem>
          <MenuItem value="3">Three</MenuItem>
          <MenuItem value="4">
            <ListItemText>Four</ListItemText>
            <ListItemIcon>
              <Add />
            </ListItemIcon>
          </MenuItem>
          <MenuItem value="5">
            <ListItemText>Five</ListItemText>
            <ListItemIcon>
              <Add />
            </ListItemIcon>
          </MenuItem>
        </TooltipSelect>
      </Grid>
    </Grid>
  );
};

const StoryTooltipSelects = ({
  isDefaultOpen = true,
  ...props
}: {
  isDefaultOpen?: boolean;
  isDark?: boolean;
}): React.ReactElement => (
  <StorybookDocsWrapper
    {...props}
    sx={{
      margin: '10px',
      padding: '20px 0px',
      width: '350px',
      height: '600px',
    }}
  >
    <StoryTooltipSelect isEnabled={true} defaultOpen={false} defaultValue="1" />
    <StoryTooltipSelect
      isEnabled={false}
      defaultOpen={false}
      defaultValue="1"
    />
    <StoryTooltipSelect
      isEnabled={true}
      defaultOpen={isDefaultOpen}
      defaultValue="4"
    />
  </StorybookDocsWrapper>
);

const parameters = {
  docs: {
    description: {
      story: 'Variations of ToolTipSelect',
    },
  },
};

export const TooltipSelectLightDemo: Story = {
  render: () => <StoryTooltipSelects isDefaultOpen={false} />,
  tags: [''],
  parameters,
};

export const TooltipSelectDarkDemo: Story = {
  render: () => <StoryTooltipSelects isDefaultOpen={false} isDark />,
  tags: ['dark'],
  parameters,
};

export const TooltipSelectLight: Story = {
  render: () => <StoryTooltipSelects />,
  tags: ['snapshot', '!autodocs'],
};

export const TooltipSelectDark: Story = {
  render: () => <StoryTooltipSelects isDark />,
  tags: ['dark', 'snapshot', '!autodocs'],
};
