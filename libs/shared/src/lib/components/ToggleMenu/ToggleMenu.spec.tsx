/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ThemeWrapper } from '@opengeoweb/theme';
import { getPosition, ToggleMenu } from './ToggleMenu';

describe('components/ToggleMenu', () => {
  const user = userEvent.setup();
  it('should render successfully', () => {
    render(
      <ThemeWrapper>
        <ToggleMenu />,
      </ThemeWrapper>,
    );
    expect(screen.getByTestId('toggleMenuButton')).toBeTruthy();
  });

  it('should render passed in children', () => {
    render(
      <ThemeWrapper>
        <ToggleMenu menuPosition="bottom">
          <div>testText</div>
        </ToggleMenu>
      </ThemeWrapper>,
    );
    fireEvent.click(screen.getByTestId('toggleMenuButton'));
    expect(screen.getByText('testText')).toBeTruthy();
  });

  it('should render passed in menu items', () => {
    const svgIcon: React.ReactElement = <svg data-testid="test-svg" />;

    render(
      <ThemeWrapper>
        <ToggleMenu
          menuItems={[
            { text: 'first', action: jest.fn(), icon: svgIcon },
            { text: 'second', action: jest.fn() },
          ]}
        />
      </ThemeWrapper>,
    );
    fireEvent.click(screen.getByTestId('toggleMenuButton'));
    expect(screen.getByText('first')).toBeTruthy();
    expect(screen.getByText('second')).toBeTruthy();
    expect(screen.getByTestId('test-svg')).toBeTruthy();
  });

  it('should render passed in header', () => {
    render(
      <ThemeWrapper>
        <ToggleMenu
          menuItems={[{ text: 'item', action: jest.fn() }]}
          menuTitle="header"
        />
      </ThemeWrapper>,
    );
    fireEvent.click(screen.getByTestId('toggleMenuButton'));
    expect(screen.getByTestId('toggleMenuTitle')).toBeTruthy();
    expect(screen.getByText('item')).toBeTruthy();
  });

  it('should not render header if not passed in', () => {
    render(
      <ThemeWrapper>
        <ToggleMenu menuItems={[{ text: 'item', action: jest.fn() }]} />
      </ThemeWrapper>,
    );
    fireEvent.click(screen.getByTestId('toggleMenuButton'));
    expect(screen.queryByTestId('toggleMenuTitle')).toBeFalsy();
    expect(screen.getByText('item')).toBeTruthy();
  });

  it('should toggle the menu when clicking a menu item', async () => {
    const mockAction = jest.fn();
    render(
      <ThemeWrapper>
        <ToggleMenu menuItems={[{ text: 'first', action: mockAction }]} />,
      </ThemeWrapper>,
    );
    expect(screen.queryByText('first')).toBeFalsy();
    fireEvent.click(screen.getByTestId('toggleMenuButton'));
    expect(screen.getByText('first')).toBeTruthy();
    fireEvent.click(screen.queryByText('first')!);
    await waitFor(() => {
      expect(screen.queryByText('first')).toBeFalsy();
    });

    await waitFor(() => {
      expect(mockAction).toHaveBeenCalled();
    });
  });

  it('should be able to pass variant', () => {
    const testVariant = 'tool';
    render(
      <ThemeWrapper>
        <ToggleMenu
          variant={testVariant}
          menuItems={[{ text: 'second', action: jest.fn() }]}
        />
      </ThemeWrapper>,
    );
    const button = screen.getByTestId('toggleMenuButton');

    expect(button.className).toContain(testVariant);
  });

  it('should show and hide tooltip', async () => {
    const props = {
      onClick: jest.fn(),
      isHidden: false,
      tooltipTitle: 'example toggle menu tooltip',
      menuItems: [{ text: 'second', action: jest.fn() }],
    };
    render(
      <ThemeWrapper>
        <ToggleMenu {...props} />
      </ThemeWrapper>,
    );
    const button = screen.queryByTestId('toggleMenuButton')!;
    expect(button).toBeTruthy();
    expect(screen.queryByRole('tooltip')).toBeFalsy();
    expect(screen.queryByText(props.tooltipTitle)).toBeFalsy();

    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(screen.getByText(props.tooltipTitle)).toBeTruthy();

    await user.unhover(button);
    await waitFor(() => expect(screen.queryByRole('tooltip')).toBeFalsy());
    expect(screen.queryByText(props.tooltipTitle)).toBeFalsy();
  });

  it('should show an empty menu when no menuItems given', () => {
    render(
      <ThemeWrapper>
        <ToggleMenu menuTitle="test title" />,
      </ThemeWrapper>,
    );
    expect(screen.getByTestId('toggleMenuButton')).toBeTruthy();
    fireEvent.click(screen.getByTestId('toggleMenuButton'));
    expect(screen.getByText('test title')).toBeTruthy();
    expect(screen.queryByRole('menuitem')).toBeFalsy();
  });

  it('should show an empty menu when menuItems is an empty list', () => {
    render(
      <ThemeWrapper>
        <ToggleMenu menuTitle="test title" menuItems={[]} />,
      </ThemeWrapper>,
    );
    expect(screen.getByTestId('toggleMenuButton')).toBeTruthy();
    fireEvent.click(screen.getByTestId('toggleMenuButton'));
    expect(screen.getByText('test title')).toBeTruthy();
    expect(screen.queryByRole('menuitem')).toBeFalsy();
  });

  it('should show custom button icon', async () => {
    const props = {
      onClick: jest.fn(),
      isHidden: false,
      tooltipTitle: 'example toggle menu tooltip',
      menuItems: [{ text: 'second', action: jest.fn() }],
    };
    render(
      <ThemeWrapper>
        <ToggleMenu {...props} buttonIcon={<span data-testid="testIcon" />} />
      </ThemeWrapper>,
    );
    expect(screen.getByTestId('testIcon')).toBeTruthy();
  });

  it('should disable the button icon', async () => {
    const props = {
      onClick: jest.fn(),
      isHidden: false,
      tooltipTitle: 'example toggle menu tooltip',
      menuItems: [{ text: 'second', action: jest.fn() }],
      isDisabled: true,
    };
    render(
      <ThemeWrapper>
        <ToggleMenu {...props} buttonIcon={<span data-testid="testIcon" />} />
      </ThemeWrapper>,
    );
    expect(
      screen.getByTestId('testIcon').getAttribute('disabled'),
    ).toBeDefined();
  });

  it('should show disabled menu items', async () => {
    const props = {
      menuItems: [
        { text: 'first', action: jest.fn(), isDisabled: true },
        { text: 'second', action: jest.fn(), isDisabled: false },
      ],
      isDefaultOpen: true,
    };
    render(
      <ThemeWrapper>
        <ToggleMenu {...props} />
      </ThemeWrapper>,
    );

    expect(screen.getAllByRole('menuitem')[0].classList).toContain(
      'Mui-disabled',
    );
    expect(screen.getAllByRole('menuitem')[1].classList).not.toContain(
      'Mui-disabled',
    );
  });

  describe('getPosition', () => {
    it('should return correct position for left', () => {
      expect(getPosition('left')).toEqual({
        anchorOrigin: {
          vertical: 'center',
          horizontal: 'left',
        },
        transformOrigin: {
          vertical: 'center',
          horizontal: 'right',
        },
      });
    });

    it('should return correct position for right', () => {
      expect(getPosition('right')).toEqual({
        anchorOrigin: {
          vertical: 'center',
          horizontal: 'right',
        },
        transformOrigin: {
          vertical: 'center',
          horizontal: 'left',
        },
      });
    });

    it('should return correct position for bottom', () => {
      expect(getPosition('bottom')).toEqual({
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'center',
        },
        transformOrigin: {
          vertical: 'top',
          horizontal: 'center',
        },
      });
    });
  });
});
