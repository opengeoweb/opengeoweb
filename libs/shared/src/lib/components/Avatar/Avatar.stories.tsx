/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import Avatar from './Avatar';
import { StorybookDocsWrapper } from '../Storybook';

const meta: Meta<typeof Avatar> = {
  title: 'components/Avatar',
  component: Avatar,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the user avatar',
      },
    },
  },
  argTypes: {
    size: {
      control: 'radio',
      options: ['small', 'large'],
      description: 'Size of icon, can be small or large',
    },
    children: {
      control: 'text',
      description: 'Avatar initials to render',
    },
  },
};
export default meta;

type Story = StoryObj<typeof Avatar>;

export const Component: Story = {
  args: {
    size: 'large',
    children: 'TU',
  },
};

const Demo = ({ ...props }): React.ReactElement => (
  <StorybookDocsWrapper
    {...props}
    sx={{
      width: '50px',
      padding: '10px',
    }}
  >
    <Avatar>G</Avatar>
    <br />
    <Avatar>GW</Avatar>
    <br />
    <Avatar size="small">G</Avatar>
    <br />
    <Avatar size="small">GW</Avatar>
  </StorybookDocsWrapper>
);

const parameters = {
  docs: {
    description: {
      story: 'Variations of Avatar',
    },
  },
};

export const AvatarLight: Story = {
  render: () => <Demo />,
  parameters: {
    ...parameters,
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60ec68c87ed4860e1d14f5dd',
      },
    ],
  },
  tags: ['snapshot'],
};

export const AvatarDark: Story = {
  render: () => <Demo isDark />,
  parameters: {
    ...parameters,
    zeplinLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60ec68cb9e0ee610eaf967ec',
      },
    ],
  },
  tags: ['dark', 'snapshot'],
};
