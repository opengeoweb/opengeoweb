/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { act, fireEvent, render, screen } from '@testing-library/react';

import userEvent from '@testing-library/user-event';
import SwitchButton from './SwitchButton';
import { SharedStoryWrapper } from '../Providers';
import { defaultDelay } from '../CustomTooltip';

describe('components/SwitchButton/SwitchButton', () => {
  beforeAll(() => {
    jest.useFakeTimers();
  });

  afterAll(() => {
    jest.useRealTimers();
  });
  it('should render SwitchButton with labels', async () => {
    render(
      <SharedStoryWrapper>
        <SwitchButton checked={false} onChange={jest.fn()} />
      </SharedStoryWrapper>,
    );
    expect(screen.getByTestId('switchMode')).toBeTruthy();
    expect(screen.getByTestId('switchMode').classList).not.toContain(
      'Mui-checked',
    );
    expect(screen.getByText('Viewer')).toBeTruthy();
    expect(screen.getByText('Editor')).toBeTruthy();
    const button = screen.getByRole('button')!;
    expect(button).toBeTruthy();
    const user = userEvent.setup({ advanceTimers: jest.advanceTimersByTime });
    await user.hover(button);

    act(() => {
      jest.advanceTimersByTime(defaultDelay);
    });
    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip.textContent).toContain('Switch mode');
  });

  it('should handle onChange', () => {
    const mockOnChange = jest.fn();
    const checked = true;
    render(
      <SharedStoryWrapper>
        <SwitchButton checked={checked} onChange={mockOnChange} />
      </SharedStoryWrapper>,
    );
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    fireEvent.click(screen.getByRole('checkbox'));
    expect(mockOnChange).toHaveBeenCalled();
  });

  it('should able to pass custom text', async () => {
    const props = {
      activeLabel: 'right',
      inActiveLabel: 'left',
      tooltip: 'my custom help text',
    };

    render(
      <SharedStoryWrapper>
        <SwitchButton {...props} />
      </SharedStoryWrapper>,
    );
    expect(screen.getByText(props.activeLabel)).toBeTruthy();
    expect(screen.getByText(props.inActiveLabel)).toBeTruthy();

    const button = screen.getByRole('button')!;
    expect(button).toBeTruthy();
    const user = userEvent.setup({ advanceTimers: jest.advanceTimersByTime });
    await user.hover(button);

    act(() => {
      jest.advanceTimersByTime(defaultDelay);
    });
    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip.textContent).toContain(props.tooltip);
  });
});
