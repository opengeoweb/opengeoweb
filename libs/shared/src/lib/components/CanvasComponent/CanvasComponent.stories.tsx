/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

/* eslint-disable no-param-reassign */
import { Meta, StoryObj } from '@storybook/react/*';
import { CanvasComponent } from './CanvasComponent';

const meta: Meta<typeof CanvasComponent> = {
  title: 'components/CanvasComponent',
  component: CanvasComponent,
  parameters: {
    docs: {
      description: {
        component: 'Canvas component',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof CanvasComponent>;

export const Component: Story = {
  args: {
    loop: false,
    redrawInterval: 0,
    onRenderCanvas: (
      ctx: CanvasRenderingContext2D,
      width: number,
      height: number,
    ): void => {
      ctx.moveTo(0, 0);
      ctx.lineTo(width, height);
      ctx.strokeStyle = 'blue';
      ctx.lineWidth = 2;
      ctx.stroke();
      for (let i = 0; i < 16; i += 1) {
        for (let j = 0; j < 16; j += 1) {
          ctx.strokeStyle = `rgb(
          0,
          ${Math.floor(255 - 16 * i)},
          ${Math.floor(255 - 16 * j)})`;
          ctx.beginPath();
          ctx.arc(12.5 + j * 25, 12.5 + i * 25, 10, 0, Math.PI * 2, true);
          ctx.stroke();
        }
      }
    },
  },
  render: (props) => (
    <div style={{ width: '100%', height: '300px' }}>
      <CanvasComponent {...props} />
    </div>
  ),
};

export const DynamicCanvas: Story = {
  parameters: {
    docs: {
      description: {
        story: 'Dynamic Canvas demo',
      },
    },
  },
  render: () => {
    let loopTimer = 0;
    let mouseX = 0;
    let mouseY = 0;
    return (
      <div style={{ width: '100%', height: '600px' }}>
        <CanvasComponent
          loop
          onMouseMove={(x, y): void => {
            mouseX = x;
            mouseY = y;
          }}
          onRenderCanvas={(
            ctx: CanvasRenderingContext2D,
            width: number,
            height: number,
          ): void => {
            loopTimer += 0.05;

            /* Clear canvas */
            ctx.beginPath();
            ctx.fillStyle = 'white';
            ctx.fillRect(0, 0, width, height);
            ctx.fillStyle = 'red';
            ctx.fillRect(mouseX - 20, mouseY - 20, 40, 40);

            /* Draw moving line */
            const x1 = Math.sin(loopTimer) * (height / 2) + width / 2;
            const y1 = Math.cos(loopTimer) * (height / 2) + height / 2;
            const x2 = Math.sin(loopTimer + Math.PI) * (height / 2) + width / 2;
            const y2 =
              Math.cos(loopTimer + Math.PI) * (height / 2) + height / 2;

            ctx.moveTo(x1, y1);
            ctx.lineTo(x2, y2);
            ctx.strokeStyle = 'blue';
            ctx.lineWidth = 10;
            ctx.stroke();
          }}
        />
      </div>
    );
  },
};

export const StaticCanvas: Story = {
  parameters: {
    docs: {
      description: {
        story: 'Static Canvas demo',
      },
    },
  },
  render: () => (
    <div style={{ width: '100%', height: '600px' }}>
      <CanvasComponent
        onRenderCanvas={(
          ctx: CanvasRenderingContext2D,
          width: number,
          height: number,
        ): void => {
          ctx.moveTo(0, 0);
          ctx.lineTo(width, height);
          ctx.strokeStyle = 'blue';
          ctx.lineWidth = 2;
          ctx.stroke();
          for (let i = 0; i < 16; i += 1) {
            for (let j = 0; j < 16; j += 1) {
              ctx.strokeStyle = `rgb(
            0,
            ${Math.floor(255 - 16 * i)},
            ${Math.floor(255 - 16 * j)})`;
              ctx.beginPath();
              ctx.arc(12.5 + j * 25, 12.5 + i * 25, 10, 0, Math.PI * 2, true);
              ctx.stroke();
            }
          }
        }}
      />
    </div>
  ),
};
