/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import Filter from './Filter';
import { DEBOUNCE_TIMEOUT } from './SearchField';
import { SharedStoryWrapper } from '../Providers';

describe('components/Filter', () => {
  it('should render component', async () => {
    render(
      <SharedStoryWrapper>
        <ThemeWrapper theme={lightTheme}>
          <Filter filters={[]} />
        </ThemeWrapper>
      </SharedStoryWrapper>,
    );
    expect(screen.getByTestId('filterList')).toBeTruthy();
    expect(screen.getByText('Search')).toBeTruthy();
  });

  it('should handle search', async () => {
    jest.useFakeTimers();
    const props = {
      filters: [],
      onSearch: jest.fn(),
      searchQuery: 'test',
    };
    render(
      <SharedStoryWrapper>
        <ThemeWrapper theme={lightTheme}>
          <Filter {...props} />
        </ThemeWrapper>
      </SharedStoryWrapper>,
    );
    expect(screen.getByTestId('filterList')).toBeTruthy();
    const textBox = screen.getByRole('textbox');
    expect(textBox.getAttribute('value')).toEqual(props.searchQuery);
    expect(props.onSearch).not.toHaveBeenCalled();

    const testInputValue = 'some new input';
    fireEvent.change(textBox, {
      target: {
        value: testInputValue,
      },
    });

    jest.advanceTimersByTime(DEBOUNCE_TIMEOUT);
    jest.clearAllTimers();
    jest.useRealTimers();

    expect(props.onSearch).toHaveBeenCalledWith(testInputValue);
  });
});
