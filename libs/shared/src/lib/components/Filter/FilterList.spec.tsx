/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import FilterList, { ListFilter } from './FilterList';
import { SharedStoryWrapper } from '../Providers';
import { translateKeyOutsideComponents } from '../../utils/i18n';

describe('src/lib/components/FilterList', () => {
  const isSelected = (labelElement: HTMLElement): boolean => {
    const { parentElement } = labelElement;
    return parentElement!.getAttribute('class')!.includes('MuiChip-filled');
  };

  it('should render component', async () => {
    const props = { onChipClick: jest.fn(), filters: [] };
    render(
      <SharedStoryWrapper>
        <ThemeWrapper theme={lightTheme}>
          <FilterList {...props} />
        </ThemeWrapper>
      </SharedStoryWrapper>,
    );

    expect(screen.getByTestId('filterList')).toBeTruthy();
    const filterAll = screen.getByText('All');
    const { parentElement } = filterAll;
    expect(parentElement!.classList.contains('MuiChip-filled')).toBeFalsy();
  });

  it('should render component with default filters if none passed', async () => {
    const props = { onChipClick: jest.fn() };
    render(
      <SharedStoryWrapper>
        <ThemeWrapper theme={lightTheme}>
          <FilterList {...props} />
        </ThemeWrapper>
      </SharedStoryWrapper>,
    );

    expect(screen.getByTestId('filterList')).toBeTruthy();
    expect(screen.getByText('All')).toBeTruthy();
  });

  it('should show option for specified filters and option to select all', () => {
    const filters: ListFilter[] = [
      { label: 'Filter1', id: 'id1', type: 'scope' },
      { label: 'Filter2', id: 'id2', type: 'scope' },
    ];
    const props = { onChipClick: jest.fn(), filters };
    const allLabel = translateKeyOutsideComponents('shared-filter-all');
    render(
      <SharedStoryWrapper>
        <ThemeWrapper theme={lightTheme}>
          <FilterList {...props} />
        </ThemeWrapper>
      </SharedStoryWrapper>,
    );

    expect(screen.getByText('Filter1')).toBeTruthy();
    expect(screen.getByText('Filter2')).toBeTruthy();
    expect(screen.getByText(allLabel)).toBeTruthy();
  });

  it('should render filter as selected', () => {
    const filters: ListFilter[] = [
      { id: 'F1', label: 'Filter1', isSelected: true, type: 'scope' },
      { id: 'F2', label: 'Filter2', type: 'scope' },
    ];
    const props = { onChipClick: jest.fn(), filters };
    const allLabel = translateKeyOutsideComponents('shared-filter-all');
    render(
      <SharedStoryWrapper>
        <ThemeWrapper theme={lightTheme}>
          <FilterList {...props} />
        </ThemeWrapper>
      </SharedStoryWrapper>,
    );

    expect(isSelected(screen.getByText('Filter1'))).toBeTruthy();
    const filter2 = screen.getByText('Filter2');
    expect(isSelected(filter2)).toBeFalsy();
    const all = screen.getByText(allLabel);
    expect(isSelected(all)).toBeFalsy();
  });

  it('should only render all-filter as selected ', () => {
    const filters: ListFilter[] = [
      { id: 'F1', label: 'Filter1', isSelected: true, type: 'scope' },
      { id: 'F2', label: 'Filter2', isSelected: true, type: 'scope' },
    ];
    const props = { onChipClick: jest.fn(), filters, isAllSelected: true };
    const allLabel = translateKeyOutsideComponents('shared-filter-all');
    render(
      <SharedStoryWrapper>
        <ThemeWrapper theme={lightTheme}>
          <FilterList {...props} />
        </ThemeWrapper>
      </SharedStoryWrapper>,
    );

    const filter1 = screen.getByText('Filter1');
    expect(isSelected(filter1)).toBeFalsy();
    const filter2 = screen.getByText('Filter2');
    expect(isSelected(filter2)).toBeFalsy();
    const all = screen.getByText(allLabel);
    expect(isSelected(all)).toBeTruthy();
  });

  it('should handle onClick and pass selected = true if allSelected is true', () => {
    const filters: ListFilter[] = [
      { id: 'F1', label: 'Filter1', isSelected: true, type: 'scope' },
      { id: 'F2', label: 'Filter2', isSelected: true, type: 'scope' },
    ];
    const props = { onChipClick: jest.fn(), filters, isAllSelected: true };
    render(
      <SharedStoryWrapper>
        <ThemeWrapper theme={lightTheme}>
          <FilterList {...props} />
        </ThemeWrapper>
      </SharedStoryWrapper>,
    );

    fireEvent.click(screen.getByText(props.filters[0].label));
    expect(props.onChipClick).toHaveBeenCalledWith(props.filters[0].id, true);
  });

  it('should handle onClick and pass selected from filter if allSelected is false', () => {
    const filters: ListFilter[] = [
      { id: 'F1', label: 'Filter1', isSelected: true, type: 'scope' },
      { id: 'F2', label: 'Filter2', isSelected: false, type: 'scope' },
    ];
    const props = { onChipClick: jest.fn(), filters };
    render(
      <SharedStoryWrapper>
        <ThemeWrapper theme={lightTheme}>
          <FilterList {...props} />
        </ThemeWrapper>
      </SharedStoryWrapper>,
    );

    fireEvent.click(screen.getByText(props.filters[0].label));
    expect(props.onChipClick).toHaveBeenCalledWith(
      props.filters[0].id,
      !props.filters[0].isSelected,
    );

    fireEvent.click(screen.getByText(props.filters[1].label));
    expect(props.onChipClick).toHaveBeenCalledWith(
      props.filters[1].id,
      !props.filters[1].isSelected,
    );
  });

  it('should handle show isAllSelected as true', () => {
    const filters: ListFilter[] = [
      { id: 'F1', label: 'Filter1', isSelected: true, type: 'scope' },
      { id: 'F2', label: 'Filter2', isSelected: false, type: 'scope' },
    ];
    const props = { onChipClick: jest.fn(), filters, isAllSelected: true };
    render(
      <SharedStoryWrapper>
        <ThemeWrapper theme={lightTheme}>
          <FilterList {...props} />
        </ThemeWrapper>
      </SharedStoryWrapper>,
    );
    const element = screen.getByText('All');
    const { parentElement } = element;

    expect(parentElement!.classList.contains('MuiChip-filled')).toBeTruthy();
  });
});
