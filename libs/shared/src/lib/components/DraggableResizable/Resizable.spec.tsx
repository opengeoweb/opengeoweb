/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { render, fireEvent, screen, renderHook } from '@testing-library/react';
import Resizable, {
  BoundsProps,
  isDraggingClassName,
  useBoundsProps,
} from './Resizable';
import { SharedThemeProvider } from '../Providers';

describe('components/Resizable', () => {
  it('should render successfully', async () => {
    const { baseElement } = render(
      <SharedThemeProvider>
        <Resizable>
          <div>test</div>
        </Resizable>
      </SharedThemeProvider>,
    );
    expect(baseElement).toBeTruthy();
    expect(await screen.findByText('test')).toBeTruthy();
  });
  it('should handle resize', async () => {
    const props = {
      onResizeStart: jest.fn(),
      size: { width: 50, height: 200 },
      setSize: jest.fn(),
    };

    render(
      <SharedThemeProvider>
        <Resizable {...props}>
          <div>test</div>
        </Resizable>
      </SharedThemeProvider>,
    );

    const resizeHandles = await screen.findAllByTestId('resizeHandle');
    expect(resizeHandles).toHaveLength(2);

    // Test both handles
    fireEvent.mouseDown(resizeHandles[0]);
    expect(props.onResizeStart).toHaveBeenCalledTimes(1);
    expect(props.setSize).toHaveBeenLastCalledWith({ width: 0, height: 0 });
    fireEvent.mouseUp(resizeHandles[0]);
    expect(props.setSize).toHaveBeenLastCalledWith(props.size);
    expect(props.setSize).toHaveBeenCalledTimes(2);

    fireEvent.mouseDown(resizeHandles[1]);
    expect(props.onResizeStart).toHaveBeenCalledTimes(2);
    expect(props.setSize).toHaveBeenLastCalledWith({ width: 0, height: 0 });
    fireEvent.mouseUp(resizeHandles[1]);
    expect(props.setSize).toHaveBeenLastCalledWith(props.size);
    expect(props.setSize).toHaveBeenCalledTimes(4);
  });

  it('should handle correct classNames when start/stop dragging', async () => {
    const props = {
      onResizeStart: jest.fn(),
      onResizeStop: jest.fn(),
      size: { width: 50, height: 200 },
      setSize: jest.fn(),
    };

    const { container } = render(
      <SharedThemeProvider>
        <Resizable {...props}>
          <div>test</div>
        </Resizable>
      </SharedThemeProvider>,
    );

    expect((container.firstChild as Element).classList).not.toContain(
      isDraggingClassName,
    );
    expect(props.onResizeStart).not.toHaveBeenCalled();
    expect(props.onResizeStop).not.toHaveBeenCalled();

    const resizeHandles = await screen.findAllByTestId('resizeHandle');
    expect(resizeHandles).toHaveLength(2);
    fireEvent.mouseDown(resizeHandles[0]);
    expect(props.onResizeStart).toHaveBeenCalled();
    expect(props.onResizeStop).not.toHaveBeenCalled();

    expect((container.firstChild as Element).classList).toContain(
      isDraggingClassName,
    );

    expect((container.firstChild as Element).classList).toContain(
      isDraggingClassName,
    );

    fireEvent.mouseUp(resizeHandles[0]);

    expect(props.onResizeStop).toHaveBeenCalled();

    expect((container.firstChild as Element).classList).not.toContain(
      isDraggingClassName,
    );
  });

  it('should not trigger callbacks when no size is given', async () => {
    const props = {
      onResizeStart: jest.fn(),
      onResizeStop: jest.fn(),
      setSize: jest.fn(),
    };

    const { container } = render(
      <SharedThemeProvider>
        <Resizable {...props}>
          <div>test</div>
        </Resizable>
      </SharedThemeProvider>,
    );

    expect((container.firstChild as Element).classList).not.toContain(
      isDraggingClassName,
    );
    expect(props.onResizeStart).not.toHaveBeenCalled();
    expect(props.onResizeStop).not.toHaveBeenCalled();

    const resizeHandles = await screen.findAllByTestId('resizeHandle');
    expect(resizeHandles).toHaveLength(2);
    expect(props.setSize).not.toHaveBeenCalled();

    fireEvent.mouseDown(resizeHandles[0]);
    expect(props.onResizeStart).toHaveBeenCalled();
    expect(props.onResizeStop).not.toHaveBeenCalled();
    expect(props.setSize).toHaveBeenCalledTimes(1);

    expect((container.firstChild as Element).classList).toContain(
      isDraggingClassName,
    );

    expect((container.firstChild as Element).classList).toContain(
      isDraggingClassName,
    );

    fireEvent.mouseUp(resizeHandles[0]);

    expect(props.onResizeStop).not.toHaveBeenCalled();
    expect(props.setSize).toHaveBeenCalledTimes(1);

    expect((container.firstChild as Element).classList).not.toContain(
      isDraggingClassName,
    );
  });

  describe('useBoundsProps', () => {
    it('should return bounds props when nodeRef is given', () => {
      const parent = document.createElement('div');
      const child = document.createElement('div');
      child.className = 'testing';

      parent.appendChild(child);

      const { result } = renderHook(() => useBoundsProps({ current: child }));

      expect((result.current as BoundsProps).bounds).toEqual(parent);
      expect((result.current as BoundsProps).boundsByDirection).toEqual(true);
    });
    it('should return empty object when no nodeRef is given', () => {
      const { result } = renderHook(() => useBoundsProps());
      expect(result.current).toEqual({});
    });
  });
});
