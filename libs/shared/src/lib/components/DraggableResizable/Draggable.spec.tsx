/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import Draggable, { DraggableComponentProps } from './Draggable';

describe('components/Draggable', () => {
  const TestWrapper: React.FC<{
    children: React.ReactNode;
    draggableProps?: DraggableComponentProps;
  }> = ({ children, draggableProps }) => {
    const nodeRef = React.useRef<HTMLDivElement>(null);
    return (
      <ThemeWrapper>
        <Draggable {...draggableProps} nodeRef={nodeRef}>
          <div ref={nodeRef}>{children}</div>
        </Draggable>
      </ThemeWrapper>
    );
  };
  it('should render successfully', async () => {
    const { baseElement } = render(
      <TestWrapper>
        <div>test</div>
      </TestWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(await screen.findByText('test')).toBeTruthy();
  });
  it('should handle dragging', async () => {
    const props = {
      bounds: 'parent',
      position: { x: 100, y: 50 },
      onStop: jest.fn(),
      handle: '#dragHandle',
    };
    const newClientX = 20;
    const newClientY = 30;

    render(
      <TestWrapper draggableProps={props}>
        <div>
          <div id="dragHandle" data-testid="dragHandle" />
          <div>test</div>
        </div>
      </TestWrapper>,
    );

    const dragHandle = await screen.findByTestId('dragHandle');
    fireEvent.mouseDown(dragHandle);
    expect(props.onStop).toHaveBeenCalledTimes(0);
    fireEvent.mouseUp(dragHandle, { clientX: newClientX, clientY: newClientY });
    expect(props.onStop).toHaveBeenLastCalledWith(
      expect.any(Object),
      expect.objectContaining({
        deltaX: newClientX,
        deltaY: newClientY,
        lastX: props.position.x,
        lastY: props.position.y,
        x: props.position.x + newClientX,
        y: props.position.y + newClientY,
      }),
    );
  });
});
