/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { ArrowDropDownDown, Delete } from '@opengeoweb/theme';
import { Box, Typography } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import { CustomAccordion } from './CustomAccordion';

const meta: Meta<typeof CustomAccordion> = {
  title: 'components/CustomAccordion',
  component: CustomAccordion,
  parameters: {
    docs: {
      description: {
        component: 'Custom accordion component',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof CustomAccordion>;

export const Component: Story = {
  // args,
  args: {
    defaultExpanded: false,
    title: 'Collapsed',
    children: 'some content',
    title2: '',
    expandedElevation: 3,
    collapsedElevation: 0,
    transitionDuration: 150,
    detailsSx: {},
    CustomExpandIcon: () => <ArrowDropDownDown />,
    disableGutters: true,
  },
};

const CustomAccordionExamples = (): React.ReactElement => {
  return (
    <Box sx={{ width: '300px' }}>
      <CustomAccordion defaultExpanded={false} title="Collapsed">
        Content
      </CustomAccordion>
      <CustomAccordion title="Expanded">
        <Box>Content 1</Box>
        <Box>Content 2</Box>
      </CustomAccordion>
      <CustomAccordion
        defaultExpanded={true}
        expandedElevation={0}
        title="Flat"
      >
        Content
      </CustomAccordion>
      <CustomAccordion
        defaultExpanded={true}
        expandedElevation={0}
        title={
          <>
            <Delete />{' '}
            <Typography
              sx={{
                display: 'flex',
                alignItems: 'center',
                fontSize: '0.75rem',
              }}
            >
              header with icon
            </Typography>
          </>
        }
      >
        Content
      </CustomAccordion>
    </Box>
  );
};

const parameters = {
  docs: {
    description: {
      story: 'Variations of CustomAccordion',
    },
  },
};

export const CustomAccordionLight: Story = {
  render: () => <CustomAccordionExamples />,
  tags: ['snapshot'],
  parameters,
};

export const CustomAccordionDark: Story = {
  render: () => <CustomAccordionExamples />,
  tags: ['snapshot', 'dark'],
  parameters,
};
