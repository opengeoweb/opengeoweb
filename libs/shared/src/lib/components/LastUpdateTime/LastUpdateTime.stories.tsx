/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Paper } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import LastUpdateTime from './LastUpdateTime';
import { StorybookDocsWrapper } from '../Storybook';

const meta: Meta<typeof LastUpdateTime> = {
  title: 'components/LastUpdateTime',
  component: LastUpdateTime,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the user LastUpdateTime',
      },
    },
  },
  argTypes: {},
};
export default meta;

type Story = StoryObj<typeof LastUpdateTime>;

export const Component: Story = {
  args: {
    lastUpdateTime: '12:43',
    dataTestId: '',
  },
};

const LastUpdateTimeDemo = ({ ...props }): React.ReactElement => {
  return (
    <StorybookDocsWrapper {...props} sx={{ width: 160 }}>
      <Paper sx={{ padding: '10px', minWidth: 160 }}>
        <LastUpdateTime
          lastUpdateTime="12:43"
          onPressRefresh={(): void => {
            // eslint-disable-next-line no-console
            console.log('on update time');
          }}
        />
      </Paper>
    </StorybookDocsWrapper>
  );
};

export const LastUpdateTimeLightTheme: Story = {
  render: () => <LastUpdateTimeDemo />,
  tags: ['snapshot'],
};

export const LastUpdateTimeDarkTheme: Story = {
  render: () => <LastUpdateTimeDemo isDark />,
  tags: ['dark', 'snapshot'],
};
