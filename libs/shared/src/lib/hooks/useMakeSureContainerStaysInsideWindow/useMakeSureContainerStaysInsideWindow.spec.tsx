/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { act, renderHook } from '@testing-library/react';
import {
  adjustPosition,
  useMakeSureContainerStaysInsideWindow,
} from './useMakeSureContainerStaysInsideWindow';

describe('hooks/useMakeSureContainerStaysInsideWindow', () => {
  it('should listen to window resize events', async () => {
    jest.spyOn(window, 'addEventListener');

    renderHook(() =>
      useMakeSureContainerStaysInsideWindow(
        { current: null },
        { x: 0, y: 0 },
        jest.fn(),
      ),
    );
    await act(async () => {
      window.dispatchEvent(new Event('resize'));
    });

    expect(window.addEventListener).toHaveBeenCalled();
    expect(window.addEventListener).toHaveBeenCalledWith(
      'resize',
      expect.any(Function),
    );
  });

  const rectDraggable = {
    width: 10,
    height: 10,
    top: 105,
    left: 105,
    bottom: 115,
    right: 115,
    x: 105,
    y: 105,
    toJSON: jest.fn(),
  };

  const rectBounding = {
    width: 20,
    height: 20,
    top: 100,
    left: 100,
    bottom: 120,
    right: 120,
    x: 100,
    y: 100,
    toJSON: jest.fn(),
  };

  const position = { x: rectDraggable.x, y: rectDraggable.y };

  it('test out of bounds left', async () => {
    const setPositionMock = jest.fn();

    const xDraggable = 99;

    adjustPosition(
      { ...rectDraggable, x: xDraggable },
      rectBounding,
      { ...position, x: xDraggable },
      setPositionMock,
    );

    expect(setPositionMock).toHaveBeenCalledWith({
      ...position,
      x: rectBounding.x,
    });
  });

  it('test out of bounds right', async () => {
    const setPositionMock = jest.fn();

    const xDraggable = 111;
    const rightDraggable = 121;

    adjustPosition(
      { ...rectDraggable, right: rightDraggable },
      rectBounding,
      { ...position, x: xDraggable },
      setPositionMock,
    );

    expect(setPositionMock).toHaveBeenCalledWith({
      ...position,
      x: rectBounding.right - rectDraggable.width,
    });
  });

  it('test out of bounds top', async () => {
    const setPositionMock = jest.fn();

    const yDraggable = 99;

    adjustPosition(
      { ...rectDraggable, y: yDraggable },
      rectBounding,
      { ...position, y: yDraggable },
      setPositionMock,
    );

    expect(setPositionMock).toHaveBeenCalledWith({
      ...position,
      y: rectBounding.y,
    });
  });

  it('test out of bounds bottom', async () => {
    const setPositionMock = jest.fn();

    const yDraggable = 111;
    const bottomDraggable = 121;

    adjustPosition(
      { ...rectDraggable, bottom: bottomDraggable },
      rectBounding,
      { ...position, y: yDraggable },
      setPositionMock,
    );

    expect(setPositionMock).toHaveBeenCalledWith({
      ...position,
      y: rectBounding.bottom - rectDraggable.height,
    });
  });

  it('test horizontally larger', async () => {
    const setPositionMock = jest.fn();

    const widthDraggable = 21;

    adjustPosition(
      { ...rectDraggable, width: widthDraggable },
      rectBounding,
      position,
      setPositionMock,
    );

    expect(setPositionMock).toHaveBeenCalledWith({
      ...position,
      x: rectBounding.x,
    });
  });

  it('test vertically larger', async () => {
    const setPositionMock = jest.fn();

    const heightDraggable = 21;

    adjustPosition(
      { ...rectDraggable, height: heightDraggable },
      rectBounding,
      position,
      setPositionMock,
    );

    expect(setPositionMock).toHaveBeenCalledWith({
      ...position,
      y: rectBounding.y,
    });
  });
});
