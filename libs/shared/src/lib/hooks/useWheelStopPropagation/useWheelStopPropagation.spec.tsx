/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { useWheelStopPropagation } from './useWheelStopPropagation';

const TestComponent: React.FC = () => {
  const ref = useWheelStopPropagation<HTMLDivElement>();

  return (
    <div ref={ref} data-testid="test-div">
      Test Div
    </div>
  );
};

describe('useWheelStopPropagation', () => {
  it('should stop wheel event propagation', () => {
    render(<TestComponent />);
    const testDiv = screen.getByTestId('test-div');

    const wheelEvent = new Event('wheel', { bubbles: true, cancelable: true });
    const stopPropagationSpy = jest.spyOn(wheelEvent, 'stopPropagation');

    fireEvent(testDiv, wheelEvent);

    expect(stopPropagationSpy).toHaveBeenCalled();
  });
});
