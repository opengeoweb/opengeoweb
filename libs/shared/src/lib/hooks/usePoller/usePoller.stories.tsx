/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { Button, Typography } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import { usePoller } from './usePoller';

const UsePollerDemo = ({
  timeout = 10000,
}: {
  timeout?: number;
}): React.ReactElement => {
  const [count, setCount] = React.useState(0);

  usePoller(
    [count],
    () => {
      setCount(count + 1);
    },
    timeout,
  );

  const seconds = new Date(timeout).getSeconds();
  const miliseconds = new Date(timeout).getMilliseconds();

  return (
    <>
      <Typography variant="subtitle1">
        <b>{`Using the poller, this counter will increase every ${seconds} seconds and ${miliseconds} miliseconds`}</b>
      </Typography>
      <Typography variant="body2">Count: {count}</Typography>
      <br />
      <br />
      <Typography variant="subtitle1">
        Pressing the button will add 2 and reset the timer back to 0
      </Typography>
      <Button
        color="secondary"
        variant="contained"
        onClick={(): void => setCount(count + 2)}
      >
        Click here
      </Button>
    </>
  );
};

const meta: Meta<typeof usePoller> = {
  title: 'hooks/UsePollerDemo',
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the user usePoller',
      },
    },
  },
  argTypes: {
    timeout: {
      control: 'number',
      description: 'interval in miliseconds for polling',
    },
  },
  render: (props) => <UsePollerDemo {...props} />,
};
export default meta;

type Story = StoryObj<typeof usePoller>;

export const Component: Story = {
  args: {
    timeout: 10000,
  },
  parameters: {
    docs: {
      description: {
        story:
          'Using the poller, this counter will increase every given seconds',
      },
    },
  },
};
