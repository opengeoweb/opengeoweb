/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { act, renderHook } from '@testing-library/react';
import { useDebounce } from './useDebounce';

// Mock setTimeout and clearTimeout
jest.useFakeTimers();

describe('useDebounce', () => {
  it('should return initial value immediately', () => {
    const { result } = renderHook(() => useDebounce('initial', 100));
    expect(result.current).toBe('initial');
  });

  it('should update value after debounce timeout', () => {
    const { result, rerender } = renderHook(
      ({ value, timeout }) => useDebounce(value, timeout),
      {
        initialProps: { value: 'initial', timeout: 100 },
      },
    );

    act(() => {
      jest.advanceTimersByTime(50); // Advance time by 50ms
    });

    rerender({ value: 'updated', timeout: 100 });

    act(() => {
      jest.advanceTimersByTime(100); // Advance time by 100ms to trigger debounce
    });

    expect(result.current).toBe('updated');
  });

  it('should not update value before debounce timeout', () => {
    const { result, rerender } = renderHook(
      ({ value, timeout }) => useDebounce(value, timeout),
      {
        initialProps: { value: 'initial', timeout: 100 },
      },
    );

    act(() => {
      jest.advanceTimersByTime(50); // Advance time by 50ms
    });

    rerender({ value: 'updated', timeout: 100 });

    act(() => {
      jest.advanceTimersByTime(50); // Advance time by another 50ms, not triggering debounce yet
    });

    expect(result.current).toBe('initial'); // Value should still be initial
  });
});
