/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  layerActions,
  serviceActions,
  uiActions,
  uiTypes,
} from '@opengeoweb/store';

import { layerSelectActions } from './reducer';
import { createMockStore } from './store';

describe('store/listeners', () => {
  it('should close Layer Info when closing Layer Select', () => {
    const store = createMockStore();
    // open layer select and layer info
    store.dispatch(
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.LayerSelect,
        setOpen: true,
      }),
    );
    store.dispatch(
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.LayerInfo,
        setOpen: true,
      }),
    );
    const layerInfo = {
      layerName: 'test-layer-name',
      serviceUrl: 'test.url',
    };
    store.dispatch(layerActions.showLayerInfo({ ...layerInfo, mapId: '1' }));
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerInfo]?.isOpen,
    ).toEqual(true);
    expect(store.getState().layers.activeLayerInfo).toEqual(layerInfo);

    store.dispatch(
      uiActions.setToggleOpenDialog({
        type: uiTypes.DialogTypes.LayerSelect,
        setOpen: false,
      }),
    );
    expect(store.getState().layers.activeLayerInfo).toEqual(undefined);

    store.dispatch(
      uiActions.setToggleOpenDialog({
        type: uiTypes.DialogTypes.LayerSelect,
        setOpen: true,
      }),
    );
    store.dispatch(layerActions.showLayerInfo({ ...layerInfo, mapId: '1' }));
    expect(store.getState().layers.activeLayerInfo).toEqual(layerInfo);

    store.dispatch(
      uiActions.setActiveMapIdForDialog({
        type: uiTypes.DialogTypes.LayerSelect,
        setOpen: false,
        mapId: 'test',
      }),
    );
    expect(store.getState().layers.activeLayerInfo).toEqual(undefined);
  });

  it('should close Layer Info when selected layer gets filtered out of the list', () => {
    const store = createMockStore();
    // open layer select and layer info
    store.dispatch(
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.LayerSelect,
        setOpen: true,
      }),
    );
    store.dispatch(
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.LayerInfo,
        setOpen: true,
      }),
    );
    const layerInfo = {
      layerName: 'RADAR',
      serviceUrl: 'test.url.nl',
    };
    store.dispatch(layerActions.showLayerInfo({ ...layerInfo, mapId: '2' }));
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerInfo]?.isOpen,
    ).toEqual(true);
    expect(store.getState().layers.activeLayerInfo).toEqual(layerInfo);
    store.dispatch(
      serviceActions.serviceSetLayers({
        id: 'serviceid_1',
        name: 'Radar',
        scope: 'system',
        serviceUrl: layerInfo.serviceUrl,
        layers: [
          {
            name: layerInfo.layerName,
            leaf: true,
            path: [],
            title: layerInfo.layerName,
            keywords: [],
          },
        ],
      }),
    );
    store.dispatch(layerSelectActions.setSearchFilter({ filterText: 'hello' }));
    expect(store.getState().layerSelect.filters.searchFilter).toEqual('hello');
    expect(store.getState().layers.activeLayerInfo).toEqual(undefined);
  });

  it('should not close Layer Info when selected layer does not get filtered out of the list', () => {
    const store = createMockStore();
    // open layer select and layer info
    store.dispatch(
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.LayerSelect,
        setOpen: true,
      }),
    );
    store.dispatch(
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.LayerInfo,
        setOpen: true,
      }),
    );
    const layerInfo = {
      layerName: 'RADAR',
      serviceUrl: 'test.url.nl',
    };
    store.dispatch(layerActions.showLayerInfo({ ...layerInfo, mapId: '2' }));
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerInfo]?.isOpen,
    ).toEqual(true);
    expect(store.getState().layers.activeLayerInfo).toEqual(layerInfo);
    store.dispatch(
      serviceActions.serviceSetLayers({
        id: 'serviceid_1',
        name: 'Radar',
        scope: 'system',
        serviceUrl: layerInfo.serviceUrl,
        layers: [
          {
            name: layerInfo.layerName,
            leaf: true,
            path: [],
            title: layerInfo.layerName,
            keywords: [],
          },
        ],
      }),
    );
    store.dispatch(layerSelectActions.setSearchFilter({ filterText: 'radar' }));
    expect(store.getState().layerSelect.filters.searchFilter).toEqual('radar');
    expect(store.getState().layers.activeLayerInfo).toEqual(layerInfo);
  });
});
