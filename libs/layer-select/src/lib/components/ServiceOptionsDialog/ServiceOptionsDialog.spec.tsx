/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { SystemScope } from '@opengeoweb/shared';
import { layerTypes } from '@opengeoweb/store';
import {
  fireEvent,
  render,
  screen,
  waitFor,
  within,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { createMockStore } from '../../store';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { TestThemeStoreProvider } from '../Providers';
import { ServiceOptionsDialog } from './ServiceOptionsDialog';

describe('src/lib/components/LayerManager/ServiceOptionsDialog', () => {
  const props = {
    services: {
      serviceid_1: {
        serviceName: 'DWD',
        serviceUrl: 'https://maps.dwd.de/geoserver/ows?',
        enabled: true,
        filterIds: [] as string[],
        scope: 'system' as SystemScope,
      },
      serviceid_2: {
        serviceName: 'KNMI Radar',
        serviceUrl: 'https:service.wms2',
        enabled: true,
        filterIds: [] as string[],
        scope: 'system' as SystemScope,
      },
      serviceid_3: {
        serviceName: 'MET Norway',
        serviceUrl: 'https:service.wms1',
        enabled: true,
        filterIds: [] as string[],
        scope: 'user' as SystemScope,
      },
    },
    layerSelectRemoveService: jest.fn(),
    layerSelectReloadService: jest.fn(),
    setServicePopupInfo: jest.fn(),
    selectedLayers: [
      {
        dimensions: {},
        enabled: true,
        id: 'layer_1',
        layerType: 'MapLayer',
        mapId: 'emptyMapView',
        name: 'Reflectivity',
        opacity: 1,
        service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR',
        status: 'default',
        style: 'radarReflectivity/nearest',
      },
    ] as unknown as layerTypes.ReduxLayer[],
  };
  const store = createMockStore();
  const user = userEvent.setup();

  it('should render the component', () => {
    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...props} />
      </TestThemeStoreProvider>,
    );

    const listItems = screen.getAllByRole('listitem');
    expect(listItems).toHaveLength(3);

    within(listItems[2]).getByRole('button', { name: /delete service/i });
    within(listItems[2]).getByRole('button', { name: /edit service/i });

    const buttons = within(listItems[1]).getAllByRole('button');
    expect(buttons).toHaveLength(2);
    expect(buttons[0]).toHaveAccessibleName(/update layers from wms service/i);
    expect(buttons[1]).toHaveAccessibleName(/show service/i);
  });

  it('should call layerSelectRemoveService with correct value', async () => {
    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...props} />
      </TestThemeStoreProvider>,
    );

    const removeButton = screen.getAllByTestId('removeServiceButton');
    fireEvent.click(removeButton[0]);
    expect(props.layerSelectRemoveService).toHaveBeenCalledWith(
      'serviceid_3',
      props.services['serviceid_3'].serviceUrl,
      props.services['serviceid_3'].serviceName,
    );
  });

  it('should disable remove button if service has active layers', async () => {
    const serviceHasActiveLayersProps = {
      services: {
        serviceid_1: {
          serviceName: 'MET Norway',
          serviceUrl: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR',
          enabled: true,
          filterIds: [] as string[],
          scope: 'user' as SystemScope,
        },
      },
      layerSelectRemoveService: jest.fn(),
      setServicePopupInfo: jest.fn(),
      selectedLayers: [
        {
          dimensions: {},
          enabled: true,
          id: 'layer_1',
          layerType: 'MapLayer',
          mapId: 'emptyMapView',
          name: 'Reflectivity',
          opacity: 1,
          service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR',
          status: 'default',
          style: 'radarReflectivity/nearest',
        },
      ] as unknown as layerTypes.ReduxLayer[],
    };

    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...serviceHasActiveLayersProps} />
      </TestThemeStoreProvider>,
    );

    await user.hover(screen.getByTestId('removeServiceTooltip'));
    expect(screen.getByTestId('removeServiceTooltip')).toBeTruthy();
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('service-has-active-layers-error'),
      ),
    ).toBeTruthy();
  });

  it('should enable remove button, if service doesn´t have any active layers', async () => {
    const serviceHasActiveLayersProps = {
      services: {
        serviceid_1: {
          serviceName: 'Some other service',
          serviceUrl: 'anotherServiceUrl',
          enabled: true,
          filterIds: [] as string[],
          scope: 'user' as SystemScope,
        },
      },
      layerSelectRemoveService: jest.fn(),
      setServicePopupInfo: jest.fn(),
      selectedLayers: [
        {
          dimensions: {},
          enabled: true,
          id: 'layer_1',
          layerType: 'MapLayer',
          mapId: 'emptyMapView',
          name: 'Reflectivity',
          opacity: 1,
          service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR',
          status: 'default',
          style: 'radarReflectivity/nearest',
        },
      ] as unknown as layerTypes.ReduxLayer[],
    };

    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...serviceHasActiveLayersProps} />
      </TestThemeStoreProvider>,
    );

    await user.hover(screen.getByTestId('removeServiceButton'));
    expect(screen.getByTestId('removeServiceButton')).toBeTruthy();
    expect(
      await screen.findByText(translateKeyOutsideComponents('delete-service')),
    ).toBeTruthy();
  });
  it('should trigger setServicePopupInfo on add service', async () => {
    const newProps = {
      ...props,
      setServicePopupInfo: jest.fn(),
    };
    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...newProps} />
      </TestThemeStoreProvider>,
    );

    fireEvent.click(screen.getByTestId('openAddServiceButton'));
    expect(screen.getByTestId('ServiceDialog')).toBeTruthy();
    expect(newProps.setServicePopupInfo).toHaveBeenCalledWith({
      isOpen: true,
      variant: 'add',
    });
  });

  it('should trigger setServicePopupInfo on add service', async () => {
    const newProps = {
      ...props,
      setServicePopupInfo: jest.fn(),
    };
    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...newProps} />
      </TestThemeStoreProvider>,
    );

    fireEvent.click(screen.getByTestId('openAddServiceButton'));
    expect(screen.getByTestId('ServiceDialog')).toBeTruthy();
    expect(newProps.setServicePopupInfo).toHaveBeenCalledWith({
      isOpen: true,
      variant: 'add',
    });
  });

  it('should call layerSelectReloadService with correct value', async () => {
    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...props} />
      </TestThemeStoreProvider>,
    );

    const updateServiceButton = screen.getAllByTestId('updateServiceButton');
    fireEvent.click(updateServiceButton[0]);
    expect(props.layerSelectReloadService).toHaveBeenCalledWith(
      props.services['serviceid_1'].serviceUrl,
    );
  });

  it('should show tooltip on hover over the first "update service" button but hide tooltip after wheel scroll starts', async () => {
    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...props} />
      </TestThemeStoreProvider>,
    );

    const firstUpdateServiceButton = screen.getAllByTestId(
      'updateServiceButton',
    )[0];
    expect(firstUpdateServiceButton).toBeInTheDocument();

    await user.hover(firstUpdateServiceButton);
    expect(await screen.findByRole('tooltip')).toBeTruthy();

    const tooltipTitleText = translateKeyOutsideComponents(
      'update-service-title',
    );
    expect(screen.getByText(tooltipTitleText)).toBeTruthy();
  });

  it('should show tooltip on hover over the first "show service" button but hide tooltip after wheel scroll starts', async () => {
    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...props} />
      </TestThemeStoreProvider>,
    );

    const firstShowServiceButton = screen.getAllByTestId(
      'openShowServiceButton',
    )[0];
    expect(firstShowServiceButton).toBeInTheDocument();

    await user.hover(firstShowServiceButton);
    expect(await screen.findByRole('tooltip')).toBeTruthy();

    const tooltipTitleText =
      translateKeyOutsideComponents('show-service-title');
    expect(screen.getByText(tooltipTitleText)).toBeTruthy();
  });

  it('should *not* close ServiceOptionsDialog when starting to traverse through options using either TAB or Shift TAB keys', async () => {
    const { container } = render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...props} />
      </TestThemeStoreProvider>,
    );

    await user.tab();
    expect(container).toBeTruthy();

    await user.tab({ shift: true });
    expect(container).toBeTruthy();
  });

  it('should *not* trigger update or show options on service option dialog when using TAB key', async () => {
    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...props} />
      </TestThemeStoreProvider>,
    );

    // Initially, focus is on first option
    // eslint-disable-next-line testing-library/no-node-access
    const firstOptionButton = document.activeElement!;
    expect(firstOptionButton).toBeTruthy();
    expect(firstOptionButton!.innerHTML).toContain(
      translateKeyOutsideComponents('update-service-title'),
    );

    // Move focus
    await user.tab();

    // No triggering of options should occur
    expect(props.layerSelectReloadService).not.toHaveBeenCalled();
    expect(props.setServicePopupInfo).not.toHaveBeenCalled();

    // Move focus
    await user.tab();
    await waitFor(() =>
      expect(
        screen.getAllByLabelText('show service')[0].matches(':focus'),
      ).toBeTruthy(),
    );

    // Still no triggering of options should occur
    expect(props.layerSelectReloadService).not.toHaveBeenCalled();
    expect(props.setServicePopupInfo).not.toHaveBeenCalled();
  });
});
