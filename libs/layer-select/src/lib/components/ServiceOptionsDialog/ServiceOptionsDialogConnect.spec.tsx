/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { SystemScope } from '@opengeoweb/shared';
import { serviceTypes } from '@opengeoweb/store';
import * as webmap from '@opengeoweb/webmap';
import { setWMSGetCapabilitiesFetcher } from '@opengeoweb/webmap';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { createMockStore } from '../../store';
import { initialState } from '../../store/reducer';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { TestThemeStoreProvider } from '../Providers';
import { ServiceOptionsDialogConnect } from './ServiceOptionsDialogConnect';

const { mockGetCapabilities } = webmap;

const mockServiceFromStore = {
  id: 'serviceid_1',
  title: 'Service test title',
  abstract: 'Service Abstract',
};

const mockState = {
  layerSelect: {
    ...initialState,
    filters: {
      ...initialState.filters,
      activeServices: {
        entities: {
          serviceid_1: {
            serviceName: 'Radar Norway',
            serviceUrl: mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
            enabled: true,
            filterIds: [],
            scope: 'system' as SystemScope,
          },
          serviceid_2: {
            serviceName: 'Precipitation Radar NL',
            serviceUrl: mockGetCapabilities.MOCK_URL_INVALID,
            enabled: true,
            filterIds: [],
            scope: 'user' as SystemScope,
          },
        },
        ids: ['serviceid_1', 'serviceid_2'],
      },
    },
  },
};

describe('src/lib/components/LayerManager/ServiceOptionsDialogConnect', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });
  const store = createMockStore({
    layerSelect: {
      ...initialState,
      filters: {
        ...initialState.filters,
        activeServices: {
          entities: {
            serviceid_1: {
              serviceName: 'Radar Norway',
              serviceUrl: mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
              enabled: true,
              filterIds: [],
              scope: 'system' as SystemScope,
            },
            serviceid_2: {
              serviceName: 'Precipitation Radar NL',
              serviceUrl: mockGetCapabilities.MOCK_URL_INVALID,
              enabled: true,
              filterIds: [],
              scope: 'user' as SystemScope,
            },
          },
          ids: ['serviceid_1', 'serviceid_2'],
        },
      },
    },
  });

  it('should render the component', () => {
    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </TestThemeStoreProvider>,
    );

    expect(screen.getByTestId('ServiceDialog')).toBeTruthy();
  });

  it('should dispatch LayerSelect removeService and openSnackbar action on click', async () => {
    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </TestThemeStoreProvider>,
    );
    const firstState = store.getState().layerSelect.filters.activeServices;

    expect(firstState.ids).toContain('serviceid_2');
    expect(firstState.ids).toHaveLength(2);

    fireEvent.click(screen.getByRole('button', { name: /delete service/i }));
    const secondState = store.getState().layerSelect.filters.activeServices;

    expect(
      screen.getByText(
        translateKeyOutsideComponents('service-has-been-deleted', {
          serviceName:
            mockState.layerSelect.filters.activeServices.entities.serviceid_2
              .serviceName,
        }),
      ),
    ).toBeTruthy();

    expect(secondState.ids).not.toContain('serviceid_2');
    expect(secondState.ids).toHaveLength(1);
  });

  it('should dispatch serviceSetLayers and openSnackbar action on updateServiceButton click', async () => {
    const store = createMockStore(mockState);

    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </TestThemeStoreProvider>,
    );

    const clearImageCacheForAllMapsSpy = jest.spyOn(
      webmap,
      'clearImageCacheForAllMaps',
    );

    const testService = {
      id: mockServiceFromStore.id,
      name: mockState.layerSelect.filters.activeServices.entities.serviceid_1
        .serviceName,
      serviceUrl: mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
      abstract: mockServiceFromStore.abstract,
      layers: [
        {
          abstract: undefined,
          crs: [],
          dimensions: [],
          geographicBoundingBox: undefined,
          keywords: [],
          leaf: true,
          name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
          path: ['RADNL_OPER_R___25PCPRR_L3_WMS'],
          queryable: false,
          styles: [],
          title: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
        },
        {
          abstract: undefined,
          crs: [],
          dimensions: [],
          geographicBoundingBox: undefined,
          keywords: [],
          leaf: true,
          name: 'RAD_NL25_PCP_CM',
          path: ['RADNL_OPER_R___25PCPRR_L3_WMS'],
          queryable: false,
          styles: [],
          title: 'RAD_NL25_PCP_CM',
        },
      ],
      scope:
        mockState.layerSelect.filters.activeServices.entities.serviceid_1.scope,
      isUpdating: true,
    } as serviceTypes.SetLayersForServicePayload;

    expect(store.getState().services.allIds).toHaveLength(0);
    const buttons = screen.getAllByTestId('updateServiceButton')[1];
    fireEvent.click(buttons);
    await waitFor(() => {
      expect(
        screen.getByText(
          translateKeyOutsideComponents('service-successfully-updated', {
            serviceName:
              mockState.layerSelect.filters.activeServices.entities.serviceid_1
                .serviceName,
          }),
        ),
      ).toBeTruthy();
    });

    expect(store.getState().services.allIds).toHaveLength(1);
    expect(store.getState().services.byId[mockServiceFromStore.id]).toEqual(
      testService,
    );

    expect(clearImageCacheForAllMapsSpy).toHaveBeenCalledTimes(1);
  });

  it('should dispatch openSnackbar action informing about an error on updateServiceButton click', async () => {
    const store = createMockStore(mockState);

    const clearImageCacheForAllMapsSpy = jest.spyOn(
      webmap,
      'clearImageCacheForAllMaps',
    );
    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </TestThemeStoreProvider>,
    );

    const buttons = screen.getAllByTestId('updateServiceButton')[0];
    fireEvent.click(buttons);

    await waitFor(() => {
      expect(
        screen.getByText(
          translateKeyOutsideComponents('unable-to-update-service', {
            message: "Url 'https://notawmsservice.nl' is not a wms service.",
          }),
        ),
      ).toBeTruthy();
    });

    expect(clearImageCacheForAllMapsSpy).toHaveBeenCalledTimes(0);
  });

  it('should dispatch trigger toggleServicePopup on add service', async () => {
    const store = createMockStore(mockState);

    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </TestThemeStoreProvider>,
    );

    expect(store.getState().layerSelect.servicePopup.isOpen).toBeFalsy();

    fireEvent.click(screen.getByTestId('openAddServiceButton'));

    expect(store.getState().layerSelect.servicePopup.isOpen).toBeTruthy();
  });
});
