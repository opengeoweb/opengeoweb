/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, screen, act } from '@testing-library/react';
import { mapActions, serviceActions } from '@opengeoweb/store';
import userEvent from '@testing-library/user-event';
import { TestThemeStoreProvider } from '../Providers';
import { KeywordFilterButtonConnect } from '../KeywordFilterButton/KeywordFilterButtonConnect';
import { KeywordFilterResultsConnect } from './KeywordFilterResultsConnect';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store';

describe('src/components/LayerSelect/KeywordFilterResults', () => {
  const keyword0 = 'keyword0';
  const keyword1 = 'keyword1';

  const mapId = 'mapId';
  it('should work', async () => {
    const store = createMockStore();
    render(
      <TestThemeStoreProvider store={store}>
        <div>
          <KeywordFilterButtonConnect mapId={mapId} />
          <KeywordFilterResultsConnect />
        </div>
      </TestThemeStoreProvider>,
    );

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(
        serviceActions.serviceSetLayers({
          id: 'serviceId',
          layers: [
            {
              leaf: true,
              name: 'layerName0',
              path: [],
              title: 'layerTitle0',
              keywords: [keyword1, keyword0],
            },
            {
              leaf: true,
              name: 'layerName1',
              path: [],
              title: 'layerTitle1',
              keywords: [keyword0],
            },
          ],
          name: 'name',
          scope: 'system',
          serviceUrl: 'serviceUrl',
        }),
      );
    });

    const user = userEvent.setup();

    await user.click(screen.getByTestId('keywordFilterButton'));

    screen.getByRole('heading', {
      name: translateKeyOutsideComponents('groups-and-keywords-dialog-title'),
    });

    // test correct order and keyword count
    const keywordsShown = screen.getAllByRole('listitem');
    expect(keywordsShown[0]).toHaveTextContent(`${keyword0} (2)`);
    expect(keywordsShown[1]).toHaveTextContent(`${keyword1} (1)`);

    expect(screen.getAllByRole('checkbox')).toHaveLength(3);

    const keyword0Name = `${keyword0} (2)`;
    const keyword1Name = `${keyword1} (1)`;
    expect(screen.getByRole('checkbox', { name: 'All' })).toBeChecked();
    expect(screen.getByRole('checkbox', { name: keyword0Name })).toBeChecked();
    expect(screen.getByRole('checkbox', { name: keyword1Name })).toBeChecked();

    fireEvent.click(screen.getByRole('checkbox', { name: keyword0Name }));
    expect(screen.getByRole('checkbox', { name: 'All' })).not.toBeChecked();
    expect(
      screen.getByRole('checkbox', { name: keyword0Name }),
    ).not.toBeChecked();
    expect(screen.getByRole('checkbox', { name: keyword1Name })).toBeChecked();

    fireEvent.click(screen.getByRole('checkbox', { name: keyword0Name }));
    expect(screen.getByRole('checkbox', { name: 'All' })).toBeChecked();
    expect(screen.getByRole('checkbox', { name: keyword0Name })).toBeChecked();
    expect(screen.getByRole('checkbox', { name: keyword1Name })).toBeChecked();

    fireEvent.click(screen.getByRole('checkbox', { name: 'All' }));
    expect(screen.getByRole('checkbox', { name: 'All' })).not.toBeChecked();
    expect(
      screen.getByRole('checkbox', { name: keyword0Name }),
    ).not.toBeChecked();
    expect(
      screen.getByRole('checkbox', { name: keyword1Name }),
    ).not.toBeChecked();

    fireEvent.click(screen.getByRole('checkbox', { name: 'All' }));
    expect(screen.getByRole('checkbox', { name: 'All' })).toBeChecked();
    expect(screen.getByRole('checkbox', { name: keyword0Name })).toBeChecked();
    expect(screen.getByRole('checkbox', { name: keyword1Name })).toBeChecked();

    expect(
      screen.queryByRole('button', { name: /only/i }),
    ).not.toBeInTheDocument();
    await user.hover(screen.getByRole('checkbox', { name: keyword0Name }));
    await screen.findByRole('button', { name: `Only ${keyword0Name}` });
    await user.click(
      screen.getByRole('button', { name: `Only ${keyword0Name}` }),
    );
    expect(screen.getByRole('checkbox', { name: 'All' })).not.toBeChecked();
    expect(screen.getByRole('checkbox', { name: keyword0Name })).toBeChecked();
    expect(
      screen.getByRole('checkbox', { name: keyword1Name }),
    ).not.toBeChecked();

    await user.click(screen.getByTestId('keywordFilterButton'));
    expect(
      screen.queryByRole('heading', {
        name: translateKeyOutsideComponents('groups-and-keywords-dialog-title'),
      }),
    ).not.toBeInTheDocument();
  });
});
