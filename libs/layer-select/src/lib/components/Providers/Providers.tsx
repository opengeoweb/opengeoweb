/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import i18n from 'i18next';
import { I18nextProvider } from 'react-i18next';
import { Theme } from '@mui/material';
import {
  lightTheme,
  ThemeProviderProps,
  ThemeWrapper,
} from '@opengeoweb/theme';
import { Provider } from 'react-redux';

import { Store } from '@reduxjs/toolkit';

import { SnackbarWrapperConnect } from '@opengeoweb/snackbar';
import { initLayerSelectI18n } from '../../utils/i18n';

interface BaseProviderProps {
  children?: React.ReactNode;
  theme?: Theme;
}

interface LayerSelectTranslationWrapperProps {
  children?: React.ReactNode;
  i18nInstance?: typeof i18n;
}

const ThemeProvider: React.FC<BaseProviderProps> = ({
  children,
  theme = lightTheme,
}) => <ThemeWrapper theme={theme}>{children}</ThemeWrapper>;

export const LayerSelecti18nProvider: React.FC<
  LayerSelectTranslationWrapperProps
> = ({ children, i18nInstance = initLayerSelectI18n() }) => {
  return <I18nextProvider i18n={i18nInstance}>{children}</I18nextProvider>;
};

export const DemoWrapper: React.FC<BaseProviderProps> = ({
  children,
  theme,
}) => {
  return (
    <LayerSelecti18nProvider>
      <ThemeProvider theme={theme}>{children}</ThemeProvider>
    </LayerSelecti18nProvider>
  );
};

const ThemeWrapperWithModules: React.FC<ThemeProviderProps> = ({
  theme,
  children,
}: ThemeProviderProps) => (
  <ThemeWrapper theme={theme}>{children} </ThemeWrapper>
);

export const LayerSelectStoreProvider: React.FC<
  ThemeProviderProps & {
    store: Store;
  }
> = ({ children, store }) => (
  <Provider store={store}>{children as React.ReactElement}</Provider>
);

export const TestThemeStoreProvider: React.FC<
  ThemeProviderProps & {
    store: Store;
  }
> = ({ children, theme = lightTheme, store }) => (
  <Provider store={store}>
    <LayerSelecti18nProvider>
      <ThemeWrapperWithModules theme={theme}>
        <SnackbarWrapperConnect>
          {children as React.ReactElement}
        </SnackbarWrapperConnect>
      </ThemeWrapperWithModules>
    </LayerSelecti18nProvider>
  </Provider>
);
