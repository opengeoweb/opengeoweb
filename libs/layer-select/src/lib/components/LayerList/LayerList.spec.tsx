/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';

import { layerTypes, storeTestSettings } from '@opengeoweb/store';
import { webmapTestSettings } from '@opengeoweb/webmap';
import { getDimensionsList } from '@opengeoweb/webmap-react';
import { LayerList, LayerListProps } from './LayerList';

import { TestThemeStoreProvider } from '../Providers';
import { LayerWithServiceName } from '../../store/types';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store';

const { defaultReduxLayerRadarKNMI, defaultReduxLayerRadarColor } =
  webmapTestSettings;

export const defaultReduxActiveLayers: LayerWithServiceName[] = [
  {
    serviceName: 'serviceid_1',
    name: 'RAD_NL25_PCP_CM',
    title: 'RADAR NL COLOR',
    leaf: true,
    path: ['BasisLayer'],
    keywords: ['keyword'],
    abstract: 'RADAR NL COLOR abstract',
    dimensions: [
      {
        name: 'reference_time',
        values: '2022-10-05,2022-10-06',
        units: 'ISO8601',
        currentValue: '2022-10-05',
      },
      {
        name: 'TIME',
        values: '2022-10-04T00:00:00Z/PT1H',
        units: 'ISO8601',
        currentValue: '2022-10-04T00:00:00Z',
      },
      {
        name: 'modellevel',
        values: '1,2,3,4,5',
        units: '-',
        currentValue: '1',
      },
      {
        name: 'Elevation',
        values: '1000,500,100',
        units: 'hPa',
        currentValue: '1000',
      },
      {
        name: 'member',
        values: '1,2,3',
        units: '-',
        currentValue: '1',
      },
    ],
  },
  {
    serviceName: 'serviceid_1',
    name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
    title: 'RADAR NL KNMI',
    leaf: true,
    path: ['testLayer'],
    keywords: ['testword'],
    abstract: 'RADAR NL KNMI abstract',
    styles: storeTestSettings.styleListForRADNLOPERR25PCPRRL3KNMILayer,
  },
  {
    serviceName: 'serviceid_1',
    name: 'MULTI_DIMENSION_LAYER',
    title: 'Multi Dimension layerTypes.Layer',
    leaf: true,
    path: [],
    keywords: [],
  },
];

export const defaultReduxActiveLayersWithoutOptionalVariables: LayerWithServiceName[] =
  [
    {
      serviceName: 'serviceid_1',
      name: 'RAD_NL25_PCP_CM',
      title: 'RADAR NL COLOR',
      leaf: true,
      path: [''],
    },
    {
      serviceName: 'serviceid_1',
      name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
      title: 'RADAR NL KNMI',
      leaf: true,
      path: [''],
      styles: storeTestSettings.styleListForRADNLOPERR25PCPRRL3KNMILayer,
    },
    {
      serviceName: 'serviceid_1',
      name: 'MULTI_DIMENSION_LAYER',
      title: 'Multi Dimension layerTypes.Layer',
      leaf: true,
      path: [],
    },
  ];

describe('src/components/LayerList', () => {
  const props: LayerListProps = {
    layerSelectWidth: 600,
    layerSelectHeight: 500,
    serviceListHeight: 200,
    services: storeTestSettings.defaultReduxServices,
    filteredLayers: defaultReduxActiveLayers,
    addLayer: jest.fn(),
    deleteLayer: jest.fn(),
    mapLayers: storeTestSettings.defaultReduxServices['serviceid_1']
      .layers! as unknown as layerTypes.ReduxLayer[],
    searchFilter: '',
    isLayerInfoVisibleInStore: true,
    onToggleActiveLayer: jest.fn(),
  };
  const store = createMockStore();

  it('should render layer list', async () => {
    render(
      <TestThemeStoreProvider store={store}>
        <LayerList {...props} />
      </TestThemeStoreProvider>,
    );

    expect(screen.getByTestId('layerList')).toBeTruthy();
  });

  it('should show correct amount of results', async () => {
    render(
      <TestThemeStoreProvider store={store}>
        <LayerList {...props} />
      </TestThemeStoreProvider>,
    );

    const results = translateKeyOutsideComponents('layer-list-results');
    expect(screen.getByText(`3 ${results}`)).toBeTruthy();
    expect(screen.getAllByTestId('layerListLayerRow').length).toEqual(3);
  });

  it('should show correct content for layer list layer rows', async () => {
    render(
      <TestThemeStoreProvider store={store}>
        <LayerList {...props} />
      </TestThemeStoreProvider>,
    );

    expect(
      screen.getAllByText(
        storeTestSettings.defaultReduxServices['serviceid_1'].name!,
      ),
    ).toBeTruthy();
    expect(screen.getByText('RADAR NL COLOR')).toBeTruthy();
    expect(screen.getByText('RADAR NL COLOR abstract')).toBeTruthy();
    expect(screen.getByText('keyword')).toBeTruthy();
    expect(screen.getByText('BasisLayer')).toBeTruthy();

    getDimensionsList(props.mapLayers[0].dimensions!).forEach((dimension) => {
      expect(
        screen.getByLabelText(`${dimension.label}: ${dimension.value}`),
      ).toBeTruthy();
    });
  });

  it('should show correct content for layer list layer rows on minimum size', async () => {
    render(
      <TestThemeStoreProvider store={store}>
        <LayerList {...props} layerSelectWidth={320} />
      </TestThemeStoreProvider>,
    );

    expect(
      screen.getAllByText(
        storeTestSettings.defaultReduxServices['serviceid_1'].name!,
      ),
    ).toBeTruthy();
    expect(screen.getByText('RADAR NL COLOR')).toBeTruthy();
    expect(screen.getByText('RADAR NL COLOR abstract')).toBeTruthy();
    expect(screen.getByText('keyword')).toBeTruthy();
    expect(screen.getByText('BasisLayer')).toBeTruthy();

    getDimensionsList(props.mapLayers[0].dimensions!).forEach((dimension) => {
      expect(
        screen.getByLabelText(`${dimension.label}: ${dimension.value}`),
      ).toBeTruthy();
    });
  });

  it('abstract text should be visible only when abstract is provided', async () => {
    render(
      <TestThemeStoreProvider store={store}>
        <LayerList {...props} />
      </TestThemeStoreProvider>,
    );

    expect(screen.getAllByTestId('layerListLayerRow').length).toEqual(3);
    expect(screen.getAllByTestId('layerAbstract').length).toEqual(2);
  });

  it('should render three layers each of them with "Add" buttons shown when no layers are provided', async () => {
    const props: LayerListProps = {
      layerSelectWidth: 600,
      layerSelectHeight: 500,
      serviceListHeight: 200,
      services: storeTestSettings.defaultReduxServices,
      filteredLayers: defaultReduxActiveLayers,
      addLayer: jest.fn(),
      deleteLayer: jest.fn(),
      mapLayers: [],
      searchFilter: '',
      isLayerInfoVisibleInStore: true,
      onToggleActiveLayer: jest.fn(),
    };

    render(
      <TestThemeStoreProvider store={store}>
        <LayerList {...props} />,
      </TestThemeStoreProvider>,
    );

    const results = translateKeyOutsideComponents('layer-list-results');
    expect(screen.getByText(`3 ${results}`)).toBeTruthy();
    expect(screen.getAllByRole('button').length).toEqual(6);
    expect(
      (await screen.findAllByText(translateKeyOutsideComponents('add'))).length,
    ).toEqual(3);
  });

  it('should render three layers: one layer with "Add" button and two layers with "Remove" button shown', async () => {
    const props: LayerListProps = {
      layerSelectWidth: 600,
      layerSelectHeight: 500,
      serviceListHeight: 200,
      services: storeTestSettings.defaultReduxServices,
      filteredLayers: defaultReduxActiveLayers,
      addLayer: jest.fn(),
      deleteLayer: jest.fn(),
      mapLayers: [defaultReduxLayerRadarColor, defaultReduxLayerRadarKNMI],
      searchFilter: '',
      isLayerInfoVisibleInStore: true,
      onToggleActiveLayer: jest.fn(),
    };

    render(
      <TestThemeStoreProvider store={store}>
        <LayerList {...props} />,
      </TestThemeStoreProvider>,
    );

    const results = translateKeyOutsideComponents('layer-list-results');
    expect(screen.getByText(`3 ${results}`)).toBeTruthy();
    expect(screen.getAllByRole('button').length).toEqual(6);
    expect(
      (await screen.findAllByText(translateKeyOutsideComponents('add'))).length,
    ).toEqual(1);
    expect(
      (await screen.findAllByText(translateKeyOutsideComponents('remove')))
        .length,
    ).toEqual(2);
  });

  it('should show the info button for each layer', () => {
    const mapId = '345';
    const props = {
      layerSelectWidth: 600,
      layerSelectHeight: 500,
      serviceListHeight: 200,
      services: storeTestSettings.defaultReduxServices,
      filteredLayers: defaultReduxActiveLayers,
      addLayer: jest.fn(),
      deleteLayer: jest.fn(),
      mapLayers: [],
      keywordIds: ['keyword', 'testword'],
      mapId,
      searchFilter: '',
      isLayerInfoVisibleInStore: true,
      onToggleActiveLayer: jest.fn(),
    };
    render(
      <TestThemeStoreProvider store={store}>
        <LayerList {...props} />
      </TestThemeStoreProvider>,
    );
    const buttons = screen.queryAllByRole('button', { name: 'layer info' });

    expect(buttons.length).toEqual(
      storeTestSettings.defaultReduxServices.serviceid_1.layers!.length,
    );
  });

  it('should render list without groups, keywords and abstract', () => {
    const mapId = '345';
    const props = {
      layerSelectWidth: 600,
      layerSelectHeight: 500,
      serviceListHeight: 200,
      services: storeTestSettings.defaultReduxServices,
      filteredLayers: defaultReduxActiveLayersWithoutOptionalVariables,
      addLayer: jest.fn(),
      deleteLayer: jest.fn(),
      mapLayers: [],
      mapId,
      searchFilter: '',
      isLayerInfoVisibleInStore: true,
      onToggleActiveLayer: jest.fn(),
    };
    render(
      <TestThemeStoreProvider store={store}>
        <LayerList {...props} />
      </TestThemeStoreProvider>,
    );
    expect(
      screen.getAllByText(
        storeTestSettings.defaultReduxServices['serviceid_1'].name!,
      ),
    ).toBeTruthy();
  });
  it('should render list without valid dimensions', () => {
    const mapId = '345';
    const props = {
      layerSelectWidth: 600,
      layerSelectHeight: 500,
      serviceListHeight: 200,
      services: storeTestSettings.defaultReduxServices,
      filteredLayers: defaultReduxActiveLayersWithoutOptionalVariables,
      addLayer: jest.fn(),
      deleteLayer: jest.fn(),
      mapLayers: [
        {
          serviceName: 'serviceid_1',
          name: 'RAD_NL25_PCP_CM',
          title: 'RADAR NL COLOR',
          leaf: true,
          path: ['BasisLayer'],
          keywords: ['keyword'],
          abstract: 'RADAR NL COLOR abstract',
          dimensions: [],
        },
      ],
      mapId,
      searchFilter: '',
      isLayerInfoVisibleInStore: true,
      onToggleActiveLayer: jest.fn(),
    };
    render(
      <TestThemeStoreProvider store={store}>
        <LayerList {...props} />
      </TestThemeStoreProvider>,
    );
    expect(
      screen.getAllByText(
        storeTestSettings.defaultReduxServices['serviceid_1'].name!,
      ),
    ).toBeTruthy();
  });
});
