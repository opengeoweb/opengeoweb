/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { LayerAddRemoveButton } from './LayerAddRemoveButton';
import { TestThemeStoreProvider } from '../Providers';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store';

describe('src/components/LayerList/LayerAddRemoveButton', () => {
  const testLayers = {
    layers: [
      {
        name: 'RAD_NL25_PCP_CM',
        service: 'https://testservice',
      },
      {
        name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
        service: 'https://testservice',
      },
      {
        name: 'MULTI_DIMENSION_LAYER',
        service: 'https://testservice',
      },
    ],
  };

  const props = {
    layer: {
      name: 'RAD_NL25_PCP_CM',
      service: 'https://testservice',
    },
    layerIndex: 0,
    serviceUrl: 'https://testservice',
    addLayer: jest.fn(),
    deleteLayer: jest.fn(),
    mapLayers: testLayers.layers,
  };

  const blahProps = {
    layer: {
      name: 'BLAH BLAH',
      service: 'https://testservice',
    },
    layerIndex: 0,
    serviceUrl: 'https://testservice',
    addLayer: jest.fn(),
    deleteLayer: jest.fn(),
    mapLayers: testLayers.layers,
  };

  it('should render the button component', async () => {
    const store = createMockStore();
    render(
      <TestThemeStoreProvider store={store}>
        <LayerAddRemoveButton {...props} />
      </TestThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(
        screen.getByTestId('layerAddRemoveButton-RAD_NL25_PCP_CM'),
      ).toBeTruthy();
    });
  });

  it('should render an "Add" button when layer given in props *cannot* be found in layers', async () => {
    const store = createMockStore();
    expect(blahProps.mapLayers[0].name === 'RAD_NL25_PCP_CM').toBeTruthy();
    expect(blahProps.layer.name === 'BLAH BLAH').toBeTruthy();
    expect(blahProps.mapLayers[0].name === blahProps.layer.name).toBeFalsy();

    render(
      <TestThemeStoreProvider store={store}>
        <LayerAddRemoveButton {...blahProps} />
      </TestThemeStoreProvider>,
    );

    const button = screen.getByText(translateKeyOutsideComponents('add'));
    await waitFor(() => {
      expect(button).toBeTruthy();
    });
    expect(screen.getByText(translateKeyOutsideComponents('add'))).toBeTruthy();
  });

  it('should render an "Remove" button when layer given in props *can* be found in layers', async () => {
    const store = createMockStore();
    expect(props.mapLayers[0].name === 'RAD_NL25_PCP_CM').toBeTruthy();
    expect(props.layer.name === 'RAD_NL25_PCP_CM').toBeTruthy();
    expect(props.mapLayers[0].name === props.layer.name).toBeTruthy();

    render(
      <TestThemeStoreProvider store={store}>
        <LayerAddRemoveButton {...props} />
      </TestThemeStoreProvider>,
    );

    const button = screen.getByText(translateKeyOutsideComponents('remove'));
    await waitFor(() => {
      expect(button).toBeTruthy();
    });
    expect(
      screen.getByText(translateKeyOutsideComponents('remove')),
    ).toBeTruthy();
  });

  it('should call addLayer when "Add" button is pressed', async () => {
    const store = createMockStore();

    render(
      <TestThemeStoreProvider store={store}>
        <LayerAddRemoveButton {...blahProps} />
      </TestThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(
        screen.getByText(translateKeyOutsideComponents('add')),
      ).toBeTruthy();
    });

    fireEvent.click(screen.getByTestId('layerAddRemoveButton-BLAH BLAH'));

    await waitFor(() => {
      expect(blahProps.addLayer).toHaveBeenCalled();
    });
  });

  it('should call deleteLayer when "Remove" button is pressed', async () => {
    const store = createMockStore();

    render(
      <TestThemeStoreProvider store={store}>
        <LayerAddRemoveButton {...props} />
      </TestThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(
        screen.getByText(translateKeyOutsideComponents('remove')),
      ).toBeTruthy();
    });

    fireEvent.click(screen.getByTestId('layerAddRemoveButton-RAD_NL25_PCP_CM'));

    await waitFor(() => {
      expect(props.deleteLayer).toHaveBeenCalled();
    });
  });
});
