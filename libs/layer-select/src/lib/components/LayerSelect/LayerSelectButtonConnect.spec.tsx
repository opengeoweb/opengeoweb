/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { uiTypes } from '@opengeoweb/store';
import { LayerSelectButtonConnect } from './LayerSelectButtonConnect';
import { TestThemeStoreProvider } from '../Providers';
import { createMockStore } from '../../store';

describe('src/components/LayerSelect/LayerSelectButtonConnect', () => {
  it('should dispatch action with passed in mapid when clicked', () => {
    const mockState = {
      ui: {
        dialogs: {
          layerSelect: {
            type: uiTypes.DialogTypes.LayerSelect,
            activeMapId: 'map1',
            isOpen: false,
          },
        },
        order: ['layerSelect'],
      },
    };
    const store = createMockStore(mockState);

    const props = {
      mapId: 'mapId_123',
    };
    render(
      <TestThemeStoreProvider store={store}>
        <LayerSelectButtonConnect {...props} />
      </TestThemeStoreProvider>,
    );

    const { layerSelect: initialState } = store.getState().ui.dialogs;
    expect(initialState.isOpen).toBeFalsy();
    expect(initialState.activeMapId).toEqual('map1');

    // button should be present
    expect(screen.getByTestId('layerSelectButton')).toBeTruthy();

    // close the legend dialog
    fireEvent.click(screen.getByTestId('layerSelectButton'));

    const { layerSelect: result } = store.getState().ui.dialogs;
    expect(result.isOpen).toBeTruthy();
    expect(result.activeMapId).toEqual(props.mapId);
  });

  it('should dispatch action to close dialog when dialog is already opened for corresponding map', () => {
    const mockState = {
      ui: {
        dialogs: {
          layerSelect: {
            type: uiTypes.DialogTypes.LayerSelect,
            activeMapId: 'mapId_123',
            isOpen: true,
            source: 'app' as const,
          },
        },
        order: ['layerSelect'],
      },
    };
    const store = createMockStore(mockState);

    const props = {
      mapId: 'mapId_123',
    };
    render(
      <TestThemeStoreProvider store={store}>
        <LayerSelectButtonConnect {...props} />
      </TestThemeStoreProvider>,
    );

    const { layerSelect: initialState } = store.getState().ui.dialogs;
    expect(initialState.isOpen).toBeTruthy();
    expect(initialState.activeMapId).toEqual('mapId_123');

    // button should be present
    expect(screen.getByTestId('layerSelectButton')).toBeTruthy();

    // close the legend dialog
    fireEvent.click(screen.getByTestId('layerSelectButton'));

    const { layerSelect: result } = store.getState().ui.dialogs;
    expect(result.isOpen).toBeFalsy();
    expect(result.activeMapId).toEqual(props.mapId);
  });

  it('should render default tooltip and icon', async () => {
    const mockState = {
      ui: {
        dialogs: {
          layerSelect: {
            type: uiTypes.DialogTypes.LayerSelect,
            activeMapId: 'map1',
            isOpen: false,
          },
        },
        order: ['layerSelect'],
      },
    };
    const store = createMockStore(mockState);

    const props = {
      mapId: 'mapId_123',
    };
    render(
      <TestThemeStoreProvider store={store}>
        <LayerSelectButtonConnect {...props} />
      </TestThemeStoreProvider>,
    );

    expect(screen.getByTestId('layerSelectButtonConnectIcon')).toBeTruthy();
    fireEvent.mouseOver(screen.getByTestId('layerSelectButtonConnectIcon'));
    expect(await screen.findByText('Open the layer selector')).toBeTruthy();
  });

  it('should render custom tooltip and icon when given', async () => {
    const mockState = {
      ui: {
        dialogs: {
          layerSelect: {
            type: uiTypes.DialogTypes.LayerSelect,
            activeMapId: 'map1',
            isOpen: false,
          },
        },
        order: ['layerSelect'],
      },
    };
    const store = createMockStore(mockState);
    const iconTestId = 'customIconTestId';
    const props = {
      mapId: 'mapId_123',
      icon: <span data-testid={iconTestId} />,
      tooltipTitle: 'Custom tooltip',
    };
    render(
      <TestThemeStoreProvider store={store}>
        <LayerSelectButtonConnect {...props} />
      </TestThemeStoreProvider>,
    );

    expect(screen.getByTestId(iconTestId)).toBeTruthy();
    fireEvent.mouseOver(screen.getByTestId(iconTestId));
    expect(await screen.findByText(props.tooltipTitle)).toBeTruthy();
  });

  it('should dispatch action with correct mapId when clicked with isMultiMap', () => {
    const mapId1 = 'mapId123';
    const mapId2 = 'mapId1234';
    const filterDialog1 = `${uiTypes.DialogTypes.LayerSelect}-${mapId1}`;
    const filterDialog2 = `${uiTypes.DialogTypes.LayerSelect}-${mapId2}`;
    const mockState = {
      ui: {
        dialogs: {
          [filterDialog2]: {
            type: `${uiTypes.DialogTypes.LayerSelect}-${mapId2}`,
            activeMapId: 'map1',
            isOpen: false,
          },
        },
        order: [filterDialog2],
      },
    };
    const store = createMockStore(mockState);

    render(
      <>
        <TestThemeStoreProvider store={store}>
          <LayerSelectButtonConnect mapId={mapId1} isMultiMap />
        </TestThemeStoreProvider>
        <TestThemeStoreProvider store={store}>
          <LayerSelectButtonConnect mapId={mapId2} isMultiMap />
        </TestThemeStoreProvider>
      </>,
    );
    const initialState = store.getState().ui.dialogs;

    expect(initialState[filterDialog1]).toBeFalsy();
    expect(initialState[filterDialog2]).toBeTruthy();
    expect(initialState[filterDialog2].activeMapId).toEqual('map1');
    expect(initialState[filterDialog2].isOpen).toBeFalsy();

    // button should be present
    for (const button of screen.getAllByTestId('layerSelectButton')) {
      expect(button).toBeTruthy();
      // open the legend dialog
      fireEvent.click(button);
    }

    const result = store.getState().ui.dialogs;
    expect(result[filterDialog1]).toBeFalsy();
    expect(result[filterDialog2]).toBeTruthy();
    expect(result[filterDialog2].activeMapId).toEqual(mapId2);
    expect(result[filterDialog2].isOpen).toBeTruthy();
  });
});
