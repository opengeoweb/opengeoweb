/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { layerTypes, mapUtils, uiTypes } from '@opengeoweb/store';
import type { Meta, StoryObj } from '@storybook/react';
import { Box } from '@mui/material';
import { LayerSelect } from './LayerSelect';
import { LayerSelectStoreProvider } from '../Providers';
import { layerSelectTypes, createMockStore } from '../../store';

const mockService = 'mockService';
const mockService2 = 'mockService2';
const mockService3 = 'mockService3';
const layerSelect: layerSelectTypes.LayerSelectStoreType = {
  filters: {
    activeServices: {
      entities: {
        [mockService]: {
          serviceId: mockService,
          serviceName: 'FMI',
          serviceUrl: 'mockserviceUrl1',
          enabled: true,
          filterIds: [],
        },
        [mockService2]: {
          serviceId: mockService2,
          serviceName: 'MET Norway',
          serviceUrl: 'mockserviceUrl2',
          enabled: true,
          filterIds: [],
        },
        [mockService3]: {
          serviceId: mockService3,
          serviceName: 'KNMI Radar',
          serviceUrl: 'mockserviceUrl3',
          enabled: true,
          filterIds: [],
        },
      },
      ids: [mockService, mockService2, mockService3],
    },
    searchFilter: 'group',
    filters: {
      entities: {},
      ids: [],
    },
  },
  allServicesEnabled: true,

  servicePopup: {
    variant: 'add',
    isOpen: false,
    serviceId: '',
    url: '',
  },
};

const mockState = {
  services: {
    byId: {
      [mockService]: {
        id: 'testId',
        name: 'testName',
        serviceUrl: mockService,
        layers: [
          {
            name: 'testLayerName',
            title: 'testLayerText',
            leaf: true,
            path: [
              'group, subgroup',
              'firstgroup',
              'group, subgroup, subsubgroup',
              'lastgroup, withsubgroup',
            ],
            keywords: [
              'keyword',
              'keyword2',
              'long keyword that activates overflow, long keyword that activates overflow, long keyword that activates overflow ',
            ],
            abstract:
              'Abstract that activates overflow. Temperature sunt autemquidam en 2 m ostris, qui in bonis sit Temperature sunt autemquidam en 2 m ostris, qui in bonis sit',
            dimensions: [
              {
                name: 'time',
                values: '2022-10-04T00:00:00Z/PT1H',
                units: 'ISO8601',
                currentValue: '2022-10-04T00:00:00Z',
              },
              {
                name: 'reference_time',
                values: '2022-10-05,2022-10-06',
                units: 'ISO8601',
                currentValue: '2022-10-05',
              },
              {
                name: 'modellevel',
                values: '1,2,3,4,5',
                units: '-',
                currentValue: '1',
              },
              {
                name: 'elevation',
                values: '1000,500,100',
                units: 'hPa',
                currentValue: '1000',
              },
              {
                name: 'member',
                values: '1,2,3',
                units: '-',
                currentValue: '1',
              },
            ],
          },
          {
            name: 'testLayerNameTwo',
            title: 'testLayerTextTwo',
            leaf: true,
            path: [],
            keywords: ['keyword'],
            dimensions: [
              {
                name: 'time',
                values: '2022-10-04T00:00:00Z/PT1H',
                units: 'ISO8601',
                currentValue: '2022-10-04T00:00:00Z',
              },
              {
                name: 'modellevel',
                values: '1,2,3,4,5',
                units: '-',
                currentValue: '1',
              },
              {
                name: 'member',
                values: '1,2,3',
                units: '-',
                currentValue: '1',
              },
            ],
          },
        ],
      },
      [mockService2]: {
        id: 'testId2',
        name: 'testName2',
        serviceUrl: mockService2,
        layers: [
          {
            name: 'testLayerNameForServiceTwo',
            title: 'testLayerTextForServiceTwo',
            leaf: true,
            path: ['group'],
            keywords: [],
            abstract: 'testLayerAbstractForServiceTwo',
          },
        ],
      },
      [mockService3]: {
        id: 'testId3',
        name: 'testName3',
        serviceUrl: mockService3,
        layers: [
          {
            name: 'testLayerNameForServiceThree',
            title: 'testLayerTextForServiceThree',
            leaf: true,
            path: [],
            keywords: [],
            abstract: 'testLayerAbstractForServiceThree',
          },
        ],
      },
    },
    allIds: [mockService, mockService2, mockService3],
  },
  layerSelect,
  ui: {
    order: [uiTypes.DialogTypes.LayerInfo],
    dialogs: {
      layerInfo: {
        activeMapId: '',
        isOpen: true,
        type: uiTypes.DialogTypes.LayerInfo,
        source: 'app' as const,
      },
    },
  },
  layers: {
    allIds: ['layerid_47'],
    byId: {
      layerid_47: {
        service: mockService,
        name: 'testLayerName',
        mapId: 'mapid_1',
        dimensions: [],
        id: 'layerid_47',
        opacity: 1,
        enabled: true,
        layerType: 'mapLayer',
        status: layerTypes.LayerStatus.default,
      } as layerTypes.Layer,
    },
    activeLayerInfo: {
      layerName: 'testLayerName',
      serviceUrl: mockService,
    },
    availableBaseLayers: {
      byId: {},
      allIds: [],
    },
  },
  webmap: {
    byId: {
      mapid_1: {
        ...mapUtils.createMap({ id: 'mapid_1' }),
        mapLayers: ['layerid_47'],
      },
    },
    allIds: ['mapid_1'],
  },
};

const store = createMockStore(mockState);

const meta: Meta<typeof LayerSelect> = {
  title: 'components/LayerSelect',
  component: LayerSelect,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the user LayerSelect',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof LayerSelect>;

export const Component: Story = {
  args: {
    showTitle: false,
    mapId: 'map-1',
  },
  render: (props) => (
    <LayerSelectStoreProvider store={store}>
      <Box minHeight={200}>
        <LayerSelect {...props} />
      </Box>
    </LayerSelectStoreProvider>
  ),
};

const LayerSelectDemo = (): React.ReactElement => (
  <LayerSelectStoreProvider store={store}>
    <div style={{ height: '100vh', backgroundColor: 'grey' }}>
      <LayerSelect mapId="mapid_1" bounds="parent" isOpen={true} />
    </div>
  </LayerSelectStoreProvider>
);

const docs = {
  description: {
    story: 'LayerSelect full size',
  },
};

export const LayerSelectDemoLightTheme: Story = {
  tags: ['snapshot'],
  parameters: {
    docs,
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62c695a1ed42f01adee34e63/version/633d283c83ab942dd0be309c',
      },
      {
        name: 'Chips light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/66e04c72bd27d8e668821841',
      },
    ],
    layout: 'fullscreen',
  },
  render: LayerSelectDemo,
};

export const LayerSelectDemoDarkTheme: Story = {
  tags: ['snapshot', 'dark'],
  parameters: {
    docs,
    zeplinLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62d7b06898ff1c11dbcc0d0a/version/633d2862b88ddf2c2e6cb2cf',
      },
      {
        name: 'Chips dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/66e04eeb0140fd4db7aba3b3',
      },
    ],
    layout: 'fullscreen',
  },
  render: LayerSelectDemo,
};

const LayerSelectDemoSmall = (): React.ReactElement => (
  <LayerSelectStoreProvider store={store}>
    <div style={{ height: '100vh', backgroundColor: 'grey' }}>
      <LayerSelect
        mapId="mapid_1"
        bounds="parent"
        isOpen={true}
        startSize={{ width: 320, height: 800 }}
      />
    </div>
  </LayerSelectStoreProvider>
);

const docsSmall = {
  description: {
    story: 'LayerSelect small size',
  },
};

export const LayerSelectDemoSmallLightTheme: Story = {
  tags: ['snapshot'],
  parameters: {
    docs: docsSmall,
    layout: 'fullscreen',
  },
  render: LayerSelectDemoSmall,
};

export const LayerSelectDemoSmallDarkTheme: Story = {
  tags: ['snapshot', 'dark'],
  parameters: {
    docs: docsSmall,
    layout: 'fullscreen',
  },
  render: LayerSelectDemoSmall,
};
