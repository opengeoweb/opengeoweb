/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { uiActions, uiTypes, serviceActions } from '@opengeoweb/store';

import {
  mockGetCapabilities,
  setWMSGetCapabilitiesFetcher,
} from '@opengeoweb/webmap';
import { TestThemeStoreProvider } from '../Providers';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { LayerSelectConnect } from './LayerSelectConnect';
import { createMockStore } from '../../store';

const mockServiceFromStore = {
  id: 'serviceid_1',
  title: 'Service test title',
  abstract: 'Service Abstract',
};

describe('src/components/LayerSelectConnect - integration', () => {
  const mockService = {
    name: mockServiceFromStore.title,
    url: mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
    abstract: mockServiceFromStore.abstract,
  };

  const user = userEvent.setup();
  const mapId = 'mapId_1';

  const store = createMockStore();
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });
  it('should add and remove a custom service', async () => {
    jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem');
    render(
      <TestThemeStoreProvider store={store}>
        <LayerSelectConnect />
      </TestThemeStoreProvider>,
    );

    await act(() =>
      store.dispatch(
        uiActions.setToggleOpenDialog({
          type: uiTypes.DialogTypes.LayerSelect,
          setOpen: true,
          mapId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(
        serviceActions.serviceSetLayers({
          id: 'serviceId',
          layers: [
            {
              leaf: true,
              name: 'layerName0',
              path: [],
              title: 'layerTitle0',
              keywords: [],
            },
            {
              leaf: true,
              name: 'layerName1',
              path: [],
              title: 'layerTitle1',
              keywords: [],
            },
          ],
          name: 'Default service name',
          scope: 'system',
          serviceUrl: 'serviceUrl',
        }),
      ),
    );

    await screen.findByTestId('layerSelectWindow');
    fireEvent.click(screen.getByTestId('serviceOptionsButton'));
    fireEvent.click(screen.getByTestId('openAddServiceButton'));
    const urlInput = await screen.findByLabelText(
      translateKeyOutsideComponents('service-url-label'),
    );
    await user.type(urlInput, mockService.url);
    await user.tab();
    await waitFor(() =>
      expect(
        screen
          .getByLabelText(translateKeyOutsideComponents('service-name-label'))
          .getAttribute('value'),
      ).toEqual(mockService.name),
    );

    fireEvent.click(screen.getByTestId('saveServiceButton'));

    // form should be closed
    await waitFor(() =>
      expect(screen.queryByTestId('saveServiceButton')).toBeFalsy(),
    );

    await waitFor(() =>
      expect(
        store.getState().layerSelect.filters.activeServices.ids.length,
      ).toEqual(2),
    );

    // check localstorage
    expect(window.localStorage.setItem).toHaveBeenCalledWith(
      'userAddedServices',
      JSON.stringify({ [mockService.url]: mockService }),
    );

    // close the service list popover
    fireEvent.click(screen.getByTestId('serviceOptionsButton'));

    // select service in layer select
    const chips = screen.getAllByTestId('serviceChip');
    expect(chips.length).toEqual(3);
    expect(chips[0].textContent).toEqual('All');
    expect(chips[2].textContent).toEqual(mockService.name);

    await screen.findByText('4 results');

    await user.click(chips[2]);

    // All should be inactive
    await waitFor(() =>
      expect(chips[0].classList).not.toContain('Mui-selected'),
    );
    expect(chips[2].classList).toContain('Mui-selected');

    await screen.findByText('2 results');

    // delete service
    fireEvent.click(screen.getByLabelText('delete service'));

    expect(screen.getAllByTestId('serviceChip').length).toEqual(2);

    // All should be active
    await waitFor(() => expect(chips[0].classList).toContain('Mui-selected'));

    // check localstorage
    expect(window.localStorage.getItem('userAddedServices')).toEqual('{}');
  });
});
