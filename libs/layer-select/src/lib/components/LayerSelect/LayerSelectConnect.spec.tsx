/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import { mapUtils, uiTypes } from '@opengeoweb/store';
import { LayerSelectConnect } from './LayerSelectConnect';
import { TestThemeStoreProvider } from '../Providers';
import { layerSelectInitialState, createMockStore } from '../../store';

describe('src/components/LayerSelect/LayerSelectConnect', () => {
  const storedFetch = global['fetch'];

  afterEach(() => {
    global['fetch'] = storedFetch;
  });

  it('should register the dialog when mounting', async () => {
    const mapId = 'mapId123';
    const mockState = {
      layerSelect: layerSelectInitialState,
      webmap: {
        byId: { [mapId]: mapUtils.createMap({ id: mapId }) },
        allIds: [mapId],
      },
    };
    const store = createMockStore(mockState);
    const initialState = store.getState().ui.dialogs;
    expect(initialState[uiTypes.DialogTypes.KeywordFilter]).toBeUndefined();
    expect(initialState[uiTypes.DialogTypes.LayerSelect]).toBeUndefined();

    expect(store.getState().ui.dialogs);
    render(
      <TestThemeStoreProvider store={store}>
        <LayerSelectConnect />
      </TestThemeStoreProvider>,
    );

    const result = store.getState().ui.dialogs;
    expect(result[uiTypes.DialogTypes.KeywordFilter]).toBeDefined();
    expect(result[uiTypes.DialogTypes.LayerSelect]).toBeDefined();
  });

  it('should register isMultiMap', async () => {
    const mapId = 'mapId123';
    const mockState = {
      webmap: {
        byId: {
          mapId123: mapUtils.createMap({ id: mapId }),
        },
        allIds: [mapId],
      },
      ui: {
        dialogs: {},
        order: [],
      },
      layers: {
        byId: {},
        allIds: [],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
    };
    const store = createMockStore(mockState);
    const keyWordFilterDialog = `${uiTypes.DialogTypes.KeywordFilter}-${mapId}`;
    const layerSelectDialog = `${uiTypes.DialogTypes.LayerSelect}-${mapId}`;

    const initialState = store.getState().ui.dialogs;
    expect(initialState[keyWordFilterDialog]).toBeUndefined();
    expect(initialState[layerSelectDialog]).toBeUndefined();

    render(
      <TestThemeStoreProvider store={store}>
        <LayerSelectConnect mapId={mapId} isMultiMap={true} />
      </TestThemeStoreProvider>,
    );

    const result = store.getState().ui.dialogs;
    expect(result[keyWordFilterDialog]).toBeDefined();
    expect(result[layerSelectDialog]).toBeDefined();
  });

  it('should register 2x isMultiMapap dialogs', async () => {
    const mapId1 = 'mapId123';
    const mapId2 = 'mapId456';
    const layerSelectDialog = `${uiTypes.DialogTypes.LayerSelect}-${mapId1}`;
    const mockState = {
      webmap: {
        byId: {
          mapId123: mapUtils.createMap({ id: mapId1 }),
          mapId456: mapUtils.createMap({ id: mapId2 }),
        },
        allIds: [mapId1, mapId2],
      },
      ui: {
        dialogs: {
          'layerSelect-mapId123': {
            isOpen: false,
            activeMapId: '',
            type: layerSelectDialog,
          },
        },
        order: ['layerSelect-mapId123'],
      },
      layers: {
        byId: {},
        allIds: [],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
    };
    const store = createMockStore(mockState);
    const keywordDialog = `${uiTypes.DialogTypes.KeywordFilter}-${mapId1}`;
    const layerSelect1Dialog = `${uiTypes.DialogTypes.LayerSelect}-${mapId1}`;
    const keywordFilterDialog = `${uiTypes.DialogTypes.KeywordFilter}-${mapId2}`;
    const layerSelect2Dialog = `${uiTypes.DialogTypes.LayerSelect}-${mapId2}`;

    const initialState = store.getState().ui.dialogs;

    expect(initialState[keywordDialog]).toBeUndefined();
    expect(initialState[layerSelect1Dialog]).toBeDefined();
    expect(initialState[keywordFilterDialog]).toBeUndefined();
    expect(initialState[layerSelect2Dialog]).toBeUndefined();

    render(
      <>
        <TestThemeStoreProvider store={store}>
          <LayerSelectConnect mapId={mapId1} isMultiMap={true} />
        </TestThemeStoreProvider>
        ,
        <TestThemeStoreProvider store={store}>
          <LayerSelectConnect mapId={mapId2} isMultiMap={true} />
        </TestThemeStoreProvider>
        ,
      </>,
    );

    const result = store.getState().ui.dialogs;

    expect(result[keywordDialog]).toBeDefined();
    expect(result[layerSelect1Dialog]).toBeDefined();
    expect(result[keywordFilterDialog]).toBeDefined();
    expect(result[layerSelect2Dialog]).toBeDefined();
  });
});
