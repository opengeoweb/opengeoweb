/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { DemoWrapper } from '../Providers';
import { ServiceChip } from './ServiceChip';
import { layerSelectTypes } from '../../store';

describe('src/components/LayerSelect/ServiceChip', () => {
  const defaultService: layerSelectTypes.ActiveServiceObject = {
    serviceName: 'testservice1',
    serviceUrl: 'https://testservice1',
    enabled: true,
    filterIds: [],
    abstract: 'Not available',
  };

  const props = {
    service: defaultService,
    isSelected: false,
    toggleChip: jest.fn(),
  };
  const disabledProps = {
    service: defaultService,
    isSelected: false,
    isDisabled: true,
    toggleChip: jest.fn(),
  };
  it('should have disabled state if isDisabled is true', () => {
    props.service.abstract = '';
    render(
      <DemoWrapper>
        <ServiceChip {...disabledProps} />,
      </DemoWrapper>,
    );

    const chip = screen.getByTestId('serviceChip');
    fireEvent.click(chip);
    expect(props.toggleChip).not.toHaveBeenCalled();
    expect(chip.classList.contains('Mui-disabled')).toBeTruthy();
  });

  it('should show tooltip when chip is hovered', async () => {
    props.service.abstract = 'WMS';
    render(
      <DemoWrapper>
        <ServiceChip {...props} />,
      </DemoWrapper>,
    );

    const chip = screen.getByTestId('serviceChip');
    expect(chip).toBeTruthy();

    fireEvent.mouseOver(chip);

    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip.textContent).toContain(
      `Abstract: ${defaultService.abstract}`,
    );
  });
});
