/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { act, render, screen } from '@testing-library/react';
import { serviceActions } from '@opengeoweb/store';
import userEvent from '@testing-library/user-event';
import { LayerType } from '@opengeoweb/webmap';
import { ServiceListConnect } from './ServiceListConnect';
import { TestThemeStoreProvider } from '../Providers';
import {
  layerSelectActions,
  layerSelectSelectors,
  createMockStore,
} from '../../store';

describe('src/components/LayerSelect/ServiceList', () => {
  it('should work as expected', async () => {
    const store = createMockStore();
    render(
      <TestThemeStoreProvider store={store}>
        <ServiceListConnect setHeight={jest.fn()} />,
      </TestThemeStoreProvider>,
    );

    const serviceName0 = 'abc';
    const serviceName1 = 'xyz';
    act(() => {
      store.dispatch(
        serviceActions.serviceSetLayers({
          id: 'id1',
          layers: [
            { leaf: true, name: 'layerName1', path: [], title: 'layerTitle1' },
          ],
          name: serviceName1,
          scope: 'system',
          serviceUrl: 'serviceUrl1',
        }),
      );

      store.dispatch(
        serviceActions.serviceSetLayers({
          id: 'id0',
          layers: [
            { leaf: true, name: 'layerName0', path: [], title: 'layerTitle0' },
          ],
          name: serviceName0,
          scope: 'system',
          serviceUrl: 'serviceUrl0',
        }),
      );
    });

    const user = userEvent.setup();

    // should render buttons in order by their name
    const buttons = screen.getAllByRole('button');
    const ALL = 'All';
    expect(buttons[0]).toHaveTextContent(ALL);
    expect(buttons[1]).toHaveTextContent(serviceName0);
    expect(buttons[2]).toHaveTextContent(serviceName1);

    // initially All button is pressed
    screen.getByRole('button', { name: ALL, pressed: true });
    screen.getByRole('button', { name: serviceName0, pressed: false });
    screen.getByRole('button', { name: serviceName1, pressed: false });

    // click name1
    await user.click(screen.getByRole('button', { name: serviceName1 }));

    screen.getByRole('button', { name: ALL, pressed: false });
    screen.getByRole('button', { name: serviceName0, pressed: false });
    screen.getByRole('button', { name: serviceName1, pressed: true });

    // click All
    await user.click(screen.getByRole('button', { name: ALL }));

    await screen.findByRole('button', { name: ALL, pressed: true });
    screen.getByRole('button', { name: serviceName0, pressed: false });
    screen.getByRole('button', { name: serviceName1, pressed: false });

    // click All again does nothing
    await user.click(screen.getByRole('button', { name: ALL }));

    screen.getByRole('button', { name: ALL, pressed: true });
    screen.getByRole('button', { name: serviceName0, pressed: false });
    screen.getByRole('button', { name: serviceName1, pressed: false });

    // click name0
    await user.click(screen.getByRole('button', { name: serviceName0 }));

    screen.getByRole('button', { name: ALL, pressed: false });
    screen.getByRole('button', { name: serviceName0, pressed: true });
    screen.getByRole('button', { name: serviceName1, pressed: false });

    // click name0 again turns All button on
    await user.click(screen.getByRole('button', { name: serviceName0 }));

    screen.getByRole('button', { name: ALL, pressed: true });
    screen.getByRole('button', { name: serviceName0, pressed: false });
    screen.getByRole('button', { name: serviceName1, pressed: false });

    // click name 1
    await user.click(screen.getByRole('button', { name: serviceName1 }));

    screen.getByRole('button', { name: ALL, pressed: false });
    screen.getByRole('button', { name: serviceName0, pressed: false });
    screen.getByRole('button', { name: serviceName1, pressed: true });
  });

  it('should render the service list with highlighted tags when a search result is found', () => {
    const store = createMockStore({
      webmap: {
        byId: {
          mapid_1: {
            id: 'mapid_1',
            isAnimating: false,
            animationStartTime: '2025-01-28T04:43:40Z',
            animationEndTime: '2025-01-28T09:33:40Z',
            isAutoUpdating: false,
            shouldEndtimeOverride: false,
            srs: 'EPSG:3857',
            bbox: {
              left: -19000000,
              bottom: -19000000,
              right: 19000000,
              top: 19000000,
            },
            baseLayers: [],
            overLayers: [],
            mapLayers: ['layerid_47'],
            featureLayers: [],
            dimensions: [],
            timeSliderSpan: 86400,
            timeStep: 60,
            animationDelay: 250,
            timeSliderWidth: 440,
            timeSliderCenterTime: 1738057420,
            timeSliderSecondsPerPx: 80,
            isTimestepAuto: true,
            isTimeSpanAuto: false,
            isTimeSliderHoverOn: false,
            isTimeSliderVisible: true,
            displayMapPin: false,
            disableMapPin: false,
            shouldShowZoomControls: true,
          },
        },
        allIds: ['mapid_1'],
      },
      services: {
        byId: {
          mockService: {
            id: 'testId',
            name: 'testName',
            serviceUrl: 'mockService',
            layers: [
              {
                name: 'testLayerName',
                title: 'testLayerText',
                leaf: true,
                path: [
                  'group, subgroup',
                  'firstgroup',
                  'group, subgroup, subsubgroup',
                  'lastgroup, withsubgroup',
                ],
                keywords: [
                  'keyword',
                  'keyword2',
                  'long keyword that activates overflow, long keyword that activates overflow, long keyword that activates overflow ',
                ],
                abstract:
                  'Abstract that activates overflow. Temperature sunt autemquidam en 2 m ostris, qui in bonis sit Temperature sunt autemquidam en 2 m ostris, qui in bonis sit',
                dimensions: [
                  {
                    name: 'time',
                    values: '2022-10-04T00:00:00Z/PT1H',
                    units: 'ISO8601',
                    currentValue: '2022-10-04T00:00:00Z',
                  },
                  {
                    name: 'reference_time',
                    values: '2022-10-05,2022-10-06',
                    units: 'ISO8601',
                    currentValue: '2022-10-05',
                  },
                  {
                    name: 'modellevel',
                    values: '1,2,3,4,5',
                    units: '-',
                    currentValue: '1',
                  },
                  {
                    name: 'elevation',
                    values: '1000,500,100',
                    units: 'hPa',
                    currentValue: '1000',
                  },
                  {
                    name: 'member',
                    values: '1,2,3',
                    units: '-',
                    currentValue: '1',
                  },
                ],
              },
              {
                name: 'testLayerNameTwo',
                title: 'testLayerTextTwo',
                leaf: true,
                path: [],
                keywords: ['keyword'],
                dimensions: [
                  {
                    name: 'time',
                    values: '2022-10-04T00:00:00Z/PT1H',
                    units: 'ISO8601',
                    currentValue: '2022-10-04T00:00:00Z',
                  },
                  {
                    name: 'modellevel',
                    values: '1,2,3,4,5',
                    units: '-',
                    currentValue: '1',
                  },
                  {
                    name: 'member',
                    values: '1,2,3',
                    units: '-',
                    currentValue: '1',
                  },
                ],
              },
            ],
          },
          mockService2: {
            id: 'testId2',
            name: 'testName2',
            serviceUrl: 'mockService2',
            layers: [
              {
                name: 'testLayerNameForServiceTwo',
                title: 'testLayerTextForServiceTwo',
                leaf: true,
                path: ['group'],
                keywords: [],
                abstract: 'testLayerAbstractForServiceTwo',
              },
            ],
          },
          mockService3: {
            id: 'testId3',
            name: 'testName3',
            serviceUrl: 'mockService3',
            layers: [
              {
                name: 'testLayerNameForServiceThree',
                title: 'testLayerTextForServiceThree',
                leaf: true,
                path: [],
                keywords: [],
                abstract: 'testLayerAbstractForServiceThree',
              },
            ],
          },
        },
        allIds: ['mockService', 'mockService2', 'mockService3'],
      },
      layers: {
        allIds: ['layerid_47'],
        byId: {
          layerid_47: {
            service: 'mockService',
            name: 'testLayerName',
            mapId: 'mapid_1',
            dimensions: [],
            id: 'layerid_47',
            opacity: 1,
            enabled: true,
            layerType: LayerType.mapLayer,
          },
        },
        activeLayerInfo: {
          layerName: 'testLayerName',
          serviceUrl: 'mockService',
        },
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
      layerSelect: {
        filters: {
          activeServices: {
            entities: {
              mockService: {
                serviceId: 'mockService',
                serviceName: 'FMI',
                serviceUrl: 'mockserviceUrl1',
                enabled: true,
                filterIds: [],
              },
              mockService2: {
                serviceId: 'mockService2',
                serviceName: 'MET Norway',
                serviceUrl: 'mockserviceUrl2',
                enabled: true,
                filterIds: [],
              },
              mockService3: {
                serviceId: 'mockService3',
                serviceName: 'KNMI Radar',
                serviceUrl: 'mockserviceUrl3',
                enabled: true,
                filterIds: [],
              },
            },
            ids: ['mockService', 'mockService2', 'mockService3'],
          },
          searchFilter: '',
          filters: {
            entities: {},
            ids: [],
          },
        },
        allServicesEnabled: true,
        servicePopup: {
          variant: 'add',
          isOpen: false,
          serviceId: '',
          url: '',
        },
      },
      ui: {
        order: ['layerInfo'],
        dialogs: {
          layerInfo: {
            activeMapId: '',
            isOpen: true,
            type: 'layerInfo',
            source: 'app',
          },
        },
      },
    });
    jest.useFakeTimers();
    render(
      <TestThemeStoreProvider store={store}>
        <ServiceListConnect setHeight={() => {}} />
      </TestThemeStoreProvider>,
    );

    expect(
      Object.values(
        layerSelectSelectors.getFilteredActiveServices(store.getState()),
      ).every((service) => service.isInSearch),
    ).toBeFalsy();

    act(() => {
      store.dispatch(
        layerSelectActions.setSearchFilter({
          filterText: 'group',
        }),
      );
    });

    act(() => {
      jest.runOnlyPendingTimers();
    });

    const searchResult = layerSelectSelectors.getFilteredActiveServices(
      store.getState(),
    );

    expect(searchResult['mockService'].isInSearch).toBeTruthy();
    expect(searchResult['mockService2'].isInSearch).toBeTruthy();
    expect(searchResult['mockService3'].isInSearch).toBeFalsy();
  });
});
