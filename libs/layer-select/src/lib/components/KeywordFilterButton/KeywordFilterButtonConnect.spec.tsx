/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { uiTypes } from '@opengeoweb/store';
import { KeywordFilterButtonConnect } from './KeywordFilterButtonConnect';
import { TestThemeStoreProvider } from '../Providers';
import { createMockStore } from '../../store';

describe('src/components/LayerSelect/KeywordFilterButton', () => {
  it('should dispatch action with correct mapId when clicked with isMultiMap', () => {
    const mapId1 = 'mapId123';
    const mapId2 = 'mapId1234';
    const filterDialogType1 = `${uiTypes.DialogTypes.KeywordFilter}-${mapId1}`;
    const filterDialogType2 = `${uiTypes.DialogTypes.KeywordFilter}-${mapId2}`;
    const mockState = {
      ui: {
        dialogs: {
          [filterDialogType1]: {
            type: filterDialogType1,
            activeMapId: 'map1',
            isOpen: false,
          },
        },
        order: [filterDialogType1],
      },
    };
    const store = createMockStore(mockState);

    render(
      <>
        <TestThemeStoreProvider store={store}>
          <KeywordFilterButtonConnect mapId={mapId1} isMultiMap />
        </TestThemeStoreProvider>
        <TestThemeStoreProvider store={store}>
          <KeywordFilterButtonConnect mapId={mapId2} isMultiMap />
        </TestThemeStoreProvider>
      </>,
    );

    expect(store.getState().ui.dialogs[filterDialogType1]).toBeTruthy();
    expect(store.getState().ui.dialogs[filterDialogType1].isOpen).toBeFalsy();
    expect(store.getState().ui.dialogs[filterDialogType1].activeMapId).toEqual(
      'map1',
    );
    expect(store.getState().ui.dialogs[filterDialogType2]).toBeFalsy();

    for (const button of screen.getAllByTestId('keywordFilterButton')) {
      expect(button).toBeTruthy();
      fireEvent.click(button);
    }

    expect(store.getState().ui.dialogs[filterDialogType1]).toBeTruthy();
    expect(store.getState().ui.dialogs[filterDialogType1].isOpen).toBeTruthy();
    expect(store.getState().ui.dialogs[filterDialogType1].activeMapId).toEqual(
      mapId1,
    );
    expect(store.getState().ui.dialogs[filterDialogType2]).toBeFalsy();
  });
});
