/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { SystemScope } from '@opengeoweb/shared';
import { act, fireEvent, render, screen } from '@testing-library/react';
import { createMockStore } from '../../store';
import { initialState } from '../../store/reducer';
import { TestThemeStoreProvider } from '../Providers';
import { SearchFieldConnect } from './SearchFieldConnect';

describe('components/LayerManager/LayerSelect/SearchField/SearchFieldConnect', () => {
  it('should dispatch action to set the filter', async () => {
    const mockState = {
      layerSelect: {
        ...initialState,
        filters: {
          ...initialState.filters,
          searchFilter: '',
          activeServices: {
            entities: {
              serviceid_1: {
                serviceName: 'MET Norway',
                serviceUrl: 'https://halo-wms.met.no/halo/default.map?',
                enabled: true,
                keywordsPerService: [],
                scope: 'system' as SystemScope,
              },
            },
            ids: ['serviceid_1'],
          },
          servicePopup: { isOpen: false, variant: 'add' },
        },
      },
    };
    const store = createMockStore(mockState);

    jest.useFakeTimers();

    render(
      <TestThemeStoreProvider store={store}>
        <SearchFieldConnect />
      </TestThemeStoreProvider>,
    );

    expect(store.getState().layerSelect.filters.searchFilter).toEqual('');
    const textField = screen.getByRole('textbox');
    fireEvent.change(textField, { target: { value: 'abc' } });

    act(() => {
      jest.runOnlyPendingTimers();
    });
    expect(store.getState().layerSelect.filters.searchFilter).toEqual('abc');

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should dispatch action to open Service Popup to save WMS url', async () => {
    const mockState = {
      layerSelect: {
        ...initialState,
        filters: {
          ...initialState.filters,
          searchFilter: '',
          activeServices: {
            entities: {
              serviceid_1: {
                serviceName: 'MET Norway',
                serviceUrl: 'https://halo-wms.met.no/halo/default.map?',
                enabled: true,
                keywordsPerService: [],
                scope: 'system' as SystemScope,
              },
            },
            ids: ['serviceid_1'],
          },
          servicePopup: { isOpen: false, variant: 'add' },
        },
      },
    };
    const store = createMockStore(mockState);
    jest.useFakeTimers();

    render(
      <TestThemeStoreProvider store={store}>
        <SearchFieldConnect />
      </TestThemeStoreProvider>,
    );

    expect(store.getState().layerSelect.servicePopup.isOpen).toBeFalsy();
    const textField = screen.getByRole('textbox');
    fireEvent.change(textField, { target: { value: 'https://testService' } });

    jest.runOnlyPendingTimers();

    fireEvent.click(screen.getByText('Save'));

    expect(store.getState().layerSelect.servicePopup.isOpen).toBeTruthy();

    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
