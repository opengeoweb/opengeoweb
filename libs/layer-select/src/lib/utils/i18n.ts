/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import i18n from 'i18next';
import {
  initReactI18next,
  useTranslation,
  UseTranslationResponse,
} from 'react-i18next';
import { SHARED_NAMESPACE, sharedTranslations } from '@opengeoweb/shared';
import layerSelectTranslations from '../../../locales/layer-select.json';

export const LAYER_SELECT_NAMESPACE = 'layerselect';

export const initLayerSelectI18n = (): typeof i18n => {
  void i18n.use(initReactI18next).init({
    lng: 'en',
    ns: LAYER_SELECT_NAMESPACE,
    interpolation: {
      escapeValue: false,
    },
    resources: {
      en: {
        [LAYER_SELECT_NAMESPACE]: layerSelectTranslations.en,
        [SHARED_NAMESPACE]: sharedTranslations.en,
      },
      fi: {
        [LAYER_SELECT_NAMESPACE]: layerSelectTranslations.fi,
        [SHARED_NAMESPACE]: sharedTranslations.fi,
      },
      no: {
        [LAYER_SELECT_NAMESPACE]: layerSelectTranslations.no,
        [SHARED_NAMESPACE]: sharedTranslations.no,
      },
      nl: {
        [LAYER_SELECT_NAMESPACE]: layerSelectTranslations.nl,
        [SHARED_NAMESPACE]: sharedTranslations.nl,
      },
    },
  });
  return i18n;
};

export const useLayerSelectTranslation = (): UseTranslationResponse<
  typeof LAYER_SELECT_NAMESPACE,
  typeof i18n
> => useTranslation(LAYER_SELECT_NAMESPACE);

export const translateKeyOutsideComponents = (
  key: string,
  params: Record<string, string | number> | undefined = undefined,
): string => i18n.t(key, { ns: [LAYER_SELECT_NAMESPACE], ...params });

export { i18n };
