/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2021 - The Norwegian Meteorological Institute (MET Norway)
 * */
import type { StorybookConfig } from '@storybook/react-webpack5';

const rootMain = require('../../../.storybook/main');
const isProduction = process.env.NODE_ENV?.toUpperCase() === 'PRODUCTION';

const getStorybook = (title: string, portNumber: number, libName: string) => ({
  title,
  url: !isProduction ? `http://localhost:${portNumber}` : `../${libName}`,
  expanded: false,
});

const config: StorybookConfig = {
  ...rootMain,
  stories: ['../src/lib/**/*.stories.@(js|jsx|ts|tsx|mdx)', '../*.mdx'],
  refs: {
    theme: getStorybook('Theme', 4900, 'theme'),
    shared: getStorybook('Shared', 4800, 'shared'),
    authentication: getStorybook('Authentication', 7100, 'authentication'),
    core: getStorybook('Core', 5001, 'core'),
    formfields: getStorybook('Form fields', 9009, 'form-fields'),
    layerselect: getStorybook('Layer select', 4401, 'layer-select'),
    metronome: getStorybook('Metronome', 5701, 'metronome'),
    sigmetairmet: getStorybook('Sigmet airmet', 4404, 'sigmet-airmet'),
    snackbar: getStorybook('Snackbar', 4501, 'snackbar'),
    spaceweather: getStorybook('Spaceweather', 4700, 'spaceweather'),
    taf: getStorybook('Taf', 4500, 'taf'),
    warnings: getStorybook('Warnings', 5500, 'warnings'),
    timeslider: getStorybook('Timeslider', 4402, 'timeslider'),
    timesliderlite: getStorybook('Timesliderlite', 5700, 'timesliderlite'),
    webmapreact: getStorybook('Webmap react', 5600, 'webmap-react'),
    cap: getStorybook('Cap', 5100, 'cap'),
    workspace: getStorybook('Workspace', 5200, 'workspace'),
    timeseries: getStorybook('Timeseries', 5300, 'timeseries'),
  },
};

export default config;
