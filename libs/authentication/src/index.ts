/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import authTranslations from '../locales/authentication.json';
import apiTranslations from '../locales/api.json';

export { default as Code } from './lib/components/pages/Code';
export { default as Login } from './lib/components/pages/Login';
export { default as Logout } from './lib/components/pages/Logout';
export * from './lib/components/UserMenuRoles';
export * from './lib/components/AuthenticationContext';
export * from './lib/components/PrivateRoute';
export * from './lib/components/ApiContext';
export * from './lib/components/apiHooks';
export * from './lib/utils/session';
export * from './lib/utils/utils';
export { AUTH_NAMESPACE } from './lib/utils/i18n';
export { authTranslations, apiTranslations };
