/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import axios, {
  AxiosHeaders,
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
} from 'axios';
import { ApiUrls, Credentials, GeoWebJWT } from './types';
import {
  createApiInstance,
  createFakeApiInstance,
  createNonAuthApiInstance,
  GEOWEB_ROLE_PRESETS_ADMIN,
  GEOWEB_ROLE_USER,
  getCurrentTimeInSeconds,
  groupsToRoles,
  makeCredentialsFromTokenResponse,
  MILLISECOND_TO_SECOND,
  refreshAccessTokenAndSetAuthContext,
  REFRESH_TOKEN_WHEN_PCT_EXPIRED,
} from './utils';

const ID_TOKEN =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdF9oYXNoIjoiM1duLW1nMGJUWWhzblFBIiwic3ViIjoiMzg3OWMzNWUtZTMwNi01YzExMDM3NWE0NGEiLCJhdWQiOiIyb2w2M3YwZ2x1ZjlyNjdzbGF1djZ1aiIsImNvZ25pdG86Z3JvdXBzIjpbImFkbWluaXN0cmF0b3JzIl0sImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTYzMTYzNDQ3LCJpc3MiOiJodHRwczovL2NvZ25pdG8taWRwLmV1LXdlc3QtMS5hbWF6b25hd3MuY29tIiwiY29nbml0bzp1c2VybmFtZSI6Im1heC52ZXJzdGFwcGVuIiwiZXhwIjoxNjMxMjcwNDcsImlhdCI6MTYzMTIzNDQ3LCJlbWFpbCI6Im1heC52ZXJzdGFwcGVuQGZha2VlbWFpbC5ubCJ9.nuMsFX9NE1xGuGVb8qPaJ4kDX2qeTx54ui9wTQNTzhQ';

describe('src/lib/components/ApiContext/utils', () => {
  describe('createFakeApiInstance', () => {
    it('should create fake api instance', () => {
      const fakeInstance = createFakeApiInstance();

      expect(fakeInstance.get).toBeDefined();
      expect(fakeInstance.put).toBeDefined();
      expect(fakeInstance.post).toBeDefined();
      expect(fakeInstance.delete).toBeDefined();
      expect(fakeInstance.patch).toBeDefined();
    });
  });

  describe('createApiInstance', () => {
    it('should create api instance', async () => {
      const props = {
        auth: {
          username: 'Michael Jackson',
          roles: [GEOWEB_ROLE_USER],
          token: '1223344',
          refresh_token: '33455214',
        },
        config: {
          baseURL: 'fakeURL',
        },
      };
      const instance = createApiInstance(props);

      expect(instance.get).toBeDefined();
      expect(instance.put).toBeDefined();
      expect(instance.post).toBeDefined();
      expect(instance.delete).toBeDefined();
      expect(instance.patch).toBeDefined();
    });

    it('should retry when a 401 is encountered', async () => {
      const dateAtWhichTokenExpires = `2021-12-20T13:00:00Z`;
      const mockedCurrentDate = `2021-12-20T12:59:00Z`;
      const props = {
        auth: {
          username: 'Michael Jackson',
          roles: [GEOWEB_ROLE_USER],
          token: '1223344',
          refresh_token: '33455214',
          expires_at:
            new Date(dateAtWhichTokenExpires).valueOf() * MILLISECOND_TO_SECOND,
        },
        onSetAuth: jest.fn(),
        config: {
          baseURL: 'fakeURL',
          appURL: 'fakeUrl',
          authTokenURL: 'fakeAuthTokenURL',
          authClientId: 'fakeauthClientId',
        },
      };

      const instance = createApiInstance(props);

      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(mockedCurrentDate).valueOf());

      /* 
        Setting the response handler to as if a 401 has happened.
        A new AxiosRequestConfig is made with an adapter faking a success 
       */
      expect(
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        await instance.interceptors.response['handlers'][0].rejected({
          response: { status: 401 },
          config: {
            inRetry: false,
            url: 'http://test',
            method: 'get',
            adapter: (): unknown => {
              return Promise.resolve({
                data: {},
                status: 200,
                statusText: 'SecondAttemptIsOK',
                headers: {},
                config: {},
                request: {},
              });
            },
          } as AxiosRequestConfig,
        }),
      ).toStrictEqual({
        config: {},
        data: {},
        headers: new AxiosHeaders({}),
        request: {},
        status: 200,
        statusText: 'SecondAttemptIsOK',
      });
    });
  });

  describe('createNonAuthApiInstance', () => {
    it('should create non auth api instance', () => {
      const props = {
        config: {
          baseURL: 'fakeURL',
          appURL: 'fakeUrl',
        },
      };
      const instance = createNonAuthApiInstance(props);

      expect(instance.get).toBeDefined();
      expect(instance.put).toBeDefined();
      expect(instance.post).toBeDefined();
      expect(instance.delete).toBeDefined();
      expect(instance.patch).toBeDefined();
    });
  });
  describe('makeCredentialsFromTokenResponse', () => {
    it('should successfully parse base64url token', () => {
      // Test with encoded token that contains the symbols '-' and '_',
      // which are part of base64url (used in jwt) but not base64
      const idTokenUrlSymbols =
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdF9oYXNoIjoiM1duLW1nMGJUWWhzblFBIiwic3ViIjoiMzg3OWMzNWUtZTMwNi01YzExMDM3NWE0NGEiLCJhdWQiOiIyb2w2M3YwZ2x1ZjlyNjdzbGF1djZ1aj8_Pj4-IiwiY29nbml0bzpncm91cHMiOlsiYWRtaW5pc3RyYXRvcnMiXSwiZW1haWxfdmVyaWZpZWQiOnRydWUsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjMxNjM0NDcsImlzcyI6Imh0dHBzOi8vY29nbml0by1pZHAuZXUtd2VzdC0xLmFtYXpvbmF3cy5jb20iLCJjb2duaXRvOnVzZXJuYW1lIjoibWF4LnZlcnN0YXBwZW4iLCJleHAiOjE2MzEyNzA0NywiaWF0IjoxNjMxMjM0NDcsImVtYWlsIjoibWF4LnZlcnN0YXBwZW5AZmFrZWVtYWlsLm5sIn0.Hivg6bb6t0xvPlHR7Im2HdVkRUxkh_zGMBo6yej_X8s';
      const jwt: GeoWebJWT = {
        access_token: 'test',
        refresh_token: 'test',
        id_token: idTokenUrlSymbols,
        expires_in: 3600,
      };
      const mockResponse = { data: jwt } as AxiosResponse;
      const credentials = makeCredentialsFromTokenResponse(mockResponse);
      expect(credentials).toBeTruthy();
    });

    it('should return Credentials based on an axios token response', () => {
      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());

      const jwt: GeoWebJWT = {
        access_token: 'test',
        refresh_token: 'test',
        id_token: ID_TOKEN,
        expires_in: 3600,
      };
      const resp: AxiosResponse = {
        data: { body: jwt },
      } as unknown as AxiosResponse;

      const credentials = makeCredentialsFromTokenResponse(resp);
      expect(credentials).toStrictEqual({
        expires_at: 1640011500,
        refresh_token: 'test',
        token: 'test',
        username: 'max.verstappen',
        roles: [GEOWEB_ROLE_USER],
        has_connection_issue: false,
      });
    });

    it('should not fail if no refresh_token is given', () => {
      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());

      const jwt: GeoWebJWT = {
        access_token: 'test',
        id_token: ID_TOKEN,
        expires_in: 3600,
      };
      const resp: AxiosResponse = {
        data: { body: jwt },
      } as unknown as AxiosResponse;

      const credentials = makeCredentialsFromTokenResponse(resp);
      expect(credentials).toStrictEqual({
        expires_at: 1640011500,
        refresh_token: '',
        token: 'test',
        username: 'max.verstappen',
        roles: [GEOWEB_ROLE_USER],
        has_connection_issue: false,
      });
    });

    it('should throw and error if no access_token is given', () => {
      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());

      const jwt: GeoWebJWT = {
        refresh_token: 'test',
        id_token: ID_TOKEN,
        expires_in: 3600,
      } as unknown as GeoWebJWT;

      const resp: AxiosResponse = {
        data: { body: jwt },
      } as unknown as AxiosResponse;

      expect(() => {
        makeCredentialsFromTokenResponse(resp);
      }).toThrow('Login failed');
    });

    it('should not fail if no expires_in is given', () => {
      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());

      const jwt: GeoWebJWT = {
        access_token: 'test',
        id_token: ID_TOKEN,
      } as unknown as GeoWebJWT;
      const resp: AxiosResponse = {
        data: { body: jwt },
      } as unknown as AxiosResponse;

      const credentials = makeCredentialsFromTokenResponse(resp);
      expect(credentials).toStrictEqual({
        expires_at: 1640011500,
        refresh_token: '',
        token: 'test',
        username: 'max.verstappen',
        roles: [GEOWEB_ROLE_USER],
        has_connection_issue: false,
      });
    });

    it('should return user roles based on an axios token response', () => {
      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());

      const authConfig = {
        GW_AUTH_ROLE_CLAIM_NAME: 'cognito:groups', // claim name which contains the user's security groups
        GW_AUTH_ROLE_CLAIM_VALUE_PRESETS_ADMIN: 'administrators', // required security group for preset-admin role
      };
      const jwt: GeoWebJWT = {
        access_token: 'test',
        refresh_token: 'test',
        id_token: ID_TOKEN, // this token contains the cognito group 'administrators'
        expires_in: 3600,
      };
      const resp: AxiosResponse = {
        data: { body: jwt },
      } as unknown as AxiosResponse;

      const credentials = makeCredentialsFromTokenResponse(resp, authConfig);
      expect(credentials).toStrictEqual({
        expires_at: 1640011500,
        refresh_token: 'test',
        token: 'test',
        username: 'max.verstappen',
        roles: [GEOWEB_ROLE_USER, GEOWEB_ROLE_PRESETS_ADMIN],
        has_connection_issue: false,
      });
    });
  });

  describe('refreshAccessTokenAndSetAuthContext', () => {
    it('should call onSetAuth with has_connection_issue=true when token service is unreachable.', async () => {
      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());
      const auth: Credentials = {
        expires_at: 1640011500,
        refresh_token: '',
        token: 'test',
        username: 'max.verstappen',
        roles: [GEOWEB_ROLE_USER],
        has_connection_issue: true,
      };

      const authConfig: ApiUrls = {
        authTokenURL: '',
        authClientId: '',
        appURL: '',
      };
      const onSetAuth = jest.fn();

      const spyOnRefreshTokenPost = jest.fn().mockImplementationOnce(() => {
        throw new Error('error');
      });

      jest.spyOn(axios, 'create').mockImplementationOnce(() => {
        return {
          post: spyOnRefreshTokenPost,
        } as unknown as AxiosInstance;
      });

      await refreshAccessTokenAndSetAuthContext({
        auth,
        onSetAuth,
        config: authConfig,
      });

      expect(onSetAuth).toHaveBeenCalledTimes(1);
      expect(onSetAuth).toHaveBeenCalledWith({
        expires_at: 1640011500,
        has_connection_issue: true,
        refresh_token: '',
        token: 'test',
        username: 'max.verstappen',
        roles: [GEOWEB_ROLE_USER],
      });
    });

    it('should keep the refresh token in auth context when new refresh token is not provided', async () => {
      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());
      const auth: Credentials = {
        expires_at: 1640011500,
        refresh_token: 'refreshTokenInAuthContext',
        token: 'test',
        username: 'max.verstappen',
        roles: [GEOWEB_ROLE_USER],
        has_connection_issue: false,
      };

      const authConfig: ApiUrls = {
        authTokenURL: '',
        authClientId: '',
        appURL: '',
      };
      const onSetAuth = jest.fn();

      const spyOnRefreshTokenPost = jest.fn().mockImplementationOnce(() => {
        return Promise.resolve({
          data: {
            access_token: 'ok',
            expires_in: 3600,
            id_token: ID_TOKEN,
          },
        });
      });

      jest.spyOn(axios, 'create').mockImplementationOnce(() => {
        return {
          post: spyOnRefreshTokenPost,
        } as unknown as AxiosInstance;
      });

      await refreshAccessTokenAndSetAuthContext({
        auth,
        onSetAuth,
        config: authConfig,
      });

      expect(onSetAuth).toHaveBeenCalledTimes(1);
      expect(onSetAuth).toHaveBeenCalledWith({
        expires_at: 1640011500,
        has_connection_issue: false,
        refresh_token: 'refreshTokenInAuthContext',
        token: 'ok',
        username: 'max.verstappen',
        roles: [GEOWEB_ROLE_USER],
      });
    });

    it('should keep the roles in auth context when role config is not provided', async () => {
      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());
      const auth: Credentials = {
        expires_at: 1640011500,
        refresh_token: 'access_token',
        token: 'refresh_token',
        username: 'max.verstappen',
        roles: [GEOWEB_ROLE_USER, GEOWEB_ROLE_PRESETS_ADMIN],
        has_connection_issue: false,
      };

      const authConfig: ApiUrls = {
        authTokenURL: '',
        authClientId: '',
        appURL: '',
      };
      const onSetAuth = jest.fn();

      const spyOnRefreshTokenPost = jest.fn().mockImplementationOnce(() => {
        return Promise.resolve({
          data: {
            access_token: 'new_access_token',
            refresh_token: 'new_refresh_token',
            expires_in: 3600,
            id_token: ID_TOKEN,
          },
        });
      });

      jest.spyOn(axios, 'create').mockImplementationOnce(() => {
        return {
          post: spyOnRefreshTokenPost,
        } as unknown as AxiosInstance;
      });

      await refreshAccessTokenAndSetAuthContext({
        auth,
        onSetAuth,
        config: authConfig,
        configURLS: undefined,
      });

      expect(onSetAuth).toHaveBeenCalledTimes(1);
      expect(onSetAuth).toHaveBeenCalledWith({
        expires_at: 1640011500,
        has_connection_issue: false,
        refresh_token: 'new_refresh_token',
        token: 'new_access_token',
        username: 'max.verstappen',
        roles: [GEOWEB_ROLE_USER, GEOWEB_ROLE_PRESETS_ADMIN],
      });
    });
  });

  describe('REFRESH_TOKEN_WHEN_PCT_EXPIRED', () => {
    it('should have a value of 75', () => {
      expect(REFRESH_TOKEN_WHEN_PCT_EXPIRED).toBe(75); // Just to make sure that the value has been restored to 75.
    });
  });

  describe('getCurrentTimeInSeconds', () => {
    it('should provide the current time in seconds', () => {
      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());

      expect(getCurrentTimeInSeconds()).toBe(1640008800);
    });
  });

  describe('groupsToRoles', () => {
    it('should return default user role if groups is undefined', () => {
      const result = groupsToRoles(undefined);
      expect(result).toEqual([GEOWEB_ROLE_USER]);
    });

    it('should return default user role if groups is an empty array', () => {
      const result = groupsToRoles([]);
      expect(result).toEqual([GEOWEB_ROLE_USER]);
    });

    it('should return default user role and preset-admin role if required group for preset-admin matches required group', () => {
      const authConfig = {
        GW_AUTH_ROLE_CLAIM_VALUE_PRESETS_ADMIN: 'presetsAdminsGroup',
      };
      const groups = ['presetsAdminsGroup', 'allUsersGroup'];
      const result = groupsToRoles(groups, authConfig);
      expect(result).toEqual([GEOWEB_ROLE_USER, GEOWEB_ROLE_PRESETS_ADMIN]);
    });

    it('should return only default user role if required group for preset-admin does not match any group', () => {
      const authConfig = {
        GW_AUTH_ROLE_CLAIM_VALUE_PRESETS_ADMIN: 'presetsAdminsGroup',
      };
      const groups = ['allUsersGroup, AdminsGroup'];
      const result = groupsToRoles(groups, authConfig);
      expect(result).toEqual([GEOWEB_ROLE_USER]);
    });
  });
});
