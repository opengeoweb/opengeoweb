/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';

import { UserMenuItemTheme, UserMenu } from '@opengeoweb/shared';

import { Grid2 as Grid, MenuItem } from '@mui/material';
import type { StoryObj } from '@storybook/react';
import { UserMenuRolesConnect } from './UserMenuRolesConnect';
import { DemoWrapperConnect } from '../Providers';
import {
  GEOWEB_ROLE_PRESETS_ADMIN,
  GEOWEB_ROLE_USER,
} from '../ApiContext/utils';
import { Role } from '../ApiContext/types';
import { createAuthenticationTestStore } from '../../utils/utils';

type Story = StoryObj<typeof UserMenuRolesConnect>;

export default {
  title: 'components/UserMenuRoles',
};

const Demo = (): React.ReactElement => {
  const initials = 't.t';
  const userName = 'testingtest';
  const userRoles = [GEOWEB_ROLE_PRESETS_ADMIN, GEOWEB_ROLE_USER];

  const [isAdmin, setIsAdmin] = React.useState<boolean>(false);
  const onChangeRole = (role: Role): void => {
    setIsAdmin(role.name === GEOWEB_ROLE_PRESETS_ADMIN.name);
  };

  return (
    <div>
      <UserMenu isOpen initials={initials} isAdmin={isAdmin}>
        <MenuItem selected divider>
          <Grid container>
            <Grid>{userName}</Grid>
          </Grid>
        </MenuItem>
        <MenuItem divider>
          <UserMenuRolesConnect
            roles={userRoles}
            temporaryOnChangeRoleForDemo={onChangeRole}
          />
        </MenuItem>
        <UserMenuItemTheme />
      </UserMenu>
    </div>
  );
};

export const AuthenticationDemo: Story = {
  render: () => (
    <DemoWrapperConnect store={createAuthenticationTestStore()}>
      <Demo />
    </DemoWrapperConnect>
  ),
};
