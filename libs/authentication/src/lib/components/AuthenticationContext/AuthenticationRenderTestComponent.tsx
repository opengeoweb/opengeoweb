/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { AuthI18nProvider } from '../Providers/Providers';
import { Credentials } from '../ApiContext/types';

interface AuthenticationRenderTestComponentProps {
  auth: Credentials | null;
  newAuth: Credentials;
  onSetAuth: (auth: Credentials) => void;
}
let numRendersAuthenticationRenderTestComponent = 0;

export const resetNumRenders = (): void => {
  numRendersAuthenticationRenderTestComponent = 0;
};

const AuthenticationRenderTestComponent: React.FC<
  AuthenticationRenderTestComponentProps
> = ({ auth, newAuth, onSetAuth }: AuthenticationRenderTestComponentProps) => {
  numRendersAuthenticationRenderTestComponent += 1;
  React.useEffect(() => {
    onSetAuth(newAuth);
  }, [newAuth, onSetAuth]);
  return (
    <AuthI18nProvider>
      <div>
        <ul data-testid="auth-render-test-list">
          <li data-testid="auth-numrenders">
            {numRendersAuthenticationRenderTestComponent}
          </li>
          <li data-testid="auth-username">{auth && auth.username}</li>
          <li data-testid="auth-token">{auth && auth.token}</li>
          <li data-testid="auth-refresh_token">{auth && auth.refresh_token}</li>
          <li data-testid="auth-expires_at">{auth && auth.expires_at}</li>
          <li data-testid="auth-has_connection_issue">
            {auth && auth.has_connection_issue}
          </li>
        </ul>
      </div>
    </AuthI18nProvider>
  );
};

export default AuthenticationRenderTestComponent;
