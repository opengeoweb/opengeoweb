/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';

import {
  AuthenticationContext,
  AuthenticationProvider,
} from './AuthenticationContext';
import AuthenticationRenderTestComponent, {
  resetNumRenders,
} from './AuthenticationRenderTestComponent';

describe('/components/AuthenticationRenderTestComponent.spec', () => {
  it('should not do render when token is updated', async () => {
    resetNumRenders();
    render(
      <AuthenticationProvider>
        <AuthenticationContext.Consumer>
          {(renderProps): React.ReactNode => {
            return (
              <AuthenticationRenderTestComponent
                auth={renderProps.auth!}
                onSetAuth={renderProps.onSetAuth}
                newAuth={{
                  username: 'username_test',
                  token: 'token_test',
                  refresh_token: 'refresh_token_test',
                  expires_at: 10000,
                  has_connection_issue: false,
                }}
              />
            );
          }}
        </AuthenticationContext.Consumer>
      </AuthenticationProvider>,
    );

    expect(screen.getByTestId('auth-render-test-list')).toBeTruthy();
    expect(screen.getByTestId('auth-numrenders').textContent).toBe('1');
    expect(screen.getByTestId('auth-token').textContent).toBe('');
    expect(screen.getByTestId('auth-refresh_token').textContent).toBe('');
    expect(screen.getByTestId('auth-expires_at').textContent).toBe('');
    expect(screen.getByTestId('auth-has_connection_issue').textContent).toBe(
      '',
    );
  });
  it('should render when has_connection_issue is updated', async () => {
    resetNumRenders();
    render(
      <AuthenticationProvider>
        <AuthenticationContext.Consumer>
          {(renderProps): React.ReactNode => {
            return (
              <AuthenticationRenderTestComponent
                auth={renderProps.auth!}
                onSetAuth={renderProps.onSetAuth}
                newAuth={{
                  username: 'username_test',
                  token: 'token_test',
                  refresh_token: 'refresh_token_test',
                  expires_at: 10000,
                  has_connection_issue: true,
                }}
              />
            );
          }}
        </AuthenticationContext.Consumer>
      </AuthenticationProvider>,
    );

    expect(screen.getByTestId('auth-render-test-list')).toBeTruthy();
    expect(screen.getByTestId('auth-numrenders').textContent).toBe('2');
    expect(screen.getByTestId('auth-token').textContent).toBe('');
    expect(screen.getByTestId('auth-refresh_token').textContent).toBe('');
    expect(screen.getByTestId('auth-expires_at').textContent).toBe('');
    expect(screen.getByTestId('auth-has_connection_issue').textContent).toBe(
      '',
    );
  });
});
