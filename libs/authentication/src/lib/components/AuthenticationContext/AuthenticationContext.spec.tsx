/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, renderHook, screen } from '@testing-library/react';

import axios, { AxiosInstance } from 'axios';
import {
  AuthenticationContext,
  AuthenticationProvider,
  getAuthConfig,
  replaceTemplateKeys,
  useAuthenticationContext,
  useAuthenticationDefaultProps,
} from './AuthenticationContext';
import { getSessionStorageProvider } from '../../utils/session';
import {
  GEOWEB_ROLE_PRESETS_ADMIN,
  GEOWEB_ROLE_USER,
  KEEP_ALIVE_POLLER_IN_SECONDS,
  MILLISECOND_TO_SECOND,
} from '../ApiContext/utils';

describe('/components/AuthenticationContext', () => {
  it('should return correct authUrls', () => {
    const testUrls = {
      GW_AUTH_LOGIN_URL: '/login/client_id={client_id}',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_TOKEN_URL: '/test124',
      GW_APP_URL: '/',
      GW_AUTH_CLIENT_ID: 'authclient123',
    };

    expect(getAuthConfig(testUrls)).toEqual({
      ...testUrls,
      GW_AUTH_LOGIN_URL: replaceTemplateKeys(
        testUrls.GW_AUTH_LOGIN_URL,
        testUrls.GW_AUTH_CLIENT_ID,
        testUrls.GW_APP_URL,
      ),
      GW_AUTH_LOGOUT_URL: replaceTemplateKeys(
        testUrls.GW_AUTH_LOGOUT_URL,
        testUrls.GW_AUTH_CLIENT_ID,
        testUrls.GW_APP_URL,
      ),
    });
  });

  it('should return correct authUrls with state param', () => {
    const testState = 'state123456';
    const spy = jest
      .spyOn(Storage.prototype, 'getItem')
      .mockReturnValue(testState);
    const testUrls = {
      GW_AUTH_LOGIN_URL: '/login/client_id={client_id}&state={state}',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_TOKEN_URL: '/test124',
      GW_APP_URL: '/',
      GW_AUTH_CLIENT_ID: 'authclient123',
    };

    expect(getAuthConfig(testUrls)).toEqual({
      ...testUrls,
      GW_AUTH_LOGIN_URL: replaceTemplateKeys(
        testUrls.GW_AUTH_LOGIN_URL,
        testUrls.GW_AUTH_CLIENT_ID,
        testUrls.GW_APP_URL,
        testState,
      ),
      GW_AUTH_LOGOUT_URL: replaceTemplateKeys(
        testUrls.GW_AUTH_LOGOUT_URL,
        testUrls.GW_AUTH_CLIENT_ID,
        testUrls.GW_APP_URL,
      ),
    });
    expect(spy).toHaveBeenCalledTimes(2);
    expect(spy).toHaveBeenCalledWith('oauth_state');
  });

  it('should use useAuthenticationDefaultProps', () => {
    const { result } = renderHook(() => useAuthenticationDefaultProps());

    expect(result.current.isLoggedIn).toBeFalsy();
    expect(result.current.auth).toBeNull();
  });

  it('should be able to render AuthenticationProvider', () => {
    const Test = 'Test';
    const TestComponent: React.FC = () => <h1>{Test}</h1>;
    render(
      <AuthenticationProvider>
        <TestComponent />
      </AuthenticationProvider>,
    );
    expect(screen.getByText('Test')).toBeTruthy();
  });

  it('should be able to render with props', () => {
    const props = {
      isLoggedIn: false,
      onLogin: jest.fn(),
      auth: {
        username: 'user',
        token: 'token',
        refresh_token: 'refresh_token',
      },
      onSetAuth: jest.fn(),
      sessionStorageProvider: getSessionStorageProvider(),
    };

    const configUrls = {
      GW_AUTH_LOGIN_URL: '/login',
      GW_AUTH_TOKEN_URL: '/token',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_CLIENT_ID: 'someID',
      GW_APP_URL: 'localhost',
    };

    let testProps;
    render(
      <AuthenticationProvider configURLS={configUrls} value={props}>
        <AuthenticationContext.Consumer>
          {(renderProps): React.ReactNode => {
            testProps = renderProps;
            return null;
          }}
        </AuthenticationContext.Consumer>
      </AuthenticationProvider>,
    );

    expect(testProps).toEqual({ ...props, authConfig: configUrls });
  });

  it('should be able to useAuthenticationContext ', () => {
    const TestComponent: React.FC = () => {
      const { isLoggedIn, onLogin } = useAuthenticationContext();
      const loggedIn = 'logged in';
      const notLoggedIn = 'not logged in';
      const logIn = 'log In';

      if (isLoggedIn) {
        return <p>{loggedIn}</p>;
      }
      return (
        <p>
          {notLoggedIn}
          <button type="button" onClick={(): void => onLogin(true)}>
            {logIn}
          </button>
        </p>
      );
    };
    render(
      <AuthenticationProvider>
        <TestComponent />
      </AuthenticationProvider>,
    );
    expect(screen.getByText('not logged in')).toBeTruthy();
    const button = screen.getByRole('button');

    fireEvent.click(button!);
    expect(screen.getByText('logged in')).toBeTruthy();
  });

  it('should be able to get currentRole and setCurrentRole', () => {
    const TestComponent: React.FC = () => {
      const { currentRole, setCurrentRole } = useAuthenticationContext();

      return (
        <p>
          <button
            type="button"
            onClick={(): void => setCurrentRole!(GEOWEB_ROLE_PRESETS_ADMIN)}
          >
            {currentRole!.name}
          </button>
        </p>
      );
    };
    render(
      <AuthenticationProvider>
        <TestComponent />
      </AuthenticationProvider>,
    );

    expect(screen.getByText(GEOWEB_ROLE_USER.name)).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByText(GEOWEB_ROLE_PRESETS_ADMIN.name)).toBeTruthy();
  });

  it('should call refreshAccessTokenAndSetAuthContext when current time is more than expires_at time', async () => {
    const configUrls = {
      GW_AUTH_LOGIN_URL: '/login',
      GW_AUTH_TOKEN_URL: '/token',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_CLIENT_ID: 'someID',
      GW_APP_URL: 'localhost',
    };

    const props = {
      isLoggedIn: false,
      onLogin: jest.fn(),
      auth: {
        username: 'user',
        token: 'token',
        refresh_token: 'refresh_token',
        expires_at: 1704067140, // 2023-12-31T23:59:00Z
      },
      onSetAuth: jest.fn(),
      sessionStorageProvider: getSessionStorageProvider(),
    };

    jest.useFakeTimers();
    jest.spyOn(global, 'setInterval');
    // Mock the current time to be one minute after the expires_at time
    jest
      .spyOn(Date, 'now')
      .mockReturnValue(new Date(`2024-01-01T00:00:00Z`).valueOf());

    const spyOnRefreshTokenPost = jest.fn().mockImplementationOnce(() => {
      return Promise.resolve({
        data: {
          access_token: 'access_token',
          expires_in: 3600,
          id_token: 'id_token',
        },
      });
    });

    jest.spyOn(axios, 'create').mockImplementationOnce(() => {
      return {
        post: spyOnRefreshTokenPost,
      } as unknown as AxiosInstance;
    });

    await render(
      <AuthenticationProvider configURLS={configUrls} value={props}>
        <div />
      </AuthenticationProvider>,
    );

    // Advance the timer by the interval + 100ms
    jest.advanceTimersByTime(
      KEEP_ALIVE_POLLER_IN_SECONDS / MILLISECOND_TO_SECOND + 100,
    );
    expect(setInterval).toHaveBeenCalledTimes(1);
    expect(spyOnRefreshTokenPost).toHaveBeenCalledTimes(1);

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should not call refreshAccessTokenAndSetAuthContext when current time is less than expires_at time', async () => {
    const configUrls = {
      GW_AUTH_LOGIN_URL: '/login',
      GW_AUTH_TOKEN_URL: '/token',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_CLIENT_ID: 'someID',
      GW_APP_URL: 'localhost',
    };

    const props = {
      isLoggedIn: false,
      onLogin: jest.fn(),
      auth: {
        username: 'user',
        token: 'token',
        refresh_token: 'refresh_token',
        expires_at: 1704067200, // 2024-01-01T00:00:00Z
      },
      onSetAuth: jest.fn(),
      sessionStorageProvider: getSessionStorageProvider(),
    };

    jest.useFakeTimers();
    jest.spyOn(global, 'setInterval');
    // Mock the current time to be one minute before the expires_at time
    jest
      .spyOn(Date, 'now')
      .mockReturnValue(new Date(`2023-12-31T23:59:00Z`).valueOf());

    const spyOnRefreshTokenPost = jest.fn().mockImplementationOnce(() => {
      return Promise.resolve({
        data: {
          access_token: 'access_token',
          expires_in: 3600,
          id_token: 'id_token',
        },
      });
    });

    jest.spyOn(axios, 'create').mockImplementationOnce(() => {
      return {
        post: spyOnRefreshTokenPost,
      } as unknown as AxiosInstance;
    });

    await render(
      <AuthenticationProvider configURLS={configUrls} value={props}>
        <div />
      </AuthenticationProvider>,
    );

    // Advance the timer by the interval + 100ms
    jest.advanceTimersByTime(
      KEEP_ALIVE_POLLER_IN_SECONDS / MILLISECOND_TO_SECOND + 100,
    );
    expect(setInterval).toHaveBeenCalledTimes(1);
    expect(spyOnRefreshTokenPost).not.toHaveBeenCalled();

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should not crash when no auth context is provided', async () => {
    const configUrls = {
      GW_AUTH_LOGIN_URL: '/login',
      GW_AUTH_TOKEN_URL: '/token',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_CLIENT_ID: 'someID',
      GW_APP_URL: 'localhost',
    };

    const props = {
      isLoggedIn: false,
      onLogin: jest.fn(),
      auth: null!,
      onSetAuth: jest.fn(),
      sessionStorageProvider: getSessionStorageProvider(),
    };

    jest
      .spyOn(Date, 'now')
      .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());
    jest.useFakeTimers();
    jest.spyOn(global, 'setInterval');
    await render(
      <AuthenticationProvider configURLS={configUrls} value={props}>
        <div />
      </AuthenticationProvider>,
    );

    expect(setInterval).toHaveBeenCalledTimes(1);
    // Advance the timer by the interval + 100ms
    jest.advanceTimersByTime(
      KEEP_ALIVE_POLLER_IN_SECONDS / MILLISECOND_TO_SECOND + 100,
    );
    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
