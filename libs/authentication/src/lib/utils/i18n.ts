/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import i18n from 'i18next';
import {
  UseTranslationResponse,
  initReactI18next,
  useTranslation,
} from 'react-i18next';
import { sharedTranslations, SHARED_NAMESPACE } from '@opengeoweb/shared';
import authTranslations from '../../../locales/authentication.json';
import apiTranslations from '../../../locales/api.json';

const API_NAMESPACE = 'api';
export const AUTH_NAMESPACE = 'auth';

export const initAuthTestI18n = (): void => {
  void i18n.use(initReactI18next).init({
    lng: 'en',
    ns: AUTH_NAMESPACE,
    interpolation: {
      escapeValue: false,
    },
    resources: {
      en: {
        [AUTH_NAMESPACE]: authTranslations.en,
        [SHARED_NAMESPACE]: sharedTranslations.en,
        [API_NAMESPACE]: apiTranslations.en,
      },
      fi: {
        [AUTH_NAMESPACE]: authTranslations.fi,
        [SHARED_NAMESPACE]: sharedTranslations.fi,
        [API_NAMESPACE]: apiTranslations.fi,
      },
      no: {
        [AUTH_NAMESPACE]: authTranslations.no,
        [SHARED_NAMESPACE]: sharedTranslations.no,
        [API_NAMESPACE]: apiTranslations.no,
      },
      nl: {
        [AUTH_NAMESPACE]: authTranslations.nl,
        [SHARED_NAMESPACE]: sharedTranslations.nl,
        [API_NAMESPACE]: apiTranslations.nl,
      },
    },
  });
};

const ns = [AUTH_NAMESPACE, API_NAMESPACE, SHARED_NAMESPACE];

export const useAuthenticationTranslation = (): UseTranslationResponse<
  typeof AUTH_NAMESPACE,
  typeof i18n
> => useTranslation(ns);

export const translateKeyOutsideComponents = (
  key: string,
  params: Record<string, string | number> | undefined = undefined,
): string => i18n.t(key, { ns, ...params });
