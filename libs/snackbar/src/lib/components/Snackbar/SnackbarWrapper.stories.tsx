/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Button, Typography } from '@mui/material';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import type { Meta, StoryObj } from '@storybook/react';
import { SnackbarWrapper } from './SnackbarWrapper';
import { SnackbarWrapperConnect } from './SnackbarWrapperConnect';
import { createMockStore, snackbarActions } from '../../store';
import { DemoWrapperConnect } from '../Providers/Providers';
import { SnackbarMessageType } from '../../store/types';

type Story = StoryObj<typeof SnackbarWrapper>;

const meta: Meta<typeof SnackbarWrapper> = {
  title: 'components/SnackbarWrapper',
  component: SnackbarWrapper,
  parameters: {
    docs: {
      description: {
        component: 'A wrapper component to show snackbar messages',
      },
    },
  },
};
export default meta;

export const Component: Story = {
  args: {
    isOpen: true,
    message: 'This is a snackbar notification',
    id: 'test-id',
  },
  render: (props) => (
    <div style={{ padding: 16, height: 120 }}>
      <SnackbarWrapper {...props} />{' '}
    </div>
  ),
  tags: ['!autodocs', '!dev'],
};

export const SnackbarDemo: Story = {
  render: () => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [open, setOpen] = React.useState(true);

    const handleToggle = (): void => {
      setOpen(!open);
    };

    const handleClose = (): void => {
      setOpen(false);
    };

    return (
      <div style={{ height: 120 }}>
        <SnackbarWrapper
          isOpen={open}
          onCloseSnackbar={handleClose}
          message="This is a snackbar notification"
          id="snackbar1"
        >
          <div>
            <Typography variant="body2"> We are snackbar children </Typography>
            <Button variant="contained" onClick={handleToggle}>
              Toggle snackbar
            </Button>
          </div>
        </SnackbarWrapper>
      </div>
    );
  },
  parameters: {
    docs: {
      description: {
        story: 'Press the submit button to trigger a snackbar',
      },
    },
  },
};

SnackbarDemo.storyName = 'SnackbarWrapper demo';

const store = createMockStore();
export const TwoSnackbarsDemo: Story = {
  render: () => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const dispatch = useDispatch();

    const handleToggle = (): void => {
      dispatch(
        snackbarActions.openSnackbar({
          message: 'I am the first snackbar',
          type: SnackbarMessageType.VERBATIM_MESSAGE,
        }),
      );
      setTimeout(() => {
        return dispatch(
          snackbarActions.openSnackbar({
            message: 'I am the second snackbar',
            type: SnackbarMessageType.VERBATIM_MESSAGE,
          }),
        );
      }, 2000);
    };

    return (
      <SnackbarWrapperConnect>
        <Button variant="contained" onClick={handleToggle}>
          Toggle two snackbars
        </Button>
      </SnackbarWrapperConnect>
    );
  },
  parameters: {
    docs: {
      description: {
        story:
          'Pressing the below button will trigger a snackbar to appear. A second snackbar will be triggered 2 sec later.',
      },
    },
  },
  decorators: [
    (Story): React.ReactElement => {
      return (
        <DemoWrapperConnect store={store}>
          <div style={{ height: 120 }}>
            <Story />
          </div>
        </DemoWrapperConnect>
      );
    },
  ],
};
