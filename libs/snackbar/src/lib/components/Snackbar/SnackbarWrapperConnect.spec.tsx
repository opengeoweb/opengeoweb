/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { SnackbarWrapperConnect } from './SnackbarWrapperConnect';
import { DemoWrapperConnect } from '../Providers/Providers';
import { SnackbarItem, SnackbarMessageType } from '../../store/types';
import { createMockStore } from '../../store';

describe('src/lib/components/Snackbar', () => {
  it('should render children even if snackbar closed', () => {
    const mockState = {
      snackbar: {
        entities: {},
        ids: [],
      },
    };

    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <SnackbarWrapperConnect>
          <div>I am a child</div>
        </SnackbarWrapperConnect>
      </DemoWrapperConnect>,
    );

    expect(screen.getByText('I am a child')).toBeTruthy();
    expect(screen.queryByTestId('snackbarComponent')).toBeFalsy();
  });

  it('should render children when snackbar opened', () => {
    const mockState = {
      snackbar: {
        entities: {
          snackbar1: {
            id: 'snackbar1',
            message: {
              message: 'snackbar message 1',
              type: SnackbarMessageType.VERBATIM_MESSAGE,
            },
          } as SnackbarItem,
        },
        ids: ['snackbar1'],
      },
    };
    const store = createMockStore(mockState);
    render(
      <DemoWrapperConnect store={store}>
        <SnackbarWrapperConnect>
          <div>I am a child</div>
        </SnackbarWrapperConnect>
      </DemoWrapperConnect>,
    );

    expect(screen.getByText('I am a child')).toBeTruthy();
    expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
  });

  it('should call close when clicked on the close button and clear snackbar from store', () => {
    const mockState = {
      snackbar: {
        entities: {
          snackbar1: {
            message: {
              message: 'snackbar message 1',
              type: SnackbarMessageType.VERBATIM_MESSAGE,
            },
            id: 'snackbar1',
          } as SnackbarItem,
        },
        ids: ['snackbar1'],
      },
    };
    const store = createMockStore(mockState);
    render(
      <DemoWrapperConnect store={store}>
        <SnackbarWrapperConnect>
          <div>I am a child</div>
        </SnackbarWrapperConnect>
      </DemoWrapperConnect>,
    );
    expect(store.getState().snackbar!.ids).toHaveLength(1);
    expect(screen.getByText('I am a child')).toBeTruthy();
    expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    fireEvent.click(screen.getByTestId('snackbarCloseButton'));

    expect(store.getState().snackbar!.ids).toHaveLength(0);
  });
});
