/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { configureStore } from '@reduxjs/toolkit';
import { waitFor } from '@testing-library/react';

import { snackbarActions, snackbarReducer } from './reducer';
import { hideTime, snackbarListener } from './listener';
import { initSnackbarI18n } from '../utils/i18n';
import { SnackbarMessage, SnackbarMessageType } from './types';

describe('store/snackbar/listener', () => {
  beforeAll(() => {
    initSnackbarI18n();
  });
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  describe('openSnackbarListener', () => {
    const snackbarMessage1: SnackbarMessage = {
      type: SnackbarMessageType.VERBATIM_MESSAGE,
      message: 'snackbar message 1',
    };
    const snackbarMessage2: SnackbarMessage = {
      type: SnackbarMessageType.TRANSLATABLE_MESSAGE,
      key: 'snackbar-message-2-key',
    };
    it('should close snackbar if one is currently open before opening the new one', () => {
      const store = configureStore({
        middleware: [snackbarListener.middleware],
        reducer: {
          snackbar: snackbarReducer,
        },
      });

      const mockDate = new Date('2022-01-09T14:00:00Z').valueOf();
      const mockDate2 = new Date('2024-01-09T14:00:00Z').valueOf();
      const snackbarId = `snackbar${mockDate}`;
      const snackbarId2 = `snackbar${mockDate2}`;
      jest
        .spyOn(Date, 'now')
        .mockReturnValueOnce(mockDate)
        .mockReturnValue(mockDate2);
      expect(store.getState().snackbar.entities).toEqual({});
      store.dispatch(snackbarActions.openSnackbar(snackbarMessage1));
      expect(store.getState().snackbar.entities[snackbarId]).toEqual({
        message: snackbarMessage1,
        id: snackbarId,
      });
      store.dispatch(snackbarActions.openSnackbar(snackbarMessage2));
      expect(store.getState().snackbar.entities[snackbarId]).toBeFalsy();
      expect(store.getState().snackbar.entities[snackbarId2]).toEqual({
        message: snackbarMessage2,
        id: snackbarId2,
      });
    });
    it('should hide snackbar after X seconds', async () => {
      const store = configureStore({
        middleware: [snackbarListener.middleware],
        reducer: {
          snackbar: snackbarReducer,
        },
      });

      const mockDate = new Date('2024-04-27T14:00:00Z').valueOf();
      const snackbarId = `snackbar${mockDate}`;
      jest.spyOn(Date, 'now').mockReturnValue(mockDate);
      expect(store.getState().snackbar.entities).toEqual({});
      store.dispatch(snackbarActions.openSnackbar(snackbarMessage1));
      expect(store.getState().snackbar.entities[snackbarId]).toEqual({
        message: snackbarMessage1,
        id: snackbarId,
      });
      jest.advanceTimersByTime(hideTime);
      await waitFor(() =>
        expect(store.getState().snackbar.entities[snackbarId]).toBeFalsy(),
      );
    });
  });
});
