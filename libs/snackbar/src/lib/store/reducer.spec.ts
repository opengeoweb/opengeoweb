/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { AnyAction } from '@reduxjs/toolkit';
import { snackbarReducer, initialState, snackbarActions } from './reducer';
import { SnackbarItem, SnackbarMessage, SnackbarMessageType } from './types';

describe('store/snackbar/reducer', () => {
  it('should return initial state if no state and action passed in', () => {
    expect(snackbarReducer(undefined, {} as AnyAction)).toEqual(initialState);
  });

  it('openSnackbar should not yet open a snackbar as that will be done in the listener', () => {
    const snackbarMessage = 'hi, I am a snackbar';
    const result = snackbarReducer(
      undefined,
      snackbarActions.openSnackbar({
        message: snackbarMessage,
        type: SnackbarMessageType.VERBATIM_MESSAGE,
      }),
    );
    expect(Object.keys(result.ids).length).toBe(0);
    expect(result.entities).toStrictEqual({});
  });

  it('triggerOpenSnackbarByListener should do nothing if no id passed', () => {
    const snackbarMessage = 'hi, I am a snackbar';
    const message: SnackbarMessage = {
      message: 'snackbar message 1',
      type: SnackbarMessageType.VERBATIM_MESSAGE,
    };
    const store = {
      entities: {
        snackbar1: {
          id: 'snackbar1',
          message,
        },
      },
      ids: ['snackbar1'],
    };
    const result = snackbarReducer(
      store,
      snackbarActions.triggerOpenSnackbarByListener({
        snackbarContent: {
          message: snackbarMessage,
        } as unknown as SnackbarItem,
      }),
    );

    expect(result).toBe(store);
  });

  it('triggerOpenSnackbarByListener should open a snackbar', () => {
    const snackbarid = 'snackbar1';
    const snackbarMessage: SnackbarMessage = {
      message: 'hi, I am a snackbar',
      type: SnackbarMessageType.VERBATIM_MESSAGE,
    };
    const result = snackbarReducer(
      undefined,
      snackbarActions.triggerOpenSnackbarByListener({
        snackbarContent: {
          message: snackbarMessage,
          id: snackbarid,
        },
      }),
    );
    expect(Object.keys(result.ids).length).toBe(1);
    expect(result.entities[snackbarid]!.message).toBe(snackbarMessage);
  });

  it('should empty the store if closeSnackbar is called', () => {
    const message: SnackbarMessage = {
      message: 'hi, I am a snackbar',
      type: SnackbarMessageType.VERBATIM_MESSAGE,
    };
    const result = snackbarReducer(
      {
        entities: {
          snackbar1: {
            id: 'snackbar1',
            message,
          },
        },
        ids: ['snackbar1'],
      },
      snackbarActions.closeSnackbar(),
    );
    expect(Object.keys(result.ids).length).toBe(0);
    expect(result.entities).toStrictEqual({});
  });
});
