/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { Theme } from '@mui/material';
import { darkTheme, lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import i18n from 'i18next';
import { I18nextProvider } from 'react-i18next';
import { initTimesliderI18n } from '../utils/i18n';

interface BaseProviderProps {
  children?: React.ReactNode;
  theme?: Theme;
}

interface TimesliderTranslationsWrapperProps {
  children?: React.ReactNode;
}

const TimesliderI18nProvider: React.FC<TimesliderTranslationsWrapperProps> = ({
  children,
}) => {
  initTimesliderI18n();
  return <I18nextProvider i18n={i18n}>{children}</I18nextProvider>;
};

export const DemoWrapper: React.FC<BaseProviderProps> = ({
  children,
  theme = 'light',
}) => {
  const themeToApply = theme === 'light' ? lightTheme : darkTheme;

  return (
    <TimesliderI18nProvider>
      <ThemeWrapper theme={themeToApply}>{children}</ThemeWrapper>
    </TimesliderI18nProvider>
  );
};
