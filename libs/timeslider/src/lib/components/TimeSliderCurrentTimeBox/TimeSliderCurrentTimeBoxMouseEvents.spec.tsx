/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { dateUtils } from '@opengeoweb/shared';
import {
  onMouseDown,
  onMouseMove,
} from './TimeSliderCurrentTimeBoxMouseEvents';

describe('src/components/TimeSlider/TimeSliderCurrentTimeBox/TimeSliderCurrentTimeBoxMouseEvents', () => {
  it('should set the cursor style to grab when hovering the timebox area', () => {
    const isTimeBoxArea = jest.fn(() => true);
    const setCursorStyle = jest.fn();
    onMouseMove(
      1,
      1,
      dateUtils.unix(dateUtils.utc()),
      1,
      5,
      false,
      isTimeBoxArea,
      setCursorStyle,
    );
    expect(setCursorStyle).toHaveBeenCalledWith('grab');
  });
  it('should set the cursor style to auto when area is false', () => {
    const isTimeBoxArea = jest.fn(() => false);
    const setCursorStyle = jest.fn();
    onMouseMove(
      1,
      1,
      dateUtils.unix(dateUtils.utc()),
      1,
      5,
      false,
      isTimeBoxArea,
      setCursorStyle,
    );
    expect(setCursorStyle).toHaveBeenCalledWith('auto');
  });
  it('should set the cursor style to grabbing when area and dragging is true', () => {
    const isTimeBoxArea = jest.fn(() => true);
    const setCursorStyle = jest.fn();
    const setMouseDownInTimeBox = jest.fn(() => true);
    onMouseDown(
      1,
      1,
      dateUtils.unix(dateUtils.utc()),
      1,
      5,
      isTimeBoxArea,
      setCursorStyle,
      setMouseDownInTimeBox,
    );
    expect(setCursorStyle).toHaveBeenCalledWith('grabbing');
    expect(setMouseDownInTimeBox).toHaveBeenCalledWith(true);
  });
  it('should not change the cursor style when not clicking on the time box area', () => {
    const isTimeBoxArea = jest.fn(() => false);
    const setCursorStyle = jest.fn();
    const setMouseDownInTimeBox = jest.fn(() => true);
    onMouseDown(
      1,
      1,
      dateUtils.unix(dateUtils.utc()),
      1,
      5,
      isTimeBoxArea,
      setCursorStyle,
      setMouseDownInTimeBox,
    );
    expect(setCursorStyle).not.toHaveBeenCalled();
    expect(setMouseDownInTimeBox).toHaveBeenCalledWith(true);
  });
});
