/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, screen } from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';
import i18n from 'i18next';
import userEvent from '@testing-library/user-event';
import { DemoWrapper } from '../../../Providers/Providers';
import {
  TimeSliderCurrentTimeBox,
  TimeSliderCurrentTimeBoxProps,
} from '../TimeSliderCurrentTimeBox';

describe('src/lib/components/TimeSliderCurrentTimeBox/CalendarButton', () => {
  it('should open a popper when clicked', async () => {
    const date = dateUtils.utc('01-01-2020');
    const props: TimeSliderCurrentTimeBoxProps = {
      secondsPerPx: 50,
      centerTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
      unfilteredSelectedTime: dateUtils.unix(dateUtils.set(date, { hours: 6 })),
      setUnfilteredSelectedTime: jest.fn(),
      onCalendarSelect: jest.fn(),
    };
    render(
      <DemoWrapper>
        <TimeSliderCurrentTimeBox {...props} />
      </DemoWrapper>,
    );
    const calendarButton = screen.getByRole('button', {
      name: i18n.t('timeslider-marker-placement'),
    });
    expect(calendarButton).toBeTruthy();
    expect(screen.queryByTestId('dateTimePickerPopOver')).toBeFalsy();

    // Click calendar toggle button to open popper
    const user = userEvent.setup();
    await user.click(calendarButton);
    const popover = await screen.findByTestId('dateTimePickerPopOver');
    expect(popover).toBeTruthy();
  });

  it('should close the open popper when re-clicked', async () => {
    const date = dateUtils.utc('01-01-2020');
    const props: TimeSliderCurrentTimeBoxProps = {
      secondsPerPx: 50,
      centerTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
      unfilteredSelectedTime: dateUtils.unix(dateUtils.set(date, { hours: 6 })),
      setUnfilteredSelectedTime: jest.fn(),
      onCalendarSelect: jest.fn(),
    };
    render(
      <DemoWrapper>
        <TimeSliderCurrentTimeBox {...props} />
      </DemoWrapper>,
    );
    const calendarButton = screen.getByRole('button', {
      name: i18n.t('timeslider-marker-placement'),
    });
    expect(calendarButton).toBeTruthy();

    // Click calendar toggle button to open popper
    const user = userEvent.setup();
    await user.click(calendarButton);
    const popover = await screen.findByTestId('dateTimePickerPopOver');
    expect(popover).toBeTruthy();

    // Close popper by clicking calendar toggle button once again
    await user.click(calendarButton);

    expect(screen.queryByTestId('dateTimePickerPopOver')).toBeFalsy();
  });

  it('should call "onSetCenterTime()" when "Enter" is hit on open popper', async () => {
    const date = dateUtils.utc('01-01-2020');
    const props: TimeSliderCurrentTimeBoxProps = {
      secondsPerPx: 50,
      centerTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
      unfilteredSelectedTime: dateUtils.unix(dateUtils.set(date, { hours: 6 })),
      setUnfilteredSelectedTime: jest.fn(),
      onSetCenterTime: jest.fn(),
      onCalendarSelect: jest.fn(),
    };
    render(
      <DemoWrapper>
        <TimeSliderCurrentTimeBox {...props} />
      </DemoWrapper>,
    );
    const calendarButton = screen.getByRole('button', {
      name: i18n.t('timeslider-marker-placement'),
    });
    expect(calendarButton).toBeTruthy();

    // Click calendar toggle button to open popper
    const user = userEvent.setup();
    await user.click(calendarButton);
    const popover = await screen.findByTestId('dateTimePickerPopOver');
    expect(popover).toBeTruthy();

    // Popper is shown with some date value on text field.
    // Hitting 'Enter' on keyboard should send that date value to be used elsewhere via calling 'onSetCenterTime()'
    await user.keyboard('{Enter}');
    expect(props.onSetCenterTime).toHaveBeenCalled();

    // Popper should disappear
    expect(screen.queryByTestId('dateTimePickerPopOver')).toBeFalsy();
  });
});
