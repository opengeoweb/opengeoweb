/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';

import { render, fireEvent, screen, act } from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';
import { TimeSliderLegend, TimeSliderLegendProps } from './TimeSliderLegend';
import {
  setTooltipPosition,
  pixelToTimestamp,
} from '../TimeSlider/timeSliderUtils';
import { DemoWrapper } from '../../Providers';

describe('src/lib/components/TimeSlider/TimeSliderLegend/TimeSliderLegend', () => {
  const date = dateUtils.utc('2020-01-01T00:00.000Z');
  const onStartTimeChangeMock = jest.fn();
  const props: TimeSliderLegendProps = {
    mapId: 'map_1',
    timeStep: 5,
    centerTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
    currentTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
    secondsPerPx: 5,
    unfilteredSelectedTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
    timeSliderWidth: 100,
    setUnfilteredSelectedTime: jest.fn(),
    onZoom: jest.fn(),
    onSetCenterTime: jest.fn(),
    onStartTimeChange: onStartTimeChangeMock,
    onEndTimeChange: jest.fn(),
    onSetTimeSliderSpan: jest.fn(),
    adjustSelectedTimeOnWheel: jest.fn(),
  };

  jest.useFakeTimers();

  it('should render the component with canvas', () => {
    render(
      <DemoWrapper>
        <TimeSliderLegend {...props} />
      </DemoWrapper>,
    );
    expect(screen.getByRole('presentation', { name: 'canvas' })).toBeTruthy();
  });

  it('should follow selected time when hovering mouse on the legend while not changing center time', () => {
    const extendProps = {
      ...props,
      isTimeSliderHoverOn: false,
    };

    const { baseElement } = render(
      <DemoWrapper>
        <TimeSliderLegend {...extendProps} />
      </DemoWrapper>,
    );
    expect(baseElement).toBeTruthy();

    const canvas = screen.getByRole('presentation', { name: 'canvas' });
    expect(canvas).toBeTruthy();

    canvas.focus();
    fireEvent.mouseMove(canvas);

    // Center time should stay intact
    expect(extendProps.onSetCenterTime).not.toHaveBeenCalled();
  });

  it('should not do any time selections at all when just moving mouse on the legend with no hover on', () => {
    const props = {
      mapId: 'map_1',
      scale: 5,
      timeStep: 5,
      centerTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
      currentTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
      selectedTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
      dataStartTime: dateUtils.unix(date),
      dataEndTime: dateUtils.unix(
        dateUtils.set(date, {
          hours: 23,
          minutes: 59,
          seconds: 59,
          milliseconds: 999,
        }),
      ),
      isTimeSliderHoverOn: false,
      secondsPerPx: 30,
      unfilteredSelectedTime: dateUtils.unix(
        dateUtils.set(date, { hours: 12 }),
      ),
      timeSliderWidth: 100,
      setUnfilteredSelectedTime: jest.fn(),
      onZoom: jest.fn(),
      onSetNewDate: jest.fn(),
      onSetCenterTime: jest.fn(),
      handleAnimationDragging: jest.fn(),
      localAnimationStartTime: dateUtils.unix(
        dateUtils.set(date, { hours: 12 }),
      ),
      localAnimationEndTime: dateUtils.unix(
        dateUtils.set(date, {
          hours: 23,
          minutes: 59,
          seconds: 59,
          milliseconds: 999,
        }),
      ),
      currentAnimationLength: 60,
      setCurrentAnimationLength: jest.fn(),
    };

    const { baseElement } = render(
      <DemoWrapper>
        <TimeSliderLegend {...props} />
      </DemoWrapper>,
    );

    expect(baseElement).toBeTruthy();

    const canvas = screen.getByRole('presentation', { name: 'canvas' })
      .firstChild as HTMLElement;
    expect(canvas).toBeTruthy();

    canvas.focus();
    fireEvent.mouseMove(canvas);

    expect(props.onSetNewDate).not.toHaveBeenCalled();
    expect(props.onSetCenterTime).not.toHaveBeenCalled();
  });

  it('should move legend only (changing center time, not selected time) when mouse is dragged far away from needle', async () => {
    const props = {
      mapId: 'map_1',
      scale: 5,
      timeStep: 5,
      centerTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
      currentTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
      selectedTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
      dataStartTime: dateUtils.unix(date),
      dataEndTime: dateUtils.unix(
        dateUtils.set(date, {
          hours: 23,
          minutes: 59,
          seconds: 59,
          milliseconds: 999,
        }),
      ),
      isTimeSliderHoverOn: false,
      secondsPerPx: 30,
      unfilteredSelectedTime: dateUtils.unix(
        dateUtils.set(date, { hours: 12 }),
      ),
      timeSliderWidth: 100,
      setUnfilteredSelectedTime: jest.fn(),
      onZoom: jest.fn(),
      onSetNewDate: jest.fn(),
      onSetCenterTime: jest.fn(),
      handleAnimationDragging: jest.fn(),
      localAnimationStartTime: dateUtils.unix(
        dateUtils.set(date, { hours: 12 }),
      ),
      localAnimationEndTime: dateUtils.unix(
        dateUtils.set(date, {
          hours: 23,
          minutes: 59,
          seconds: 59,
          milliseconds: 999,
        }),
      ),
      currentAnimationLength: 60,
      setCurrentAnimationLength: jest.fn(),
    };

    const { baseElement } = render(
      <DemoWrapper>
        <TimeSliderLegend {...props} />
      </DemoWrapper>,
    );
    expect(baseElement).toBeTruthy();

    const canvas = screen.getByRole('presentation', { name: 'canvas' })
      .firstChild as HTMLCanvasElement;
    expect(canvas).toBeTruthy();
    canvas.focus();

    fireEvent.mouseDown(canvas, {
      x: 0,
      y: 0,
    });

    fireEvent.mouseMove(canvas, {
      buttons: 1, // Dragging ~ mouse down while mouse moving
      x: 0,
      y: 0,
      width: canvas.width,
    });

    expect(props.onSetCenterTime).toHaveBeenCalled();
    expect(props.onSetNewDate).not.toHaveBeenCalled();
  });

  it('should move current time when mouse scrolled on legend', () => {
    const { baseElement } = render(
      <DemoWrapper>
        <TimeSliderLegend {...props} />
      </DemoWrapper>,
    );
    expect(baseElement).toBeTruthy();
    const canvas = screen.getByRole('presentation', { name: 'canvas' })
      .firstChild as HTMLElement;

    // deltaY values equivalent to one movement of a mouse scroll wheel
    // should move one timestep forward or backward.
    const deltaY1 = 100;
    const deltaY2 = -100;

    fireEvent.wheel(canvas, { deltaY: deltaY1 });
    expect(props.adjustSelectedTimeOnWheel).toHaveBeenCalled();

    jest.runOnlyPendingTimers();
    expect(props.adjustSelectedTimeOnWheel).toHaveBeenCalledTimes(1);
    expect(props.adjustSelectedTimeOnWheel).toHaveBeenCalledWith(
      expect.objectContaining({
        event: expect.any(Object),
        deltaY: deltaY1,
      }),
    );

    fireEvent.wheel(canvas, { deltaY: deltaY2 });
    expect(props.adjustSelectedTimeOnWheel).toHaveBeenCalledTimes(2);

    jest.runOnlyPendingTimers();
    expect(props.adjustSelectedTimeOnWheel).toHaveBeenCalledTimes(2);
    expect(props.adjustSelectedTimeOnWheel).toHaveBeenCalledWith(
      expect.objectContaining({
        event: expect.any(Object),
        deltaY: deltaY2,
      }),
    );
  });

  it('should display tooltips when dragging start and end animation handles', async () => {
    render(
      <DemoWrapper>
        <TimeSliderLegend {...props} />
      </DemoWrapper>,
    );

    expect(screen.getByTestId('timeSliderLegend')).toBeInTheDocument();

    fireEvent.mouseDown(screen.getByTestId('timeSliderLegend'));
    fireEvent.mouseMove(screen.getByTestId('timeSliderLegend'), {
      clientX: 50,
    });
    fireEvent.mouseUp(screen.getByTestId('timeSliderLegend'));

    expect(screen.getByTestId('timeSliderLegend')).toBeInTheDocument();
  });

  it('updates the tooltip position when moved', () => {
    render(
      <DemoWrapper>
        <TimeSliderLegend {...props} />
      </DemoWrapper>,
    );

    const initialTooltipX = 10;
    const initialTooltipY = 20;
    const initialTooltipPosition = setTooltipPosition(
      initialTooltipX,
      initialTooltipY,
    );

    fireEvent.click(screen.getByTestId('timeSliderLegend'));

    // Updated tooltip position
    const updatedTooltipX = 30;
    const updatedTooltipY = 40;
    const updatedTooltipPosition = setTooltipPosition(
      updatedTooltipX,
      updatedTooltipY,
    );

    expect(updatedTooltipPosition).not.toEqual(initialTooltipPosition);
  });

  it('calculates animationStartTime using pixelToTimestamp', async () => {
    const testProps = {
      ...props,
      reduxAnimationStartTime: '2022-01-01T00:00:00Z',
      reduxAnimationEndTime: '2022-01-01T01:00:00Z',
    };

    render(
      <DemoWrapper>
        <TimeSliderLegend {...testProps} />
      </DemoWrapper>,
    );
    const initialX = 10;
    const movedX = 20;
    const deltaX = movedX - initialX;
    const { centerTime, timeSliderWidth, secondsPerPx } = props;

    const dataStartTime = dateUtils.unix(date);
    const initialTime = dataStartTime;

    // Calculate expected start time
    const calculatedTime = pixelToTimestamp(
      dataStartTime + deltaX,
      centerTime,
      timeSliderWidth,
      secondsPerPx,
    );

    expect(calculatedTime).not.toEqual(initialTime);
  });

  it('should move time when moving mouse while hover is toggled on', async () => {
    const props = {
      mapId: 'map_1',
      scale: 5,
      timeStep: 5,
      centerTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
      currentTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
      selectedTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
      dataStartTime: dateUtils.unix(date),
      dataEndTime: dateUtils.unix(
        dateUtils.set(date, {
          hours: 23,
          minutes: 59,
          seconds: 59,
          milliseconds: 999,
        }),
      ),
      isTimeSliderHoverOn: true,
      secondsPerPx: 30,
      unfilteredSelectedTime: dateUtils.unix(
        dateUtils.set(date, { hours: 12 }),
      ),
      timeSliderWidth: 100,
      setUnfilteredSelectedTime: jest.fn(),
      onZoom: jest.fn(),
      onSetCenterTime: jest.fn(),
      handleAnimationDragging: jest.fn(),
      localAnimationStartTime: dateUtils.unix(
        dateUtils.set(date, { hours: 12 }),
      ),
      localAnimationEndTime: dateUtils.unix(
        dateUtils.set(date, {
          hours: 23,
          minutes: 59,
          seconds: 59,
          milliseconds: 999,
        }),
      ),
      currentAnimationLength: 60,
      setCurrentAnimationLength: jest.fn(),
    };

    const { baseElement } = render(
      <DemoWrapper>
        <TimeSliderLegend {...props} />
      </DemoWrapper>,
    );

    expect(baseElement).toBeTruthy();
    const mouseMoveEvent = new MouseEvent('mousemove', {
      clientX: 10,
    });
    Object.defineProperty(mouseMoveEvent, 'movementX', { value: 5 });

    fireEvent.mouseOver(screen.getByTestId('customCanvasMouseOverContainer'));
    fireEvent(document, mouseMoveEvent);

    jest.runOnlyPendingTimers();
    expect(props.setUnfilteredSelectedTime).toHaveBeenCalled();
  });

  it('should call updateTimeSpan, when ctrl / command is pressed and wheel event is fired on top of timeslider', async () => {
    const extendProps = {
      ...props,
      selectedTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
      dataStartTime: dateUtils.unix(dateUtils.set(date, { hours: 11 })),
      dataEndTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
      unfilteredSelectedTime: dateUtils.unix(
        dateUtils.set(date, { hours: 12 }),
      ),
    };

    const { baseElement } = render(
      <DemoWrapper>
        <TimeSliderLegend {...extendProps} />
      </DemoWrapper>,
    );

    expect(baseElement).toBeTruthy();
    const canvas = screen.getByRole('presentation', { name: 'canvas' })
      .firstChild as HTMLElement;
    fireEvent.mouseOver(canvas);
    fireEvent.wheel(canvas, { deltaY: 100, ctrlKey: true });
    act(() => {
      jest.runOnlyPendingTimers();
    });
    expect(props.onSetTimeSliderSpan).toHaveBeenCalled();

    jest.useRealTimers();
  });
});
