/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { dateUtils, CanvasComponent } from '@opengeoweb/shared';
import React from 'react';
import { render, renderHook, waitFor, screen } from '@testing-library/react';
import {
  AUTO_MOVE_AREA_PADDING,
  getAutoMoveAreaWidth,
  getNewCenterOfFixedPointZoom,
  minutesToDescribedDuration,
  moveRelativeToTimePoint,
  timeBoxGeom,
  pixelToTimestamp,
  scalingCoefficient,
  TimeInMinutes,
  timestampToPixel,
  timestampToPixelEdges,
  useCanvasTarget,
  calculateMouseTimeAndCheckBounds,
  Scale,
  getSpeedDelay,
  defaultDelay,
} from './timeSliderUtils';

describe('src/components/TimeSlider/timeSliderUtils', () => {
  describe('scalingCoefficient', () => {
    const timeStart = dateUtils.unix(dateUtils.utc('2020-07-01T10:00:00Z'));
    const timeEnd = dateUtils.unix(dateUtils.utc('2020-07-01T10:10:00Z'));
    it('should return the proper time bounds', () => {
      expect(scalingCoefficient(timeEnd, timeStart, 600)).toBe(1);
      expect(scalingCoefficient(timeEnd, timeStart, 30)).toBe(0.05);
    });
  });
  describe('timestampToPixelEdges', () => {
    const timeStamp = dateUtils.unix(dateUtils.utc('2020-07-01T10:05:00Z'));
    const timeStart = dateUtils.unix(dateUtils.utc('2020-07-01T10:00:00Z'));
    const timeEnd = dateUtils.unix(dateUtils.utc('2020-07-01T10:10:00Z'));
    it('should return the proper time bounds', () => {
      expect(timestampToPixelEdges(timeStamp, timeStart, timeEnd, 600)).toBe(
        300,
      );
      expect(
        timestampToPixelEdges(
          dateUtils.unix(dateUtils.utc('2020-07-01T10:02:00Z')),
          timeStart,
          timeEnd,
          30,
        ),
      ).toBe(6);
    });
  });

  describe('timestampToPixel', () => {
    const centerTime = dateUtils.unix(dateUtils.utc('2020-07-01T10:00:00Z'));
    it('should return the proper pixel position', () => {
      expect(
        timestampToPixel(
          dateUtils.unix(dateUtils.utc('2020-07-01T09:50:00Z')),
          centerTime,
          1000,
          10,
        ),
      ).toBe(440);
      expect(
        timestampToPixel(
          dateUtils.unix(dateUtils.utc('2020-07-01T10:15:00Z')),
          centerTime,
          1000,
          10,
        ),
      ).toBe(590);
    });
  });

  describe('pixelToTimestamp', () => {
    const centerTime = dateUtils.unix(dateUtils.utc('2020-07-01T10:00:00Z'));
    it('should return the proper timestamp', () => {
      expect(pixelToTimestamp(620, centerTime, 1000, 10)).toBe(
        dateUtils.unix(dateUtils.utc('2020-07-01T10:20:00Z')),
      );
      expect(pixelToTimestamp(560, centerTime, 1000, 10)).toBe(
        dateUtils.unix(dateUtils.utc('2020-07-01T10:10:00Z')),
      );
    });
  });

  // Test inverse with 10 random numbers
  const centerTime = dateUtils.unix(dateUtils.utc('2020-07-01T10:00:00Z'));
  const testPixelWidths = new Array(10)
    .fill(0)
    // eslint-disable-next-line no-unused-vars
    .map((_) => Math.random() * 1000);
  describe.each(testPixelWidths)(
    'timestampToPixel(pixelToTimestamp(%d, ...), ...)',
    (t) => {
      it(`should return ${t}`, () => {
        expect(
          timestampToPixel(
            pixelToTimestamp(t, centerTime, 1000, 10),
            centerTime,
            1000,
            10,
          ),
        ).toBeCloseTo(t);
      });
    },
  );

  describe('getNewCenterOfFixedPointZoom', () => {
    it('should return new center time', () => {
      const secondsPerPxStart = 2;
      const secondsPerPxEnd = 1;
      const fixedTimePoint = dateUtils.unix(
        dateUtils.utc('2021-01-01 18:00:00'),
      );
      const oldCenterTime = dateUtils.unix(
        dateUtils.utc('2021-01-01 12:00:00'),
      );
      const newCenterTime = getNewCenterOfFixedPointZoom(
        fixedTimePoint,
        secondsPerPxStart,
        secondsPerPxEnd,
        oldCenterTime,
      );
      expect(newCenterTime).toEqual(
        dateUtils.unix(dateUtils.utc('2021-01-01 15:00:00')),
      );
    });

    it('should keep the fixed point at same pixel location', () => {
      const canvasWidth = 1000;
      const fixedPointPx = 750;
      const secondsPerPxStart = 10;
      const secondsPerPxEnd = 17;
      const oldCenterTime = dateUtils.unix(
        dateUtils.utc('2021-01-01 12:00:00'),
      );

      const fixedPointUnix = pixelToTimestamp(
        fixedPointPx,
        oldCenterTime,
        canvasWidth,
        secondsPerPxStart,
      );
      const newCenterTime = getNewCenterOfFixedPointZoom(
        fixedPointUnix,
        secondsPerPxStart,
        secondsPerPxEnd,
        oldCenterTime,
      );
      const zoomedFixedPointPx = timestampToPixel(
        fixedPointUnix,
        newCenterTime,
        canvasWidth,
        secondsPerPxEnd,
      );
      expect(zoomedFixedPointPx).toEqual(fixedPointPx);
    });
  });

  describe('custom hook "useCanvasTarget"', () => {
    it('should not be a hooked component if ref does not exist on component', () => {
      const { result } = renderHook(() => useCanvasTarget('mousedown'));
      expect(result.current).toEqual([false, { current: null }]);
    });

    it('should use "useCallback" hook', () => {
      const useCallbackSpy = jest
        .spyOn(React, 'useCallback')
        .mockReturnValueOnce(expect.any(Function));

      const { result } = renderHook(() => useCanvasTarget('mousedown'));
      expect(useCallbackSpy).toHaveBeenCalledTimes(1);
      expect(result.current).toEqual([false, { current: null }]);
    });

    it('should use "useRef" hook', () => {
      const useRefSpy = jest
        .spyOn(React, 'useRef')
        .mockReturnValueOnce({ current: null });

      renderHook(() => useCanvasTarget('mousedown'));

      expect(useRefSpy).toHaveBeenCalledTimes(1);
      expect(useRefSpy).toHaveBeenCalledWith(null);
    });

    it('should be called with "mousedown" event when adding document listener', () => {
      jest.spyOn(document, 'addEventListener');

      renderHook(() => useCanvasTarget('mousedown'));
      expect(document.addEventListener).toHaveBeenCalled();
      expect(document.addEventListener).toHaveBeenCalledWith(
        'mousedown',
        expect.any(Function),
      );
    });

    it('should not remove window listener on rerender', async () => {
      jest.spyOn(window, 'removeEventListener');
      const { rerender } = renderHook(() => useCanvasTarget('mousedown'));
      rerender();
      await waitFor(() =>
        expect(window.removeEventListener).not.toHaveBeenCalled(),
      );
    });

    it('should be able to render canvas component', () => {
      let testRef: React.RefObject<CanvasComponent>;

      const TestComponent: React.FC = () => {
        const [isAllowedCanvasNodePointed, nodeRef] =
          useCanvasTarget('mousedown');
        testRef = nodeRef;

        return isAllowedCanvasNodePointed && nodeRef && nodeRef.current ? (
          <CanvasComponent ref={nodeRef}>Component with ref</CanvasComponent>
        ) : (
          <CanvasComponent>Component with NO ref</CanvasComponent>
        );
      };

      render(<TestComponent />);
      const canvas = screen.getByRole('presentation', { name: 'canvas' });
      expect(canvas).toBeDefined();
      expect(testRef!.current).toBeNull();

      render(<TestComponent />);
      expect(screen.queryByText('Component with ref')).toBeFalsy();
    });
  });

  describe('moveRelativeToTimePoint', () => {
    it('should move the given time point to given position', () => {
      const timePoint = dateUtils.unix(dateUtils.utc('2021-01-01 18:00:00'));
      const secondsPerPx = 10;
      const canvasWidth = 1200;
      const timePointPx = 1200 / 4;
      const newCenterTime = moveRelativeToTimePoint(
        timePoint,
        timePointPx,
        secondsPerPx,
        canvasWidth,
      );
      expect(newCenterTime).toEqual(
        dateUtils.unix(dateUtils.utc('2021-01-01 18:50:00')),
      );
      expect(
        timestampToPixel(timePoint, newCenterTime, canvasWidth, secondsPerPx),
      ).toBeCloseTo(timePointPx);
    });
  });

  describe('getAutoMoveAreaWidth', () => {
    const smallWidth =
      timeBoxGeom.smallWidth / 2 +
      timeBoxGeom.iconWidth / 2 +
      AUTO_MOVE_AREA_PADDING;
    const largeWidth =
      timeBoxGeom.largeWidth / 2 +
      timeBoxGeom.iconWidth / 2 +
      AUTO_MOVE_AREA_PADDING;
    it('should return small width for a non-year scale', () => {
      expect(getAutoMoveAreaWidth(Scale.Day)).toEqual(smallWidth);
    });
    it('should return large width for year scale', () => {
      expect(getAutoMoveAreaWidth(Scale.Year)).toEqual(largeWidth);
    });
  });
});

describe('minutesToDescribedDuration', () => {
  it('should display time to format correctly', () => {
    expect(minutesToDescribedDuration(1)).toEqual('1m');
    expect(minutesToDescribedDuration(60)).toEqual('1h');
    expect(minutesToDescribedDuration(119)).toEqual('1h 59m');
    expect(minutesToDescribedDuration(TimeInMinutes.DAY)).toEqual('1d');
    expect(minutesToDescribedDuration(TimeInMinutes.DAY + 61)).toEqual('1d 1h');
    expect(minutesToDescribedDuration(TimeInMinutes.MONTH)).toEqual('1mo');
    expect(
      minutesToDescribedDuration(TimeInMinutes.MONTH * 2 + TimeInMinutes.DAY),
    ).toEqual('2mos 1d');
    expect(minutesToDescribedDuration(TimeInMinutes.YEAR)).toEqual('1yr');
    expect(
      minutesToDescribedDuration(
        TimeInMinutes.YEAR +
          TimeInMinutes.MONTH +
          TimeInMinutes.DAY +
          TimeInMinutes.HOUR +
          1,
      ),
    ).toEqual('1yr 1mo');
    expect(
      minutesToDescribedDuration(
        TimeInMinutes.YEAR * 2 + TimeInMinutes.MONTH * 2,
      ),
    ).toEqual('2yrs 2mos');
  });
});

describe('calculateMouseTimeAndCheckBounds', () => {
  it('sets tooltipPosition.current and returns a valid timestamp if dragging is within bounds', () => {
    const tooltipPosition = { current: 0 };

    const result = calculateMouseTimeAndCheckBounds(
      150, // x
      100, // startAnimationPosition
      300, // endAnimationPosition
      20, // DRAG_AREA_WIDTH
      50, // pixelsBetweenViewportAndTimesliderOnLeft
      200, // centerTime
      600, // canvasWidth
      2, // secondsPerPx
      tooltipPosition,
    );

    expect(tooltipPosition.current).toBeDefined();

    expect(result).toBeDefined();
  });

  it('returns a valid timestamp for a given pixel position', () => {
    const mousePosition = 150;
    const centerTime = 200;
    const canvasWidth = 600;
    const secondsPerPx = 2;

    const result = pixelToTimestamp(
      mousePosition,
      centerTime,
      canvasWidth,
      secondsPerPx,
    );

    expect(result).toBeDefined();
    expect(result).not.toBeNaN();
  });
});

describe('getSpeedDelay', () => {
  it('should return correct delay for the passed speedFactor', () => {
    expect(getSpeedDelay(0.1)).toEqual(defaultDelay / 0.1);
    expect(getSpeedDelay(0.2)).toEqual(defaultDelay / 0.2);
    expect(getSpeedDelay(0.5)).toEqual(defaultDelay / 0.5);
    expect(getSpeedDelay(1)).toEqual(defaultDelay / 1);
    expect(getSpeedDelay(2)).toEqual(defaultDelay / 2);
    expect(getSpeedDelay(4)).toEqual(defaultDelay / 4);
    expect(getSpeedDelay(8)).toEqual(defaultDelay / 8);
    expect(getSpeedDelay(16)).toEqual(defaultDelay / 16);
  });
});
