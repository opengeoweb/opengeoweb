/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';
import { DemoWrapper } from '../../Providers/Providers';
import { TimeSlider, TimeSliderProps } from './TimeSlider';
import { roundWithTimeStep } from './timeSliderUtils';
import { TimeSliderButtons } from '../TimeSliderButtons/TimeSliderButtons';
import { TimeSliderCurrentTimeBox } from '../TimeSliderCurrentTimeBox/TimeSliderCurrentTimeBox';
import { TimeSliderLegend } from '../TimeSliderLegend/TimeSliderLegend';

describe('src/components/TimeSlider/TimeSlider', () => {
  const defaultProps: TimeSliderProps = {
    dataEndTime: 0,
    dataStartTime: 0,
    selectedTime: 0,
    timeStep: 0,
    onSetCenterTime: (): void => {},
    onSetNewDate: (): void => {},
    buttons: undefined,
    timeBox: undefined,
    legend: undefined,
  };

  const buttonsTest = <TimeSliderButtons />;

  const timeBoxTest = (
    <TimeSliderCurrentTimeBox
      centerTime={0}
      secondsPerPx={0}
      {...defaultProps}
      unfilteredSelectedTime={0}
      setUnfilteredSelectedTime={() => {}}
      onCalendarSelect={() => {}}
    />
  );

  const legendTest = (
    <TimeSliderLegend
      mapId=""
      centerTime={0}
      timeSliderWidth={0}
      secondsPerPx={0}
      {...defaultProps}
      unfilteredSelectedTime={0}
      setUnfilteredSelectedTime={() => {}}
    />
  );

  it('should render the wrapped components', () => {
    render(
      <DemoWrapper>
        <TimeSlider
          {...defaultProps}
          buttons={buttonsTest}
          timeBox={timeBoxTest}
          legend={legendTest}
        />
      </DemoWrapper>,
    );
    expect(screen.getByTestId('timeSliderButtons')).toBeTruthy();
    expect(screen.getByTestId('timeSliderTimeBox')).toBeTruthy();
    expect(screen.getByTestId('timeSliderLegend')).toBeTruthy();
  });

  it('should hide the timeslider', () => {
    const props = {
      ...defaultProps,
      mapId: 'map_1',
      isVisible: false,
      onToggleTimeSlider: jest.fn(),
    };

    render(
      <DemoWrapper>
        <TimeSlider
          {...props}
          buttons={buttonsTest}
          timeBox={timeBoxTest}
          legend={legendTest}
        />
      </DemoWrapper>,
    );
    expect(screen.queryByTestId('timeSliderButtons')).toBeFalsy();
    expect(screen.queryByTestId('timeSliderTimeBox')).toBeFalsy();
    expect(screen.queryByTestId('timeSliderLegend')).toBeFalsy();

    expect(props.onToggleTimeSlider).not.toHaveBeenCalled();
    fireEvent.keyDown(document, {
      key: 't',
      code: 'KeyT',
    });
    expect(props.onToggleTimeSlider).toHaveBeenCalledTimes(1);
  });

  it('should be able to hide the timeslider with t key when map is active', async () => {
    const props = {
      ...defaultProps,
      mapId: 'map_1',
      onToggleTimeSlider: jest.fn(),
    };
    const { baseElement } = render(
      <DemoWrapper>
        <TimeSlider
          {...props}
          buttons={buttonsTest}
          timeBox={timeBoxTest}
          legend={legendTest}
        />
      </DemoWrapper>,
    );
    expect(screen.getByTestId('timeSlider')).toBeTruthy();
    expect(props.onToggleTimeSlider).not.toHaveBeenCalled();
    fireEvent.keyDown(baseElement, {
      key: 't',
      code: 'KeyT',
    });
    expect(props.onToggleTimeSlider).toHaveBeenCalledTimes(1);
  });

  it('should be able to show the timeslider with t key when map is active', async () => {
    const props = {
      ...defaultProps,
      mapId: 'map_1',
      onToggleTimeSlider: jest.fn(),
      isVisible: false,
    };
    const { baseElement } = render(
      <DemoWrapper>
        <TimeSlider
          {...props}
          buttons={buttonsTest}
          timeBox={timeBoxTest}
          legend={legendTest}
        />
      </DemoWrapper>,
    );
    expect(screen.queryByTestId('timeSlider')).toBeFalsy();
    expect(props.onToggleTimeSlider).not.toHaveBeenCalled();
    fireEvent.keyDown(baseElement, {
      key: 't',
      code: 'KeyT',
    });
    expect(props.onToggleTimeSlider).toHaveBeenCalledTimes(1);
  });
  it('timeslider cannot be removed and fetched back with t key when map is inactive', async () => {
    const props = { ...defaultProps, mapId: 'map_1', mapIsActive: false };
    const { baseElement } = render(
      <DemoWrapper>
        <TimeSlider
          {...props}
          buttons={buttonsTest}
          timeBox={timeBoxTest}
          legend={legendTest}
        />
      </DemoWrapper>,
    );
    expect(screen.getByTestId('timeSlider')).toBeTruthy();
    fireEvent.keyDown(baseElement, {
      key: 't',
      code: 'KeyT',
    });
    expect(screen.getByTestId('timeSlider')).toBeTruthy();
  });
  it('timeslider cannot be removed and fetched back with t key in an input or textarea field, but can be done elsewhere', async () => {
    const props2 = {
      ...defaultProps,
      mapId: 'map_1',
      onToggleTimeSlider: jest.fn(),
    };
    render(
      <DemoWrapper>
        <input data-testid="fakeTestField" />
        <textarea data-testid="fakeTestArea" />
        <div data-testid="testDiv" />
        <TimeSlider
          {...props2}
          buttons={buttonsTest}
          timeBox={timeBoxTest}
          legend={legendTest}
        />
      </DemoWrapper>,
    );
    expect(screen.getByTestId('timeSlider')).toBeTruthy();
    const inputField = screen.getByTestId('fakeTestField');
    expect(inputField).toBeTruthy();
    const testDiv = screen.getByTestId('testDiv');
    expect(testDiv).toBeTruthy();
    const textArea = screen.getByTestId('fakeTestArea');
    expect(textArea).toBeTruthy();

    expect(screen.getByTestId('timeSlider')).toBeTruthy();
    expect(props2.onToggleTimeSlider).not.toHaveBeenCalled();

    fireEvent.keyDown(inputField, {
      key: 't',
      code: 'KeyT',
    });
    expect(props2.onToggleTimeSlider).not.toHaveBeenCalled();

    fireEvent.keyDown(textArea, {
      key: 't',
      code: 'KeyT',
    });
    expect(props2.onToggleTimeSlider).not.toHaveBeenCalled();

    fireEvent.keyDown(testDiv, {
      key: 't',
      code: 'KeyT',
    });
    await waitFor(() =>
      expect(props2.onToggleTimeSlider).toHaveBeenCalledWith(false),
    );
  });

  describe('change time with home button', () => {
    const now = `${dateUtils.dateToString(
      dateUtils.utc(),
      dateUtils.DATE_FORMAT_YEAR,
    )}T14:00:00Z`;

    jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());

    const props: TimeSliderProps = {
      timeStep: 5,
      selectedTime: 0,
      dataStartTime: 0,
      dataEndTime: 0,
      onSetNewDate: jest.fn(),
      onSetCenterTime: jest.fn(),
      buttons: undefined,
      timeBox: undefined,
      legend: undefined,
    };

    const renderTimeSliderLegendAndClickHome = (
      props: TimeSliderProps,
    ): void => {
      const { baseElement } = render(
        <DemoWrapper>
          <TimeSlider
            {...props}
            buttons={buttonsTest}
            timeBox={timeBoxTest}
            legend={legendTest}
          />
        </DemoWrapper>,
      );
      expect(baseElement).toBeTruthy();
      const canvas = screen.queryAllByRole('presentation', {
        name: 'canvas',
      })[0].firstChild as HTMLElement;
      canvas.focus();

      fireEvent.keyDown(canvas, {
        key: 'Home',
        code: 'Home',
        keyCode: 36,
      });
    };

    it('should call onSetNewDate and onSetCenterTime with correct parameters when no data is available in the future and home button is pressed', () => {
      const props2: TimeSliderProps = {
        ...props,
        dataStartTime: dateUtils.unix(
          dateUtils.set(dateUtils.utc(), {
            hours: 0,
            minutes: 0,
            seconds: 0,
            milliseconds: 0,
          }),
        ),
        dataEndTime: dateUtils.unix(
          dateUtils.set(dateUtils.utc(), {
            hours: 1,
            minutes: 0,
            seconds: 0,
            milliseconds: 0,
          }),
        ),
      };

      renderTimeSliderLegendAndClickHome(props2);

      expect(props2.onSetNewDate).toHaveBeenCalledTimes(1);
      expect(props2.onSetNewDate).toHaveBeenCalledWith(
        dateUtils.dateToIsoString(dateUtils.fromUnix(props2.dataEndTime)),
      );
      expect(props2.onSetCenterTime).toHaveBeenCalledTimes(1);
      expect(props2.onSetCenterTime).toHaveBeenCalledWith(props2.dataEndTime);
    });

    it('should call onSetNewDate and onSetCenterTime with correct parameters when observation and forecast data are available and home button is pressed', () => {
      const props2: TimeSliderProps = {
        ...props,
        dataStartTime: dateUtils.unix(
          dateUtils.set(dateUtils.utc(), {
            hours: 0,
            minutes: 0,
            seconds: 0,
            milliseconds: 0,
          }),
        ),
        dataEndTime: dateUtils.unix(
          dateUtils.set(dateUtils.utc(), {
            hours: 23,
            minutes: 59,
            seconds: 59,
            milliseconds: 999,
          }),
        ),
      };

      renderTimeSliderLegendAndClickHome(props2);

      expect(props2.onSetNewDate).toHaveBeenCalledTimes(1);
      expect(props2.onSetNewDate).toHaveBeenCalledWith(
        dateUtils.dateToIsoString(
          dateUtils.fromUnix(
            roundWithTimeStep(dateUtils.unix(dateUtils.utc()), props2.timeStep),
          ),
        ),
      );
      expect(props2.onSetCenterTime).toHaveBeenCalledTimes(1);
      expect(props2.onSetCenterTime).toHaveBeenCalledWith(
        roundWithTimeStep(dateUtils.unix(dateUtils.utc()), props2.timeStep),
      );
    });

    it('should call onSetNewDate and onSetCenterTime with correct parameters when no data is available in the past and home button is pressed', () => {
      jest.spyOn(dateUtils, 'utc').mockReturnValue(new Date(now));

      const props2: TimeSliderProps = {
        ...props,
        dataStartTime: dateUtils.unix(
          dateUtils.set(dateUtils.utc(), {
            hours: 14,
            minutes: 0,
            seconds: 0,
            milliseconds: 0,
          }),
        ),
        dataEndTime: dateUtils.unix(
          dateUtils.set(dateUtils.utc(), {
            hours: 23,
            minutes: 59,
            seconds: 59,
            milliseconds: 999,
          }),
        ),
      };

      renderTimeSliderLegendAndClickHome(props2);

      expect(props2.onSetNewDate).toHaveBeenCalledTimes(1);
      expect(props2.onSetNewDate).toHaveBeenCalledWith(
        dateUtils.dateToIsoString(dateUtils.fromUnix(props2.dataStartTime)),
      );
      expect(props2.onSetCenterTime).toHaveBeenCalledTimes(1);
      expect(props.onSetCenterTime).toHaveBeenCalledWith(props2.dataStartTime);
    });

    it('should not call onSetNewDate and onSetCenterTime when map is inactive and home button is pressed', () => {
      const props2: TimeSliderProps = {
        ...props,
        dataStartTime: dateUtils.unix(
          dateUtils.set(dateUtils.utc(), {
            hours: 0,
            minutes: 0,
            seconds: 0,
            milliseconds: 0,
          }),
        ),
        dataEndTime: dateUtils.unix(
          dateUtils.set(dateUtils.utc(), {
            hours: 10,
            minutes: 0,
            seconds: 0,
            milliseconds: 0,
          }),
        ),
        mapIsActive: false,
      };

      renderTimeSliderLegendAndClickHome(props2);

      expect(props2.onSetNewDate).not.toHaveBeenCalled();
      expect(props2.onSetCenterTime).not.toHaveBeenCalled();
    });
  });
  describe('change time with arrow left or right', () => {
    beforeAll(() => {
      jest.useFakeTimers();
    });
    afterAll(() => {
      jest.clearAllTimers();
      jest.useRealTimers();
    });

    const props: TimeSliderProps = {
      ...defaultProps,
      timeStep: 5,
      selectedTime: dateUtils.unix(dateUtils.utc('2020-01-01T00:00:00.000Z')),
      dataStartTime: dateUtils.unix(dateUtils.utc('2010-01-01T00:00:00.000Z')),
      dataEndTime: dateUtils.unix(dateUtils.utc('2030-01-01T00:00:00.000Z')),
      onSetNewDate: jest.fn(),
      onSetCenterTime: jest.fn(),
    };

    function changeLegendAnimatiomValueWithKeyboard(
      arrowKeyDirection: 'left' | 'right',
      mapIsActive: boolean,
    ): void {
      render(
        <DemoWrapper>
          <TimeSlider
            {...props}
            mapIsActive={mapIsActive}
            buttons={buttonsTest}
            timeBox={timeBoxTest}
            legend={legendTest}
          />
        </DemoWrapper>,
      );
      const canvas = screen.queryAllByRole('presentation', {
        name: 'canvas',
      })[0].firstChild as HTMLElement;
      canvas.focus();
      fireEvent.keyDown(
        canvas,
        arrowKeyDirection === 'left'
          ? {
              ctrlKey: true,
              key: 'ArrowLeft',
              code: 'ArrowLeft',
              keyCode: 37,
            }
          : {
              ctrlKey: true,
              key: 'ArrowRight',
              code: 'ArrowRight',
              keyCode: 39,
            },
      );
      jest.runOnlyPendingTimers();
    }

    it('Legend slider value can be changed with an right arrow key and a ctrl key if map is active', async () => {
      const newTime = dateUtils.dateToIsoString(
        dateUtils.add(dateUtils.fromUnix(props.selectedTime), {
          minutes: props.timeStep,
        }),
      );
      changeLegendAnimatiomValueWithKeyboard('right', true);

      expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
      expect(props.onSetNewDate).toHaveBeenCalledWith(newTime);
    });
    it('rail slider value can not be changed with an right arrow key and a ctrl key if map is inactive', async () => {
      changeLegendAnimatiomValueWithKeyboard('right', false);

      expect(props.onSetNewDate).not.toHaveBeenCalled();
    });
    it('rail slider value can be changed with an left arrow key and a ctrl key if map is active', async () => {
      const newTime = dateUtils.dateToIsoString(
        dateUtils.sub(dateUtils.fromUnix(props.selectedTime), {
          minutes: props.timeStep,
        }),
      );

      changeLegendAnimatiomValueWithKeyboard('left', true);

      expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
      expect(props.onSetNewDate).toHaveBeenCalledWith(newTime);
    });

    it('rail slider value can not be changed with an left arrow key and a ctrl key if map is inactive', async () => {
      changeLegendAnimatiomValueWithKeyboard('left', false);

      expect(props.onSetNewDate).not.toHaveBeenCalled();
    });
  });
});
