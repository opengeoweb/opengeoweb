/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';

import type { Meta, StoryObj } from '@storybook/react';
import { StorybookDocsWrapper } from '@opengeoweb/shared';
import { TimeSpanButton } from './TimeSpanButton';

const meta: Meta<typeof TimeSpanButton> = {
  title: 'components/TimeSliderButtons/TimeSpanButton',
  component: TimeSpanButton,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the TimeSpanButton component',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof TimeSpanButton>;

export const Component: Story = {
  args: {
    isOpenByDefault: false,
    isTimeSpanAuto: false,
    timeSliderSpan: 3600,
    layerHasTimeStep: true,
    disabled: false,
  },
  render: (props) => (
    <div style={{ padding: 20, height: 66 }}>
      <div
        style={{
          position: 'absolute',
        }}
      >
        <TimeSpanButton {...props} />
      </div>
    </div>
  ),
};

export const ComponentDark: Story = {
  ...Component,
  tags: ['dark'],
  render: (props) => (
    <StorybookDocsWrapper isDark>
      <div style={{ padding: 20, height: 66 }}>
        <div
          style={{
            position: 'absolute',
          }}
        >
          <TimeSpanButton {...props} />
        </div>
      </div>
    </StorybookDocsWrapper>
  ),
};

const TimeSpanButtonDemo = (): React.ReactElement => {
  return (
    <div
      style={{
        width: '250px',
        height: '1200px',
        position: 'absolute',
      }}
    >
      <div
        style={{
          position: 'absolute',
          bottom: '16px',
          left: '16px',
        }}
      >
        <TimeSpanButton
          timeSliderSpan={3600}
          isOpenByDefault
          onChangeSliderValue={(): void => {}}
          onToggleTimeSpan={(): void => {}}
          isTimeSpanAuto={false}
        />
      </div>
    </div>
  );
};

export const TimeSpanButtonDemoLight: Story = {
  render: () => <TimeSpanButtonDemo />,
  tags: ['snapshot', '!autodocs'],
  parameters: {
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/609aac2d83b125bd6e2040b7/version/62c841daa73da317154f7aea',
      },
    ],
  },
};

TimeSpanButtonDemoLight.storyName = 'TimeSpanButtonDemoLight';

export const TimeSpanButtonDemoDark: Story = {
  render: () => <TimeSpanButtonDemo />,
  tags: ['snapshot', 'dark', '!autodocs'],
  parameters: {
    zeplinLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5eea2a91efab124620a1c354/version/62c841ee31ced8193a45c613',
      },
    ],
  },
};

TimeSpanButtonDemoDark.storyName = 'TimeSpanButtonDemoDark';
