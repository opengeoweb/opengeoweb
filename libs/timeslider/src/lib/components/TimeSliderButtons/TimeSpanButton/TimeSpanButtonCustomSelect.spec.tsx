/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { TimeSpanButtonCustomSelect } from './TimeSpanButtonCustomSelect';
import { DemoWrapper } from '../../../Providers/Providers';

describe('src/components/TimeSlider/TimeSliderButtons/TimeSpanButtonCustomSelect', () => {
  const props = {
    setOpen: jest.fn(),
    onChangeSliderValue: jest.fn(),
    timeSliderSpan: 60,
  };
  const user = userEvent.setup();

  it('render the input element and handle time changes', async () => {
    render(
      <DemoWrapper>
        <TimeSpanButtonCustomSelect {...props} />
      </DemoWrapper>,
    );

    const textField = screen.getByLabelText('custom-input');
    expect(textField).toBeInTheDocument();

    // timeSliderSpan at 60 should render 1h
    expect(textField.getAttribute('value')).toEqual('1');
    await user.type(textField, '0{enter}');

    // Setting value to 10 should call change on timeSliderSpan * 10
    expect(props.onChangeSliderValue).toHaveBeenCalledWith(600);

    const selector = screen.getByRole('combobox');
    expect(selector).toBeInTheDocument();

    await user.click(selector);
    await user.click(screen.getByText('months'));

    // Setting selector to month should call change with month(in minutes) * value
    expect(props.onChangeSliderValue).toHaveBeenCalledWith(43200 * 10);
  });
});
