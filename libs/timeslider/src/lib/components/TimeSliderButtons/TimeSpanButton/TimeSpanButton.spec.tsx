/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { TimeSpanButton } from './TimeSpanButton';
import { DemoWrapper } from '../../../Providers/Providers';
import { translateKeyOutsideComponents } from '../../../utils/i18n';

describe('src/components/TimeSlider/TimeSliderButtons/TimeSpanButton', () => {
  const props = {
    timeSliderSpan: 2 * 24 * 60,
    onChangeSliderValue: jest.fn(),
    onToggleTimeSpan: jest.fn(),
    isTimeSpanAuto: false,
    isOpenByDefault: false,
    layerHasTimeStep: true,
  };
  const user = userEvent.setup();

  it('should at start in tests render the component with a span text', () => {
    render(
      <DemoWrapper>
        <TimeSpanButton {...props} />
      </DemoWrapper>,
    );

    expect(
      screen.getByRole('button', { name: /time span/i }),
    ).toHaveTextContent(translateKeyOutsideComponents('timeslider-day'));
  });

  it('should show and hide tooltip', async () => {
    render(
      <DemoWrapper>
        <TimeSpanButton {...props} />
      </DemoWrapper>,
    );

    const button = screen.getByLabelText(
      translateKeyOutsideComponents('timeslider-time-span'),
    );
    expect(button).toBeInTheDocument();

    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
    expect(screen.queryByText(/Time span/)).not.toBeInTheDocument();

    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeInTheDocument();
    expect(screen.getByText(/Time span/)).toBeInTheDocument();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'));
    expect(screen.queryByText(/Time span/)).not.toBeInTheDocument();
  });

  it('should toggle a menu on click', async () => {
    render(
      <DemoWrapper>
        <TimeSpanButton {...props} />
      </DemoWrapper>,
    );

    expect(screen.queryByRole('menu')).not.toBeInTheDocument();

    const button = screen.getByLabelText(
      translateKeyOutsideComponents('timeslider-time-span'),
    );
    await user.click(button);
    expect(screen.getByRole('menu')).toBeInTheDocument();

    await user.keyboard('{Escape}');
    expect(screen.queryByRole('menu')).not.toBeInTheDocument();
  });

  it('should render the button correctly with 13 marks and 2 days as active', async () => {
    render(
      <DemoWrapper>
        <TimeSpanButton {...props} />
      </DemoWrapper>,
    );

    await user.click(screen.getByRole('button', { name: /time span/i }));

    expect(screen.getAllByRole('menuitem')).toHaveLength(14);

    expect(screen.getByRole('menuitem', { name: /48 h/i })).toHaveClass(
      'Mui-selected',
    );
  });

  it('handleChange is called to increase/decrease scale', async () => {
    render(
      <DemoWrapper>
        <TimeSpanButton {...props} />
      </DemoWrapper>,
    );
    await user.click(screen.getByRole('button', { name: /time span/i }));

    await user.click(screen.getByRole('menuitem', { name: /6 h/i }));
    expect(props.onChangeSliderValue).toHaveBeenCalledTimes(1);

    expect(props.onChangeSliderValue).toHaveBeenLastCalledWith(360);
  });

  it('span button can be correctly called with other span value than 5', async () => {
    const props2 = {
      isOpenByDefault: false,
      layerHasTimeStep: true,
      timeSliderSpan: 2 * 7 * 24 * 60,
      onChangeSliderValue: jest.fn(),
      onToggleTimeSpan: jest.fn(),
      onMenuItemClick: jest.fn(),
      isTimeSpanAuto: false,
    };
    render(
      <DemoWrapper>
        <TimeSpanButton {...props2} />
      </DemoWrapper>,
    );

    await user.click(screen.getByRole('button', { name: /time span/i }));

    expect(screen.getByRole('menuitem', { name: /2 weeks/i })).toHaveClass(
      'Mui-selected',
    );
  });

  it('TimeSpanButton value can be scrolled with mouse', async () => {
    jest.useFakeTimers();

    render(
      <DemoWrapper>
        <TimeSpanButton {...props} />
      </DemoWrapper>,
    );

    const button = screen.getByRole('button', { name: /time span/i });

    fireEvent.wheel(button, { deltaY: 1 });
    expect(props.onChangeSliderValue).toHaveBeenCalledTimes(1);
    expect(props.onChangeSliderValue).toHaveBeenCalledWith(24 * 60);

    // wait for throttle time
    jest.runOnlyPendingTimers();

    fireEvent.wheel(button, { deltaY: -1 });
    expect(props.onChangeSliderValue).toHaveBeenCalledTimes(2);
    expect(props.onChangeSliderValue).toHaveBeenCalledWith(7 * 24 * 60);

    jest.useRealTimers();
  });
});
