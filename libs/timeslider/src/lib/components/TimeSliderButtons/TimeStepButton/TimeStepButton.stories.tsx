/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';

import { Box } from '@mui/system';
import type { Meta, StoryObj } from '@storybook/react';
import { StorybookDocsWrapper } from '@opengeoweb/shared';
import { TimeStepButton } from './TimeStepButton';

const meta: Meta<typeof TimeStepButton> = {
  title: 'components/TimeSliderButtons/TimeStepButton',
  component: TimeStepButton,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the TimeStepButton component',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof TimeStepButton>;

export const Component: Story = {
  args: {
    timeStep: 5,
    isOpenByDefault: false,
    disabled: false,
    timeStepFromLayer: 1,
    onChangeTimeStep: () => {},
    isTimestepAuto: false,
  },
  render: (props) => (
    <div style={{ padding: 20, height: 66 }}>
      <div
        style={{
          position: 'absolute',
        }}
      >
        <TimeStepButton {...props} />
      </div>
    </div>
  ),
};

export const ComponentDark: Story = {
  ...Component,
  tags: ['dark'],
  render: (props) => (
    <StorybookDocsWrapper isDark>
      <div style={{ padding: 20, height: 66 }}>
        <div
          style={{
            position: 'absolute',
          }}
        >
          <TimeStepButton {...props} />
        </div>
      </div>
    </StorybookDocsWrapper>
  ),
};

const TimeStepButtonDemo = (): React.ReactElement => {
  return (
    <Box
      sx={{
        width: '250px',
        height: '1300px',
        position: 'absolute',
      }}
    >
      <div
        style={{
          position: 'absolute',
          bottom: '16px',
          left: '16px',
        }}
      >
        <TimeStepButton
          onChangeTimeStep={(): void => {}}
          timeStep={5}
          isOpenByDefault
          isTimestepAuto={false}
        />
      </div>
    </Box>
  );
};

export const TimeStepButtonDemoLight: Story = {
  render: () => <TimeStepButtonDemo />,
  tags: ['!autodocs', 'snapshot'],
  parameters: {
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/609aac2d83b125bd6e2040b7/version/62c841daa73da317154f7aea',
      },
    ],
  },
};

TimeStepButtonDemoLight.storyName = 'TimeStepButtonDemoLight';

export const TimeStepButtonDemoDark: Story = {
  render: () => <TimeStepButtonDemo />,
  tags: ['!autodocs', 'snapshot', 'dark'],
  parameters: {
    zeplinLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5eea2a91efab124620a1c354/version/62c841ee31ced8193a45c613',
      },
    ],
  },
};

TimeStepButtonDemoDark.storyName = 'TimeStepButtonDemoDark';
