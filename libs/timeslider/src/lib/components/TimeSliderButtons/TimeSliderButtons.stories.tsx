/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { Grid2 as Grid } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import { StorybookDocsWrapper } from '@opengeoweb/shared';
import { TimeSliderButtons } from './TimeSliderButtons';

import { HideSliderButton } from './HideSliderButton/HideSliderButton';
import { BackwardForwardStepButton } from './BackwardForwardStepButton/BackwardForwardStepButton';
import { PlayButton } from './PlayButton/PlayButton';
import { NowButton } from './NowButton/NowButton';
import { AutoUpdateButton } from './AutoUpdateButton/AutoUpdateButton';
import { OptionsMenuButton } from './OptionsMenuButton';

const meta: Meta<typeof TimeSliderButtons> = {
  title: 'components/TimeSliderButtons',
  component: TimeSliderButtons,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the TimeSliderButtons',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof TimeSliderButtons>;

export const Component: Story = {
  args: {
    backwardBtn: <BackwardForwardStepButton />,
    forwardBtn: <BackwardForwardStepButton isForwardStep />,
    playBtn: <PlayButton />,
    nowBtn: <NowButton />,
    autoUpdateBtn: <AutoUpdateButton />,
    optionsMenuBtn: <OptionsMenuButton />,
  },
  render: (props) => (
    <div style={{ padding: 20 }}>
      <TimeSliderButtons {...props} />
    </div>
  ),
  tags: ['!dev'],
};

export const ComponentDark: Story = {
  ...Component,
  tags: ['dark', '!dev'],
  render: (props) => (
    <StorybookDocsWrapper isDark>
      <div style={{ padding: 20 }}>
        <TimeSliderButtons {...props} />
      </div>
    </StorybookDocsWrapper>
  ),
};

export const TimeSliderButtonsDemo: Story = {
  render: () => (
    <div
      style={{
        marginTop: '100px',
        marginLeft: '20px',
      }}
    >
      <Grid container spacing={1}>
        <Grid>
          <TimeSliderButtons />
        </Grid>
        <Grid>
          <HideSliderButton />
        </Grid>
        <Grid>
          <HideSliderButton isVisible={true} />
        </Grid>
      </Grid>
    </div>
  ),
  parameters: {
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/609aac2d83b125bd6e2040b7/version/625e84ad2f0ac41432c8f568',
      },
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5eea2a91efab124620a1c354/version/625e84c775ae1910e6caa462',
      },
    ],
    docs: {
      description: {
        story: 'Demo with HideSliderButton',
      },
    },
  },
};

TimeSliderButtonsDemo.storyName = 'Time Slider Buttons';
