/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { translateKeyOutsideComponents } from '../../../utils/i18n';
import { AutoUpdateButton } from './AutoUpdateButton';
import { DemoWrapper } from '../../../Providers/Providers';

describe('src/components/TimeSlider/AutoUpdateButton', () => {
  const user = userEvent.setup();

  it('should render the component with text', () => {
    const props = {
      isAutoUpdating: false,
      toggleAutoUpdate: jest.fn(),
      disabled: false,
    };

    render(
      <DemoWrapper>
        <AutoUpdateButton {...props} />
      </DemoWrapper>,
    );
    const componentText = screen.getByTestId('auto-update-svg-off').textContent;
    expect(componentText).toContain('AUTO');
  });

  it('should show and hide tooltip', async () => {
    const props = {
      isAutoUpdating: true,
      toggleAutoUpdate: jest.fn(),
      disabled: false,
    };

    render(
      <DemoWrapper>
        <AutoUpdateButton {...props} />
      </DemoWrapper>,
    );
    const button = screen.queryByTestId('autoUpdateButton')!;
    expect(button).toBeTruthy();
    expect(screen.queryByRole('tooltip')).toBeFalsy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('timeslider-auto-update-on'),
      ),
    ).toBeFalsy();

    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('timeslider-auto-update-on'),
      ),
    ).toBeTruthy();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
      timeout: 3000,
    });
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('timeslider-auto-update-on'),
      ),
    ).toBeFalsy();
  });
});
