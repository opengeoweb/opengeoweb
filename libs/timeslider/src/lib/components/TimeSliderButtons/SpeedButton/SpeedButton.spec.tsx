/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  render,
  waitForElementToBeRemoved,
  screen,
  fireEvent,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { SpeedButton, SpeedButtonProps } from './SpeedButton';
import { DemoWrapper } from '../../../Providers/Providers';
import {
  defaultAnimationDelayAtStart,
  getSpeedDelay,
} from '../../TimeSlider/timeSliderUtils';
import { translateKeyOutsideComponents } from '../../../utils/i18n';

describe('src/components/TimeSlider/SpeedButton', () => {
  const props: SpeedButtonProps = {
    animationDelay: defaultAnimationDelayAtStart,
    setMapAnimationDelay: jest.fn(),
  };
  const user = userEvent.setup();
  it('should render the button with text', () => {
    render(
      <DemoWrapper>
        <SpeedButton {...props} />
      </DemoWrapper>,
    );

    expect(
      screen.getByRole('button', {
        name: /speed/i,
      }),
    ).toHaveTextContent('x4');
  });

  it('should show and hide tooltip', async () => {
    const props2: SpeedButtonProps = {
      animationDelay: defaultAnimationDelayAtStart,
      setMapAnimationDelay: jest.fn(),
    };
    render(
      <DemoWrapper>
        <SpeedButton {...props2} />
      </DemoWrapper>,
    );

    const button = screen.getByRole('button', {
      name: /speed/i,
    });
    expect(button).toBeInTheDocument();
    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
    expect(
      screen.queryByText(translateKeyOutsideComponents('timeslider-speed')),
    ).not.toBeInTheDocument();

    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(
      screen.getByText(translateKeyOutsideComponents('timeslider-speed')),
    ).toBeTruthy();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
      timeout: 3000,
    });
    expect(
      screen.queryByText(translateKeyOutsideComponents('timeslider-speed')),
    ).not.toBeInTheDocument();
  });

  it('should toggle a menu', async () => {
    render(
      <DemoWrapper>
        <SpeedButton {...props} />
      </DemoWrapper>,
    );

    expect(screen.queryByRole('menu')).not.toBeInTheDocument();
    await user.click(
      screen.getByRole('button', {
        name: /speed/i,
      }),
    );
    expect(screen.getByRole('menu')).toBeInTheDocument();
    await user.keyboard('{Escape}');
    expect(screen.queryByRole('menu')).not.toBeInTheDocument();
  });

  it('should render the slider correctly with 9 marks and one active', async () => {
    render(
      <DemoWrapper>
        <SpeedButton {...props} />
      </DemoWrapper>,
    );

    await user.click(
      screen.getByRole('button', {
        name: /speed/i,
      }),
    );
    expect(screen.getAllByRole('menuitem')).toHaveLength(9);

    expect(
      screen.getByRole('menuitem', {
        name: /x4/i,
      }),
    ).toHaveClass('Mui-selected');
  });

  it('should render button as enabled', () => {
    render(
      <DemoWrapper>
        <SpeedButton {...props} />
      </DemoWrapper>,
    );

    expect(
      screen.getByRole('button', {
        name: /speed/i,
      }),
    ).toBeEnabled();
  });

  it('speedButton menu can be opened and closed and handleChange is called correctly', async () => {
    render(
      <DemoWrapper>
        <SpeedButton {...props} />
      </DemoWrapper>,
    );
    await user.click(
      screen.getByRole('button', {
        name: /speed/i,
      }),
    );
    await user.click(
      screen.getByRole('menuitem', {
        name: /x0.1/i,
      }),
    );

    expect(props.setMapAnimationDelay).toHaveBeenCalledTimes(1);
    expect(props.setMapAnimationDelay).toHaveBeenCalledWith(getSpeedDelay(0.1));

    await user.click(
      screen.getByRole('button', {
        name: /speed/i,
      }),
    );
    await user.click(
      screen.getByRole('menuitem', {
        name: /x16/i,
      }),
    );
    expect(props.setMapAnimationDelay).toHaveBeenCalledTimes(2);
    expect(props.setMapAnimationDelay).toHaveBeenLastCalledWith(
      getSpeedDelay(16),
    );
  });

  it('speedButton  can be correctly called with other delay than default delay', async () => {
    const props2 = {
      animationDelay: 1000,
      setMapAnimationDelay: jest.fn(),
    };
    render(
      <DemoWrapper>
        <SpeedButton {...props2} />
      </DemoWrapper>,
    );

    expect(
      screen.getByRole('button', {
        name: /speed/i,
      }),
    ).toHaveTextContent('x1');
    await user.click(
      screen.getByRole('button', {
        name: /speed/i,
      }),
    );
    expect(
      screen.getByRole('menuitem', {
        name: 'x1',
      }),
    ).toHaveClass('Mui-selected');
  });

  it('SpeedButton value can be scrolled with mouse', async () => {
    jest.useFakeTimers();

    render(
      <DemoWrapper>
        <SpeedButton {...props} />
      </DemoWrapper>,
    );

    const button = screen.getByRole('button', { name: /speed/i });

    fireEvent.wheel(button, { deltaY: 1 });
    expect(props.setMapAnimationDelay).toHaveBeenCalledTimes(1);
    expect(props.setMapAnimationDelay).toHaveBeenCalledWith(getSpeedDelay(2));

    // wait for throttle time
    jest.runOnlyPendingTimers();

    fireEvent.wheel(button, { deltaY: -1 });
    expect(props.setMapAnimationDelay).toHaveBeenCalledTimes(2);
    expect(props.setMapAnimationDelay).toHaveBeenCalledWith(getSpeedDelay(8));

    jest.useRealTimers();
  });
});
