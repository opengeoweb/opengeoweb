/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { dateToManualFormatString } from './clockContainerUtils';

describe('clockContainerUtils', () => {
  it('should manually format date to utc', () => {
    const date = new Date('2022-11-27T15:30+00:00');
    const dateString = dateToManualFormatString(date);

    expect(dateString).toEqual('Sun 27 Nov 15:30 UTC');
  });
  it('should inform user date is not available when undefined or invalid', () => {
    const dateString = dateToManualFormatString(undefined);

    expect(dateString).toEqual('Date not available');
  });
});
