/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React, { CSSProperties, FC, useState } from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { StorybookDocsWrapper } from '@opengeoweb/shared';
import { TimeSliderClock } from './TimeSliderClock';

const meta: Meta<typeof TimeSliderClock> = {
  title: 'components/TimeSliderClock',
  component: TimeSliderClock,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the TimeSliderClock',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof TimeSliderClock>;

export const Component: Story = {
  args: {
    time: 0,
    hideButton: false,
    isPopperOpenByDefault: false,
  },
  render: (props) => (
    <div
      style={{
        height: 80,
      }}
    >
      <TimeSliderClock {...props} />
    </div>
  ),
};
export const ComponentDark: Story = {
  ...Component,
  tags: ['dark'],
  render: (props) => (
    <StorybookDocsWrapper isDark>
      {' '}
      <div
        style={{
          height: 80,
        }}
      >
        <TimeSliderClock {...props} />
      </div>
    </StorybookDocsWrapper>
  ),
};

const DemoTimeSliderClock: FC<{ left: boolean }> = ({ left }) => {
  const [hideButton, setHideButton] = useState(false);

  React.useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent): void => {
      if (event.key === 't') {
        setHideButton((hideButton) => !hideButton);
      }
    };

    document.addEventListener('keydown', handleKeyDown);
    return (): void => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, []);

  const style: CSSProperties = {
    position: 'relative',
    height: left ? 100 : 400,
    width: left ? 700 : 400,
  };

  return (
    <div style={style}>
      <TimeSliderClock
        time={0}
        hideButton={hideButton}
        isPopperOpenByDefault={true}
      />
    </div>
  );
};

const zeplinLight = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/609aac2d83b125bd6e2040b7/version/62c841daa73da317154f7aea',
    },
  ],
};

const zeplinDark = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5eea2a91efab124620a1c354/version/62c841ee31ced8193a45c613',
    },
  ],
};

export const DemoTimeSliderClockMenuLeftLight: Story = {
  render: () => <DemoTimeSliderClock left={true} />,
  tags: ['snapshot', '!autodocs'],
  parameters: zeplinLight,
};
DemoTimeSliderClockMenuLeftLight.storyName =
  'Time Slider Clock Menu Left Light';

export const DemoTimeSliderClockMenuLeftDark: Story = {
  render: () => <DemoTimeSliderClock left={true} />,
  tags: ['snapshot', '!autodocs', 'dark'],
  parameters: zeplinDark,
};
DemoTimeSliderClockMenuLeftDark.storyName = 'Time Slider Clock Menu Left Dark';

export const DemoTimeSliderClockMenuBottomLight: Story = {
  render: () => <DemoTimeSliderClock left={false} />,
  tags: ['snapshot', '!autodocs'],
  parameters: zeplinLight,
};
DemoTimeSliderClockMenuBottomLight.storyName =
  'Time Slider Clock Menu Bottom Light';

export const DemoTimeSliderClockMenuBottomDark: Story = {
  render: () => <DemoTimeSliderClock left={false} />,
  tags: ['snapshot', '!autodocs', 'dark'],
  parameters: zeplinDark,
};
DemoTimeSliderClockMenuBottomDark.storyName =
  'Time Slider Clock Menu Bottom Dark';
