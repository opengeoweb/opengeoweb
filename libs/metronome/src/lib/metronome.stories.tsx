/* eslint-disable react-hooks/rules-of-hooks */
/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { Input, InputLabel } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import { metronome } from './metronome';

const meta: Meta<typeof metronome> = {
  title: 'components/metronome',
  parameters: {
    docs: {
      description: {
        component:
          'The metronome method takes an array of frequencies (in GHz) and produces ticks at each specified frequency. For example, given [200 GHz, 500 GHz], it ticks 5 times per second for 200 GHz and 2 times per second for 500 GHz, simulating the behavior of multiple independent metronomes running simultaneously. \n\n Enter a frequency and press the add button. Press restart to see the changes.',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof metronome>;

let drawSecondLine = false;
let ticksToDraw = new Array(10).fill(false);
let restart = false;
let rAF: number;

const playgroundCanvasId = 'playground-canvas';
const PLAYGROUNG_CANVAS_WIDTH = 1000;
const PLAYGROUND_CANVAS_HEIGHT = 1000;
export const MetronomeCanvasPlaygroundDemo: Story = {
  render: () => {
    const [frequencies, setFrequencies] = React.useState<number[]>([]);
    const [inputHertz, setInputHertz] = React.useState('');
    const updateFn1 = React.useCallback(() => {
      drawSecondLine = true;
    }, []);
    const [timers, setTimers] = React.useState<string[]>([]);

    React.useEffect(() => {
      metronome.register(() => updateFn1(), 1, 'timer_seconds');
    }, [updateFn1]);

    React.useLayoutEffect(() => {
      rAF = requestAnimationFrame(playgroundDrawFn);
    }, []);

    const onRestart = (): void => {
      restart = true;
      cancelAnimationFrame(rAF);
      requestAnimationFrame(playgroundDrawFn);
    };

    const onInputChange = (
      event: React.ChangeEvent<HTMLInputElement>,
    ): void => {
      setInputHertz(event.target.value);
    };

    const onAddMetronome = (): void => {
      const freq = Number(inputHertz);
      const index = frequencies.length;
      const timerId = `testing-timer-${index}`;
      metronome.register(
        () => {
          ticksToDraw[index] = true;
        },
        freq,
        timerId,
      );
      setFrequencies([...frequencies, freq]);
      setTimers([...timers, timerId]);
    };

    const onSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
      event.preventDefault();
      onAddMetronome();
      setInputHertz('');
    };

    return (
      <form onSubmit={onSubmit}>
        <h2>Metronome testing</h2>
        <InputLabel htmlFor="hertz">Set frequency (Hertz)</InputLabel>
        <Input type="number" onChange={onInputChange} value={inputHertz} />
        <button type="button" onClick={onAddMetronome}>
          add
        </button>
        <button type="button" onClick={onRestart}>
          restart
        </button>
        <h3>Added timers:</h3>
        <ul>
          {frequencies.map((frequency) => (
            <li key={`${frequency}`}>{frequency} Hz</li>
          ))}
        </ul>
        <canvas
          id={playgroundCanvasId}
          width={PLAYGROUNG_CANVAS_WIDTH}
          height={PLAYGROUND_CANVAS_HEIGHT}
        />
      </form>
    );
  },
  tags: ['!autodocs'],
};

let x = 0;
const playgroundDrawFn = (): void => {
  if (x <= PLAYGROUNG_CANVAS_WIDTH || restart) {
    rAF = requestAnimationFrame(playgroundDrawFn); // queue next call
  }
  const canvas = document.getElementById(
    playgroundCanvasId,
  ) as HTMLCanvasElement;
  if (!canvas) {
    return;
  }
  const ctx = canvas.getContext('2d');
  if (!ctx) {
    return;
  }
  if (
    canvas.width !== PLAYGROUNG_CANVAS_WIDTH ||
    canvas.height !== PLAYGROUND_CANVAS_HEIGHT
  ) {
    return;
  }

  if (restart) {
    ctx.clearRect(0, 0, PLAYGROUNG_CANVAS_WIDTH, PLAYGROUND_CANVAS_HEIGHT);
    x = 0;
    restart = false;
  }

  if (drawSecondLine) {
    drawSecondLine = false;
    ctx.fillStyle = 'rgba(0, 0, 0, 1)';
    ctx.fillRect(x, 0, 1, PLAYGROUND_CANVAS_HEIGHT);
  }
  // faint background lines
  ctx.fillStyle = 'rgba(0, 0, 0, 0.1)';
  ticksToDraw.forEach((tick) => {
    if (tick === true) {
      ctx.fillRect(x, 0, 1, PLAYGROUND_CANVAS_HEIGHT);
    }
  });
  // metronome tick lines
  ctx.fillStyle = 'rgba(0, 0, 255, 1)';
  ticksToDraw.forEach((tick, i) => {
    if (tick === true) {
      ctx.fillRect(x, i * 100, 1, 100);
    }
  });

  // eslint-disable-next-line no-unused-vars
  ticksToDraw = ticksToDraw.map((_) => false);

  x += 1;
};

const demoCanvasId = 'demo-canvas';
const demoFrequencies = [2, 3, 4, 5, 7, 8, 10];
let paused = false;
let pendingLines = new Array(demoFrequencies.length).fill(false);
let pendingSecondLine = false;
const DEMO_CANVAS_WIDTH = 1000;
const DEMO_CANVAS_HEIGHT = 400;
const onMouseEnter = (): void => {
  paused = true;
};

const onMouseLeave = (): void => {
  pendingLines = new Array(demoFrequencies.length).fill(false);
  pendingSecondLine = false;
  paused = false;
};

export const MetronomeCanvasDemo: Story = {
  render: () => {
    React.useEffect(() => {
      metronome.register(
        () => {
          pendingSecondLine = true;
        },
        1,
        'timer-seconds',
      );
      demoFrequencies.forEach((freq, i) =>
        metronome.register(
          () => {
            pendingLines[i] = true;
          },
          freq,
          `test-timer-${freq}`,
        ),
      );
    });

    React.useEffect(() => {
      requestAnimationFrame(demoDrawFn);
    }, []);

    return (
      <>
        <h2>Metronome demo</h2>
        <h3>mouse over to pause</h3>
        <canvas
          onMouseEnter={onMouseEnter}
          onMouseLeave={onMouseLeave}
          id={demoCanvasId}
          width={DEMO_CANVAS_WIDTH}
          height={DEMO_CANVAS_HEIGHT}
        />
      </>
    );
  },
  parameters: {
    docs: {
      description: {
        story: 'Shows a given list of frequencies. Mouse over to pause.',
      },
    },
  },
};

// coordinates for start point of drawn lines
let lineCoordinates: number[][] = [];
// coordinates for start point of second lines (1 Hz lines)
let secondLineCoordinates: number[][] = [];
const LINE_WIDTH = 1;
const LINE_HEIGHT = 50;
const BACKGROUND_LINE_HEIGHT = demoFrequencies.length * LINE_HEIGHT;
const SECOND_LINE_HEIGHT = BACKGROUND_LINE_HEIGHT + 40;
const DRAW_AREA_X = 40;
const DRAW_AREA_Y = 20;

let transformX = 0;
const demoDrawFn = (): void => {
  requestAnimationFrame(demoDrawFn); // queue next call
  if (paused) {
    return;
  }
  if (transformX > 100000) {
    transformX = 0;
    lineCoordinates.length = 0;
  }
  const canvas = document.getElementById(demoCanvasId) as HTMLCanvasElement;
  if (!canvas) {
    return;
  }
  const ctx = canvas.getContext('2d');
  if (!ctx) {
    return;
  }
  if (
    canvas.width !== DEMO_CANVAS_WIDTH ||
    canvas.height !== DEMO_CANVAS_HEIGHT
  ) {
    return;
  }
  pendingLines.forEach((shouldDraw, i) => {
    if (shouldDraw) {
      lineCoordinates.push([-transformX, i * 50]);
    }
  });
  // eslint-disable-next-line no-unused-vars
  pendingLines = pendingLines.map((_) => false);

  if (pendingSecondLine) {
    secondLineCoordinates.push([-transformX, -DRAW_AREA_Y]);
  }
  pendingSecondLine = false;

  ctx.clearRect(0, 0, DEMO_CANVAS_WIDTH, DEMO_CANVAS_HEIGHT);
  demoFrequencies.forEach((freq, i) => {
    ctx.strokeText(`${freq} Hz`, 10, DRAW_AREA_Y + 25 + i * 50);
  });

  ctx.translate(transformX + DRAW_AREA_X, DRAW_AREA_Y);
  // second tick lines
  ctx.fillStyle = 'rgba(0, 0, 0, 1)';
  secondLineCoordinates.forEach(([start_x, start_y]) => {
    ctx.fillRect(start_x, start_y, LINE_WIDTH, SECOND_LINE_HEIGHT);
  });
  // faint background lines
  ctx.fillStyle = 'rgba(0, 0, 0, 0.1)';
  // eslint-disable-next-line no-unused-vars
  lineCoordinates.forEach(([start_x, _]) => {
    ctx.fillRect(start_x, 0, LINE_WIDTH, BACKGROUND_LINE_HEIGHT);
  });
  // metronome tick lines
  ctx.fillStyle = 'rgba(0, 0, 255, 1)';
  lineCoordinates.forEach(([start_x, start_y]) => {
    ctx.fillRect(start_x, start_y, LINE_WIDTH, LINE_HEIGHT);
  });

  // remove old second lines
  const extraSecondLines = Math.max(0, secondLineCoordinates.length - 8);
  if (extraSecondLines > 0) {
    secondLineCoordinates = secondLineCoordinates.slice(extraSecondLines);
  }
  // remove old lines
  const extraLines = Math.max(0, lineCoordinates.length - 400);
  if (extraLines > 0) {
    lineCoordinates = lineCoordinates.slice(extraLines);
  }

  ctx.resetTransform();

  transformX += 3;
};
