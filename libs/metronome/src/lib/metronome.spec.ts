/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { metronome } from './metronome';

describe('src/lib/utils/metronome', () => {
  describe('metronome', () => {
    beforeEach(() => {
      metronome.unregisterAllTimers();
    });
    it('metronome should be able to register and unregister timers', () => {
      metronome.register(() => {}, 4, 'timerTest4Hz');
      metronome.register(() => {}, 2, 'timerTest2Hz');

      expect(metronome.timers.length).toBe(2);
      expect(metronome.timers[0].timerId).toBe('timerTest4Hz');
      expect(metronome.timers[1].timerId).toBe('timerTest2Hz');

      metronome.unregister('timerTest4Hz');

      expect(metronome.timers.length).toBe(1);
      expect(metronome.timers[0].timerId).toBe('timerTest2Hz');

      metronome.unregister('timerTest2Hz');

      expect(metronome.timers.length).toBe(0);
    });
    it('metronome should not register the same id twice, but should silently take the second', () => {
      jest.useFakeTimers();
      jest.spyOn(global, 'setTimeout');
      metronome.init(); // Re-init to allow jest to fake the timer

      const functionCallA = jest.fn();
      const functionCallB = jest.fn();

      const timerA = metronome.register(functionCallA, 4, 'sameTimerId');
      expect(timerA).toEqual({
        hertz: 4,
        isPaused: false,
        timerFunc: functionCallA,
        timerId: 'sameTimerId',
      });
      const timerB = metronome.register(functionCallB, 2, 'sameTimerId');
      expect(timerB).toEqual({
        hertz: 2,
        isPaused: false,
        timerFunc: functionCallB,
        timerId: 'sameTimerId',
      });
      expect(metronome.timers.length).toBe(1);
      jest.advanceTimersByTime(1000);
      expect(functionCallA).toHaveBeenCalledTimes(0);
      expect(functionCallB).toHaveBeenCalledTimes(2);
      expect(metronome.timers.length).toBe(1);
    });

    it('metronome should tick 8 times a second with a 8hz timer', async () => {
      jest.useFakeTimers();
      jest.spyOn(global, 'setTimeout');
      metronome.init(); // Re-init to allow jest to fake the timer

      const functionCall = jest.fn();
      metronome.register(functionCall, 8, 'timerTest8Hz');
      jest.advanceTimersByTime(10000); // Catches more fallout with 10s

      expect(functionCall).toHaveBeenCalledTimes(80);

      expect(metronome.timers.length).toBe(1);
      expect(metronome.timers[0].timerId).toBe('timerTest8Hz');

      metronome.unregister('timerTest8Hz');
      expect(metronome.timers.length).toBe(0);
      functionCall.mockRestore();
    });

    it('metronome should tick 3 times a second with a 3hz timer', async () => {
      jest.useFakeTimers();
      jest.spyOn(global, 'setTimeout');
      metronome.init(); // Re-init to allow jest to fake the timer

      const functionCall = jest.fn();
      metronome.register(functionCall, 3, 'timerTest3Hz');
      jest.advanceTimersByTime(10000); // Catches more fallout with 10s

      expect(functionCall).toHaveBeenCalledTimes(30);

      expect(metronome.timers.length).toBe(1);
      expect(metronome.timers[0].timerId).toBe('timerTest3Hz');

      metronome.unregister('timerTest3Hz');
      expect(metronome.timers.length).toBe(0);
      functionCall.mockRestore();
    });

    it('metronome should tick 2 times a second with a 2hz timer', async () => {
      jest.useFakeTimers();
      jest.spyOn(global, 'setTimeout');
      metronome.init(); // Re-init to allow jest to fake the timer

      const functionCall = jest.fn();
      metronome.register(functionCall, 2, 'timerTest2Hz');
      jest.advanceTimersByTime(10000); // Catches more fallout with 10s

      expect(functionCall).toHaveBeenCalledTimes(20);

      expect(metronome.timers.length).toBe(1);
      expect(metronome.timers[0].timerId).toBe('timerTest2Hz');

      metronome.unregister('timerTest2Hz');
      expect(metronome.timers.length).toBe(0);
      functionCall.mockRestore();
    });

    it('metronome should tick 7 times a second with a 7hz timer', async () => {
      jest.useFakeTimers();
      jest.spyOn(global, 'setTimeout');
      metronome.init(); // Re-init to allow jest to fake the timer

      const functionCall = jest.fn();
      metronome.register(functionCall, 7, 'timerTest7Hz');
      jest.advanceTimersByTime(10000); // Catches more fallout with 10s

      expect(functionCall).toHaveBeenCalledTimes(70);

      expect(metronome.timers.length).toBe(1);
      expect(metronome.timers[0].timerId).toBe('timerTest7Hz');

      metronome.unregister('timerTest7Hz');
      expect(metronome.timers.length).toBe(0);
      functionCall.mockRestore();
    });

    it('metronome should tick 99 times a second with a 99hz timer', async () => {
      jest.useFakeTimers();
      jest.spyOn(global, 'setTimeout');
      metronome.init(); // Re-init to allow jest to fake the timer

      const functionCall = jest.fn();
      metronome.register(functionCall, 99, 'timerTest99Hz');
      jest.advanceTimersByTime(10000); // Catches more fallout with 10s

      expect(functionCall).toHaveBeenCalledTimes(990);

      expect(metronome.timers.length).toBe(1);
      expect(metronome.timers[0].timerId).toBe('timerTest99Hz');

      metronome.unregister('timerTest99Hz');
      expect(metronome.timers.length).toBe(0);
      functionCall.mockRestore();
    });

    it('metronome should tick 100 times a second with a 100hz timer', async () => {
      jest.useFakeTimers();
      jest.spyOn(global, 'setTimeout');

      metronome.init();
      const functionCall = jest.fn();
      metronome.register(functionCall, 100, 'timerTest100Hz');
      jest.advanceTimersByTime(1000); // it takes roughly 10ms to run 1 tick when testing -> 1 seconds worth of ticks 100 * 10 = 1000

      expect(functionCall).toHaveBeenCalledTimes(100);
      expect(metronome.timers.length).toBe(1);
      expect(metronome.timers[0].timerId).toBe('timerTest100Hz');

      metronome.unregister('timerTest100Hz');
      expect(metronome.timers.length).toBe(0);
      functionCall.mockRestore();
    });

    it('metronome should tick 100 times a second with a 10000hz timer', async () => {
      jest.useFakeTimers();
      jest.spyOn(global, 'setTimeout');

      metronome.init();
      const functionCall = jest.fn();
      metronome.register(functionCall, 10000, 'timerTest10000Hz');
      jest.advanceTimersByTime(1000); // it takes roughly 10ms to run 1 tick when testing -> 1 seconds worth of ticks 100 * 10 = 1000

      // the timers cannot run faster than the metronome, which runs on 10ms interval
      expect(functionCall).toHaveBeenCalledTimes(100);
      expect(metronome.timers.length).toBe(1);
      expect(metronome.timers[0].timerId).toBe('timerTest10000Hz');

      metronome.unregister('timerTest10000Hz');
      expect(metronome.timers.length).toBe(0);
      functionCall.mockRestore();
    });

    it('metronome should be able to pause and continue a timer', async () => {
      jest.useFakeTimers();
      jest.spyOn(global, 'setTimeout');

      metronome.init();
      const functionCall = jest.fn();
      metronome.register(functionCall, 100, 'timerTest100Hz');
      jest.advanceTimersByTime(1000); // it takes roughly 10ms to run 1 tick when testing -> 1 seconds worth of ticks 100 * 10 = 1000

      expect(functionCall).toHaveBeenCalledTimes(100);
      expect(metronome.timers.length).toBe(1);
      expect(metronome.timers[0].timerId).toBe('timerTest100Hz');

      metronome.pauseTimer('timerTest100Hz');

      jest.advanceTimersByTime(1000);
      expect(metronome.isPaused('timerTest100Hz')).toBeTruthy();
      expect(functionCall).toHaveBeenCalledTimes(100);

      metronome.continueTimer('timerTest100Hz');
      expect(metronome.isPaused('timerTest100Hz')).toBeFalsy();
      jest.advanceTimersByTime(1000);
      expect(functionCall).toHaveBeenCalledTimes(200);

      metronome.unregister('timerTest100Hz');
      expect(metronome.timers.length).toBe(0);
      functionCall.mockRestore();
    });

    it('metronome should be able to pause and continue a timer using toggle', async () => {
      jest.useFakeTimers();
      jest.spyOn(global, 'setTimeout');

      metronome.init();
      const functionCall = jest.fn();
      metronome.register(functionCall, 100, 'timerTest100Hz');
      jest.advanceTimersByTime(1000); // it takes roughly 10ms to run 1 tick when testing -> 1 seconds worth of ticks 100 * 10 = 1000

      expect(functionCall).toHaveBeenCalledTimes(100);
      expect(metronome.timers.length).toBe(1);
      expect(metronome.timers[0].timerId).toBe('timerTest100Hz');

      metronome.toggleTimer('timerTest100Hz');

      jest.advanceTimersByTime(1000);
      expect(metronome.isPaused('timerTest100Hz')).toBeTruthy();
      expect(functionCall).toHaveBeenCalledTimes(100);

      metronome.toggleTimer('timerTest100Hz');
      expect(metronome.isPaused('timerTest100Hz')).toBeFalsy();
      jest.advanceTimersByTime(1000);
      expect(functionCall).toHaveBeenCalledTimes(200);

      metronome.unregister('timerTest100Hz');
      expect(metronome.timers.length).toBe(0);
      functionCall.mockRestore();
    });
    it('metronome should be able to change the speed of a timer', async () => {
      jest.useFakeTimers();
      jest.spyOn(global, 'setTimeout');

      metronome.init();
      const functionCall = jest.fn();
      metronome.register(functionCall, 100, 'timerTestVaryingHz');
      jest.advanceTimersByTime(1000); // it takes roughly 10ms to run 1 tick when testing -> 1 seconds worth of ticks 100 * 10 = 1000

      expect(functionCall).toHaveBeenCalledTimes(100);
      expect(metronome.timers.length).toBe(1);
      expect(metronome.timers[0].timerId).toBe('timerTestVaryingHz');

      metronome.setSpeed('timerTestVaryingHz', 50);
      jest.advanceTimersByTime(1000);
      expect(functionCall).toHaveBeenCalledTimes(150);

      metronome.setSpeed('timerTestVaryingHz', 25);
      jest.advanceTimersByTime(1000);
      expect(functionCall).toHaveBeenCalledTimes(175);

      metronome.setSpeed('notARealTimer', 50);
      jest.advanceTimersByTime(1000);
      expect(functionCall).toHaveBeenCalledTimes(200);

      metronome.unregister('timerTestVaryingHz');
      expect(metronome.timers.length).toBe(0);
      functionCall.mockRestore();
    });
    it('metronome functions should not crash when a non existing timerid is given', async () => {
      metronome.register(() => {}, 10, 'reallydoesnotexist');
      metronome.unregister('doesnotexist');
      metronome.pauseTimer('doesnotexist');
      metronome.continueTimer('doesnotexist');
      metronome.toggleTimer('doesnotexist');
    });
  });
});
