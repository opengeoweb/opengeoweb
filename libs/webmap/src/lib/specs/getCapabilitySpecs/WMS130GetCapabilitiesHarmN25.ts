/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

export default `<?xml version="1.0" encoding="UTF-8"?>
<WMS_Capabilities
        version="1.3.0"
        updateSequence="0"
        xmlns="http://www.opengis.net/wms"
        xmlns:xlink="http://www.w3.org/1999/xlink"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.opengis.net/wms http://schemas.opengis.net/wms/1.3.0/capabilities_1_3_0.xsd"

     >
    <Service>
        <Name>WMS</Name>
        <Title>HARM_N25</Title>
        <Abstract>This service demonstrates how the ADAGUC server can be used to create OGC services.</Abstract>
        <KeywordList>
            <Keyword>view</Keyword>
            <Keyword>infoMapAccessService</Keyword>
            <Keyword>ADAGUCServer version 2.5.8, of Apr 13 2021 07:55:43 </Keyword>
        </KeywordList>
        <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;"/>
        <ContactInformation>
          
        </ContactInformation>
        <Fees>no conditions apply</Fees>
        <AccessConstraints>None</AccessConstraints>
        <MaxWidth>8192</MaxWidth>
        <MaxHeight>8192</MaxHeight>
    </Service>
    <Capability>
        <Request>
            <GetCapabilities>
                <Format>text/xml</Format>
                <DCPType><HTTP><Get><OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;"/></Get></HTTP></DCPType>
            </GetCapabilities>
            <GetMap>
                <Format>image/png</Format>
                <Format>image/png;mode=8bit</Format>
                <Format>image/png;mode=8bit_noalpha</Format>
                <Format>image/png;mode=24bit</Format>
                <Format>image/png;mode=32bit</Format>
                <Format>image/gif</Format>
                <Format>image/jpeg</Format>
                <!--<Format>image/webp</Format>-->
                <DCPType><HTTP><Get><OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;"/></Get></HTTP></DCPType>
            </GetMap>
            <GetFeatureInfo>
                <Format>image/png</Format>
                <Format>text/plain</Format>
                <Format>text/html</Format>
                <Format>text/xml</Format>
                <Format>application/json</Format>
                <DCPType><HTTP><Get><OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;"/></Get></HTTP></DCPType>
            </GetFeatureInfo>
        </Request>
        <Exception>
            <Format>XML</Format>
            <Format>INIMAGE</Format>
            <Format>BLANK</Format>
        </Exception>
        <UserDefinedSymbolization SupportSLD="1" />
<Layer>
<Title>WMS of  HARM_N25</Title>
<CRS>EPSG:3411</CRS>
<CRS>EPSG:3412</CRS>
<CRS>EPSG:3575</CRS>
<CRS>EPSG:3857</CRS>
<CRS>EPSG:4258</CRS>
<CRS>EPSG:4326</CRS>
<CRS>CRS:84</CRS>
<CRS>EPSG:25831</CRS>
<CRS>EPSG:25832</CRS>
<CRS>EPSG:28992</CRS>
<CRS>EPSG:7399</CRS>
<CRS>EPSG:50001</CRS>
<CRS>EPSG:54030</CRS>
<CRS>EPSG:32661</CRS>
<CRS>EPSG:40000</CRS>
<CRS>EPSG:900913</CRS>
<CRS>PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs</CRS>
<BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Layer queryable="1" opaque="1" cascaded="0">
<Name>geopotential__at_pl</Name>
<Title>Geopotential height (PL)</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="elevation" units="hPa" default="1000" multipleValues="1" nearestValue="0" >200,300,400,500,600,700,800,850,900,925,950,1000</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>geopotential/contour</Name>    <Title>Geopotential contour</Title>    <Abstract>Contourlines on a transparent background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=geopotential__at_pl&amp;format=image/png&amp;STYLE=geopotential/contour"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=geopotential__at_pl&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=geopotential__at_pl&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>air_pressure_at_sea_level</Name>
<Title>Pressure MSL</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>pressure_cwk/contour</Name>    <Title>Pressure contour (1/5)</Title>    <Abstract>Contourlines on a transparent background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_pressure_at_sea_level&amp;format=image/png&amp;STYLE=pressure_cwk/contour"/>    </LegendURL>  </Style>   <Style>    <Name>pressure_cwk_2.5/contour</Name>    <Title>Pressure contour (2.5/10)</Title>    <Abstract>Contourlines on a transparent background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_pressure_at_sea_level&amp;format=image/png&amp;STYLE=pressure_cwk_2.5/contour"/>    </LegendURL>  </Style>   <Style>    <Name>pressure_cwk_5/contour</Name>    <Title>Pressure contour (5/10)</Title>    <Abstract>Contourlines on a transparent background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_pressure_at_sea_level&amp;format=image/png&amp;STYLE=pressure_cwk_5/contour"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_pressure_at_sea_level&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_pressure_at_sea_level&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>air_temperature__at_2m</Name>
<Title>Temperature 2m</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>temperature/shadedcontour</Name>    <Title>Temperature shaded + contours</Title>    <Abstract>Contours with shaded background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_temperature__at_2m&amp;format=image/png&amp;STYLE=temperature/shadedcontour"/>    </LegendURL>  </Style>   <Style>    <Name>temperature-hi/shadedcontour</Name>    <Title>Temperature (Hi) shaded + contours</Title>    <Abstract>Contours with shaded background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_temperature__at_2m&amp;format=image/png&amp;STYLE=temperature-hi/shadedcontour"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_temperature__at_2m&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_temperature__at_2m&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>air_temperature__at_pl</Name>
<Title>Temperature (PL)</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="elevation" units="hPa" default="1000" multipleValues="1" nearestValue="0" >200,300,400,500,600,700,800,850,900,925,950,1000</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>temperature/shadedcontour</Name>    <Title>Temperature shaded + contours</Title>    <Abstract>Contours with shaded background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_temperature__at_pl&amp;format=image/png&amp;STYLE=temperature/shadedcontour"/>    </LegendURL>  </Style>   <Style>    <Name>temperature-hi/shadedcontour</Name>    <Title>Temperature (Hi) shaded + contours</Title>    <Abstract>Contours with shaded background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_temperature__at_pl&amp;format=image/png&amp;STYLE=temperature-hi/shadedcontour"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_temperature__at_pl&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_temperature__at_pl&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>dew_point_temperature__at_2m</Name>
<Title>Dew Point 2m</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>temperature/shadedcontour</Name>    <Title>Temperature shaded + contours</Title>    <Abstract>Contours with shaded background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=dew_point_temperature__at_2m&amp;format=image/png&amp;STYLE=temperature/shadedcontour"/>    </LegendURL>  </Style>   <Style>    <Name>temperature-hi/shadedcontour</Name>    <Title>Temperature (Hi) shaded + contours</Title>    <Abstract>Contours with shaded background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=dew_point_temperature__at_2m&amp;format=image/png&amp;STYLE=temperature-hi/shadedcontour"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=dew_point_temperature__at_2m&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=dew_point_temperature__at_2m&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>wind__at_10m</Name>
<Title>Wind 10m flags</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>Windbarbs/barb</Name>    <Title>Wind barbs</Title>    <Abstract>Rendered with barbs</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=wind__at_10m&amp;format=image/png&amp;STYLE=Windbarbs/barb"/>    </LegendURL>  </Style>   <Style>    <Name>Windbarbs_mps/barbshaded</Name>    <Title>Wind barbs+sh</Title>    <Abstract>Rendered with barbs</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=wind__at_10m&amp;format=image/png&amp;STYLE=Windbarbs_mps/barbshaded"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=wind__at_10m&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=wind__at_10m&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>wind__at_pl</Name>
<Title>Wind vectors (PL)</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="elevation" units="hPa" default="1000" multipleValues="1" nearestValue="0" >200,300,400,500,600,700,800,850,900,925,950,1000</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>Windbarbs/barb</Name>    <Title>Wind barbs</Title>    <Abstract>Rendered with barbs</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=wind__at_pl&amp;format=image/png&amp;STYLE=Windbarbs/barb"/>    </LegendURL>  </Style>   <Style>    <Name>Windbarbs_mps/barbshaded</Name>    <Title>Wind barbs+sh</Title>    <Abstract>Rendered with barbs</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=wind__at_pl&amp;format=image/png&amp;STYLE=Windbarbs_mps/barbshaded"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=wind__at_pl&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=wind__at_pl&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>precipitation_flux</Name>
<Title>Prec: Precipitation rate</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>precip_cwk/shadedcontour</Name>    <Title>Precipitation CWKnet shaded + contours</Title>    <Abstract>Contours with shaded background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=precipitation_flux&amp;format=image/png&amp;STYLE=precip_cwk/shadedcontour"/>    </LegendURL>  </Style>   <Style>    <Name>precip_cwk/contour</Name>    <Title>Precipitation CWKnet contour</Title>    <Abstract>Contourlines on a transparent background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=precipitation_flux&amp;format=image/png&amp;STYLE=precip_cwk/contour"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=precipitation_flux&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=precipitation_flux&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>snowfall_flux</Name>
<Title>Prec: Snowfall rate</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>precip_cwk/shadedcontour</Name>    <Title>Precipitation CWKnet shaded + contours</Title>    <Abstract>Contours with shaded background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=snowfall_flux&amp;format=image/png&amp;STYLE=precip_cwk/shadedcontour"/>    </LegendURL>  </Style>   <Style>    <Name>precip_cwk/contour</Name>    <Title>Precipitation CWKnet contour</Title>    <Abstract>Contourlines on a transparent background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=snowfall_flux&amp;format=image/png&amp;STYLE=precip_cwk/contour"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=snowfall_flux&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=snowfall_flux&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>graupel_flux</Name>
<Title>Prec: graupel rate</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>precip_cwk/shadedcontour</Name>    <Title>Precipitation CWKnet shaded + contours</Title>    <Abstract>Contours with shaded background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=graupel_flux&amp;format=image/png&amp;STYLE=precip_cwk/shadedcontour"/>    </LegendURL>  </Style>   <Style>    <Name>precip_cwk/contour</Name>    <Title>Precipitation CWKnet contour</Title>    <Abstract>Contourlines on a transparent background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=graupel_flux&amp;format=image/png&amp;STYLE=precip_cwk/contour"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=graupel_flux&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=graupel_flux&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>cloud_area_fraction</Name>
<Title>Cloud cover - total</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>cloudcover/bilinear</Name>    <Title>Cloud cover bilinear</Title>    <Abstract>Rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=cloud_area_fraction&amp;format=image/png&amp;STYLE=cloudcover/bilinear"/>    </LegendURL>  </Style>   <Style>    <Name>totalcloudcover/bilinear</Name>    <Title>Total cloud cover bilinear</Title>    <Abstract>Rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=cloud_area_fraction&amp;format=image/png&amp;STYLE=totalcloudcover/bilinear"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=cloud_area_fraction&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=cloud_area_fraction&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>high_type_cloud_area_fraction</Name>
<Title>Cloud cover - high</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>cloudcover/bilinear</Name>    <Title>Cloud cover bilinear</Title>    <Abstract>Rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=high_type_cloud_area_fraction&amp;format=image/png&amp;STYLE=cloudcover/bilinear"/>    </LegendURL>  </Style>   <Style>    <Name>highcloudcover/bilinear</Name>    <Title>High cloud cover bilinear</Title>    <Abstract>Rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=high_type_cloud_area_fraction&amp;format=image/png&amp;STYLE=highcloudcover/bilinear"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=high_type_cloud_area_fraction&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=high_type_cloud_area_fraction&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>medium_type_cloud_area_fraction</Name>
<Title>Cloud cover - medium</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>cloudcover/bilinear</Name>    <Title>Cloud cover bilinear</Title>    <Abstract>Rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=medium_type_cloud_area_fraction&amp;format=image/png&amp;STYLE=cloudcover/bilinear"/>    </LegendURL>  </Style>   <Style>    <Name>midcloudcover/bilinear</Name>    <Title>Medium cloud cover bilinear</Title>    <Abstract>Rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=medium_type_cloud_area_fraction&amp;format=image/png&amp;STYLE=midcloudcover/bilinear"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=medium_type_cloud_area_fraction&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=medium_type_cloud_area_fraction&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>low_type_cloud_area_fraction</Name>
<Title>Cloud cover - low</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>cloudcover/bilinear</Name>    <Title>Cloud cover bilinear</Title>    <Abstract>Rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=low_type_cloud_area_fraction&amp;format=image/png&amp;STYLE=cloudcover/bilinear"/>    </LegendURL>  </Style>   <Style>    <Name>lowcloudcover/bilinear</Name>    <Title>Low cloud cover bilinear</Title>    <Abstract>Rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=low_type_cloud_area_fraction&amp;format=image/png&amp;STYLE=lowcloudcover/bilinear"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=low_type_cloud_area_fraction&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=low_type_cloud_area_fraction&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>relative_humidity__at_2m</Name>
<Title>Relative humidity 2m</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>relhum/bilinear</Name>    <Title>Fraction bilinear</Title>    <Abstract>Shaded</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=relative_humidity__at_2m&amp;format=image/png&amp;STYLE=relhum/bilinear"/>    </LegendURL>  </Style>   <Style>    <Name>relhum/contour</Name>    <Title>Fraction contour</Title>    <Abstract>Contourlines on a transparent background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=relative_humidity__at_2m&amp;format=image/png&amp;STYLE=relhum/contour"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=relative_humidity__at_2m&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=relative_humidity__at_2m&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>relative_humidity__at_pl</Name>
<Title>Relative humidity (PL)</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="elevation" units="hPa" default="1000" multipleValues="1" nearestValue="0" >200,300,400,500,600,700,800,850,900,925,950,1000</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>relhum/bilinear</Name>    <Title>Fraction bilinear</Title>    <Abstract>Shaded</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=relative_humidity__at_pl&amp;format=image/png&amp;STYLE=relhum/bilinear"/>    </LegendURL>  </Style>   <Style>    <Name>relhum/contour</Name>    <Title>Fraction contour</Title>    <Abstract>Contourlines on a transparent background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=relative_humidity__at_pl&amp;format=image/png&amp;STYLE=relhum/contour"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=relative_humidity__at_pl&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=relative_humidity__at_pl&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>air_temperature__max_at_2m</Name>
<Title>Temperature 2m max.</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>temperature/shadedcontour</Name>    <Title>Temperature shaded + contours</Title>    <Abstract>Contours with shaded background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_temperature__max_at_2m&amp;format=image/png&amp;STYLE=temperature/shadedcontour"/>    </LegendURL>  </Style>   <Style>    <Name>temperature-hi/shadedcontour</Name>    <Title>Temperature (Hi) shaded + contours</Title>    <Abstract>Contours with shaded background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_temperature__max_at_2m&amp;format=image/png&amp;STYLE=temperature-hi/shadedcontour"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_temperature__max_at_2m&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_temperature__max_at_2m&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>air_temperature__min_at_2m</Name>
<Title>Temperature 2m min.</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>temperature/shadedcontour</Name>    <Title>Temperature shaded + contours</Title>    <Abstract>Contours with shaded background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_temperature__min_at_2m&amp;format=image/png&amp;STYLE=temperature/shadedcontour"/>    </LegendURL>  </Style>   <Style>    <Name>temperature-hi/shadedcontour</Name>    <Title>Temperature (Hi) shaded + contours</Title>    <Abstract>Contours with shaded background</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_temperature__min_at_2m&amp;format=image/png&amp;STYLE=temperature-hi/shadedcontour"/>    </LegendURL>  </Style>   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_temperature__min_at_2m&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=air_temperature__min_at_2m&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>cloud_top_altitude</Name>
<Title>Cloud top alt.</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=cloud_top_altitude&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=cloud_top_altitude&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>cloud_base_altitude</Name>
<Title>Cloud base alt.</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=cloud_base_altitude&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=cloud_base_altitude&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
<Layer queryable="1" opaque="1" cascaded="0">
<Name>wind_speed_of_gust__at_10m</Name>
<Title>Wind 10m gust</Title>
<EX_GeographicBoundingBox>
  <westBoundLongitude>-0.018500</westBoundLongitude>
  <eastBoundLongitude>11.081500</eastBoundLongitude>
  <southBoundLatitude>48.988500</southBoundLatitude>
  <northBoundLatitude>55.888499</northBoundLatitude>
</EX_GeographicBoundingBox><BoundingBox CRS="EPSG:3411" minx="2688993.562116" miny="-3278523.471547" maxx="3846301.904223" maxy="-2122703.884475" />
<BoundingBox CRS="EPSG:3412" minx="-12905.545004" miny="32191797.929062" maxx="7682316.457914" maxy="39969365.065534" />
<BoundingBox CRS="EPSG:3575" minx="-778807.959117" miny="-4476772.614323" maxx="84497.453773" maxy="-3692751.174658" />
<BoundingBox CRS="EPSG:3857" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="EPSG:4258" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:4326" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="CRS:84" minx="48.988500" miny="-0.018500" maxx="55.888499" maxy="11.081500" />
<BoundingBox CRS="EPSG:25831" minx="279182.558346" miny="5426179.030812" maxx="1090961.507595" maxy="6223228.410085" />
<BoundingBox CRS="EPSG:25832" minx="-159402.202885" miny="5426183.018603" maxx="652276.579080" maxy="6230493.226418" />
<BoundingBox CRS="EPSG:28992" minx="-240652.647815" miny="110612.618048" maxx="571683.043099" maxy="892835.526754" />
<BoundingBox CRS="EPSG:7399" minx="-1425.633663" miny="5767138.881305" maxx="853954.563912" maxy="6475446.638616" />
<BoundingBox CRS="EPSG:50001" minx="-2000000.000000" miny="-2000000.000000" maxx="10000000.000000" maxy="8500000.000000" />
<BoundingBox CRS="EPSG:54030" minx="-1527.513706" miny="5221582.890802" maxx="914980.709608" maxy="5927845.206474" />
<BoundingBox CRS="EPSG:32661" minx="1998466.191528" miny="-2750310.873970" maxx="2913034.053643" maxy="-1825956.532686" />
<BoundingBox CRS="EPSG:40000" minx="-1543.066873" miny="-4778984.782666" maxx="918545.325596" maxy="-3849050.837712" />
<BoundingBox CRS="EPSG:900913" minx="-2059.410529" miny="6272910.310152" maxx="1233586.906600" maxy="7536250.885751" />
<BoundingBox CRS="PROJ4:%2Bproj%3Dlonglat%20%2Ba%3D6367470%20%2Be%3D0%20%2Bno_defs" minx="-0.018500" miny="48.988500" maxx="11.081500" maxy="55.888499" />
<Dimension name="time" units="ISO8601" default="2021-05-10T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H</Dimension>
<Dimension name="reference_time" units="ISO8601" default="2021-05-17T03:00:00Z" multipleValues="1" nearestValue="0" current="1">2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z</Dimension>
   <Style>    <Name>auto/nearest</Name>    <Title>Automatic minimum and maximum</Title>    <Abstract>Automatic min/max rendered with nearest neighbour interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=wind_speed_of_gust__at_10m&amp;format=image/png&amp;STYLE=auto/nearest"/>    </LegendURL>  </Style>   <Style>    <Name>auto/bilinear</Name>    <Title>Automatic minimum and maximum bilinear</Title>    <Abstract>Automatic min/max rendered with bilinear interpolation</Abstract>    <LegendURL width="300" height="400">       <Format>image/png</Format>       <OnlineResource xlink:type="simple" xlink:href="https://adaguc-server-geoweb.geoweb.knmi.cloud/wms?DATASET=HARM_N25&amp;SERVICE=WMS&amp;&amp;version=1.1.1&amp;service=WMS&amp;request=GetLegendGraphic&amp;layer=wind_speed_of_gust__at_10m&amp;format=image/png&amp;STYLE=auto/bilinear"/>    </LegendURL>  </Style></Layer>
    </Layer>
  </Capability>
</WMS_Capabilities>`;
