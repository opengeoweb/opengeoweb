/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fakeMaxAgeResponsesByLayer } from '../utils/fakeMaxAgeResponses';
import WMImage from './WMImage';

const mockRandom = 123;

describe('components/WMImage', () => {
  beforeEach(() => {
    jest.spyOn(global.Math, 'random').mockReturnValue(mockRandom);
  });
  afterEach(() => {
    jest.spyOn(global.Math, 'random').mockRestore();
  });

  it('should initialise with correct params', () => {
    const url = 'http://test/';
    const fn = jest.fn();
    const image = new WMImage(url, fn);

    expect(image.randomize).toBeFalsy();
    expect(image.getElement().src).toEqual('');

    // trigger load
    image.load();

    expect(image.srcToLoad).toEqual(url);
    expect(image.getElement().src).toEqual(url);
  });

  it('should initialise with correct params and disabled cache as options', () => {
    const url = 'http://test/';
    const fn = jest.fn();
    const options = {
      randomizer: true,
      headers: [],
    };
    const image = new WMImage(url, fn, options);

    expect(image.randomize).toBeTruthy();
    expect(image.getElement().src).toEqual('');

    // trigger load
    image.load();
    expect(image.srcToLoad).toEqual(url);
    expect(image.getElement().src).toEqual(`${url}?random=${mockRandom}`);
  });

  it('should correctly randomize url', () => {
    const imageLoadCalls = jest
      .spyOn(global.Image.prototype, 'src', 'set')
      .mockImplementation();
    const url =
      'https://geoserver.app.meteo.fi/geoserver/ows?SERVICE=WMS&&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap';
    const fn = jest.fn();
    const options = {
      randomizer: true,
      headers: [],
    };
    const image = new WMImage(url, fn, options);

    expect(image.randomize).toBeTruthy();
    expect(image.getElement().src).toEqual('');

    // trigger load
    image.load();
    expect(image.srcToLoad).toEqual(url);
    const receivedCall = imageLoadCalls.mock.lastCall![0];
    expect(receivedCall).toContain(`&random=${mockRandom}`);
    imageLoadCalls.mockRestore();
  });

  it('should trigger the callback when image loading completed', () => {
    const src = 'https://testservice/';
    const callback = jest.fn();
    const image = new WMImage(src, callback);
    const spy = jest.spyOn(image, '_loadEvent' as never);

    expect(image.isLoading()).toBeFalsy();
    expect(image.isLoaded()).toBeFalsy();

    image.load();
    expect(image.isLoading()).toBeTruthy();

    // fire img load event
    image.el.dispatchEvent(new Event('load'));

    expect(image.isLoading()).toBeFalsy();
    expect(image.isLoaded()).toBeTruthy();
    expect(image.hasError()).toBeFalsy();
    expect(callback).toHaveBeenCalled();
    expect(spy).toHaveBeenCalled();
  });

  it('should handle error', () => {
    const src = 'https://testservice/';
    const image = new WMImage(src);

    // trigger error
    image.el.dispatchEvent(new Event('error'));

    expect(image.isLoading()).toBeFalsy();
    expect(image.isLoaded()).toBeTruthy();
    expect(image.hasError()).toBeTruthy();
    expect(image.getLoadDuration()).toBeUndefined();
  });

  it('should return load duration', () => {
    jest.useFakeTimers();

    const src = 'https://testservice/';
    const image = new WMImage(src);

    expect(image.isLoading()).toBeFalsy();
    expect(image.isLoaded()).toBeFalsy();
    expect(image.getLoadDuration()).toBeUndefined();

    image.load();
    expect(image.isLoading()).toBeTruthy();

    // fire img load event after 1 second
    jest.advanceTimersByTime(1000);
    image.el.dispatchEvent(new Event('load'));

    expect(image.isLoading()).toBeFalsy();
    expect(image.isLoaded()).toBeTruthy();
    expect(image.hasError()).toBeFalsy();

    expect(image.getLoadDuration()).toBeGreaterThanOrEqual(1000);

    jest.useRealTimers();
  });

  it('should handle staleness check', () => {
    const mockLayerName = 'test_layer_123_456';
    fakeMaxAgeResponsesByLayer[mockLayerName] = 5;

    jest.useFakeTimers();

    const src = `https://testservice?LAYERS=${mockLayerName}`;
    const image = new WMImage(src);

    expect(image.isStale()).toBe(true); // edge case

    expect(image.isLoading()).toBeFalsy();
    expect(image.isLoaded()).toBeFalsy();
    expect(image.getLoadDuration()).toBeUndefined();

    image.load();
    expect(image.isLoading()).toBeTruthy();
    expect(image.isStale()).toBe(false);

    jest.advanceTimersByTime(1000);
    // fire img load event after 1 second
    image.el.dispatchEvent(new Event('load'));

    expect(image.isLoading()).toBeFalsy();
    expect(image.isLoaded()).toBeTruthy();
    expect(image.hasError()).toBeFalsy();
    expect(image.isStale()).toBe(false);

    jest.advanceTimersByTime(4001);
    expect(image.isStale()).toBe(true);

    delete fakeMaxAgeResponsesByLayer[mockLayerName];
    jest.useRealTimers();
  });
});
