/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import proj4 from 'proj4';
import type WMJSDimension from './WMJSDimension';
import type WMLayer from './WMLayer';
import { Bbox } from './WMBBOX';
import type WMImage from './WMImage';
import type IWMJSMap from './IWMJSMap';

export interface AnimationStep {
  name: string;
  value: string;
  requests?: { url: string; headers: Headers[] }[];
  imagesInPrefetch?: WMImage[];
}

export interface WMPosition {
  x: number;
  y: number;
}

export interface LinkedInfoContent {
  layer: WMLayer;
  message?: string;
}

export interface LinkedInfo {
  linkedInfo: LinkedInfoContent;
}

export interface TimeInterval {
  year: number;
  month: number;
  day: number;
  hour: number;
  minute: number;
  second: number;
  isRegularInterval: boolean;
}

export interface Dimension {
  name?: string;
  units?: string;
  currentValue: string;
  maxValue?: string;
  minValue?: string;
  timeInterval?: TimeInterval;
  synced?: boolean;
  values?: string;
  unitSymbol?: string /* Unit symbol of the WMS Dimension */;
  validSyncSelection?: boolean;
}

export interface Style {
  title: string;
  name: string;
  legendURL: string;
  abstract: string;
}

export interface GeographicBoundingBox {
  west: string;
  east: string;
  north: string;
  south: string;
}

export interface WMLayerType {
  setDimension: (
    name: string,
    value: string,
    updateMapDimensions: boolean,
  ) => void;
  getDimension: (name: string) => WMJSDimension;
  dimensions?: WMJSDimension[];
  title?: string;
  headers?: Headers[];
}

export interface CoordinateReferenceSystem {
  name?: string;
  bbox?: Bbox;
}

export interface LayerProps {
  name:
    | string /* The layer name, it is the identifier of the layer.  */
    | null /* When null, this is a Layer Group and it will have children, leaf will be false */;
  leaf: boolean /* True means that it represents a Layer, in that case name is also set to the layer name. children will be empty */;
  path: string[] /* The hierarchical path of the layer, like a breadcrumb */;
  title?: string /* The title of the layer */;
  abstract?: string /* The abstract of the layer */;
  keywords?: string[] /* The keywords of the layer */;
  styles?: Style[];
  dimensions?: Dimension[];
  geographicBoundingBox?: GeographicBoundingBox | undefined;
  queryable?: boolean;
  crs?: CoordinateReferenceSystem[];
}

/**
 * Represents the layers as advertised in the WMS GetCapabilities document
 */
export interface LayerTree extends LayerProps {
  children: LayerTree[] /* The children of the layer */;
}

export interface WMSBoundingBox {
  attr: {
    SRS?: string;
    CRS?: string;
    maxy: string; // northBoundLatitude
    miny: string; // southBoundLatitude
    maxx: string; // eastBoundLongitude
    minx: string; // westBoundLongitude
  };
}

/* From WMS GetCapabilities */
/**
 * This interface is for reading from the WMS GetCapabilities document, which can have different structures for V1.1.1 and V1.3.0
 */
export interface WMSLayerFromGetCapabilities {
  attr: { queryable: string; opaque: string; cascaded: string };
  Name: { attr: Record<string, unknown>; value: string };
  Title: { attr: Record<string, unknown>; value: string };
  Dimension: unknown /* Dimension element in the WMS GetCapabilities document, can be single object or array */;
  Style: unknown /* Style element in the WMS GetCapabilities document, can be single object or array */;
  Extent: unknown /* Extent element in the WMS GetCapabilities document, can be single object or array */;
  ScaleHint: unknown;
  Abstract: { value: string };
  KeywordList: { Keyword: unknown };
  Layer: WMSLayerFromGetCapabilities | WMSLayerFromGetCapabilities[];
  EX_GeographicBoundingBox?: {
    northBoundLatitude: { attr: Record<string, unknown>; value: string };
    southBoundLatitude: { attr: Record<string, unknown>; value: string };
    westBoundLongitude: { attr: Record<string, unknown>; value: string };
    eastBoundLongitude: { attr: Record<string, unknown>; value: string };
  } /* Geographic bounding box element in the WMS GetCapabilities document, single object, only in V1.3.0 */;
  LatLonBoundingBox?: {
    attr: {
      maxy: string; // northBoundLatitude
      miny: string; // southBoundLatitude
      maxx: string; // eastBoundLongitude
      minx: string; // westBoundLongitude
    };
  };
  /* Geographic bounding box element in the WMS GetCapabilities document */
  BoundingBox?: WMSBoundingBox[];
  SRS?: { value: string }[];
}

export interface WMJSMapMouseClickEvent {
  map: IWMJSMap;
  x: number;
  y: number;
  shiftKeyPressed: boolean;
}
export interface IWMJSMapMouseCoordinates {
  mouseX: number;
  mouseDownX: number;
  mouseY: number;
  mouseDownY: number;
}
export interface IWMProj4 {
  crs: string;
  proj4: typeof proj4;
}

export interface GetCapabilitiesJson {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  WMS_Capabilities?: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  WMT_MS_Capabilities?: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ServiceExceptionReport?: any;
}

export interface WMSServiceInfo {
  id?: string;
  version: string;
  abstract: string;
  title: string;
  onlineresource: string;
  getmapURL: string;
  getCapabilitiesJson: GetCapabilitiesJson;
}

export interface Capability {
  Layer: WMSLayerFromGetCapabilities;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  Request: any;
}

export interface XLINK {
  'xlink:href': string;
}

export interface Header {
  name: string;
  value: string;
}
