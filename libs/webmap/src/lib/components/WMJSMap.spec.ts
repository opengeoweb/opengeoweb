/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  WMBBOX,
  WMJSMap,
  WMLayer,
  WMProj4Defs,
  isProjectionSupported,
} from '.';
import { setWMSGetCapabilitiesFetcher } from '../utils';
import { defaultReduxLayerRadarKNMI } from '../specs/layerSpecs';
import { mockGetCapabilities } from '../specs';

import * as WMJSTools from './WMJSTools';

describe('components/WMJSMap', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });
  it('should initialise with correct elements', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);

    // load layer
    expect(
      map._loadingDiv.className.includes('WMJSDivBuffer-loading'),
    ).toBeTruthy();
    expect(baseElement.innerHTML).toContain('class="WMJSDivBuffer-loading"');
    // main div
    expect(baseElement.innerHTML).toContain(`id="${map._baseDiv.id}"`);
    // zoombox
    expect(baseElement.innerHTML).toContain('class="wmjs-zoombox"');
    // boundingbox
    expect(baseElement.innerHTML).toContain('class="wmjs-boundingbox"');
  });

  it('should attach events', () => {
    const mockBaseElement = document.createElement('div');
    mockBaseElement.addEventListener = jest.fn();
    mockBaseElement.appendChild = jest.fn();

    const map = new WMJSMap(mockBaseElement);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    map._baseDiv = mockBaseElement;

    map._attachEvents();

    expect(mockBaseElement.addEventListener).toHaveBeenCalledWith(
      'mousedown',
      map._mouseDownEvent,
    );
    expect(mockBaseElement.addEventListener).toHaveBeenCalledWith(
      'mouseup',
      map._mouseUpEvent,
    );
    expect(mockBaseElement.addEventListener).toHaveBeenCalledWith(
      'mousemove',
      map._mouseMoveEvent,
    );
    expect(mockBaseElement.addEventListener).toHaveBeenCalledWith(
      'contextmenu',
      map._onContextMenu,
    );
    expect(mockBaseElement.addEventListener).toHaveBeenCalledWith(
      'wheel',
      map._mouseWheelEvent,
    );
  });

  it('should deattach events', () => {
    const mockBaseElement = document.createElement('div');
    mockBaseElement.removeEventListener = jest.fn();
    mockBaseElement.appendChild = jest.fn();

    const map = new WMJSMap(mockBaseElement);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    map._baseDiv = mockBaseElement;

    map._detachEvents();

    expect(mockBaseElement.removeEventListener).toHaveBeenCalledWith(
      'mousedown',
      map._mouseDownEvent,
    );
    expect(mockBaseElement.removeEventListener).toHaveBeenCalledWith(
      'mouseup',
      map._mouseUpEvent,
    );
    expect(mockBaseElement.removeEventListener).toHaveBeenCalledWith(
      'mousemove',
      map._mouseMoveEvent,
    );
    expect(mockBaseElement.removeEventListener).toHaveBeenCalledWith(
      'contextmenu',
      map._onContextMenu,
    );
    expect(mockBaseElement.removeEventListener).toHaveBeenCalledWith(
      'wheel',
      map._mouseWheelEvent,
    );
  });

  it('should set correct cursors when start panning map', () => {
    const baseElement = document.createElement('div');

    const map = new WMJSMap(baseElement);

    map._mapPanStart(0, 0);

    expect(getComputedStyle(map._baseDiv).cursor).toEqual('move');
  });

  it('should set correct cursors when end panning map', () => {
    const baseElement = document.createElement('div');

    const map = new WMJSMap(baseElement);

    map._mapPanEnd(0, 0);

    expect(getComputedStyle(map._baseDiv).cursor).toEqual('default');
  });

  it('should set correct cursors when start zooming map', () => {
    const baseElement = document.createElement('div');

    const map = new WMJSMap(baseElement);
    map._mapZooming = 1;

    map._mapZoom();

    expect(getComputedStyle(map._baseDiv).cursor).toEqual('crosshair');

    // not allowed
    map.mouseMove(0, 0);
    map.mouseDown(1, 1);

    map._mapZoom();
    expect(getComputedStyle(map._baseDiv).cursor).toEqual('not-allowed');
  });

  it('should set correct cursors when end zooming map', () => {
    const baseElement = document.createElement('div');

    const map = new WMJSMap(baseElement);
    map._mapZooming = 1;

    map._mapZoomEnd();

    expect(getComputedStyle(map._baseDiv).cursor).toEqual('default');
  });

  it('should handle fly to bbox', () => {
    const mockBaseElement = document
      .createElement('div')
      .appendChild(document.createElement('div'));
    const map = new WMJSMap(mockBaseElement);

    const spy = {
      flyZoomToBBOXStartZoom: jest.fn(),
      flyZoomToBBOXStop: jest.fn(),
      flyZoomToBBOXFly: {
        setBBOX: jest.fn(),
      },
    };
    const { mouseX, mouseY } = map.getMapMouseCoordinates();
    expect(mouseX).toEqual(0);
    expect(mouseY).toEqual(0);
    const [newPageX, newPageY] = [100, 240];

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    map.wMFlyToBBox = spy;
    map._mouseWheelEvent({ pageX: newPageX, pageY: newPageY } as WheelEvent);

    const { mouseX: mouseX2, mouseY: mouseY2 } = map.getMapMouseCoordinates();

    expect(mouseX2).toEqual(newPageX);
    expect(mouseY2).toEqual(newPageY);

    expect(spy.flyZoomToBBOXStartZoom).toHaveBeenCalledWith(
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      map._mouseWheelEventBBOXCurrent,
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      map._mouseWheelEventBBOXNew,
    );
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    map._mapPanStart();
    expect(spy.flyZoomToBBOXStop).toHaveBeenCalled();

    map.setBBOX(0, 0, 0, 0);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(spy.flyZoomToBBOXFly.setBBOX).toHaveBeenCalledWith(map._bbox);
  });

  it('should set mouse coordinates', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);

    const { mouseX, mouseY } = map.getMapMouseCoordinates();
    expect(mouseX).toEqual(0);
    expect(mouseY).toEqual(0);
    const [newX, newY] = [100, 240];

    map._setMouseCoordinates(newX, newY);
    const { mouseX: mouseX2, mouseY: mouseY2 } = map.getMapMouseCoordinates();
    expect(mouseX2).toEqual(newX);
    expect(mouseY2).toEqual(newY);
  });

  it('should handle mouse movement', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);
    const { mouseX, mouseY } = map.getMapMouseCoordinates();
    expect(mouseX).toEqual(0);
    expect(mouseY).toEqual(0);
    const [newX, newY] = [100, 240];

    map.mouseMove(newX, newY, new MouseEvent('move'));
    const { mouseX: mouseX2, mouseY: mouseY2 } = map.getMapMouseCoordinates();
    expect(mouseX2).toEqual(newX);
    expect(mouseY2).toEqual(newY);
  });

  it('should handle panning with percentage', () => {
    jest.useFakeTimers();

    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);

    expect(map._drawnBBOX).toMatchObject({
      bottom: -90,
      left: -180,
      right: 180,
      top: 90,
    });

    map.mapPanPercentage(0, 0);

    jest.runOnlyPendingTimers();

    const expectedBbox = {
      bottom: -180,
      left: -180,
      right: 180,
      top: 180,
    };
    // The map bbox is adjusted
    expect(map._drawnBBOX).toMatchObject(expectedBbox);

    map.mapPanPercentage(0.1, 0.1);

    // Bbox is not updated instantly because of debounce
    expect(map._drawnBBOX).toMatchObject(expectedBbox);

    jest.runOnlyPendingTimers();

    expect(map._drawnBBOX).toMatchObject({
      bottom: -216,
      left: -216,
      right: 144,
      top: 144,
    });

    jest.useRealTimers();
  });

  it('should get correct coordinates for document', () => {
    const baseElement = document.createElement('div');
    const parentElement = document.createElement('div');
    parentElement.appendChild(baseElement);
    const map = new WMJSMap(baseElement);

    expect(
      map._getMouseCoordinatesForDocument(new MouseEvent('click')),
    ).toEqual({
      x: 0,
      y: 0,
    });

    expect(
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      map._getMouseCoordinatesForDocument({ pageX: 10, pageY: 10 }),
    ).toEqual({
      x: 10,
      y: 10,
    });

    expect(
      map._getMouseCoordinatesForDocument(
        new TouchEvent('touchstart', {
          changedTouches: [
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            {
              screenX: 0,
              screenY: 0,
            },
          ],
        }),
      ),
    ).toEqual({
      x: 0,
      y: 0,
    });

    expect(
      map._getMouseCoordinatesForDocument(
        new TouchEvent('touchstart', {
          changedTouches: [
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            { screenX: 10, screenY: 10 },
          ],
        }),
      ),
    ).toEqual({
      x: 10,
      y: 10,
    });

    // mock offset
    Object.assign(parentElement, {
      ...parentElement,
      getBoundingClientRect: () => ({
        left: 100,
        top: 100,
      }),
    });

    expect(
      map._getMouseCoordinatesForDocument(new MouseEvent('click')),
    ).toEqual({
      x: -100,
      y: -100,
    });

    const getMouseXCoordinateSpy = jest.spyOn(WMJSTools, 'getMouseXCoordinate');
    const getMouseYCoordinateSpy = jest.spyOn(WMJSTools, 'getMouseYCoordinate');
    const testEvent = new MouseEvent('click');
    map._getMouseCoordinatesForDocument(testEvent);
    expect(getMouseXCoordinateSpy).toHaveBeenCalledWith(testEvent);
    expect(getMouseYCoordinateSpy).toHaveBeenCalledWith(testEvent);
  });

  it('should get correct coordinates for element', () => {
    const baseElement = document.createElement('div');
    const parentElement = document.createElement('div');
    parentElement.appendChild(baseElement);

    const map = new WMJSMap(baseElement);

    expect(map._getMouseCoordsForElement(new MouseEvent('click'))).toEqual({
      x: 0,
      y: 0,
    });

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(map._getMouseCoordsForElement({ pageX: 10, pageY: 10 })).toEqual({
      x: 10,
      y: 10,
    });

    expect(
      map._getMouseCoordsForElement(
        new TouchEvent('touchstart', {
          changedTouches: [
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            {
              screenX: 0,
              screenY: 0,
            },
          ],
        }),
      ),
    ).toEqual({
      x: 0,
      y: 0,
    });

    expect(
      map._getMouseCoordsForElement(
        new TouchEvent('touchstart', {
          changedTouches: [
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            {
              screenX: 10,
              screenY: 10,
            },
          ],
        }),
      ),
    ).toEqual({
      x: 10,
      y: 10,
    });

    // mock offset
    Object.assign(parentElement, {
      ...parentElement,
      getBoundingClientRect: () => ({
        left: 100,
        top: 100,
      }),
    });
    expect(map._getMouseCoordsForElement(new MouseEvent('click'))).toEqual({
      x: -100,
      y: -100,
    });

    const getMouseXCoordinateSpy = jest.spyOn(WMJSTools, 'getMouseXCoordinate');
    const getMouseYCoordinateSpy = jest.spyOn(WMJSTools, 'getMouseYCoordinate');
    const testEvent = new MouseEvent('click');
    map._getMouseCoordsForElement(testEvent);
    expect(getMouseXCoordinateSpy).toHaveBeenCalledWith(testEvent);
    expect(getMouseYCoordinateSpy).toHaveBeenCalledWith(testEvent);
  });

  it('should handle layers', async () => {
    const baseElement = document.createElement('div');

    const map = new WMJSMap(baseElement);

    const testLayer1 = new WMLayer({
      ...defaultReduxLayerRadarKNMI,
      id: 'wmjsmap-testlayer-1',
      enabled: true,
    });

    const testLayer2 = new WMLayer({
      ...defaultReduxLayerRadarKNMI,
      id: 'wmjsmap-testlayer-2',
      enabled: true,
    });

    await map.addLayer(testLayer1);
    await map.addLayer(testLayer2);
    expect(map.getLayers()).toEqual([testLayer2, testLayer1]);

    map.removeAllLayers();
    expect(map.getLayers()).toEqual([]);
  });

  it('should set projection', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);
    const srs = 'EPSG:3857';
    const bbox = { left: -180, bottom: -180, right: 180, top: 180 };
    const wmbbox = new WMBBOX(bbox.left, bbox.bottom, bbox.right, bbox.top);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const defaultBboxSpy = jest.spyOn(map._defaultBBOX, 'setBBOX');
    map.setProjection(srs, wmbbox);

    expect(defaultBboxSpy).toHaveBeenCalledWith(wmbbox);
    expect(defaultBboxSpy).toHaveBeenCalledTimes(1);

    expect(map.getProjection()).toEqual({
      srs,
      bbox: expect.objectContaining(bbox),
      height: 2,
      width: 2,
    });

    // should not update defaultBBOX if srs has not changed
    map.setProjection(srs, new WMBBOX(-100, -100, 100, 100));
    expect(defaultBboxSpy).toHaveBeenCalledTimes(1);

    // should update defaultBBOX if srs has changed
    map.setProjection('EPSG:4326', new WMBBOX(100, 100, -100, -100));
    expect(defaultBboxSpy).toHaveBeenCalledTimes(2);
  });

  it('should check if projection is supported', () => {
    WMProj4Defs.forEach((proj4Definition) => {
      expect(isProjectionSupported(proj4Definition[0])).toBe(true);
    });

    expect(isProjectionSupported('ESPG:9999')).toBeFalsy();
    expect(isProjectionSupported('ESPG:1234')).toBeFalsy();
  });

  describe('setSize', () => {
    it('should set size when passing numbers', () => {
      const baseElement = document.createElement('div');
      const map = new WMJSMap(baseElement);

      const width = 234;
      const height = 567;

      map.setSize(width, height);

      const { width: mapWidth, height: mapHeight } = map.getSize();
      expect(mapWidth).toEqual(width);
      expect(mapHeight).toEqual(height);
    });

    it('should set size when passing strings', () => {
      const baseElement = document.createElement('div');
      const map = new WMJSMap(baseElement);

      const width = 234;
      const height = 567;

      map.setSize(width, height);

      const { width: mapWidth, height: mapHeight } = map.getSize();
      expect(mapWidth).toEqual(width);
      expect(mapHeight).toEqual(height);
    });

    it('should not call undefined base div', () => {
      const root = document.createElement('div');
      const baseElement = document.createElement('div');
      root.appendChild(baseElement);
      const map = new WMJSMap(baseElement);
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      map._baseDiv.style = undefined;
      map.setSize(100, 100);
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      map._baseDiv = undefined;
      map.setSize(100, 100);
    });

    it('should not call undefined main element', () => {
      const root = document.createElement('div');
      const baseElement = document.createElement('div');
      root.appendChild(baseElement);
      const map = new WMJSMap(baseElement);
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      map._mainElement.style = undefined;
      map.setSize(100, 100);
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      map.mainElment = undefined;
      map.setSize(100, 100);
    });
  });

  describe('calculateBoundingBoxAndZoom', () => {
    it('should not move the bbox if location is inside of it', () => {
      const map = new WMJSMap(document.createElement('div'));
      map.setSize(512, 256);
      const bbox = new WMBBOX({
        left: 868140,
        bottom: 8193203.25,
        right: 1609641,
        top: 8563953.75,
      });

      map.setProjection('EPSG:3857', bbox);

      const osloLatLng = { latitude: 59.9139, longitude: 10.7522 };
      map.calculateBoundingBoxAndZoom(
        osloLatLng.latitude,
        osloLatLng.longitude,
      );

      expect(
        map.getBBOX().equals(bbox.left, bbox.bottom, bbox.right, bbox.top),
      ).toBeTruthy();
      map.destroy();
    });

    it('should move the bbox if location is outside of it', () => {
      const map = new WMJSMap(document.createElement('div'));
      map.setSize(512, 256);
      const bbox = new WMBBOX({
        left: 868140,
        bottom: 8193203.25,
        right: 1609641,
        top: 8563953.75,
      });

      map.setProjection('EPSG:3857', bbox);

      const newYorkLatLng = { latitude: 40.7128, longitude: -74.006 };
      map.calculateBoundingBoxAndZoom(
        newYorkLatLng.latitude,
        newYorkLatLng.longitude,
      );

      expect(
        map.getBBOX().equals(bbox.left, bbox.bottom, bbox.right, bbox.top),
      ).toBeFalsy();
      map.destroy();
    });
  });
});
