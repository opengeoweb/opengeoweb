/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

// this type is recreated from DateInterval class (WMTime.ts), if that's changed this needs to change as well. Because of circular dependencies this file is split from WMTime
interface DateInterval {
  year: number;
  month: number;
  day: number;
  hour: number;
  minute: number;
  second: number;
  isRegularInterval: boolean;
  getTime: () => number;
}

export interface CustomDate extends Date {
  add: (dateInterval: DateInterval) => void;
  substract: (dateInterval: DateInterval) => void;
  addMultipleTimes: (dateInterval: DateInterval, times: number) => void;
  substractMultipleTimes: (dateInterval: DateInterval, times: number) => void;
  toISO8601: () => string;
  clone: () => CustomDate;
}
