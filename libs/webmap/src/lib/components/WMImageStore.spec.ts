/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import WMImage from './WMImage';
import WMImageStore, { WMImageEventType } from './WMImageStore';

describe('components/WMImageStore', () => {
  it('should initialise with correct params', () => {
    const store = new WMImageStore(100, { id: 'test' });
    expect(store._maxNumberOfImages).toEqual(100);
    expect(store._options.id).toEqual('test');
  });

  it('should return image if src exists', () => {
    const src = 'https://testurl.com';
    const src2 = 'https://anotherurl.com';
    const store = new WMImageStore(1, { id: 'test' });
    expect(store.getImageForSrc(src)).toEqual(undefined);
    expect(store.getImage('')).toEqual(null);
    expect(store.getImage(src)).toEqual(expect.any(WMImage));
    expect(store.getImageForSrc(src)).toEqual(expect.any(WMImage));
    store.getImage(src2);
    // first one should be removed because maxNumberOfImages was set to 1
    expect(store.getImageForSrc(src)).toEqual(undefined);
    expect(store.getImageForSrc(src2)).toEqual(expect.any(WMImage));
  });

  it('should re-initialize the images on clear', async () => {
    const src = 'https://testurl.com';
    const store = new WMImageStore(1, { id: 'test' });
    expect(store.getImageForSrc(src)).toEqual(undefined);
    store.getImage(src);
    void store.load(src);
    expect(store.getImage(src).isLoading()).toEqual(true);
    expect(store.getNumImagesLoading()).toEqual(1);
    store.clear();
    expect(store.getImageForSrc(src)).toBeUndefined();
    expect(store.getNumImagesLoading()).toEqual(0);
  });

  it('should trigger the image callback', () => {
    const src = 'https://testservice/';
    const callback = jest.fn();
    const image = new WMImage(src, callback);
    const store = new WMImageStore(1, { id: 'test' });
    store.addImageEventCallback(callback, 'image1');
    store.imageLoadEventCallback(image, WMImageEventType.Loaded);
    expect(callback).toHaveBeenCalled();
  });

  it('should handle updates to the callback list', () => {
    const callback1 = jest.fn();
    const callback2 = jest.fn();
    const store = new WMImageStore(1, { id: 'test' });
    store.addImageEventCallback(callback1, null!);
    expect(store._loadEventCallbackList).toHaveLength(0);
    store.addImageEventCallback(null!, 'image1');
    expect(store._loadEventCallbackList).toHaveLength(0);
    store.addImageEventCallback(callback1, 'image1');
    store.addImageEventCallback(callback2, 'image2');
    expect(store._loadEventCallbackList).toHaveLength(2);
    store.removeEventCallback('image1');
    expect(store._loadEventCallbackList).toHaveLength(1);
    expect(store._loadEventCallbackList[0].id).toEqual('image2');
    store.removeAllEventCallbacks();
    expect(store._loadEventCallbackList).toHaveLength(0);
  });

  it('should trigger both image callback for images with the same url', async () => {
    const store = new WMImageStore(10, { id: 'test' });
    const src = 'https://testservice/';

    const a = store.getImage(src);
    const b = store.getImage(src);
    const loadSpyA = jest.spyOn(a, '_load').mockImplementation(() => {
      a._loadEvent(false);
    });
    const loadSpyB = jest.spyOn(b, '_load').mockImplementation(() => {
      b._loadEvent(false);
    });

    expect(a.isLoaded()).toBe(false);
    expect(b.isLoaded()).toBe(false);

    try {
      await a.loadAwait();
    } catch (e) {
      expect(e).toBeTruthy();
    }
    try {
      await b.loadAwait();
    } catch (e) {
      expect(e).toBeTruthy();
    }

    expect(loadSpyA).toHaveBeenCalledTimes(2);
    expect(loadSpyB).toHaveBeenCalledTimes(2);
    expect(a.isLoaded()).toBe(true);
    expect(b.isLoaded()).toBe(true);
  });

  it('should not register a GeoReferencedImage if source is missing', async () => {
    const store = new WMImageStore(10, { id: 'test' });
    const image = store.getGeoReferencedImage(null!, {
      extent: [0, 50, 10, 60],
      resolution: 5,
    });
    expect(image).toBeNull();
  });

  it('should not register a GeoReferencedImage if source has invalid url', async () => {
    const store = new WMImageStore(10, { id: 'test' });
    const image = store.getGeoReferencedImage(
      'a=1&BBOX=0,50,10,60&WIDTH=1000&HEIGHT=1000&LAYER=TEST',
      {
        extent: [0, 50, 10, 60],
        resolution: 5,
      },
    );
    expect(image).toBeNull();
  });

  it('should register a GeoReferencedImage without errors', async () => {
    const store = new WMImageStore(10, { id: 'test' });
    const imageUrlToCheck =
      'http://test/bla?a=1&BBOX=0,50,10,60&WIDTH=1000&HEIGHT=1000&LAYER=TEST';
    const image = store.getGeoReferencedImage(imageUrlToCheck, {
      extent: [0, 50, 10, 60],
      resolution: 5,
    });
    const imageLoadSpy = jest.spyOn(image, '_load').mockImplementation(() => {
      image._loadEvent(false);
    });
    try {
      await image.loadAwait();
    } catch (e) {
      expect(e).toBeTruthy();
    }
    expect(imageLoadSpy).toHaveBeenCalledTimes(1);
    expect(image.hashUrl).toEqual('http://test/bla?a=1&LAYER=TEST');
    expect(image.isLoaded()).toBeTruthy();
    expect(image.geoReference).toEqual({
      extent: [0, 50, 10, 60],
      resolution: 5,
    });

    const image2 = store.getGeoReferencedImage(imageUrlToCheck, {
      extent: [0, 50, 10, 60],
      resolution: 5,
    });
    // Should be the same object
    expect(image2).toBe(image);
  });

  it('should get closest image when using getAltGeoReferencedImage', async () => {
    const store = new WMImageStore(10, { id: 'test' });

    const addImage = async (
      imageUrlToCheck: string,
      extent: number[],
    ): Promise<WMImage> => {
      const image = store.getGeoReferencedImage(imageUrlToCheck, {
        extent: [...extent],
        resolution: 5,
      });
      jest.spyOn(image, '_load').mockImplementation(() => {
        image._loadEvent(false);
      });
      await image.loadAwait();
      return image;
    };

    // Images with ELEVATION=1 are a group, and images with ELEVATION=2 are another group
    const images = [
      {
        url: 'http://test/bla?a=1&BBOX=0,50,10,60&WIDTH=1000&HEIGHT=1000&LAYER=TEST&ELEVATION=1',
        extent: [0, 50, 10, 60],
      },
      {
        url: 'http://test/bla?a=1&BBOX=0,50,10,60&WIDTH=1000&HEIGHT=1000&LAYER=TEST&ELEVATION=2',
        extent: [0, 50, 10, 60],
      },
      {
        url: 'http://test/bla?a=1&BBOX=4,55,8,58&WIDTH=1000&HEIGHT=1000&LAYER=TEST&ELEVATION=1',
        extent: [4, 54, 8, 58],
      },
      {
        url: 'http://test/bla?a=1&BBOX=5,54,6,55&WIDTH=1000&HEIGHT=1000&LAYER=TEST&ELEVATION=1',
        extent: [5, 54, 6, 55],
      },
      {
        url: 'http://test/bla?a=1&BBOX=5,54,6,55&WIDTH=1000&HEIGHT=1000&LAYER=TEST&ELEVATION=2',
        extent: [5, 54, 6, 55],
      },
    ];

    await addImage(images[0].url, images[0].extent);
    const imageWithElevationIs2 = await addImage(
      images[1].url,
      images[1].extent,
    );
    await addImage(images[2].url, images[2].extent);
    const imageWithElevationIs1 = await addImage(
      images[3].url,
      images[3].extent,
    );

    // Currently the most recent image for the same image url is returned, not yet based on its extent.
    const altImageAWithElevationIs1 = store.getAltGeoReferencedImage(
      images[0].url,
      [0, 50, 10, 60],
      5,
    );
    expect(altImageAWithElevationIs1).toBe(imageWithElevationIs1);

    const altImageBWithElevationIs2 = store.getAltGeoReferencedImage(
      images[1].url,
      [0, 50, 10, 60],
      5,
    );
    expect(altImageBWithElevationIs2).toBe(imageWithElevationIs2);
  });

  it('getAltGeoReferencedImage should return falsy if invalid source is given', async () => {
    const store = new WMImageStore(10, { id: 'test' });
    const altImage = store.getAltGeoReferencedImage(null!, [0, 50, 10, 60], 5);
    expect(altImage).toBeFalsy();
  });

  it('getAltGeoReferencedImage should return falsy if not yet registered image is requested', async () => {
    const store = new WMImageStore(10, { id: 'test' });
    const altImage = store.getAltGeoReferencedImage(
      'http://test/bla?a=1&BBOX=0,50,10,60&WIDTH=1000&HEIGHT=1000&LAYER=TEST&TIME=2',
      [0, 50, 10, 60],
      5,
    );
    expect(altImage).toBeFalsy();
  });
});
