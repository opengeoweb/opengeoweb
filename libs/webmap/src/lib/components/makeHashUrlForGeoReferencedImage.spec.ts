/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { makeHashUrlForGeoReferencedImage } from './makeHashUrlForGeoReferencedImage';

describe('components/makeHashUrlForGeoReferencedImage', () => {
  it('should make hashed georeferenced url without width, height and bbox', () => {
    const a = makeHashUrlForGeoReferencedImage(
      'http://adaguc-server-geoweb.geoweb.knmi.cloud:8080/adaguc-server?DATASET=SAT&SERVICE=WMS&&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&LAYERS=output_3km_dust&WIDTH=475&HEIGHT=892&SRS=EPSG%3A32661&BBOX=-3488964.2796964603,-11004046.949538987,5484118.042808909,5846457.117144777&STYLES=rgba%2Frgba&FORMAT=image/png&TRANSPARENT=TRUE&&TIME=2025-01-20T10%3A00%3A00Z',
    );
    expect(a).toBe(
      'http://adaguc-server-geoweb.geoweb.knmi.cloud:8080/adaguc-server?DATASET=SAT&SERVICE=WMS&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&LAYERS=output_3km_dust&SRS=EPSG%3A32661&STYLES=rgba%2Frgba&FORMAT=image%2Fpng&TRANSPARENT=TRUE&TIME=2025-01-20T10%3A00%3A00Z',
    );
  });

  it('should return undefined if url is invalid', () => {
    expect(
      makeHashUrlForGeoReferencedImage(
        'DATASET=SAT&SERVICE=WMS&&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&LAYERS=output_3km_dust&WIDTH=475&HEIGHT=892&SRS=EPSG%3A32661&BBOX=-3488964.2796964603,-11004046.949538987,5484118.042808909,5846457.117144777&STYLES=rgba%2Frgba&FORMAT=image/png&TRANSPARENT=TRUE&&TIME=2025-01-20T10%3A00%3A00Z',
      ),
    ).toBeUndefined();
  });

  it('should throw an exception undefined', () => {
    expect(makeHashUrlForGeoReferencedImage(undefined!)).toBeUndefined();
  });
});
