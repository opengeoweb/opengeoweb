/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { WMJSMap } from '.';
import WMJSAnimate from './WMAnimate';

describe('components/WMAnimate', () => {
  it('should not be animating as default', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);
    expect(map.isAnimating).toBeFalsy();
  });

  it('should stop animating when triggered', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);
    map.isAnimating = true;
    map.animationList = [
      { name: 'n1', value: new Date().toISOString(), requests: [] },
      { name: 'n2', value: new Date().toISOString(), requests: [] },
    ];

    map.stopAnimating();
    expect(map.isAnimating).toBeFalsy();
  });

  it('should find closest animation step when closest is before', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);
    map.isAnimating = true;
    map.currentAnimationStep = 0;
    const now = new Date();
    const before = new Date(now.getTime() - 200000);
    const after = new Date(now.getTime() + 300000);
    const anim = new WMJSAnimate(map);
    map.animationList = [
      { name: 'n1', value: before.toISOString(), requests: [] },
      { name: 'n2', value: after.toISOString(), requests: [] },
    ];
    expect(anim.isCurrentAnimationTimeCloseToWallClockTime()).toBeTruthy();
  });

  it('should find closest animation step when closest is after', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);
    map.isAnimating = true;
    map.currentAnimationStep = 0;
    const now = new Date();
    const before = new Date(now.getTime() - 500000);
    const after = new Date(now.getTime() + 100000);
    const anim = new WMJSAnimate(map);
    map.animationList = [
      { name: 'n1', value: before.toISOString(), requests: [] },
      { name: 'n2', value: after.toISOString(), requests: [] },
    ];

    expect(anim.isCurrentAnimationTimeCloseToWallClockTime()).toBeFalsy();
  });

  it('should not find animation step when current time is not between ', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);
    map.isAnimating = true;
    map.currentAnimationStep = 0;
    const now = new Date();
    const before = new Date(now.getTime() - 500000);
    const after = new Date(now.getTime() - 10000);
    const anim = new WMJSAnimate(map);
    map.animationList = [
      { name: 'n1', value: before.toISOString(), requests: [] },
      { name: 'n2', value: after.toISOString(), requests: [] },
    ];
    expect(anim.isCurrentAnimationTimeCloseToWallClockTime()).toBeFalsy();
  });

  it('should set isCurrentAnimationTimeCloseToWallClockTime() to false when not between timesteps', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);
    map.isAnimating = true;
    map.currentAnimationStep = 0;
    const now = new Date();
    const before = new Date(now.getTime() - 500000);
    const after = new Date(now.getTime() - 10000);
    const anim = new WMJSAnimate(map);
    map.animationList = [
      { name: 'n1', value: before.toISOString(), requests: [] },
      { name: 'n2', value: after.toISOString(), requests: [] },
    ];

    expect(anim.isCurrentAnimationTimeCloseToWallClockTime()).toBeFalsy();
  });
});
