/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import WMImageStore from './WMImageStore';
import { Bbox } from './WMBBOX';
import WMListener from './WMListener';
import { CanvasBBOX, CanvasGeoLayer } from './WMCanvasBuffer';
import WMJSDimension from './WMJSDimension';
import MapPin from './WMMapPin';
import WMLayer from './WMLayer';
import { AnimationStep, IWMJSMapMouseCoordinates, IWMProj4 } from './types';
import { WMMemo } from './WMMemo';
import type { TileServerSettings, TileServerDefinition } from '../utils';
import { WMProjectionWH } from './WMProjection';

/**
 * *
 * IWMJSMap interface
 */
interface IWMJSMap {
  prefetchLayerImagesForTimeMemo: WMMemo;
  shouldPrefetch: boolean;
  mapPin: MapPin;
  mapdimensions: WMJSDimension[];
  timestepInMinutes: number | undefined;
  showLayerInfo: boolean;
  stopAnimating: () => void;
  isAnimating: boolean;
  animationDelay: number;
  animationList: AnimationStep[] | string | undefined;
  currentAnimationStep: number;
  initialAnimationStep: number;
  setAnimationDelay: (delay: number) => void;
  isDestroyed: boolean;
  getMapImageStore: WMImageStore;
  holdShiftToScroll: boolean;

  /**
   * getMapMouseCoordinates returns the coordinates of the mouse in pixels for the map
   * @returns The coordinates of the mouse in pixels for the map
   */
  getMapMouseCoordinates: () => IWMJSMapMouseCoordinates;
  /**
   * Trigger a mouseUp for the map
   * @param mouseCoordX
   * @param mouseCoordY
   * @param e
   */
  mouseUp(mouseCoordX: number, mouseCoordY: number, e?: MouseEvent): void;
  /**
   * Trigger a mouseMove for the map
   * @param mouseCoordX
   * @param mouseCoordY
   * @param e
   */
  mouseMove(mouseCoordX: number, mouseCoordY: number, e?: MouseEvent): void;
  /**
   * Trigger a mousedown for the map
   * @param mouseCoordX
   * @param mouseCoordY
   * @param e
   */
  mouseDown(mouseCoordX: number, mouseCoordY: number, e?: MouseEvent): void;

  /**
   * Set a list with tileservers to be used within the map instance
   * @param tileRenderSettings
   */
  setWMTileRendererTileSettings(tileRenderSettings: TileServerSettings): void;

  /**
   * Add one tileserver to the existing set of tileservers to the map instance
   * @param tileRenderSetting
   */
  addWMTileRendererTileSetting(tileRenderSetting: TileServerDefinition): void;

  /**
   * Displays a message in the map
   * @param message
   */
  setMessage(message: string): void;
  /**
   * Displays a message in the map, meant to print a time offset in the map (used with multimapviews)
   * @param message
   */
  setTimeOffset(message: string): void;

  /**
   * Displays the scalebar in the map
   * @param display
   */
  displayScaleBarInMap(display: boolean): void;

  /**
   * Do not show mouse cursor coordinates in the map
   */
  hideMouseCursorProperties(): void;

  /**
   * Get all layers currently in the map (except the baselayer and overlayer)
   * @param reverseOrder True to return them in reversed order
   */
  getLayers(reverseOrder?: boolean): WMLayer[];
  /**
   * Check if this layer is present in the map
   * @param layer
   */
  hasLayer(layer: WMLayer): boolean;
  /**
   * Set an existing layer for the map, usually used to change the name of the layer
   * @param layer
   */
  setLayer(layer: WMLayer): Promise<IWMJSMap>;
  /**
   * Displays or hides a given layer
   * @param layer
   * @param enabled
   */
  displayLayer(layer: WMLayer, enabled: boolean): void;
  /**
   * Remove all layers (except baselayers and overlayers)
   */
  removeAllLayers(): void;
  /**
   * Delete this specific layer
   * @param layerToDelete
   */
  deleteLayer(layerToDelete: WMLayer): void;
  /**
   * @param layer of type WMLayer
   */
  addLayer(layer: WMLayer): Promise<IWMJSMap>;
  /**
   * Zoom to given layer
   * @param _layer
   */
  zoomToLayer(_layer: WMLayer): void;
  /**
   * Get the current active layer.
   */
  getActiveLayer(): WMLayer;

  /**
   * Reorder layers according to an index array with indices on how the layers should be reordered.
   */
  reorderLayers(order: number[]): boolean;

  /**
   * Set the projection of the current webmap object
   * _srs also accepts a projectionProperty object
   */
  setProjection(_srs: string, _bbox?: Bbox): void;

  /**
   * Returns the current boundingbox, projection, width and height of the map
   */
  getProjection(): WMProjectionWH;

  /**
   * Set the default boundingbox, used when pressing the home button in the map.
   * @param bbox
   */
  setDefaultBBOX(bbox: Bbox): void;

  /**
   * Change the current boundingbox of the map
   * @param bbox
   */
  setBBOX(bbox: Bbox): boolean;

  /**
   * Returns the current boundingbox of the map, updated with the extent for which WMS layers are loaded
   */
  getBBOX(): Bbox;

  /**
   * Get the current intermediate boundingbox, it is immediately updated when panning or zooming the map. This one is not the one which is used for WMS requests. It can be used to update drawings in the canvas
   */
  getDrawBBOX(): Bbox;

  /**
   * Get the width and height in pixels of the map
   */
  getSize(): { width: number; height: number };

  /**
   * Change the size of the map in pixels, e.g. when container is resized.s
   * @param w
   * @param h
   */
  setSize(w: number, h: number): void;

  /**
   * Zoomout given ratio
   * @param ratio 1.0, means zoomout using full boundingbox width, 0.1 means zoom out 10% of full boundingbox width
   */
  zoomOut(ratio?: number): void;
  /**
   * Zoomin given ratio
   * @param ratio 1.0, meane zoomin using full boundingbox width, 0.1 means zoom in to 10% of full boundingbox width
   */
  zoomIn(ratio?: number): void;

  /**
   * Zooms the map to the specified bounding box
   * @param _newbbox
   */
  zoomTo(_newbbox: Bbox): void;

  /**
   * Displays the legend of the layer inside the map
   * @param _displayLegendInMap
   */
  displayLegendInMap(_displayLegendInMap: boolean): void;

  /**
   * Tell the map to load layers and draw again
   * @param animationList
   */
  draw(animationList?: AnimationStep[] | string | undefined): void;

  /**
   * Just refresh the drawing buffer, useful when opacity of a layer is changed.
   */
  redrawBuffer(): void;

  /**
   * Set the baselayers for the map
   * @param layers
   */
  setBaseLayers(layers: WMLayer[]): void;

  /**
   * Return a list of baselayers from the map
   */
  getBaseLayers(): WMLayer[];

  /**
   * Returns how many layers the map currently has (baselayers and overlays not included)
   */
  getNumLayers(): number;

  /**
   * Returns the HTML base element used for the map
   */
  getBaseElement(): HTMLElement;

  /**
   * Destroys the map, useful when a component container unmounts.
   */
  destroy(): void;

  /**
   * Pan the map with a given percentage (used for keyboard controls)
   * @param percentageDiffX
   * @param percentageDiffY
   */
  mapPanPercentage(percentageDiffX: number, percentageDiffY: number): void;
  /**
   * Changes the cursor icon of the map
   * @param cursor 'move', 'pointer' or '' ( for default)
   */
  setCursor(cursor?: string): void;

  /**
   * Returns the id of this map
   */
  getId(): string;

  /**
   * Returns proj4 instance of the map
   */
  getProj4(): IWMProj4;

  /**
   * Zoom in to specified location
   * @param lat
   * @param lng
   */
  calculateBoundingBoxAndZoom(lat: number, lng: number): void;

  /**
   * Add something to the event listener
   * @param name
   * @param f
   * @param keep
   */
  addListener<T>(
    name: string,
    f: (param: T) => void | boolean,
    keep?: boolean,
  ): boolean;
  /**
   * Remove something from the event listener
   * @param name
   * @param f
   */
  removeListener<T>(name: string, f: (param: T) => void): void;
  /**
   * Get a direct handle to the event listener system
   */
  getListener(): WMListener;
  /**
   * Suspend a specific event
   * @param name
   */
  suspendEvent(name: string): void;
  /**
   * Resume a specific event
   * @param name
   */
  resumeEvent(name: string): void;

  /**
   * Returns a list of all used map dimensions
   */
  getDimensionList(): WMJSDimension[];

  /**
   * Get a specific map dimension
   * @param name
   */
  getDimension(name: string): WMJSDimension | undefined;

  /**
   * Set a specific map dimension
   * @param name
   * @param value
   * @param triggerEvent
   * @param adjustLayerDims
   */
  setDimension(
    name: string,
    value: string,
    triggerEvent?: boolean,
    adjustLayerDims?: boolean,
  ): void;

  /**
   * Get a handle to the imagestore
   */
  getImageStore(): WMImageStore;

  /**
   * Tell the imagestore to forget all its images
   */
  clearImageCache(): void;

  /**
   * Handle to the map pin
   */
  getMapPin(): MapPin;

  /**
   * For a given WMS GetMap imageUrl, return an alternative image which is directly available for display.
   * @param imageUrl
   * @param bbox
   * @param filterBad
   */
  getAlternativeImage(
    imageUrl: string,
    bbox: CanvasBBOX,
    filterBad: boolean,
  ): CanvasGeoLayer[];

  configureMapDimensions(layer: WMLayer): void;

  detachWheelEvent(): void;
  attachWheelEvent(): void;
}

export default IWMJSMap;
