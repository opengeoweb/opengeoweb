/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import WMListener from './WMListener';

describe('components/WMListener', () => {
  describe('WMListener', () => {
    it('should register new event and trigger it once', () => {
      const listener = new WMListener();
      const eventA = jest.fn();
      listener.addEventListener('test', eventA, false);
      expect(eventA).not.toHaveBeenCalled();
      listener.triggerEvent('anotherone', 'info');
      expect(eventA).not.toHaveBeenCalled();
      listener.triggerEvent('test', 'info');
      expect(eventA).toHaveBeenCalledTimes(1);
      expect(eventA).toHaveBeenCalledWith('info');
      listener.triggerEvent('test', 'nextinfo');
      expect(eventA).toHaveBeenCalledTimes(1);
      expect(eventA).toHaveBeenCalledWith('info');
    });
    it('should register new event and trigger it always when keep is true', () => {
      const listener = new WMListener();
      const eventA = jest.fn();
      listener.addEventListener('test', eventA, true);
      expect(eventA).not.toHaveBeenCalled();
      listener.triggerEvent('anotherone', 'info');
      expect(eventA).not.toHaveBeenCalled();
      listener.triggerEvent('test', 'info');
      expect(eventA).toHaveBeenCalledTimes(1);
      expect(eventA).toHaveBeenCalledWith('info');
      listener.triggerEvent('test', 'nextinfo');
      expect(eventA).toHaveBeenCalledTimes(2);
      expect(eventA).toHaveBeenCalledWith('nextinfo');
    });
    it('should register same events once with multiple functions', () => {
      const listener = new WMListener();
      const eventA = jest.fn();
      const eventB = jest.fn();
      listener.addEventListener('test', eventA, false);
      listener.addEventListener('test', eventB, false);
      listener.triggerEvent('test');
      expect(eventA).toHaveBeenCalledTimes(1);
      expect(eventB).toHaveBeenCalledTimes(1);
      listener.triggerEvent('test');
      expect(eventA).toHaveBeenCalledTimes(1);
      expect(eventB).toHaveBeenCalledTimes(1);
    });
    it('should register same event and same function once', () => {
      const listener = new WMListener();
      const eventA = jest.fn();
      const eventB = jest.fn();
      listener.addEventListener('test', eventA, false);
      listener.addEventListener('test', eventA, false);
      listener.addEventListener('test', eventB, false);
      listener.addEventListener('test', eventB, false);
      listener.triggerEvent('test');
      expect(eventA).toHaveBeenCalledTimes(1);
      expect(eventB).toHaveBeenCalledTimes(1);
      listener.triggerEvent('test');
      expect(eventA).toHaveBeenCalledTimes(1);
      expect(eventB).toHaveBeenCalledTimes(1);
    });
    it('should deregister a registered event', () => {
      const listener = new WMListener();
      const eventA = jest.fn();
      listener.addEventListener('test', eventA, true);
      expect(eventA).not.toHaveBeenCalled();
      listener.triggerEvent('test', 'infoA');
      expect(eventA).toHaveBeenCalledWith('infoA');
      expect(eventA).toHaveBeenCalledTimes(1);
      listener.triggerEvent('test', 'infoB');
      expect(eventA).toHaveBeenCalledTimes(2);
      expect(listener.removeEventListener('test', eventA)).toBe(1);
      listener.triggerEvent('test', 'infoC');
      expect(eventA).toHaveBeenCalledTimes(2);
    });

    it('should deregister multiple registered events', () => {
      const listener = new WMListener();
      const eventA = jest.fn();
      const eventB = jest.fn();
      listener.addEventListener('test', eventA, true);
      listener.addEventListener('test', eventB, true);
      expect(listener.removeEventListeners('test')).toBe(2);
    });

    it('should deregister only registered events with the same function callback', () => {
      const listener = new WMListener();
      const eventA = jest.fn();
      const eventB = jest.fn();
      listener.addEventListener('test', eventA, true);
      listener.addEventListener('test', eventB, true);
      expect(listener.removeEventListener('test', eventA)).toBe(1);
    });

    it('should return proper results from registered functions', () => {
      const listener = new WMListener();
      const eventA = (): string => {
        return 'hello from A';
      };
      const eventB = (): string => {
        return 'hello from B';
      };
      listener.addEventListener('test', eventA);
      listener.addEventListener('test', eventB);
      const result = listener.triggerEvent('test');

      expect(result).toEqual(['hello from A', 'hello from B']);
    });

    it('should not trigger this event once it is suspended', () => {
      const listener = new WMListener();
      const eventA = jest.fn();
      const eventB = jest.fn();
      listener.addEventListener('eventA', eventA, true);
      listener.addEventListener('eventB', eventB, true);
      listener.suspendEvent('eventA');
      listener.triggerEvent('eventA');
      listener.triggerEvent('eventB');
      expect(eventA).toHaveBeenCalledTimes(0);
      expect(eventB).toHaveBeenCalledTimes(1);
    });

    it('should resume events once it was suspended', () => {
      const listener = new WMListener();
      const eventA = jest.fn();
      const eventB = jest.fn();
      listener.addEventListener('eventA', eventA, true);
      listener.addEventListener('eventB', eventB, true);
      listener.suspendEvent('eventA');
      listener.triggerEvent('eventA');
      listener.triggerEvent('eventB');
      listener.resumeEvent('eventA');
      listener.triggerEvent('eventA');
      listener.triggerEvent('eventB');
      expect(eventA).toHaveBeenCalledTimes(1);
      expect(eventB).toHaveBeenCalledTimes(2);
    });

    it('should suspend all events once it was suspended', () => {
      const listener = new WMListener();
      const eventA = jest.fn();
      const eventB = jest.fn();
      listener.addEventListener('eventA', eventA, true);
      listener.addEventListener('eventB', eventB, true);
      listener.suspendEvents();
      listener.triggerEvent('eventA');
      listener.triggerEvent('eventB');
      expect(eventA).toHaveBeenCalledTimes(0);
      expect(eventB).toHaveBeenCalledTimes(0);
      listener.resumeEvents();
      listener.triggerEvent('eventA');
      listener.triggerEvent('eventB');
      expect(eventA).toHaveBeenCalledTimes(1);
      expect(eventB).toHaveBeenCalledTimes(1);
    });
  });
});
