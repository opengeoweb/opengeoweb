/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { WMBBOX } from '.';
import WMFlyToBBox from './WMFlyToBBox';
import type WMJSMap from './WMJSMap';

describe('WMFlyToBBox', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should start zoom to fly ', () => {
    const testBBox = new WMBBOX({
      left: 10,
      right: 10,
      bottom: 10,
      top: 10,
    });
    const map = {
      _updateBoundingBox: jest.fn(),
      updateBBOX: { setBBOX: jest.fn() },
      drawnBBOX: { setBBOX: jest.fn() },
      draw: jest.fn(),
    };
    const flyToBBox = new WMFlyToBBox(map as unknown as WMJSMap);
    const spy = jest.spyOn(flyToBBox, 'flyZoomToBBOXTimerFunc');
    expect(spy).not.toHaveBeenCalled();
    flyToBBox.flyZoomToBBOXStartZoom(testBBox, testBBox);
    expect(spy).toHaveBeenCalled();
    spy.mockReset();
    // should not be called when busy
    flyToBBox.flyZoomToBBOXStartZoom(testBBox, testBBox);
    expect(spy).not.toHaveBeenCalled();
  });

  it('should stop zoom to fly ', () => {
    const map = {
      setBBOX: jest.fn(),
    };
    const flyToBBox = new WMFlyToBBox(map as unknown as WMJSMap);
    const spy = jest.spyOn(flyToBBox, 'resetBBoxTimer');

    flyToBBox.flyZoomToBBOXStop();

    expect(map.setBBOX).toHaveBeenCalledWith(flyToBBox.flyZoomToBBOXFly);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(flyToBBox.flyZoomToBBOXTimerFuncBusyAndContinue).toEqual(0);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(flyToBBox.flyZoomToBBOXTimerFuncBusy).toEqual(0);
    expect(spy).toHaveBeenCalled();
  });

  it('should debounce zoom', () => {
    const testBBox = new WMBBOX({
      left: 10,
      right: 10,
      bottom: 10,
      top: 10,
    });
    const map = {
      _updateBoundingBox: jest.fn(),
      _updateBBOX: { setBBOX: jest.fn() },
      _drawnBBOX: { setBBOX: jest.fn() },
      draw: jest.fn(),
    };
    const flyToBBox = new WMFlyToBBox(map as unknown as WMJSMap);

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const spy = jest.spyOn(flyToBBox, 'flyZoomToBBOXDebounced');
    const spy2 = jest.spyOn(flyToBBox, 'flyZoomToBBOXTimerFuncReadyDebounced');

    expect(spy).toHaveBeenCalledTimes(0);

    flyToBBox.flyZoomToBBOXStartZoom(testBBox, testBBox);
    expect(spy).toHaveBeenCalledTimes(1);

    // simulate a bunch of scrolls
    new Array(10).fill(0).forEach(() => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const hasDebounce = flyToBBox.flyZoomToBBOXTimerLoop === 5;
      spy2.mockReset();
      flyToBBox.flyZoomToBBOXTimerFunc();
      expect(spy).toHaveBeenCalled();

      if (hasDebounce) {
        expect(spy2).toHaveBeenCalled();
        expect(map.draw).toHaveBeenCalledWith('flyZoomToBBOXTimerFunc');
      } else {
        expect(spy2).not.toHaveBeenCalled();
      }
    });
  });
});
