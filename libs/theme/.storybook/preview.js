/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Box } from '@mui/material';
import '../../../libs/theme/src/lib/components/Theme/styles.css';
import { ThemeWrapper } from '../src/lib/components/Theme/ThemeContext';
import { darkTheme, lightTheme } from '../src/lib/utils/themes';
import { parameters } from '../../../.storybook/preview';

export const decorators = [
  (Story, params) => {
    const theme = params.tags.indexOf('dark') !== -1 ? darkTheme : lightTheme;
    const shouldDisableWrapper =
      params.tags.indexOf('fullscreen-snapshot') !== -1 ? true : false;

    return (
      <ThemeWrapper theme={theme} shouldDisableWrapper={shouldDisableWrapper}>
        {shouldDisableWrapper ? <Story /> : <Box>{<Story />}</Box>}
      </ThemeWrapper>
    );
  },
];

export { parameters };
export default {
  parameters: parameters,
};
