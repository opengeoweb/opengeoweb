/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Grid2 as Grid, Paper, Typography, useTheme } from '@mui/material';
import type { StoryObj } from '@storybook/react';
import { CSSProperties, GeowebColorPalette } from '../types';

type Story = StoryObj<typeof Typography>;

export default {
  title: 'demo/Colors',
};

const style = {
  chapterTitle: {
    textTransform: 'capitalize',
    fontSize: '1.5rem',
    marginBottom: '10px',
  },
  paper: {
    marginBottom: '50px',
    marginRight: '80px',
  },
  colorBox: {
    height: '80px',
    width: '80px',
  },
  colorTitle: {
    textTransform: 'capitalize',
  },
};

interface ColorObject {
  name: string;
  fill?: string;
  color?: string;
}

const camelCaseToSeparated = (text: string): string =>
  text
    .split(/(?=[A-Z])/)
    .map((s) => s.toLowerCase())
    .join(' ');

const createListFromColorGeowebColorPalette = (
  colors: GeowebColorPalette,
): { chapterTitle: string; colorList: ColorObject[] }[] => {
  return Object.keys(colors).map((chapterTitle) => {
    const definedColors = colors[chapterTitle as keyof GeowebColorPalette];

    const colorList = Object.keys(definedColors!).reduce<ColorObject[]>(
      (list, name) => {
        const value = definedColors![name as keyof typeof definedColors];

        // string values
        if (typeof value === 'string') {
          return list.concat({
            name: camelCaseToSeparated(name),
            fill: value,
          });
        }

        // 3 level deep properties (for example buttons)
        const deepProps = Object.keys(value);
        const isDeep = typeof value[deepProps[0]] === 'object';
        if (isDeep) {
          return list.concat(
            deepProps.map((propName) => ({
              name: camelCaseToSeparated(`${name}:${propName}`),
              ...(value[propName] as CSSProperties),
            })),
          );
        }

        // 2 level deep props
        return list.concat({
          name: camelCaseToSeparated(name),
          ...(value as CSSProperties),
        });
      },
      [],
    );

    return {
      chapterTitle: camelCaseToSeparated(chapterTitle),
      colorList,
    };
  });
};

interface ColorsDemoProps {
  showSourceCode?: boolean;
}

const ColorsDemo: React.FC<ColorsDemoProps> = ({ showSourceCode = true }) => {
  const {
    palette: { geowebColors },
  } = useTheme();
  const definedColors = createListFromColorGeowebColorPalette(geowebColors);
  return (
    <Grid container>
      <Grid size={showSourceCode ? 9 : 12}>
        {definedColors.map(({ chapterTitle, colorList }) => (
          <Grid key={chapterTitle}>
            <Grid size={12}>
              <Typography sx={style.chapterTitle} variant="h2">
                {chapterTitle}
              </Typography>
            </Grid>
            <Grid size={12}>
              <Grid container>
                {colorList.map(({ fill, color, name, ...otherProps }) => (
                  <Grid sx={style.paper} key={name}>
                    <Paper
                      elevation={0}
                      sx={style.colorBox}
                      style={{ backgroundColor: fill || color }}
                    />
                    <Typography sx={style.colorTitle}>{name}</Typography>
                    <Typography>{fill}</Typography>
                    {Object.keys(otherProps).map((keyName) => (
                      <Typography key={keyName}>
                        {`${keyName}: ${otherProps[keyName as keyof typeof otherProps]}`}
                      </Typography>
                    ))}
                  </Grid>
                ))}
              </Grid>
            </Grid>
          </Grid>
        ))}
      </Grid>
      {showSourceCode && (
        <Grid sx={{ overflowX: 'hidden' }} size={3}>
          <pre>{JSON.stringify(geowebColors, null, 2)}</pre>
        </Grid>
      )}
    </Grid>
  );
};

export const ColorsLight: Story = {
  render: () => <ColorsDemo />,
  tags: ['snapshot'],
  parameters: {
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf84d920b99428d7f98555',
      },
    ],
    layout: 'fullscreen',
  },
};
ColorsLight.storyName = 'Colors light theme';

export const ColorsDark: Story = {
  render: () => <ColorsDemo />,
  tags: ['snapshot', 'dark'],
  parameters: {
    zeplinkLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e5dc12278e35d433aa2d',
      },
    ],
    layout: 'fullscreen',
  },
};
ColorsDark.storyName = 'Colors dark theme';
