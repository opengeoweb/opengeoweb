/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import {
  Typography,
  Box,
  Select as MuiSelect,
  BaseSelectProps,
  Paper,
  MenuItem,
  FormControl,
  InputLabel,
  SelectChangeEvent,
} from '@mui/material';
import type { StoryObj } from '@storybook/react';
import { StoryHeader } from '../components/Storybook';

type Story = StoryObj<typeof MuiSelect>;

export default {
  title: 'demo/Select',
};

// wrapper for state
const Select: React.FC<BaseSelectProps> = ({
  label,
  value,
  error,
  ...props
}) => {
  const [newValue, setValue] = React.useState(value);
  const handleChange = (event: SelectChangeEvent<unknown>): void => {
    setValue(event.target.value);
  };

  return (
    <FormControl variant="filled" sx={{ marginRight: '10px' }} error={error}>
      <InputLabel>{label}</InputLabel>
      <MuiSelect
        value={newValue}
        onChange={handleChange}
        autoWidth={false}
        style={{ width: '200px' }}
        {...props}
      >
        <MenuItem value={10}>Ten</MenuItem>
        <MenuItem value={20}>Twenty</MenuItem>
        <MenuItem value={30}>Thirty and some really long text stuff</MenuItem>
      </MuiSelect>
    </FormControl>
  );
};

const SelectDemo: React.FC = () => (
  <Paper sx={{ padding: 1 }}>
    <Typography sx={{ fontSize: '1.6rem', margin: '10px 0' }}>
      Select variants
    </Typography>
    <Box>
      <StoryHeader title="filled" />

      <Select label="Label" />

      <Select label="Label" value={10} />

      <Select label="Label" value="input text error" error />

      <Select label="Label" disabled />
    </Box>
  </Paper>
);

export const SelectLight: Story = {
  render: () => <SelectDemo />,
  tags: ['snapshot'],
  parameters: {
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf92695e71d82acd448ea2',
      },
    ],
  },
};

SelectLight.storyName = 'Select light theme';

export const SelectDark: Story = {
  render: () => <SelectDemo />,
  tags: ['snapshot', 'dark'],
  parameters: {
    zeplinLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e68f80d83109782953ac',
      },
    ],
  },
};

SelectDark.storyName = 'Select dark theme';
