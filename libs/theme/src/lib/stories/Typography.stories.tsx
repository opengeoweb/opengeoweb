/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import {
  Grid2 as Grid,
  Typography as MuiTypography,
  Link,
} from '@mui/material';

import type { StoryObj } from '@storybook/react';

type Story = StoryObj<typeof MuiTypography>;

export default {
  title: 'demo/Typography',
};

// TODO: needs to be updated with styles from design
const TypographyDemo: React.FC = () => {
  return (
    <Grid container>
      {/** Typography */}
      <Grid size={{ xs: 12 }}>
        <MuiTypography variant="h1">h1</MuiTypography>
        <MuiTypography variant="h2">h2</MuiTypography>
        <MuiTypography variant="h3">h3</MuiTypography>
        <MuiTypography variant="h4">h4</MuiTypography>
        <MuiTypography variant="h5">h5</MuiTypography>
        <MuiTypography variant="h6">h6</MuiTypography>
        <MuiTypography variant="subtitle1">subtitle1</MuiTypography>
        <MuiTypography variant="subtitle2">subtitle2</MuiTypography>
        <MuiTypography variant="body1">body1</MuiTypography>
        <MuiTypography variant="body2">body2</MuiTypography>
        <MuiTypography variant="overline">overline</MuiTypography>
        <br />
        <MuiTypography variant="caption">caption</MuiTypography>
        <br />
        <MuiTypography variant="button">button</MuiTypography>
        <br />
        <Link
          href="#"
          onClick={(event: React.MouseEvent): void => {
            event.preventDefault();
          }}
        >
          Link
        </Link>
      </Grid>
    </Grid>
  );
};

export const TypographyLight: Story = {
  render: () => (
    <div style={{ width: '200px' }}>
      <TypographyDemo />
    </div>
  ),
  tags: ['snapshot', 'fullscreen-snapshot'],
  parameters: {
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecfcf3a512c7547edc1aafb',
      },
    ],
  },
};
TypographyLight.storyName = 'Typography light theme';

export const TypographyDark: Story = {
  render: () => (
    <div style={{ width: '200px' }}>
      <TypographyDemo />
    </div>
  ),
  tags: ['snapshot', 'dark', 'fullscreen-snapshot'],
  parameters: {
    zeplinLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093ff656cbb4c0b83c67722',
      },
    ],
  },
};
TypographyDark.storyName = 'Typography dark theme';
