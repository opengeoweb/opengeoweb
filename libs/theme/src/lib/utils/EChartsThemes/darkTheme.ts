/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

export const EChartsDarkTheme = {
  backgroundColor: '#393939',
  textStyle: {
    color: '#ffffff',
    fontSize: '10px',
    fontWeight: '400',
  },
  title: {
    textStyle: {
      color: '#ffffff',
    },
    subtextStyle: {
      color: '#ffffff',
    },
  },
  line: {
    itemStyle: {
      borderWidth: 1,
    },
    lineStyle: {
      width: 2,
    },
    symbolSize: 4,
    symbol: 'circle',
    smooth: false,
  },
  bar: {
    itemStyle: {
      barBorderWidth: 0,
      barBorderColor: '#ccc',
    },
  },
  scatter: {
    itemStyle: {
      borderWidth: 0,
      borderColor: '#ccc',
    },
  },
  categoryAxis: {
    axisLine: {
      show: true,
      lineStyle: {
        color: '#979797',
      },
    },
    axisTick: {
      show: true,
      lineStyle: {
        color: '#979797',
      },
    },
    axisLabel: {
      show: true,
      color: 'rgba(255, 255, 255, 0.67)',
      fontSize: '8px',
    },
    splitLine: {
      show: false,
      lineStyle: {
        color: ['#979797'],
      },
    },
    splitArea: {
      show: false,
      areaStyle: {
        color: ['#979797'],
      },
    },
  },
  valueAxis: {
    axisLine: {
      show: true,
      lineStyle: {
        color: '#979797',
      },
    },
    axisTick: {
      show: true,
      lineStyle: {
        color: '#979797',
      },
    },
    axisLabel: {
      show: true,
      color: 'rgba(255, 255, 255, 0.67)',
      fontSize: '8px',
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: ['#979797'],
      },
    },
    splitArea: {
      show: false,
      areaStyle: {
        color: ['#979797'],
      },
    },
  },
  logAxis: {
    axisLine: {
      show: true,
      lineStyle: {
        color: '#979797',
      },
    },
    axisTick: {
      show: true,
      lineStyle: {
        color: '#979797',
      },
    },
    axisLabel: {
      show: true,
      color: 'rgba(255, 255, 255, 0.67)',
      fontSize: '8px',
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: ['#979797'],
      },
    },
    splitArea: {
      show: false,
      areaStyle: {
        color: ['#979797'],
      },
    },
  },
  timeAxis: {
    nameTextStyle: {
      color: 'rgba(255, 255, 255, 0.67)',
    },
    axisLine: {
      show: true,
      lineStyle: {
        color: '#979797',
      },
    },
    axisTick: {
      show: true,
      lineStyle: {
        color: '#979797',
      },
    },
    axisLabel: {
      show: true,
      color: 'rgba(255, 255, 255, 0.67)',
      fontSize: '8px',
    },
    splitLine: {
      show: false,
      lineStyle: {
        color: ['#979797'],
      },
    },
    splitArea: {
      show: false,
      areaStyle: {
        color: ['#979797'],
      },
    },
  },
  toolbox: {
    iconStyle: {
      borderColor: '#ffffff',
    },
    emphasis: {
      iconStyle: {
        borderColor: '#979797',
      },
    },
  },
  legend: {
    textStyle: {
      color: '#979797',
    },
  },
  tooltip: {
    axisPointer: {
      lineStyle: {
        color: '#979797',
        width: '1',
      },
      crossStyle: {
        color: '#979797',
        width: '1',
      },
    },
  },
  timeline: {
    lineStyle: {
      color: '#979797',
      width: 1,
    },
    itemStyle: {
      color: '#e60049',
      borderWidth: 1,
    },
    controlStyle: {
      color: '#979797',
      borderColor: '#979797',
      borderWidth: 0.5,
    },
    checkpointStyle: {
      color: '#e43c59',
      borderColor: '#c23531',
    },
    label: {
      color: 'rgba(255, 255, 255, 0.67)',
      fontSize: '8px',
    },
    emphasis: {
      itemStyle: {
        color: '#a9334c',
      },
      controlStyle: {
        color: '#979797',
        borderColor: '#979797',
        borderWidth: 0.5,
      },
      label: {
        color: 'rgba(255, 255, 255, 0.67)',
      },
    },
  },
  dataZoom: {
    backgroundColor: 'rgba(47,69,84,0)',
    dataBackgroundColor: 'rgba(255,255,255,0.3)',
    fillerColor: 'rgba(167,183,204,0.4)',
    handleColor: '#313131',
    handleSize: '100%',
    textStyle: {
      color: '#979797',
    },
  },
  markPoint: {
    label: {
      color: 'rgba(255, 255, 255, 0.67)',
    },
    emphasis: {
      label: {
        color: 'rgba(255, 255, 255, 0.67)',
      },
    },
  },
};
