/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { Button, Theme, useTheme } from '@mui/material';
import { ThemeWrapper, useThemeContext } from './ThemeContext';
import { darkTheme, lightTheme } from '../../utils/themes';
import { ThemeTypes } from '../../types';

interface TestComponentProps {
  onChangeTheme: (theme: Theme) => void;
}

const TestComponent: React.FC<TestComponentProps> = ({
  onChangeTheme,
}: TestComponentProps) => {
  const theme = useTheme();
  const { namedTheme, setThemeName } = useThemeContext();
  const toggleTheme = (): void => {
    setThemeName?.(
      namedTheme?.name === ThemeTypes.LIGHT_THEME
        ? ThemeTypes.DARK_THEME
        : ThemeTypes.LIGHT_THEME,
    );
  };
  const isDark = namedTheme?.theme.palette.mode === 'dark';

  React.useEffect(() => {
    onChangeTheme(theme);
  }, [theme, onChangeTheme]);
  return (
    <div className="test-component">
      <Button data-testid="test-btn" color="secondary" onClick={toggleTheme}>
        {isDark ? 'toggle to light' : 'toggle to dark'}
      </Button>
    </div>
  );
};

describe('Theme/ThemeContext', () => {
  it('wrap a component with a provider with default theme', () => {
    const props = {
      onChangeTheme: jest.fn(),
    };
    render(
      <ThemeWrapper>
        <TestComponent {...props} />
      </ThemeWrapper>,
    );

    expect(screen.getByRole('button')).toBeTruthy();
    expect(props.onChangeTheme).toHaveBeenCalledWith(lightTheme);
  });
  it('wrap a component with a provider with given theme 2', () => {
    const props = {
      onChangeTheme: jest.fn(),
    };
    render(
      <ThemeWrapper>
        <TestComponent {...props} />
      </ThemeWrapper>,
    );
    expect(props.onChangeTheme).toHaveBeenCalledWith(lightTheme);

    const themeButton = screen.getByRole('button');
    expect(themeButton).toBeTruthy();
    expect(themeButton.textContent).toEqual('toggle to dark');

    // change theme
    fireEvent.click(themeButton);
    expect(props.onChangeTheme).toHaveBeenCalledWith(darkTheme);
    expect(themeButton.textContent).toEqual('toggle to light');
  });
});
