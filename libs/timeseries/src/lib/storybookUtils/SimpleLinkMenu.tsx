/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  Menu,
  ListItemButton,
  Checkbox,
  ListItemText,
  List,
} from '@mui/material';
import { CustomIconButton } from '@opengeoweb/shared';
import {
  CoreAppStore,
  mapSelectors,
  genericSelectors,
  genericActions,
} from '@opengeoweb/store';
import { Link, LinkOff } from '@opengeoweb/theme';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

interface SimpleLinkMenuProps {
  panelId: string;
}

export const SimpleLinkMenu: React.FC<SimpleLinkMenuProps> = ({ panelId }) => {
  const dispatch = useDispatch();
  const maps = useSelector((store: CoreAppStore) =>
    mapSelectors.getAllMapIds(store),
  );
  const selectedMapIds = useSelector((store: CoreAppStore) =>
    genericSelectors.getLinkedMaps(store, panelId),
  );
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleMenuOpen = (event: React.MouseEvent<HTMLElement>): void => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = (): void => {
    setAnchorEl(null);
  };

  const handleMapSelect = (mapId: string): void => {
    const updatedMapIds = selectedMapIds.includes(mapId)
      ? selectedMapIds.filter((id) => id !== mapId)
      : [...selectedMapIds, mapId];
    dispatch(
      genericActions.updateLinkedMap({
        panelId,
        mapIds: updatedMapIds,
      }),
    );
  };

  return (
    <>
      <CustomIconButton
        onClick={handleMenuOpen}
        tooltipTitle="Link"
        sx={{ marginLeft: 0.5, marginRight: 0.5 }}
      >
        {selectedMapIds.length > 0 ? (
          <Link
            style={{
              fill: anchorEl ? 'white' : undefined,
              backgroundColor: anchorEl ? '#186DFF' : undefined,
            }}
          />
        ) : (
          <LinkOff
            style={{
              fill: anchorEl ? 'white' : undefined,
              backgroundColor: anchorEl ? '#186DFF' : undefined,
            }}
          />
        )}
      </CustomIconButton>
      <Menu
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleMenuClose}
        keepMounted
      >
        <List>
          {maps.map((mapId) => (
            <ListItemButton
              key={mapId}
              onClick={() => handleMapSelect(mapId)}
              dense
            >
              <Checkbox
                edge="start"
                checked={selectedMapIds.includes(mapId)}
                tabIndex={-1}
                disableRipple
              />
              <ListItemText primary={mapId} />
            </ListItemButton>
          ))}
        </List>
      </Menu>
    </>
  );
};
