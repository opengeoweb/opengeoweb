/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  render,
  fireEvent,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';

import { uiTypes } from '@opengeoweb/store';
import userEvent from '@testing-library/user-event';
import TimeSeriesManagerMapButtonConnect, {
  TOOLTIP_TITLE,
} from './TimeSeriesManagerMapButtonConnect';
import { createMockStore } from '../../store/store';
import { TimeSeriesThemeStoreProvider } from '../Providers';

describe('src/components/TimeSeriesManager/TimeSeriesManagerMapButtonConnect', () => {
  it('should dispatch action with passed in mapid when clicked', () => {
    const mockState = {
      ui: {
        dialogs: {
          timeSeriesManager: {
            type: uiTypes.DialogTypes.TimeSeriesManager,
            activeMapId: 'map1',
            isOpen: false,
          },
        },
        order: ['timeSeriesManager'],
      },
    };
    const store = createMockStore(mockState);
    const props = {
      viewId: 'mapId_123',
    };
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesManagerMapButtonConnect {...props} />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(store.getState().ui.dialogs.timeSeriesManager.isOpen).toBeFalsy();

    // button should be present
    expect(screen.getByTestId('timeSeriesManagerButton')).toBeTruthy();

    // close the dialog
    fireEvent.click(screen.getByTestId('timeSeriesManagerButton'));

    expect(store.getState().ui.dialogs.timeSeriesManager.isOpen).toBeTruthy();
  });

  it('should dispatch action to close dialog when dialog is already opened for corresponding map', () => {
    const mockState = {
      ui: {
        dialogs: {
          timeSeriesManager: {
            type: uiTypes.DialogTypes.TimeSeriesManager,
            activeMapId: 'mapId_123',
            isOpen: true,
            source: 'app' as const,
          },
        },
        order: ['timeSeriesManager'],
      },
    };
    const store = createMockStore(mockState);
    const props = {
      viewId: 'mapId_123',
    };
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesManagerMapButtonConnect {...props} />
      </TimeSeriesThemeStoreProvider>,
    );
    expect(store.getState().ui.dialogs.timeSeriesManager.isOpen).toBeTruthy();
    expect(store.getState().ui.dialogs.timeSeriesManager.activeMapId).toEqual(
      props.viewId,
    );

    // button should be present
    expect(screen.getByTestId('timeSeriesManagerButton')).toBeTruthy();

    // close the dialog
    fireEvent.click(screen.getByTestId('timeSeriesManagerButton'));

    expect(store.getState().ui.dialogs.timeSeriesManager.activeMapId).toEqual(
      props.viewId,
    );
    expect(store.getState().ui.dialogs.timeSeriesManager.isOpen).toBeFalsy();
  });

  it('should show and hide tooltip', async () => {
    const mockState = {
      ui: {
        dialogs: {
          timeSeriesManager: {
            type: uiTypes.DialogTypes.TimeSeriesManager,
            activeMapId: 'mapId_123',
            isOpen: true,
            source: 'app' as const,
          },
        },
        order: ['timeSeriesManager'],
      },
    };
    const user = userEvent.setup();
    const store = createMockStore(mockState);
    const props = {
      viewId: 'mapId_123',
    };
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesManagerMapButtonConnect {...props} />
      </TimeSeriesThemeStoreProvider>,
    );
    expect(screen.queryByText(TOOLTIP_TITLE)).toBeFalsy();

    // button should be present
    const button = screen.getByTestId('timeSeriesManagerButton');
    expect(button).toBeTruthy();
    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(screen.getByText(TOOLTIP_TITLE)).toBeTruthy();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
      timeout: 3000,
    });
    expect(screen.queryByText(TOOLTIP_TITLE)).toBeFalsy();
  });
});
