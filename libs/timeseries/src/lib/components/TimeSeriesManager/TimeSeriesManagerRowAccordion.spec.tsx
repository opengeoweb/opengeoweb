/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { TFunction } from 'i18next';
import TimeSeriesManagerRowAccordion from './TimeSeriesManagerRowAccordion';
import { mockTimeSeriesServices } from '../TimeSeries/mockTimeSeriesServices';
import { Parameter, Plot } from '../TimeSeries/types';
import { TimeSeriesThemeStoreProvider } from '../Providers';
import { createMockStore } from '../../store/store';

describe('src/components/TimeSeriesManager/TimeSeriesManagerRowAccordion', () => {
  const store = createMockStore();
  const parameter1: Parameter = {
    id: 'parameter1',
    plotId: 'Plot_1',
    unit: '°C',
    propertyName: 'Temperature',
    plotType: 'line',
    serviceId: mockTimeSeriesServices[0].id,
    collectionId: 'ecmwf',
    opacity: 70,
  };
  const styles = { plot: {}, row: {}, rowText: {} };

  const collectionId = 'ecmwf';
  const mockPlot: Plot = {
    title: 'Plot 1',
    plotId: 'plot_1',
    parameters: [
      {
        id: '1',
        plotId: 'plot_1',
        unit: '°C',
        propertyName: 'Temperature',
        plotType: 'line',
        serviceId: 'fmi',
        collectionId,
        opacity: 70,
      },
      {
        id: '2',
        plotId: 'plot_2',
        unit: 'mm',
        propertyName: 'Precipitation1h',
        plotType: 'bar',
        serviceId: 'fmi',
        collectionId,
        opacity: 70,
      },
    ],
  };

  const addParameter = jest.fn();
  const deleteParameter = jest.fn();
  const patchParameter = jest.fn();
  const togglePlot = jest.fn();
  const t = ((key: string) => key) as unknown as TFunction;

  it('should render', () => {
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesManagerRowAccordion
          styles={styles}
          parameter={parameter1}
          rowIsEnabled={true}
          plotIsEnabled={true}
          addParameter={addParameter}
          patchParameter={patchParameter}
          deleteParameter={deleteParameter}
          togglePlot={togglePlot}
          plot={mockPlot}
          t={t}
        />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(screen.getByTestId('timeSeriesManagerRowAccordion')).toBeTruthy();
  });

  it('should call togglePlot when button is clicked', () => {
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesManagerRowAccordion
          styles={styles}
          parameter={parameter1}
          rowIsEnabled={true}
          plotIsEnabled={true}
          addParameter={addParameter}
          patchParameter={patchParameter}
          deleteParameter={deleteParameter}
          togglePlot={togglePlot}
          plot={mockPlot}
          t={t}
        />
      </TimeSeriesThemeStoreProvider>,
    );

    fireEvent.click(screen.getAllByTestId('accordionEnableButton')[0]);
    expect(togglePlot).toHaveBeenCalledWith('plot_1');
  });

  it('should call addParameter when duplicate button is clicked', () => {
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesManagerRowAccordion
          styles={styles}
          parameter={parameter1}
          rowIsEnabled={true}
          plotIsEnabled={true}
          addParameter={addParameter}
          patchParameter={patchParameter}
          deleteParameter={deleteParameter}
          togglePlot={togglePlot}
          plot={mockPlot}
          t={t}
        />
      </TimeSeriesThemeStoreProvider>,
    );

    fireEvent.click(screen.getAllByTestId('accordionDuplicateButton')[0]);
    expect(addParameter).toHaveBeenCalledWith(parameter1);
  });

  it('should call deleteParameter when delete button is clicked', () => {
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesManagerRowAccordion
          styles={styles}
          parameter={parameter1}
          rowIsEnabled={true}
          plotIsEnabled={true}
          addParameter={addParameter}
          patchParameter={patchParameter}
          deleteParameter={deleteParameter}
          togglePlot={togglePlot}
          plot={mockPlot}
          t={t}
        />
      </TimeSeriesThemeStoreProvider>,
    );

    fireEvent.click(screen.getAllByTestId('accordionDeleteButton')[0]);
    expect(deleteParameter).toHaveBeenCalledWith(parameter1.id);
  });
});
