/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { MenuItem, SelectChangeEvent } from '@mui/material';
import { TooltipSelect } from '@opengeoweb/shared';
import { TFunction } from 'i18next';
import { CustomDimension, Parameter } from '../../TimeSeries/types';

const ParameterDimensionValueSelect: React.FC<{
  rowIsEnabled: boolean;
  parameter: Parameter;
  customDimensions: CustomDimension[];
  patchParameter: (parameter: Partial<Parameter>) => void;
  t: TFunction;
}> = ({ rowIsEnabled, parameter, customDimensions, patchParameter, t }) => {
  const [selectedDimension, setSelectedDimension] =
    React.useState<CustomDimension>();

  React.useEffect(() => {
    if (customDimensions) {
      setSelectedDimension(
        customDimensions.find((d) => d.id === parameter.dimension?.id),
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [customDimensions, parameter.dimension?.id]);

  if (
    !selectedDimension ||
    !customDimensions ||
    customDimensions.length === 0
  ) {
    return null;
  }

  return (
    <TooltipSelect
      SelectDisplayProps={{ 'aria-label': 'timeseriesManager-dimensionValue' }}
      value={
        parameter.dimension?.value !== undefined
          ? parameter.dimension?.value
          : ''
      }
      data-testid="parameterDimensionValueSelect"
      tooltip={`${t('timeseries-dimension-value')}: ${parameter.dimension?.value !== undefined ? parameter.dimension?.value : ''}`}
      isEnabled={rowIsEnabled}
      onChange={(event: SelectChangeEvent<unknown>): void => {
        const newParameter = {
          id: parameter.id,
          dimension: {
            id: parameter.dimension!.id,
            value: event.target.value as string,
          },
        };
        patchParameter(newParameter);
      }}
    >
      <MenuItem disabled>{t('timeseries-dimension-value')}</MenuItem>
      {selectedDimension &&
        selectedDimension.values &&
        selectedDimension.values.map((value) => {
          return (
            <MenuItem
              key={`dimensionvaluemenu-${value}`}
              value={value}
              data-testid={`timeseries-dimension-value-${value}`}
            >
              {value}
            </MenuItem>
          );
        })}
    </TooltipSelect>
  );
};

export default ParameterDimensionValueSelect;
