/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { MenuItem, SelectChangeEvent } from '@mui/material';
import { TooltipSelect } from '@opengeoweb/shared';
import { TFunction } from 'i18next';
import { CustomDimension, Parameter } from '../../TimeSeries/types';
import { REFERENCE_TIME_ID } from '../../../constants';

const ParameterDimensionIdSelect: React.FC<{
  rowIsEnabled: boolean;
  parameter: Parameter;
  customDimensions: CustomDimension[];
  hasReferenceTime: boolean;
  patchParameter: (parameter: Partial<Parameter>) => void;
  t: TFunction;
}> = ({
  rowIsEnabled,
  parameter,
  customDimensions,
  hasReferenceTime,
  patchParameter,
  t,
}) => {
  // Custom dimensions or reference time are not available
  if (
    (!customDimensions || customDimensions.length === 0) &&
    !hasReferenceTime
  ) {
    return null;
  }

  const defaultDimension = hasReferenceTime
    ? REFERENCE_TIME_ID
    : customDimensions[0].id;

  return (
    <TooltipSelect
      SelectDisplayProps={{ 'aria-label': 'timeseriesManager-dimensionId' }}
      // If parameter has no selected dimension id, choose reference time if it is available, otherwise the first dimension
      value={parameter.dimension?.id || defaultDimension}
      data-testid="parameterDimensioIdSelect"
      tooltip={`${t('timeseries-dimension-id')}: ${parameter.dimension?.id || defaultDimension}`}
      isEnabled={rowIsEnabled}
      onChange={(event: SelectChangeEvent<unknown>): void => {
        const newParameter = {
          id: parameter.id,
          dimension: {
            id: event.target.value as string,
            value: '',
          },
        };

        // Select the first value of selected dimension by default
        if (
          newParameter.dimension.id &&
          newParameter.dimension.id !== REFERENCE_TIME_ID
        ) {
          newParameter.dimension.value =
            (customDimensions.find(
              (dimension) => dimension.id === newParameter.dimension.id,
            )?.values[0] as string) || '';
        }
        patchParameter(newParameter);
      }}
    >
      <MenuItem disabled>{t('timeseries-dimension-id')}</MenuItem>
      {hasReferenceTime && (
        <MenuItem
          key="dimensionmenu-id-reference-time"
          value={REFERENCE_TIME_ID}
          data-testid="timeseries-dimension-id-reference-time"
        >
          {t('timeseries-reference-time')}
        </MenuItem>
      )}
      {customDimensions &&
        customDimensions.map((dimension: CustomDimension) => {
          return (
            <MenuItem
              key={`dimensionmenu-id-${dimension.id}`}
              value={dimension.id}
              data-testid={`timeseries-dimension-id-${dimension.id}`}
            >
              {dimension.id}
            </MenuItem>
          );
        })}
    </TooltipSelect>
  );
};

export default ParameterDimensionIdSelect;
