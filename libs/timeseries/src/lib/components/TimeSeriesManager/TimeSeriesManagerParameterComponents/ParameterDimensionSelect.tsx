/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React, { useState } from 'react';
import { Grid2 as Grid } from '@mui/material';
import { TFunction } from 'i18next';
import { useSelector } from 'react-redux';
import { dateUtils } from '@opengeoweb/shared';
import {
  CustomDimension,
  EDRInstance,
  Parameter,
} from '../../TimeSeries/types';
import { getService } from '../../../store/selectors';
import { TimeSeriesModuleState } from '../../../store';
import {
  fetchEdrAllInstances,
  getParameterListForCollectionId,
} from '../../../utils/edrUtils';
import ParameterDimensionIdSelect from './ParameterDimensionIdSelect';
import ParameterDimensionValueSelect from './ParameterDimensionValueSelect';
import { ReferenceTimeSelect } from './ParameterReferenceTimeSelect';
import { REFERENCE_TIME_ID } from '../../../constants';

const ParameterDimensionSelect: React.FC<{
  rowIsEnabled: boolean;
  parameter: Parameter;
  patchParameter: (parameter: Partial<Parameter>) => void;
  t: TFunction;
}> = ({ rowIsEnabled, parameter, patchParameter, t }) => {
  const { serviceId } = parameter;

  const service = useSelector((store: TimeSeriesModuleState) =>
    getService(store, serviceId),
  );
  const [customDimensions, setCustomDimensions] = React.useState<
    CustomDimension[]
  >([]);
  const [refTimeList, setRefTimeList] = useState<EDRInstance[]>([]);

  React.useEffect(() => {
    if (service) {
      getParameterListForCollectionId(service, parameter.collectionId)
        .then((params) => {
          if (params.customDimensions) {
            setCustomDimensions(params.customDimensions);
          }
        })
        .catch(() => {});
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [parameter.collectionId, service?.url]);

  React.useEffect(() => {
    if (service && service.type === 'EDR') {
      fetchEdrAllInstances(service.url, parameter.collectionId)
        .then((refTimeList: EDRInstance[]) => {
          // Sort the reference times in descending order (newest first)
          const sortedRefTimeList = refTimeList.sort((a, b) => {
            return (
              dateUtils.parseCustomDateString(b.id).getTime() -
              dateUtils.parseCustomDateString(a.id).getTime()
            );
          });
          setRefTimeList(sortedRefTimeList);
        })
        .catch(() => {});
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [parameter.collectionId, service?.url]);

  const paramInstanceId: string | undefined =
    parameter.instanceId ||
    (refTimeList.length > 0 ? refTimeList[0].id : undefined);

  const hasReferenceTime =
    refTimeList.length > 0 &&
    !refTimeList.some((value) => value.id === 'unvalidated');

  if (!paramInstanceId) {
    return null;
  }

  if (
    (!customDimensions || customDimensions.length === 0) &&
    !hasReferenceTime
  ) {
    return null;
  }

  return (
    <>
      <Grid container className="rowDimensionIdWidth">
        <ParameterDimensionIdSelect
          rowIsEnabled={rowIsEnabled}
          parameter={parameter}
          customDimensions={customDimensions}
          patchParameter={patchParameter}
          t={t}
          hasReferenceTime={hasReferenceTime}
        />
      </Grid>
      <Grid container className="rowDimensionValueWidth">
        {parameter.dimension?.id &&
        parameter.dimension?.id !== REFERENCE_TIME_ID ? (
          <ParameterDimensionValueSelect
            rowIsEnabled={rowIsEnabled}
            parameter={parameter}
            customDimensions={customDimensions}
            patchParameter={patchParameter}
            t={t}
          />
        ) : (
          <ReferenceTimeSelect
            rowIsEnabled={rowIsEnabled}
            parameter={parameter}
            patchParameter={patchParameter}
            t={t}
            refTimeList={refTimeList}
          />
        )}
      </Grid>
    </>
  );
};

export default ParameterDimensionSelect;
