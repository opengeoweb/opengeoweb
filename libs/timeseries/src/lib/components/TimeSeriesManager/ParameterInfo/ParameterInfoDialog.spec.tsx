/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, screen } from '@testing-library/react';
import { InstanceInformation, QueryInfoElement } from './ParameterInfoDialog';
import { TimeseriesI18nProvider } from '../../Providers';
import { translateKeyOutsideComponents } from '../../../utils/i18n';

describe('QueryInfoElement', () => {
  it('should show loading indicator when instanceInformation is loading', () => {
    const instanceInformation: InstanceInformation = {
      isLoading: true,
    };
    render(
      <TimeseriesI18nProvider>
        <QueryInfoElement instanceInformation={instanceInformation} />
      </TimeseriesI18nProvider>,
    );
    expect(screen.getByTestId('loadingIndicator')).toBeTruthy();
  });

  it('should show no data available message when instances array is empty', () => {
    const instanceInformation: InstanceInformation = {
      isLoading: false,
      instances: [],
    };
    render(
      <TimeseriesI18nProvider>
        <QueryInfoElement instanceInformation={instanceInformation} />
      </TimeseriesI18nProvider>,
    );
    expect(screen.queryByTestId('loadingIndicator')).toBeFalsy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('timeseries-info-no-data-available'),
      ),
    ).toBeTruthy();
  });

  it('should show no data available message when no locations or position available', () => {
    const instanceInformation: InstanceInformation = {
      isLoading: false,
      instances: [
        {
          id: 'instance1',
          data_queries: {},
        },
      ],
    };

    render(
      <TimeseriesI18nProvider>
        <QueryInfoElement instanceInformation={instanceInformation} />
      </TimeseriesI18nProvider>,
    );
    expect(
      screen.getByText(
        translateKeyOutsideComponents('timeseries-info-no-data-available'),
      ),
    ).toBeTruthy();
  });

  it('should display correct query information when instances array is not empty', () => {
    const instanceInformation: InstanceInformation = {
      isLoading: false,
      instances: [
        {
          id: 'instance1',
          data_queries: {
            locations: {
              link: {
                variables: {
                  output_formats: ['CoverageJSON', 'GeoJSON'],
                },
              },
            },
            position: {
              link: {
                variables: {
                  output_formats: ['CoverageJSON', 'GeoJSON'],
                },
              },
            },
          },
        },
      ],
    };

    render(
      <TimeseriesI18nProvider>
        <QueryInfoElement instanceInformation={instanceInformation} />
      </TimeseriesI18nProvider>,
    );
    expect(screen.getByText('locations, position')).toBeTruthy();
  });

  it('should display correct query information when only position present', () => {
    const instanceInformation: InstanceInformation = {
      isLoading: false,
      instances: [
        {
          id: 'instance1',
          data_queries: {
            position: {
              link: {
                variables: {
                  output_formats: ['CoverageJSON', 'GeoJSON'],
                },
              },
            },
          },
        },
      ],
    };

    render(
      <TimeseriesI18nProvider>
        <QueryInfoElement instanceInformation={instanceInformation} />
      </TimeseriesI18nProvider>,
    );
    expect(screen.getByText('position')).toBeTruthy();
  });

  it('should display correct query information when only locations present', () => {
    const instanceInformation: InstanceInformation = {
      isLoading: false,
      instances: [
        {
          id: 'instance1',
          data_queries: {
            locations: {
              link: {
                variables: {
                  output_formats: ['CoverageJSON', 'GeoJSON'],
                },
              },
            },
          },
        },
      ],
    };

    render(
      <TimeseriesI18nProvider>
        <QueryInfoElement instanceInformation={instanceInformation} />
      </TimeseriesI18nProvider>,
    );
    expect(screen.getByText('locations')).toBeTruthy();
  });
});
