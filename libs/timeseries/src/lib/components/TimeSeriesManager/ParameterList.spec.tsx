/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen } from '@testing-library/react';
import { TFunction } from 'i18next';
import { Plot } from '../TimeSeries/types';
import { ParameterList } from './ParameterList';
import { createMockStore } from '../../store/store';
import { TimeSeriesThemeStoreProvider } from '../Providers';

const collectionId = 'ecmwf';
const mockPlot: Plot = {
  title: 'Plot 1',
  plotId: 'plot_1',
  parameters: [
    {
      id: '1',
      plotId: 'plot_1',
      unit: '°C',
      propertyName: 'Temperature',
      plotType: 'line',
      serviceId: 'fmi',
      collectionId,
      opacity: 70,
    },
    {
      id: '2',
      plotId: 'plot_2',
      unit: 'mm',
      propertyName: 'Precipitation1h',
      plotType: 'bar',
      serviceId: 'fmi',
      collectionId,
      opacity: 70,
    },
  ],
};

const mockDeleteParameter = jest.fn();
const mockToggleParameter = jest.fn();
const mockPatchParameter = jest.fn();
const mockAddParameter = jest.fn();
const mockMoveParameter = jest.fn();
const togglePlot = jest.fn();
const t = ((key: string) => key) as unknown as TFunction;

describe('src/components/TimeSeriesManager/ParameterList', () => {
  it('should render without crashing', () => {
    const store = createMockStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <ParameterList
          plot={mockPlot}
          plotIsEnabled={true}
          deleteParameter={mockDeleteParameter}
          toggleParameter={mockToggleParameter}
          patchParameter={mockPatchParameter}
          addParameter={mockAddParameter}
          moveParameter={mockMoveParameter}
          togglePlot={togglePlot}
          t={t}
        />
      </TimeSeriesThemeStoreProvider>,
    );
  });

  it('should render correct number of parameters', async () => {
    const store = createMockStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <ParameterList
          plot={mockPlot}
          plotIsEnabled={true}
          deleteParameter={mockDeleteParameter}
          toggleParameter={mockToggleParameter}
          patchParameter={mockPatchParameter}
          addParameter={mockAddParameter}
          moveParameter={mockMoveParameter}
          togglePlot={togglePlot}
          t={t}
        />
      </TimeSeriesThemeStoreProvider>,
    );
    expect(await screen.findAllByTestId('enableButton')).toHaveLength(
      mockPlot.parameters!.length,
    );
  });

  it('should call deleteParameter when delete button is clicked', () => {
    const store = createMockStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <ParameterList
          plot={mockPlot}
          plotIsEnabled={true}
          deleteParameter={mockDeleteParameter}
          toggleParameter={mockToggleParameter}
          patchParameter={mockPatchParameter}
          addParameter={mockAddParameter}
          moveParameter={mockMoveParameter}
          togglePlot={togglePlot}
          t={t}
        />
      </TimeSeriesThemeStoreProvider>,
    );
    fireEvent.click(screen.getAllByTestId('deleteButton')[0]);
    expect(mockDeleteParameter).toHaveBeenCalledWith('1');
  });

  it('should call toggleParameter when visibility button is clicked', () => {
    const store = createMockStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <ParameterList
          plot={mockPlot}
          plotIsEnabled={true}
          deleteParameter={mockDeleteParameter}
          toggleParameter={mockToggleParameter}
          patchParameter={mockPatchParameter}
          addParameter={mockAddParameter}
          moveParameter={mockMoveParameter}
          togglePlot={togglePlot}
          t={t}
        />
      </TimeSeriesThemeStoreProvider>,
    );

    fireEvent.click(screen.getAllByTestId('enableButton')[0]);
    expect(mockToggleParameter).toHaveBeenCalledWith('1');
  });

  it('should show collectionId', () => {
    const store = createMockStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <ParameterList
          plot={mockPlot}
          plotIsEnabled={true}
          addParameter={mockAddParameter}
          deleteParameter={mockDeleteParameter}
          toggleParameter={mockToggleParameter}
          patchParameter={mockPatchParameter}
          moveParameter={mockMoveParameter}
          togglePlot={togglePlot}
          t={t}
        />
      </TimeSeriesThemeStoreProvider>,
    );
    // There should be two plot lines with the same collectionId
    expect(screen.queryAllByText(collectionId).length).toBe(6);
  });
});
