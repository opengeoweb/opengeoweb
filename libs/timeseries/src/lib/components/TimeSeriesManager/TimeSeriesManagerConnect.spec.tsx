/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, screen } from '@testing-library/react';
import { CoreAppStore, uiTypes } from '@opengeoweb/store';
import userEvent from '@testing-library/user-event';
import { TimeSeriesManagerConnect } from './TimeSeriesManagerConnect';
import { TimeSeriesModuleState } from '../../store/types';
import { mockTimeSeriesServices } from '../TimeSeries/mockTimeSeriesServices';
import { createMockStore } from '../../store/store';
import { TimeSeriesThemeStoreProvider } from '../Providers';

describe('src/components/TimeSeriesManager/TimeSeriesManagerConnect', () => {
  it('should display manager and edit plot title', async () => {
    const mockMapId = 'mockMapId';
    const title = 'title';
    const coreStore = {
      webmap: {
        byId: {
          mockMapId: {
            bbox: {
              left: -450651.2255879827,
              bottom: 6490531.093143953,
              right: 1428345.8183648037,
              top: 7438773.776232235,
            },
            srs: 'EPSG:3857',
            id: 'test',
            isAnimating: false,
            isAutoUpdating: false,
            shouldEndtimeOverride: false,
            baseLayers: [],
            overLayers: [],
            mapLayers: [],
            featureLayers: [],
          },
        },
        allIds: [mockMapId],
      },
      ui: {
        order: [uiTypes.DialogTypes.TimeSeriesManager],
        dialogs: {
          timeSeriesManager: {
            activeMapId: mockMapId,
            isOpen: true,
            type: uiTypes.DialogTypes.TimeSeriesManager,
            source: 'app',
          },
        },
      },
    } as CoreAppStore;

    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: mockMapId,
          plots: [{ plotId: 'plotId1', title }],
          parameters: [],
        },
        services: mockTimeSeriesServices,
        timeseriesSelect: {
          filters: {
            searchFilter: '',
            serviceFilterChips: {
              entities: {
                [mockTimeSeriesServices[0].id]: {
                  serviceId: mockTimeSeriesServices[0].id,
                  type: mockTimeSeriesServices[0].type,
                  serviceUrl: mockTimeSeriesServices[0].url,
                  serviceName: mockTimeSeriesServices[0].name,
                  enabled: true,
                  scope: 'system',
                },
              },
              ids: [mockTimeSeriesServices[0].id],
            },
            allServiceFilterChipsEnabled: true,
          },
          servicePopup: {
            isOpen: false,
            variant: 'add',
          },
        },
      },
    };

    const mockState = { ...timeSeriesStore, ...coreStore };
    const store = createMockStore(mockState);
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesManagerConnect />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(
      screen.getByRole('heading', { name: /time series manager/i }),
    ).toBeInTheDocument();

    const user = userEvent.setup();
    const sTitle = await screen.findByLabelText(`${title}, click to edit`);
    await user.click(sTitle);
    const editTitle = await screen.findByLabelText(`Edit title ${title}`);
    await user.click(editTitle);
    await user.type(editTitle, '123{enter}');
    expect(await screen.findByText(`title123`)).toBeInTheDocument();
  });
});
