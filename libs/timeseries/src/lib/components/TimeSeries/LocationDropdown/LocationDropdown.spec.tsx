/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  fireEvent,
  render,
  screen,
  waitFor,
  within,
} from '@testing-library/react';
import { LocationDropdown } from './LocationDropdown';
import { TimeSeriesThemeWrapper } from '../../Providers';
import { translateKeyOutsideComponents } from '../../../utils/i18n';
import { testDataWith3Services } from './testData';

describe('components/Timeseries/LocationDropdown', () => {
  it('should render the LocationDropdown', () => {
    render(
      <TimeSeriesThemeWrapper>
        <LocationDropdown handleChange={() => {}} selectedLocation="Helsinki" />
      </TimeSeriesThemeWrapper>,
    );

    expect(
      screen.getByText(
        translateKeyOutsideComponents('timeseries-location-dropdown-search'),
      ),
    );
  });

  it('should render a dropdown for each service', async () => {
    render(
      <TimeSeriesThemeWrapper>
        <LocationDropdown
          splitGeoJson={testDataWith3Services}
          handleChange={() => {}}
          selectedLocation="Helsinki"
        />
      </TimeSeriesThemeWrapper>,
    );
    expect((await screen.findAllByTestId('serviceDropdown')).length).toEqual(3);
  });

  it('should show the amount of locations on dropdown', async () => {
    render(
      <TimeSeriesThemeWrapper>
        <LocationDropdown
          splitGeoJson={testDataWith3Services}
          handleChange={() => {}}
          selectedLocation="Helsinki"
        />
      </TimeSeriesThemeWrapper>,
    );

    // testDataWith3Services contains metNorway service with 2 features, fmi service with 3 features and metOffice UK service with 2 features
    const listOfDropdowns = await screen.findAllByTestId('serviceDropdown');
    expect(listOfDropdowns[0]).toHaveTextContent('2');
    expect(listOfDropdowns[1]).toHaveTextContent('3');
    expect(listOfDropdowns[2]).toHaveTextContent('2');
  });

  it('should call handleChange when a location on list is clicked', async () => {
    const mockHandleChange = jest.fn();

    render(
      <TimeSeriesThemeWrapper>
        <LocationDropdown
          splitGeoJson={testDataWith3Services}
          handleChange={mockHandleChange}
          selectedLocation="Helsinki"
        />
      </TimeSeriesThemeWrapper>,
    );

    const dropdownItems = await screen.findAllByTestId(
      'locationDropdownListItem',
    );
    fireEvent.click(dropdownItems[0]);
    expect(mockHandleChange).toHaveBeenCalledTimes(1);
  });

  it('should filter out list based on user input', async () => {
    const mockHandleChange = jest.fn();
    render(
      <TimeSeriesThemeWrapper>
        <LocationDropdown
          splitGeoJson={testDataWith3Services}
          handleChange={mockHandleChange}
          selectedLocation="Helsinki"
        />
      </TimeSeriesThemeWrapper>,
    );

    const dropDownItems = await screen.findAllByTestId(
      'locationDropdownListItem',
    );
    expect(dropDownItems.length).toEqual(7);

    const SearchBar = await screen.findByTestId('locationDropdownSearchBar');
    const inputElement = within(SearchBar).getByRole('textbox');
    fireEvent.change(inputElement, { target: { value: 'Helsinki' } });

    await waitFor(async () => {
      const dropDownItems = await screen.findAllByTestId(
        'locationDropdownListItem',
      );
      expect(dropDownItems.length).toEqual(1);
    });
  });
});
