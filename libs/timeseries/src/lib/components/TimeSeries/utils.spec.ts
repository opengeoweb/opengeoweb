/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { FeatureCollection } from 'geojson';
import {
  addDrawFunctionToGeojson,
  circleDrawFunction,
  createGeojson,
  drawFunctionShowHoveredInfo,
} from './utils';
import { TimeSeriesPresetLocation } from './types';

describe('components/TimeSeries/utils', () => {
  describe('createGeojson', () => {
    it('should create a geojson for given locations', () => {
      const lat = 60.192059;
      const lon = 24.945831;
      const locations = [
        {
          lat,
          lon,
          name: 'Helsinki',
        },
      ];
      const drawFunctionId = 'test1';
      const hoverDrawFunctionId = 'hovertest1';
      const result = createGeojson(
        locations,
        drawFunctionId,
        hoverDrawFunctionId,
      );
      const expectedResult = {
        features: [
          {
            geometry: { coordinates: [lon, lat], type: 'Point' },
            properties: {
              hoverDrawFunctionId,
              drawFunctionId,
              name: locations[0].name,
            },
            type: 'Feature',
          },
        ],
        type: 'FeatureCollection',
      };
      expect(result).toEqual(expectedResult);
    });
    it('should not fail on empty location list', () => {
      const locations: TimeSeriesPresetLocation[] = [];
      const drawFunctionId = 'test2';
      const hoverDrawFunctionId = 'hovertest2';
      const result = createGeojson(
        locations,
        drawFunctionId,
        hoverDrawFunctionId,
      );
      const expectedResult = {
        features: [],
        type: 'FeatureCollection',
      };
      expect(result).toEqual(expectedResult);
    });
  });
  describe('addDrawFunctionToGeojson', () => {
    it('should update drawFunctionId for selected/unselected locations', () => {
      const lat = 60.192059;
      const lon = 24.945831;
      const drawFunctionId1 = '1';
      const drawFunctionId2 = '2';
      const hoverDrawFunctionId1 = '3';

      const geojson = {
        features: [
          {
            geometry: { coordinates: [lon, lat], type: 'Point' },
            properties: { drawFunctionId: drawFunctionId1, name: 'Helsinki' },
            type: 'Feature',
          },
          {
            geometry: { coordinates: [lon, lat], type: 'Point' },
            properties: { drawFunctionId: drawFunctionId2, name: 'Amsterdam' },
            type: 'Feature',
          },
        ],
        type: 'FeatureCollection',
      } as FeatureCollection;
      const result = addDrawFunctionToGeojson(
        geojson,
        drawFunctionId1,
        drawFunctionId2,
        hoverDrawFunctionId1,
        0,
      );

      const expectedResult = {
        features: [
          {
            geometry: { coordinates: [lon, lat], type: 'Point' },
            properties: {
              drawFunctionId: drawFunctionId2,
              hoverDrawFunctionId: hoverDrawFunctionId1,
              name: 'Helsinki',
            },
            type: 'Feature',
          },
          {
            geometry: { coordinates: [lon, lat], type: 'Point' },
            properties: {
              hoverDrawFunctionId: hoverDrawFunctionId1,
              drawFunctionId: drawFunctionId1,
              name: 'Amsterdam',
            },
            type: 'Feature',
          },
        ],
        type: 'FeatureCollection',
      };
      expect(result).toEqual(expectedResult);
    });
  });
  describe('circleDrawFunction', () => {
    it('should draw a circle', () => {
      const canvas = document.createElement('canvas');
      const context = canvas.getContext('2d')!;
      circleDrawFunction(
        {
          context,
          featureIndex: 0,
          coord: { x: 1, y: 1 },
          selected: false,
          isInEditMode: false,
          feature: {
            geometry: { coordinates: [60, 24], type: 'Point' },
            properties: { drawFunctionId: 'test', name: 'Helsinki' },
            type: 'Feature',
          },
          mouseX: 1,
          mouseY: 1,
          isHovered: false,
        },
        '#F00',
        12,
      );

      expect(context.fillStyle).toEqual('#000000');
    });

    it('should draw a tooltip when hovered', () => {
      const canvas = document.createElement('canvas');
      const context = canvas.getContext('2d')!;
      drawFunctionShowHoveredInfo({
        context,
        featureIndex: 0,
        coord: { x: 1, y: 1 },
        selected: false,
        isInEditMode: false,
        feature: {
          geometry: { coordinates: [60, 24], type: 'Point' },
          properties: { drawFunctionId: 'test', name: 'Helsinki' },
          type: 'Feature',
        },
        mouseX: 1,
        mouseY: 1,
        isHovered: true,
      });

      expect(context.font).toEqual('16px Roboto');
    });
  });
});
