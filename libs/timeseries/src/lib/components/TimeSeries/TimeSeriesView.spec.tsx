/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, renderHook, waitFor, screen } from '@testing-library/react';
import { produce } from 'immer';
import { Feature, Point, Polygon } from 'geojson';
import { dateUtils, TimeSeriesService } from '@opengeoweb/shared';

import * as api from './api';
import * as edrUtils from '../../utils/edrUtils';
import {
  fetchParameters,
  getUtcTime,
  TimeSeriesView,
  useGetPlotsWithData,
} from './TimeSeriesView';
import {
  ParameterWithData,
  Plot,
  PlotPreset,
  Parameter,
  PlotWithData,
  TimeSeriesLocation,
  GeoJsonWithService,
  PointerLocation,
} from './types';
import { createMockStore } from '../../store/store';
import { TimeSeriesThemeStoreProvider } from '../Providers';

describe('components/TimeSeries/TimeSeriesView', () => {
  const PLOT_ID_1 = 'PLOT_ID_1';
  const LINE = 'line';
  const PROPERTY_NAME_1 = 'PROPERTY_NAME_1';
  const PROPERTY_NAME_2 = 'PROPERTY_NAME_2';
  const UNIT = 'unit';
  const SERVICE_URL = 'SERVICE_URL';
  const COLLECTIONID_1 = 'ecmwf';

  const service1OGC: TimeSeriesService = {
    id: 'fmi_ogc',
    url: SERVICE_URL,
    type: 'OGC',
    description: 'A desciption',
    name: 'FMI OGC',
    scope: 'system',
  };

  const service1EDR: TimeSeriesService = {
    id: 'fmi_edr',
    url: SERVICE_URL,
    type: 'EDR',
    description: 'A desciption',
    name: 'FMI EDR',
    scope: 'system',
  };

  const ogcParameter: Parameter = {
    plotId: PLOT_ID_1,
    plotType: LINE,
    propertyName: PROPERTY_NAME_1,
    unit: UNIT,
    serviceId: service1OGC.id,
    collectionId: COLLECTIONID_1,
  };

  const ogcParameter2: Parameter = {
    ...ogcParameter,
    propertyName: PROPERTY_NAME_2,
  };

  const edrParameter: Parameter = {
    ...ogcParameter,
    serviceId: service1EDR.id,
  };

  const edrParameter2: Parameter = {
    ...edrParameter,
    propertyName: PROPERTY_NAME_2,
  };

  const TIME_1 = '20220113T120000';
  const TIME_2 = '20220113T130000';
  const RESULT = ['1', '2'];

  const parameterData = {
    timestep: [
      dateUtils.stringToDate(TIME_1, "yyyyMMdd'T'HHmmss"),
      dateUtils.stringToDate(TIME_2, "yyyyMMdd'T'HHmmss"),
    ],
    value: RESULT.map((el) => Number(el)),
  };

  const geoJsonByCollection: GeoJsonWithService[] = [];

  const expectedOgcParameterWithData = {
    ...ogcParameter,
    ...parameterData,
  } as ParameterWithData;

  const expectedEdrParameterWithData = {
    ...edrParameter,
    ...parameterData,
  } as ParameterWithData;

  const expectedEdrParameter2WithData = {
    ...edrParameter2,
    ...parameterData,
  } as ParameterWithData;

  const mapPinLocation = { lat: 1.0, lon: 2.0 };

  const plot: Plot = {
    plotId: PLOT_ID_1,
    title: 'Title',
  };

  const ogcPreset: PlotPreset = {
    mapId: 'mapId',
    plots: [plot],
    parameters: [ogcParameter, ogcParameter2],
  };

  const edrPreset: PlotPreset = {
    mapId: 'mapId',
    plots: [plot],
    parameters: [edrParameter, edrParameter2],
  };

  const mockGetOgcParameter = async (): Promise<ParameterWithData | null> => {
    return expectedOgcParameterWithData;
  };

  const mockGetEdrParameter = async (
    parameterWithData: ParameterWithData,
  ): Promise<ParameterWithData | null> => {
    return parameterWithData;
  };

  describe('TimeSeriesView', () => {
    it('should render', async () => {
      const mockState = {
        timeSeries: { plotPreset: ogcPreset, services: [service1OGC] },
      };
      const store = createMockStore(mockState);
      const spy = jest
        .spyOn(api, 'getOgcParameter')
        .mockImplementation(mockGetOgcParameter);
      jest.useFakeTimers();
      const { rerender, unmount } = render(
        <TimeSeriesThemeStoreProvider store={store}>
          <TimeSeriesView
            selectedLocation={mapPinLocation}
            plotPreset={ogcPreset}
            services={[service1OGC]}
            splitGeoJsonByCollection={geoJsonByCollection}
          />
        </TimeSeriesThemeStoreProvider>,
      );
      expect(await screen.findByTestId('TimeSeriesChart')).toBeTruthy();

      expect(spy).toHaveBeenCalledTimes(2);

      // Should not rerender if lat lon did not change
      rerender(
        <TimeSeriesThemeStoreProvider store={store}>
          <TimeSeriesView
            selectedLocation={mapPinLocation}
            plotPreset={ogcPreset}
            services={[service1OGC]}
            splitGeoJsonByCollection={geoJsonByCollection}
          />
        </TimeSeriesThemeStoreProvider>,
      );

      expect(spy).toHaveBeenCalledTimes(2);

      // timeout should trigger
      jest.advanceTimersByTime(60_000);
      expect(spy).toHaveBeenCalledTimes(4);
      unmount();
    });
  });

  describe('useUpdateTimeSeriesData', () => {
    it('should update data correctly', async () => {
      const spy = jest
        .spyOn(api, 'getOgcParameter')
        .mockImplementation(mockGetOgcParameter);
      let location = mapPinLocation;
      let plotPreset = ogcPreset;
      const services = [service1OGC];
      const { rerender, result } = renderHook(() =>
        useGetPlotsWithData(
          plotPreset,
          location,
          services,
          geoJsonByCollection,
        ),
      );
      await waitFor(() => {
        expect(spy).toHaveBeenCalledTimes(2);
      });

      let current: PlotWithData;
      await waitFor(() => {
        // eslint-disable-next-line prefer-destructuring
        current = result.current![0];
        expect(current.title).toEqual(plotPreset.plots[0].title);
      });
      expect(current!.plotId).toEqual(plotPreset.plots[0].plotId);
      expect(current!.parametersWithData.length).toEqual(2);

      plotPreset = { ...ogcPreset, parameters: [ogcParameter] };
      rerender();
      await waitFor(() => {
        expect(spy).toHaveBeenCalledTimes(3);
      });
      await waitFor(() => {
        expect(result.current![0].parametersWithData.length).toEqual(1);
      });

      plotPreset = ogcPreset;
      location = { ...mapPinLocation };
      rerender();
      await waitFor(() => {
        expect(spy).toHaveBeenCalledTimes(5);
      });
      await waitFor(() => {
        expect(result.current![0].parametersWithData.length).toEqual(2);
      });
    });

    it('should remove hidden plots and parameters', async () => {
      const spy = jest
        .spyOn(api, 'getOgcParameter')
        .mockImplementation(mockGetOgcParameter);

      const location = mapPinLocation;
      let plotPreset = ogcPreset;
      const services = [service1OGC];
      const { rerender, result } = renderHook(() =>
        useGetPlotsWithData(
          plotPreset,
          location,
          services,
          geoJsonByCollection,
        ),
      );

      await waitFor(() => {
        expect(spy).toHaveBeenCalledTimes(2);
      });
      await waitFor(() => {
        expect(result.current).toHaveLength(1);
      });

      // should remove hidden plot
      plotPreset = produce(ogcPreset, (draft) => {
        draft.plots[0].enabled = false;
      });
      rerender(plotPreset);
      await waitFor(() => {
        expect(spy).toHaveBeenCalledTimes(2);
      });
      expect(result.current).toEqual(undefined);

      // should remove hidden parameter
      const plotPresetWithHiddenParameter = produce(ogcPreset, (draft) => {
        draft.parameters[0].enabled = false;
      });
      plotPreset = plotPresetWithHiddenParameter;
      rerender();
      await waitFor(() => {
        expect(spy).toHaveBeenCalledTimes(3);
      });
      await waitFor(() => {
        expect(result.current).toHaveLength(1);
      });
    });
  });

  describe('fetchParameters', () => {
    it('should get parameter with data from one service', async () => {
      const spy = jest
        .spyOn(api, 'getOgcParameter')
        .mockImplementation(mockGetOgcParameter);

      const parametersWithData = await fetchParameters(
        { ...ogcPreset, parameters: [ogcParameter] },
        mapPinLocation,
        [service1OGC],
        geoJsonByCollection,
      );

      expect(spy).toHaveBeenCalledTimes(1);

      expect(parametersWithData).toEqual([expectedOgcParameterWithData]);
    });

    it('should get parameter with data from EDR service', async () => {
      const spy = jest
        .spyOn(edrUtils, 'getEdrParameter')
        .mockImplementation(() =>
          mockGetEdrParameter(expectedEdrParameterWithData),
        );

      const parametersWithData = await fetchParameters(
        {
          ...edrPreset,
          parameters: [{ ...edrParameter, serviceId: service1EDR.id }],
        },
        mapPinLocation,
        [service1EDR],
        geoJsonByCollection,
      );

      expect(spy).toHaveBeenCalledTimes(1);

      expect(parametersWithData).toEqual([expectedEdrParameterWithData]);
    });

    it('should fetch data for both EDR parameters', async () => {
      const spy = jest
        .spyOn(edrUtils, 'getEdrParameter')
        .mockImplementationOnce(() =>
          mockGetEdrParameter(expectedEdrParameterWithData),
        )
        .mockImplementationOnce(() =>
          mockGetEdrParameter(expectedEdrParameter2WithData),
        );

      const location: TimeSeriesLocation = {
        ...mapPinLocation,
        id: 'id',
        serviceId: service1EDR.id,
        collectionId: COLLECTIONID_1,
      };

      const parametersWithData = await fetchParameters(
        {
          ...edrPreset,
          parameters: [edrParameter, edrParameter2],
        },
        location,
        [service1EDR],
        geoJsonByCollection,
      );

      expect(spy).toHaveBeenCalledTimes(2);

      expect(parametersWithData).toEqual([
        expectedEdrParameterWithData,
        expectedEdrParameter2WithData,
      ]);
    });
  });

  it('should fetch parameters from all services and collections', async () => {
    const edrSpy = jest
      .spyOn(edrUtils, 'getEdrParameter')
      .mockImplementation(() =>
        mockGetEdrParameter(expectedEdrParameterWithData),
      );

    const ogcSpy = jest
      .spyOn(api, 'getOgcParameter')
      .mockImplementation(() => Promise.resolve(expectedOgcParameterWithData));

    const location: TimeSeriesLocation = {
      ...mapPinLocation,
      id: 'id',
      serviceId: service1EDR.id,
      collectionId: COLLECTIONID_1,
    };

    const parametersWithData = await fetchParameters(
      {
        ...edrPreset,
        parameters: [edrParameter, ogcParameter],
      },
      location,
      [service1OGC, service1EDR],
      geoJsonByCollection,
    );

    expect(edrSpy).toHaveBeenCalledTimes(1);
    expect(ogcSpy).toHaveBeenCalledTimes(1);
    expect(parametersWithData).toEqual([
      expectedEdrParameterWithData,
      expectedOgcParameterWithData,
    ]);
  });

  describe('fetchParameters with service & collection GeoJSON', () => {
    const point: Feature<Point> = {
      type: 'Feature',
      id: 'test-point',
      geometry: {
        type: 'Point',
        coordinates: [24.0, 60.0],
      },
      properties: {},
    };

    const polygon: Feature<Polygon> = {
      type: 'Feature',
      id: 'test-polygon',
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [113.338953, -43.634597],
            [113.338953, -10.668186],
            [153.569469, -10.668186],
            [153.569469, -43.634597],
            [113.338953, -43.634597],
          ],
        ],
      },
      properties: {},
    };

    const geoJsonWithService: GeoJsonWithService = {
      serviceName: service1EDR.name,
      collectionName: COLLECTIONID_1,
      geoJson: {
        type: 'FeatureCollection',
        features: [point, polygon],
      },
    };

    const pointerLocation: PointerLocation = { lat: 59.9, lon: 24.1 };

    it('should use timeseries location directly', async () => {
      // timeseries location that matches a service and collection in GeoJSON
      const timeseriesLocation: TimeSeriesLocation = {
        lat: 60.0,
        lon: 24.0,
        id: 'test-point',
        serviceId: service1EDR.id,
        collectionId: COLLECTIONID_1,
      };

      const findNearestSpy = jest.spyOn(edrUtils, 'findNearestPoint');
      const getEdrSpy = jest
        .spyOn(edrUtils, 'getEdrParameter')
        .mockResolvedValueOnce(expectedEdrParameterWithData);

      const params = await fetchParameters(
        { ...edrPreset, parameters: [edrParameter] },
        timeseriesLocation,
        [service1EDR],
        [geoJsonWithService],
      );

      expect(findNearestSpy).not.toHaveBeenCalled();
      expect(getEdrSpy).toHaveBeenCalledTimes(1);
      expect(params).toEqual([expectedEdrParameterWithData]);
    });

    it('should use polygon timeseries location', async () => {
      // timeseries location that matches a service and collection in GeoJSON
      const timeseriesLocation: TimeSeriesLocation = {
        lat: 0, // Because we are selecting the polygon location by the exact id the pointer location is not important here
        lon: 0,
        id: 'test-polygon',
        serviceId: service1EDR.id,
        collectionId: COLLECTIONID_1,
      };

      const findNearestSpy = jest.spyOn(edrUtils, 'findNearestPoint');
      const getEdrSpy = jest
        .spyOn(edrUtils, 'getEdrParameter')
        .mockResolvedValueOnce(expectedEdrParameterWithData);

      const params = await fetchParameters(
        { ...edrPreset, parameters: [edrParameter] },
        timeseriesLocation,
        [service1EDR],
        [geoJsonWithService],
      );

      expect(findNearestSpy).not.toHaveBeenCalled();
      expect(getEdrSpy).toHaveBeenCalledTimes(1);
      expect(params).toEqual([expectedEdrParameterWithData]);
    });

    it('should not search for nearest point if position query succeeds', async () => {
      const findNearestSpy = jest.spyOn(edrUtils, 'findNearestPoint');
      jest
        .spyOn(edrUtils, 'getEdrParameter')
        .mockImplementationOnce(() =>
          Promise.resolve(expectedEdrParameterWithData),
        );

      const params = await fetchParameters(
        { ...edrPreset, parameters: [edrParameter] },
        pointerLocation,
        [service1EDR],
        [geoJsonWithService],
      );

      expect(findNearestSpy).not.toHaveBeenCalled();
      expect(params).toEqual([expectedEdrParameterWithData]);
    });

    it('should attempt to find nearest point', async () => {
      const findNearestSpy = jest
        .spyOn(edrUtils, 'findNearestPoint')
        .mockReturnValue(point);
      const getEdrSpy = jest
        .spyOn(edrUtils, 'getEdrParameter')
        .mockResolvedValueOnce(null) // position query result
        .mockResolvedValueOnce(expectedEdrParameterWithData);

      const params = await fetchParameters(
        { ...edrPreset, parameters: [edrParameter] },
        pointerLocation,
        [service1EDR],
        [geoJsonWithService],
      );

      expect(findNearestSpy).toHaveBeenCalledWith(
        pointerLocation,
        geoJsonWithService.geoJson.features,
      );

      // getEdrParameter called twice - first with original location, then with nearest point
      expect(getEdrSpy).toHaveBeenCalledTimes(2);
      expect(params).toEqual([expectedEdrParameterWithData]);
    });
  });
});

describe('getUtcTime', () => {
  it('should convert time string to utc time', () => {
    const apiTime = '20220113T120000';
    const utcTime = getUtcTime(apiTime);

    expect(utcTime.toISOString()).toEqual('2022-01-13T12:00:00.000Z');
  });
});
