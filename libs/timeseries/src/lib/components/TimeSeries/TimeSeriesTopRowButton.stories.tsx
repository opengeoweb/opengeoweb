/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { Location } from '@opengeoweb/theme';
import type { Meta, StoryObj } from '@storybook/react';

import { TimeSeriesTopRowButton } from './TimeSeriesTopRowButton';

const meta: Meta<typeof TimeSeriesTopRowButton> = {
  title: 'components/TimeSeriesButtons',
  component: TimeSeriesTopRowButton,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the TimeSeriesTopRowButton',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof TimeSeriesTopRowButton>;

export const Component: Story = {
  args: {
    shortText: 'shortText',
    longText: '',
    icon: <Location />,
  },
  tags: ['!dev'],
};

const testButton = (
  <TimeSeriesTopRowButton
    shortText="shortText"
    longText="longText"
    icon={<Location />}
  />
);

const TimeSeriesTopRowButtonDemo = (): React.ReactElement => (
  <div style={{ padding: '5vh' }}>
    <div
      style={{
        width: '500px',
        height: '30px',
        containerType: 'inline-size',
        containerName: 'time-series-container',
      }}
    >
      {testButton}
    </div>
    <div
      style={{
        width: '100px',
        height: '30px',
        containerType: 'inline-size',
        containerName: 'time-series-container',
      }}
    >
      {testButton}
    </div>
  </div>
);

export const TimeSeriesTopRowButtonDemoLight: Story = {
  render: () => <TimeSeriesTopRowButtonDemo />,
  tags: ['snapshot'],
};
TimeSeriesTopRowButtonDemoLight.storyName =
  'TimeSeries TopRowButtons light theme';

export const TimeSeriesTopRowButtonDemoDark: Story = {
  render: () => <TimeSeriesTopRowButtonDemo />,
  tags: ['snapshot', 'dark'],
};
TimeSeriesTopRowButtonDemoDark.storyName =
  'TimeSeries TopRowButtons dark theme';
