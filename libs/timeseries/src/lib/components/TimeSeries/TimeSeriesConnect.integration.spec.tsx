/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
  within,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { genericActions, mapActions } from '@opengeoweb/store';
import { produce } from 'immer';
import { webmapUtils } from '@opengeoweb/webmap';
import { Feature, FeatureCollection, Point } from 'geojson';
import { ToolkitStore } from '@reduxjs/toolkit/dist/configureStore';
import { Store } from '@reduxjs/toolkit';
import { SystemScope, ServiceInterface } from '@opengeoweb/shared';
import { TimeSeriesConnect } from './TimeSeriesConnect';
import { TimeSeriesLocation, TimeSeriesPresetLocation } from './types';
import { serverMock } from '../../../mocks/server';
import {
  parameterNameMock,
  EDR_FINLAND_URL,
  OGC_URL,
  EDR_NORWAY_URL,
  EDR_NORWAY_COLLECTION_ID,
} from '../../../mocks';
import { TimeSeriesThemeStoreProvider } from '../Providers';
import { TimeSeriesManagerConnect } from '../TimeSeriesManager';
import { TimeSeriesStoreType } from '../../store/types';
import { timeSeriesActions } from '../../store/reducer';
import { mockGeoJSON } from '../../utils/fakeData/mockSharedData';
import { createMockStore } from '../../store/store';

const mapId = 'mapId';
const geojsonLayerId = 'geojsonLayerId';
const plotTitle = 'plotTitle';

const service = {
  id: 'serviceId',
  type: 'EDR' as ServiceInterface,
  url: EDR_FINLAND_URL,
  description: 'a description',
  name: 'FMI OGC',
  scope: 'system' as SystemScope,
};

const transformGeoJSONToPresetLocations = (
  geoJSON: FeatureCollection<Point>,
): TimeSeriesPresetLocation[] => {
  return geoJSON.features.map((feature) => ({
    name: feature.properties?.name || '',
    lat: feature.geometry.coordinates[1],
    lon: feature.geometry.coordinates[0],
  }));
};

describe('src/components/TimeSeries/TimeSeriesConnect/integration', () => {
  const plotId = 'plotId';
  const plotServicesEdr = [service];
  const collectionId = 'ecmwf';

  const timeSeriesPresetEdrFinland: TimeSeriesStoreType = {
    plotPreset: {
      mapId,
      parameters: [
        {
          plotId,
          plotType: 'bar',
          propertyName: parameterNameMock,
          serviceId: 'serviceId',
          unit: 'unit',
          collectionId,
        },
      ],
      plots: [{ plotId, title: plotTitle }],
    },
    services: plotServicesEdr,
    timeseriesSelect: {
      filters: {
        searchFilter: '',
        serviceFilterChips: {
          entities: {
            [service.id]: {
              serviceId: service.id,
              type: service.type,
              serviceUrl: service.url,
              serviceName: service.name,
              enabled: true,
              scope: 'system',
            },
          },
          ids: [service.id],
        },
        allServiceFilterChipsEnabled: true,
      },
      servicePopup: {
        isOpen: false,
        variant: 'add',
      },
    },
  };

  const defaultParamOgc = `?f=json&limit=200`;

  const plotPresetOgc = produce(timeSeriesPresetEdrFinland, (draft) => {
    if (draft.services) {
      draft.services[0].type = 'OGC';
      draft.services[0].url = `${OGC_URL}${defaultParamOgc}`;
    }
  });

  const presetLocations = transformGeoJSONToPresetLocations(mockGeoJSON);

  jest.spyOn(webmapUtils, 'generateLayerId').mockReturnValue(geojsonLayerId);

  beforeAll(() => serverMock.listen());
  afterEach(() => serverMock.resetHandlers());
  afterAll(() => serverMock.close());

  const setup = (
    timeSeriesPreset: TimeSeriesStoreType,
    presetLocations?: TimeSeriesPresetLocation[],
    twoTimeSeries?: boolean,
  ): Store => {
    const store = createMockStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesConnect
          timeSeriesPresetLocations={presetLocations}
          panelId="panel1Id"
        />
        {twoTimeSeries && (
          <TimeSeriesConnect
            timeSeriesPresetLocations={presetLocations}
            panelId="panel2Id"
          />
        )}
        <TimeSeriesManagerConnect />
      </TimeSeriesThemeStoreProvider>,
    );

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(
        timeSeriesActions.registerTimeSeriesPreset(timeSeriesPreset),
      );
      store.dispatch(
        genericActions.updateLinkedMap({
          panelId: 'panel1Id',
          mapIds: [mapId],
        }),
      );
      store.dispatch(
        genericActions.addSharedData({
          panelId: mapId,
          data: { features: [{ id: 'mockId', geoJSON: mockGeoJSON }] },
        }),
      );
    });

    return store;
  };

  it('should work for ogc timeseries', async () => {
    const store = setup(
      plotPresetOgc,
      presetLocations as unknown as TimeSeriesPresetLocation[],
    );
    await clickAround(store, false, 'collectionOGC');
  });

  it('should work for edr finland timeseries', async () => {
    const store = setup(timeSeriesPresetEdrFinland);

    fireEvent.click(await screen.findByTestId('locationSelectButton'));

    const listItems = await screen.findAllByTestId('locationDropdownListItem');

    const listItem = listItems.find((item) =>
      within(item).queryByText('Helsinki'),
    );
    fireEvent.click(listItem!);

    await waitFor(() => {
      expect(store.getState().webmap.byId[mapId].mapPinLocation).toEqual({
        lat: mockGeoJSON.features[0].geometry.coordinates[1],
        lon: mockGeoJSON.features[0].geometry.coordinates[0],
        id: 'helsinki',
        serviceId: service.id,
        collectionId: 'ecmwf',
      });
    });

    const locationSelectButton = await screen.findByTestId(
      'locationSelectButton',
    );
    expect(locationSelectButton).toHaveTextContent('Helsinki');
  });

  it('should work for edr norway timeseries', async () => {
    // Step 1: Create a plot preset specific to EDR Norway
    const plotPresetEdrNorway = produce(timeSeriesPresetEdrFinland, (draft) => {
      if (draft.services) {
        draft.services[0].url = EDR_NORWAY_URL;
        if (
          draft.plotPreset &&
          draft.plotPreset.parameters &&
          draft.plotPreset.parameters.length > 0
        ) {
          // Update collectionId for parameters
          draft.plotPreset.parameters.forEach((draftParameter) => {
            draftParameter.collectionId = EDR_NORWAY_COLLECTION_ID;
          });
        }
      }
    });

    // Step 2: Set up the store with the plot preset
    const store = setup(plotPresetEdrNorway);

    // Step 3: Simulate dropdown interaction and verify list items
    fireEvent.click(await screen.findByTestId('locationSelectButton'));

    const listItems = await screen.findAllByTestId('locationDropdownListItem');
    expect(listItems).toHaveLength(1);

    const norwayItem = listItems.find((item) =>
      within(item).queryByText('Helsinki'),
    );

    expect(norwayItem).toBeInTheDocument();
    fireEvent.click(norwayItem!);

    // Step 4: Verify store updates after clicking the location
    await waitFor(() => {
      const { mapPinLocation } = store.getState().webmap.byId[mapId];
      expect(mapPinLocation).toEqual({
        lat: mockGeoJSON.features[0].geometry.coordinates[1],
        lon: mockGeoJSON.features[0].geometry.coordinates[0],
        id: 'helsinki',
        serviceId: service.id,
        collectionId: EDR_NORWAY_COLLECTION_ID,
      });
    });

    // Step 5: Assert TimeSeriesChart renders
    expect(screen.getByTestId('TimeSeriesChart')).toBeInTheDocument();
  });

  it('should work for two ogc timeseries', async () => {
    const hasTwoTimeseries = true;
    setup(
      plotPresetOgc,
      presetLocations as unknown as TimeSeriesPresetLocation[],
      hasTwoTimeseries,
    );

    const user = userEvent.setup();

    const openManagerButtons = screen.getAllByRole('button', {
      name: 'TimeSeries Manager',
    });
    expect(openManagerButtons).toHaveLength(2);
    const [first, second] = openManagerButtons;
    expect(first).toHaveAttribute('aria-pressed', 'false');
    expect(second).toHaveAttribute('aria-pressed', 'false');
    expect(
      screen.queryByRole('heading', { name: /time series manager/i }),
    ).not.toBeInTheDocument();

    await user.click(first);
    expect(first).toHaveAttribute('aria-pressed', 'true');
    expect(second).toHaveAttribute('aria-pressed', 'false');
    screen.getByRole('heading', { name: /time series manager/i });

    await user.click(second);
    expect(first).toHaveAttribute('aria-pressed', 'false');
    expect(second).toHaveAttribute('aria-pressed', 'true');
    screen.getByRole('heading', { name: /time series manager/i });

    await user.click(second);
    expect(first).toHaveAttribute('aria-pressed', 'false');
    expect(second).toHaveAttribute('aria-pressed', 'false');
    expect(
      screen.queryByRole('heading', { name: /time series manager/i }),
    ).not.toBeInTheDocument();
  });
});

// Rewrite this click around function

const clickAround = async (
  store: ToolkitStore,
  isEdr = false,
  collectionId = '',
): Promise<void> => {
  // 1. map pin has not been set, so no chart is shown
  expect(screen.queryByTestId('TimeSeriesChart')).not.toBeInTheDocument();

  // 2. Simulate setting map pin on a location
  act(() => {
    store.dispatch(
      mapActions.setMapPinLocation({
        mapId,
        mapPinLocation: {
          lat: mockGeoJSON.features[1].geometry.coordinates[1],
          lon: mockGeoJSON.features[1].geometry.coordinates[0],
          id: isEdr ? 'a12cpp' : undefined,
          serviceId: mockGeoJSON.features[1].properties?.serviceId,
          collectionId: mockGeoJSON.features[1].properties?.collectionId,
        },
      }),
    );
  });

  await waitFor(() => {
    store.dispatch(
      genericActions.addSharedData({
        panelId: mapId,
        data: { selectedFeatureId: mockGeoJSON.features[1].id as string },
      }),
    );
  });

  const geojson =
    store.getState().syncGroups.linkedState.sharedData[mapId].features[0]
      .geoJSON;

  const selectedFeatureIndex = geojson.features.findIndex(
    (feature: Feature) => feature.properties!.name === 'A12-CPP',
  );

  const feature = geojson.features[selectedFeatureIndex];

  const [lon, lat] = feature.geometry.coordinates;
  const location: TimeSeriesLocation = {
    lat,
    lon,
    id: feature.id?.toString(),
    serviceId: feature.properties?.serviceId,
    collectionId: feature.properties?.collectionId,
  };

  act(() => {
    store.dispatch(
      genericActions.addSharedData({
        panelId: mapId,
        data: { selectedFeatureId: location.id || '' },
      }),
    );
  });

  expect(
    store.getState().syncGroups.linkedState.sharedData[mapId].selectedFeatureId,
  ).toBe('a12cpp');

  // 3. Set location to Helsinki with location select, this should also change map pin location
  const locationSelectButton = await screen.findByTestId(
    'locationSelectButton',
  );
  expect(locationSelectButton).toBeInTheDocument();
  expect(locationSelectButton).toHaveTextContent('Helsinki');

  fireEvent.click(locationSelectButton);

  const locationDropdownCollectionAccordion = await screen.findByTestId(
    'locationDropdownCollectionAccordion',
  );
  expect(locationDropdownCollectionAccordion).toBeInTheDocument();

  fireEvent.click(locationDropdownCollectionAccordion);

  const listItems = await screen.findAllByTestId('locationDropdownListItem');
  const listItem = listItems.find((item) => within(item).queryByText('Rome'));

  expect(listItem).toBeInTheDocument();

  fireEvent.click(listItem!);

  expect(await screen.findByTestId('TimeSeriesChart')).toBeInTheDocument();

  await waitFor(() => {
    store.dispatch(
      genericActions.addSharedData({
        panelId: mapId,
        data: { selectedFeatureId: mockGeoJSON.features[2].id as string },
      }),
    );
  });

  await waitFor(() => {
    expect(
      store.getState().syncGroups.linkedState.sharedData[mapId]
        .selectedFeatureId,
    ).toBe('rome');
  });

  expect(store.getState().webmap.byId[mapId].mapPinLocation).toEqual({
    lat: mockGeoJSON.features[2].geometry.coordinates[1],
    lon: mockGeoJSON.features[2].geometry.coordinates[0],
    id: isEdr ? 'rome' : undefined,
    serviceId: isEdr ? service.id : undefined,
    collectionId: isEdr ? collectionId : undefined,
  });

  // 4. User clicked outside any features
  act(() => {
    store.dispatch(
      mapActions.setMapPinLocation({
        mapId,
        mapPinLocation: { lat: 1, lon: 1 },
      }),
    );
  });

  act(() => {
    store.dispatch(
      genericActions.addSharedData({
        panelId: mapId,
        data: { selectedFeatureId: undefined },
      }),
    );
  });

  await waitFor(() => {
    expect(
      store.getState().syncGroups.linkedState.sharedData[mapId]
        .selectedFeatureId,
    ).toBeUndefined();
  });

  // 5. Open timeseries manager
  await waitFor(async () => {
    // eslint-disable-next-line testing-library/no-wait-for-side-effects
    fireEvent.click(await screen.findByTestId('timeSeriesManagerButton'));
  });
  await screen.findByText('Time Series Manager');
  await screen.findByText(plotTitle);
  expect(screen.getAllByText(parameterNameMock)[1]).toHaveTextContent(
    'Temperature',
  );

  // 6. Open timeseries select
  fireEvent.click(await screen.findByTestId('timeSeriesSelectButton'));
  await screen.findByText('Timeseries Select for plotTitle');
  const timeSeriesSelectList = await screen.findByTestId(
    'timeseriesSelectList',
  );
  const row = within(timeSeriesSelectList).getByText(collectionId);
  fireEvent.click(row);
  expect(
    within(screen.getByTestId('TimeSeriesSelect')).getByText(parameterNameMock),
  ).toBeTruthy();
};
