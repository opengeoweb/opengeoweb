/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mapUtils, syncGroupsTypes } from '@opengeoweb/store';
import { produce } from 'immer';
import axios from 'axios';
import { Store } from '@reduxjs/toolkit';
import timeseriesTranslations from '../../../../locales/timeseries.json';
import {
  getUniqueServiceAndCollectionIds,
  TimeSeriesConnect,
} from './TimeSeriesConnect';
import { Parameter } from './types';
import { createMockStore } from '../../store/store';
import { TimeSeriesThemeStoreProvider } from '../Providers';
import { TimeSeriesModuleState } from '../../store';

jest.mock('axios');

describe('src/components/TimeSeries/TimeSeriesConnect', () => {
  const mockedAxios = axios as jest.Mocked<typeof axios>;

  const mockFeature = {
    id: 'feature1',
    geometry: {
      type: 'Point',
      coordinates: [24.9353, 60.1699],
    },
    properties: {
      name: 'Helsinki',
      collectionId: 'ecmwf',
      serviceId: 'fmi_ecmwf',
    },
    selected: true,
  };

  mockedAxios.get.mockResolvedValue({
    data: { features: [mockFeature] },
  });

  const lat = 60.192059;
  const lon = 24.945831;

  const timeSeriesPresetLocations = [
    {
      lat,
      lon,
      name: 'Helsinki',
    },
  ];

  const user = userEvent.setup();
  const mapId = 'TimeseriesMap';
  const mockMap = mapUtils.createMap({ id: 'TimeseriesMap' });

  const initialState = {
    webmap: {
      byId: {
        [mapId]: mockMap,
      },
      allIds: [mapId],
    },
    timeSeries: {
      plotPreset: {
        mapId: 'TimeseriesMap',
        plots: [
          {
            title: 'Plot 1',
            plotId: 'Plot_1',
          },
        ],
        parameters: [
          {
            plotId: 'Plot_1',
            plotType: 'line' as const,
            serviceId: 'fmi_ecmwf',
            propertyName: 'Pressure',
            id: 'timeseriesid_17',
            collectionId: 'ecmwf',
          },
        ],
      },
      services: [
        {
          url: 'https://opendata.fmi.fi/edr',
          description: 'something',
          id: 'fmi_ecmwf',
          type: 'EDR' as const,
          name: 'test service',
        },
      ],
    },
    syncGroups: {
      linkedState: {
        sharedData: {
          TimeseriesPanel: {
            features: [
              {
                geoJSON: {
                  features: [mockFeature],
                },
              },
            ],
          },
        },
        links: {
          TimeseriesPanel: ['TimeseriesMap'],
        },
      },
      mapPinLocation: {
        lat: 60.1699,
        lon: 24.9353,
        id: 'feature1',
        collectionId: 'ecmwf',
        serviceId: 'fmi_ecmwf',
      },
    },
  };

  it('should show correct tooltip for enabled mappin', async () => {
    const mockState = produce(initialState, (draft) => {
      draft.webmap.byId[mapId].disableMapPin = false;
    });
    const store = createMockStore(mockState);
    jest.spyOn(console, 'warn').mockImplementation();

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesConnect
          timeSeriesPresetLocations={timeSeriesPresetLocations}
          panelId="panelId"
        />
      </TimeSeriesThemeStoreProvider>,
    );

    const button = screen.getByTestId('toggleLockLocationButton');
    expect(button).toBeTruthy();
    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(screen.getByRole('tooltip').textContent).toBe(
      timeseriesTranslations['en']['timeseries-lock-location'],
    );

    await user.unhover(button);
    await waitFor(() => expect(screen.queryByRole('tooltip')).toBeFalsy());
  });

  it('should show correct tooltip for disabled map pin', async () => {
    const mockState = produce(initialState, (draft) => {
      draft.webmap.byId[mapId].disableMapPin = false;
    });
    const store = createMockStore(mockState);

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesConnect
          timeSeriesPresetLocations={timeSeriesPresetLocations}
          panelId="panelId"
        />
      </TimeSeriesThemeStoreProvider>,
    );

    const button = screen.getByTestId('toggleLockLocationButton');
    expect(button).toBeTruthy();

    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(screen.getByRole('tooltip').textContent).toBe(
      timeseriesTranslations['en']['timeseries-lock-location'],
    );

    await user.unhover(button);
    await waitFor(() => expect(screen.queryByRole('tooltip')).toBeFalsy());

    await user.click(button);
    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(screen.getByRole('tooltip').textContent).toBe(
      timeseriesTranslations['en']['timeseries-unlock-location'],
    );
  });

  it('should re-enable the map pin and make the map pin visible when clicking toggleLockLocationButton', async () => {
    const mockState = produce(initialState, (draft) => {
      draft.webmap.byId[mapId].disableMapPin = false;
    });
    const store = createMockStore(mockState);

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesConnect
          timeSeriesPresetLocations={timeSeriesPresetLocations}
          panelId="panelId"
        />
      </TimeSeriesThemeStoreProvider>,
    );

    const button = screen.getByTestId('toggleLockLocationButton');
    expect(button).toBeTruthy();

    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(screen.getByRole('tooltip').textContent).toBe(
      timeseriesTranslations['en']['timeseries-lock-location'],
    );

    await user.unhover(button);
    await user.click(button);

    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(screen.getByRole('tooltip').textContent).toBe(
      timeseriesTranslations['en']['timeseries-unlock-location'],
    );
  });

  it('should show correct tooltip for marker toggle', async () => {
    const store = createMockStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesConnect
          timeSeriesPresetLocations={timeSeriesPresetLocations}
          panelId="panelId"
        />
      </TimeSeriesThemeStoreProvider>,
    );
    const user = userEvent.setup();
    const switchElement = screen.getByRole('checkbox');
    await user.hover(switchElement);
    expect((await screen.findByRole('tooltip')).textContent).toBe(
      timeseriesTranslations['en'][
        'timeseries-toggle-location-markers-tooltip'
      ],
    );
  });

  it('toggle markers should change drawing function id in geojson', async () => {
    const store: Store<
      TimeSeriesModuleState & syncGroupsTypes.SynchronizationGroupModuleState
    > = createMockStore(initialState);

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesConnect
          timeSeriesPresetLocations={timeSeriesPresetLocations}
          panelId="TimeseriesPanel"
        />
      </TimeSeriesThemeStoreProvider>,
    );

    const user = userEvent.setup();
    const switchElement = screen.getByRole('checkbox');

    // toggle so the sharedData, including the geojson, gets added to store
    await user.click(switchElement);
    await waitFor(() => {
      expect(
        store.getState().syncGroups?.linkedState.sharedData['TimeseriesPanel']
          .features![0].geoJSON.features[0].properties?.drawFunctionId,
      ).toBeTruthy();
    });

    const oldDrawFunctionId =
      store.getState().syncGroups?.linkedState.sharedData['TimeseriesPanel']
        .features![0].geoJSON.features[0].properties?.drawFunctionId;

    // toggle back, drawFunctionId should change
    await user.click(switchElement);
    await waitFor(() => {
      expect(
        store.getState().syncGroups?.linkedState.sharedData['TimeseriesPanel']
          .features![0].geoJSON.features[0].properties?.drawFunctionId,
      ).not.toBe(oldDrawFunctionId);
    });
  });

  describe('getUniqueServiceAndCollectionIds', () => {
    const parameter1: Parameter = {
      serviceId: 'service1',
      collectionId: 'collection1',
      plotId: 'plot1',
      propertyName: 'property1',
      plotType: 'bar',
    };
    const parameter2: Parameter = {
      serviceId: 'service2',
      collectionId: 'collection2',
      plotId: 'plot2',
      propertyName: 'property2',
      plotType: 'bar',
    };
    const parameter3: Parameter = {
      serviceId: 'service3',
      collectionId: 'collection3',
      plotId: 'plot3',
      propertyName: 'property3',
      plotType: 'bar',
    };

    it('should filter out duplicates', () => {
      const result = getUniqueServiceAndCollectionIds([
        parameter1,
        parameter1,
        parameter2,
        parameter2,
      ]);
      expect(result).toEqual([
        { serviceId: 'service1', collectionId: 'collection1' },
        { serviceId: 'service2', collectionId: 'collection2' },
      ]);
    });

    it('should return all unique service and collection ids', () => {
      const result = getUniqueServiceAndCollectionIds([
        parameter1,
        parameter2,
        parameter3,
      ]);
      expect(result).toEqual([
        { serviceId: 'service1', collectionId: 'collection1' },
        { serviceId: 'service2', collectionId: 'collection2' },
        { serviceId: 'service3', collectionId: 'collection3' },
      ]);
    });
  });
});
