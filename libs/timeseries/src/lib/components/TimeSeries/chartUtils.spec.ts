/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  DefaultLabelFormatterCallbackParams,
  LineSeriesOption,
  ScatterSeriesOption,
  TooltipComponentOption,
  YAXisComponentOption,
} from 'echarts';
import { produce } from 'immer';
import { cloneDeep } from 'lodash';
import { dateUtils } from '@opengeoweb/shared';
import {
  dateFormatter,
  Y_AXIS_OFFSET,
  getGrid,
  getOption,
  getSeries,
  getTooltipTimeLabel,
  getXAxis,
  getYAxis,
  PlotChart,
  getEChartsSeriesDataByTimestep,
  CalculateTimeSpan,
  paramIntervals,
  getParameterDisplayName,
  roundDecimalValues,
} from './chartUtils';
import { COLOR_NAME_TO_HEX_MAP } from '../../constants';
import { ParameterWithData, Plot, PlotWithData } from './types';

describe('components/TimeSeries/chartUtils', () => {
  const date1 = new Date('2022-05-16T07:00:00.000Z');
  const date2 = new Date('2022-05-16T08:00:00.000Z');
  const date3 = new Date('2022-05-16T09:00:00.000Z');
  const date4 = new Date('2022-05-16T10:00:00.000Z');
  const color1 = 'O';
  const mockPlot: Plot = {
    plotId: 'id0',
    title: 'title0',
  };

  const mockParameterWithData: ParameterWithData = {
    plotId: 'id0',
    plotType: 'line',
    propertyName: 'propertyName',
    timestep: [date1, date2],
    unit: 'unit',
    value: [1, 2],
    color: color1,
    serviceId: 'fmi',
    collectionId: 'ecmwf',
    opacity: 70,
  };

  const plotChart: PlotChart = {
    ...mockPlot,
    parametersByUnit: {
      [mockParameterWithData.unit]: [mockParameterWithData],
    },
    countPreviousUnits: 0,
  };

  const mockParameterWithCustomizedForTemperatureData: ParameterWithData = {
    plotId: 'id0',
    plotType: 'line',
    propertyName: 'Temp',
    timestep: [date1, date2, date3, date4],
    unit: 'unit',
    value: [10, 16, 19, 26],
    color: color1,
    serviceId: 'fmi',
    collectionId: 'ecmwf',
    opacity: 70,
  };

  const customizedPlotChart: PlotChart = {
    ...mockPlot,
    parametersByUnit: {
      [mockParameterWithCustomizedForTemperatureData.unit]: [
        mockParameterWithCustomizedForTemperatureData,
      ],
    },
    countPreviousUnits: 0,
  };

  describe('getYAxis', () => {
    it('should get a common (non-customized) y axis', () => {
      const expectedMin = 1;
      const expectedMax = 2;
      const expectedCustomizedInterval = undefined;

      const customizedInterval = paramIntervals.find(
        (pres) =>
          pres.unit === 'Hubba' ||
          (pres.propertyName && pres.propertyName.toLowerCase() === 'Bubba'),
      )?.interval;

      const yAxis = getYAxis([plotChart]);

      const expectedYAxis: YAXisComponentOption[] = [
        {
          gridIndex: 0,
          name: 'title0',
          type: 'value',
          axisLabel: {
            formatter: `{value} unit`,
          },
          axisLine: {
            show: true,
          },
          axisTick: {
            show: true,
          },
          offset: 0,
          position: 'left',
          nameTextStyle: {
            align: 'left',
          },
          min: expectedMin,
          max: expectedMax,
          interval: expectedCustomizedInterval,
        },
      ];

      expect(customizedInterval).not.toBeDefined();
      expect(yAxis).toEqual(expectedYAxis);
    });

    it('should get y axis with customized range values for temperature parameters', () => {
      const expectedMin = 8;
      const expectedMax = 28;
      const expectedCustomizedInterval = 4;

      const customizedInterval = paramIntervals.find(
        (pres) =>
          pres.propertyName && pres.propertyName.toLowerCase() === 'temp',
      )?.interval;

      const minNumberOfIntervals = paramIntervals.find(
        (pres) =>
          pres.propertyName && pres.propertyName.toLowerCase() === 'temp',
      )?.minNumberOfIntervals;

      const maxNumberOfIntervals = paramIntervals.find(
        (pres) =>
          pres.propertyName && pres.propertyName.toLowerCase() === 'temp',
      )?.maxNumberOfIntervals;

      const yAxis = getYAxis([customizedPlotChart]);

      const expectedYAxis: YAXisComponentOption[] = [
        {
          gridIndex: 0,
          name: 'title0',
          type: 'value',
          axisLabel: {
            formatter: `{value} unit`,
          },
          axisLine: {
            show: true,
          },
          axisTick: {
            show: true,
          },
          nameTextStyle: {
            align: 'left',
          },
          offset: 0,
          position: 'left',
          min: expectedMin,
          max: expectedMax,
          interval: expectedCustomizedInterval,
        },
      ];

      expect(expectedCustomizedInterval).toBeDefined();
      expect(customizedInterval).toBeDefined();
      expect(minNumberOfIntervals).toBeDefined();
      expect(maxNumberOfIntervals).toBeDefined();

      expect(expectedCustomizedInterval).toBeGreaterThan(minNumberOfIntervals!);
      expect(expectedCustomizedInterval).toBeLessThan(maxNumberOfIntervals!);
      expect(expectedCustomizedInterval).toBe(2 * customizedInterval!);

      expect(yAxis).toEqual(expectedYAxis);
    });
  });
  it('should get multiple y axis', () => {
    const plotChart2 = produce(plotChart, (draft) => {
      draft.parametersByUnit['unit2'] = [
        mockParameterWithData,
        mockParameterWithData,
      ];
      draft.parametersByUnit['unit3'] = [mockParameterWithData];
    });

    const yAxis = getYAxis([plotChart2]);

    const expectedYAxis: YAXisComponentOption[] = [
      {
        gridIndex: 0,
        name: 'title0',
        type: 'value',
        axisLabel: {
          formatter: `{value} unit`,
        },
        axisLine: {
          show: true,
        },
        axisTick: {
          show: true,
        },
        offset: 0,
        position: 'left',
        min: 1,
        max: 2,
        nameTextStyle: {
          align: 'left',
        },
        interval: undefined,
      },
      {
        axisLabel: {
          formatter: `{value} unit2`,
        },
        axisLine: {
          show: true,
        },
        axisTick: {
          show: true,
        },
        gridIndex: 0,
        max: 2,
        min: 1,
        interval: undefined,
        name: undefined,
        offset: 0,
        position: 'right',
        type: 'value',
        nameTextStyle: {
          align: 'left',
        },
      },
      {
        axisLabel: {
          formatter: `{value} unit3`,
        },
        axisLine: {
          show: true,
        },
        axisTick: {
          show: true,
        },
        gridIndex: 0,
        max: 2,
        min: 1,
        interval: undefined,
        name: undefined,
        offset: Y_AXIS_OFFSET,
        position: 'left',
        type: 'value',
        nameTextStyle: {
          align: 'left',
        },
      },
    ];
    expect(yAxis).toEqual(expectedYAxis);
  });

  describe('getXAxis', () => {
    it('should get x axis', () => {
      const now = new Date();
      const maxDate = dateUtils.add(now, { days: 10 });
      const minDate = dateUtils.sub(now, { days: 1 });
      const xAxisList = getXAxis([plotChart, plotChart], minDate, maxDate);
      expect(xAxisList.length).toEqual(2);
      expect(
        xAxisList.every((option) => option.type === 'category'),
      ).toBeTruthy();
      expect(
        xAxisList.find((option) => option.gridIndex === 0)?.show,
      ).toBeFalsy();
    });
  });

  describe('getParameterDisplayName', () => {
    const temperature: ParameterWithData = {
      ...mockParameterWithData,
      propertyName: 'Temperature',
      collectionId: 'ecmwf',
    };

    const temperatureDuplicate: ParameterWithData = {
      ...temperature,
      collectionId: 'Some other collection',
    };

    const pressure: ParameterWithData = {
      ...mockParameterWithData,
      propertyName: 'Pressure',
      collectionId: 'noaa',
    };

    it('should always show parameter name with collection name', () => {
      expect(getParameterDisplayName(temperature)).toBe('Temperature - ecmwf');
      expect(getParameterDisplayName(temperatureDuplicate)).toBe(
        'Temperature - Some other collection',
      );
      expect(getParameterDisplayName(pressure)).toBe('Pressure - noaa');
    });
  });

  describe('roundDecimalValues', () => {
    it('should return a whole number as a string', () => {
      expect(roundDecimalValues(10)).toBe('10');
      expect(roundDecimalValues(0)).toBe('0');
      expect(roundDecimalValues(-5)).toBe('-5');
    });

    it('should round decimal numbers to one decimal place as a string', () => {
      expect(roundDecimalValues(10.567)).toBe('10.6');
      expect(roundDecimalValues(5.49)).toBe('5.5');
      expect(roundDecimalValues(-3.1415)).toBe('-3.1');
    });

    it('should handle zero correctly', () => {
      expect(roundDecimalValues(0)).toBe('0');
      expect(roundDecimalValues(0.0)).toBe('0');
      expect(roundDecimalValues(-0.0)).toBe('0');
    });

    it('should handle large numbers correctly', () => {
      expect(roundDecimalValues(100000.5678)).toBe('100000.6');
      expect(roundDecimalValues(-99999.999)).toBe('-100000.0');
    });

    it('should handle Infinity and NaN gracefully', () => {
      expect(roundDecimalValues(Infinity)).toBe('Infinity');
      expect(roundDecimalValues(-Infinity)).toBe('-Infinity');
      expect(roundDecimalValues(NaN)).toBe('no data');
    });

    it('should always return a string', () => {
      expect(typeof roundDecimalValues(10)).toBe('string');
      expect(typeof roundDecimalValues(10.5)).toBe('string');
      expect(typeof roundDecimalValues(0)).toBe('string');
    });
  });

  describe('getEChartsSeriesDataByTimestep', () => {
    it('should return entries with correct values for all timesteps', () => {
      expect(
        getEChartsSeriesDataByTimestep([date1], mockParameterWithData).length,
      ).toEqual(1);
      const eChartSeries = getEChartsSeriesDataByTimestep(
        [date1, date2],
        mockParameterWithData,
      );
      // one entry for each timesteps
      expect(eChartSeries.length).toEqual(2);
      // first timestep has correct value
      const firstTimestep = eChartSeries.find(
        (entry) => entry[0] === date1.toISOString(),
      );
      expect(
        firstTimestep && firstTimestep[1] === mockParameterWithData.value[0],
      ).toBeTruthy();
      // second timestep has correct value
      const secondTimestep = eChartSeries.find(
        (entry) => entry[0] === date2.toISOString(),
      );
      expect(
        secondTimestep && secondTimestep[1] === mockParameterWithData.value[1],
      ).toBeTruthy();
    });

    it('should return NaN when specified timestep does not exist in ParameterWithData', () => {
      const eChartSeries = getEChartsSeriesDataByTimestep(
        [new Date(0)],
        mockParameterWithData,
      );
      expect(eChartSeries[0][1]).toBeNaN();
    });

    it('should keep null values if provided by backend', async () => {
      // @ts-expect-error simulate parsed array of numbers including null
      const parsedValues: number[] = [null, 2];
      const parameterWithNullData = {
        ...mockParameterWithData,
        timestep: [date1, date2],
        unit: 'unit',
        value: parsedValues,
      };
      const eChartSeries = getEChartsSeriesDataByTimestep(
        [date1, date2],
        parameterWithNullData,
      );
      expect(eChartSeries).toEqual([
        [date1.toISOString(), null],
        [date2.toISOString(), 2],
      ]);
    });
  });

  describe('getSeries()', () => {
    it('should convert parameter to series', () => {
      const series = getSeries([plotChart]);

      expect(series).toEqual([
        {
          data: [
            [date1.toISOString(), 1],
            [date2.toISOString(), 2],
          ],
          itemStyle: {
            color:
              COLOR_NAME_TO_HEX_MAP[
                color1 as keyof typeof COLOR_NAME_TO_HEX_MAP
              ],
            opacity: mockParameterWithData.opacity! / 100,
          },
          lineStyle: {
            color:
              COLOR_NAME_TO_HEX_MAP[
                color1 as keyof typeof COLOR_NAME_TO_HEX_MAP
              ],
            opacity: mockParameterWithData.opacity! / 100,
          },
          name: 'propertyName - ecmwf',
          type: 'line',
          xAxisIndex: 0,
          yAxisIndex: 0,
        },
      ]);
    });
    it('should return correct plot type and styles for area plot', () => {
      const plotChartWithAreaPlot = cloneDeep(plotChart);
      plotChartWithAreaPlot.parametersByUnit[
        mockParameterWithData.unit
      ][0].plotType = 'area';

      const series: LineSeriesOption[] = getSeries([
        plotChartWithAreaPlot,
      ]) as LineSeriesOption[];
      expect(series[0].type).toBe('line');
      expect(series[0].areaStyle).toBeTruthy();
    });
    it('should return correct styles for scatter plot', () => {
      const plotChartWithAreaPlot = cloneDeep(plotChart);
      plotChartWithAreaPlot.parametersByUnit[
        mockParameterWithData.unit
      ][0].plotType = 'scatter';

      const series: ScatterSeriesOption[] = getSeries([
        plotChartWithAreaPlot,
      ]) as ScatterSeriesOption[];
      expect(series[0].type).toBe('scatter');
      expect(series[0].symbolSize).toBe(6);
    });
  });

  describe('getGrid', () => {
    it('should get grid for plot with one parameter', () => {
      const grid = getGrid([plotChart]);

      expect(grid).toEqual([
        {
          height: '100px',
          left: '60px',
          right: '60px',
          top: '70px',
        },
      ]);
    });

    it('should get grid for plot with two parameter with different units', () => {
      const plotChart2 = produce(plotChart, (draft) => {
        draft.parametersByUnit['unit2'] = [
          {
            ...mockParameterWithData,
            unit: 'unit2',
          },
        ];
      });
      const grid = getGrid([plotChart2]);

      expect(grid).toEqual([
        {
          height: '100px',
          left: '60px',
          right: '60px',
          top: '70px',
        },
      ]);
    });

    it('should get grid for two plots with parameters with same unit', () => {
      const plotChart2: PlotChart = {
        ...plotChart,
        plotId: 'plotId2',
      };
      const grid = getGrid([plotChart, plotChart2]);

      expect(grid).toEqual([
        {
          height: '100px',
          left: '60px',
          right: '60px',
          top: '70px',
        },
        {
          height: '100px',
          left: '60px',
          right: '60px',
          top: '220px',
        },
      ]);
    });
  });

  describe('getOption()', () => {
    it('should get option', () => {
      const option = getOption([
        {
          ...mockPlot,
          parametersWithData: [
            mockParameterWithData,
            { ...mockParameterWithData, propertyName: 'propertyName1' },
          ],
        },
      ]);
      expect(option.xAxis).toHaveLength(1);
      expect(option.yAxis).toHaveLength(1);
      expect(option.series).toHaveLength(2);
      expect(option.grid).toHaveLength(1);
      expect(option.tooltip).toEqual({
        confine: true,
        extraCssText: 'white-space:pre-wrap;',
        trigger: 'axis',
        formatter: expect.any(Function),
      });
      expect(option.dataZoom).toEqual([
        {
          show: true,
          realtime: true,
          left: 70,
          right: 70,
          xAxisIndex: [0, 1],
          labelFormatter: expect.any(Function),
        },
      ]);
      const { formatter } = option.tooltip as TooltipComponentOption;
      if (formatter) {
        const formatCallback = formatter as unknown as (
          params: DefaultLabelFormatterCallbackParams[],
        ) => void;
        const paramLabel = formatCallback([
          {
            seriesName: 'param1',
            data: [date1.toISOString(), 1],
            componentType: '',
            componentSubType: '',
            componentIndex: 0,
            name: '',
            dataIndex: 0,
            value: '',
            $vars: [],
          },
          {
            seriesName: 'param2',
            data: [date1.toISOString(), 2],
            componentType: '',
            componentSubType: '',
            componentIndex: 0,
            name: '',
            dataIndex: 0,
            value: '',
            $vars: [],
          },
        ]);
        expect(paramLabel).toEqual(
          '<b>Mon 16 May 07:00</b><br />param1: 1<br />param2: 2',
        );
      }
    });
  });

  describe('dateFormatter', () => {
    it('should format date', () => {
      const date = '2022-01-13T12:00:00.000Z';
      const formattedDate = dateFormatter(date);

      expect(formattedDate).toEqual('Thu 12:00');
    });
  });

  describe('getTooltipTimeLabel', () => {
    it('should format tooltip date', () => {
      const date = '2022-01-13T12:00:00.000Z';
      const formattedDate = getTooltipTimeLabel(date);

      expect(formattedDate).toEqual('Thu 13 Jan 12:00');
    });
  });

  describe('CalculateTimeSpan', () => {
    const now = new Date('2024-07-09T12:00:00Z');

    beforeEach(() => {
      jest.useFakeTimers().setSystemTime(now);
    });

    afterAll(() => {
      jest.useRealTimers();
    });

    const mockPlotWithData = (): PlotWithData[] => [
      {
        parametersWithData: [
          {
            timestep: [],
            value: [],
            unit: '',
            plotId: '',
            propertyName: '',
            plotType: 'bar',
            serviceId: '',
            collectionId: '',
          },
        ],
        title: '',
        plotId: '',
      },
    ];

    const createMockPlotsWithData = (timesteps: Date[][][]): PlotWithData[] => {
      return timesteps.map((parameterTimesteps, index) => ({
        ...mockPlotWithData()[0],
        parametersWithData: parameterTimesteps.map((timestep, i) => ({
          ...mockPlotWithData()[0].parametersWithData[0],
          timestep,
          plotId: `${index}-${i}`,
        })),
      }));
    };

    it('should contain only observation', () => {
      const plotsWithData = createMockPlotsWithData([
        [[new Date('2024-07-07T12:00:00Z'), new Date('2024-07-08T12:00:00Z')]],
      ]);

      const results = CalculateTimeSpan(plotsWithData);
      expect(results).toEqual({
        left: dateUtils.sub(now, { days: 1 }),
        right: now,
      });
    });
    it('should contain only forecast', () => {
      const plotsWithData = createMockPlotsWithData([
        [[new Date('2024-07-10T12:00:00Z'), new Date('2024-07-11T12:00:00Z')]],
      ]);
      const result = CalculateTimeSpan(plotsWithData);
      expect(result).toEqual({
        left: now,
        right: dateUtils.add(now, { days: 10 }),
      });
    });
    it('should contain both observation and forecast', () => {
      const plotsWithData = createMockPlotsWithData([
        [[new Date('2024-07-08T12:00:00Z'), new Date('2024-07-10T12:00:00Z')]],
      ]);

      const result = CalculateTimeSpan(plotsWithData);
      expect(result).toEqual({
        left: dateUtils.sub(now, { days: 1 }),
        right: dateUtils.add(now, { days: 10 }),
      });
    });

    it('contains both observation and forecast in multiple parameters with multiple timesteps', () => {
      const plotsWithData = createMockPlotsWithData([
        [
          [new Date('2024-07-05T12:00:00Z'), new Date('2024-07-06T12:00:00Z')],
          [new Date('2024-07-08T12:00:00Z'), new Date('2024-07-09T12:00:00Z')],
        ],
        [
          [new Date('2024-07-10T12:00:00Z'), new Date('2024-07-11T12:00:00Z')],
          [new Date('2024-07-12T12:00:00Z'), new Date('2024-07-13T12:00:00Z')],
        ],
      ]);

      const result = CalculateTimeSpan(plotsWithData);
      expect(result).toEqual({
        left: dateUtils.sub(now, { days: 1 }),
        right: dateUtils.add(now, { days: 10 }),
      });
    });
    it('does not contain forecast neither observation', () => {
      const plotsWithData: PlotWithData[] = [];

      const results = CalculateTimeSpan(plotsWithData);
      expect(results).toEqual({
        left: now,
        right: now,
      });
    });
  });
});
