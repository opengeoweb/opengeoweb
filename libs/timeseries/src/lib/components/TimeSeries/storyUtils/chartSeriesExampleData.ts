/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { produce } from 'immer';

import { dateUtils } from '@opengeoweb/shared';
import { PlotWithData } from '../types';

const exampleTemperatureValues = [
  4, 4, 4, 3, 3, 4, 4, 4, 5, 5, 5, 5, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 6, 6,
  6, 6,
];

const examplePrecipitationValues = [
  0, 0.1, 0.1, 0.2, 0.3, 0.1, 0.1, 0.1, 0.2, 0.3, 0.5, 0.5, 0.4, 0.3, 0.3, 0.3,
  0.5, 0.5, 1, 1, 1.1, 1.1, 0.3, 0.4, 0.1, 0.1, 0.2, 0.2,
];

const startTime = dateUtils.isoStringToDate('2022-01-01T12:00:00.000Z')!;
const timeSteps = exampleTemperatureValues.map((_value, index) =>
  dateUtils.add(startTime, { hours: index }),
);

export const plotsWithData: PlotWithData[] = [
  {
    plotId: 'plot_1',
    title: 'Plot 1',
    parametersWithData: [
      {
        plotId: 'plot_1',
        unit: '°C',
        propertyName: 'Temperature',
        plotType: 'line',
        timestep: timeSteps,
        value: exampleTemperatureValues,
        serviceId: 'fmi',
        collectionId: 'ecmwf',
      },
    ],
  },
  {
    plotId: 'plot_2',
    title: 'Plot 2',
    parametersWithData: [
      {
        plotId: 'plot_2',
        unit: 'mm',
        propertyName: 'Precipitation1h',
        plotType: 'bar',
        timestep: timeSteps,
        value: examplePrecipitationValues,
        serviceId: 'fmi',
        collectionId: 'ecmwf',
      },
    ],
  },
];

export const plotsWithNullData = produce(plotsWithData[0], (draft) => {
  const values = [
    ...new Array(10).fill(3),
    null,
    ...new Array(5).fill(4),
    null,
    null,
    ...new Array(10).fill(5),
  ];
  draft.parametersWithData[0].value = values;
});
