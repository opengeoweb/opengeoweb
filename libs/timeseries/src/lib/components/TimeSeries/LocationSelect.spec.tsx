/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { render, screen } from '@testing-library/react';
import { FeatureCollection, GeometryObject, Point } from 'geojson';
import { MapLocation } from '@opengeoweb/webmap-react';
import { TFunction } from 'i18next';
import { GeoJsonWithService } from './types';
import { LocationSelect } from './LocationSelect';
import { translateKeyOutsideComponents } from '../../utils/i18n';

describe('LocationSelect', () => {
  const mockSetMapPinLocation = jest.fn();
  const mockSetSelectedLocation = jest.fn();
  const mockSetTimeSeriesLocation = jest.fn();
  const mockSetSelectedFeatureIndex = jest.fn();
  const mockAddTimeSeriesData = jest.fn();

  const defaultProps = {
    splitGeoJson: [] as GeoJsonWithService[],
    selectedLocation: '',
    disableMapPin: false,
    geojson: { features: [] } as unknown as FeatureCollection<Point>,
    panelId: 'panelId',
    selectedFeature: {} as MapLocation,
    setMapPinLocation: mockSetMapPinLocation,
    setSelectedLocation: mockSetSelectedLocation,
    setTimeSeriesLocation: mockSetTimeSeriesLocation,
    setSelectedFeatureIndex: mockSetSelectedFeatureIndex,
    featureSelect: mockAddTimeSeriesData,
    t: translateKeyOutsideComponents as unknown as TFunction,
  };

  it('should set be able to set location if geojson features are present', () => {
    const geojson = {
      features: [
        {
          id: '1',
          properties: { name: 'Location1' },
          geometry: { coordinates: [1, 1] },
        },
        {
          id: '2',
          properties: { name: 'Location2' },
          geometry: {
            coordinates: [
              [
                [-73.987235, -33.768378],
                [-73.987235, 5.244486],
                [-34.729993, 5.244486],
                [-34.729993, -33.768378],
                [-73.987235, -33.768378],
              ],
            ],
          },
        },
      ],
      type: 'FeatureCollection',
    } as unknown as FeatureCollection<GeometryObject>;

    render(<LocationSelect {...defaultProps} geojson={geojson} />);
    const locationSelectButton = screen.getByTestId('locationSelectButton');
    expect(locationSelectButton).toBeInTheDocument();
  });
});
