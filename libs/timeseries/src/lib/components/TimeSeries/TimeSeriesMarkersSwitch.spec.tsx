/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { TimeSeriesMarkersSwitch } from './TimeSeriesMarkersSwitch';
import { TimeseriesI18nProvider } from '../Providers';

describe('src/components/TimeSeries/TimeSeriesMarkersSwitch', () => {
  it('calls the callback function with correct value on toggle - part 1', async () => {
    const callback = jest.fn();
    render(
      <TimeseriesI18nProvider>
        <TimeSeriesMarkersSwitch
          hideCircles={false}
          setHideCircles={callback}
        />
      </TimeseriesI18nProvider>,
    );

    expect(callback).not.toHaveBeenCalled();

    const user = userEvent.setup();
    await user.click(screen.getByRole('checkbox'));

    expect(callback).toHaveBeenCalledWith(true);
  });

  it('calls the callback function with correct value on toggle - part 2', async () => {
    const callback = jest.fn();
    render(
      <TimeseriesI18nProvider>
        <TimeSeriesMarkersSwitch hideCircles={true} setHideCircles={callback} />
      </TimeseriesI18nProvider>,
    );

    expect(callback).not.toHaveBeenCalled();

    const user = userEvent.setup();
    await user.click(screen.getByRole('checkbox'));

    expect(callback).toHaveBeenCalledWith(false);
  });

  it('should be toggled if hideCircles is set to false', () => {
    const callback = jest.fn();
    render(
      <TimeseriesI18nProvider>
        <TimeSeriesMarkersSwitch
          hideCircles={false}
          setHideCircles={callback}
        />
      </TimeseriesI18nProvider>,
    );

    expect(screen.getByRole('checkbox')).toBeChecked();
  });

  it('should be untoggled if hideCircles is set to true', () => {
    const callback = jest.fn();
    render(
      <TimeseriesI18nProvider>
        <TimeSeriesMarkersSwitch hideCircles={true} setHideCircles={callback} />
      </TimeseriesI18nProvider>,
    );
    expect(screen.getByRole('checkbox')).not.toBeChecked();
  });
});
