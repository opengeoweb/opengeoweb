/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Box, Typography } from '@mui/material';
import { SwitchButton } from '@opengeoweb/shared';
import React from 'react';
import { useTimeseriesTranslation } from '../../utils/i18n';

export interface TimeSeriesMarkersSwitchProps {
  hideCircles: boolean;
  setHideCircles: React.Dispatch<React.SetStateAction<boolean>>;
}

export const TimeSeriesMarkersSwitch: React.FC<
  TimeSeriesMarkersSwitchProps
> = ({ hideCircles, setHideCircles }) => {
  const { t } = useTimeseriesTranslation();
  return (
    <Box
      sx={{
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        'label:first-of-type': {
          marginLeft: '5px',
          marginRight: '-5px',
        },
        'label:last-of-type': {
          marginLeft: '-5px',
        },
      }}
    >
      <Typography variant="body2" sx={{ marginTop: '4px' }}>
        {t('timeseries-toggle-location-markers-label')}
      </Typography>
      <Box sx={{ marginTop: '-5px', minWidth: '95px' }}>
        <SwitchButton
          activeLabel="ON"
          inActiveLabel="OFF"
          tooltip={t('timeseries-toggle-location-markers-tooltip')}
          checked={!hideCircles}
          onChange={(event) => {
            setHideCircles(!event.target.checked);
          }}
        />
      </Box>
    </Box>
  );
};
