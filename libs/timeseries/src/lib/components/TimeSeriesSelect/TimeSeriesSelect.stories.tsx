/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';

import { SystemScope, TimeSeriesService } from '@opengeoweb/shared';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import type { Meta, StoryObj } from '@storybook/react';
import { useDispatch } from 'react-redux';
import { timeSeriesActions } from '../../store';
import { createMockStore } from '../../store/store';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import { Plot } from '../TimeSeries/types';
import { TimeSeriesSelect } from './TimeSeriesSelect';

const meta: Meta<typeof TimeSeriesSelect> = {
  title: 'components/TimeSeriesSelect',
  component: TimeSeriesSelect,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the TimeSeriesSelect',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof TimeSeriesSelect>;

const plot: Plot = {
  title: 'Plot 1',
  plotId: 'Plot_1',
  parameters: [
    {
      plotId: 'Plot_1',
      unit: '°C',
      propertyName: 'Temperature',
      plotType: 'line',
      serviceId: 'fmi_EDR',
      collectionId: 'ecmwf',
    },
    {
      plotId: 'Plot_2',
      unit: 'mm',
      propertyName: 'Precipitation1h',
      plotType: 'bar',
      serviceId: 'fmi_EDR',
      collectionId: 'ecmwf',
    },
    {
      plotId: 'Plot_3',
      propertyName: 'GeopHeight',
      unit: 'm2 s-2',
      plotType: 'line',
      serviceId: 'fmi_EDR',
      collectionId: 'ecmwf',
    },
    {
      plotId: 'Plot_4',
      propertyName: 'DewPoint',
      unit: 'C',
      plotType: 'line',
      serviceId: 'fmi_EDR',
      collectionId: 'ecmwf',
    },
    {
      plotId: 'Plot_5',
      propertyName: 'Humidity',
      unit: '%',
      plotType: 'line',
      serviceId: 'fmi_EDR',
      collectionId: 'ecmwf',
    },
  ],
};

const TimeSeriesSelectDemo: React.FC<{
  services?: TimeSeriesService[];
  searchFilter?: string;
}> = ({
  services = [
    {
      id: 'ogc_fake_api',
      type: 'OGC',
      url: 'someURL',
      description: 'Fake OGC service',
      name: 'Fake OGC',
      scope: 'system',
    },
  ],
  searchFilter = 'OGC',
}) => {
  const dispatch = useDispatch();

  dispatch(timeSeriesActions.setSearchFilter({ filterText: searchFilter }));
  const filterChips = services.map((service) => {
    return {
      serviceId: service.id,
      serviceUrl: service.url,
      serviceName: service.name,
      type: service.type,
      scope: 'system' as SystemScope,
    };
  });
  dispatch(timeSeriesActions.setServiceFilterChipsInStore(filterChips));

  // Add services to store for options dialog to see them
  dispatch(timeSeriesActions.addServices({ timeSeriesServices: services }));

  return (
    <div style={{ height: '100vh', backgroundColor: 'grey' }}>
      <TimeSeriesSelect
        isOpen={true}
        order={0}
        onClose={(): void => {}}
        onMouseDown={(): void => {}}
        selectPlot={plot}
        handleAddOrRemoveClick={(): void => {}}
        services={services}
      />
    </div>
  );
};

export const Component: Story = {
  args: {
    isOpen: true,
    order: 0,
    onClose: (): void => {},
    onMouseDown: (): void => {},
    selectPlot: plot,
    handleAddOrRemoveClick: (): void => {},
    services: [
      {
        id: 'norway_EDR',
        type: 'EDR',
        url: 'https://interpol.met.no',
        description: 'Norwegian EDR service',
        name: 'MetNorway',
        scope: 'system',
      },
      {
        id: 'fmi_EDR',
        type: 'EDR',
        url: 'https://opendata.fmi.fi/edr',
        description: 'Finnish EDR service',
        name: 'FMI',
        scope: 'system',
      },
      {
        id: 'nl_EDR',
        type: 'EDR',
        url: 'https://gw-edr.pub.knmi.cloud/edr',
        description: 'Netherlands EDR service',
        name: 'KNMI',
      },
      {
        id: 'ogc_fake_api',
        type: 'OGC',
        url: 'someURL',
        description: 'Fake OGC service',
        name: 'Fake OGC',
        scope: 'system',
      },
    ],
  },
  render: (props) => (
    <TimeSeriesThemeStoreProvider store={createMockStore()}>
      <div style={{ height: 500 }}>
        <TimeSeriesSelect {...props} />
      </div>
    </TimeSeriesThemeStoreProvider>
  ),
};

export const TimeSeriesSelectDemoWithEDR = (): React.ReactElement => (
  <TimeSeriesThemeStoreProvider store={createMockStore()} theme={lightTheme}>
    <TimeSeriesSelectDemo
      services={[
        {
          id: 'norway_EDR',
          type: 'EDR',
          url: 'https://interpol.met.no',
          description: 'Norwegian EDR service',
          name: 'MetNorway',
          scope: 'system',
        },
        {
          id: 'fmi_EDR',
          type: 'EDR',
          url: 'https://opendata.fmi.fi/edr',
          description: 'Finnish EDR service',
          name: 'FMI',
          scope: 'system',
        },
        {
          id: 'nl_EDR',
          type: 'EDR',
          url: 'https://gw-edr.pub.knmi.cloud/edr',
          description: 'Netherlands EDR service',
          name: 'KNMI',
        },
        {
          id: 'ogc_fake_api',
          type: 'OGC',
          url: 'someURL',
          description: 'Fake OGC service',
          name: 'Fake OGC',
          scope: 'system',
        },
      ]}
      searchFilter=""
    />
  </TimeSeriesThemeStoreProvider>
);
TimeSeriesSelectDemoWithEDR.storyName = 'TimeSeriesSelect with EDR';

export const TimeSeriesSelectDemoLightTheme: Story = {
  render: () => (
    <TimeSeriesThemeStoreProvider store={createMockStore()} theme={lightTheme}>
      <TimeSeriesSelectDemo />
    </TimeSeriesThemeStoreProvider>
  ),
  tags: ['snapshot'],
};
TimeSeriesSelectDemoLightTheme.storyName = 'TimeSeriesSelect light theme';

export const TimeSeriesSelectDemoDarkTheme: Story = {
  render: () => (
    <TimeSeriesThemeStoreProvider store={createMockStore()} theme={darkTheme}>
      <TimeSeriesSelectDemo />
    </TimeSeriesThemeStoreProvider>
  ),
  tags: ['snapshot'],
};

TimeSeriesSelectDemoDarkTheme.storyName = 'TimeSeriesSelect dark theme';
