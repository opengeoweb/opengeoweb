/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { uiTypes } from '@opengeoweb/store';
import { TFunction } from 'i18next';
import { TimeSeriesSelectButtonConnect } from './TimeSeriesSelectButtonConnect';
import { createMockStore } from '../../store/store';
import { PlotPreset } from '../TimeSeries/types';
import { TimeSeriesThemeStoreProvider } from '../Providers';

describe('components/TimeSeriesSelect/TimeSeriesSelectButtonConnect', () => {
  it('should dispatch action with passed in mapid when clicked', async () => {
    const mapId = 'mapId';
    const mockState = {
      ui: {
        dialogs: {
          timeSeriesSelect: {
            type: uiTypes.DialogTypes.TimeSeriesSelect,
            activeMapId: 'differentMapId',
            isOpen: true,
          },
        },
        order: ['timeSeriesSelect'],
      },
      timeSeries: { plotPreset: { mapId } as PlotPreset },
    };
    const store = createMockStore(mockState);
    const t = ((key: string) => key) as unknown as TFunction;

    const mockOnClick = jest.fn();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectButtonConnect
          onClick={mockOnClick}
          isEnabled={true}
          t={t}
          plotId="testId1"
          selectPlotId="testId1"
        />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(store.getState().ui.dialogs.timeSeriesSelect.activeMapId).toEqual(
      'differentMapId',
    );

    fireEvent.click(screen.getByTestId('timeSeriesSelectButton'));
    expect(store.getState().ui.dialogs.timeSeriesSelect.activeMapId).toEqual(
      mapId,
    );
    expect(mockOnClick).toHaveBeenCalledTimes(1);
  });
  it('should not dispatch action if dialog is already open for corresponding map', () => {
    const mapId = 'mapId';
    const mockState = {
      ui: {
        dialogs: {
          timeSeriesSelect: {
            type: uiTypes.DialogTypes.TimeSeriesSelect,
            activeMapId: mapId,
            isOpen: true,
          },
        },
        order: ['timeSeriesSelect'],
      },
      timeSeries: { plotPreset: { mapId } as PlotPreset },
    };
    const store = createMockStore(mockState);
    const t = ((key: string) => key) as unknown as TFunction;

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectButtonConnect
          onClick={jest.fn()}
          isEnabled={true}
          t={t}
          plotId="testId1"
          selectPlotId="testId1"
        />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(store.getState().ui.dialogs.timeSeriesSelect.activeMapId).toEqual(
      mapId,
    );

    fireEvent.click(screen.getByTestId('timeSeriesSelectButton'));
    expect(store.getState().ui.dialogs.timeSeriesSelect.activeMapId).toEqual(
      mapId,
    );
  });
});
