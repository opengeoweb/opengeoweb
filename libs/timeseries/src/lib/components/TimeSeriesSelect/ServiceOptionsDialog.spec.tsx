/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { TimeSeriesService } from '@opengeoweb/shared';

import { ServiceOptionsDialog } from './ServiceOptionsDialog';
import { TimeSeriesThemeStoreProvider } from '../Providers';
import { createMockStore } from '../../store/store';

describe('src/lib/components/TimeSeriesSelect/ServiceOptionsDialog', () => {
  it('should show no services message if no services passed', async () => {
    const props = {
      services: [],
      parametersInUse: [],
      openServicePopup: jest.fn(),
      openNewServicePopup: jest.fn(),
      removeService: jest.fn(),
    };
    const store = createMockStore();
    const user = userEvent.setup();

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...props} />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(screen.getByText('No services found')).toBeTruthy();

    // Press Esc to close the dialog
    await user.keyboard('{Escape}');
    expect(screen.queryByRole('menu')).toBeFalsy();
  });

  it('should open new service popup on add button click', async () => {
    const service: TimeSeriesService = {
      id: 'testid',
      name: 'test',
      url: 'testurl.fi',
      description: 'testdesc',
      scope: 'user',
      type: 'EDR',
    };

    const props = {
      services: [service],
      parametersInUse: [],
      openServicePopup: jest.fn(),
      openNewServicePopup: jest.fn(),
      removeService: jest.fn(),
    };
    const store = createMockStore();

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...props} />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(screen.getByTestId('add-new-ts-service')).toBeTruthy();

    fireEvent.click(screen.getByTestId('add-new-ts-service'));

    await waitFor(() => {
      expect(props.openNewServicePopup).toHaveBeenCalledTimes(1);
    });
  });

  it('should open service info popup on info button click', async () => {
    const service: TimeSeriesService = {
      id: 'testid',
      name: 'test',
      url: 'testurl.fi',
      description: 'testdesc',
      scope: 'system',
      type: 'EDR',
    };

    const props = {
      services: [service],
      parametersInUse: [],
      openServicePopup: jest.fn(),
      openNewServicePopup: jest.fn(),
      removeService: jest.fn(),
    };
    const store = createMockStore();

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...props} />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(screen.getByTestId('show-button-testid')).toBeTruthy();

    fireEvent.click(screen.getByTestId('show-button-testid'));

    await waitFor(() => {
      expect(props.openServicePopup).toHaveBeenCalledTimes(1);
    });

    await waitFor(() => {
      expect(props.openServicePopup).toHaveBeenCalledWith(
        'show',
        'testid',
        'testurl.fi',
      );
    });
  });

  it('should open service edit popup on edit button click', async () => {
    const service: TimeSeriesService = {
      id: 'testid',
      name: 'test',
      url: 'testurl.fi',
      description: 'testdesc',
      scope: 'user',
      type: 'EDR',
    };

    const props = {
      services: [service],
      parametersInUse: [],
      openServicePopup: jest.fn(),
      openNewServicePopup: jest.fn(),
      removeService: jest.fn(),
    };
    const store = createMockStore();

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...props} />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(screen.getByTestId('edit-button-testid')).toBeTruthy();

    fireEvent.click(screen.getByTestId('edit-button-testid'));

    await waitFor(() => {
      expect(props.openServicePopup).toHaveBeenCalledTimes(1);
    });

    await waitFor(() => {
      expect(props.openServicePopup).toHaveBeenCalledWith(
        'edit',
        'testid',
        'testurl.fi',
      );
    });
  });

  it('should call remove function on delete button click', async () => {
    const service: TimeSeriesService = {
      id: 'testid',
      name: 'test',
      url: 'testurl.fi',
      description: 'testdesc',
      scope: 'user',
      type: 'EDR',
    };

    const props = {
      services: [service],
      parametersInUse: [],
      openServicePopup: jest.fn(),
      openNewServicePopup: jest.fn(),
      removeService: jest.fn(),
    };
    const store = createMockStore();

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...props} />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(screen.getByTestId('remove-button-testid')).toBeTruthy();

    fireEvent.click(screen.getByTestId('remove-button-testid'));

    await waitFor(() => {
      expect(props.removeService).toHaveBeenCalledTimes(1);
    });

    await waitFor(() => {
      expect(props.removeService).toHaveBeenCalledWith(
        'testid',
        'Service test has been deleted',
      );
    });
  });
});
