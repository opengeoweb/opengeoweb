/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { TimeSeriesSelect } from './TimeSeriesSelect';
import { ogcParameters } from './ogcParameters';
import { TimeSeriesThemeStoreProvider } from '../Providers';
import { createMockStore } from '../../store/store';

describe('components/TimeSeriesSelect', () => {
  it('should render', async () => {
    const store = createMockStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelect
          isOpen={true}
          order={0}
          onClose={jest.fn()}
          onMouseDown={jest.fn()}
          selectPlot={{
            title: 'Plot 1',
            plotId: 'plotId1',
            parameters: [
              {
                plotId: 'plotId1',
                plotType: 'line',
                propertyName: 'Temperature',
                unit: '°C',
                serviceId: 'serviceId',
                collectionId: 'ecmwf',
              },
            ],
          }}
          handleAddOrRemoveClick={jest.fn()}
          services={[
            {
              id: 'serviceId',
              type: 'OGC',
              url: 'serviceUrl',
              description: 'desc',
              name: 'OGC',
              scope: 'system',
            },
          ]}
        />
      </TimeSeriesThemeStoreProvider>,
    );
    expect(
      screen.getByRole('heading', { name: /timeseries select for plot 1/i }),
    ).toBeTruthy();

    const row = await screen.findByText('collectionOGC');
    fireEvent.click(row);

    expect(screen.getByText(ogcParameters[0].propertyName)).toBeTruthy();
  });
});
