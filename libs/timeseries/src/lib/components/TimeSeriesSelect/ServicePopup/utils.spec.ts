/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { TimeSeriesService } from '@opengeoweb/shared';
import {
  loadEDRService,
  validateServiceName,
  validateServiceUrl,
} from './utils';

const mockServices: TimeSeriesService[] = [
  {
    name: 'Test1',
    description: 'Some description.',
    id: 't1',
    type: 'EDR',
    url: 'https://www.test.nl',
    scope: 'user',
  },
  {
    name: 'TeSt_2',
    description: 'Some description.',
    id: 'test_2',
    type: 'EDR',
    url: 'testurl2.com',
    scope: 'system',
  },
];

// eslint-disable-next-line no-unused-vars
const mockFetchEdrCollections = jest.fn((x: string) =>
  Promise.resolve({ title: 'A', description: 'B' }),
);

jest.mock('../../../utils/edrUtils', () => ({
  validateServiceUrl: (
    x: string,
  ): Promise<{ title?: string; description?: string }> =>
    mockFetchEdrCollections(x),
}));

describe('src/lib/components/TimeSeriesSelect/ServicePopup/ServicePopup', () => {
  describe('validateServiceName', () => {
    it('should validate ServiceName for existing names', () => {
      expect(validateServiceName('test', [])).toBeTruthy();
      expect(validateServiceName('testing', mockServices)).toBeTruthy();

      expect(validateServiceName('test1', mockServices)).toEqual(
        'timeseries-validations-name-existing',
      );
      expect(validateServiceName('Test1', mockServices)).toEqual(
        'timeseries-validations-name-existing',
      );
    });
    it('should validate but ignore own name', () => {
      expect(validateServiceName('test1', mockServices, 'test1')).toBeTruthy();
      expect(validateServiceName('Test1', mockServices, 'Test1')).toBeTruthy();

      expect(validateServiceName('test1', mockServices, 'test_2')).toEqual(
        'timeseries-validations-name-existing',
      );
    });
  });

  describe('validateServiceUrl', () => {
    it('should validate validateServiceUrl URL is valid', () => {
      expect(validateServiceUrl('http://www.test.nl', [])).toBeTruthy();
      expect(validateServiceUrl('https://www.test.nl', [])).toBeTruthy();

      expect(validateServiceUrl('', [])).toEqual(
        'timeseries-validations-service-valid-url',
      );
      expect(validateServiceUrl(undefined!, [])).toEqual(
        'timeseries-validations-service-valid-url',
      );
      expect(validateServiceUrl('://www.test.nl', [])).toEqual(
        'timeseries-validations-service-valid-url',
      );
      expect(validateServiceUrl('www.test.nl', [])).toEqual(
        'timeseries-validations-service-valid-url',
      );
      expect(validateServiceUrl('test.nl', [])).toEqual(
        'timeseries-validations-service-valid-url',
      );
    });

    it('should validate validateServiceUrl for existing Urls', () => {
      expect(
        validateServiceUrl('http://www.test.nl', mockServices),
      ).toBeTruthy();
      expect(validateServiceUrl('https://www.test.nl', mockServices)).toEqual(
        'timeseries-validations-service-existing',
      );
    });
  });

  describe('loadEDRService', () => {
    it('should fetch layers', async () => {
      const service = await loadEDRService('testUrl', mockServices);
      expect(service).toMatchObject({
        id: '',
        name: 'A',
        url: 'testUrl',
        description: 'B',
        type: 'EDR',
        scope: 'user',
      });
      expect(mockFetchEdrCollections.mock.calls).toHaveLength(1);
      expect(mockFetchEdrCollections.mock.calls[0][0]).toBe('testUrl');
    });

    it('should return existing one', async () => {
      const service = await loadEDRService('https://www.test.nl', mockServices);
      expect(service).toMatchObject(mockServices[0]);
      expect(mockFetchEdrCollections.mock.calls).toHaveLength(0);
    });
  });
});
