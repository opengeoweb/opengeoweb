/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { Provider } from 'react-redux';
import { SnackbarWrapperConnect } from '@opengeoweb/snackbar';
import { ThemeWrapper, lightTheme } from '@opengeoweb/theme';
import { TimeSeriesTypes } from '../../../store';
import { ServicePopupConnect } from './ServicePopupConnect';
import { createMockStore } from '../../../store/store';

export default {
  title: 'components/TimeSeriesSelect/ServicePopupConnect',
  tags: ['!autodocs'],
};

const mockState: TimeSeriesTypes.TimeSeriesModuleState = {
  timeSeries: {
    timeseriesSelect: {
      filters: {
        searchFilter: '',
        allServiceFilterChipsEnabled: true,
        serviceFilterChips: {
          entities: {},
          ids: [],
        },
      },
      servicePopup: {
        variant: 'add',
        isOpen: true,
        serviceId: 'exampleservice',
        url: 'exampleservice',
      },
    },
    services: [],
  },
};

const store = createMockStore(mockState);

export const ServicePopupAddWithExistingServiceError: React.FC = () => (
  <Provider store={store}>
    <ThemeWrapper theme={lightTheme}>
      <SnackbarWrapperConnect>
        <ServicePopupConnect />
      </SnackbarWrapperConnect>
    </ThemeWrapper>
  </Provider>
);
