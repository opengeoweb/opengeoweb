/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { produce } from 'immer';
import { TimeSeriesService } from '@opengeoweb/shared';
import { initialState, timeSeriesReducer } from './reducer';
import { Parameter, PlotPreset } from '../components/TimeSeries/types';
import { ServiceFilterChipsType, TimeSeriesStoreType } from './types';
import { mockTimeSeriesServices } from '../components/TimeSeries/mockTimeSeriesServices';
import { timeSeriesActions } from '.';

const {
  deletePlot,
  registerTimeSeriesPreset,
  togglePlot,
  deleteParameter,
  addParameter,
  toggleParameter,
  addPlot,
  patchParameter,
  setSearchFilter,
  setCurrentParameterInfoDisplayed,
  addServices,
  updateTitle,
  moveParameter,
  closeServicePopup,
  setServicePopup,
  upsertService,
  removeService,
  loadUserAddedTimeSeriesServices,
} = timeSeriesActions;

jest.mock('uuid', () => ({ v4: (): string => 'mockUUID' }));

describe('store/reducer', () => {
  it('should return initial state if no state and action passed in', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore can't trigger empty actions (yet)
    expect(timeSeriesReducer(undefined, {})).toEqual(initialState);
  });

  const plot1 = { plotId: 'plotId1', title: 'Plot 1' };
  const plot2 = { plotId: 'plotId2', title: 'Plot 2' };
  const parameter1: Parameter = {
    id: 'parameter1',
    plotId: plot1.plotId,
    unit: '°C',
    propertyName: 'Temperature',
    plotType: 'line',
    serviceId: mockTimeSeriesServices[0].id,
    collectionId: 'ecmwf',
    opacity: 70,
  };

  const plotPreset: PlotPreset = {
    mapId: 'TimeseriesMap',
    plots: [plot1, plot2],
    parameters: [
      parameter1,
      {
        id: 'parameter2',
        plotId: plot2.plotId,
        unit: 'mm',
        propertyName: 'Precipitation1h',
        plotType: 'bar',
        serviceId: mockTimeSeriesServices[0].id,
        collectionId: 'ecmwf',
        opacity: 70,
      },
    ],
  };

  const serviceFilterChipStore: ServiceFilterChipsType = {
    entities: {
      [mockTimeSeriesServices[0].id]: {
        serviceId: mockTimeSeriesServices[0].id,
        type: mockTimeSeriesServices[0].type,
        serviceUrl: mockTimeSeriesServices[0].url,
        serviceName: mockTimeSeriesServices[0].name,
        enabled: true,
        scope: 'system',
        isLoading: false,
      },
    },
    ids: [mockTimeSeriesServices[0].id],
  };

  const mockTimeSeriesState: TimeSeriesStoreType = {
    plotPreset,
    services: mockTimeSeriesServices,
    timeseriesSelect: {
      filters: {
        searchFilter: '',
        serviceFilterChips: serviceFilterChipStore,
        allServiceFilterChipsEnabled: true,
      },
      servicePopup: {
        isOpen: false,
        variant: 'add',
      },
    },
  };

  it('should load uder added timeseriesservices correctly', () => {
    const newServiceId = 'timeseriesserviceid_mockUUID';
    const newServices: TimeSeriesService[] = [
      {
        name: 'test',
        url: 'testUrl.fi',
        description: 'test desc',
        scope: 'system',
        id: 'id1',
        type: 'EDR',
      },
      {
        name: 'test2',
        url: mockTimeSeriesServices[0].url,
        description: 'test desc2',
        scope: 'system',
        id: 'id2',
        type: 'EDR',
      },
      {
        name: 'test3',
        url: 'userAddedService.fi',
        description: 'test desc3',
        scope: 'user',
        id: 'id3',
        type: 'EDR',
      },
    ];
    expect(
      timeSeriesReducer(
        mockTimeSeriesState,
        loadUserAddedTimeSeriesServices(newServices),
      ),
    ).toEqual({
      ...mockTimeSeriesState,
      services: [
        ...mockTimeSeriesServices,
        {
          ...newServices[0],
          scope: 'user',
        },
        {
          ...newServices[2],
          id: newServiceId,
        },
      ],
      timeseriesSelect: {
        ...mockTimeSeriesState.timeseriesSelect,
        filters: {
          ...mockTimeSeriesState.timeseriesSelect?.filters,
          serviceFilterChips: {
            entities: {
              ...serviceFilterChipStore.entities,
              [newServices[0].id]: {
                enabled: true,
                isLoading: false,
                scope: 'user',
                serviceId: newServices[0].id,
                serviceName: newServices[0].name,
                serviceUrl: newServices[0].url,
                type: 'EDR',
              },
              [newServiceId]: {
                enabled: true,
                isLoading: false,
                scope: 'user',
                serviceId: newServiceId,
                serviceName: newServices[2].name,
                serviceUrl: newServices[2].url,
                type: 'EDR',
              },
            },
            ids: [
              ...serviceFilterChipStore.ids,
              newServices[0].id,
              newServiceId,
            ],
          },
        },
      },
    });
  });

  it('should register plot', () => {
    const expectedState = produce(mockTimeSeriesState, (draft) => {
      draft.plotPreset!.parameters.forEach((draftParameter) => {
        draftParameter.id = expect.any(String);
      });
    });
    expect(
      timeSeriesReducer(
        undefined,
        registerTimeSeriesPreset({
          plotPreset,
          services: mockTimeSeriesServices,
          timeseriesSelect: {
            filters: {
              searchFilter: '',
              allServiceFilterChipsEnabled: true,
              serviceFilterChips: {
                entities: {},
                ids: [],
              },
            },
            servicePopup: {
              isOpen: false,
              variant: 'add',
            },
          },
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should register an extra service', () => {
    const testService = {
      id: 'test',
      url: 'test',
      description: 'test',
      type: 'OGC',
      scope: 'system',
      name: 'name',
    } as TimeSeriesService;

    const testService2 = {
      id: 'test1',
      url: 'testWithDifferentUrl',
      description: 'test',
      type: 'OGC',
      scope: 'system',
      name: 'name1',
    } as TimeSeriesService;

    const state = {
      plotPreset,
      services: [...mockTimeSeriesServices, testService, testService2],
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: {
            entities: {
              [mockTimeSeriesServices[0].id]: {
                serviceId: mockTimeSeriesServices[0].id,
                type: mockTimeSeriesServices[0].type,
                serviceUrl: mockTimeSeriesServices[0].url,
                serviceName: mockTimeSeriesServices[0].name,
                enabled: true,
                scope: 'system',
                isLoading: false,
              },
              [testService.id]: {
                serviceId: testService.id,
                type: testService.type,
                serviceUrl: testService.url,
                serviceName: testService.name,
                enabled: true,
                scope: 'system',
                isLoading: false,
              },
              [testService2.id]: {
                serviceId: testService2.id,
                type: testService2.type,
                serviceUrl: testService2.url,
                serviceName: testService2.name,
                enabled: true,
                scope: 'system',
                isLoading: false,
              },
            },
            ids: [
              mockTimeSeriesServices[0].id,
              testService.id,
              testService2.id,
            ],
          },
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };
    const expectedState = produce(state, (draft) => {
      draft.plotPreset!.parameters.forEach((draftParameter) => {
        draftParameter.id = expect.any(String);
      });
    });

    const store: TimeSeriesStoreType = {
      plotPreset: {
        mapId: 'TimeseriesMap',
        plots: [plot1],
        parameters: [parameter1],
      },
      services: [...mockTimeSeriesServices, testService],
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: {
            entities: {
              [mockTimeSeriesServices[0].id]: {
                serviceId: mockTimeSeriesServices[0].id,
                type: mockTimeSeriesServices[0].type,
                serviceUrl: mockTimeSeriesServices[0].url,
                serviceName: mockTimeSeriesServices[0].name,
                enabled: true,
                isLoading: false,
                scope: 'system',
              },
              [testService.id]: {
                serviceId: testService.id,
                type: testService.type,
                serviceUrl: testService.url,
                serviceName: testService.name,
                enabled: true,
                isLoading: false,
                scope: 'system',
              },
            },
            ids: [mockTimeSeriesServices[0].id, testService.id],
          },
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };
    const newState = timeSeriesReducer(
      store,
      registerTimeSeriesPreset({
        plotPreset,
        services: [...mockTimeSeriesServices, testService2],
      }),
    );
    expect(newState).toEqual(expectedState);
  });

  it('should adjust parameter service id if the service was already in the store', () => {
    const testServiceWhichShouldNotBeAdded = {
      id: 'test',
      url: mockTimeSeriesServices[0].url,
      description: 'test',
      type: 'OGC',
    } as TimeSeriesService;

    const store: TimeSeriesStoreType = {
      plotPreset: {
        mapId: 'TimeseriesMap',
        plots: [plot1],
        parameters: [
          {
            id: 'parameter1',
            plotId: plot1.plotId,
            unit: '°C',
            propertyName: 'Temperature',
            plotType: 'line',
            serviceId: mockTimeSeriesServices[0].id,
            collectionId: 'ecmwf',
          },
        ],
      },
      services: [...mockTimeSeriesServices],
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };
    const newState = timeSeriesReducer(
      store,
      registerTimeSeriesPreset({
        plotPreset: {
          mapId: 'TimeseriesMap',
          plots: [plot1],
          parameters: [
            {
              id: 'parameter1',
              plotId: plot1.plotId,
              unit: '°C',
              propertyName: 'Temperature',
              plotType: 'line',
              serviceId: testServiceWhichShouldNotBeAdded.id,
              collectionId: 'ecmwf',
            },
          ],
        },
        services: [testServiceWhichShouldNotBeAdded],
        timeseriesSelect: {
          filters: {
            searchFilter: '',
            serviceFilterChips: {
              entities: {
                [testServiceWhichShouldNotBeAdded.id]: {
                  serviceId: testServiceWhichShouldNotBeAdded.id,
                  type: testServiceWhichShouldNotBeAdded.type,
                  serviceUrl: testServiceWhichShouldNotBeAdded.url,
                  serviceName: testServiceWhichShouldNotBeAdded.name,
                  enabled: true,
                  scope: 'system',
                },
              },
              ids: [testServiceWhichShouldNotBeAdded.id],
            },
            allServiceFilterChipsEnabled: true,
          },
          servicePopup: {
            isOpen: false,
            variant: 'add',
          },
        },
      }),
    );
    // The existing mockTimeSeriesServices should have been re-used in the parameter id
    expect(newState.plotPreset?.parameters[0].serviceId).toEqual(
      mockTimeSeriesServices[0].id,
    );
  });

  it('should register an extra service with a new id if the service id was existing and the url is different', () => {
    const testService = {
      id: 'test',
      url: 'test',
      description: 'test',
      type: 'OGC',
      scope: 'system',
      name: 'name',
    } as TimeSeriesService;

    const testService2 = {
      id: 'test',
      url: 'testWithDifferentUrl',
      description: 'test',
      type: 'OGC',
      scope: 'system',
      name: 'name2',
    } as TimeSeriesService;

    const newServiceId = 'timeseriesserviceid_mockUUID'; // This is the id for testService2

    const state = {
      plotPreset,
      services: [...mockTimeSeriesServices, testService, testService2],
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: {
            entities: {
              [mockTimeSeriesServices[0].id]: {
                serviceId: mockTimeSeriesServices[0].id,
                type: mockTimeSeriesServices[0].type,
                serviceUrl: mockTimeSeriesServices[0].url,
                serviceName: mockTimeSeriesServices[0].name,
                enabled: true,
                isLoading: false,
                scope: 'system',
              },
              [testService.id]: {
                serviceId: testService.id,
                type: testService.type,
                serviceUrl: testService.url,
                serviceName: testService.name,
                enabled: true,
                isLoading: false,
                scope: 'system',
              },
              [newServiceId]: {
                serviceId: newServiceId,
                type: testService2.type,
                serviceUrl: testService2.url,
                serviceName: testService2.name,
                enabled: true,
                isLoading: false,
                scope: 'system',
              },
            },
            ids: [mockTimeSeriesServices[0].id, testService.id, newServiceId],
          },
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };
    const expectedState = produce(state, (draft) => {
      draft.plotPreset!.parameters.forEach((draftParameter) => {
        draftParameter.id = expect.any(String);
      });
      draft.services[2].id = newServiceId;
    });

    const store: TimeSeriesStoreType = {
      plotPreset: {
        mapId: 'TimeseriesMap',
        plots: [plot1],
        parameters: [parameter1],
      },
      services: [...mockTimeSeriesServices, testService],
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: {
            entities: {
              [mockTimeSeriesServices[0].id]: {
                serviceId: mockTimeSeriesServices[0].id,
                type: mockTimeSeriesServices[0].type,
                serviceUrl: mockTimeSeriesServices[0].url,
                serviceName: mockTimeSeriesServices[0].name,
                enabled: true,
                isLoading: false,
                scope: 'system',
              },
              [testService.id]: {
                serviceId: testService.id,
                type: testService.type,
                serviceUrl: testService.url,
                serviceName: testService.name,
                enabled: true,
                isLoading: false,
                scope: 'system',
              },
            },
            ids: [mockTimeSeriesServices[0].id, testService.id],
          },
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };
    const newState = timeSeriesReducer(
      store,
      registerTimeSeriesPreset({
        plotPreset,
        services: [...mockTimeSeriesServices, testService2],
      }),
    );
    expect(newState).toEqual(expectedState);
  });

  it('should delete plot', () => {
    const expectedState: TimeSeriesStoreType = {
      plotPreset: {
        mapId: 'TimeseriesMap',
        plots: [plot1],
        parameters: [parameter1],
      },
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };
    expect(
      timeSeriesReducer(
        {
          plotPreset,
          services: mockTimeSeriesServices,
          timeseriesSelect: {
            filters: {
              searchFilter: '',
              serviceFilterChips: serviceFilterChipStore,
              allServiceFilterChipsEnabled: true,
            },
            servicePopup: {
              isOpen: false,
              variant: 'add',
            },
          },
        },
        deletePlot({ plotId: plot2.plotId }),
      ),
    ).toEqual(expectedState);
  });
  it('should toggle plot', () => {
    const initialState: TimeSeriesStoreType = {
      plotPreset,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };

    const state = timeSeriesReducer(
      initialState,
      togglePlot({ plotId: 'plotId2' }),
    );
    const expectedState1 = produce(initialState, (draft) => {
      draft.plotPreset!.plots[1].enabled = false;
    });
    expect(state).toEqual(expectedState1);

    const expectedState2 = produce(expectedState1, (draft) => {
      draft.plotPreset!.plots[1].enabled = true;
    });
    expect(timeSeriesReducer(state, togglePlot({ plotId: 'plotId2' }))).toEqual(
      expectedState2,
    );
  });

  it('should delete plot parameter', () => {
    const idToDelete = 'id1';
    const initialPlotPreset = produce(plotPreset, (draft) => {
      draft.parameters.push({
        id: idToDelete,
        plotId: plot1.plotId,
        unit: 'unit1',
        propertyName: 'prop1',
        plotType: 'line',
        serviceId: mockTimeSeriesServices[0].id,
        collectionId: 'ecmwf',
      });
    });
    expect(
      timeSeriesReducer(
        {
          plotPreset: initialPlotPreset,
          services: mockTimeSeriesServices,
          timeseriesSelect: {
            filters: {
              searchFilter: '',
              serviceFilterChips: serviceFilterChipStore,
              allServiceFilterChipsEnabled: true,
            },
            servicePopup: {
              isOpen: false,
              variant: 'add',
            },
          },
        },
        deleteParameter({ parameterId: idToDelete }),
      ),
    ).toEqual({
      plotPreset,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    });
  });

  it('should add parameter', () => {
    const newParameter: Parameter = {
      id: expect.any(String),
      plotId: 'plotId1',
      unit: '°C',
      propertyName: 'DewPoint',
      plotType: 'line',
      serviceId: mockTimeSeriesServices[0].id,
      collectionId: 'ecmwf',
    };
    const expectedState = produce(mockTimeSeriesState, (draft) => {
      draft.plotPreset!.parameters.push(newParameter);
    });

    expect(
      timeSeriesReducer(
        mockTimeSeriesState,
        addParameter({ parameter: newParameter }),
      ),
    ).toEqual(expectedState);
  });
  it('should toggle parameter', () => {
    const initialState: TimeSeriesStoreType = {
      plotPreset,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };

    const idToToggle = plotPreset.parameters[1].id;

    const state1 = timeSeriesReducer(
      initialState,
      toggleParameter({ parameterId: idToToggle! }),
    );
    const expectedState1 = produce(initialState, (draft) => {
      draft.plotPreset!.parameters[1].enabled = false;
    });
    expect(state1).toEqual(expectedState1);

    const state2 = timeSeriesReducer(
      state1,
      toggleParameter({
        parameterId: idToToggle!,
      }),
    );
    const expectedState2 = produce(expectedState1, (draft) => {
      draft.plotPreset!.parameters[1].enabled = true;
    });
    expect(state2).toEqual(expectedState2);
  });

  it('should add a plot', () => {
    const titleToAdd = 'Plot_2';
    const expectedState = produce(mockTimeSeriesState, (draft) => {
      draft.plotPreset!.plots.push({
        title: titleToAdd,
        plotId: expect.any(String),
      });
    });
    expect(
      timeSeriesReducer(mockTimeSeriesState, addPlot({ title: titleToAdd })),
    ).toEqual(expectedState);
  });

  it('should patch color, plot type and opacity of a parameter', () => {
    const expectedState = produce(mockTimeSeriesState, (draft) => {
      draft.plotPreset!.parameters[0].color = 'blue';
      draft.plotPreset!.parameters[0].plotType = 'bar';
      draft.plotPreset!.parameters[0].opacity = 70;
    });
    const parameterToPatch = plotPreset.parameters[0];
    expect(
      timeSeriesReducer(
        mockTimeSeriesState,
        patchParameter({
          parameter: {
            id: parameterToPatch.id,
            color: 'blue',
            plotType: 'bar',
            opacity: 70,
          },
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should patch instanceId (reference time) of a parameter', () => {
    const expectedState = produce(mockTimeSeriesState, (draft) => {
      draft.plotPreset!.parameters[0].instanceId = '2000-01-01T00:00:00Z';
    });
    const parameterToPatch = plotPreset.parameters[0];
    expect(
      timeSeriesReducer(
        mockTimeSeriesState,
        patchParameter({
          parameter: {
            id: parameterToPatch.id,
            instanceId: '2000-01-01T00:00:00Z',
          },
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should patch property name of a parameter', () => {
    const expectedState = produce(mockTimeSeriesState, (draft) => {
      draft.plotPreset!.parameters[0].propertyName = 'testPropertyName';
    });
    const parameterToPatch = plotPreset.parameters[0];
    expect(
      timeSeriesReducer(
        mockTimeSeriesState,
        patchParameter({
          parameter: {
            id: parameterToPatch.id,
            propertyName: 'testPropertyName',
          },
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should not patch plotId, unit, enabled, serviceId and instances of a parameter', () => {
    const parameterToPatch = plotPreset.parameters[0];
    expect(
      timeSeriesReducer(
        mockTimeSeriesState,
        patchParameter({
          parameter: {
            id: parameterToPatch.id,
            plotId: 'changedPlotId',
            unit: 'changedUnit',
            enabled: !parameterToPatch.enabled,
            serviceId: 'changedServiceId',
            instances: [{ id: 'newServiceId' }],
          },
        }),
      ),
    ).toEqual(mockTimeSeriesState);
  });

  it('should delete supported parameter properties on patching', () => {
    const expectedState = produce(mockTimeSeriesState, (draft) => {
      delete draft.plotPreset!.parameters[0].color;
      delete draft.plotPreset!.parameters[0].opacity;
      delete draft.plotPreset!.parameters[0].instanceId;
    });
    const parameterToPatch = plotPreset.parameters[0];
    expect(
      timeSeriesReducer(
        mockTimeSeriesState,
        patchParameter({
          parameter: {
            id: parameterToPatch.id,
            color: undefined,
            opacity: undefined,
            instanceId: undefined,
          },
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should not delete mandatory or unsupported parameter properties on patching', () => {
    const parameterToPatch = plotPreset.parameters[0];
    expect(
      timeSeriesReducer(
        mockTimeSeriesState,
        patchParameter({
          parameter: {
            id: parameterToPatch.id,
            plotType: undefined,
            propertyName: undefined,
            plotId: undefined,
            unit: undefined,
            enabled: undefined,
            serviceId: undefined,
            instances: undefined,
          },
        }),
      ),
    ).toEqual(mockTimeSeriesState);
  });

  it('should set a search string', () => {
    const expectedState: TimeSeriesStoreType = {
      plotPreset,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: 'some search filter',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };
    expect(
      timeSeriesReducer(
        {
          plotPreset,
          services: mockTimeSeriesServices,
          timeseriesSelect: {
            filters: {
              searchFilter: '',
              serviceFilterChips: serviceFilterChipStore,
              allServiceFilterChipsEnabled: true,
            },
            servicePopup: {
              isOpen: false,
              variant: 'add',
            },
          },
        },
        setSearchFilter({ filterText: 'some search filter' }),
      ),
    ).toEqual(expectedState);
  });

  it('should set parameterInfo as id', () => {
    const expectedState: TimeSeriesStoreType = {
      plotPreset,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        currentParameterInfo: parameter1.id,
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };
    expect(
      timeSeriesReducer(
        {
          plotPreset,
          services: mockTimeSeriesServices,
          timeseriesSelect: {
            filters: {
              searchFilter: '',
              serviceFilterChips: serviceFilterChipStore,
              allServiceFilterChipsEnabled: true,
            },
            servicePopup: {
              isOpen: false,
              variant: 'add',
            },
          },
        },
        setCurrentParameterInfoDisplayed({
          parameter: parameter1.id as string,
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should set parameterInfo as Parameter-type', () => {
    const expectedState: TimeSeriesStoreType = {
      plotPreset,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        currentParameterInfo: parameter1,
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };
    expect(
      timeSeriesReducer(
        {
          plotPreset,
          services: mockTimeSeriesServices,
          timeseriesSelect: {
            filters: {
              searchFilter: '',
              serviceFilterChips: serviceFilterChipStore,
              allServiceFilterChipsEnabled: true,
            },
            servicePopup: {
              isOpen: false,
              variant: 'add',
            },
          },
        },
        setCurrentParameterInfoDisplayed({
          parameter: parameter1,
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should not duplicate services', () => {
    const expectedState: TimeSeriesStoreType = {
      plotPreset,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };
    expect(
      timeSeriesReducer(
        {
          plotPreset,
          services: mockTimeSeriesServices,
          timeseriesSelect: {
            filters: {
              searchFilter: '',
              serviceFilterChips: serviceFilterChipStore,
              allServiceFilterChipsEnabled: true,
            },
            servicePopup: {
              isOpen: false,
              variant: 'add',
            },
          },
        },
        addServices({
          timeSeriesServices: mockTimeSeriesServices,
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should add a generated id for different services with matching ids', () => {
    const expectedState: TimeSeriesStoreType = {
      plotPreset,
      services: [
        ...mockTimeSeriesServices,
        {
          ...mockTimeSeriesServices[0],
          url: 'someurl',
          id: 'timeseriesserviceid_mockUUID',
        },
      ],
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: {
            entities: {
              [mockTimeSeriesServices[0].id]: {
                serviceId: mockTimeSeriesServices[0].id,
                type: mockTimeSeriesServices[0].type,
                serviceUrl: mockTimeSeriesServices[0].url,
                serviceName: mockTimeSeriesServices[0].name,
                enabled: true,
                scope: 'system',
                isLoading: false,
              },
              timeseriesserviceid_mockUUID: {
                serviceId: 'timeseriesserviceid_mockUUID',
                type: mockTimeSeriesServices[0].type,
                serviceUrl: 'someurl',
                serviceName: mockTimeSeriesServices[0].name,
                enabled: true,
                scope: 'system',
                isLoading: false,
              },
            },
            ids: [mockTimeSeriesServices[0].id, 'timeseriesserviceid_mockUUID'],
          },
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };
    expect(
      timeSeriesReducer(
        {
          plotPreset,
          services: mockTimeSeriesServices,
          timeseriesSelect: {
            filters: {
              searchFilter: '',
              serviceFilterChips: {
                entities: {
                  [mockTimeSeriesServices[0].id]: {
                    serviceId: mockTimeSeriesServices[0].id,
                    type: mockTimeSeriesServices[0].type,
                    serviceUrl: mockTimeSeriesServices[0].url,
                    serviceName: mockTimeSeriesServices[0].name,
                    enabled: true,
                    scope: 'system',
                    isLoading: false,
                  },
                },
                ids: [mockTimeSeriesServices[0].id],
              },
              allServiceFilterChipsEnabled: true,
            },
            servicePopup: {
              isOpen: false,
              variant: 'add',
            },
          },
        },
        addServices({
          timeSeriesServices: [
            { ...mockTimeSeriesServices[0], url: 'someurl' },
          ],
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should update plot title', () => {
    const plotPresetNewTitle = produce(plotPreset, (draft) => {
      draft.plots[0].title = 'newTitle';
    });
    const expectedState: TimeSeriesStoreType = {
      plotPreset: plotPresetNewTitle,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };
    expect(
      timeSeriesReducer(
        mockTimeSeriesState,
        updateTitle({
          plotId: plot1.plotId,
          title: 'newTitle',
        }),
      ),
    ).toEqual(expectedState);
  });
  it('should change parameter plot id', () => {
    const paramNewPlotId = produce(plotPreset, (draft) => {
      draft.parameters[1].plotId = plot1.plotId;
    });
    const expectedState: TimeSeriesStoreType = {
      plotPreset: paramNewPlotId,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };
    expect(
      timeSeriesReducer(
        mockTimeSeriesState,
        moveParameter({
          oldIndex: 0,
          newIndex: 1,
          toPlotId: plot1.plotId,
          plotId: plot2.plotId,
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should set servicepopup correctly', () => {
    const expectedState: TimeSeriesStoreType = {
      plotPreset,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: true,
          variant: 'edit',
          url: 'https://testurl.fi',
          serviceId: 'test_id',
        },
      },
    };

    expect(
      timeSeriesReducer(
        mockTimeSeriesState,
        setServicePopup({
          isOpen: true,
          variant: 'edit',
          url: 'https://testurl.fi',
          serviceId: 'test_id',
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should close servicepopup', () => {
    const localMockState: TimeSeriesStoreType = {
      ...mockTimeSeriesState,
      timeseriesSelect: {
        ...mockTimeSeriesState.timeseriesSelect!,
        servicePopup: {
          isOpen: true,
          variant: 'add',
        },
      },
    };
    const expectedState: TimeSeriesStoreType = {
      plotPreset,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };

    expect(timeSeriesReducer(localMockState, closeServicePopup())).toEqual(
      expectedState,
    );
  });

  it('should add timeseries services correctly', () => {
    const expectedState: TimeSeriesStoreType = {
      plotPreset,
      services: [
        ...mockTimeSeriesServices,
        {
          id: 'testid',
          description: 'testdesc',
          url: 'testurl',
          name: 'test',
          type: 'EDR',
          scope: 'user',
        },
      ],
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: {
            ids: [...serviceFilterChipStore.ids, 'testid'],
            entities: {
              ...serviceFilterChipStore.entities,
              testid: {
                serviceId: 'testid',
                serviceName: 'test',
                serviceUrl: 'testurl',
                type: 'EDR',
                scope: 'user',
                isLoading: false,
                enabled: true,
              },
            },
          },
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };

    expect(
      timeSeriesReducer(
        mockTimeSeriesState,
        upsertService({
          id: 'testid',
          description: 'testdesc',
          url: 'testurl',
          name: 'test',
          type: 'EDR',
          isUpdate: false,
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should update timeseries service correctly', () => {
    const localMockState: TimeSeriesStoreType = {
      plotPreset,
      services: [
        ...mockTimeSeriesServices,
        {
          id: 'testid',
          description: 'testdesc',
          url: 'testurl',
          name: 'test',
          type: 'EDR',
          scope: 'user',
        },
      ],
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: {
            ids: [...serviceFilterChipStore.ids, 'testid'],
            entities: {
              ...serviceFilterChipStore.entities,
              testid: {
                serviceId: 'testid',
                serviceName: 'test',
                serviceUrl: 'testurl',
                type: 'EDR',
                scope: 'user',
                isLoading: false,
                enabled: true,
              },
            },
          },
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };

    const expectedState: TimeSeriesStoreType = {
      plotPreset,
      services: [
        ...mockTimeSeriesServices,
        {
          id: 'testid',
          description: 'testdesc2',
          url: 'testurl2',
          name: 'test2',
          type: 'EDR',
          scope: 'user',
        },
      ],
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: {
            ids: [...serviceFilterChipStore.ids, 'testid'],
            entities: {
              ...serviceFilterChipStore.entities,
              testid: {
                serviceId: 'testid',
                serviceName: 'test2',
                serviceUrl: 'testurl2',
                type: 'EDR',
                scope: 'user',
                isLoading: false,
                enabled: true,
              },
            },
          },
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };

    expect(
      timeSeriesReducer(
        localMockState,
        upsertService({
          id: 'testid',
          description: 'testdesc2',
          url: 'testurl2',
          name: 'test2',
          type: 'EDR',
          isUpdate: true,
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should remove timeseries services correctly', () => {
    const localMockState: TimeSeriesStoreType = {
      plotPreset,
      services: [
        ...mockTimeSeriesServices,
        {
          id: 'testid',
          description: 'testdesc',
          url: 'testurl',
          name: 'test',
          type: 'EDR',
          scope: 'user',
        },
      ],
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: {
            ids: [...serviceFilterChipStore.ids, 'testid'],
            entities: {
              ...serviceFilterChipStore.entities,
              testid: {
                serviceId: 'testid',
                serviceName: 'test',
                serviceUrl: 'testurl',
                type: 'EDR',
                scope: 'user',
                isLoading: false,
                enabled: true,
              },
            },
          },
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'add',
        },
      },
    };

    expect(
      timeSeriesReducer(localMockState, removeService({ id: 'testid' })),
    ).toEqual(mockTimeSeriesState);
  });
});
