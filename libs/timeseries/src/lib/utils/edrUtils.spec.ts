/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { TimeSeriesService, dateUtils } from '@opengeoweb/shared';
import axios from 'axios';
import { Feature, Point, Polygon } from 'geojson';
import * as turf from '@turf/turf';
import {
  Parameter,
  EDRParameter,
  EDRParameters,
  EDRInstance,
  TimeSeriesLocation,
} from '../components/TimeSeries/types';
import {
  getBaseQueryParamArray,
  fetchEdrLatestInstance,
  fetchEdrParameterApiData,
  getEdrParameter,
  isSupportedInstance,
  latestInstance,
  latestSupportedInstance,
  supportedOutputFormat,
  listSupportedInstance,
  getCollectionListForTimeSeriesService,
  getParameterListForCollectionId,
  clipEdrInterval,
  edrListener,
  getUnit,
  getLocations,
  fetchEdrCollections,
  validateServiceUrl,
  createUrl,
  findNearestPoint,
  getFeatureCenterPoint,
} from './edrUtils';
import { serverMock } from '../../mocks/server';
import {
  edrPositionResponseMock,
  edrLocationResponseMock,
  latestInstanceMock,
  finlandEcmwfInstances,
  parameterNameMock,
  parameterUnitMock,
  parameterMock,
  timestepMock,
  valueMock,
  EDR_FINLAND_URL,
  EDR_FINLAND_ECMWF_COLLECTION_ID,
  EDR_COLLECTIONS_TEST_URL,
  EDR_COLLECTION_NO_INSTANCES_ID,
  EDR_FINLAND_NAME,
  EDR_FINLAND_DESCRIPTION,
} from '../../mocks';

export const edrParameterUnitTestValues: EDRParameters = {
  param_no_unit: {
    id: 'param_no_unit',
    observedProperty: {},
  } as EDRParameter,
  param_str_label: {
    id: 'param_str_label',
    observedProperty: {},
    unit: { label: 'str_label' },
  } as EDRParameter,
  param_en_label: {
    id: 'param_en_label',
    unit: { label: { en: 'label_en' } },
  } as EDRParameter,
  param_fi_label: {
    id: 'param_fi_label',
    unit: { label: { fi: 'label_fi' } },
  } as EDRParameter,
  param_fi_en_label: {
    id: 'param_fi_en_label',
    unit: { label: { fi: 'label_fi', en: 'label_en' } },
  } as EDRParameter,
  param_symbol_str: {
    id: 'param_symbol_str',
    unit: { symbol: 'symbol_str' },
  } as EDRParameter,
  param_symbol_value: {
    id: 'param_symbol_str',
    unit: { symbol: { value: 'symbol_value' } },
  } as EDRParameter,
  param_symbol_and_label: {
    id: 'param_symbol_and_label',
    unit: {
      symbol: { value: 'symbol_value', type: 'UCUM' },
      label: { en: 'symbol_value' },
    },
  },
} as EDRParameters;

beforeAll(() => serverMock.listen());
afterAll(() => serverMock.close());

describe('utils/edrUtils', () => {
  const supportedInstance: EDRInstance = {
    id: '20230101',
    extent: {
      temporal: {
        interval: [['2023-01-01T00:00:00Z', '2023-01-11T00:00:00Z']],
        crs: 'TIMECRS["DateTime",TDATUM["Gregorian Calendar"],CS[TemporalDateTime,1],AXIS["Time (T)",future]',
        values: ['R60/2023-01-01T00:00:00Z/PT240M'],
      },
      spatial: {
        crs: 'EPSG:4326',
        bbox: [-180, -90, 180, 90],
      },
    },
    data_queries: {
      position: {
        link: {
          variables: {
            output_formats: ['unknown', supportedOutputFormat],
          },
        },
      },
    },
  };

  describe('createUrl', () => {
    it('should return the same url when no parameters applied', () => {
      const testUrl = 'https://fmi.fi/';
      expect(createUrl(testUrl)).toBe(testUrl);
      const testUrl2 = 'https://opendata.fmi.fi/edr';
      expect(createUrl(testUrl2)).toBe(testUrl2);
      const testUrl3 = 'https://opendata.fmi.fi/edr?query=a&query2=aefaf+fawef';
      expect(createUrl(testUrl3)).toBe(testUrl3);
    });

    it('should concatenate paths correctly', () => {
      const testPath1 = '/collections/abc';
      const testPath2 = 'collections/abc/';
      const testPath3 = 'collections';
      const testUrl1 = 'https://fmi.fi';
      expect(createUrl(testUrl1, testPath1)).toBe(
        'https://fmi.fi/collections/abc',
      );
      expect(createUrl(testUrl1, testPath2)).toBe(
        'https://fmi.fi/collections/abc/',
      );
      expect(createUrl(testUrl1, testPath3)).toBe('https://fmi.fi/collections');
      const testUrl2 = 'https://opendata.fmi.fi/edr';
      expect(createUrl(testUrl2, testPath1)).toBe(
        'https://opendata.fmi.fi/edr/collections/abc',
      );
      expect(createUrl(testUrl2, testPath2)).toBe(
        'https://opendata.fmi.fi/edr/collections/abc/',
      );

      // Just to test different slash configurations
      const testUrl2b = `${testUrl2}/`;
      expect(createUrl(testUrl2b, testPath3)).toBe(
        'https://opendata.fmi.fi/edr/collections',
      );
      const testUrl3 = 'https://opendata.fmi.fi/edr?query=a&query2=b';
      expect(createUrl(testUrl3, testPath1)).toBe(
        'https://opendata.fmi.fi/edr/collections/abc?query=a&query2=b',
      );
      expect(createUrl(testUrl3, testPath2)).toBe(
        'https://opendata.fmi.fi/edr/collections/abc/?query=a&query2=b',
      );
      expect(createUrl(testUrl3, testPath3)).toBe(
        'https://opendata.fmi.fi/edr/collections?query=a&query2=b',
      );
    });

    it('should handle additional query parameters', () => {
      const testUrl1 = 'https://fmi.fi';
      expect(createUrl(testUrl1, undefined, [['testParam', '3']])).toBe(
        'https://fmi.fi/?testParam=3',
      );
      expect(
        createUrl(testUrl1, undefined, [
          ['testParam', '3'],
          ['t2', 'abc'],
        ]),
      ).toBe('https://fmi.fi/?testParam=3&t2=abc');
      const testUrl2 = 'https://opendata.fmi.fi/edr';
      expect(createUrl(testUrl2, undefined, [['testParam', '3']])).toBe(
        'https://opendata.fmi.fi/edr?testParam=3',
      );
      const testUrl2b = `${testUrl2}/`;
      expect(
        createUrl(testUrl2b, undefined, [
          ['testParam', '3'],
          ['t2', 'abc'],
        ]),
      ).toBe('https://opendata.fmi.fi/edr/?testParam=3&t2=abc');
      const testUrl3 = 'https://opendata.fmi.fi/edr?query=a&query2=b';
      expect(createUrl(testUrl3, undefined, [['testParam', '3']])).toBe(
        'https://opendata.fmi.fi/edr?query=a&query2=b&testParam=3',
      );
      expect(
        createUrl(testUrl3, undefined, [
          ['testParam', '3'],
          ['t2', 'abc'],
        ]),
      ).toBe('https://opendata.fmi.fi/edr?query=a&query2=b&testParam=3&t2=abc');
    });

    it('should url-encode contents of queryParameters-parameter', () => {
      const testUrl1 = 'https://fmi.fi';
      expect(
        createUrl(testUrl1, undefined, [['testPa&4++//+ram', '#@ "?']]),
      ).toBe('https://fmi.fi/?testPa&4++//+ram=%23@%20%22?');
    });

    it('should work together', () => {
      const testUrl = 'https://opendata.fmi.fi/edr?query=a&query2=b';
      const testPath = '/collections/a/';
      expect(
        createUrl(testUrl, testPath, [
          ['testParam', '123'],
          ['testParam#2', 'abc'],
        ]),
      ).toBe(
        'https://opendata.fmi.fi/edr/collections/a/?query=a&query2=b&testParam=123&testParam%232=abc',
      );
    });
  });

  describe('latestInstance', () => {
    it('should return default instance if array is empty', () => {
      const instance = latestInstance([]);
      expect(instance).toEqual({ id: '' });
    });

    it('should return latest instance by id', () => {
      const instance = latestInstance([
        { id: '20230101' },
        { id: '20230109' },
        { id: '20230108' },
      ]);
      expect(instance.id).toEqual('20230109');
    });
  });

  describe('isSupportedInstance', () => {
    it('should require position data query', () => {
      expect(isSupportedInstance({ id: '20230101' })).toBeFalsy();
      expect(
        isSupportedInstance({ id: '20230101', data_queries: {} }),
      ).toBeFalsy();
    });

    it('should require supported format', () => {
      expect(
        isSupportedInstance({
          id: '20230101',
          data_queries: {
            position: { link: { variables: { output_formats: ['unknown'] } } },
          },
        }),
      ).toBeFalsy();

      expect(isSupportedInstance(supportedInstance)).toBeTruthy();
    });
  });

  describe('latestSupportedInstance', () => {
    it('should return latest supported', () => {
      const instances = [
        supportedInstance,
        {
          id: '20230102',
          data_queries: {
            position: {
              link: {
                variables: {
                  output_formats: ['unknown'],
                },
              },
            },
          },
        },
      ];
      expect(latestSupportedInstance(instances)).toBe(instances[0]);
    });
  });

  describe('listOfSupportedInstance', () => {
    it('should return list of supported instance', () => {
      const instances = [
        supportedInstance,
        {
          id: '20230102',
          data_queries: {
            position: {
              link: {
                variables: {
                  output_formats: ['unknown'],
                },
              },
            },
          },
        },
      ];
      expect(listSupportedInstance(instances)).toStrictEqual([
        supportedInstance,
      ]);
    });
  });

  describe('edrPositionRequestUrl', () => {
    beforeAll(() => {
      jest.useFakeTimers();
      jest.setSystemTime(new Date('2023-01-05T00:00:00Z'));
    });

    afterAll(() => {
      jest.useRealTimers();
    });

    it('should return url with correct request parameters', () => {
      const urlParams = getBaseQueryParamArray(
        parameterMock,
        supportedInstance,
        [1000, 1000],
      );
      expect(urlParams.find((e) => e[0] === 'parameter-name')).toEqual([
        'parameter-name',
        parameterMock.propertyName,
      ]);
      expect(urlParams.find((e) => e[0] === 'crs')).toEqual([
        'crs',
        'EPSG:4326',
      ]);
      expect(urlParams.find((e) => e[0] === 'datetime')).toEqual([
        'datetime',
        '2023-01-01T00:00:00Z/2023-01-11T00:00:00Z',
      ]);
    });

    it('should handle missing extend by ignoring time and crs', () => {
      const urlParams = getBaseQueryParamArray(parameterMock, {
        ...supportedInstance,
        extent: undefined,
      });

      expect(urlParams.find((e) => e[0] === 'parameter-name')).toEqual([
        'parameter-name',
        parameterMock.propertyName,
      ]);
      expect(urlParams.find((e) => e[0] === 'crs')).toBeFalsy();
      expect(urlParams.find((e) => e[0] === 'datetime')).toBeFalsy();
    });

    it('should include supported output format', () => {
      const urlParams = getBaseQueryParamArray(
        parameterMock,
        supportedInstance,
      );
      expect(urlParams.find((e) => e[0] === 'f')).toEqual([
        'f',
        supportedOutputFormat,
      ]);
    });

    it('should include dimensions', () => {
      const urlParams = getBaseQueryParamArray(
        {
          ...parameterMock,
          dimension: {
            id: 'dim1',
            value: 'A',
          },
        },
        supportedInstance,
      );
      expect(urlParams.find((e) => e[0] === 'dim1')).toEqual(['dim1', 'A']);
    });
  });

  describe('fetchEdrParameterApiData', () => {
    it('should fetch position data', async () => {
      const spy = jest.fn();
      edrListener.addEventListener('edr', spy);
      const res = await fetchEdrParameterApiData(
        EDR_FINLAND_URL,
        parameterMock,
        {
          lat: 1,
          lon: 2,
        },
        latestInstanceMock,
      );
      expect(res).toEqual(edrPositionResponseMock);
      expect(spy).toHaveBeenCalledTimes(2);
      expect(spy).toHaveBeenNthCalledWith(1, true);
      expect(spy).toHaveBeenNthCalledWith(2, false);
      edrListener.removeEventListener('edr', spy);
    });

    it('should fetch location data', async () => {
      const spy = jest.fn();
      edrListener.addEventListener('edr', spy);
      const location: TimeSeriesLocation = {
        lat: 1,
        lon: 2,
        id: 'helsinki',
        serviceId: 'fmi',
        collectionId: EDR_FINLAND_ECMWF_COLLECTION_ID,
      };
      const res = await fetchEdrParameterApiData(
        EDR_FINLAND_URL,
        parameterMock,
        location,
        latestInstanceMock,
      );
      expect(res).toEqual(edrLocationResponseMock);
      expect(spy).toHaveBeenCalledTimes(2);
      expect(spy).toHaveBeenNthCalledWith(1, true);
      expect(spy).toHaveBeenNthCalledWith(2, false);
      edrListener.removeEventListener('edr', spy);
    });

    it('should return null on error', async () => {
      const spy = jest.fn();
      edrListener.addEventListener('edr', spy);

      const nonSupportedPropertyParam = {
        ...parameterMock,
        propertyName: 'nonSupportedProperty',
      };

      const res = await fetchEdrParameterApiData(
        EDR_FINLAND_URL,
        nonSupportedPropertyParam,
        {
          lat: 1,
          lon: 2,
        },
        supportedInstance,
      );
      expect(res).toEqual(null);
      expect(spy).toHaveBeenCalledTimes(2);
      expect(spy).toHaveBeenNthCalledWith(1, true);
      expect(spy).toHaveBeenNthCalledWith(2, false);
      edrListener.removeEventListener('edr', spy);
    });
  });

  describe('fetchEdrLatestInstances', () => {
    it('should fetch data2', async () => {
      const instance = await fetchEdrLatestInstance(
        EDR_FINLAND_URL,
        EDR_FINLAND_ECMWF_COLLECTION_ID,
      );
      expect(instance).toEqual(finlandEcmwfInstances.instances[1]);
    });

    it('should return object with empty id on error', async () => {
      const res = await fetchEdrLatestInstance(
        'http://url-that-doesnt-have-instances-api.com',
        '',
      );
      expect(res.id).toEqual('');
    });

    it('should handle the URLs correctly', async () => {
      const spy = jest.spyOn(axios, 'get');
      await fetchEdrLatestInstance(
        `${EDR_FINLAND_URL}?apikey=3`,
        EDR_FINLAND_ECMWF_COLLECTION_ID,
      );
      expect(spy).toHaveBeenCalledWith(
        `${EDR_FINLAND_URL}/collections/${EDR_FINLAND_ECMWF_COLLECTION_ID}/instances?apikey=3`,
      );
    });
  });

  describe('getEdrParameter', () => {
    const parameter: Parameter = {
      plotId: 'plotId1',
      plotType: 'line',
      propertyName: parameterNameMock,
      serviceId: 'fmi',
      collectionId: EDR_FINLAND_ECMWF_COLLECTION_ID,
      instanceId: '20230927T120000',
    };
    it('should fetch data', async () => {
      const paramWithData = await getEdrParameter(parameter, EDR_FINLAND_URL, {
        lat: 1.2,
        lon: 3.4,
      });
      expect(paramWithData).toEqual({
        ...parameter,
        unit: parameterUnitMock,
        timestep: timestepMock.map((time) => new Date(time)),
        value: valueMock,
      });
    });

    it('should return null on error', async () => {
      const paramWithData = await getEdrParameter(
        {
          ...parameter,
          propertyName: 'NotSupportedProperty',
        },
        EDR_FINLAND_URL,
        { lat: 1.2, lon: 3.4 },
      );
      expect(paramWithData).toEqual(null);
    });

    it('should handle URLs correctly', async () => {
      const spy = jest.spyOn(axios, 'get');
      await getEdrParameter(parameter, `${EDR_FINLAND_URL}?a=3`, {
        lat: 1.2,
        lon: 3.4,
      });
      expect(spy).toHaveBeenCalledTimes(3);
      expect(spy).toHaveBeenNthCalledWith(
        1,
        `${EDR_FINLAND_URL}/collections/${EDR_FINLAND_ECMWF_COLLECTION_ID}?a=3`,
      );
      expect(spy).toHaveBeenNthCalledWith(
        2,
        `${EDR_FINLAND_URL}/collections/${EDR_FINLAND_ECMWF_COLLECTION_ID}/instances?a=3`,
      );
      expect(spy).toHaveBeenNthCalledWith(
        3,
        `${EDR_FINLAND_URL}/collections/${EDR_FINLAND_ECMWF_COLLECTION_ID}/instances/20230927T120000/position?a=3&parameter-name=Temperature&f=CoverageJSON&coords=POINT(3.4%201.2)`,
      );
    });
  });

  describe('validateServiceUrl', () => {
    it('should return title and description on valid address', async () => {
      expect(await validateServiceUrl(EDR_FINLAND_URL)).toEqual({
        title: EDR_FINLAND_NAME,
        description: EDR_FINLAND_DESCRIPTION,
      });
    });

    it('should return false on invalid url', async () => {
      expect(await validateServiceUrl('unsupported.fi')).toBe(false);
    });

    it('should handle URLs correctly', async () => {
      const spy = jest.spyOn(axios, 'get');
      await validateServiceUrl(`${EDR_FINLAND_URL}?a=3`);
      expect(spy).toHaveBeenLastCalledWith(
        `${EDR_FINLAND_URL}/collections?a=3`,
      );
    });
  });

  describe('getCollectionListForTimeSeriesService', () => {
    it('should return collection list for timeseries service', async () => {
      const expectedResult = [
        {
          collectionId: 'ecmwf',
          serviceId: 'fmi_ecmwf',
          serviceName: 'test',
          parameters: [
            {
              plotType: 'line',
              propertyName: 'Temperature',
              unit: ' ',
              serviceId: 'fmi_ecmwf',
              collectionId: 'ecmwf',
              color: 'X',
              opacity: 70,
            },
          ],
          customDimensions: [
            {
              id: 'timestep',
              interval: [180, 1440],
              reference: 'minutes',
              values: [180, 360, 720, 1440],
            },
          ],
        },
      ];
      const service: TimeSeriesService = {
        name: 'test',
        id: 'fmi_ecmwf',
        description: 'fmi timeseries edr endpoint',
        url: EDR_FINLAND_URL,
        type: 'EDR',
      };
      const result = await getCollectionListForTimeSeriesService(service);
      expect(result).toEqual(expectedResult);
    });
    it('should return collection list for timeseries service when name is missing', async () => {
      const expectedResult = [
        {
          collectionId: 'ecmwf',
          serviceId: 'fmi_ecmwf',
          serviceName: 'fmi_ecmwf',
          parameters: [
            {
              plotType: 'line',
              propertyName: 'Temperature',
              unit: ' ',
              serviceId: 'fmi_ecmwf',
              collectionId: 'ecmwf',
              color: 'X',
              opacity: 70,
            },
          ],
          customDimensions: [
            {
              id: 'timestep',
              interval: [180, 1440],
              reference: 'minutes',
              values: [180, 360, 720, 1440],
            },
          ],
        },
      ];
      const service = {
        id: 'fmi_ecmwf',
        description: 'fmi timeseries edr endpoint',
        url: EDR_FINLAND_URL,
        type: 'EDR',
      } as unknown as TimeSeriesService;
      const result = await getCollectionListForTimeSeriesService(service);
      expect(result).toEqual(expectedResult);
    });
  });

  describe('getParameterListForCollectionId', () => {
    it('should return collection for given id for timeseries service', async () => {
      const expectedResult = {
        collectionId: 'ecmwf',
        parameters: [
          {
            collectionId: 'ecmwf',
            color: 'X',
            opacity: 70,
            plotType: 'line',
            propertyName: 'Temperature',
            serviceId: 'fmi_ecmwf',
            unit: ' ',
          },
        ],
        serviceId: 'fmi_ecmwf',
        serviceName: 'test',
        customDimensions: [
          {
            id: 'timestep',
            interval: [180, 1440],
            reference: 'minutes',
            values: [180, 360, 720, 1440],
          },
        ],
      };
      const service: TimeSeriesService = {
        name: 'test',
        id: 'fmi_ecmwf',
        description: 'fmi timeseries edr endpoint',
        url: EDR_FINLAND_URL,
        type: 'EDR',
      };
      const result = await getParameterListForCollectionId(service, 'ecmwf');
      expect(result).toEqual(expectedResult);
    });
  });

  describe('clipEdrInterval', () => {
    it('should not clip dates if timerange is within cliprange', async () => {
      const [start, end] = clipEdrInterval(
        ['2020-06-10T12:00:00Z', '2020-06-10T18:00:00Z'],
        3,
        3,
        dateUtils.isoStringToDate('2020-06-10T15:00:00Z', false)!.getTime(),
      );
      expect(start).toEqual('2020-06-10T12:00:00Z');
      expect(end).toEqual('2020-06-10T18:00:00Z');
    });
    it('should round dates to minutes if timerange is within cliprange', async () => {
      const [start, end] = clipEdrInterval(
        ['2020-06-10T13:34:56Z', '2020-06-10T18:12:34Z'],
        3,
        3,
        dateUtils.isoStringToDate('2020-06-10T16:01:23Z', false)!.getTime(),
      );
      expect(start).toEqual('2020-06-10T13:34:00Z');
      expect(end).toEqual('2020-06-10T18:12:00Z');
    });
    it('should clip dates if timerange is outside cliprange', async () => {
      const [start, end] = clipEdrInterval(
        ['2020-06-10T12:00:00Z', '2020-06-10T18:00:00Z'],
        2,
        2,
        dateUtils.isoStringToDate('2020-06-10T15:00:00Z', false)!.getTime(),
      );
      expect(start).toEqual('2020-06-10T13:00:00Z');
      expect(end).toEqual('2020-06-10T17:00:00Z');
    });

    it('should clip start if start timerange is outside cliprange', async () => {
      const [start, end] = clipEdrInterval(
        ['2010-06-10T12:00:00Z', '2020-06-10T18:00:00Z'],
        12,
        2,
        dateUtils.isoStringToDate('2020-06-10T18:00:00Z', false)!.getTime(),
      );
      expect(start).toEqual('2020-06-10T06:00:00Z');
      expect(end).toEqual('2020-06-10T18:00:00Z');
    });
    it('should clip end if end timerange is outside cliprange', async () => {
      const [start, end] = clipEdrInterval(
        ['2020-06-10T12:00:00Z', '2020-06-10T23:00:00Z'],
        100,
        2,
        dateUtils.isoStringToDate('2020-06-10T18:00:00Z', false)!.getTime(),
      );
      expect(start).toEqual('2020-06-10T12:00:00Z');
      expect(end).toEqual('2020-06-10T20:00:00Z');
    });
    it('should make a reasonable time range of KNMI EDR interval', async () => {
      const [start, end] = clipEdrInterval(
        ['2003-06-10T12:00:00Z', '2020-06-10T17:00:00Z'],
        12,
        48,
        dateUtils.isoStringToDate('2020-06-10T18:00:00Z', false)!.getTime(),
      );
      expect(start).toEqual('2020-06-10T06:00:00Z');
      expect(end).toEqual('2020-06-10T17:00:00Z');
    });
  });
  describe('parameter_units', () => {
    it('should work without unit', () => {
      const unit = getUnit(
        edrParameterUnitTestValues['param_no_unit'],
        'en',
        'en',
      );
      expect(unit).toEqual('-?-');
    });
    it('unit_en_label_in_en', () => {
      const unit = getUnit(
        edrParameterUnitTestValues['param_en_label'],
        'en',
        'en',
      );
      expect(unit).toEqual('label_en');
    });
    it('unit_fi_label_in_fi', () => {
      const unit = getUnit(
        edrParameterUnitTestValues['param_fi_label'],
        'fi',
        'en',
      );
      expect(unit).toEqual('label_fi');
    });
    it('unit_fallback_label', () => {
      const unit = getUnit(
        edrParameterUnitTestValues['param_fi_en_label'],
        'no',
        'en',
      );
      expect(unit).toEqual('label_en');
    });
    it('unit_fallback_to_random_label', () => {
      const unit = getUnit(
        edrParameterUnitTestValues['param_fi_label'],
        'no',
        'de',
      );
      expect(unit).toEqual('label_fi');
    });
    it('unit_symbol_string', () => {
      const unit = getUnit(
        edrParameterUnitTestValues['param_symbol_str'],
        'en',
        'en',
      );

      expect(unit).toEqual('symbol_str');
    });
  });
  it('unit_symbol_value', () => {
    const unit = getUnit(
      edrParameterUnitTestValues['param_symbol_value'],
      'en',
      'en',
    );
    expect(unit).toEqual('symbol_value');
  });
  it('unit_symbol_and_label', () => {
    const unit = getUnit(
      edrParameterUnitTestValues['param_symbol_and_label'],
      'no',
      'de',
    );
    expect(unit).toEqual('symbol_value');
  });
});
describe('test EDR collections', () => {
  const service: TimeSeriesService = {
    name: 'test',
    id: 'test',
    description: 'test timeseries edr endpoint',
    url: EDR_COLLECTIONS_TEST_URL,
    type: 'EDR',
  };
  describe('EDR /collections', () => {
    it('should call /collections', async () => {
      const res = await fetchEdrCollections('https://geoservices.knmi.nl/edr');
      expect(res.length).toBe(1);
    });
  });
  describe('EDR /collections 2', () => {
    it('should call /collections', async () => {
      const res = await fetchEdrCollections(EDR_COLLECTIONS_TEST_URL);
      expect(res.length).toBe(1);
    });
  });
  describe('EDR without instances', () => {
    it('should call /locations', async () => {
      const res = await getLocations(
        service,
        EDR_COLLECTION_NO_INSTANCES_ID,
        'circ',
        'hover',
      );
      expect(res).toBeDefined();
    });
    it('should not use instance', async () => {
      const expectedResult = {
        collectionId: EDR_COLLECTION_NO_INSTANCES_ID,
        parameters: [
          {
            collectionId: EDR_COLLECTION_NO_INSTANCES_ID,
            color: 'A',
            opacity: 70,
            plotType: 'line',
            propertyName: 'param_id2',
            serviceId: 'test',
            unit: ' ',
          },
        ],
        serviceId: 'test',
        serviceName: 'test',
        customDimensions: [],
      };

      const result = await getParameterListForCollectionId(
        service,
        EDR_COLLECTION_NO_INSTANCES_ID,
      );
      expect(result).toEqual(expectedResult);
    });
  });

  describe('findNearestPoint', () => {
    const targetLocation = { lon: 5.0, lat: 52.0 };

    it('should return null for empty features array', () => {
      const result = findNearestPoint(targetLocation, [], 20);
      expect(result).toBeNull();
    });

    it('should find single point within max distance', () => {
      const features: Feature<Point>[] = [
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [5.02, 52.02], // Near target
          },
          properties: {},
        },
      ];

      const result = findNearestPoint(targetLocation, features, 20);
      expect(result).toBe(features[0]);
    });

    it('should find nearest point among multiple points', () => {
      const features: Feature<Point>[] = [
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [5.5, 52.5], // Further
          },
          properties: {},
        },
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [5.1, 52.1], // Nearest
          },
          properties: {},
        },
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [5.3, 52.3], // Middle
          },
          properties: {},
        },
      ];

      const result = findNearestPoint(targetLocation, features, 20);
      expect(result).toBe(features[1]);
    });

    it('should return null when all points are beyond max distance', () => {
      const features: Feature<Point>[] = [
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [7.0, 54.0],
          },
          properties: {},
        },
      ];

      const result = findNearestPoint(targetLocation, features, 20);
      expect(result).toBeNull();
    });

    it('should handle points at max distance boundary', () => {
      const features: Feature<Point>[] = [
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [5.0, 52.175], // Just under 20km away
          },
          properties: {},
        },
      ];

      const result = findNearestPoint(targetLocation, features, 20);
      expect(result).toBe(features[0]);
    });

    it('should work with longer max distance', () => {
      const features: Feature<Point>[] = [
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [6.0, 53.0], // ~130km away
          },
          properties: {},
        },
      ];

      const resultDefault = findNearestPoint(targetLocation, features);
      expect(resultDefault).toBeNull();

      const resultCustom = findNearestPoint(targetLocation, features, 150);
      expect(resultCustom).toBe(features[0]);
    });
  });
});

describe('getFeatureCenterPoint', () => {
  const pointFeature = {
    type: 'Feature',
    geometry: {
      type: 'Point',
      coordinates: [5.0, 52.0],
    },
    properties: {},
  } as Feature<Point>;

  const polygonFeature = {
    type: 'Feature',
    properties: {},
    geometry: {
      type: 'Polygon',
      coordinates: [
        [
          [10.0, -10.0],
          [20.0, -15.0],
          [18.0, -17.0],
        ],
      ],
    },
  } as Feature<Polygon>;

  it('should return point geometry location', () => {
    const [lon, lat] = getFeatureCenterPoint(pointFeature);
    expect(lon).toBe(5.0);
    expect(lat).toBe(52.0);
  });

  it('should return polygon geometry center calculated by turf', () => {
    const [lon, lat] = getFeatureCenterPoint(polygonFeature);
    const turfCenter = turf.center(polygonFeature).geometry.coordinates;
    expect(lon).toBe(turfCenter[0]);
    expect(lat).toBe(turfCenter[1]);
  });
});
