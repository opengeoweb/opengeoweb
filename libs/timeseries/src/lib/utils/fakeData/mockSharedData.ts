/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { FeatureCollection, Point } from 'geojson';
import { helsinkiLat, helsinkiLon } from '../../../mocks';

export const mockGeoJSON: FeatureCollection<Point> = {
  type: 'FeatureCollection',
  features: [
    {
      id: 'helsinki',
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [helsinkiLon, helsinkiLat],
      },
      properties: {
        name: 'Helsinki',
        serviceId: 'FMI',
        collectionId: 'helsinki-forecast',
        drawFunctionId: 'drawFunctionId_1',
        hoverDrawFunctionId: 'drawFunctionId_2',
      },
    },
    {
      id: 'a12cpp',
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [3.8103, 55.3992],
      },
      properties: {
        name: 'A12-CPP',
        serviceId: 'METNorway',
        collectionId: 'meps-det-vdiv',
        drawFunctionId: 'drawFunctionId_3',
        hoverDrawFunctionId: 'drawFunctionId_4',
      },
    },
    {
      id: 'rome',
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [12.4924, 41.8902],
      },
      properties: {
        name: 'Rome',
        serviceId: 'FMI',
        collectionId: 'rome-forecast',
        drawFunctionId: 'drawFunctionId_5',
        hoverDrawFunctionId: 'drawFunctionId_6',
      },
    },
    {
      id: 'newyork',
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [-74.006, 40.7128],
      },
      properties: {
        name: 'New York',
        serviceId: 'NOAA',
        collectionId: 'ny-observations',
        drawFunctionId: 'drawFunctionId_7',
        hoverDrawFunctionId: 'drawFunctionId_8',
      },
    },
  ],
};
