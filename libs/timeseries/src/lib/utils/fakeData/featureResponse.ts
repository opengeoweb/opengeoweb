/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

export const featureResponse = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      geometry: { type: 'Point', coordinates: [4.89707, 52.377956] },
      properties: {
        timestep: [
          '2023-04-14T09:00:00Z',
          '2023-04-14T10:00:00Z',
          '2023-04-14T11:00:00Z',
          '2023-04-14T12:00:00Z',
          '2023-04-14T13:00:00Z',
          '2023-04-14T14:00:00Z',
          '2023-04-14T15:00:00Z',
          '2023-04-14T16:00:00Z',
          '2023-04-14T17:00:00Z',
          '2023-04-14T18:00:00Z',
          '2023-04-14T19:00:00Z',
          '2023-04-14T20:00:00Z',
          '2023-04-14T21:00:00Z',
          '2023-04-14T22:00:00Z',
          '2023-04-14T23:00:00Z',
          '2023-04-15T00:00:00Z',
          '2023-04-15T01:00:00Z',
          '2023-04-15T02:00:00Z',
          '2023-04-15T03:00:00Z',
          '2023-04-15T04:00:00Z',
          '2023-04-15T05:00:00Z',
          '2023-04-15T06:00:00Z',
          '2023-04-15T07:00:00Z',
          '2023-04-15T08:00:00Z',
          '2023-04-15T09:00:00Z',
          '2023-04-15T10:00:00Z',
          '2023-04-15T11:00:00Z',
          '2023-04-15T12:00:00Z',
          '2023-04-15T13:00:00Z',
          '2023-04-15T14:00:00Z',
          '2023-04-15T15:00:00Z',
          '2023-04-15T16:00:00Z',
          '2023-04-15T17:00:00Z',
          '2023-04-15T18:00:00Z',
          '2023-04-15T19:00:00Z',
          '2023-04-15T20:00:00Z',
          '2023-04-15T21:00:00Z',
          '2023-04-15T22:00:00Z',
          '2023-04-15T23:00:00Z',
          '2023-04-16T00:00:00Z',
          '2023-04-16T01:00:00Z',
          '2023-04-16T02:00:00Z',
          '2023-04-16T03:00:00Z',
          '2023-04-16T04:00:00Z',
          '2023-04-16T05:00:00Z',
          '2023-04-16T06:00:00Z',
          '2023-04-16T07:00:00Z',
          '2023-04-16T08:00:00Z',
          '2023-04-16T09:00:00Z',
        ],
        dims: { reference_time: '2023-04-14T09:00:00Z' },
        observationType: 'MeasureTimeseriesObservation',
        observedPropertyName: 'relative_humidity__at_2m',
        result: [
          65.859097, 56.851739, 51.378381, 46.309346, 45.613509, 42.316067,
          40.736789, 45.416671, 47.311819, 56.119245, 63.890684, 67.588055,
          70.809245, 74.329513, 73.301107, 78.652447, 84.658337, 88.296074,
          90.44801, 92.1947, 92.968267, 90.580279, 86.010182, 75.715637,
          64.771235, 61.238754, 59.895897, 58.472162, 62.259394, 65.085298,
          67.844087, 69.515014, 71.260661, 73.632002, 78.200585, 81.292063,
          82.927829, 83.490938, 85.842448, 88.791978, 89.997452, 91.241479,
          92.952198, 93.97611, 94.379771, 93.858343, 91.059887, 85.030639,
          76.082724,
        ],
      },
      id: 'HARM_N25;relative_humidity__at_2m;4.897070,52.377956;reference_time=2023-04-14T09:00:00Z;2023-04-14T09:00:00Z$2023-04-16T09:00:00Z',
    },
  ],
  timeStamp: '2023-04-14T12:46:19Z',
  numberReturned: 1,
  numberMatched: 1,
  links: [
    {
      rel: 'self',
      type: 'application/geo+json',
      title: 'This document',
      href: '/ogcapi/collections/HARM_N25/items',
    },
    {
      rel: 'alternate',
      type: 'text/html',
      title: 'This document',
      href: '/ogcapi/collections/HARM_N25/items?f=html',
    },
  ],
};
