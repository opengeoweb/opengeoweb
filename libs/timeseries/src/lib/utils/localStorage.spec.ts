/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { TimeSeriesService } from '@opengeoweb/shared';
import {
  getUserAddedEDRServices,
  setUserAddedEDRServices,
} from './localStorage';

beforeEach(() => {
  window.localStorage.clear();
});

const testServices: TimeSeriesService[] = [
  {
    id: 'testId',
    name: 'test service 1',
    description: 'test description',
    url: 'https://example.com/edr',
    type: 'EDR',
    scope: 'system',
  },
];

describe('src/lib/utils/localStorage', () => {
  describe('getUserAddedServices', () => {
    it('should return empty userAddedServices when localStorage is empty', () => {
      expect(getUserAddedEDRServices()).toEqual([]);
    });
    it('should return valid UserAddedServices from localStorage', () => {
      window.localStorage.setItem(
        'userAddedEDRServices',
        JSON.stringify(testServices),
      );
      expect(getUserAddedEDRServices()).toEqual(testServices);
    });
    it('should return empty array if stored JSON is invalid', () => {
      const consoleSpy = jest.spyOn(console, 'error').mockImplementation();
      window.localStorage.setItem('userAddedEDRServices', 'invalidJSON');
      expect(getUserAddedEDRServices()).toEqual([]);
      expect(consoleSpy).toHaveBeenCalled();
    });
  });
});

describe('setUserAddedServices', () => {
  it('should persist given UserAddedServices', () => {
    setUserAddedEDRServices(testServices);
    expect(
      JSON.parse(window.localStorage.getItem('userAddedEDRServices')!),
    ).toEqual(testServices);
  });
});
