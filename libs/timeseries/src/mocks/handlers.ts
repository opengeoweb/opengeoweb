/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { http, HttpResponse } from 'msw';
import {
  EDRCollection,
  EDRDomain,
  EDRInstance,
  EDRParameter,
  EDRParameters,
  EDRPositionResponse,
  EDRRange,
  EDRRangeParameter,
  Parameter,
} from '../lib/components/TimeSeries/types';

export const EDR_FINLAND_NAME = 'EDR Finland';
export const EDR_FINLAND_DESCRIPTION = 'EDR Finland Description';

export const EDR_FINLAND_URL = 'https://opendata.fmi.fi/edr';
export const EDR_FINLAND_ECMWF_COLLECTION_ID = 'ecmwf';

export const EDR_NORWAY_URL = 'https://interpol.met.no';
export const EDR_NORWAY_COLLECTION_ID = 'meps-det-vdiv';

export const EDR_KNMIGEOSERVICES_URL = 'https://geoservices.knmi.nl/edr';
export const EDR_KNMIGEOSERVICES_COLLECTION_ID = 'uwcw_ha43_dini_5p5km_hagl';

export const EDR_COLLECTIONS_TEST_URL =
  'https://collection-test.opengeoweb.com/edr';
export const EDR_COLLECTION_NO_INSTANCES_ID = 'without_instances';
export const EDR_COLLECTION_INSTANCES_LOCATIONS = 'instances_locations';
export const EDR_COLLECTION_INSTANCES_POSITION = 'instances_position';
export const EDR_COLLECTION_INSTANCES_LOCATIONS_AND_POSITION =
  'instances_locations_and_position';

export const OGC_URL =
  'https://geoservices.knmi.nl/ogcapi/collections/uwcw_ha43_dini_5p5km/items';
const date = `20230927`;
export const latestInstanceIdMock = `${date}T120000`;
const oldInstanceIdMock = `${date}T000000`;
export const latestInstanceMock: EDRInstance = {
  id: latestInstanceIdMock,
  extent: {
    spatial: {
      crs: 'CRS:84',
      bbox: [-180, -90, 180, 90],
    },
    temporal: {
      interval: [['2023-09-26T03:00:00Z', '2023-10-06T00:00:00Z']],
      crs: 'TIMECRS["DateTime",TDATUM["Gregorian Calendar"],CS[TemporalDateTime,1],AXIS["Time (T)",future]',
      values: ['R60/2023-09-26T00:00:00Z/PT240M'],
    },
  },
  data_queries: {
    position: {
      link: {
        variables: {
          output_formats: ['CoverageJSON'],
        },
      },
    },
    locations: {
      link: {
        variables: {
          output_formats: ['CoverageJSON'],
        },
      },
    },
  },
};

export const oldInstanceMock: EDRInstance = {
  id: oldInstanceIdMock,
  extent: {
    spatial: {
      crs: 'CRS:84',
      bbox: [-180, -90, 180, 90],
    },
    temporal: {
      interval: [['2023-09-26T06:00:00Z', '2023-10-06T03:00:00Z']],
      crs: 'TIMECRS["DateTime",TDATUM["Gregorian Calendar"],CS[TemporalDateTime,1],AXIS["Time (T)",future]',
      values: ['R60/2023-09-26T00:00:00Z/PT240M'],
    },
  },
  data_queries: {
    position: {
      link: {
        variables: {
          output_formats: ['CoverageJSON'],
        },
      },
    },
  },
};

export const helsinkiLat = 60.192059;
export const helsinkiLon = 24.945831;
export const helsinki = 'Helsinki';

export const parameterNameMock = 'Temperature';
export const parameterUnitMock = '˚C';

export const parameterMock: Parameter = {
  plotId: 'plot-1',
  propertyName: parameterNameMock,
  unit: parameterUnitMock,
  plotType: 'line',
  serviceId: 'test-service',
  collectionId: EDR_FINLAND_ECMWF_COLLECTION_ID,
};

export const timestepMock = [
  '2023-09-27T03:00:00.000Z',
  '2023-09-27T04:00:00.000Z',
  '2023-09-27T05:00:00.000Z',
];
export const valueMock = [1020, 1020, 1020];
export const valueMock2 = [1030, 1030, 1030];

const edrParameters = {
  [parameterNameMock]: {
    id: parameterNameMock,
    unit: {
      symbol: {
        value: parameterUnitMock,
      },
    },
  } as EDRParameter,
} as EDRParameters;
const edrParametersMetNo = [
  {
    id: parameterNameMock,
    unit: {
      symbol: {
        value: parameterUnitMock,
      },
    },
  } as EDRParameter,
];
const edrParameterTestDataQueriesWI = {
  position: {
    link: {
      variables: {
        output_formats: ['CoverageJSON'],
      },
    },
  },
  locations: {
    link: {
      variables: {
        output_formats: ['CoverageJSON'],
      },
    },
  },
  instances: {
    link: {},
  },
};

const edrParameterTestDataQueries = {
  position: {
    link: {
      variables: {
        output_formats: ['CoverageJSON'],
      },
    },
  },
  locations: {
    link: {
      variables: {
        // output_formats: ['CoverageJSON'],
      },
    },
  },
};

export const finlandEcmwfInstances = {
  instances: [
    {
      id: oldInstanceIdMock,
      data_queries: edrParameterTestDataQueries,
      parameter_names: { id1: { id: 'id1' } },
    },
    {
      id: latestInstanceIdMock,
      data_queries: edrParameterTestDataQueries,
      parameter_names: { id2: { id: 'id2' } },
    },
  ],
};

export const knmiUwcwHa43DInifInstances = {
  instances: [
    {
      id: oldInstanceIdMock,
      data_queries: edrParameterTestDataQueries,
      parameter_names: { id1: { id: 'id1' } },
    },
    {
      id: latestInstanceIdMock,
      data_queries: edrParameterTestDataQueries,
      parameter_names: { id2: { id: 'id2' } },
    },
  ],
};
const edrParameterTestDataQueriesNorwayWI = {
  locations: {
    link: {
      variables: {
        output_formats: ['CoverageJSON'],
      },
    },
  },
  instances: {
    link: {},
  },
};
const edrParameterTestDataQueriesNorway = {
  locations: {
    link: {
      variables: {
        output_formats: ['CoverageJSON'],
      },
    },
  },
};
const norwayInstances = {
  instances: [
    {
      id: oldInstanceIdMock,
      data_queries: edrParameterTestDataQueriesNorway,
      parameters: [{ id: 'id1' }],
    },
    {
      id: latestInstanceIdMock,
      data_queries: edrParameterTestDataQueriesNorway,
      parameters: [{ id: 'id2' }],
    },
  ],
};
export const edrPositionResponseMock = {
  domain: {
    axes: {
      t: {
        values: timestepMock,
      },
    },
  } as EDRDomain,
  parameters: edrParameters,
  ranges: {
    [parameterNameMock]: {
      values: valueMock,
    } as EDRRangeParameter,
  } as EDRRange,
} as EDRPositionResponse;

export const edrLocationResponseMock = {
  domain: {
    axes: {
      t: {
        values: timestepMock,
      },
    },
  } as EDRDomain,
  parameters: edrParameters,
  ranges: {
    [parameterNameMock]: {
      values: valueMock2,
    } as EDRRangeParameter,
  } as EDRRange,
} as EDRPositionResponse;

const locations = {
  features: [
    {
      id: 'helsinki',
      geometry: {
        type: 'Point',
        coordinates: [helsinkiLon, helsinkiLat],
      },
      properties: {
        name: helsinki,
      },
    },
  ],
};

export const collectionsMockNorway: EDRCollection = {
  id: EDR_NORWAY_COLLECTION_ID,
  title: EDR_NORWAY_COLLECTION_ID,
  description: EDR_NORWAY_COLLECTION_ID,
  extent: {
    spatial: {
      crs: '',
      bbox: [0, 0, 0, 0],
    },
  },
  data_queries: { locations: {}, instances: {} },
  parameters: [{ id: 'Temperature' }],
};

export const collectionsMockFinland: EDRCollection = {
  id: EDR_FINLAND_ECMWF_COLLECTION_ID,
  title: EDR_FINLAND_ECMWF_COLLECTION_ID,
  description: EDR_FINLAND_ECMWF_COLLECTION_ID,
  extent: {
    spatial: {
      crs: '',
      bbox: [0, 0, 0, 0],
    },
    custom: [
      {
        id: 'timestep',
        interval: [180, 1440],
        reference: 'minutes',
        values: [180, 360, 720, 1440],
      },
    ],
  },
  data_queries: { position: {}, instances: {} },
  parameter_names: { Temperature: { id: 'Temperature' } },
};
export const edrEndpoints = [
  http.get(`${EDR_FINLAND_URL}`, () => {
    return HttpResponse.json({
      title: EDR_FINLAND_NAME,
      description: EDR_FINLAND_DESCRIPTION,
    });
  }),
  http.get(`${EDR_FINLAND_URL}/collections`, () => {
    return HttpResponse.json({
      collections: [collectionsMockFinland],
    });
  }),
  http.get(`${EDR_NORWAY_URL}/collections`, () => {
    return HttpResponse.json({
      collections: [collectionsMockNorway],
    });
  }),
  http.get(
    `${EDR_FINLAND_URL}/collections/${EDR_FINLAND_ECMWF_COLLECTION_ID}/instances`,
    () => {
      return HttpResponse.json(finlandEcmwfInstances);
    },
  ),
  http.get(
    `${EDR_FINLAND_URL}/collections/${EDR_FINLAND_ECMWF_COLLECTION_ID}/locations`,
    () => {
      return HttpResponse.json(locations);
    },
  ),
  http.get(
    `${EDR_NORWAY_URL}/collections/${EDR_NORWAY_COLLECTION_ID}/locations`,
    () => {
      return HttpResponse.json(locations);
    },
  ),
  // these endpoints are only used by norway
  http.get(`${EDR_NORWAY_URL}/collections/${EDR_NORWAY_COLLECTION_ID}`, () => {
    return HttpResponse.json({
      id: `${EDR_NORWAY_COLLECTION_ID}`,
      parameters: edrParametersMetNo,
      data_queries: edrParameterTestDataQueriesNorwayWI,
    });
  }),
  http.get(
    `${EDR_NORWAY_URL}/collections/${EDR_NORWAY_COLLECTION_ID}/instances/${latestInstanceIdMock}`,
    () => {
      return HttpResponse.json({
        parameters: edrParametersMetNo,
        data_queries: edrParameterTestDataQueriesNorway,
      });
    },
  ),
  http.get(
    `${EDR_NORWAY_URL}/collections/${EDR_NORWAY_COLLECTION_ID}/instances`,
    () => {
      return HttpResponse.json(norwayInstances);
    },
  ),

  http.get(
    `${EDR_NORWAY_URL}/collections/${EDR_NORWAY_COLLECTION_ID}/instances/${latestInstanceIdMock}/locations/:id`,
    () => {
      return HttpResponse.json(edrPositionResponseMock);
    },
  ),
  // these endpoints are only used by finland
  http.get(
    `${EDR_FINLAND_URL}/collections/${EDR_FINLAND_ECMWF_COLLECTION_ID}`,
    () => {
      return HttpResponse.json({
        id: `${EDR_FINLAND_ECMWF_COLLECTION_ID}`,
        parameter_names: edrParameters,
        data_queries: edrParameterTestDataQueriesWI,
      });
    },
  ),
  http.get(
    `${EDR_FINLAND_URL}/collections/${EDR_FINLAND_ECMWF_COLLECTION_ID}/position`,
    ({ request }) => {
      // if not supported parameter return error status
      const url = new URL(request.url);
      if (url.searchParams.get('parameter-name') !== parameterNameMock) {
        return HttpResponse.json({ error: 'Bad Request' }, { status: 400 });
      }

      return HttpResponse.json(edrPositionResponseMock);
    },
  ),
  http.get(
    `${EDR_FINLAND_URL}/collections/${EDR_FINLAND_ECMWF_COLLECTION_ID}/instances/${latestInstanceIdMock}/position`,
    ({ request }) => {
      const url = new URL(request.url);
      // if not supported parameter return error status
      if (url.searchParams.get('parameter-name') !== parameterNameMock) {
        return HttpResponse.json({ error: 'Bad Request' }, { status: 400 });
      }
      return HttpResponse.json(edrPositionResponseMock);
    },
  ),
  http.get(
    `${EDR_FINLAND_URL}/collections/${EDR_FINLAND_ECMWF_COLLECTION_ID}/instances/${latestInstanceIdMock}/locations/helsinki`,
    ({ request }) => {
      const url = new URL(request.url);
      // if not supported parameter return error status
      if (url.searchParams.get('parameter-name') !== parameterNameMock) {
        return HttpResponse.json({ error: 'Bad Request' }, { status: 400 });
      }
      return HttpResponse.json(edrLocationResponseMock);
    },
  ),
  http.get(
    `${EDR_FINLAND_URL}/collections/${EDR_FINLAND_ECMWF_COLLECTION_ID}/instances/${latestInstanceIdMock}`,
    () => {
      return HttpResponse.json({
        id: latestInstanceIdMock,
        parameter_names: edrParameters,
        data_queries: edrParameterTestDataQueries,
      });
    },
  ),
  http.get(`${EDR_COLLECTIONS_TEST_URL}/collections`, () => {
    return HttpResponse.json({
      collections: [
        {
          id: EDR_COLLECTION_NO_INSTANCES_ID,
          title: 'title',
          description: 'description',
          data_queries: { position: { link: {} } },
          parameter_names: { param_id2: { id: 'param_id2' } },
          output_formats: ['CoverageJSON'],
          extent: {},
        },
      ],
    });
  }),
  http.get(
    `${EDR_COLLECTIONS_TEST_URL}/collections/${EDR_COLLECTION_NO_INSTANCES_ID}`,
    () => {
      return HttpResponse.json({
        id: EDR_COLLECTION_NO_INSTANCES_ID,
        title: 'title',
        description: 'description',
        data_queries: { position: { link: {} } },
        parameter_names: { param_id2: { id: 'param_id2' } },
        output_formats: ['CoverageJSON'],
        extent: {},
      });
    },
  ),
  http.get(
    `${EDR_COLLECTIONS_TEST_URL}/collections/${EDR_COLLECTION_NO_INSTANCES_ID}/locations`,
    () => {
      return HttpResponse.json(edrTestCollectionNoInstancesLocationsMock);
    },
  ),
  http.get(`${EDR_KNMIGEOSERVICES_URL}/collections`, () => {
    return HttpResponse.json({
      links: [],
      collections: [
        {
          id: 'without_instances_1',
          title: 'title',
          description: 'description',
          data_queries: [
            { position: { link: {} } },
            { location: { link: {} } },
          ],
          parameter_names: [{ id: 'param_id2' }],
        },
      ],
    });
  }),
  http.get(
    `${EDR_KNMIGEOSERVICES_URL}/collections/${EDR_KNMIGEOSERVICES_COLLECTION_ID}/instances`,
    () => {
      return HttpResponse.json(knmiUwcwHa43DInifInstances);
    },
  ),
];
export const edrTestCollectionNoInstancesMock = {};
export const edrTestCollectionNoInstancesLocationsMock = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [2.9358, 54.3256, 42.7],
      },
      properties: {
        name: 'D15-FA-1',
      },
      id: '06201',
    },
    {
      id: 'Brazil',
      type: 'Feature',
      properties: {
        name: 'Brazil',
      },
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [-73.987235, -33.768378],
            [-73.987235, 5.244486],
            [-34.729993, 5.244486],
            [-34.729993, -33.768378],
            [-73.987235, -33.768378],
          ],
        ],
      },
    },
  ],
};

export const ogcEndpoints = [
  http.get(OGC_URL, () => {
    return HttpResponse.json({
      features: [
        {
          properties: {
            timestep: timestepMock,
            result: valueMock,
          },
        },
      ],
    });
  }),
];
export const unsupportedEndpoints = [
  http.get('*', ({ request }) => {
    return HttpResponse.json(
      { error: `Unsupported mock endpoint: ${request.url}` },
      { status: 400 },
    );
  }),
];
