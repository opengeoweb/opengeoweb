/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { Button } from '@mui/material';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';

import { dateUtils } from '@opengeoweb/shared';
import type { Meta, StoryObj } from '@storybook/react';
import { ReactHookFormDateTime } from '.';
import { FormFieldsWrapper, zeplinLinks } from './Providers';

type Story = StoryObj<typeof ReactHookFormDateTime>;

const meta: Meta<typeof ReactHookFormDateTime> = {
  title: 'Components/Date Time',
  component: ReactHookFormDateTime,
  parameters: {
    docs: {
      description: {
        component: 'ReactHookFormDateTime component',
      },
    },
  },
};
export default meta;

export const Component: Story = {
  args: {
    name: 'date-time-example',
    label: 'Date time label',
    rules: {
      required: false,
      validate: {
        isValidDate: dateUtils.isValidIsoDateString,
      },
    },
    isReadOnly: false,
    disabled: false,
    helperText: '',
    'data-testid': 'some string',
    shouldHideUTC: false,
    format: 'dd/MM/yyyy HH:mm',
    openTo: 'hours',
    defaultNullValue: '',
  },
  render: (props) => (
    <FormFieldsWrapper
      options={{
        defaultValues: {
          [props.name]: dateUtils.dateToString(dateUtils.utc()),
        },
      }}
    >
      <ReactHookFormDateTime {...props} />
    </FormFieldsWrapper>
  ),
};

const DateTimeDemoLayout: React.FC<{ children?: React.ReactNode }> = ({
  children,
}) => (
  <>
    <ReactHookFormDateTime
      name="dateTime-A"
      rules={{
        required: true,
        validate: {
          isValidDate: dateUtils.isValidIsoDateString,
        },
      }}
      label="Test label"
    />

    <ReactHookFormDateTime
      name="dateTime-B"
      rules={{
        required: true,
        validate: {
          isValidDate: dateUtils.isValidIsoDateString,
        },
      }}
    />

    <ReactHookFormDateTime
      name="dateTime-readonly"
      label="readonly disabled"
      disabled
      isReadOnly
      rules={{
        required: true,
        validate: {
          isValidDate: dateUtils.isValidIsoDateString,
        },
      }}
    />

    <ReactHookFormDateTime
      name="dateTime-C"
      disabled
      rules={{
        required: true,
        validate: {
          isValidDate: dateUtils.isValidIsoDateString,
        },
      }}
    />

    <ReactHookFormDateTime
      name="dateTime-D"
      disabled
      rules={{
        required: true,
        validate: {
          isValidDate: dateUtils.isValidIsoDateString,
        },
      }}
    />

    <ReactHookFormDateTime
      name="dateTime-E"
      label="Spaceweather example"
      disablePast
      rules={{
        required: true,
        validate: {
          isValidDate: dateUtils.isValidIsoDateString,
        },
      }}
      shouldHideUTC
      sx={{
        width: 200,
        marginRight: 2,
      }}
    />

    <DateTimeErrorDemo />

    <ReactHookFormDateTime
      name="dateTime-G"
      label="NOW +10mins disablePast"
      rules={{
        required: true,
        validate: {
          isValidDate: dateUtils.isValidIsoDateString,
        },
      }}
      disablePast
    />

    <ReactHookFormDateTime
      name="dateTime-H"
      label="NOW -10mins disableFuture"
      rules={{
        required: true,
        validate: {
          isValidDate: dateUtils.isValidIsoDateString,
        },
      }}
      disableFuture
    />

    {children}
  </>
);

const DateTimeErrorDemo = (): React.ReactElement => {
  const { setError } = useFormContext();

  React.useEffect(() => {
    setError('dateTime-F', { message: 'Error!' });
  }, [setError]);

  return <ReactHookFormDateTime label="error" name="dateTime-F" />;
};

const DateTimeDemo = (): React.ReactElement => {
  const { handleSubmit } = useFormContext();

  return (
    <DateTimeDemoLayout>
      <Button
        variant="contained"
        color="secondary"
        onClick={handleSubmit(() => {})}
      >
        Validate
      </Button>
    </DateTimeDemoLayout>
  );
};

export const DateTime: Story = {
  render: () => (
    <FormFieldsWrapper
      options={{
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues: {
          'dateTime-B': '2021-02-01T12:00:00Z',
          'dateTime-D': '2021-02-01T12:00:00Z',
          'dateTime-readonly': '2021-02-01T12:00:00Z',
          'dateTime-G': dateUtils.dateToString(
            dateUtils.add(dateUtils.utc(), { minutes: 10 }),
          ),
          'dateTime-H': dateUtils.dateToString(
            dateUtils.sub(dateUtils.utc(), { minutes: 10 }),
          ),
        },
      }}
    >
      <DateTimeDemo />
    </FormFieldsWrapper>
  ),
  name: 'DateTime examples',
  parameters: {
    zeplinLink: zeplinLinks,
    docs: {
      description: {
        story: 'Press the submit button to test out validation',
      },
    },
  },
};

// snapshots
const SnapShotLayout = ({ ...props }): React.ReactElement => (
  <FormFieldsWrapper
    {...props}
    options={{
      mode: 'onChange',
      reValidateMode: 'onChange',
      defaultValues: {
        'dateTime-B': '2021-02-01T12:00:00Z',
        'dateTime-D': '2021-02-01T12:00:00Z',
        'dateTime-readonly': '2021-02-01T12:00:00Z',
      },
    }}
  >
    <DateTimeDemoLayout />
  </FormFieldsWrapper>
);

// light theme
export const DateTimeLightTheme: Story = {
  render: () => <SnapShotLayout />,
  tags: ['snapshot'],
  parameters: {
    zeplinLink: [zeplinLinks[0]],
  },
};

// dark theme
export const DateTimeDarkTheme: Story = {
  render: () => <SnapShotLayout />,
  tags: ['snapshot', 'dark'],
  parameters: {
    zeplinLink: [zeplinLinks[1]],
  },
};
