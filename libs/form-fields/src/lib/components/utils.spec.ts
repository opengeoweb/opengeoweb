/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { Polygon } from 'geojson';
import {
  isLatitude,
  isLongitude,
  isValidMax,
  isValidGeoJsonCoordinates,
  isMaximumOneDrawing,
  isGeometryDirty,
  hasIntersectionWithFIR,
  isNonOrBothCoordinates,
  isNumeric,
  isValidMin,
  isEmpty,
  isInteger,
  hasMulitpleIntersections,
  isXHoursBefore,
  isXHoursAfter,
  countIntersectionPoints,
} from './utils';

describe('components/ReactHookForm/utils', () => {
  describe('isEmpty', () => {
    it('should return true if value is empty', () => {
      expect(isEmpty('')).toBeTruthy();
      expect(isEmpty(' ')).toBeTruthy();
      expect(isEmpty('  ')).toBeTruthy();
      expect(isEmpty(null)).toBeTruthy();
      expect(isEmpty(undefined)).toBeTruthy();
    });
    it('should return false if value is not empty', () => {
      expect(isEmpty('1')).toBeFalsy();
      expect(isEmpty(0)).toBeFalsy();
    });
  });

  describe('isXHoursBefore', () => {
    it('should return true when 4 hours difference before', () => {
      const startValue = '2020-09-17T13:00:00Z';
      const endValue = '2020-09-17T17:00:00Z';
      expect(isXHoursBefore(startValue, endValue)).toBeTruthy();
    });

    it('should return true when 8 hours difference before', () => {
      const startValue = '2020-09-17T13:00:00Z';
      const endValue = '2020-09-17T21:00:00Z';
      expect(isXHoursBefore(startValue, endValue, 8)).toBeTruthy();
    });

    it('should return true when less than 4 hours difference before', () => {
      const startValue = '2020-09-17T13:00:00Z';
      const endValue = '2020-09-17T14:00:00Z';
      expect(isXHoursBefore(startValue, endValue)).toBeTruthy();
    });

    it('should return false when more than 4 hours difference before', () => {
      const startValue = '2020-09-17T13:00:00Z';
      const endValue = '2020-09-17T17:01:00Z';
      expect(isXHoursBefore(startValue, endValue)).toBeFalsy();
    });

    it('should return true when values are empty', () => {
      expect(isXHoursBefore('', '')).toBeTruthy();
    });
  });

  describe('isXHoursAfter', () => {
    it('should return true when 4 hours difference after', () => {
      const startValue = '2020-09-17T13:00:00Z';
      const endValue = '2020-09-17T17:00:00Z';
      expect(isXHoursAfter(startValue, endValue)).toBeTruthy();
    });

    it('should return true when 8 hours difference after', () => {
      const startValue = '2020-09-17T13:00:00Z';
      const endValue = '2020-09-17T21:00:00Z';
      expect(isXHoursAfter(endValue, startValue, 8)).toBeTruthy();
    });

    it('should return true when less than 4 hours difference after', () => {
      const startValue = '2020-09-17T13:00:00Z';
      const endValue = '2020-09-17T14:00:00Z';
      expect(isXHoursAfter(endValue, startValue)).toBeTruthy();
    });

    it('should return false when more than 4 hours difference after', () => {
      const startValue = '2020-09-17T13:00:00Z';
      const endValue = '2020-09-17T17:01:00Z';
      expect(isXHoursAfter(endValue, startValue)).toBeFalsy();
    });

    it('should return true when values are empty', () => {
      expect(isXHoursAfter('', '')).toBeTruthy();
    });
  });

  describe('isLatitude', () => {
    it('should return true when valid latitude', () => {
      expect(isLatitude(60.12)).toBeTruthy();
      expect(isLatitude(0.01)).toBeTruthy();
      expect(isLatitude(90)).toBeTruthy();
      expect(isLatitude(null)).toBeTruthy();
      expect(isLatitude(undefined)).toBeTruthy();
    });
    it('should return false when not valid latitude', () => {
      expect(isLatitude(0.123)).toBeFalsy();
      expect(isLatitude(90.01)).toBeFalsy();
      expect(isLatitude(-90.01)).toBeFalsy();
    });
  });

  describe('isLongitude', () => {
    it('should return true when valid longitude', () => {
      expect(isLongitude(60.12)).toBeTruthy();
      expect(isLongitude(0)).toBeTruthy();
      expect(isLongitude(180)).toBeTruthy();
      expect(isLongitude(-180)).toBeTruthy();
      expect(isLongitude(null)).toBeTruthy();
      expect(isLongitude(undefined)).toBeTruthy();
    });
    it('should return false when not valid longitude', () => {
      expect(isLongitude(0.123)).toBeFalsy();
      expect(isLongitude(181)).toBeFalsy();
      expect(isLongitude(-181)).toBeFalsy();
    });
  });

  describe('isValidMax', () => {
    it('should return true when value is valid for the given max value', () => {
      expect(isValidMax(1, 2)).toBeTruthy();
      expect(isValidMax(2, 2)).toBeTruthy();
    });

    it('should return false when value is invalid for the given max value', () => {
      expect(isValidMax(3, 2)).toBeFalsy();
    });

    it('should return true when value is empty', () => {
      expect(isValidMax('' as unknown as number, 1)).toBeTruthy();
    });
  });

  describe('isValidMin', () => {
    it('should return true when value is valid for the given min value', () => {
      expect(isValidMin(1, 1)).toBeTruthy();
      expect(isValidMin(2, 1)).toBeTruthy();
    });

    it('should return false when value is invalid for the given min value', () => {
      expect(isValidMin(1, 2)).toBeFalsy();
    });

    it('should return true when value is empty', () => {
      expect(isValidMin('' as unknown as number, 1)).toBeTruthy();
    });
  });

  describe('isValidGeoJsonCoordinates', () => {
    it('should return false when geojson has no geometry', () => {
      const geojsonNoGeometry: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
            },
            geometry: null!,
          },
        ],
      };
      expect(isValidGeoJsonCoordinates(geojsonNoGeometry)).toBeFalsy();
    });

    it('should return false when geojson has empty polygon coordinates', () => {
      const geojsonNoPolygonCoordinates: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [[]],
            },
          },
        ],
      };
      expect(
        isValidGeoJsonCoordinates(geojsonNoPolygonCoordinates),
      ).toBeFalsy();
    });

    it('should return false when geojson has empty point coordinates', () => {
      const geojsonNoPointCoordinates: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'point',
            },
            geometry: {
              type: 'Point',
              coordinates: [],
            },
          },
        ],
      };
      expect(isValidGeoJsonCoordinates(geojsonNoPointCoordinates)).toBeFalsy();
    });

    it('should return true when geojson has polygon coordinates', () => {
      const geojsonPolygonCoordinates: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [3.7513346922388284, 52.93209131750574],
                  [4.890169687103436, 52.26807619123409],
                  [3.250247294498403, 51.960365357854286],
                  [3.7513346922388284, 52.93209131750574],
                ],
              ],
            },
          },
        ],
      };
      expect(isValidGeoJsonCoordinates(geojsonPolygonCoordinates)).toBeTruthy();
    });

    it('should return true when geojson has point coordinates', () => {
      const geojsonPointCoordinates: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'point',
            },
            geometry: {
              type: 'Point',
              coordinates: [3.7513346922388284, 52.93209131750574],
            },
          },
        ],
      };
      expect(isValidGeoJsonCoordinates(geojsonPointCoordinates)).toBeTruthy();
    });
  });

  describe('isMaximumOneDrawing', () => {
    it('should return true when geojson has no geometry', () => {
      const geojsonNoGeometry: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
            },
            geometry: null!,
          },
        ],
      };
      expect(isMaximumOneDrawing(geojsonNoGeometry)).toBeTruthy();
    });

    it('should return true when geojson has empty polygon coordinates', () => {
      const geojsonNoPolygonCoordinates: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [[]],
            },
          },
        ],
      };
      expect(isMaximumOneDrawing(geojsonNoPolygonCoordinates)).toBeTruthy();
    });

    it('should return false when geojson for polygon has multiple drawings', () => {
      const geojsonPointPolygonCoordinates: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [3.7513346922388284, 52.93209131750574],
                  [4.890169687103436, 52.26807619123409],
                  [3.250247294498403, 51.960365357854286],
                  [3.7513346922388284, 52.93209131750574],
                ],
                [
                  [3.7513346922388284, 52.93209131750574],
                  [4.890169687103436, 52.26807619123409],
                  [3.250247294498403, 51.960365357854286],
                  [3.7513346922388284, 52.93209131750574],
                ],
              ],
            },
          },
        ],
      };
      expect(isMaximumOneDrawing(geojsonPointPolygonCoordinates)).toBeFalsy();
    });
    it('should return true when geojson for polygon has a single drawing', () => {
      const geojsonPointPolygonCoordinates: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [3.7513346922388284, 52.93209131750574],
                  [4.890169687103436, 52.26807619123409],
                  [3.250247294498403, 51.960365357854286],
                  [3.7513346922388284, 52.93209131750574],
                ],
              ],
            },
          },
        ],
      };
      expect(isMaximumOneDrawing(geojsonPointPolygonCoordinates)).toBeTruthy();
    });
  });

  describe('hasMulitpleIntersections', () => {
    it('should return true when geojson has type MultiPolygon and has more than one set of coordinates', () => {
      const geojsonMultiPolygon: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
            },
            geometry: {
              type: 'MultiPolygon',
              coordinates: [
                [
                  [
                    [5.697859456058259, 50.81606622387362],
                    [5.945196168798731, 50.99673451308248],
                    [6.011797, 50.757273],
                    [5.697859456058259, 50.81606622387362],
                  ],
                ],
                [
                  [
                    [6.28897126, 51.8259],
                    [6.500002, 55.000002],
                    [6.5, 53.666667],
                    [7.191667, 53.3],
                    [6.28897126, 51.8259],
                  ],
                ],
              ],
            },
          },
        ],
      };

      expect(hasMulitpleIntersections(geojsonMultiPolygon)).toBeTruthy();
    });

    it('should return false when geojson is not of type MultiPolygon and has only one set of coordinates', () => {
      const geojsonPolygon: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [3.7513346922388284, 52.93209131750574],
                  [4.890169687103436, 52.26807619123409],
                  [3.250247294498403, 51.960365357854286],
                  [4.890169687103436, 52.26807619123409],
                  [3.250247294498403, 51.960365357854286],
                  [3.250247294498403, 51.960365357854286],
                  [3.7513346922388284, 52.93209131750574],
                ],
              ],
            },
          },
        ],
      };

      expect(hasMulitpleIntersections(geojsonPolygon)).toBeFalsy();
    });

    it('should return false when no geometry', () => {
      const geojsonNoGeometry: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
            },
            geometry: null!,
          },
        ],
      };

      expect(hasMulitpleIntersections(geojsonNoGeometry)).toBeFalsy();
    });

    it('should return false when geometry is a point', () => {
      const geojsonPointGeometry: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              stroke: '#f24a00',
              'stroke-width': 1.5,
              'stroke-opacity': 1,
              fill: '#f24a00',
              'fill-opacity': 0.5,
            },
            geometry: {
              type: 'Point',
              coordinates: [3.631625, 52.775657],
            },
          },
        ],
      } as GeoJSON.FeatureCollection;

      expect(hasMulitpleIntersections(geojsonPointGeometry)).toBeFalsy();
    });
  });

  describe('hasIntersectionWithFIR', () => {
    it('should return true when geojson has no geometry', () => {
      const geojsonNoGeometry: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
            },
            geometry: null!,
          },
        ],
      };
      expect(
        hasIntersectionWithFIR(geojsonNoGeometry, geojsonNoGeometry),
      ).toBeTruthy();
    });

    it('should return true when geojson has empty polygon coordinates', () => {
      const geojsonNoPolygonCoordinates: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [[]],
            },
          },
        ],
      };
      expect(
        hasIntersectionWithFIR(
          geojsonNoPolygonCoordinates,
          geojsonNoPolygonCoordinates,
        ),
      ).toBeTruthy();
    });

    it('should return false when no intersection between drawing and FIR', () => {
      const geojsonPointPolygonCoordinates: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [3.7513346922388284, 52.93209131750574],
                  [4.890169687103436, 52.26807619123409],
                  [3.250247294498403, 51.960365357854286],
                  [3.7513346922388284, 52.93209131750574],
                ],
                [
                  [3.7513346922388284, 52.93209131750574],
                  [4.890169687103436, 52.26807619123409],
                  [3.250247294498403, 51.960365357854286],
                  [3.7513346922388284, 52.93209131750574],
                ],
              ],
            },
          },
        ],
      };
      const geojsonPointPolygonIntersectionCoordinates: GeoJSON.FeatureCollection =
        {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                selectionType: 'polygon',
              },
              geometry: {
                type: 'Polygon',
                coordinates: [[]],
              },
            },
          ],
        };
      expect(
        hasIntersectionWithFIR(
          geojsonPointPolygonCoordinates,
          geojsonPointPolygonIntersectionCoordinates,
        ),
      ).toBeFalsy();
    });

    it('should return true when intersection between drawing and FIR', () => {
      const geojsonPointPolygonCoordinates: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [3.7513346922388284, 52.93209131750574],
                  [4.890169687103436, 52.26807619123409],
                  [3.250247294498403, 51.960365357854286],
                  [3.7513346922388284, 52.93209131750574],
                ],
                [
                  [3.7513346922388284, 52.93209131750574],
                  [4.890169687103436, 52.26807619123409],
                  [3.250247294498403, 51.960365357854286],
                  [3.7513346922388284, 52.93209131750574],
                ],
              ],
            },
          },
        ],
      };
      const geojsonPointPolygonIntersectionCoordinates: GeoJSON.FeatureCollection =
        {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                selectionType: 'polygon',
              },
              geometry: {
                type: 'Polygon',
                coordinates: [[[2.7513346922388284, 52.93209131750574]]],
              },
            },
          ],
        };
      expect(
        hasIntersectionWithFIR(
          geojsonPointPolygonCoordinates,
          geojsonPointPolygonIntersectionCoordinates,
        ),
      ).toBeTruthy();
    });
  });

  describe('countIntersectionPoints', () => {
    it('should return the number of unique intersection points not in the original coordinates', () => {
      const geojsonPointPolygonCoordinates: GeoJSON.FeatureCollection<Polygon> =
        {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                selectionType: 'polygon',
              },
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [3.7513346922388284, 52.93209131750574],
                    [4.890169687103436, 52.26807619123409],
                    [3.250247294498403, 51.960365357854286],
                    [3.7513346922388284, 52.93209131750574],
                  ],
                  [
                    [3.7513346922388284, 52.93209131750574],
                    [4.890169687103436, 52.26807619123409],
                    [3.250247294498403, 51.960365357854286],
                    [3.7513346922388284, 52.93209131750574],
                  ],
                ],
              },
            },
          ],
        };
      const geojsonPointPolygonIntersectionCoordinates: GeoJSON.FeatureCollection<Polygon> =
        {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                selectionType: 'polygon',
              },
              geometry: {
                type: 'Polygon',
                coordinates: [[[2.7513346922388284, 52.93209131750574]]],
              },
            },
          ],
        };
      expect(
        countIntersectionPoints(
          geojsonPointPolygonCoordinates,
          geojsonPointPolygonIntersectionCoordinates,
        ),
      ).toBe(1);
    });
  });

  describe('isGeometryDirty', () => {
    it('should return false when geojsons are not complete or are equal', () => {
      const geojson: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
            },
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            geometry: {
              type: 'Polygon',
            },
          },
        ],
      };

      const geojson2: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [4.890169687103436, 52.26807619123409],
                  [3.250247294498403, 51.960365357854286],
                ],
              ],
            },
          },
        ],
      };

      expect(isGeometryDirty(null, null)).toBeFalsy();
      expect(isGeometryDirty(undefined, undefined)).toBeFalsy();
      expect(isGeometryDirty(null, undefined)).toBeFalsy();
      expect(isGeometryDirty(undefined, null)).toBeFalsy();
      expect(isGeometryDirty(geojson, null)).toBeFalsy();
      expect(isGeometryDirty(null, geojson)).toBeFalsy();
      expect(isGeometryDirty(geojson2, geojson)).toBeFalsy();
      expect(isGeometryDirty(geojson2, geojson2)).toBeFalsy();
    });

    it('should return true when geojsons are complete and have different coordinates', () => {
      const geojson: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
              testProp1: 'testProp1',
              testProp2: 'testProp2',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [4.890169687103436, 52.26807619123409],
                  [3.250247294498403, 51.960365357854286],
                ],
              ],
            },
          },
        ],
      };
      const geojson2: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'polygon',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [3.250247294498403, 51.960365357854286],
                  // eslint-disable-next-line @typescript-eslint/no-loss-of-precision
                  [4.890169687103436, 52.263807619123409],
                ],
              ],
            },
          },
        ],
      };

      expect(isGeometryDirty(geojson, geojson2)).toBeTruthy();
    });
  });

  describe('isNonOrBothCoordinates', () => {
    it('should return true when no or both coordinates are given', () => {
      expect(isNonOrBothCoordinates(null, null)).toBeTruthy();
      expect(isNonOrBothCoordinates(undefined, undefined)).toBeTruthy();
      expect(isNonOrBothCoordinates(5, 52)).toBeTruthy();
    });
    it('should return true when only the first coordinate is given', () => {
      expect(isNonOrBothCoordinates(5, null)).toBeTruthy();
      expect(isNonOrBothCoordinates(5, undefined)).toBeTruthy();
    });
    it('should return false when only the second coordinate is given', () => {
      expect(isNonOrBothCoordinates(null, 52)).toBeFalsy();
      expect(isNonOrBothCoordinates(undefined, 52)).toBeFalsy();
      expect(isNonOrBothCoordinates('' as unknown as number, 52)).toBeFalsy();
    });
  });

  describe('isInteger', () => {
    it('should return true when it is a integer value', () => {
      expect(isInteger(500)).toBeTruthy();
      expect(isInteger('' as unknown as number)).toBeTruthy();
    });

    it('should return false when it is not integer value', () => {
      expect(isInteger(100.23)).toBeFalsy();
      expect(isInteger('test' as unknown as number)).toBeFalsy();
    });
  });

  describe('isNumeric', () => {
    it('should return true when it is a numeric value', () => {
      expect(isNumeric('500')).toBeTruthy();
      expect(isNumeric('300.23')).toBeTruthy();
      expect(isNumeric('-5')).toBeTruthy();
      expect(isNumeric(500)).toBeTruthy();
      expect(isNumeric(300.23)).toBeTruthy();
      expect(isNumeric(-5)).toBeTruthy();
    });
    it('should return false when it is not a numeric value', () => {
      expect(isNumeric('52,56')).toBeFalsy();
      expect(isNumeric('a')).toBeFalsy();
      expect(isNumeric('ab')).toBeFalsy();
      expect(isNumeric('12ads')).toBeFalsy();
      expect(isNumeric(NaN)).toBeFalsy();
    });
  });
});
