/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { Button, FormControlLabel, MenuItem, Radio } from '@mui/material';
import * as React from 'react';

import { useFormContext } from 'react-hook-form';
import { dateUtils } from '@opengeoweb/shared';
import type { Meta, StoryObj } from '@storybook/react';
import {
  ReactHookFormDateTime,
  ReactHookFormRadioGroup,
  ReactHookFormSelect,
  ReactHookFormTextField,
  ReactHookFormNumberField,
  defaultFormOptions,
} from '.';
import { FormFieldsWrapper, zeplinLinks } from './Providers';

type Story = StoryObj<typeof FormFieldsWrapper>;

const meta: Meta<typeof ReactHookFormDateTime> = {
  title: 'Components/Form Provider',
  tags: ['!autodocs'],
};
export default meta;

const ReactHookFormProviderDemo = (): React.ReactElement => {
  const { handleSubmit, watch } = useFormContext();

  return (
    <form
      onSubmit={handleSubmit((formData) => {
        // eslint-disable-next-line no-console
        console.log(formData);
      })}
    >
      <ReactHookFormDateTime
        name="dateTime-A"
        rules={{
          required: true,
          validate: {
            isValidDate: dateUtils.isValidIsoDateString,
          },
        }}
      />

      <ReactHookFormTextField
        name="textdemo"
        label="Text"
        rules={{ required: true }}
      />

      <ReactHookFormNumberField
        name="numberdemo"
        label="Decimal number"
        inputMode="decimal"
        defaultValue={12.239}
        rules={{ required: true }}
      />

      <ReactHookFormSelect
        name="options"
        label="options"
        rules={{
          required: true,
        }}
      >
        <MenuItem value="">-</MenuItem>
        <MenuItem value="single" key="single">
          single
        </MenuItem>
        <MenuItem value="multiple" key="multiple">
          multiple
        </MenuItem>
      </ReactHookFormSelect>

      {watch('options') === 'multiple' && (
        <ReactHookFormTextField
          name="extraInfo"
          label="extra info"
          rules={{ required: true }}
        />
      )}

      <ReactHookFormRadioGroup
        name="radio2"
        rules={{
          required: true,
          validate: {
            eatApples: (value: string): boolean | string =>
              value === 'apples' || 'You should eat more apples',
          },
        }}
      >
        <FormControlLabel value="bananas" control={<Radio />} label="Bananas" />
        <FormControlLabel value="apples" control={<Radio />} label="Apples" />
      </ReactHookFormRadioGroup>

      <Button variant="contained" color="secondary" type="submit">
        Validate
      </Button>
    </form>
  );
};

export const FormProvider: Story = {
  render: () => (
    <FormFieldsWrapper
      options={{
        ...defaultFormOptions,
        defaultValues: {
          'dateTime-A': '2021-01-01T12:00:00Z',
        },
      }}
    >
      <ReactHookFormProviderDemo />
    </FormFieldsWrapper>
  ),
  parameters: {
    zeplinLink: zeplinLinks,
  },
};

// snapshots
const SnapShotLayout = ({ ...props }): React.ReactElement => (
  <FormFieldsWrapper
    options={{
      ...defaultFormOptions,
      defaultValues: {
        'dateTime-A': '2021-01-01T12:00:00Z',
      },
    }}
    {...props}
  >
    <ReactHookFormProviderDemo />
  </FormFieldsWrapper>
);

// light theme
export const FormProviderLightTheme: Story = {
  render: () => <SnapShotLayout />,
  tags: ['snapshot'],
  parameters: {
    zeplinLink: [zeplinLinks[0]],
  },
};

// dark theme
export const FormProviderDarkTheme: Story = {
  render: () => <SnapShotLayout />,
  tags: ['snapshot', 'dark'],
  parameters: {
    zeplinLink: [zeplinLinks[1]],
  },
};
