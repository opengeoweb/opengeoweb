/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { Typography, Button } from '@mui/material';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';
import type { Meta, StoryObj } from '@storybook/react';
import { ReactHookFormNumberField } from '.';
import { FormFieldsWrapper, zeplinLinks } from './Providers';
import { isInteger, isNumeric } from './utils';

type Story = StoryObj<typeof ReactHookFormNumberField>;

const meta: Meta<typeof ReactHookFormNumberField> = {
  title: 'Components/Number Field',
  component: ReactHookFormNumberField,
  parameters: {
    docs: {
      description: {
        component: 'ReactHookFormNumberField component',
      },
    },
  },
};
export default meta;

export const Component: Story = {
  args: {
    name: 'number-example',
    label: 'Number label',
    rules: {
      required: false,
    },
    isReadOnly: false,
    disabled: false,
    helperText: '',
    defaultValue: 123.01,
    inputMode: 'decimal',
    size: 'medium',
  },

  render: (props) => (
    <FormFieldsWrapper>
      <ReactHookFormNumberField {...props} />
    </FormFieldsWrapper>
  ),
};

const ReactHookFormNumberFieldDemoLayout = (): React.ReactElement => {
  const { handleSubmit } = useFormContext();

  return (
    <form
      onSubmit={handleSubmit((formData) => {
        // eslint-disable-next-line no-console
        console.log(formData);
      })}
    >
      <Typography>Inputmode Decimal</Typography>
      <ReactHookFormNumberField
        name="decimaldemo"
        label="Decimal with validation"
        rules={{
          required: true,
          validate: {
            isNumeric,
          },
        }}
        inputMode="decimal"
      />

      <ReactHookFormNumberField
        name="decimaldemo2"
        label="Decimal defaultValue from provider"
        rules={{}}
        inputMode="decimal"
      />

      <ReactHookFormNumberField
        name="decimaldemo3"
        label="Decimal defaultValue NaN"
        rules={{}}
        inputMode="decimal"
      />

      <ReactHookFormNumberField
        name="decimaldemo4"
        label="Decimal no defaultValue, required"
        rules={{
          required: true,
        }}
        inputMode="decimal"
      />

      <Typography>Inputmode Numeric</Typography>
      <ReactHookFormNumberField
        name="numericdemo"
        label="Numeric"
        rules={{
          required: true,
          validate: {
            isInteger,
          },
        }}
        defaultValue={12.4}
      />

      <Typography>Error</Typography>
      <ReactHookFormNumberField
        name="errordemo"
        error
        label="Error"
        defaultValue={12}
      />

      <Button variant="contained" color="secondary" type="submit">
        Validate
      </Button>
    </form>
  );
};

export const NumberField: Story = {
  render: () => (
    <FormFieldsWrapper
      options={{
        defaultValues: {
          decimaldemo: null,
          decimaldemo2: -13.45,
          decimaldemo3: NaN,
        },
      }}
    >
      <ReactHookFormNumberFieldDemoLayout />
    </FormFieldsWrapper>
  ),
  parameters: {
    zeplinLink: zeplinLinks,
    docs: {
      description: {
        story: 'Press the submit button to test out validation',
      },
    },
  },
};

// snapshots
const NumberFieldSnapshotLayout = (): React.ReactElement => (
  <>
    <ReactHookFormNumberField
      name="emptydemo"
      label="Empty field"
      rules={{
        required: true,
        validate: {
          isNumeric,
        },
      }}
      inputMode="decimal"
    />

    <ReactHookFormNumberField
      name="disableddemo"
      label="Disabled field"
      rules={{}}
      disabled
      inputMode="decimal"
      defaultValue={12}
    />

    <ReactHookFormNumberField
      name="readonlyDemo"
      label="Disabled readOnly field"
      rules={{}}
      disabled
      isReadOnly
      inputMode="decimal"
      defaultValue={123}
    />

    <ReactHookFormNumberField
      name="decimaldemo"
      label="Decimal field"
      rules={{}}
      inputMode="decimal"
      defaultValue={-13.45}
    />

    <ReactHookFormNumberField
      name="numericdemo"
      label="Numeric field"
      rules={{
        required: true,
        validate: {
          isInteger,
        },
      }}
      defaultValue={12}
    />

    <ReactHookFormNumberField
      name="errordemo"
      error
      label="Error"
      defaultValue={12}
    />
  </>
);

// light theme
export const NumberFieldLightTheme: Story = {
  render: () => (
    <FormFieldsWrapper>
      <NumberFieldSnapshotLayout />
    </FormFieldsWrapper>
  ),
  tags: ['snapshot'],
  parameters: {
    zeplinLink: [zeplinLinks[0]],
  },
};

// dark theme
export const NumberFieldDarkTheme: Story = {
  render: () => (
    <FormFieldsWrapper>
      <NumberFieldSnapshotLayout />
    </FormFieldsWrapper>
  ),
  tags: ['snapshot', 'dark'],
  parameters: {
    zeplinLink: [zeplinLinks[1]],
  },
};
