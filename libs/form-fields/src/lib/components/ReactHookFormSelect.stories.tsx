/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { Button, MenuItem } from '@mui/material';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';

import type { Meta, StoryObj } from '@storybook/react';
import { ReactHookFormSelect } from '.';
import { FormFieldsWrapper, zeplinLinks } from './Providers';

type Story = StoryObj<typeof ReactHookFormSelect>;

const meta: Meta<typeof ReactHookFormSelect> = {
  title: 'Components/Select',
  component: ReactHookFormSelect,
  parameters: {
    docs: {
      description: {
        component: 'ReactHookFormSelect component',
      },
    },
  },
};
export default meta;

export const Component: Story = {
  args: {
    children: [
      <MenuItem key="empty" value="">
        -
      </MenuItem>,
      <MenuItem value="option-1" key="option-1">
        Option 1
      </MenuItem>,
      <MenuItem value="option-2" key="option-2">
        Option 2
      </MenuItem>,
      <MenuItem value="option-3" key="option-3">
        Option 3
      </MenuItem>,
    ],
    name: 'select1',
    label: 'Select an option',
    rules: {
      required: true,
    },
    isReadOnly: false,
    disabled: false,
    defaultValue: '',
    size: 'medium',
  },
  argTypes: {
    helperText: { table: { disable: true } }, // hide helperText from controls
  },
  render: (props) => (
    <FormFieldsWrapper>
      <ReactHookFormSelect {...props} />
    </FormFieldsWrapper>
  ),
};

const SelectDemoLayout: React.FC<{ children?: React.ReactNode }> = ({
  children,
}) => (
  <>
    <ReactHookFormSelect
      name="select1"
      label="Unit"
      rules={{
        required: true,
      }}
    >
      <MenuItem value="">-</MenuItem>
      <MenuItem value="FL" key="FL">
        FL
      </MenuItem>
      <MenuItem value="M" key="M">
        M
      </MenuItem>
      <MenuItem value="FT" key="FT">
        FT
      </MenuItem>
    </ReactHookFormSelect>

    <ReactHookFormSelect
      name="select2"
      label="Unit"
      defaultValue="FT"
      rules={{
        required: true,
      }}
    >
      <MenuItem value="FL" key="FL">
        FL
      </MenuItem>
      <MenuItem value="M" key="M">
        M
      </MenuItem>
      <MenuItem value="FT" key="FT">
        FT
      </MenuItem>
    </ReactHookFormSelect>

    <ReactHookFormSelect
      name="select3"
      label="Unit"
      defaultValue="FT"
      disabled
      rules={{
        required: true,
      }}
    >
      <MenuItem value="FL" key="FL">
        FL
      </MenuItem>
      <MenuItem value="M" key="M">
        M
      </MenuItem>
      <MenuItem value="FT" key="FT">
        FT
      </MenuItem>
    </ReactHookFormSelect>

    <ReactHookFormSelect
      name="readonly"
      label="Readonly"
      defaultValue="M"
      disabled
      isReadOnly
      rules={{
        required: true,
      }}
    >
      <MenuItem value="FL" key="FL">
        FL
      </MenuItem>
      <MenuItem value="M" key="M">
        M
      </MenuItem>
      <MenuItem value="FT" key="FT">
        FT
      </MenuItem>
    </ReactHookFormSelect>

    <ReactHookFormSelect
      name="error"
      label="Error"
      defaultValue="M"
      error
      rules={{
        required: true,
      }}
    >
      <MenuItem value="FL" key="FL">
        FL
      </MenuItem>
      <MenuItem value="M" key="M">
        M
      </MenuItem>
      <MenuItem value="FT" key="FT">
        FT
      </MenuItem>
    </ReactHookFormSelect>
    {children}
  </>
);

const ReactHookFormSelectDemo = (): React.ReactElement => {
  const { handleSubmit } = useFormContext();

  return (
    <SelectDemoLayout>
      <Button
        variant="contained"
        color="secondary"
        onClick={handleSubmit(() => {})}
      >
        Validate
      </Button>
    </SelectDemoLayout>
  );
};

export const Select: Story = {
  render: () => (
    <FormFieldsWrapper>
      <ReactHookFormSelectDemo />
    </FormFieldsWrapper>
  ),
  parameters: {
    docs: {
      description: {
        story: 'Press the submit button to test out validation',
      },
    },
  },
};

// light theme
export const SelectLightTheme: Story = {
  render: () => (
    <FormFieldsWrapper>
      <SelectDemoLayout />
    </FormFieldsWrapper>
  ),
  tags: ['snapshot'],
  parameters: {
    zeplinLink: [zeplinLinks[0]],
  },
};

// dark theme
export const SelectDarkTheme: Story = {
  render: () => (
    <FormFieldsWrapper>
      <SelectDemoLayout />
    </FormFieldsWrapper>
  ),
  tags: ['snapshot', 'dark'],
  parameters: {
    zeplinLink: [zeplinLinks[1]],
  },
};
