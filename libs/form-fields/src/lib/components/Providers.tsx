/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Box, GlobalStyles } from '@mui/material';
import { darkTheme, lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import React from 'react';
import { UseFormProps } from 'react-hook-form';
import { I18nextProvider } from 'react-i18next';
import ReactHookFormProviderWrapper from './ReactHookFormProvider';
import { i18n, initFormFieldsI18n } from '../utils/i18n';

export const zeplinLinks = [
  {
    name: 'Light theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf92695e71d82acd448ea2/version/63369f756eabe599b4d02942',
  },

  {
    name: 'Dark theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e68f80d83109782953ac/version/63369f77ef66c34e37d38303',
  },
];

interface StoryLayoutProps {
  children?: React.ReactNode;
}

export const FormFieldsWrapper: React.FC<
  StoryLayoutProps & {
    options?: UseFormProps;
  }
> = ({ children, options }) => (
  <ReactHookFormProviderWrapper options={options}>
    {children}
  </ReactHookFormProviderWrapper>
);

export const FormFieldsWrappeDecorator: React.FC<
  StoryLayoutProps & {
    isDarkTheme?: boolean;
  }
> = ({ children, isDarkTheme = false }) => {
  const backgroundColor = `${
    isDarkTheme
      ? darkTheme.palette.geowebColors.background.surface
      : lightTheme.palette.geowebColors.background.surface
  }!important`;

  return (
    <FormFieldsI18nProvider>
      <ThemeWrapper theme={isDarkTheme ? darkTheme : lightTheme}>
        <GlobalStyles
          styles={{
            body: {
              backgroundColor,
            },
          }}
        />

        <Box
          sx={{
            padding: 2,
            display: 'inline-block',
            backgroundColor,
          }}
        >
          <Box
            sx={{
              width: 300,
              '> .MuiFormControl-root, form > .MuiFormControl-root, > .MuiPaper-root > .MuiFormControl-root':
                {
                  marginBottom: 2,
                },
            }}
          >
            {children}
          </Box>
        </Box>
      </ThemeWrapper>
    </FormFieldsI18nProvider>
  );
};
export const FormFieldsI18nProvider: React.FC<{
  children?: React.ReactNode;
}> = ({ children }) => {
  initFormFieldsI18n();
  return <I18nextProvider i18n={i18n}>{children}</I18nextProvider>;
};
