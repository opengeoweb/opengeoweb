/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Button, FormControlLabel, Radio } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import ReactHookFormRadioGroup from './ReactHookFormRadioGroup';
import ReactHookFormProvider from './ReactHookFormProvider';
import { FormFieldsI18nProvider } from './Providers';
import { initFormFieldsI18n } from '../utils/i18n';

beforeAll(() => {
  initFormFieldsI18n();
});

describe('ReactHookFormRadioGroup', () => {
  const user = userEvent.setup();
  it('should render successfully', () => {
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormRadioGroup name="test" rules={{ required: true }}>
            <FormControlLabel
              value="test1"
              control={<Radio />}
              label="Test 1"
            />
          </ReactHookFormRadioGroup>
        </ReactHookFormProvider>
      );
    };
    const { baseElement } = render(<Wrapper />);
    expect(baseElement).toBeTruthy();
  });

  it('should select a radiobutton', async () => {
    const testOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormRadioGroup
            name="test"
            rules={{ required: true }}
            onChange={testOnChange}
          >
            <FormControlLabel
              value="test1"
              control={<Radio />}
              label="Test 1"
            />
            <FormControlLabel
              value="test2"
              control={<Radio />}
              label="Test 2"
            />
          </ReactHookFormRadioGroup>
        </ReactHookFormProvider>
      );
    };

    render(<Wrapper />);
    const radioTest2 = screen.queryByRole('radio', {
      name: /Test 2/i,
    }) as HTMLInputElement;
    fireEvent.click(radioTest2);

    await waitFor(() => {
      expect(testOnChange).toHaveBeenCalledTimes(1);
    });
    expect(
      (screen.queryByRole('radio', { name: /Test 1/i }) as HTMLInputElement)
        .checked,
    ).toBeFalsy();
    expect(
      (screen.getByRole('radio', { name: /Test 2/i }) as HTMLInputElement)
        .checked,
    ).toBeTruthy();
  });

  it('should show the error message on error', async () => {
    render(
      <ReactHookFormProvider
        options={{
          mode: 'onChange',
          reValidateMode: 'onChange',
          defaultValues: {
            radio: 'apples',
          },
        }}
      >
        <ReactHookFormRadioGroup
          name="radio"
          rules={{
            required: true,
            validate: {
              eatApples: (value: string): boolean | string =>
                value === 'apples' || 'You should eat more apples',
            },
          }}
        >
          <FormControlLabel value="apples" control={<Radio />} label="Apples" />
          <FormControlLabel
            value="bananas"
            control={<Radio />}
            label="Bananas"
          />
        </ReactHookFormRadioGroup>
      </ReactHookFormProvider>,
    );

    const bananas = screen.queryByRole('radio', {
      name: /Bananas/i,
    }) as HTMLInputElement;

    expect(
      (screen.getByRole('radio', { name: /Apples/i }) as HTMLInputElement)
        .checked,
    ).toBeTruthy();
    expect(bananas.checked).toBeFalsy();

    fireEvent.click(bananas);

    await waitFor(() => {
      expect(
        (screen.queryByRole('radio', { name: /Apples/i }) as HTMLInputElement)
          .checked,
      ).toBeFalsy();
    });
    expect(
      (screen.getByRole('radio', { name: /Bananas/i }) as HTMLInputElement)
        .checked,
    ).toBeTruthy();
    expect(screen.getByText('You should eat more apples')).toBeTruthy();
    expect(screen.getByText('You should eat more apples').classList).toContain(
      'Mui-error',
    );
  });

  it('should focus the first radiobutton on TAB', async () => {
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormRadioGroup name="test" rules={{ required: true }}>
            <FormControlLabel
              value="apples"
              control={<Radio />}
              label="Apples"
            />
            <FormControlLabel
              value="bananas"
              control={<Radio />}
              label="Bananas"
            />
          </ReactHookFormRadioGroup>
        </ReactHookFormProvider>
      );
    };
    render(<Wrapper />);

    const apples = screen.queryByRole('radio', {
      name: /Apples/i,
    }) as HTMLInputElement;

    expect(apples.matches(':focus')).toBeFalsy();

    const bananas = screen.queryByRole('radio', {
      name: /Bananas/i,
    }) as HTMLInputElement;

    expect(bananas.matches(':focus')).toBeFalsy();

    await user.tab();

    expect(apples.matches(':focus')).toBeTruthy();

    expect(bananas.matches(':focus')).toBeFalsy();
    await user.tab();

    expect(apples.matches(':focus')).toBeFalsy();
    expect(bananas.matches(':focus')).toBeFalsy();
  });

  it('should focus the first radiobutton on error', async () => {
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();

      return (
        <form onSubmit={handleSubmit(() => {})}>
          <ReactHookFormRadioGroup name="test" rules={{ required: true }}>
            <FormControlLabel
              value="apples"
              control={<Radio />}
              label="Apples"
            />
            <FormControlLabel
              value="bananas"
              control={<Radio />}
              label="Bananas"
            />
          </ReactHookFormRadioGroup>
          <Button type="submit">Validate</Button>
        </form>
      );
    };
    render(
      <FormFieldsI18nProvider>
        <ReactHookFormProvider>
          <Wrapper />
        </ReactHookFormProvider>
      </FormFieldsI18nProvider>,
    );
    const apples = screen.queryByRole('radio', {
      name: /Apples/i,
    }) as HTMLInputElement;

    expect(apples.matches(':focus')).toBeFalsy();

    const bananas = screen.queryByRole('radio', {
      name: /Bananas/i,
    }) as HTMLInputElement;

    expect(bananas.matches(':focus')).toBeFalsy();

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(apples.matches(':focus')).toBeTruthy();
    });
    expect(bananas.matches(':focus')).toBeFalsy();

    expect(screen.getByText('This field is required')).toBeTruthy();
  });

  it('should handle prop isReadOnly', () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          mode: 'onChange',
          reValidateMode: 'onChange',
          defaultValues: {
            radio: 'apples',
          },
        }}
      >
        <ReactHookFormRadioGroup
          name="test"
          rules={{ required: true }}
          isReadOnly
        >
          <FormControlLabel value="apples" control={<Radio />} label="Apples" />
          <FormControlLabel
            value="bananas"
            control={<Radio />}
            label="Bananas"
          />
        </ReactHookFormRadioGroup>
      </ReactHookFormProvider>,
    );
    expect((container.firstChild! as HTMLElement).classList).toContain(
      'is-read-only',
    );
  });
});
