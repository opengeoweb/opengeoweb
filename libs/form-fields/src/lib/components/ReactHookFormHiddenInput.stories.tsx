/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { Button, Typography } from '@mui/material';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';
import type { Meta, StoryObj } from '@storybook/react';
import { ReactHookFormHiddenInput } from '.';
import { FormFieldsWrapper } from './Providers';

type Story = StoryObj<typeof ReactHookFormHiddenInput>;

const meta: Meta<typeof ReactHookFormHiddenInput> = {
  title: 'Components/Hidden Input',
  component: ReactHookFormHiddenInput,
  parameters: {
    docs: {
      description: {
        component: 'ReactHookFormHiddenInput component',
      },
    },
  },
  tags: ['!autodocs'],
};
export default meta;

const ReactHookFormHiddenInputLayout = (): React.ReactElement => {
  const { handleSubmit } = useFormContext();
  const [value, setValue] = React.useState('');

  return (
    <form
      onSubmit={handleSubmit((data) => {
        setValue(data['hide']);
      })}
    >
      <Typography>
        Click submit to see the hidden input value that would get submitted
      </Typography>
      <ReactHookFormHiddenInput name="hide" defaultValue="hidden value" />
      <Button
        type="submit"
        style={{ marginBottom: '20px' }}
        variant="contained"
        color="secondary"
      >
        Submit
      </Button>
      {value && <Typography>{value}</Typography>}
    </form>
  );
};

export const HiddenInput: Story = {
  render: () => (
    <FormFieldsWrapper>
      <ReactHookFormHiddenInputLayout />
    </FormFieldsWrapper>
  ),
};

export const HiddenInputDarkTheme: Story = {
  render: () => (
    <FormFieldsWrapper>
      <ReactHookFormHiddenInputLayout />
    </FormFieldsWrapper>
  ),
  tags: ['dark'],
};
