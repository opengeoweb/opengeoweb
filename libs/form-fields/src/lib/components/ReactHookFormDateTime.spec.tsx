/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { Button } from '@mui/material';
import { ThemeWrapper } from '@opengeoweb/theme';
import { dateUtils } from '@opengeoweb/shared';
import { useFormContext } from 'react-hook-form';

import ReactHookFormDateTime, {
  getDateValue,
  getFormattedValueForDatePicker,
} from './ReactHookFormDateTime';
import ReactHookFormProvider from './ReactHookFormProvider';
import { FormFieldsI18nProvider } from './Providers';
import { initFormFieldsI18n } from '../utils/i18n';

beforeAll(() => {
  initFormFieldsI18n();
});

describe('components/ReactHookFormDateTime', () => {
  describe('ReactHookFormDateTime component', () => {
    it('should render successfully', () => {
      const { baseElement } = render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>,
      );
      expect(baseElement).toBeTruthy();
    });

    it('should show UTC in the field', () => {
      render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>,
      );
      expect(screen.getByText('UTC')).toBeTruthy();
    });

    it('should be possible to set a date by typing in the field', async () => {
      jest.spyOn(console, 'warn').mockImplementationOnce(() => {});
      const mockOnChange = jest.fn();
      const newValue = '2021/02/18 12:37:00';
      const newValueAsDate = dateUtils.stringToDate(
        newValue,
        'yyyy/MM/dd HH:mm:ss',
        true,
      );
      render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
            onChange={mockOnChange}
            format="yyyy/MM/dd HH:mm:ss"
          />
        </ReactHookFormProvider>,
      );
      const dateInput = screen.getByRole('textbox');
      fireEvent.change(dateInput, {
        target: { value: newValue },
      });
      await waitFor(() =>
        expect(mockOnChange).toHaveBeenLastCalledWith(
          `${newValueAsDate?.toISOString().substring(0, 19)}Z`,
        ),
      );
    });

    it('should open the date and time pickers and should be possible to choose a date', async () => {
      const mockOnChange = jest.fn();
      render(
        <ThemeWrapper>
          <ReactHookFormProvider>
            <ReactHookFormDateTime
              name="testDateTime"
              rules={{ required: true }}
              onChange={mockOnChange}
            />
          </ReactHookFormProvider>
        </ThemeWrapper>,
      );
      const datePicker = screen.getByRole('button');
      fireEvent.click(datePicker);

      // Dates should be visible
      expect(screen.getByText('F')).toBeTruthy(); // F from Friday
      // Choose first day of the month
      fireEvent.click(screen.getByText('1'));

      // hours should be visible
      expect(screen.getByLabelText('1 hours')).toBeTruthy();
      // minutes should be visible
      expect(screen.getByLabelText('54 minutes')).toBeTruthy();

      // Close the datepicker
      fireEvent.click(datePicker);
      await waitFor(() => expect(mockOnChange).toHaveBeenCalled());
    });

    it('should be disabled', () => {
      render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
            disabled
          />
        </ReactHookFormProvider>,
      );
      const datePicker = screen.getByRole('button');

      expect(datePicker.classList).toContain('Mui-disabled');
    });

    it('should handle prop isReadOnly', () => {
      render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
            disabled
            isReadOnly
          />
        </ReactHookFormProvider>,
      );
      expect(
        screen.getByRole('textbox').classList.contains('MuiInputBase-readOnly'),
      ).toBeTruthy();
    });

    it('should show default label and value', () => {
      render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>,
      );
      expect(screen.getAllByText('Select date and time')).toBeTruthy();
      const dateInput = screen.getByRole('textbox');
      expect(dateInput.getAttribute('value')).toEqual('');
    });

    it('should show given label and value', () => {
      render(
        <ReactHookFormProvider
          options={{
            defaultValues: {
              testDateTime: '2021-02-01T12:00:00Z',
            },
          }}
        >
          <ReactHookFormDateTime
            name="testDateTime"
            label="Test label"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>,
      );
      expect(screen.getAllByText('Test label')).toBeTruthy();
      const dateInput = screen.getByRole('textbox');
      expect(dateInput.getAttribute('value')!).toEqual('01/02/2021 12:00');
    });

    it('should show error state on error', async () => {
      render(
        <FormFieldsI18nProvider>
          <ReactHookFormProvider
            options={{
              mode: 'onChange',
              reValidateMode: 'onChange',
              defaultValues: {
                testDateTime: '2021-01-01T12:00:00Z',
              },
            }}
          >
            <ReactHookFormDateTime
              name="testDateTime"
              rules={{ required: true }}
            />
          </ReactHookFormProvider>
        </FormFieldsI18nProvider>,
      );
      const dateInput = screen.getByRole('textbox');
      fireEvent.input(dateInput, {
        target: { value: '' },
      });
      await waitFor(() => {
        expect(dateInput.getAttribute('value')).toEqual('');
      });
      expect(screen.getByText('This field is required')).toBeTruthy();
      expect(screen.getByRole('alert').classList).toContain('Mui-error');
    });

    it('should set focus when error on submit', async () => {
      const Wrapper = (): React.ReactElement => {
        const { handleSubmit } = useFormContext();
        return (
          <form onSubmit={handleSubmit(() => {})}>
            <ReactHookFormDateTime
              name="testDateTime"
              rules={{ required: true }}
            />
            <Button type="submit">Validate</Button>
          </form>
        );
      };

      render(
        <FormFieldsI18nProvider>
          <ReactHookFormProvider>
            <Wrapper />
          </ReactHookFormProvider>
        </FormFieldsI18nProvider>,
      );

      fireEvent.click(screen.getByText('Validate'));

      await waitFor(() => {
        expect(screen.getByText('This field is required')).toBeTruthy();
      });
      expect(screen.getByRole('alert').classList).toContain('Mui-error');
      expect(screen.getByRole('textbox').matches(':focus')).toBeTruthy();
    });
  });

  describe('getFormattedValueForDatePicker', () => {
    afterEach(() => {
      jest.clearAllMocks();
      jest.restoreAllMocks();
      jest.clearAllTimers();
      jest.useRealTimers();
    });
    it('should return formatted date when valid date', () => {
      jest.useFakeTimers().setSystemTime(new Date(`2023-01-01T14:12:34Z`));
      const test = new Date();
      expect(test.getUTCSeconds()).toBe(34);
      expect(typeof getFormattedValueForDatePicker(test)).toBe('string');
      expect(getFormattedValueForDatePicker(test)).toBe('2023-01-01T14:12:00Z');
      expect(test.getUTCSeconds()).toBe(34);
    });
    it('should return null when invalid date', () => {
      const test = new Date('invalid');
      expect(getFormattedValueForDatePicker(test)).toBeNull();
    });
  });

  describe('getDateValue', () => {
    it('should handle date string', () => {
      const test = '2024/05/07 12:00';
      expect(getDateValue(test)).toBeInstanceOf(Date);
    });
    it('should handle iso string', () => {
      const test = '2024-05-07T12:00:00Z';
      expect(getDateValue(test)).toBeInstanceOf(Date);
    });
    it('should return null when invalid date', () => {
      expect(getDateValue('invalid')).toBeNull();
      const test = '2024/05/07T12:00:00Z';
      expect(getDateValue(test)).toBeNull();
    });
    it('should set seconds to zero', () => {
      expect(getDateValue('2014-02-11T11:30:30')!.getSeconds()).toEqual(0);
    });
  });
});
