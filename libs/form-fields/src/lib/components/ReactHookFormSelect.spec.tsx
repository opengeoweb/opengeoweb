/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';

import { Button, MenuItem } from '@mui/material';
import { FieldValues, useFormContext } from 'react-hook-form';
import ReactHookFormSelect from './ReactHookFormSelect';
import ReactHookFormProvider from './ReactHookFormProvider';
import { FormFieldsI18nProvider } from './Providers';
import { initFormFieldsI18n } from '../utils/i18n';

beforeAll(() => {
  initFormFieldsI18n();
});

describe('ReactHookFormSelect', () => {
  const Wrapper = (props: { disabled?: boolean }): React.ReactElement => {
    return (
      <ReactHookFormProvider>
        <ReactHookFormSelect name="unit" rules={{ required: true }} {...props}>
          <MenuItem value="FL">FL</MenuItem>
          <MenuItem value="M">M</MenuItem>
        </ReactHookFormSelect>
      </ReactHookFormProvider>
    );
  };

  it('should render successfully', () => {
    const { baseElement } = render(<Wrapper />);
    expect(baseElement).toBeTruthy();
  });

  it('should be disabled', () => {
    render(<Wrapper disabled />);
    expect(screen.getByRole('combobox')!.classList).toContain('Mui-disabled');
  });

  it('should show default value empty', () => {
    render(<Wrapper />);
    const selectInput = screen.getByRole('textbox', {
      hidden: true,
    });
    expect(selectInput.getAttribute('value')).toEqual('');
  });

  it('should show given label and value', () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            unit: 'FL',
          },
        }}
      >
        <ReactHookFormSelect
          name="unit"
          label="Select unit"
          rules={{ required: true }}
        >
          <MenuItem value="FL">FL</MenuItem>
          <MenuItem value="M">M</MenuItem>
        </ReactHookFormSelect>
      </ReactHookFormProvider>,
    );
    expect(screen.getByText('Select unit')).toBeTruthy();
    const selectInput = screen.getByRole('textbox', {
      hidden: true,
    });
    expect(selectInput.getAttribute('value')).toEqual('FL');
  });

  it('should select an option', async () => {
    render(<Wrapper />);
    const selectInput = screen.getByRole('textbox', {
      hidden: true,
    });
    expect(selectInput.getAttribute('value')).toEqual('');

    fireEvent.mouseDown(screen.getByRole('combobox'));
    const menuItem = await screen.findByText('M');
    fireEvent.click(menuItem);

    await waitFor(() => {
      expect(selectInput.getAttribute('value')).toEqual('M');
    });
    expect(screen.getByText('M')).toBeTruthy();
  });

  it('should show error state and set focus when error on submit', async () => {
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <FormFieldsI18nProvider>
          <form onSubmit={handleSubmit(() => {})}>
            <ReactHookFormSelect name="unit" rules={{ required: true }}>
              <MenuItem value="FL">FL</MenuItem>
              <MenuItem value="M">M</MenuItem>
            </ReactHookFormSelect>
            <Button type="submit">Validate</Button>
          </form>
        </FormFieldsI18nProvider>
      );
    };

    render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(screen.getByText('This field is required')).toBeTruthy();
    });
    expect(screen.getByRole('alert').classList).toContain('Mui-error');
    expect(screen.getByRole('alert').classList).toContain('Mui-focused');
  });

  it('should handdle prop isReadOnly', () => {
    const { container } = render(
      <ReactHookFormProvider>
        <ReactHookFormSelect isReadOnly name="unit" rules={{ required: true }}>
          <MenuItem value="FL">FL</MenuItem>
          <MenuItem value="M">M</MenuItem>
        </ReactHookFormSelect>
      </ReactHookFormProvider>,
    );
    expect((container.firstChild! as HTMLElement).classList).toContain(
      'is-read-only',
    );
  });

  it('should be able to pass null as value', async () => {
    const testFormValues = { unit: null };
    // eslint-disable-next-line no-unused-vars
    let result: FieldValues;
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <form
          onSubmit={handleSubmit((values) => {
            result = values;
          })}
        >
          <ReactHookFormSelect name="unit" rules={{}}>
            <MenuItem value="FL">FL</MenuItem>
            <MenuItem value="M">M</MenuItem>
          </ReactHookFormSelect>
          <Button type="submit">Validate</Button>
        </form>
      );
    };

    render(
      <ReactHookFormProvider options={{ defaultValues: testFormValues }}>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));

    const selectInput = screen.getByRole('textbox', {
      hidden: true,
    });

    await waitFor(() => {
      expect((selectInput as HTMLInputElement).value).toEqual('');
    });
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(result).toEqual(testFormValues);
  });
});
