![current version](https://img.shields.io/badge/dynamic/json?color=blue&label=version&query=version&url=https://gitlab.com/opengeoweb/opengeoweb/raw/master/libs/workspace/package.json)
![coverage](https://gitlab.com/opengeoweb/opengeoweb/badges/master/coverage.svg?job=test-workspace)

# Workspace

React component library with Workspace components for the opengeoweb project. This library was generated with [Nx](https://nx.dev).

## Installation

```
npm install @opengeoweb/workspace
```

## Use

You can use any component exported from workspace by importing them, for example:

```
import {
  WorkspaceViewConnect, } from '@opengeoweb/workspace';
```

## Running unit tests

Run `nx test workspace` to execute the unit tests via [Jest](https://jestjs.io).

### TypeScript Documentation

- [TypeScript Docs](https://opengeoweb.gitlab.io/opengeoweb/typescript-docs/workspace/)
