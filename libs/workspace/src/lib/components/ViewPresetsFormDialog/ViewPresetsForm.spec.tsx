/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import {
  defaultFormOptions,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';

import ViewPresetsForm, { ViewPresetsFormProps } from './ViewPresetsForm';
import { PresetAction } from '../../store/viewPresets/types';
import { emptyViewPreset } from '../../store/viewPresets/utils';
import { DemoWrapper } from '../Providers';
import { i18n, translateKeyOutsideComponents } from '../../utils/i18n';

describe('components/ViewPresetsDialog/ViewPresetsForm', () => {
  it('should work with default props', () => {
    const props: ViewPresetsFormProps = {
      viewComponentType: 'Map',
      formValues: {
        ...emptyViewPreset('Map'),
        title: 'test title',
      },
      action: PresetAction.SAVE_AS,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <ViewPresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    const input = screen.getByRole('textbox', {
      name: translateKeyOutsideComponents(
        i18n.t,
        'workspace-mappreset-dialog-title',
      ),
    });

    expect(
      screen.getByText(
        translateKeyOutsideComponents(i18n.t, 'workspace-dialog-abstract'),
      ),
    ).toBeTruthy();

    expect(input.matches(':focus')).toBeTruthy();
    expect(input!.getAttribute('value')).toEqual(props.formValues.title);
    expect(screen.getByTestId('initialProps').getAttribute('value')).toEqual(
      '[object Object]',
    );
    expect(screen.getByTestId('scope').getAttribute('value')).toEqual('user');
    expect(screen.getByTestId('componentType').getAttribute('value')).toEqual(
      'Map',
    );
    expect(screen.getByTestId('keywords').getAttribute('value')).toEqual('');
  });

  it('should validate required', async () => {
    const props: ViewPresetsFormProps = {
      viewComponentType: 'Map',
      formValues: {
        title: 'test title',
      },
      action: PresetAction.SAVE_AS,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <ViewPresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    const input = screen.getByRole('textbox', {
      name: translateKeyOutsideComponents(
        i18n.t,
        'workspace-mappreset-dialog-title',
      ),
    });

    expect(input.matches(':focus')).toBeTruthy();
    expect(input!.getAttribute('value')).toEqual(props.formValues.title);
    expect(screen.queryByRole('alert')).toBeFalsy();

    fireEvent.change(input!, { target: { value: '' } });
    await waitFor(() => {
      expect(screen.getByRole('alert')).toBeTruthy();
    });

    fireEvent.change(input!, { target: { value: 'new value' } });
    await waitFor(() => {
      expect(screen.queryByRole('alert')).toBeFalsy();
    });
  });

  it('should show error when title only contains spaces', async () => {
    const props: ViewPresetsFormProps = {
      viewComponentType: 'Map',
      formValues: {
        title: '',
      },
      action: PresetAction.SAVE_AS,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <ViewPresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    const input = screen.getByRole('textbox', {
      name: translateKeyOutsideComponents(
        i18n.t,
        'workspace-mappreset-dialog-title',
      ),
    });

    fireEvent.change(input!, { target: { value: '  ' } });
    await waitFor(() => {
      expect(screen.getByRole('alert')).toBeTruthy();
    });
  });

  it('should show error', async () => {
    const errorMessage = 'test could not save preset';
    const props: ViewPresetsFormProps = {
      viewComponentType: 'Map',
      formValues: {
        title: 'test title',
      },
      error: errorMessage,
      action: PresetAction.SAVE_AS,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <ViewPresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    expect(await screen.findByText(errorMessage)).toBeTruthy();
  });

  it('should submit the form when submitting the title field', async () => {
    const props: ViewPresetsFormProps = {
      viewComponentType: 'Map',
      formValues: {
        title: 'test submit on enter',
      },
      onSubmit: jest.fn(),
      action: PresetAction.SAVE_AS,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <ViewPresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    const input = screen.getByRole('textbox', {
      name: translateKeyOutsideComponents(
        i18n.t,
        'workspace-mappreset-dialog-title',
      ),
    });

    expect(input).toBeTruthy();
    fireEvent.submit(input!);
    await waitFor(() => {
      expect(props.onSubmit).toHaveBeenCalled();
    });
  });

  it('should show form for edit action', async () => {
    const props: ViewPresetsFormProps = {
      viewComponentType: 'Map',
      formValues: {
        title: 'test submit on enter',
      },
      onSubmit: jest.fn(),
      action: PresetAction.EDIT,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <ViewPresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    const input = screen.getByRole('textbox', {
      name: translateKeyOutsideComponents(
        i18n.t,
        'workspace-mappreset-dialog-title',
      ),
    });

    expect(input).toBeTruthy();
  });
});
