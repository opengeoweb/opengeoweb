/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  screen,
  act,
} from '@testing-library/react';
import { layerTypes } from '@opengeoweb/store';
import { LayerType } from '@opengeoweb/webmap';
import { viewPresetActions } from '../../store/viewPresets';
import { PresetAction, ViewPreset } from '../../store/viewPresets/types';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WorkspaceWrapperProviderWithStore } from '../Providers';
import ViewPresetsFormDialogConnect from './ViewPresetsFormDialogConnect';
import { emptyViewPreset } from '../../store/viewPresets/utils';
import { PresetsApi } from '../../utils/api';
import { ViewPresetListItem } from '../../store/viewPresetsList/types';
import { i18n, translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store/store';

describe('src/components/ViewPresetsDialog/ViewPresetsFormDialogConnect', () => {
  it('should render if dialog in store', async () => {
    const viewPresetDialog = {
      title: translateKeyOutsideComponents(
        i18n.t,
        'workspace-mappreset-save-as',
      ),
      action: PresetAction.SAVE_AS,
      viewPresetId: 'screen1',
      panelId: 'screen1',
      formValues: {
        title: 'Radar',
        initialProps: {
          mapPreset: {
            layers: [
              {
                name: 'WorldMap_Light_Grey_Canvas',
                type: 'twms',
                dimensions: [],
                id: 'layerid_100',
                opacity: 1,
                enabled: true,
                layerType: LayerType.baseLayer,
              },
              {
                service: 'https://geoservices.knmi.nl/wms?DATASET=baselayers&',
                name: 'countryborders',
                format: 'image/png',
                dimensions: [],
                id: 'layerid_109',
                opacity: 1,
                enabled: true,
                layerType: LayerType.overLayer,
              },
            ],
            activeLayerId: 'layerid_107',
            autoTimeStepLayerId: 'layerid_107',
            autoUpdateLayerId: 'layerid_107',
            proj: {
              bbox: {
                left: -450651.2255879827,
                bottom: 6490531.093143953,
                right: 1428345.8183648037,
                top: 7438773.776232235,
              },
              srs: 'EPSG:3857',
            },
            shouldAnimate: false,
            shouldAutoUpdate: false,
            showTimeSlider: true,
            displayMapPin: false,
            shouldShowZoomControls: true,
            animationPayload: {
              interval: 5,
              speed: 4,
            },
            toggleTimestepAuto: true,
            shouldShowLegend: false,
          },
          syncGroupsIds: [],
        },
      },
    };

    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetsFormDialogConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.openViewPresetDialog({ viewPresetDialog }),
      ),
    );

    await waitFor(() =>
      expect(store.getState().viewPresets.viewPresetDialog).toEqual(
        viewPresetDialog,
      ),
    );

    await screen.findByTestId('map-preset-dialog');
  });
  it('should not render dialog if not dialog in store', () => {
    render(
      <WorkspaceWrapperProviderWithStore>
        <ViewPresetsFormDialogConnect />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.queryByTestId('map-preset-dialog')).toBeFalsy();
  });

  it('should save an existing preset as a new preset when choosing Save As', async () => {
    const mapId = 'mapId_123';
    const testViewPresets: ViewPresetListItem[] = [
      {
        id: 'preset-1',
        scope: 'system',
        title: 'Observation',
        date: '2022-06-01T12:34:27.787192',
      },
      {
        id: 'preset-2',
        scope: 'system',
        title: 'Observation & elevation',
        date: '2022-06-01T12:34:27.787192',
      },
    ];

    const layerId10 = 'layerid_10';
    const testViewPreset: ViewPreset = {
      ...emptyViewPreset('Map'),
      initialProps: {
        mapPreset: {
          layers: [
            {
              id: layerId10,
              layerType: 'mapLayer',
              name: '10M/ta',
              service: 'https://geoservices.knmi.nl/adagucserver?dataset=OBS',
            },
            {
              enabled: true,
              id: 'layerid_20',
              layerType: 'mapLayer',
              name: 'air_pressure_at_mean_sea_level_hagl',
              service:
                'https://geoservices.knmi.nl/adagucserver?dataset=uwcw_ha43_dini_5p5km',
            },
          ] as layerTypes.Layer[],
          proj: {
            bbox: {
              bottom: 6408480.4514,
              left: 58703.6377,
              right: 3967387.5161,
              top: 11520588.9031,
            },
            srs: 'EPSG:3857',
          },
          animationPayload: {
            interval: 5,
            speed: 4,
          },
          activeLayerId: layerId10,
          autoTimeStepLayerId: layerId10,
          autoUpdateLayerId: layerId10,
          displayMapPin: true,
          shouldAnimate: false,
          shouldAutoUpdate: true,
          shouldShowZoomControls: true,
          showTimeSlider: true,
        },
        syncGroupsIds: ['group1', 'group2'],
      },
    };
    const newTestId = 'new-test-id';
    const mockSaveAs = jest.fn(() => newTestId);
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) => {
            resolve({
              data: testViewPresets,
            });
          });
        },
        getViewPreset: (): Promise<{ data: ViewPreset }> => {
          return new Promise((resolve) => {
            resolve({
              data: testViewPreset,
            });
          });
        },
        saveViewPresetAs:
          mockSaveAs as unknown as PresetsApi['saveViewPresetAs'],
        saveViewPreset: jest.fn(),
        deleteViewPreset: jest.fn(),
      };
    };

    const viewPresetDialog = {
      title: translateKeyOutsideComponents(
        i18n.t,
        'workspace-mappreset-save-as',
      ),
      action: PresetAction.SAVE_AS,
      viewPresetId: 'preset-1',
      panelId: mapId,
      formValues: {
        ...testViewPreset,
        title: testViewPresets[0].title,
      },
      abstract: '',
    };

    const store = createMockStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <ViewPresetsFormDialogConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.openViewPresetDialog({ viewPresetDialog }),
      ),
    );
    await screen.findByTestId('map-preset-dialog');

    const input = screen.getByRole('textbox', { name: /Map preset name/i });
    expect(input).toBeTruthy();
    expect(input!.getAttribute('value')).toEqual(testViewPresets[0].title);

    const newTitle = ' New title ';
    fireEvent.change(input!, {
      target: { value: newTitle },
    });
    expect(input!.getAttribute('value')).toEqual(newTitle);

    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(mockSaveAs).toHaveBeenCalled();
    });
    expect(mockSaveAs).toHaveBeenCalledWith({
      ...testViewPreset,
      title: newTitle.trim(),
      scope: 'user',
      abstract: '',
    });
  });

  it('should save an existing preset as a new preset when choosing Save As with updating the scope', async () => {
    const mapId = 'mapId_123';
    const scope = 'system';
    const testViewPresets: ViewPresetListItem[] = [
      {
        id: 'preset-1',
        scope: 'system',
        title: 'Observation',
        date: '2022-06-01T12:34:27.787192',
      },
      {
        id: 'preset-2',
        scope: 'system',
        title: 'Observation & elevation',
        date: '2022-06-01T12:34:27.787192',
      },
    ];

    const layerId10 = 'layerid_10';
    const testViewPreset: ViewPreset = {
      ...emptyViewPreset('Map'),
      initialProps: {
        mapPreset: {
          layers: [
            {
              id: layerId10,
              layerType: 'mapLayer',
              name: '10M/ta',
              service: 'https://geoservices.knmi.nl/adagucserver?dataset=OBS',
            },
            {
              enabled: true,
              id: 'layerid_20',
              layerType: 'mapLayer',
              name: 'air_pressure_at_mean_sea_level_hagl',
              service:
                'https://geoservices.knmi.nl/adagucserver?dataset=uwcw_ha43_dini_5p5km',
            },
          ] as layerTypes.Layer[],
          proj: {
            bbox: {
              bottom: 6408480.4514,
              left: 58703.6377,
              right: 3967387.5161,
              top: 11520588.9031,
            },
            srs: 'EPSG:3857',
          },
          animationPayload: {
            interval: 5,
            speed: 4,
          },
          activeLayerId: layerId10,
          autoTimeStepLayerId: layerId10,
          autoUpdateLayerId: layerId10,
          displayMapPin: true,
          shouldAnimate: false,
          shouldAutoUpdate: true,
          shouldShowZoomControls: true,
          showTimeSlider: true,
        },
        syncGroupsIds: ['group1', 'group2'],
      },
    };
    const newTestId = 'new-test-id';
    const mockSaveAs = jest.fn(() => newTestId);
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) => {
            resolve({
              data: testViewPresets,
            });
          });
        },
        getViewPreset: (): Promise<{ data: ViewPreset }> => {
          return new Promise((resolve) => {
            resolve({
              data: { ...testViewPreset, scope },
            });
          });
        },
        saveViewPresetAs:
          mockSaveAs as unknown as PresetsApi['saveViewPresetAs'],
        saveViewPreset: jest.fn(),
        deleteViewPreset: jest.fn(),
      };
    };

    const viewPresetDialog = {
      title: translateKeyOutsideComponents(
        i18n.t,
        'workspace-mappreset-save-as',
      ),
      action: PresetAction.SAVE_AS,
      viewPresetId: 'preset-1',
      panelId: mapId,
      formValues: {
        ...testViewPreset,
        title: testViewPresets[0].title,
      },
    };

    const store = createMockStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <ViewPresetsFormDialogConnect scope={scope} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.openViewPresetDialog({ viewPresetDialog }),
      ),
    );
    await screen.findByTestId('map-preset-dialog');

    const input = screen.getByRole('textbox', { name: /Map preset name/i });
    expect(input).toBeTruthy();
    expect(input!.getAttribute('value')).toEqual(testViewPresets[0].title);

    const newTitle = ' New title ';
    fireEvent.change(input!, {
      target: { value: newTitle },
    });
    expect(input!.getAttribute('value')).toEqual(newTitle);

    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(mockSaveAs).toHaveBeenCalled();
    });
  });

  it('should delete a user preset', async () => {
    const mapId = 'mapId_123';

    const mockDelete = jest.fn();
    const testViewPresets: ViewPresetListItem[] = [
      {
        id: 'preset-1',
        scope: 'user',
        title: 'Observations',
        date: '2022-06-01T12:34:27.787192',
      },
    ];

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) => {
            resolve({
              data: testViewPresets,
            });
          });
        },
        getViewPreset: jest.fn(),
        saveViewPreset: jest.fn(),
        saveViewPresetAs: jest.fn(),
        deleteViewPreset: mockDelete,
      };
    };

    const viewPresetDialog = {
      title: translateKeyOutsideComponents(
        i18n.t,
        'workspace-mappreset-dialog-title-delete',
      ),
      action: PresetAction.DELETE,
      viewPresetId: testViewPresets[0].id,
      panelId: mapId,
      formValues: {
        title: testViewPresets[0].title,
        initialProps: {
          mapPreset: {
            layers: [
              {
                name: 'WorldMap_Light_Grey_Canvas',
                type: 'twms',
                dimensions: [],
                id: 'layerid_100',
                opacity: 1,
                enabled: true,
                layerType: LayerType.baseLayer,
              },
              {
                service: 'https://geoservices.knmi.nl/wms?DATASET=baselayers&',
                name: 'countryborders',
                format: 'image/png',
                dimensions: [],
                id: 'layerid_109',
                opacity: 1,
                enabled: true,
                layerType: LayerType.overLayer,
              },
            ],
            activeLayerId: 'layerid_107',
            autoTimeStepLayerId: 'layerid_107',
            autoUpdateLayerId: 'layerid_107',
            proj: {
              bbox: {
                left: -450651.2255879827,
                bottom: 6490531.093143953,
                right: 1428345.8183648037,
                top: 7438773.776232235,
              },
              srs: 'EPSG:3857',
            },
            shouldAnimate: false,
            shouldAutoUpdate: false,
            showTimeSlider: true,
            displayMapPin: false,
            shouldShowZoomControls: true,
            animationPayload: {
              interval: 5,
              speed: 4,
            },
            toggleTimestepAuto: true,
            shouldShowLegend: false,
          },
          syncGroupsIds: [],
        },
      },
    };

    const store = createMockStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <ViewPresetsFormDialogConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.openViewPresetDialog({ viewPresetDialog }),
      ),
    );
    await screen.findByTestId('map-preset-dialog');

    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(mockDelete).toHaveBeenCalled();
    });
    expect(mockDelete).toHaveBeenCalledWith(testViewPresets[0].id);
  });

  it('should reset a preset', async () => {
    const mapId = 'mapId_123';

    const mockGetViewPreset = jest.fn();
    const testViewPresets: ViewPresetListItem[] = [
      {
        id: 'preset-1',
        scope: 'user',
        title: 'Observations',
        date: '2022-06-01T12:34:27.787192',
      },
    ];

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) => {
            resolve({
              data: testViewPresets,
            });
          });
        },
        getViewPreset: mockGetViewPreset,
      };
    };

    const viewPresetDialog = {
      title: 'Reset to original',
      action: PresetAction.RESET,
      viewPresetId: testViewPresets[0].id,
      panelId: mapId,
      formValues: {
        title: testViewPresets[0].title,
      },
    };

    const store = createMockStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <ViewPresetsFormDialogConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.openViewPresetDialog({ viewPresetDialog }),
      ),
    );
    await screen.findByTestId('map-preset-dialog');

    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(mockGetViewPreset).toHaveBeenCalled();
    });
    expect(mockGetViewPreset).toHaveBeenCalledWith(testViewPresets[0].id);
  });
});
