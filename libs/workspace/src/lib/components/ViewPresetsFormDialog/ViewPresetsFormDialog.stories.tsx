/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { darkTheme } from '@opengeoweb/theme';
import React from 'react';
import { AxiosError, AxiosResponseHeaders } from 'axios';
import { Box } from '@mui/material';
import ViewPresetsFormDialog from './ViewPresetsFormDialog';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { PresetsApi } from '../../utils/api';
import { PresetAction } from '../../store/viewPresets/types';
import { WorkspaceWrapperProviderWithStore } from '../Providers';
import { useWorkspaceTranslation } from '../../utils/i18n';
import { createMockStore } from '../../store/store';

export default { title: 'components/ViewPresets/ViewPresetsFormDialog' };

const SaveAsDemo = (): React.ReactElement => {
  const { t } = useWorkspaceTranslation();

  return (
    <Box sx={{ minHeight: 1080 }}>
      <ViewPresetsFormDialog
        isOpen
        title={t('workspace-mappreset-save-as')}
        action={PresetAction.SAVE_AS}
        viewPresetId="Map"
        viewComponentType="Map"
        formValues={{
          title: 'Map preset',
          abstract: 'some abstract',
        }}
        onClose={(): void => {}}
        onSuccess={(): void => {}}
        onReset={(): void => {}}
      />
    </Box>
  );
};

export const SaveAsDemoLightTheme = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore store={createMockStore()}>
    <SaveAsDemo />
  </WorkspaceWrapperProviderWithStore>
);

export const SaveAsDemoDarkTheme = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore
    store={createMockStore()}
    theme={darkTheme}
  >
    <SaveAsDemo />
  </WorkspaceWrapperProviderWithStore>
);

SaveAsDemoLightTheme.tags = ['snapshot'];

SaveAsDemoDarkTheme.tags = ['snapshot'];

const DeleteDemo = (): React.ReactElement => {
  const { t } = useWorkspaceTranslation();

  return (
    <Box sx={{ minHeight: 1080 }}>
      <ViewPresetsFormDialog
        isOpen
        title={t('workspace-mappreset-dialog-title-delete')}
        action={PresetAction.DELETE}
        viewPresetId="Map"
        viewComponentType="Map"
        formValues={{
          title: 'Radar',
        }}
        onClose={(): void => {}}
        onSuccess={(): void => {}}
        onReset={(): void => {}}
      />
    </Box>
  );
};

export const DeleteDemoLightTheme = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore store={createMockStore()}>
    <DeleteDemo />
  </WorkspaceWrapperProviderWithStore>
);

export const DeleteDemoDarkTheme = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore
    store={createMockStore()}
    theme={darkTheme}
  >
    <DeleteDemo />
  </WorkspaceWrapperProviderWithStore>
);

DeleteDemoLightTheme.tags = ['snapshot'];

DeleteDemoDarkTheme.tags = ['snapshot'];

const EditDemo = (): React.ReactElement => {
  const { t } = useWorkspaceTranslation();

  return (
    <Box sx={{ minHeight: 1080 }}>
      <ViewPresetsFormDialog
        isOpen
        title={t('workspace-mappreset-dialog-title-edit')}
        action={PresetAction.EDIT}
        viewPresetId="Map"
        viewComponentType="Map"
        formValues={{
          title: 'Radar',
        }}
        onClose={(): void => {}}
        onSuccess={(): void => {}}
        onReset={(): void => {}}
      />
    </Box>
  );
};

export const EditDemoLightTheme = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore>
    <EditDemo />
  </WorkspaceWrapperProviderWithStore>
);

export const EditDemoDarkTheme = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore theme={darkTheme}>
    <EditDemo />
  </WorkspaceWrapperProviderWithStore>
);

const fakeBackendErrorSaveAs: AxiosError = {
  isAxiosError: true,
  config: undefined!,
  toJSON: undefined!,
  name: 'API error',
  message: '',
  response: {
    data: 'Could not save preset: Preset title already in use, please choose a different title',
    status: 400,
    statusText: '',
    config: undefined!,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

const fakeBackendErrorDelete: AxiosError = {
  isAxiosError: true,
  config: undefined!,
  toJSON: undefined!,
  name: 'API error',
  message: '',
  response: {
    data: 'Could not delete preset: Error deleting preset',
    status: 400,
    statusText: '',
    config: undefined!,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

const createApiWithErrors = (): PresetsApi => {
  return {
    ...createFakeApi(),
    saveViewPresetAs: (): Promise<string> => {
      return new Promise((_, reject) => {
        setTimeout(() => {
          reject(fakeBackendErrorSaveAs);
        }, 1000);
      });
    },
    deleteViewPreset: (): Promise<void> => {
      return new Promise((_, reject) => {
        setTimeout(() => {
          reject(fakeBackendErrorDelete);
        }, 1000);
      });
    },
  };
};

export const SaveAsWithError = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore createApi={createApiWithErrors}>
    <SaveAsDemo />
  </WorkspaceWrapperProviderWithStore>
);

export const DeleteWithError = (): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore createApi={createApiWithErrors}>
    <DeleteDemo />
  </WorkspaceWrapperProviderWithStore>
);
