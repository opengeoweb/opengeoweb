/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { render } from '@testing-library/react';
import * as apiUtils from '../../utils/api';
import * as fakeApiUtils from '../../utils/fakeApi';
import { WorkspaceWrapperProviderWithStore } from './Providers';
import { WorkspaceModuleProvider } from './WorkspaceModuleProvider';

jest.mock('../../utils/api', () => ({
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ...(jest.requireActual('../../utils/api') as any),
  createApi: jest.fn(),
}));

jest.mock('../../utils/fakeApi', () => ({
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ...(jest.requireActual('../../utils/fakeApi') as any),
  createApi: jest.fn(),
}));

describe('components/Providers/WorkspaceModuleProvider', () => {
  it('should use realApi when baseURL is given', () => {
    const props = {
      config: {
        baseURL: 'test.baseURL',
        appURL: 'test.appURL',
        authTokenURL: 'test.authTokenURL',
        authClientId: 'test.authCLientId',
      },
      auth: {
        username: 'test_user',
        token: 'test_token',
        refresh_token: 'test_refresh_token',
      },
      onSetAuth: jest.fn(),
      id: 'mappresets-mini',
    };
    const componentChildren = 'some children';

    render(
      <WorkspaceWrapperProviderWithStore>
        <WorkspaceModuleProvider {...props}>
          <p>{componentChildren}</p>
        </WorkspaceModuleProvider>
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(apiUtils.createApi).toHaveBeenCalled();
  });

  it('should use fakeApi when no baseURL is given', () => {
    const props = {
      config: {
        appURL: 'test.appURL',
        authTokenURL: 'test.authTokenURL',
        authClientId: 'test.authCLientId',
      },
      auth: {
        username: 'test_user',
        token: 'test_token',
        refresh_token: 'test_refresh_token',
      },
      onSetAuth: jest.fn(),
      id: 'mappresets-mini',
    };
    const componentChildren = 'some children';

    render(
      <WorkspaceWrapperProviderWithStore>
        <WorkspaceModuleProvider {...props}>
          <p>{componentChildren}</p>
        </WorkspaceModuleProvider>
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(fakeApiUtils.createApi).toHaveBeenCalled();
  });
});
