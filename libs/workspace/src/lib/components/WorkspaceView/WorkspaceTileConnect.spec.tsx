/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  fireEvent,
  screen,
  render,
  act,
  waitFor,
} from '@testing-library/react';
import { Mosaic, MosaicNode } from 'react-mosaic-component';
import { MosaicKey } from 'react-mosaic-component/lib/types';
import { mapActions } from '@opengeoweb/store';
import WorkspaceTileConnect from './WorkspaceTileConnect';
import { WorkspaceComponentLookupPayload } from '../../store/workspace/types';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { workspaceActions } from '../../store/workspace/reducer';
import { viewPresetActions } from '../../store/viewPresets';
import {
  ViewPresetError,
  ViewPresetErrorComponent,
  ViewPresetErrorType,
} from '../../store/viewPresets/types';
import { i18n, translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store/store';

const componentsLookUp = (
  payload: WorkspaceComponentLookupPayload,
): React.ReactElement => {
  const { componentType, initialProps: props, id } = payload;
  switch (componentType) {
    case 'MyTestComponent':
    default:
      return (
        <div data-testid="MyTestComponentTestId">
          {id}
          {JSON.stringify(props)}
        </div>
      );
  }
};

describe('components/WorkspaceView/WorkspaceTileConnect', () => {
  const screenConfig = {
    id: 'preset1',
    title: 'Preset 1',
    views: {
      allIds: ['screen1', 'screen2'],
      byId: {
        screen1: {
          title: 'screen 1',
          id: 'viewpreset1',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
        screen2: {
          title: 'screen 2',
          id: 'viewpreset2',
          componentType: 'Map',
          initialProps: {
            mapPreset: {
              layers: [
                {
                  service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
                  name: 'RAD_NL25_PCP_CM',
                  format: 'image/png',
                  enabled: true,
                  layerType: 'mapLayer',
                },
              ],
              proj: {
                bbox: {
                  left: -450651.2255879827,
                  bottom: 6490531.093143953,
                  right: 1428345.8183648037,
                  top: 7438773.776232235,
                },
                srs: 'EPSG:3857',
              },
            },
          },
        },
      },
    },
    mosaicNode: {
      direction: 'row' as const,
      first: 'screen1',
      second: 'screen2',
    },
  };
  it('should add a view', async () => {
    const store = createMockStore();
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: 'screen1',
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );
    await waitFor(() =>
      expect(store.getState().workspace.views.allIds).toHaveLength(2),
    );

    fireEvent.click(screen.getAllByTestId('split-horizontal-btn')[0]);

    await waitFor(() =>
      expect(store.getState().workspace.views.allIds).toHaveLength(3),
    );
  });

  it('should render default ViewLoading component', async () => {
    const screenConfig2 = {
      id: 'preset1',
      title: 'Preset 1',
      views: {
        allIds: ['screen1'],
        byId: {
          screen1: {
            title: 'screen 1',
            id: 'viewpreset1',
            componentType: 'ViewLoading',
            initialProps: {},
          },
        },
      },
      mosaicNode: {
        direction: 'row' as const,
        first: 'screen1',
        second: 'second',
      },
    };

    const store = createMockStore();
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: 'screen1',
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig2 }),
      ),
    );
    expect(screen.queryAllByText('Loading viewpreset1')).toHaveLength(2);
  });

  it('should render default ViewError component', async () => {
    const screenConfig2 = {
      id: 'preset1',
      title: 'Preset 1',
      views: {
        allIds: ['screen1'],
        byId: {
          screen1: {
            title: 'screen 1',
            id: 'viewPreset1',
            componentType: 'ViewError',
            initialProps: {},
          },
        },
      },
      mosaicNode: {
        direction: 'row' as const,
        first: 'screen1',
        second: '',
      },
    };

    const store = createMockStore();
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: 'screen1',
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig2 }),
      ),
    );

    expect(
      screen.queryAllByText(
        translateKeyOutsideComponents(
          i18n.t,
          'workspace-viewpresets-fail-fetch',
          {
            VIEWPRESET_ID: 'viewPreset1',
          },
        ),
      ),
    ).toHaveLength(2);
  });

  it('should use the bbox and srs of the clicked view when splitting a map', async () => {
    const bbox = {
      // different bbox than in screenconfig to simulate user has moved the map
      left: 299317.22579447436,
      bottom: 4028678.1331711058,
      right: 2178314.269747261,
      top: 4976920.816259388,
    };
    const srs = 'EPSG:32661';

    const store = createMockStore();
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: 'screen2',
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );
    await act(() =>
      store.dispatch(mapActions.registerMap({ mapId: 'screen2' })),
    );
    await act(() =>
      store.dispatch(mapActions.setBbox({ mapId: 'screen2', bbox, srs })),
    );

    fireEvent.click(screen.getAllByTestId('split-vertical-btn')[0]);
    await waitFor(() =>
      expect(store.getState().workspace.views.allIds).toHaveLength(3),
    );

    expect(store.getState().workspace.views.byId['1_screen']).toEqual({
      componentType: 'Map',
      initialProps: {
        mapPreset: { proj: { bbox, srs } },
        syncGroupsIds: [],
      },
    });
  });

  it('should create a new id for a splitted view', async () => {
    const screenConfig2 = {
      id: 'preset1',
      title: 'Preset 1',
      views: {
        allIds: ['1_screen', '2_screen'],
        byId: {
          '1_screen': {
            title: 'screen 1',
            id: 'viewpreset1',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
          '2_screen': {
            title: 'screen 2',
            id: 'viewpreset2',
            componentType: 'Map',
            initialProps: {
              mapPreset: {
                layers: [
                  {
                    service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
                    name: 'RAD_NL25_PCP_CM',
                    format: 'image/png',
                    enabled: true,
                    layerType: 'mapLayer',
                  },
                ],
                proj: {
                  bbox: {
                    left: -450651.2255879827,
                    bottom: 6490531.093143953,
                    right: 1428345.8183648037,
                    top: 7438773.776232235,
                  },
                  srs: 'EPSG:3857',
                },
              },
            },
          },
        },
      },
      mosaicNode: {
        direction: 'row' as const,
        first: '1_screen',
        second: '2_screen',
      },
    };
    const bbox = {
      // different bbox than in screenconfig to simulate user has moved the map
      left: 299317.22579447436,
      bottom: 4028678.1331711058,
      right: 2178314.269747261,
      top: 4976920.816259388,
    };
    const srs = 'EPSG:32661';

    const store = createMockStore();
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: 'screen_no_1',
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig2 }),
      ),
    );
    await act(() =>
      store.dispatch(mapActions.registerMap({ mapId: 'screen_no_1' })),
    );
    await act(() =>
      store.dispatch(mapActions.setBbox({ mapId: 'screen_no_1', bbox, srs })),
    );

    fireEvent.click(screen.getAllByTestId('split-vertical-btn')[0]);

    await waitFor(() =>
      expect(store.getState().workspace.views.allIds).toHaveLength(3),
    );

    expect(store.getState().workspace.views.byId['3_screen']).toEqual({
      componentType: 'Map',
      initialProps: {
        mapPreset: { proj: { bbox, srs } },
        syncGroupsIds: [],
      },
    });
  });

  it('should unregister view on unmount', async () => {
    const store = createMockStore();
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: 'screen1',
    };

    const { unmount } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );

    await waitFor(() =>
      expect(store.getState().viewPresets.ids).toHaveLength(1),
    );

    unmount();
    await waitFor(() =>
      expect(store.getState().viewPresets.ids).toHaveLength(0),
    );
  });

  it('should show loading bar when viewpreset is loading', async () => {
    const mapId = 'screen1';
    const store = createMockStore();
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: mapId,
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.fetchViewPresets({
          panelId: mapId,
          filterParams: {},
        }),
      ),
    );
    expect(screen.getAllByTestId('loading-bar')).toBeTruthy();
  });

  it('should show viewpreset list dialog and close it', async () => {
    const mapId = 'test-1';
    const store = createMockStore();
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: mapId,
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.toggleViewPresetListDialog({
          panelId: mapId,
          isViewPresetListDialogOpen: true,
        }),
      ),
    );
    expect(
      store.getState().viewPresets.entities[mapId].isViewPresetListDialogOpen,
    ).toEqual(true);

    const backDrops = screen.queryAllByTestId('workspace-viewpreset-menu');
    expect(backDrops).toBeTruthy();
    fireEvent.click(backDrops[0].firstChild!);

    await waitFor(() => {
      expect(
        store.getState().viewPresets.entities[mapId].isViewPresetListDialogOpen,
      ).toEqual(false);
    });
    const backDrops2 = screen.queryAllByTestId('workspace-viewpreset-menu');
    expect(backDrops2).toHaveLength(0);
  });

  it('should show error when viewpreset has error', async () => {
    const mapId = 'test-1';
    const testError: ViewPresetError = {
      component: ViewPresetErrorComponent.PRESET_DETAIL,
      message: 'Testing something went wrong',
      errorType: ViewPresetErrorType.GENERIC,
    };

    const store = createMockStore();
    const props = {
      componentsLookUp,
      path: [],
      mosaicNodeId: mapId,
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <WorkspaceTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.errorViewPreset({
          panelId: mapId,
          error: testError,
        }),
      ),
    );

    expect(screen.getAllByText(testError.message)).toBeTruthy();
  });
});
