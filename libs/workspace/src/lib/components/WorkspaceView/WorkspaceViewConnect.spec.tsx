/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import WorkspaceViewConnect from './WorkspaceViewConnect';
import {
  WorkspaceComponentLookupPayload,
  WorkspacePreset,
} from '../../store/workspace/types';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { workspaceActions } from '../../store/workspace/reducer';
import { createMockStore } from '../../store/store';

const componentsLookUp = (
  payload: WorkspaceComponentLookupPayload,
): React.ReactElement => {
  const { componentType, initialProps: props, id } = payload;
  switch (componentType) {
    case 'MyTestComponent':
      return (
        <div data-testid="MyTestComponentTestId">
          {id}
          {JSON.stringify(props)}
        </div>
      );
    default:
      return null!;
  }
};

describe('components/WorkspaceView/WorkspaceViewConnect', () => {
  const mosaicNode = {
    direction: 'row',
    first: 'screen1',
    second: 'screen2',
  } as const;
  const screenConfig: WorkspacePreset = {
    id: 'preset1',
    title: 'Preset 1',
    views: {
      allIds: ['screen1', 'screen2'],
      byId: {
        screen1: {
          id: 'test-screen-1',
          title: 'screen 1',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
        screen2: {
          id: 'test-screen-2',
          title: 'screen 2',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
      },
    },
    mosaicNode,
  };
  it('should return a view with correct title', async () => {
    const store = createMockStore();
    const { baseElement } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      baseElement.querySelector('.mosaic-window-title')!.textContent,
    ).toEqual(
      `${screenConfig.views.byId![screenConfig.views.allIds[0]].title}${
        screenConfig.views.allIds[0]
      }`,
    );
  });

  it('should expand a view', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );
    fireEvent.click(screen.getAllByLabelText('Expand window')[0]);

    expect(store.getState().workspace.mosaicNode).toEqual({
      ...mosaicNode,
      splitPercentage: 70,
    });
  });

  it('should set active window', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );

    expect(store.getState().ui.activeWindowId).toEqual('screen1');

    const toolbar = screen.getAllByText('screen 2')[0];
    fireEvent.click(toolbar);

    expect(store.getState().ui.activeWindowId).toEqual('screen2');
  });

  it('should keep window active on click if window is already active', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );

    expect(store.getState().ui.activeWindowId).toEqual('screen1');

    const toolbar = screen.getAllByText('screen 1')[0];
    fireEvent.click(toolbar);

    expect(store.getState().ui.activeWindowId).toEqual('screen1');
  });

  it('should split a view vertically', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );

    fireEvent.click(screen.getAllByLabelText('Split window vertically')[0]);

    expect(store.getState().workspace.mosaicNode).toEqual({
      direction: 'row',
      first: {
        direction: 'row',
        first: 'screen1',
        second: expect.any(String),
      },
      second: 'screen2',
    });
  });

  it('should split a view horizontally', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );

    fireEvent.click(screen.getAllByLabelText('Split window horizontally')[0]);

    expect(store.getState().workspace.mosaicNode).toEqual({
      direction: 'row',
      first: {
        direction: 'column',
        first: 'screen1',
        second: expect.any(String),
      },
      second: 'screen2',
    });
  });

  it('should resize a view', async () => {
    const resizeSpy = jest.fn();
    window.addEventListener('resize', resizeSpy);

    const store = createMockStore();

    const { baseElement } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );

    // eslint-disable-next-line testing-library/no-node-access
    const draggableLine = baseElement.querySelector('.mosaic-split-line')!;
    fireEvent.mouseDown(draggableLine, { clientX: 1 });
    fireEvent.mouseUp(draggableLine);
    expect(store.getState().workspace.mosaicNode).toEqual({
      direction: 'row',
      first: 'screen1',
      second: 'screen2',
      splitPercentage: expect.any(Number),
    });
    await waitFor(() => expect(resizeSpy).toHaveBeenCalled());
    window.removeEventListener('resize', resizeSpy);
  });

  it('should not have unexpand after split window', async () => {
    const store = createMockStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );

    // First of all there should be two expand buttons
    const expandButtons = await screen.findAllByLabelText('Expand window');
    expect(expandButtons).toHaveLength(2);
    expect(screen.queryByLabelText('Unexpand window')).toBeNull();

    // Now we click one expand button
    fireEvent.click(expandButtons[0]);

    // And expect to find one expand button and on unexpand button
    expect(screen.getAllByLabelText('Expand window')).toHaveLength(1);
    expect(screen.getAllByLabelText('Unexpand window')).toHaveLength(1);

    // Now we split a window
    fireEvent.click(screen.getAllByLabelText('Split window horizontally')[0]);

    // Now there should be 3 expand buttons
    await waitFor(() =>
      expect(screen.getAllByLabelText('Expand window')).toHaveLength(3),
    );
    expect(screen.queryByLabelText('Unexpand window')).toBeNull();
  });

  it('should not have unexpand after resize window', async () => {
    const resizeSpy = jest.fn();
    window.addEventListener('resize', resizeSpy);

    const store = createMockStore();

    const { baseElement } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );

    // First of all there should be two expand buttons
    const expandButtons = screen.getAllByLabelText('Expand window');
    expect(expandButtons).toHaveLength(2);
    expect(screen.queryByLabelText('Unexpand window')).toBeNull();

    // Now we click one expand button
    fireEvent.click(expandButtons[0]);

    // And expect to find one expand button and on unexpand button
    expect(screen.getAllByLabelText('Expand window')).toHaveLength(1);
    expect(screen.getAllByLabelText('Unexpand window')).toHaveLength(1);

    // Now we resize a window
    const draggableLine =
      // eslint-disable-next-line testing-library/no-node-access
      baseElement.getElementsByClassName('mosaic-split-line')[0];
    fireEvent.mouseDown(draggableLine, { clientX: 1 });
    fireEvent.mouseUp(draggableLine);

    await waitFor(() => expect(resizeSpy).toHaveBeenCalled());

    // And expect to find two expand buttons
    expect(screen.getAllByLabelText('Expand window')).toHaveLength(2);
    expect(screen.queryByLabelText('Unexpand window')).toBeNull();
  });

  it('should close a view', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );
    expect(screen.getAllByLabelText('Close window')).toHaveLength(2);

    fireEvent.click(screen.getAllByLabelText('Close window')[0]);

    expect(screen.getAllByLabelText('Close window')).toHaveLength(1);
  });

  it('should listen to window orientation change event', async () => {
    jest.spyOn(window, 'addEventListener');
    jest.spyOn(window, 'removeEventListener');

    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );
    // Trigger the window orientation change event.
    await act(() => window.dispatchEvent(new Event('orientationchange')));

    await waitFor(() => {
      expect(window.addEventListener).toHaveBeenCalled();
    });
    expect(window.addEventListener).toHaveBeenCalledWith(
      'orientationchange',
      expect.any(Function),
    );
    fireEvent.click(screen.getAllByTestId('close-btn')[0]);
    await waitFor(() => expect(window.removeEventListener).toHaveBeenCalled());
  });

  // NOTE: Leave this test as last to avoid the next test failing with a typeError from react-mosaic-component. Fix proposed in https://github.com/nomcopter/react-mosaic/pull/216
  it('should resize a view and should update views afterwards with 1 action', async () => {
    const resizeSpy = jest.fn();
    window.addEventListener('resize', resizeSpy);

    const store = createMockStore();
    const { baseElement } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );
    await waitFor(() =>
      expect(store.getState().workspace.mosaicNode).toEqual({
        direction: 'row',
        first: 'screen1',
        second: 'screen2',
      }),
    );
    // eslint-disable-next-line testing-library/no-node-access
    const draggableLine = baseElement.querySelector('.mosaic-split-line')!;
    fireEvent.mouseDown(draggableLine, { clientX: 1 });
    fireEvent.mouseMove(draggableLine, { clientX: 100 });
    fireEvent.mouseMove(draggableLine, { clientX: 50 });
    fireEvent.mouseMove(draggableLine, { clientX: 150 });
    fireEvent.mouseUp(draggableLine);

    await waitFor(() =>
      expect(store.getState().workspace.mosaicNode).toEqual({
        direction: 'row',
        first: 'screen1',
        second: 'screen2',
        splitPercentage: expect.any(Number),
      }),
    );
    await waitFor(() => expect(resizeSpy).toHaveBeenCalledTimes(1));
    window.removeEventListener('resize', resizeSpy);
  });
});
