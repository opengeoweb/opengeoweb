/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, screen, act } from '@testing-library/react';
import { loadingIndicatorActions } from '@opengeoweb/store';
import { LoadingIndicatorConnect } from './LoadingIndicatorConnect';
import { WorkspaceWrapperProviderWithStore } from '../../Providers';
import { createMockStore } from '../../../store/store';

describe('src/components/WorkspaceViewTitle/LoadingIndicatorConnect', () => {
  describe('LoadingIndicatorConnect', () => {
    it('should return idle icon when not loading', async () => {
      const panelId = 'panel1';
      const store = createMockStore();
      render(
        <WorkspaceWrapperProviderWithStore store={store}>
          <LoadingIndicatorConnect panelId={panelId} />
        </WorkspaceWrapperProviderWithStore>,
      );

      await act(() =>
        store.dispatch(
          loadingIndicatorActions.setGetMapIsLoading({
            id: panelId,
            isGetMapLoading: false,
          }),
        ),
      );

      expect(screen.queryByTestId('cloudicon-loading')).toBeFalsy();
      expect(screen.getByTestId('cloudicon-idle')).toBeTruthy();
    });
    it('should return loading icon when loading', async () => {
      const store = createMockStore();
      const panelId = 'panel1';
      render(
        <WorkspaceWrapperProviderWithStore store={store}>
          <LoadingIndicatorConnect panelId={panelId} />
        </WorkspaceWrapperProviderWithStore>,
      );

      await act(() =>
        store.dispatch(
          loadingIndicatorActions.setGetMapIsLoading({
            id: panelId,
            isGetMapLoading: true,
          }),
        ),
      );

      expect(screen.queryByTestId('cloudicon-idle')).toBeFalsy();
      expect(screen.getByTestId('cloudicon-loading')).toBeTruthy();
    });
  });
});
