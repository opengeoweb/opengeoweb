/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { WorkspaceI18nProvider } from '../Providers';
import workspaceTranslations from '../../../../locales/workspace.json';
import WorkspaceSelectList from './WorkspaceSelectList';
import { WorkspacePresetListItem } from '../../store/workspace/types';
import {
  getEmptyMapWorkspace,
  getEmptyWorkspaceListItem,
} from '../../store/workspaceList/utils';
import { i18n } from '../../utils/i18n';

describe('workspace/components/WorkspaceSelectList', () => {
  const testList: WorkspacePresetListItem[] = [
    {
      id: 'screenConfigRadarTemp',
      title: 'Radar and temperature',
      date: '2022-06-01T12:34:27.787184',
      scope: 'system',
      abstract: 'This can contain an abstract',
      viewType: 'multiWindow',
    },
    {
      id: 'screenConfigTimeSeries',
      title: 'TimeSeries Example',
      date: '2022-06-01T12:34:27.787184',
      scope: 'system',
      abstract:
        'This contains a very very long abstract so that you can see some ellipsis. Who knows how it will behave. ',
      viewType: 'multiWindow',
    },
  ];
  it('should render component', () => {
    const props = {
      workspacePresets: testList,
      currentSelectedId: testList[0].id,
      onClickWorkspacePreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
    };
    render(
      <WorkspaceI18nProvider>
        <WorkspaceSelectList {...props} />
      </WorkspaceI18nProvider>,
    );
    expect(screen.getByTestId('workspace-selectList')).toBeTruthy();
    expect(screen.getByText('New workspace')).toBeTruthy();
  });
  it('should render number of results + 1 for hardcoded FE new workspace preset', () => {
    const props = {
      workspacePresets: testList,
      currentSelectedId: testList[0].id,
      onClickWorkspacePreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
    };
    render(
      <WorkspaceI18nProvider>
        <WorkspaceSelectList {...props} />
      </WorkspaceI18nProvider>,
    );
    const results = workspaceTranslations['en']['workspace-results'];
    expect(screen.getByText(`${testList.length} ${results}`)).toBeTruthy();

    expect(screen.getAllByTestId('workspace-selectListRow').length).toBe(3);
    // Hardcoded FE `New workspace` item
    expect(
      screen.getByText(getEmptyWorkspaceListItem(i18n.t).title),
    ).toBeTruthy();
    expect(screen.getByText(testList[0].title)).toBeTruthy();
    expect(screen.getByText(testList[0].abstract)).toBeTruthy();
    expect(screen.getByText(testList[1].title)).toBeTruthy();
  });
  it('should call onClickWorkspacePreset when clicking on preset', () => {
    const props = {
      workspacePresets: testList,
      currentSelectedId: testList[0].id,
      onClickWorkspacePreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
    };
    render(<WorkspaceSelectList {...props} />);
    const listItems = screen.getAllByTestId('workspace-selectListRow');

    fireEvent.click(listItems[1]);
    expect(props.onClickWorkspacePreset).toHaveBeenCalledWith(testList[0].id);
  });

  it('should hide new workspace when filters are selected', () => {
    const props = {
      workspacePresets: testList,
      currentSelectedId: testList[0].id,
      onClickWorkspacePreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
      hideNewWorkspace: true,
    };
    render(<WorkspaceSelectList {...props} />);
    expect(screen.queryByText('New workspace')).toBeFalsy();
  });

  it('should highlight search results in title', () => {
    const props = {
      workspacePresets: testList,
      currentSelectedId: testList[0].id,
      onClickWorkspacePreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
      searchQuery: 'rad',
    };
    render(<WorkspaceSelectList {...props} />);
    const foundSearchMarks = screen.getAllByTestId('mark');

    expect(foundSearchMarks).toHaveLength(1);
    expect(foundSearchMarks[0].innerHTML).toEqual('Rad');
  });

  it('should highlight search results in abstract', () => {
    const props = {
      workspacePresets: testList,
      currentSelectedId: testList[0].id,
      onClickWorkspacePreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
      searchQuery: 'contain',
    };
    render(<WorkspaceSelectList {...props} />);
    const foundSearchMarks = screen.getAllByTestId('mark');

    expect(foundSearchMarks).toHaveLength(2);
    expect(foundSearchMarks[0].innerHTML).toEqual('contain');
    expect(foundSearchMarks[1].innerHTML).toEqual('contain');
  });

  it('should use a newPreset', () => {
    const props = {
      workspacePresets: testList,
      currentSelectedId: testList[0].id,
      onClickWorkspacePreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
      searchQuery: 'contain',
      newPreset: {
        ...getEmptyMapWorkspace(),
        id: 'test-id',
        title: 'Test preset',
        viewType: 'singleWindow' as const,
        abstract: '',
      },
    };
    render(<WorkspaceSelectList {...props} />);

    expect(screen.getByText(props.newPreset.title)).toBeTruthy();
    fireEvent.click(screen.getByText(props.newPreset.title));

    expect(props.onClickWorkspacePreset).toHaveBeenCalledWith(
      props.newPreset.id,
    );
  });
});
