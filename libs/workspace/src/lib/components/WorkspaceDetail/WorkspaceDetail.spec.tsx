/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import {
  render,
  fireEvent,
  screen,
  waitFor,
  act,
} from '@testing-library/react';
import { routerActions, syncGroupsReducer, uiReducer } from '@opengeoweb/store';
import { GEOWEB_ROLE_PRESETS_ADMIN } from '@opengeoweb/authentication';
import { snackbarReducer, snackbarListener } from '@opengeoweb/snackbar';
import { configureStore } from '@reduxjs/toolkit';
import { ToolkitStore } from '@reduxjs/toolkit/dist/configureStore';
import { WorkspaceDetail } from './WorkspaceDetail';
import { componentsLookUp } from '../../storyUtils/componentsLookUp';
import {
  defaultAuthConfig,
  WorkspaceWrapperProviderWithStore,
} from '../Providers/Providers';
import {
  workspaceActions,
  workspaceReducer,
} from '../../store/workspace/reducer';
import {
  WorkspaceError,
  WorkspaceErrorType,
  WorkspacePreset,
  WorkspacePresetFromBE,
} from '../../store/workspace/types';
import { getEmptyMapWorkspace } from '../../store/workspaceList/utils';
import { viewPresetsReducer } from '../../store/viewPresets';
import { PresetsApi } from '../../utils/api';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { i18n, translateKeyOutsideComponents } from '../../utils/i18n';
import { ViewPresetListItem } from '../../store/viewPresetsList/types';
import { viewPresetOptionId } from '../WorkspaceOptions/WorkspaceOptions';
import { createMockStore } from '../../store/store';
import { viewPresetsListener } from '../../store/viewPresets/listener';
import { viewPresetsListReducer } from '../../store/viewPresetsList';
import { workspaceListReducer } from '../../store/workspaceList';

const reducer = {
  ui: uiReducer,
  snackbar: snackbarReducer,
  viewPresets: viewPresetsReducer,
  viewPresetsList: viewPresetsListReducer,
  workspace: workspaceReducer,
  workspaceList: workspaceListReducer,
  syncGroups: syncGroupsReducer,
};

const createMockStoreForTest = (): ToolkitStore =>
  configureStore({
    reducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(
        viewPresetsListener.middleware,
        snackbarListener.middleware,
      ),
  });

describe('WorkspaceDetail', () => {
  it('should render correctly', async () => {
    const testChildren = 'some children';
    render(
      <WorkspaceWrapperProviderWithStore>
        <WorkspaceDetail componentsLookUp={componentsLookUp}>
          <p>{testChildren}</p>
        </WorkspaceDetail>
      </WorkspaceWrapperProviderWithStore>,
    );

    await screen.findByTestId('workspace');
    expect(screen.getByText(testChildren)).toBeTruthy();
  });

  it('should show loading bar when isLoading and keep WorkspacePage visible', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore>
        <WorkspaceDetail
          componentsLookUp={componentsLookUp}
          workspaceId="test"
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.fetchWorkspace({
          workspaceId: '',
        }),
      ),
    );
    expect(screen.getByTestId('loading-bar')).toBeTruthy();
    expect(screen.getByTestId('WorkspacePage')).toBeTruthy();
  });

  it('should render empty map preset when no workspace id', async () => {
    const store = createMockStore();
    const spy = jest.spyOn(routerActions, 'navigateToUrl');

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await waitFor(() =>
      expect(store.getState().workspace.title).toEqual(
        getEmptyMapWorkspace(i18n.t).title,
      ),
    );
    await waitFor(() =>
      expect(store.getState().workspace.mosaicNode).toEqual(
        getEmptyMapWorkspace().mosaicNode,
      ),
    );
    await waitFor(() =>
      expect(store.getState().workspace.views).toEqual(
        getEmptyMapWorkspace(i18n.t).views,
      ),
    );

    await waitFor(() => expect(spy).toHaveBeenCalledWith({ url: '/' }));
  });

  it('should start fetching workspace', async () => {
    const testWorkspaceId = 'test';
    const initialWorkspaceId = 'initialTestId';

    render(
      <WorkspaceWrapperProviderWithStore>
        <WorkspaceDetail
          initialWorkspaceId={initialWorkspaceId}
          workspaceId={testWorkspaceId}
          componentsLookUp={componentsLookUp}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('loading-bar')).toBeTruthy();
  });

  it('should show not found error', async () => {
    const testError: WorkspaceError = {
      message: 'test error',
      type: WorkspaceErrorType.NOT_FOUND,
    };

    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail workspaceId="" componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.errorWorkspace({
          workspaceId: '',
          error: testError,
        }),
      ),
    );
    expect(await screen.findByText(`Failed to fetch workspace`)).toBeTruthy();
    expect(screen.queryByText('Try again')).toBeFalsy();
    expect(screen.queryByTestId('WorkspacePage')).toBeFalsy();
  });

  it('should show generic error and be able to try again', async () => {
    const testError: WorkspaceError = {
      message: 'test error',
      type: WorkspaceErrorType.GENERIC,
    };

    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail workspaceId="" componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.errorWorkspace({
          workspaceId: '',
          error: testError,
        }),
      ),
    );
    expect(await screen.findByText(`Failed to fetch workspace`)).toBeTruthy();
    expect(screen.getByText('TRY AGAIN')).toBeTruthy();
    expect(screen.queryByTestId('WorkspacePage')).toBeFalsy();

    fireEvent.click(screen.getByText('TRY AGAIN'));

    await waitFor(() =>
      expect(screen.queryByText(`Failed to fetch workspace`)).toBeFalsy(),
    );
  });

  it('should show save error and be able to try again', async () => {
    const testError: WorkspaceError = {
      message: 'test error',
      type: WorkspaceErrorType.SAVE,
    };
    const store = createMockStore();

    const mockSave = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveWorkspacePreset: mockSave,
      };
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <WorkspaceDetail workspaceId="" componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.errorWorkspace({
          workspaceId: '',
          error: testError,
        }),
      ),
    );

    expect(
      await screen.findByText(
        `Failed to save workspace "${getEmptyMapWorkspace(i18n.t).title}"`,
      ),
    ).toBeTruthy();
    expect(screen.getByText('TRY AGAIN')).toBeTruthy();
    expect(screen.getByTestId('WorkspacePage')).toBeTruthy();

    fireEvent.click(screen.getByText('TRY AGAIN'));

    await waitFor(() =>
      expect(
        screen.queryByText(
          `Failed to save workspace "${getEmptyMapWorkspace(i18n.t).title}"`,
        ),
      ).toBeFalsy(),
    );
    expect(mockSave).toHaveBeenCalled();
  });

  it('should refetch view presets when user switches to admin role', async () => {
    const store = createMockStore();

    const mockGetViewPresets = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getViewPresets: mockGetViewPresets,
      };
    };
    const componentChildren = 'some children';

    const { rerender } = render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <WorkspaceDetail componentsLookUp={componentsLookUp}>
          <p>{componentChildren}</p>
        </WorkspaceDetail>
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('workspace')).toBeTruthy();
    await waitFor(() =>
      expect(mockGetViewPresets).toHaveBeenLastCalledWith({
        scope: 'user,system',
      }),
    );
    mockGetViewPresets.mockReset();
    rerender(
      <WorkspaceWrapperProviderWithStore
        store={store}
        createApi={createApi}
        auth={{ ...defaultAuthConfig, currentRole: GEOWEB_ROLE_PRESETS_ADMIN }}
      >
        <WorkspaceDetail componentsLookUp={componentsLookUp}>
          <p>{componentChildren}</p>
        </WorkspaceDetail>
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('workspace')).toBeTruthy();
    await waitFor(() =>
      expect(mockGetViewPresets).toHaveBeenLastCalledWith({ scope: 'system' }),
    );
    expect(mockGetViewPresets).toHaveBeenCalledTimes(1);
  });

  it('should fetch view presets and be able to select duplicate delete and edit them', async () => {
    const store = createMockStoreForTest();

    const mosaicNode = {
      direction: 'row',
      first: 'screen1',
      second: 'screen2',
    } as const;
    const viewPresetTitle = 'My view preset 1';
    const screenConfig: WorkspacePreset = {
      id: 'preset1',
      title: 'workspace Preset 1',
      views: {
        allIds: ['screen1'],
        byId: {
          screen1: {
            id: 'test-screen-1',
            title: viewPresetTitle,
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
            scope: 'user',
          },
        },
      },
      scope: 'user',
      mosaicNode,
    };

    const mockViewPresets = [
      {
        id: 'screenConfigC',
        title: viewPresetTitle,
        date: '2022-06-01T12:34:27.787184',
        scope: 'user',
      },
      {
        title: 'Radar user',
        id: 'screenRadar',
        date: '2022-06-01T12:34:27.787184',
        scope: 'user',
      },
    ];

    const deleteViewPresetFn = jest.fn();
    const saveAsViewPresetFn = jest.fn();
    const saveViewPresetFn = jest.fn();
    const getViewPresets = jest.fn().mockImplementation(
      (): Promise<{ data: ViewPresetListItem[] }> =>
        new Promise((resolve) => {
          resolve({
            data: mockViewPresets as ViewPresetListItem[],
          });
        }),
    );
    const createMockApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getViewPresets,
        getWorkspacePreset: () =>
          new Promise((resolve) => {
            resolve({
              data: screenConfig as unknown as WorkspacePresetFromBE,
            });
          }),
        deleteViewPreset: deleteViewPresetFn,
        saveViewPresetAs: saveAsViewPresetFn,
        saveViewPreset: saveViewPresetFn,
      };
    };

    const authAdminProps = {
      isLoggedIn: true, // user is logged in
      auth: {
        username: 'user.name',
        token: '1223344',
        refresh_token: '33455214',
      },
      currentRole: GEOWEB_ROLE_PRESETS_ADMIN,
      onLogin: (): void => null!,
      onSetAuth: (): void => null!,
      sessionStorageProvider: null!,
    };
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );
    render(
      <WorkspaceWrapperProviderWithStore
        createApi={createMockApi}
        store={store}
        auth={authAdminProps}
      >
        <WorkspaceDetail componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await waitFor(() => {
      expect(store.getState().viewPresetsList.ids).toHaveLength(0);
    });

    await waitFor(() => {
      expect(store.getState().viewPresetsList.ids).toHaveLength(
        mockViewPresets.length,
      );
    });

    await waitFor(() => {
      expect(screen.getAllByText(viewPresetTitle)[0]).toBeTruthy();
    });
    expect(getViewPresets).toHaveBeenCalled();
    getViewPresets.mockClear();

    // open view preset dialog
    fireEvent.click(screen.getAllByTestId('open-viewpresets')[0]);
    expect(screen.getByText('View presets menu')).toBeTruthy();

    // select user preset
    fireEvent.click(screen.getAllByText(mockViewPresets[0].title)[1]);
    await waitFor(() =>
      expect(screen.queryByText('View presets menu')).toBeFalsy(),
    );

    // open view list dialog
    const viewPresetOptionMenu = screen.getAllByTestId('open-viewpresets')[1];
    fireEvent.click(viewPresetOptionMenu);
    await waitFor(() => {
      expect(screen.getByText('View presets menu')).toBeTruthy();
    });

    // duplicate
    expect(saveAsViewPresetFn).not.toHaveBeenCalled();
    fireEvent.click(screen.getAllByTestId('viewpreset-listOptionsButton')[0]);
    const duplicateButton = screen.getByText('Duplicate');
    await waitFor(() => {
      expect(duplicateButton).toBeTruthy();
    });

    fireEvent.click(duplicateButton);
    fireEvent.click(screen.getByText('Save'));
    await waitFor(() => {
      expect(saveAsViewPresetFn).toHaveBeenCalled();
    });
    expect(getViewPresets).toHaveBeenCalledTimes(1);

    // edit
    expect(saveViewPresetFn).not.toHaveBeenCalled();
    fireEvent.click(screen.getAllByTestId('viewpreset-listOptionsButton')[0]);
    const editButton = screen.getByText(
      translateKeyOutsideComponents(i18n.t, 'workspace-edit'),
    );
    await waitFor(() => {
      expect(editButton).toBeTruthy();
    });

    fireEvent.click(editButton);
    fireEvent.click(screen.getByText('Save'));
    await waitFor(() => {
      expect(saveViewPresetFn).toHaveBeenCalled();
    });
    expect(getViewPresets).toHaveBeenCalledTimes(2);

    // delete
    expect(deleteViewPresetFn).not.toHaveBeenCalled();
    fireEvent.click(screen.queryAllByTestId('viewpreset-listDeleteButton')[0]);
    await waitFor(() => {
      expect(screen.getByText('Delete map preset')).toBeTruthy();
    });

    fireEvent.click(screen.getByText('Delete'));
    await waitFor(() => {
      expect(deleteViewPresetFn).toHaveBeenCalled();
    });
    expect(getViewPresets).toHaveBeenCalledTimes(3);
  });

  it('should show viewpreset with changes if after deleting no layers remain', async () => {
    const store = createMockStoreForTest();

    const mosaicNode = {
      direction: 'row',
      first: 'screen1',
      second: 'screen2',
    } as const;
    const viewPresetTitle = 'My view preset 1';
    const screenConfig: WorkspacePreset = {
      id: 'preset1',
      title: 'workspace Preset 1',
      views: {
        allIds: ['screen1'],
        byId: {
          screen1: {
            id: 'test-screen-1',
            title: viewPresetTitle,
            componentType: 'Map',
            initialProps: {
              mapPreset: {
                layers: [],
              },
              syncGroupsIds: [],
            },
            scope: 'user',
          },
        },
      },
      scope: 'user',
      mosaicNode,
    };

    const mockViewPresets = [
      {
        id: 'screenConfigC',
        title: viewPresetTitle,
        date: '2022-06-01T12:34:27.787184',
        scope: 'user',
      },
      {
        title: 'Radar user',
        id: 'screenRadar',
        date: '2022-06-01T12:34:27.787184',
        scope: 'user',
      },
    ];

    const createMockApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> =>
          new Promise((resolve) => {
            resolve({
              data: mockViewPresets as ViewPresetListItem[],
            });
          }),
        getWorkspacePreset: () =>
          new Promise((resolve) => {
            resolve({
              data: screenConfig as unknown as WorkspacePresetFromBE,
            });
          }),
      };
    };
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );

    render(
      <WorkspaceWrapperProviderWithStore
        createApi={createMockApi}
        store={store}
      >
        <WorkspaceDetail componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await waitFor(() => {
      expect(store.getState().viewPresetsList.ids).toHaveLength(0);
    });

    await waitFor(() => {
      expect(store.getState().viewPresetsList.ids).toHaveLength(
        mockViewPresets.length,
      );
    });

    await waitFor(() => {
      expect(screen.getAllByText(viewPresetTitle)[0]).toBeTruthy();
    });

    // open view preset dialog
    fireEvent.click(screen.getAllByTestId('open-viewpresets')[0]);
    expect(screen.getByText('View presets menu')).toBeTruthy();

    // select user preset
    fireEvent.click(screen.getAllByText(mockViewPresets[0].title)[1]);
    await waitFor(() =>
      expect(screen.queryByText('View presets menu')).toBeFalsy(),
    );

    // open option menu
    const viewPresetOptionMenu = screen.getAllByTestId(viewPresetOptionId)[2];

    // delete
    expect(
      store.getState().viewPresets.entities.screen1.hasChanges,
    ).toBeFalsy();

    fireEvent.click(viewPresetOptionMenu);
    await waitFor(() => {
      expect(screen.getByText('Options')).toBeTruthy();
    });
    fireEvent.click(screen.getByText('Delete'));
    await waitFor(() => {
      expect(screen.queryByText('Options')).toBeFalsy();
    });

    fireEvent.click(screen.getByText('Delete'));

    await waitFor(() => {
      expect(
        screen.getByText(
          `Map preset "${mockViewPresets[0].title}" successfully deleted`,
        ),
      ).toBeTruthy();
    });
    expect(
      store.getState().viewPresets.entities.screen1.hasChanges,
    ).toBeTruthy();

    await waitFor(() => {
      expect(screen.getAllByText('New map preset')).toBeDefined();
    });
  });

  it('should fetch view presets and be able to select save, save as and delete them', async () => {
    const store = createMockStoreForTest();

    const mosaicNode = {
      direction: 'row',
      first: 'screen1',
      second: 'screen2',
    } as const;
    const viewPresetTitle = 'My view preset 1';
    const screenConfig: WorkspacePreset = {
      id: 'preset1',
      title: 'workspace Preset 1',
      views: {
        allIds: ['screen1'],
        byId: {
          screen1: {
            id: 'test-screen-1',
            title: viewPresetTitle,
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
            scope: 'user',
          },
        },
      },
      scope: 'user',
      mosaicNode,
    };

    const mockViewPresets = [
      {
        id: 'screenConfigC',
        title: viewPresetTitle,
        date: '2022-06-01T12:34:27.787184',
        scope: 'user',
      },
      {
        title: 'Radar user',
        id: 'screenRadar',
        date: '2022-06-01T12:34:27.787184',
        scope: 'user',
      },
    ];

    const createMockApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> =>
          new Promise((resolve) => {
            resolve({
              data: mockViewPresets as ViewPresetListItem[],
            });
          }),
        getWorkspacePreset: () =>
          new Promise((resolve) => {
            resolve({
              data: screenConfig as unknown as WorkspacePresetFromBE,
            });
          }),
      };
    };

    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );

    render(
      <WorkspaceWrapperProviderWithStore
        createApi={createMockApi}
        store={store}
      >
        <WorkspaceDetail componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await waitFor(() => {
      expect(store.getState().viewPresetsList.ids).toHaveLength(0);
    });

    await waitFor(() => {
      expect(store.getState().viewPresetsList.ids).toHaveLength(
        mockViewPresets.length,
      );
    });

    await waitFor(() => {
      expect(screen.getAllByText(viewPresetTitle)[0]).toBeTruthy();
    });

    // open view preset dialog
    fireEvent.click(screen.getAllByTestId('open-viewpresets')[0]);
    expect(screen.getByText('View presets menu')).toBeTruthy();

    // select user preset
    fireEvent.click(screen.getAllByText(mockViewPresets[0].title)[1]);
    await waitFor(() => {
      expect(screen.queryByText('View presets menu')).toBeFalsy();
    });

    // open option menu
    const viewPresetOptionMenu = screen.getAllByTestId(viewPresetOptionId)[2];
    fireEvent.click(viewPresetOptionMenu);
    await waitFor(() => {
      expect(screen.getByText('Options')).toBeTruthy();
    });

    // save
    fireEvent.click(screen.getByText('Save'));

    await waitFor(() => {
      expect(screen.queryByText('Options')).toBeFalsy();
    });

    expect(store.getState().workspace.scope).toEqual('user');

    await waitFor(() => {
      expect(
        screen.getByText(
          `Map preset "${mockViewPresets[0].title}" successfully saved`,
        ),
      ).toBeTruthy();
    });

    // save as
    fireEvent.click(viewPresetOptionMenu);
    await waitFor(() => {
      expect(screen.getByText('Options')).toBeTruthy();
    });
    fireEvent.click(screen.getByText('Save as'));
    await waitFor(() => {
      expect(screen.queryByText('Options')).toBeFalsy();
    });

    fireEvent.click(screen.getByText('Save'));
    await waitFor(() => {
      expect(
        screen.getByText(
          `New map preset "${mockViewPresets[0].title}" successfully saved`,
        ),
      ).toBeTruthy();
    });
    // edit
    fireEvent.click(viewPresetOptionMenu);
    await waitFor(() => {
      expect(screen.getByText('Options')).toBeTruthy();
    });
    fireEvent.click(
      screen.getByText(translateKeyOutsideComponents(i18n.t, 'workspace-edit')),
    );
    await waitFor(() => {
      expect(screen.queryByText('Options')).toBeFalsy();
    });

    fireEvent.click(screen.getByText('Save'));
    await waitFor(() => {
      expect(
        screen.getByText(
          `Map preset "${mockViewPresets[0].title}" successfully edited`,
        ),
      ).toBeTruthy();
    });
    // delete
    fireEvent.click(viewPresetOptionMenu);
    await waitFor(() => {
      expect(screen.getByText('Options')).toBeTruthy();
    });
    fireEvent.click(screen.getByText('Delete'));
    await waitFor(() => {
      expect(screen.queryByText('Options')).toBeFalsy();
    });

    fireEvent.click(screen.getByText('Delete'));

    await waitFor(() => {
      expect(
        screen.getByText(
          `Map preset "${mockViewPresets[0].title}" successfully deleted`,
        ),
      ).toBeTruthy();
    });

    await waitFor(() => {
      expect(screen.getAllByText('New map preset')).toBeTruthy();
    });
  });

  it('should show viewpreset with changes if after deleting still map layers remain', async () => {
    const store = createMockStoreForTest();

    const mosaicNode = {
      direction: 'row',
      first: 'screen1',
      second: 'screen2',
    } as const;
    const viewPresetTitle = 'My view preset 1';
    const screenConfig: WorkspacePreset = {
      id: 'preset1',
      title: 'workspace Preset 1',
      views: {
        allIds: ['screen1'],
        byId: {
          screen1: {
            id: 'test-screen-1',
            title: viewPresetTitle,
            componentType: 'Map',
            initialProps: {
              mapPreset: {
                layers: [
                  {
                    name: 'precipitation',
                    format: 'image/png',
                    enabled: true,
                    style: 'radar/nearest',
                    id: 'test_layerid_active',
                    layerType: 'mapLayer',
                  },
                ],
              },
              syncGroupsIds: [],
            },
            scope: 'user',
          },
        },
      },
      scope: 'user',
      mosaicNode,
    };

    const mockViewPresets = [
      {
        id: 'screenConfigC',
        title: viewPresetTitle,
        date: '2022-06-01T12:34:27.787184',
        scope: 'user',
      },
      {
        title: 'Radar user',
        id: 'screenRadar',
        date: '2022-06-01T12:34:27.787184',
        scope: 'user',
      },
    ];

    const createMockApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> =>
          new Promise((resolve) => {
            resolve({
              data: mockViewPresets as ViewPresetListItem[],
            });
          }),
        getWorkspacePreset: () =>
          new Promise((resolve) => {
            resolve({
              data: screenConfig as unknown as WorkspacePresetFromBE,
            });
          }),
      };
    };
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );

    render(
      <WorkspaceWrapperProviderWithStore
        createApi={createMockApi}
        store={store}
      >
        <WorkspaceDetail componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await waitFor(() => {
      expect(store.getState().viewPresetsList.ids).toHaveLength(0);
    });

    await waitFor(() => {
      expect(store.getState().viewPresetsList.ids).toHaveLength(
        mockViewPresets.length,
      );
    });

    await waitFor(() => {
      expect(screen.getAllByText(viewPresetTitle)[0]).toBeTruthy();
    });

    // open view preset dialog
    fireEvent.click(screen.getAllByTestId('open-viewpresets')[0]);
    expect(screen.getByText('View presets menu')).toBeTruthy();

    // select user preset
    fireEvent.click(screen.getAllByText(mockViewPresets[0].title)[1]);
    await waitFor(() => {
      expect(screen.queryByText('View presets menu')).toBeFalsy();
    });

    // open option menu
    const viewPresetOptionMenu = screen.getAllByTestId(viewPresetOptionId)[2];

    // delete
    expect(
      store.getState().viewPresets.entities.screen1.hasChanges,
    ).toBeFalsy();

    fireEvent.click(viewPresetOptionMenu);
    await waitFor(() => {
      expect(screen.getByText('Options')).toBeTruthy();
    });
    fireEvent.click(screen.getByText('Delete'));
    await waitFor(() => {
      expect(screen.queryByText('Options')).toBeFalsy();
    });

    fireEvent.click(screen.getByText('Delete'));

    await waitFor(() => {
      expect(
        screen.getByText(
          `Map preset "${mockViewPresets[0].title}" successfully deleted`,
        ),
      ).toBeTruthy();
    });
    expect(
      store.getState().viewPresets.entities.screen1.hasChanges,
    ).toBeTruthy();

    await waitFor(() => {
      expect(screen.getAllByText('New map preset')).toBeTruthy();
    });
  });
});
