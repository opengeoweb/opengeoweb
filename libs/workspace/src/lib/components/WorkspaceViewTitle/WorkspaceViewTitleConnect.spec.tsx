/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  screen,
  act,
} from '@testing-library/react';
import { timeSeriesActions } from '@opengeoweb/timeseries';
import { GEOWEB_ROLE_PRESETS_ADMIN } from '@opengeoweb/authentication';
import { WorkspaceViewTitleConnect } from './WorkspaceViewTitleConnect';
import { createApi as createFakeApi } from '../../utils/fakeApi';

import { WorkspaceWrapperProviderWithStore } from '../Providers';
import { PresetsApi } from '../../utils/api';
import { PresetAction } from '../../store/viewPresets/types';
import { viewPresetActions } from '../../store/viewPresets';
import { workspaceActions } from '../../store/workspace/reducer';
import { WorkspacePreset } from '../../store/workspace/types';
import { i18n, translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store/store';
import { viewPresetOptionId } from '../WorkspaceOptions/WorkspaceOptions';

describe('src/components/WorkspaceViewTitle/WorkspaceViewTitleConnect', () => {
  const mapId = 'mapId_123';
  const timeSeriesId = 'timeSeriesId_123';
  const workspacePreset: WorkspacePreset = {
    id: 'preset1',
    title: 'Preset 1',
    views: {
      allIds: [mapId, 'preset-2'],
      byId: {
        [mapId]: {
          id: 'test-screen-1',
          title: 'Observation',
          componentType: 'Map',
          initialProps: { mapPreset: {}, syncGroupsIds: [] },
          scope: 'user',
        },
      },
    },
    mosaicNode: mapId,
  };
  const workspacePresetTimeSeries: WorkspacePreset = {
    id: 'preset1',
    title: 'Preset 1',
    views: {
      allIds: [timeSeriesId, 'preset-2'],
      byId: {
        [timeSeriesId]: {
          id: 'test-screen-2',
          title: 'Time Series',
          componentType: 'TimeSeries',
          initialProps: {
            plotPreset: {
              mapId,
              plots: [],
              parameters: [],
            },
            services: [],
          },
          scope: 'user',
        },
      },
    },
    mosaicNode: timeSeriesId,
  };

  it('should save an existing preset when choosing Save', async () => {
    const store = createMockStore();

    const mockSave = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveViewPreset: mockSave,
      };
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <WorkspaceViewTitleConnect
          panelId={mapId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: mapId,
          panelId: mapId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(workspaceActions.setPreset({ workspacePreset })),
    );
    await screen.findByLabelText('Observation');
    fireEvent.click(await screen.findByTestId(viewPresetOptionId));

    await waitFor(() => {
      expect(screen.getByText('Save')).toBeTruthy();
    });

    fireEvent.click(screen.getByText('Save'), { exact: true });

    await screen.findByText('Map preset "Observation" successfully saved');

    await waitFor(() => {
      expect(mockSave).toHaveBeenCalledWith(mapId, expect.any(Object));
    });
  });

  it('should show all options except Reset as disabled when not logged in', async () => {
    const authAdminProps = {
      isLoggedIn: false,
      auth: {
        username: '',
        token: '',
        refresh_token: '',
      },
      onLogin: (): void => null!,
      onSetAuth: (): void => null!,
      sessionStorageProvider: null!,
    };
    const store = createMockStore();

    const mockSave = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveViewPreset: mockSave,
      };
    };

    render(
      <WorkspaceWrapperProviderWithStore
        store={store}
        createApi={createApi}
        auth={authAdminProps}
      >
        <WorkspaceViewTitleConnect
          panelId={mapId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: mapId,
          panelId: mapId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(workspaceActions.setPreset({ workspacePreset })),
    );
    await screen.findByLabelText('Observation');
    fireEvent.click(await screen.findByTestId(viewPresetOptionId));

    await waitFor(() => {
      expect(screen.getByText('Save')).toBeTruthy();
    });

    screen.queryAllByRole('menuitem').forEach((item) => {
      if (item.textContent === 'Reset') {
        expect(item.getAttribute('aria-disabled')).toBeFalsy();
      } else {
        expect(item.getAttribute('aria-disabled')).toBeTruthy();
      }
    });
  });

  it('should open viewpresetdialog when choosing delete', async () => {
    const store = createMockStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewTitleConnect
          panelId={mapId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: mapId,
          panelId: mapId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(workspaceActions.setPreset({ workspacePreset })),
    );
    await screen.findByLabelText('Observation');
    fireEvent.click(screen.getByTestId(viewPresetOptionId));

    fireEvent.click(await screen.findByText('Delete'), { exact: true });

    await waitFor(() =>
      expect(store.getState().viewPresets.viewPresetDialog).toEqual({
        title: translateKeyOutsideComponents(
          i18n.t,
          'workspace-mappreset-dialog-title-delete',
        ),
        action: PresetAction.DELETE,
        viewPresetId: mapId,
        panelId: mapId,
        formValues: { title: workspacePreset.views.byId![mapId].title },
      }),
    );
  });

  it('should open viewpresetdialog when choosing reset', async () => {
    const store = createMockStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewTitleConnect
          panelId={mapId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: mapId,
          panelId: mapId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(workspaceActions.setPreset({ workspacePreset })),
    );
    await screen.findByLabelText('Observation');
    fireEvent.click(screen.getByTestId(viewPresetOptionId));

    fireEvent.click(await screen.findByText('Reset'), { exact: true });

    await waitFor(() =>
      expect(store.getState().viewPresets.viewPresetDialog).toEqual({
        title: translateKeyOutsideComponents(i18n.t, 'workspace-reset-title'),
        action: PresetAction.RESET,
        viewPresetId: mapId,
        panelId: mapId,
        formValues: { title: workspacePreset.views.byId![mapId].title },
      }),
    );
  });

  it('should open viewpresetdialog when choosing save as (Map)', async () => {
    const store = createMockStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewTitleConnect
          panelId={mapId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: mapId,
          panelId: mapId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(workspaceActions.setPreset({ workspacePreset })),
    );
    await screen.findByLabelText('Observation');
    fireEvent.click(screen.getByTestId(viewPresetOptionId));

    await waitFor(() => {
      expect(screen.getByText('Save as')).toBeTruthy();
    });

    fireEvent.click(screen.getByText('Save as'), { exact: true });

    await waitFor(() =>
      expect(store.getState().viewPresets.viewPresetDialog).toEqual({
        title: translateKeyOutsideComponents(
          i18n.t,
          'workspace-mappreset-save-as',
        ),
        action: PresetAction.SAVE_AS,
        viewPresetId: mapId,
        panelId: mapId,
        formValues: {
          title: workspacePreset.views.byId![mapId].title,
          initialProps: { mapPreset: expect.any(Object), syncGroupsIds: [] },
        },
      }),
    );
  });

  it('should open viewpresetdialog when choosing save as (Map) for admin', async () => {
    const store = createMockStore();
    const authAdminProps = {
      isLoggedIn: true,
      auth: {
        username: 'user.name',
        token: '1223344',
        refresh_token: '33455214',
      },
      currentRole: GEOWEB_ROLE_PRESETS_ADMIN,
      onLogin: (): void => null!,
      onSetAuth: (): void => null!,
      sessionStorageProvider: null!,
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store} auth={authAdminProps}>
        <WorkspaceViewTitleConnect
          panelId={mapId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: mapId,
          panelId: mapId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(workspaceActions.setPreset({ workspacePreset })),
    );
    await screen.findByLabelText('Observation');
    fireEvent.click(screen.getByTestId(viewPresetOptionId));

    await waitFor(() => {
      expect(screen.getByText('Save as')).toBeTruthy();
    });

    fireEvent.click(screen.getByText('Save as'), { exact: true });

    await waitFor(() =>
      expect(store.getState().viewPresets.viewPresetDialog).toEqual({
        title: translateKeyOutsideComponents(
          i18n.t,
          'workspace-mappreset-save-as',
        ),
        action: PresetAction.SAVE_AS,
        viewPresetId: mapId,
        panelId: mapId,
        formValues: {
          title: workspacePreset.views.byId![mapId].title,
          initialProps: { mapPreset: expect.any(Object), syncGroupsIds: [] },
        },
      }),
    );
  });

  it('should open viewpresetdialog when choosing save as (TimeSeries)', async () => {
    const store = createMockStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewTitleConnect
          panelId={timeSeriesId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: timeSeriesId,
          panelId: timeSeriesId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(
        timeSeriesActions.registerTimeSeriesPreset({
          plotPreset: {
            mapId,
            plots: [],
            parameters: [],
          },
          services: [],
        }),
      ),
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: workspacePresetTimeSeries,
        }),
      ),
    );
    await screen.findByLabelText('Time Series');
    fireEvent.click(screen.getByTestId(viewPresetOptionId));

    await waitFor(() => {
      expect(screen.getByText('Save as')).toBeTruthy();
    });

    fireEvent.click(screen.getByText('Save as'), { exact: true });

    await waitFor(() =>
      expect(store.getState().viewPresets.viewPresetDialog).toEqual({
        title: translateKeyOutsideComponents(
          i18n.t,
          'workspace-mappreset-save-as',
        ),
        action: PresetAction.SAVE_AS,
        viewPresetId: timeSeriesId,
        panelId: timeSeriesId,
        formValues: {
          title: workspacePresetTimeSeries.views.byId![timeSeriesId].title,
          initialProps: { plotPreset: expect.any(Object), services: [] },
        },
      }),
    );
  });

  it('should show title', async () => {
    const store = createMockStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewTitleConnect
          panelId={mapId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: mapId,
          panelId: mapId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(workspaceActions.setPreset({ workspacePreset })),
    );
    expect(
      screen.getByText(workspacePreset.views.byId![mapId]!.title!),
    ).toBeTruthy();
  });

  it('should show title with changes for existing preset', async () => {
    const store = createMockStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewTitleConnect
          panelId={mapId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() => {
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: mapId,
          panelId: mapId,
        }),
      );
      store.dispatch(
        viewPresetActions.setViewPresetHasChanges({
          panelId: mapId,
          hasChanges: true,
        }),
      );
    });

    await act(() =>
      store.dispatch(workspaceActions.setPreset({ workspacePreset })),
    );

    expect(
      screen.getByText(
        `${workspacePreset.views.byId![mapId]!.title!} (${translateKeyOutsideComponents(i18n.t, 'workspace-modified-new')})`,
      ),
    ).toBeTruthy();
  });

  it('should show title with changes for new preset', async () => {
    const store = createMockStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewTitleConnect
          panelId={mapId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() => {
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: '',
          panelId: mapId,
        }),
      );
      store.dispatch(
        viewPresetActions.setViewPresetHasChanges({
          panelId: mapId,
          hasChanges: true,
        }),
      );
    });

    expect(
      screen.getByText(
        `${translateKeyOutsideComponents(i18n.t, 'workspace-mappreset-new')} (${translateKeyOutsideComponents(i18n.t, 'workspace-modified-existing')})`,
      ),
    ).toBeTruthy();
  });

  it('show new title if no title can be found', async () => {
    render(
      <WorkspaceWrapperProviderWithStore>
        <WorkspaceViewTitleConnect
          panelId={mapId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(
      screen.getByText(
        translateKeyOutsideComponents(i18n.t, 'workspace-mappreset-new'),
      ),
    ).toBeTruthy();
  });

  it('should edit an existing map preset when choosing edit', async () => {
    const store = createMockStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewTitleConnect
          panelId={mapId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: mapId,
          panelId: mapId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(workspaceActions.setPreset({ workspacePreset })),
    );
    await screen.findByLabelText('Observation');
    fireEvent.click(screen.getByTestId(viewPresetOptionId));

    await waitFor(() => {
      expect(
        screen.getByText(
          translateKeyOutsideComponents(i18n.t, 'workspace-edit'),
        ),
      ).toBeTruthy();
    });

    fireEvent.click(
      screen.getByText(translateKeyOutsideComponents(i18n.t, 'workspace-edit')),
      { exact: true },
    );

    await waitFor(() =>
      expect(store.getState().viewPresets.viewPresetDialog).toEqual({
        title: translateKeyOutsideComponents(
          i18n.t,
          'workspace-mappreset-dialog-title-edit',
        ),
        action: PresetAction.EDIT,
        viewPresetId: mapId,
        panelId: mapId,
        formValues: {
          title: workspacePreset.views.byId![mapId].title,
          initialProps: { mapPreset: expect.any(Object), syncGroupsIds: [] },
        },
      }),
    );
  });

  it('should edit an existing time series preset when choosing edit', async () => {
    const store = createMockStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewTitleConnect
          panelId={timeSeriesId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: timeSeriesId,
          panelId: timeSeriesId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(
        timeSeriesActions.registerTimeSeriesPreset({
          plotPreset: {
            mapId,
            plots: [],
            parameters: [],
          },
          services: [],
        }),
      ),
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: workspacePresetTimeSeries,
        }),
      ),
    );
    await screen.findByLabelText('Time Series');
    fireEvent.click(screen.getByTestId(viewPresetOptionId));

    await waitFor(() => {
      expect(
        screen.getByText(
          translateKeyOutsideComponents(i18n.t, 'workspace-edit'),
        ),
      ).toBeTruthy();
    });

    fireEvent.click(
      screen.getByText(translateKeyOutsideComponents(i18n.t, 'workspace-edit')),
      { exact: true },
    );

    await waitFor(() =>
      expect(store.getState().viewPresets.viewPresetDialog).toEqual({
        title: translateKeyOutsideComponents(
          i18n.t,
          'workspace-mappreset-dialog-title-edit',
        ),
        action: PresetAction.EDIT,
        viewPresetId: timeSeriesId,
        panelId: timeSeriesId,
        formValues: {
          title: workspacePresetTimeSeries.views.byId![timeSeriesId].title,
          initialProps: { plotPreset: expect.any(Object), services: [] },
        },
      }),
    );
  });
});
