/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import Box from '@mui/material/Box';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import React from 'react';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { WorkspaceOptions } from './WorkspaceOptions';
import { createMockStore } from '../../store';

export default {
  title: 'components/Workspace/WorkspaceOptions',
};

const emptyHandler = (): void => {};

export const WorkspaceOptionsLight = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      store={createMockStore()}
      theme={lightTheme}
    >
      <Box sx={{ padding: 1, height: 350, width: 260 }}>
        <WorkspaceOptions
          title="test title"
          isDefaultOpen
          handleSave={emptyHandler}
          handleSaveAs={emptyHandler}
          handleDelete={emptyHandler}
          handleEdit={emptyHandler}
          handleReset={emptyHandler}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

WorkspaceOptionsLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64007f6e01c60f40373838dd/version/6408ab735bfa6d1f591c2871',
    },
  ],
};
WorkspaceOptionsLight.tags = ['snapshot'];

export const WorkspaceOptionsDark = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      store={createMockStore()}
      theme={darkTheme}
    >
      <Box sx={{ padding: 1, height: 350, width: 260 }}>
        <WorkspaceOptions
          title="test title"
          isDefaultOpen
          handleSave={emptyHandler}
          handleSaveAs={emptyHandler}
          handleDelete={emptyHandler}
          handleEdit={emptyHandler}
          handleReset={emptyHandler}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

WorkspaceOptionsDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64107547f399ec0ea49b1cdc/version/64107547f399ec0ea49b1cdd',
    },
  ],
};
WorkspaceOptionsDark.tags = ['snapshot'];

export const WorkspaceOptionsLightAdmin = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      store={createMockStore()}
      theme={lightTheme}
    >
      <Box sx={{ padding: 1, height: 320, width: 260 }}>
        <WorkspaceOptions
          title="admin test title"
          isDefaultOpen
          isAdmin
          handleSaveAs={emptyHandler}
          handleReset={emptyHandler}
          handleDelete={emptyHandler}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

WorkspaceOptionsLightAdmin.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/667ac5892ce864ec98866808',
    },
  ],
};
WorkspaceOptionsLightAdmin.tags = ['snapshot'];

export const WorkspaceOptionsDarkAdmin = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      store={createMockStore()}
      theme={darkTheme}
    >
      <Box sx={{ padding: 1, height: 320, width: 260 }}>
        <WorkspaceOptions
          title="admin test title"
          isDefaultOpen
          isAdmin
          handleSaveAs={emptyHandler}
          handleReset={emptyHandler}
          handleDelete={emptyHandler}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

WorkspaceOptionsDarkAdmin.tags = ['snapshot'];
