/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import {
  act,
  fireEvent,
  render,
  waitFor,
  screen,
} from '@testing-library/react';
import { defaultDelay } from '@opengeoweb/shared';
import {
  viewPresetOptionId,
  WorkspaceOptions,
  WorkspaceOptionsProps,
} from './WorkspaceOptions';
import { WorkspaceI18nProvider } from '../Providers';
import { i18n, translateKeyOutsideComponents } from '../../utils/i18n';

describe('workspace/components/WorkspaceOptions', () => {
  it('should display passed title on screen', () => {
    const title = 'someTitle';
    render(
      <WorkspaceI18nProvider>
        <WorkspaceOptions title={title} />
      </WorkspaceI18nProvider>,
    );
    expect(screen.getByText('someTitle')).toBeTruthy();
  });

  it('should show options menu when clicked', () => {
    const title = 'someTitle';
    const props = {
      title,
      handleReset: jest.fn(),
    };
    render(
      <WorkspaceI18nProvider>
        <WorkspaceOptions {...props} />
      </WorkspaceI18nProvider>,
    );
    expect(screen.queryByRole('menu')).toBeFalsy();
    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    expect(screen.getAllByRole('menuitem').length).toBe(5);

    const resetMenuItem = screen.getByRole('menuitem', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-reset'),
    });
    fireEvent.click(resetMenuItem);
    expect(props.handleReset).toHaveBeenCalled();
    expect(screen.queryByRole('menu')).toBeFalsy();
  });

  it('should show options menu when clicked for admin', () => {
    const props: WorkspaceOptionsProps = {
      title: 'someTitle',
      isAdmin: true,
      handleSaveAs: jest.fn(),
      handleDelete: jest.fn(),
      handleReset: jest.fn(),
    };

    render(
      <WorkspaceI18nProvider>
        <WorkspaceOptions {...props} />
      </WorkspaceI18nProvider>,
    );
    expect(screen.queryByRole('menu')).toBeFalsy();
    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const handleSaveSystemPresetBtn = screen.getByText(
      translateKeyOutsideComponents(
        i18n.t,
        'workspace-admin-save-new-workspace',
      ),
    );
    expect(handleSaveSystemPresetBtn).toBeTruthy();
    expect(screen.getAllByRole('menuitem').length).toBe(4);

    fireEvent.click(handleSaveSystemPresetBtn);
    expect(props.handleSaveAs).toHaveBeenCalled();
    expect(screen.queryByRole('menu')).toBeFalsy();
  });

  it('should show options menu when clicked for admin inside view preset options', () => {
    const props: WorkspaceOptionsProps = {
      title: 'someTitle',
      isAdmin: true,
      handleDelete: jest.fn(),
      handleSave: jest.fn(),
      handleSaveAs: jest.fn(),
      handleEdit: jest.fn(),
      handleReset: jest.fn(),
      id: viewPresetOptionId,
    };

    render(
      <WorkspaceI18nProvider>
        <WorkspaceOptions {...props} />
      </WorkspaceI18nProvider>,
    );

    // delete
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const deleteMenuItem = screen.getByRole('menuitem', { name: 'Delete' });
    fireEvent.click(deleteMenuItem);
    expect(props.handleDelete).toHaveBeenCalled();
    expect(screen.queryByRole('menu')).toBeFalsy();

    // save
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    fireEvent.click(screen.getByRole('menuitem', { name: 'Save' }));
    expect(props.handleSave).toHaveBeenCalled();
    expect(screen.queryByRole('menu')).toBeFalsy();

    // save as
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    fireEvent.click(screen.getByRole('menuitem', { name: 'Save as' }));
    expect(props.handleSaveAs).toHaveBeenCalled();
    expect(screen.queryByRole('menu')).toBeFalsy();

    // Edit
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    fireEvent.click(
      screen.getByRole('menuitem', {
        name: translateKeyOutsideComponents(i18n.t, 'workspace-edit'),
      }),
    );
    expect(props.handleEdit).toHaveBeenCalled();
    expect(screen.queryByRole('menu')).toBeFalsy();

    // Reset
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    fireEvent.click(screen.getByRole('menuitem', { name: 'Reset' }));
    expect(props.handleReset).toHaveBeenCalled();
    expect(screen.queryByRole('menu')).toBeFalsy();
  });

  it('should show options menu when clicked for admin and no handlers given', () => {
    const props = {
      title: 'someTitle',
      isAdmin: true,
    };

    render(
      <WorkspaceI18nProvider>
        <WorkspaceOptions {...props} />
      </WorkspaceI18nProvider>,
    );
    expect(screen.queryByRole('menu')).toBeFalsy();
    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    expect(screen.getAllByRole('menuitem').length).toBe(4);

    const deleteMenuItem = screen.getByRole('menuitem', { name: 'Delete' });
    expect(deleteMenuItem.getAttribute('aria-disabled')).toBe('true');
  });

  it('should disable save option if no handler is given', () => {
    const title = 'someTitle';
    render(<WorkspaceOptions title={title} />);
    fireEvent.click(screen.getByRole('button'));
    const saveMenuItem = screen.getByRole('menuitem', { name: 'Save' });
    expect(saveMenuItem.getAttribute('aria-disabled')).toBe('true');
  });

  it('should enable save option if a handler is given', () => {
    const title = 'someTitle';
    const handleSave = jest.fn();
    render(<WorkspaceOptions title={title} handleSave={handleSave} />);
    fireEvent.click(screen.getByRole('button'));
    const saveMenuItem = screen.getByRole('menuitem', { name: 'Save' });
    expect(saveMenuItem.getAttribute('aria-disabled')).toBeFalsy();

    expect(handleSave).not.toHaveBeenCalled();
    fireEvent.click(saveMenuItem);
    expect(handleSave).toHaveBeenCalled();
  });

  it('should show a tooltip when hovering', async () => {
    const title = 'some title';
    jest.useFakeTimers();
    render(<WorkspaceOptions title={title} />);
    fireEvent.mouseOver(screen.getByRole('button'));
    act(() => {
      jest.advanceTimersByTime(defaultDelay);
    });
    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip.textContent).toBe(title);

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should close a tooltip when the menu is opened', async () => {
    const title = 'some title';
    jest.useFakeTimers();
    render(<WorkspaceOptions title={title} />);
    const button = screen.getByRole('button');
    fireEvent.mouseOver(button);
    act(() => {
      jest.advanceTimersByTime(defaultDelay);
    });
    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip).toBeTruthy();

    fireEvent.click(button);
    await waitFor(() => {
      expect(screen.queryByRole('tooltip')).toBeFalsy();
    });

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should show title in italic when the workspace has changes', () => {
    render(<WorkspaceOptions title="someTitle" hasChanges={true} />);
    const tab = screen.getByRole('button');
    const styles = getComputedStyle(tab!);
    expect(styles.fontStyle).toEqual('italic');
  });

  it('should show title not in italic when the workspace has no changes', () => {
    render(<WorkspaceOptions title="someTitle" hasChanges={false} />);
    const tab = screen.getByRole('button');
    const styles = getComputedStyle(tab!);
    expect(styles.fontStyle).not.toEqual('italic');
  });

  it('should show default no changes', () => {
    render(<WorkspaceOptions title="someTitle" />);
    const tab = screen.getByRole('button');
    const styles = getComputedStyle(tab!);
    expect(styles.fontStyle).not.toEqual('italic');
  });

  it('should show the button as disabled', () => {
    const props = {
      title: 'test',
      isDisabled: true,
    };
    render(<WorkspaceOptions {...props} />);
    const button = screen.getByRole('button');
    expect(button.getAttribute('disabled')).toBeDefined();
  });

  it('should show panelId if given', () => {
    const props = {
      title: 'test',
      isDisabled: true,
      panelId: 'test-panel-1',
    };
    render(<WorkspaceOptions {...props} />);
    expect(screen.getByText(props.panelId)).toBeTruthy();
  });

  it('should not show "Save (incl. viewpresets)" if prop handleSaveAll is not given', () => {
    const props: WorkspaceOptionsProps = {
      title: 'test',
      panelId: 'test-panel-1',
      isDefaultOpen: true,
    };
    render(<WorkspaceOptions {...props} />);
    const saveAllMenuItem = screen.queryByRole('menuitem', {
      name: 'Save (incl. viewpresets)',
    });
    expect(saveAllMenuItem).toBeFalsy();
  });

  it('should show "Edit (name and abstract)" and handle handleEdit', () => {
    const props: WorkspaceOptionsProps = {
      title: 'test',
      panelId: 'test-panel-1',
      handleEdit: jest.fn(),
      isDefaultOpen: true,
    };
    render(
      <WorkspaceI18nProvider>
        <WorkspaceOptions {...props} />
      </WorkspaceI18nProvider>,
    );
    const editMenuItem = screen.getByRole('menuitem', {
      name: 'Edit (name and abstract)',
    });
    expect(editMenuItem).toBeTruthy();
    expect(props.handleEdit).toHaveBeenCalledTimes(0);
    fireEvent.click(editMenuItem);
    expect(props.handleEdit).toHaveBeenCalledTimes(1);
  });

  it('should show "Edit (name)" for view presets and handle handleEdit', () => {
    const props: WorkspaceOptionsProps = {
      title: 'test',
      panelId: 'test-panel-1',
      handleEdit: jest.fn(),
      isDefaultOpen: true,
      id: viewPresetOptionId,
    };
    render(<WorkspaceOptions {...props} />);
    const editMenuItem = screen.getByRole('menuitem', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-edit'),
    });
    expect(editMenuItem).toBeTruthy();
    expect(props.handleEdit).toHaveBeenCalledTimes(0);
    fireEvent.click(editMenuItem);
    expect(props.handleEdit).toHaveBeenCalledTimes(1);
  });
});
