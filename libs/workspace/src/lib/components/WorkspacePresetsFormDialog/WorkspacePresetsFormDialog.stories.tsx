/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import Box from '@mui/material/Box';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import React from 'react';
import { createMockStore } from '../../store';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import WorkspacePresetsFormDialog from './WorkspacePresetsFormDialog';
import {
  WorkspacePresetAction,
  WorkspacePresetFromBE,
} from '../../store/workspace/types';
import {
  ViewPresetErrorComponent,
  ViewPresetErrorType,
} from '../../store/viewPresets/types';
import { ERROR_TITLE_BE } from './WorkspacePresetsForm';
import {
  WorkspaceFormValues,
  WorkspaceListErrorCategory,
  WorkspaceListErrorType,
} from '../../store/workspaceList/types';

export default {
  title: 'components/Workspace/WorkspaceFormDialog',
};

const emptyHandler = (): void => {};

const screenConfig: WorkspacePresetFromBE = {
  id: 'preset1',
  title: 'Radar with animating observations',
  views: [
    { mosaicNodeId: 'screen1', viewPresetId: 'view-1' },
    { mosaicNodeId: 'screen2', viewPresetId: 'view-2' },
  ],
  abstract: 'This is some abstract on radar and animating observations',
  mosaicNode: {
    direction: 'row',
    first: 'screen1',
    second: 'screen2',
  },
};

const testViews = [
  {
    mosaicNodeId: 'screen1',
    viewPresetId: 'already-existing',
    hasChanges: false,
    title: 'my existing system preset 1',
    isSelected: false,
    scope: 'system',
  },
  {
    mosaicNodeId: 'screen2',
    viewPresetId: 'already-existing-2',
    hasChanges: true,
    title: 'my existing system preset 2',
    isSelected: true,
    scope: 'system',
    abstract: 'test abstract',
  },
  {
    mosaicNodeId: 'screen3',
    viewPresetId: 'already-existing-3',
    hasChanges: true,
    title: 'my existing user preset 1',
    isSelected: true,
    scope: 'user',
  },
  {
    mosaicNodeId: 'screen4',
    viewPresetId: '',
    hasChanges: true,
    title: '',
    isSelected: true,
    scope: 'user',
  },
  {
    mosaicNodeId: 'screen5',
    viewPresetId: '',
    hasChanges: false,
    title: '',
    isSelected: false,
    scope: 'system',
  },
];

export const FormDialogLightSaveAs = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      store={createMockStore()}
      theme={lightTheme}
    >
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.SAVE_AS}
          presetId="preset1"
          formValues={{ ...screenConfig, views: testViews }}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

export const FormDialogDarkSaveAs = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      store={createMockStore()}
      theme={darkTheme}
    >
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.SAVE_AS}
          presetId="preset1"
          formValues={{ ...screenConfig, views: testViews }}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

export const FormDialogDarkDelete = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      store={createMockStore()}
      theme={darkTheme}
    >
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.DELETE}
          presetId="preset1"
          formValues={screenConfig}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

export const FormDialogLightDelete = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      store={createMockStore()}
      theme={lightTheme}
    >
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.DELETE}
          presetId="preset1"
          formValues={screenConfig}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

export const FormDialogLightDuplicate = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      store={createMockStore()}
      theme={lightTheme}
    >
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.DUPLICATE}
          presetId="preset1"
          formValues={screenConfig}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

export const FormDialogDarkDuplicate = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      store={createMockStore()}
      theme={darkTheme}
    >
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.DUPLICATE}
          presetId="preset1"
          formValues={screenConfig}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

FormDialogLightSaveAs.tags = ['snapshot'];

FormDialogDarkSaveAs.tags = ['snapshot'];

FormDialogLightDelete.tags = ['snapshot'];

FormDialogDarkDelete.tags = ['snapshot'];

export const FormDialogSaveSystemPreset = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore store={createMockStore()}>
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS}
          presetId="preset1"
          formValues={screenConfig}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

FormDialogSaveSystemPreset.parameters = {
  zeplinLink: [
    {
      name: 'FormDialogSaveSystemPresetNew',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/667ac5892ce864ec98866808',
    },
  ],
};

FormDialogSaveSystemPreset.tags = ['snapshot'];

export const FormDialogSaveSystemPresetDark = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      store={createMockStore()}
      theme={darkTheme}
    >
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS}
          presetId="preset1"
          formValues={screenConfig}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

FormDialogSaveSystemPresetDark.tags = ['snapshot'];

const viewsWithChanges = [
  {
    mosaicNodeId: 'screen1',
    viewPresetId: 'already-existing',
    hasChanges: true,
    title: 'my existing system preset 1',
    isSelected: true,
    scope: 'system',
  },
  {
    mosaicNodeId: 'screen2',
    viewPresetId: 'already-existing',
    hasChanges: false,
    title: 'my existing system preset 1',
    isSelected: true,
    scope: 'system',
  },
  {
    mosaicNodeId: 'screen3',
    viewPresetId: '',
    hasChanges: true,
    title: '',
    isSelected: true,
    scope: 'system',
    abstract: 'test abstract',
  },
  {
    mosaicNodeId: 'screen4',
    viewPresetId: '',
    hasChanges: true,
    title: '',
    isSelected: false,
    scope: 'system',
  },
];

export const FormDialogSaveSystemPresetChanges = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore store={createMockStore()}>
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS}
          presetId="preset1"
          formValues={{
            ...screenConfig,
            views: viewsWithChanges,
          }}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

FormDialogSaveSystemPresetChanges.tags = ['snapshot'];

export const FormDialogSaveSystemPresetChangesDark = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      store={createMockStore()}
      theme={darkTheme}
    >
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS}
          presetId="preset1"
          formValues={{
            ...screenConfig,
            views: viewsWithChanges,
          }}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

FormDialogSaveSystemPresetChangesDark.tags = ['snapshot'];

const viewsWithErrors = [
  {
    mosaicNodeId: 'screen1',
    viewPresetId: 'already-existing',
    hasChanges: true,
    title: 'my existing system preset 1',
    isSelected: true,
    error: {
      message: 'Something went wrong',
      component: ViewPresetErrorComponent.PRESET_LIST,
      errorType: ViewPresetErrorType.GENERIC,
    },
  },
  {
    mosaicNodeId: 'screen2',
    viewPresetId: 'already-existing',
    hasChanges: false,
    title: 'my existing system preset 1',
    isSelected: true,
  },
  {
    mosaicNodeId: 'screen3',
    viewPresetId: '',
    hasChanges: true,
    title: '',
    isSelected: true,
    abstract: 'test abstract',
  },
  {
    mosaicNodeId: 'screen4',
    viewPresetId: '',
    hasChanges: true,
    title: '',
    isSelected: true,
    error: {
      message: 'Name already exists',
      component: ViewPresetErrorComponent.PRESET_LIST,
      errorType: ViewPresetErrorType.GENERIC,
    },
  },
];

export const FormDialogSaveSystemPresetErrors = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore store={createMockStore()}>
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS}
          presetId="preset1"
          formValues={{
            ...screenConfig,
            views: viewsWithErrors,
          }}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
          isWorkspaceSaved
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

FormDialogSaveSystemPresetErrors.tags = ['snapshot'];

export const FormDialogSaveSystemPresetErrorsDark = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      store={createMockStore()}
      theme={darkTheme}
    >
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS}
          presetId="preset1"
          formValues={{
            ...screenConfig,
            views: viewsWithErrors,
          }}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
          isWorkspaceSaved
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

FormDialogSaveSystemPresetErrorsDark.tags = ['snapshot'];

export const FormDialogSaveSystemPresetChangesUnchecked =
  (): React.ReactElement => {
    return (
      <WorkspaceWrapperProviderWithStore store={createMockStore()}>
        <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
          <WorkspacePresetsFormDialog
            action={WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS}
            presetId="preset1"
            formValues={{
              ...screenConfig,
              views: [
                {
                  mosaicNodeId: 'screen1',
                  viewPresetId: 'already-existing',
                  hasChanges: true,
                  title: 'my existing system preset 1',
                  isSelected: false,
                  scope: 'system',
                },
                {
                  mosaicNodeId: 'screen2',
                  viewPresetId: 'already-existing',
                  hasChanges: false,
                  title: 'my existing system preset 1',
                  isSelected: false,
                  scope: 'system',
                },
                {
                  mosaicNodeId: 'screen3',
                  viewPresetId: '',
                  hasChanges: true,
                  title: '',
                  isSelected: false,
                  scope: 'system',
                },
                {
                  mosaicNodeId: 'screen4',
                  viewPresetId: '',
                  hasChanges: true,
                  title: '',
                  isSelected: false,
                  scope: 'system',
                },
              ],
            }}
            isOpen
            onFormSubmit={emptyHandler}
            onClose={emptyHandler}
            isLoading={false}
          />
        </Box>
      </WorkspaceWrapperProviderWithStore>
    );
  };

export const FormDialogSaveSystemPresetWorkspaceError =
  (): React.ReactElement => {
    return (
      <WorkspaceWrapperProviderWithStore store={createMockStore()}>
        <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
          <WorkspacePresetsFormDialog
            action={WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS}
            presetId="preset1"
            formValues={
              {
                id: 'preset1',
                title: 'Radar',
                abstract: 'This is some abstract on radar',
                mosaicNode: 'screen1',
                views: [
                  {
                    mosaicNodeId: 'screen1',
                    viewPresetId: 'view3939923',
                    hasChanges: false,
                    title: 'my existing system preset',
                    isSelected: true,
                    scope: 'system',
                  },
                ],
              } as WorkspaceFormValues
            }
            isOpen
            onFormSubmit={emptyHandler}
            onClose={emptyHandler}
            isLoading={false}
            error={{
              type: WorkspaceListErrorType.VERBATIM_ERROR,
              message: 'Saving workspace failed, try again',
              category: WorkspaceListErrorCategory.GENERIC,
            }}
          />
        </Box>
      </WorkspaceWrapperProviderWithStore>
    );
  };

FormDialogSaveSystemPresetWorkspaceError.tags = ['snapshot'];

export const FormDialogSaveWorkspaceTitleError = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore store={createMockStore()}>
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS}
          presetId="preset1"
          formValues={
            {
              id: 'preset1',
              title: 'Radar',
              abstract: 'This is some abstract on radar',
              mosaicNode: 'screen1',
              views: [
                {
                  mosaicNodeId: 'screen1',
                  viewPresetId: '',
                  title: 'my new system preset',
                  hasChanges: true,
                  isSelected: false,
                  scope: 'system',
                },
              ],
            } as WorkspaceFormValues
          }
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
          error={{
            type: WorkspaceListErrorType.VERBATIM_ERROR,
            message: ERROR_TITLE_BE,
            category: WorkspaceListErrorCategory.GENERIC,
          }}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

FormDialogSaveWorkspaceTitleError.tags = ['snapshot'];

export const FormDialogDeleteSystemPresetLight = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore store={createMockStore()}>
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.DELETE_SYSTEM_PRESET}
          presetId="preset1"
          formValues={screenConfig}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

FormDialogDeleteSystemPresetLight.parameters = {
  zeplinLink: [
    {
      name: 'light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/667ac70ad0da3162e0ec43ce',
    },
  ],
};

FormDialogDeleteSystemPresetLight.tags = ['snapshot'];

export const FormDialogDeleteSystemPresetDark = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      store={createMockStore()}
      theme={darkTheme}
    >
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.DELETE_SYSTEM_PRESET}
          presetId="preset1"
          formValues={screenConfig}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

FormDialogDeleteSystemPresetDark.tags = ['snapshot'];
