/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import {
  defaultFormOptions,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';

import WorkspacePresetsForm, {
  emptyWorkspacePreset,
} from './WorkspacePresetsForm';
import { WorkspacePresetAction } from '../../store/workspace/types';
import {
  VerbatimWorkspaceListError,
  WorkspaceListErrorCategory,
  WorkspaceListErrorType,
} from '../../store/workspaceList/types';
import { DemoWrapper } from '../Providers';
import { i18n, translateKeyOutsideComponents } from '../../utils/i18n';

describe('components/WorkspacePresetsDialog/WorkspacePresetsForm', () => {
  it('should work with default props', async () => {
    const props = {
      formValues: {
        ...emptyWorkspacePreset,
        title: 'test title',
        abstract: 'this abstract',
      },
      action: WorkspacePresetAction.DUPLICATE,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <WorkspacePresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    const input = screen.getAllByRole('textbox')[0];

    expect(input.matches(':focus')).toBeTruthy();
    expect(input!.getAttribute('value')).toEqual(props.formValues.title);
    expect(screen.getAllByRole('textbox')[1]!.textContent).toEqual(
      props.formValues.abstract,
    );
    expect(screen.getByTestId('scope').getAttribute('value')).toEqual('user');

    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();

    const abstractInput = screen.getByRole('textbox', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-abstract'),
    });

    fireEvent.change(abstractInput, { target: { value: 'new abstract' } });
    await waitFor(() => {
      expect(abstractInput.textContent).toEqual('new abstract');
    });
  });

  it('should show form for save as action', () => {
    const props = {
      formValues: {
        ...emptyWorkspacePreset,
        title: 'test title',
        abstract: 'this abstract',
      },
      action: WorkspacePresetAction.SAVE_AS,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <WorkspacePresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();
  });

  it('should not show form for delete action', () => {
    const props = {
      formValues: {
        ...emptyWorkspacePreset,
        title: 'test title',
        abstract: 'this abstract',
      },
      action: WorkspacePresetAction.DELETE,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <WorkspacePresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    expect(screen.queryByTestId('workspace-preset-form')).toBeFalsy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents(
          i18n.t,
          'workspace-dialog-confirm-delete-message',
          {
            TITLE: props.formValues.title,
          },
        ),
      ),
    ).toBeTruthy();
  });

  it('should validate required', async () => {
    const props = {
      formValues: {
        title: 'test title',
      },
      action: WorkspacePresetAction.DUPLICATE,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <WorkspacePresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    const input = screen.getAllByRole('textbox')[0];

    expect(input.matches(':focus')).toBeTruthy();
    expect(input!.getAttribute('value')).toEqual(props.formValues.title);
    expect(screen.queryByRole('alert')).toBeFalsy();

    fireEvent.change(input!, { target: { value: '' } });
    await waitFor(() => {
      expect(screen.getByRole('alert')).toBeTruthy();
    });

    fireEvent.change(input!, { target: { value: 'new value' } });
    await waitFor(() => {
      expect(screen.queryByRole('alert')).toBeFalsy();
    });
  });

  it('should show error when title only contains spaces', async () => {
    const props = {
      formValues: {
        title: '',
      },
      action: WorkspacePresetAction.DUPLICATE,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <WorkspacePresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    const input = screen.getAllByRole('textbox')[0];
    fireEvent.change(input!, { target: { value: '  ' } });
    await waitFor(() => {
      expect(screen.getByRole('alert')).toBeTruthy();
    });
  });

  it('should show error', async () => {
    const errorMessage = 'test could not save preset';
    const error: VerbatimWorkspaceListError = {
      type: WorkspaceListErrorType.VERBATIM_ERROR,
      message: errorMessage,
      category: WorkspaceListErrorCategory.GENERIC,
    };
    const props = {
      formValues: {
        title: 'test title',
      },
      error,
      action: WorkspacePresetAction.DUPLICATE,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <WorkspacePresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    expect(await screen.findByText(errorMessage)).toBeTruthy();
  });

  it('should submit the form when submitting the title field', async () => {
    const props = {
      formValues: {
        title: 'test submit on enter',
      },
      onSubmit: jest.fn(),
      action: WorkspacePresetAction.DUPLICATE,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <WorkspacePresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    const input = screen.getAllByRole('textbox')[0];
    expect(input).toBeTruthy();
    fireEvent.submit(input!);
    await waitFor(() => {
      expect(props.onSubmit).toHaveBeenCalled();
    });
  });

  it('should show input fields for Edit (title and abstract)', async () => {
    const props = {
      formValues: {
        title: 'edited title',
      },
      onSubmit: jest.fn(),
      action: WorkspacePresetAction.EDIT,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <WorkspacePresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    const nameInput = screen.getByRole('textbox', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
    });
    const abstractInput = screen.getByRole('textbox', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-abstract'),
    });

    expect(nameInput).toBeTruthy();
    expect(nameInput.getAttribute('disabled')).toBeNull();

    expect(abstractInput).toBeTruthy();
    expect(abstractInput.getAttribute('disabled')).toBeNull();
  });

  it('should show fields for viewpresets with changes when saving as', async () => {
    const props = {
      formValues: {
        title: 'edited title',
        views: [
          {
            mosaicNodeId: 'screen1',
            viewPresetId: 'already-existing',
            hasChanges: true,
            scope: 'system' as const,
            title: 'my existing system preset 1',
            isSelected: true,
          },
          {
            mosaicNodeId: 'screen2',
            viewPresetId: 'already-existing',
            hasChanges: true,
            scope: 'user' as const,
            title: 'my existing user preset 1',
            isSelected: true,
          },
          {
            mosaicNodeId: 'screen3',
            viewPresetId: '',
            hasChanges: true,
            scope: 'system' as const,
            title: '',
            isSelected: true,
            abstract: 'this is some abstract',
          },
          {
            mosaicNodeId: 'screen4',
            viewPresetId: '',
            hasChanges: true,
            scope: 'system' as const,
            title: '',
            isSelected: false,
          },
        ],
      },
      onSubmit: jest.fn(),
      action: WorkspacePresetAction.SAVE_AS,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <WorkspacePresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    expect(
      screen.getByText(
        translateKeyOutsideComponents(
          i18n.t,
          'workspace-save-new-viewpreset-warning-user',
        ),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents(
          i18n.t,
          'workspace-save-new-viewpreset-warning-system',
        ),
      ),
    ).toBeTruthy();
  });

  it('should show admin fields if viewpresets with changes', async () => {
    const props = {
      formValues: {
        title: 'edited title',
        views: [
          {
            mosaicNodeId: 'screen1',
            viewPresetId: 'already-existing',
            hasChanges: true,
            scope: 'system' as const,
            title: 'my existing system preset 1',
            isSelected: true,
          },
          {
            mosaicNodeId: 'screen2',
            viewPresetId: 'already-existing',
            hasChanges: false,
            scope: 'system' as const,
            title: 'my existing system preset 1',
            isSelected: true,
          },
          {
            mosaicNodeId: 'screen3',
            viewPresetId: '',
            hasChanges: true,
            scope: 'system' as const,
            title: '',
            isSelected: true,
            abstract: 'this is some abstract',
          },
          {
            mosaicNodeId: 'screen4',
            viewPresetId: '',
            hasChanges: true,
            scope: 'system' as const,
            title: '',
            isSelected: false,
          },
        ],
      },
      onSubmit: jest.fn(),
      action: WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <WorkspacePresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    expect(
      screen.getByText(
        translateKeyOutsideComponents(
          i18n.t,
          'workspace-admin-save-new-viewpreset',
        ),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents(
          i18n.t,
          'workspace-admin-save-new-viewpreset-warning',
        ),
      ),
    ).toBeTruthy();
  });

  it('should show admin fields when saving a workspace', async () => {
    const props = {
      formValues: {
        title: 'edited title',
        views: [
          {
            mosaicNodeId: 'screen1',
            viewPresetId: 'already-existing',
            hasChanges: true,
            scope: 'system' as const,
            title: 'my existing system preset 1',
            isSelected: true,
          },
          {
            mosaicNodeId: 'screen2',
            viewPresetId: 'already-existing',
            hasChanges: false,
            scope: 'system' as const,
            title: 'my existing system preset 1',
            isSelected: true,
          },
          {
            mosaicNodeId: 'screen3',
            viewPresetId: '',
            hasChanges: true,
            scope: 'system' as const,
            title: '',
            isSelected: true,
            abstract: 'this is some abstract',
          },
          {
            mosaicNodeId: 'screen4',
            viewPresetId: '',
            hasChanges: true,
            scope: 'system' as const,
            title: '',
            isSelected: false,
          },
        ],
      },
      onSubmit: jest.fn(),
      action: WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <WorkspacePresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    expect(
      screen.getByText(
        translateKeyOutsideComponents(
          i18n.t,
          'workspace-admin-save-new-viewpreset',
        ),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents(
          i18n.t,
          'workspace-admin-save-new-viewpreset-warning',
        ),
      ),
    ).toBeTruthy();

    const nameInput = screen.queryAllByRole('textbox', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
    });
    expect(nameInput).toBeTruthy();
    const abstractInput = screen.queryAllByRole('textbox', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-abstract'),
    });
    expect(abstractInput).toBeTruthy();
  });

  it('should show admin fields when saving a workspace', async () => {
    const props = {
      formValues: {
        title: 'edited title',
        views: [
          {
            mosaicNodeId: 'screen1',
            viewPresetId: 'already-existing',
            hasChanges: true,
            scope: 'user' as const,
            title: 'my existing system preset 1',
            isSelected: true,
          },
          {
            mosaicNodeId: 'screen2',
            viewPresetId: 'already-existing',
            hasChanges: false,
            scope: 'user' as const,
            title: 'my existing system preset 1',
            isSelected: true,
          },
          {
            mosaicNodeId: 'screen3',
            viewPresetId: '',
            hasChanges: true,
            scope: 'user' as const,
            title: '',
            isSelected: true,
            abstract: 'this is some abstract',
          },
          {
            mosaicNodeId: 'screen4',
            viewPresetId: '',
            hasChanges: true,
            scope: 'user' as const,
            title: '',
            isSelected: false,
          },
        ],
      },
      onSubmit: jest.fn(),
      action: WorkspacePresetAction.SAVE_SYSTEM_PRESET,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <WorkspacePresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    expect(
      screen.getByText(
        translateKeyOutsideComponents(
          i18n.t,
          'workspace-admin-save-new-viewpreset',
        ),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents(
          i18n.t,
          'workspace-admin-save-new-viewpreset-warning',
        ),
      ),
    ).toBeTruthy();

    const nameInput = screen.queryAllByRole('textbox', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
    });
    expect(nameInput).toBeTruthy();
    const abstractInput = screen.queryAllByRole('textbox', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-abstract'),
    });
    expect(abstractInput).toBeTruthy();
  });

  it('should show admin fields when saving a user workspace', async () => {
    const props = {
      formValues: {
        title: 'edited title',
        views: [
          {
            mosaicNodeId: 'screen1',
            viewPresetId: 'already-existing',
            hasChanges: true,
            scope: 'user' as const,
            title: 'my existing system preset 1',
            isSelected: true,
          },
          {
            mosaicNodeId: 'screen2',
            viewPresetId: 'already-existing',
            hasChanges: false,
            scope: 'system' as const,
            title: 'my existing system preset 1',
            isSelected: true,
          },
          {
            mosaicNodeId: 'screen3',
            viewPresetId: '',
            hasChanges: true,
            scope: 'user' as const,
            title: '',
            isSelected: true,
            abstract: 'this is some abstract',
          },
          {
            mosaicNodeId: 'screen4',
            viewPresetId: '',
            hasChanges: true,
            scope: 'system' as const,
            title: '',
            isSelected: false,
          },
        ],
      },
      onSubmit: jest.fn(),
      action: WorkspacePresetAction.SAVE,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <WorkspacePresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    expect(
      screen.getByText(
        translateKeyOutsideComponents(i18n.t, 'workspace-save-new-viewpreset'),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents(
          i18n.t,
          'workspace-save-new-viewpreset-warning-user',
        ),
      ),
    ).toBeTruthy();

    const nameInput = screen.queryAllByRole('textbox', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
    });
    expect(nameInput).toBeTruthy();
    const abstractInput = screen.queryAllByRole('textbox', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-abstract'),
    });
    expect(abstractInput).toBeTruthy();
  });

  it('should not show form for reset action', () => {
    const props = {
      formValues: {
        ...emptyWorkspacePreset,
        title: 'test title',
        abstract: 'this abstract',
      },
      action: WorkspacePresetAction.RESET,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <WorkspacePresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    expect(screen.queryByTestId('workspace-preset-form')).toBeFalsy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents(
          i18n.t,
          'workspace-dialog-confirm-reset-personal-message',
          {
            TITLE: props.formValues.title,
          },
        ),
      ),
    ).toBeTruthy();
  });

  it('should disable inputs when form is loading', async () => {
    const props = {
      formValues: {
        title: 'edited title',
        views: [
          {
            mosaicNodeId: 'screen1',
            viewPresetId: 'already-existing',
            hasChanges: true,
            scope: 'system' as const,
            title: 'my existing system preset 1',
            isSelected: true,
          },
          {
            mosaicNodeId: 'screen2',
            viewPresetId: 'already-existing',
            hasChanges: false,
            scope: 'system' as const,
            title: 'my existing system preset 2',
            isSelected: true,
          },
          {
            mosaicNodeId: 'screen3',
            viewPresetId: '',
            hasChanges: true,
            scope: 'system' as const,
            title: '',
            isSelected: true,
            abstract: 'this is some abstract',
          },
          {
            mosaicNodeId: 'screen4',
            viewPresetId: '',
            hasChanges: true,
            scope: 'system' as const,
            title: '',
            isSelected: false,
          },
        ],
      },
      onSubmit: jest.fn(),
      action: WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS,
      isLoading: true,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <WorkspacePresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    const nameInputs = screen.getAllByRole('textbox', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
    });

    const abstractInputs = screen.getAllByRole('textbox', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-abstract'),
    });
    abstractInputs.forEach((input) => {
      expect(input.getAttribute('disabled')).not.toBeNull();
    });

    expect(abstractInputs).toBeTruthy();

    nameInputs.forEach((nameInput) => {
      expect(nameInput.getAttribute('disabled')).not.toBeNull();
    });

    expect(abstractInputs).toBeTruthy();
  });

  it('should disable workspace inputs when workspace already has been saved', async () => {
    const props = {
      formValues: {
        title: 'edited title',
        views: [
          {
            mosaicNodeId: 'screen1',
            viewPresetId: 'already-existing',
            hasChanges: true,
            title: 'my existing system preset 1',
            isSelected: true,
            scope: 'system' as const,
          },
          {
            mosaicNodeId: 'screen2',
            viewPresetId: 'already-existing',
            hasChanges: false,
            title: 'my existing system preset 2',
            isSelected: true,
            scope: 'system' as const,
          },
          {
            mosaicNodeId: 'screen3',
            viewPresetId: '',
            hasChanges: true,
            title: '',
            isSelected: true,
            scope: 'system' as const,
          },
          {
            mosaicNodeId: 'screen4',
            viewPresetId: '',
            hasChanges: true,
            title: '',
            isSelected: true,
            scope: 'system' as const,
          },
        ],
      },
      onSubmit: jest.fn(),
      action: WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS,
      hasSavedWorkspace: true,
    };

    render(
      <DemoWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
          }}
        >
          <WorkspacePresetsForm {...props} />
        </ReactHookFormProvider>
      </DemoWrapper>,
    );

    const nameInputs = screen.getAllByRole('textbox', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
    });
    nameInputs.forEach((nameInput) => {
      if (nameInput.getAttribute('name') === 'title') {
        expect(nameInput.getAttribute('disabled')).not.toBeNull();
      } else {
        expect(nameInput.getAttribute('disabled')).toBeNull();
      }
    });

    const abstractInput = screen.getAllByRole('textbox', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-abstract'),
    });
    abstractInput.forEach((input, index) => {
      // only the main abstract is disabled
      if (index === 0) {
        expect(input.getAttribute('disabled')).not.toBeNull();
      } else {
        expect(input.getAttribute('disabled')).toBeNull();
      }
    });

    expect(abstractInput).toBeTruthy();
  });
});
