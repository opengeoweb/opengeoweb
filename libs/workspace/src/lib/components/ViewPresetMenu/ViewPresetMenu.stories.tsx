/* *
/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { Box } from '@mui/material';
import { darkTheme } from '@opengeoweb/theme';
import { WorkspaceWrapperProviderWithStore } from '../Providers';
import { ViewPresetMenu } from './ViewPresetMenu';
import { viewPresetActions } from '../../store/viewPresets';
import { createMockStore } from '../../store';

export default {
  title: 'components/ViewPresets/ViewPresetMenu',
};

const store = createMockStore();

const ViewPresetMenuDemo = (): React.ReactElement => {
  store.dispatch(
    viewPresetActions.registerViewPreset({
      viewPresetId: 'radar',
      panelId: 'test-1',
    }),
  );
  store.dispatch(
    viewPresetActions.registerViewPreset({
      viewPresetId: 'harm',
      panelId: 'test-1',
    }),
  );
  store.dispatch(
    viewPresetActions.setActiveViewPresetId({
      panelId: 'test-1',
      viewPresetId: '',
    }),
  );
  store.dispatch(
    viewPresetActions.fetchedViewPresets({
      panelId: 'test-1',
      viewPresets: [
        {
          title: 'Radar custom',
          id: 'radar',
          date: '',
          scope: 'user',
        },
        {
          title: 'Harmonie',
          id: 'harm',
          date: '',
          scope: 'system',
        },
      ],
      filterParams: {},
    }),
  );
  return (
    <Box
      sx={{
        width: '400px',
        height: '100vh',
        backgroundColor: 'geowebColors.background.surfaceApp',
        position: 'relative',
      }}
    >
      <ViewPresetMenu panelId="test-1" closeMenu={(): void => {}} isOpen />
    </Box>
  );
};

const zeplinLink = [
  {
    name: 'View preset flow',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6405b9253a7786669f49ed78/version/64897b7b4e80562697b8c312',
  },
];

export const ViewPresetMenuLight = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore store={store}>
      <ViewPresetMenuDemo />
    </WorkspaceWrapperProviderWithStore>
  );
};

ViewPresetMenuLight.parameters = {
  zeplinLink,
};
ViewPresetMenuLight.tags = ['snapshot'];

export const ViewPresetMenuDark = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore store={store} theme={darkTheme}>
      <ViewPresetMenuDemo />
    </WorkspaceWrapperProviderWithStore>
  );
};

ViewPresetMenuDark.parameters = {
  zeplinLink,
};
ViewPresetMenuDark.tags = ['snapshot'];
