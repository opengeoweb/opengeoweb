/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';

import { CustomDialog, ToolContainer } from '@opengeoweb/shared';
import { WorkspaceSelectListConnect } from '../WorkspaceSelectList/WorkspaceSelectListConnect';
import { useWorkspaceTranslation } from '../../utils/i18n';

export interface WorkspaceMenuProps {
  closeMenu?: () => void;
  isOpen?: boolean;
}

export const WorkspaceMenu: React.FC<WorkspaceMenuProps> = ({
  isOpen = false,
  closeMenu,
}: WorkspaceMenuProps) => {
  const { t } = useWorkspaceTranslation();
  return (
    <CustomDialog
      open={isOpen}
      onClose={(): void => {
        if (closeMenu) {
          closeMenu();
        }
      }}
      isResizable
      hasLayout={false}
      resizableDefaultSize={{ width: 380, height: '90%' }}
      slotProps={{
        backdrop: {
          style: {
            top: 40,
          },
        },
        root: {
          style: {
            top: 40,
          },
        },
      }}
      data-testid="workspace-menu"
    >
      <ToolContainer
        title={t('workspace-menu')}
        onClose={closeMenu}
        style={{ width: '100%', height: '100%' }}
        onClick={(event): void => {
          event.stopPropagation();
        }}
      >
        <WorkspaceSelectListConnect />
      </ToolContainer>
    </CustomDialog>
  );
};
