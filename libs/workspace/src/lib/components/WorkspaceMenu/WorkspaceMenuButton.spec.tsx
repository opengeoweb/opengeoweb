/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';
import { WorkspaceMenuButton } from './WorkspaceMenuButton';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';

describe('WorkspaceMenuButton', () => {
  it('should render correctly', async () => {
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme}>
        <WorkspaceMenuButton />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(await screen.findByTestId('workspaceMenuButton')).toBeTruthy();
  });

  it('should close WorkspaceMenu', async () => {
    const props = {
      showMenu: true,
      onToggleDialog: jest.fn(),
    };

    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme}>
        <WorkspaceMenuButton {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(screen.getByTestId('workspaceMenuButton'));
    expect(props.onToggleDialog).toHaveBeenCalledWith(false);

    fireEvent.click(await screen.findByTestId('closeBtn'));
    expect(props.onToggleDialog).toHaveBeenCalledWith(false);
  });

  it('should open WorkspaceMenu for initial closed menu', async () => {
    const props = {
      showMenu: false,
      onToggleDialog: jest.fn(),
    };

    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme}>
        <WorkspaceMenuButton {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(screen.getByTestId('workspaceMenuButton'));
    expect(props.onToggleDialog).toHaveBeenCalledWith(true);
  });
});
