/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { Provider } from 'react-redux';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { ComponentsLookUpWrapper } from './ComponentsLookUpWrapper';
import { workspaceActions } from '../../store/workspace/reducer';
import { createMockStore } from '../../store';

interface TestModuleComponentProps {
  onPreventCloseView?: (on: boolean) => void;
}

const TestModuleComponent: React.FC<TestModuleComponentProps> = ({
  onPreventCloseView = (): void => {},
}: TestModuleComponentProps) => {
  const [shouldPreventClose, togglePreventClose] =
    React.useState<boolean>(true);
  const onClick = (): void => {
    onPreventCloseView(shouldPreventClose);
    togglePreventClose(!shouldPreventClose);
  };
  const testButton = 'test button';

  return (
    <div>
      <button type="button" onClick={onClick}>
        {testButton}
      </button>
    </div>
  );
};

describe('workspace/components/ComponentsLookUpWrapper', () => {
  it('should wrap a component and fire default actions', async () => {
    const store = createMockStore();

    const testId = 'test-1';
    const testPayload = {
      id: testId,
      componentType: 'test',
      initialProps: {},
    };

    store.dispatch(workspaceActions.addWorkspaceView(testPayload));

    render(
      <Provider store={store}>
        <ComponentsLookUpWrapper
          component={TestModuleComponent}
          payload={testPayload}
          shouldPreventClose={true}
        />
      </Provider>,
    );

    expect(store.getState().workspace.views.byId).toEqual({
      'test-1': {
        componentType: 'test',
        initialProps: {},
      },
    });

    fireEvent.click(screen.getByRole('button'));

    await waitFor(() =>
      expect(store.getState().workspace.views.byId).toEqual({
        'test-1': {
          componentType: 'test',
          initialProps: {},
          shouldPreventClose: true,
        },
      }),
    );

    fireEvent.click(screen.getByRole('button'));
    await waitFor(() =>
      expect(store.getState().workspace.views.byId).toEqual({
        'test-1': {
          componentType: 'test',
          initialProps: {},
          shouldPreventClose: false,
        },
      }),
    );
  });
});
