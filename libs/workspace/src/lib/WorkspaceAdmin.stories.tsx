/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';

import { StoryWrapper } from './storyUtils/StoryWrapper';
import { useFirstTimeError } from './storyUtils/useFirstTimeError';
import { PresetsApi } from './utils/api';
import { createApi as createFakeApi } from './utils/fakeApi';
import { ViewPreset } from './store/viewPresets/types';

export default {
  title: 'components/Workspace/Workspace Admin Demo',
};

export const Admin = (): React.ReactElement => {
  return <StoryWrapper isAdmin />;
};

export const AdminScreenConfigRadarTemp = (): React.ReactElement => {
  return <StoryWrapper isAdmin workspaceId="screenConfigRadarTemp" />;
};

export const ErrorSaveWorkspace = (): React.ReactElement => {
  const { fetch } = useFirstTimeError();

  const createFakeApiWithErrorOnSave = (): PresetsApi => {
    const currentFakeApi = createFakeApi();

    return {
      ...currentFakeApi,
      saveWorkspacePreset: (workspaceId, data): Promise<void> => {
        return fetch(currentFakeApi.saveWorkspacePreset(workspaceId, data));
      },
      saveWorkspacePresetAs: (data): Promise<string> => {
        return fetch(currentFakeApi.saveWorkspacePresetAs(data));
      },
    };
  };
  return (
    <StoryWrapper
      createApi={createFakeApiWithErrorOnSave}
      workspaceId="screenConfigRadarTemp"
      isAdmin
    />
  );
};

export const ErrorSaveWorkspaceViews = (): React.ReactElement => {
  const { fetch } = useFirstTimeError();

  const createFakeApiWithErrorOnSave = (): PresetsApi => {
    const currentFakeApi = createFakeApi();

    return {
      ...currentFakeApi,
      saveViewPreset: (presetId: string, data: ViewPreset): Promise<void> => {
        return fetch(currentFakeApi.saveViewPreset(presetId, data));
      },
      saveViewPresetAs: (data: ViewPreset): Promise<string> => {
        return fetch(currentFakeApi.saveViewPresetAs(data));
      },
    };
  };
  return (
    <StoryWrapper
      createApi={createFakeApiWithErrorOnSave}
      workspaceId="screenConfigRadarTemp"
      isAdmin
    />
  );
};
