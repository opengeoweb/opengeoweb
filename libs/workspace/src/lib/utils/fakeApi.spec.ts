/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as utils from '@opengeoweb/api';
import { createFakeApiInstance } from '@opengeoweb/api';
import { LayerType } from '@opengeoweb/webmap';
import {
  createApi,
  ERROR_NOT_FOUND,
  extractViewPresetFromJSON,
  extractWorkspacePresetFromJSON,
  filterViewPresetList,
  filterWorkspacePresetList,
  sortViewPresets,
} from './fakeApi';
import {
  WorkspacePresetFromBE,
  ViewPresetListItem,
} from '../store/workspace/types';
import { ViewPreset } from '../store/viewPresets/types';
import workspacePresetsList from './fakeApi/workspacePresetsList.json';
import workspacePresetsListFullContent from './fakeApi/workspacePresets.json';
import viewPresetsListFullContent from './fakeApi/viewPresets.json';
import viewPresetsList from './fakeApi/viewPresetsList.json';

describe('src/utils/fakeApi', () => {
  const fakeAxiosInstance = createFakeApiInstance();

  describe('createApi', () => {
    it('should contain all api calls', async () => {
      const api = createApi();
      expect(api.getWorkspacePresets).toBeTruthy();
      expect(api.getWorkspacePreset).toBeTruthy();
      expect(api.saveWorkspacePreset).toBeTruthy();
      expect(api.saveWorkspacePresetAs).toBeTruthy();
      expect(api.deleteWorkspacePreset).toBeTruthy();
      expect(api.getViewPresets).toBeTruthy();
      expect(api.getViewPreset).toBeTruthy();
      expect(api.saveViewPreset).toBeTruthy();
      expect(api.saveViewPresetAs).toBeTruthy();
      expect(api.deleteViewPreset).toBeTruthy();
    });

    it('should call with the right params for getWorkspacePresets if no scope passed', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi();

      const params = {};
      await api.getWorkspacePresets(params);
      expect(spy).toHaveBeenCalledWith('/workspacepreset', { params });
    });

    it('should call with the right params for getWorkspacePresets if one scope passed', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi();

      const params = { scope: 'system' };
      await api.getWorkspacePresets(params);
      expect(spy).toHaveBeenCalledWith('/workspacepreset', { params });
    });

    it('should call with the right params for getWorkspacePresets if mulitple scope passed', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi();

      const params = { scope: 'system,user' };
      await api.getWorkspacePresets(params);
      expect(spy).toHaveBeenCalledWith('/workspacepreset', { params });
    });

    it('should call with the right params for getWorkspacePreset', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi();

      const presetId = 'screenConfigRadarTemp';
      await api.getWorkspacePreset(presetId);
      expect(spy).toHaveBeenCalledWith(`/workspacepreset/${presetId}`);
    });

    it('should return error for getWorkspacePreset when id can not be found', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const api = createApi();

      const presetId = 'non-existing';

      await expect(async () =>
        api.getWorkspacePreset(presetId),
      ).rejects.toThrow(ERROR_NOT_FOUND);
    });

    it('should call with the right params for saveWorkspacePreset', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'put');
      const api = createApi();

      const workspacePreset: WorkspacePresetFromBE = {
        id: 'screenConfigRadarTemp',
        title: 'Radar and temperature',
        views: [
          {
            mosaicNodeId: 'radarA',
            viewPresetId: 'radar',
          },
          { mosaicNodeId: 'tempB', viewPresetId: 'temp' },
        ],
        syncGroups: [
          { id: 'Area_RadarTemp', type: 'SYNCGROUPS_TYPE_SETBBOX' },
          { id: 'Time_RadarTemp', type: 'SYNCGROUPS_TYPE_SETTIME' },
        ],
        mosaicNode: {
          direction: 'row',
          first: 'radarA',
          second: 'tempB',
          splitPercentage: 50,
        },
        scope: 'system',
        abstract: 'This can contain an abstract',
      };
      await api.saveWorkspacePreset(workspacePreset.id!, workspacePreset);
      expect(spy).toHaveBeenCalledWith(
        `/workspacepreset/${workspacePreset.id}/`,
        workspacePreset,
      );
    });

    it('should call with the right params for saveWorkspacePresetAs', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi();

      const workspacePreset: WorkspacePresetFromBE = {
        title: 'Radar and temperature',
        views: [
          {
            mosaicNodeId: 'radarA',
            viewPresetId: 'radar',
          },
          { mosaicNodeId: 'tempB', viewPresetId: 'temp' },
        ],
        syncGroups: [
          { id: 'Area_RadarTemp', type: 'SYNCGROUPS_TYPE_SETBBOX' },
          { id: 'Time_RadarTemp', type: 'SYNCGROUPS_TYPE_SETTIME' },
        ],
        mosaicNode: {
          direction: 'row',
          first: 'radar',
          second: 'temp',
          splitPercentage: 50,
        },
        scope: 'system',
        abstract: 'This can contain an abstract',
      };
      await api.saveWorkspacePresetAs(workspacePreset);
      expect(spy).toHaveBeenCalledWith(`/workspacepreset/`, workspacePreset);
    });

    it('should call with the right params for deleteWorkspacePreset', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'delete');
      const api = createApi();
      const presetId = 'test-1';
      await api.deleteWorkspacePreset(presetId);
      expect(spy).toHaveBeenCalledWith(`/workspacepreset/${presetId}`);
    });

    it('should call with the right params for getViewPresets if no scope passed', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi();

      const params = {};
      await api.getViewPresets(params);
      expect(spy).toHaveBeenCalledWith('/viewpreset', { params });
    });

    it('should call with the right params for getViewPresets if one scope passed', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi();

      const params = { scope: 'system' };
      await api.getViewPresets(params);
      expect(spy).toHaveBeenCalledWith('/viewpreset', { params });
    });

    it('should call with the right params for getViewPresets if mulitple scope passed', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi();

      const params = { scope: 'system,user' };
      await api.getViewPresets(params);
      expect(spy).toHaveBeenCalledWith('/viewpreset', { params });
    });

    it('should call with the right params for getViewPreset', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi();

      const presetId = 'test-1';
      await api.getViewPreset(presetId);
      expect(spy).toHaveBeenCalledWith(`/viewpreset/${presetId}`);
    });

    it('should call with the right params for saveViewPreset', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'put');
      const api = createApi();

      const preset: ViewPreset = {
        componentType: 'Map',
        id: 'Radar',
        keywords: 'WMS,Radar',
        title: 'My new radar',
        initialProps: {
          mapPreset: {
            layers: [
              {
                id: 'base-layer-1',
                name: 'OpenStreetMap_NL',
                type: 'twms',
                layerType: LayerType.baseLayer,
                enabled: true,
              },
              {
                enabled: true,
                format: 'image/png',
                layerType: LayerType.mapLayer,
                name: 'RAD_NL25_PCP_CM',
                service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
                style: 'precip-blue-transparent/nearest',
              },
            ],
            proj: {
              bbox: {
                bottom: 6408480.4514,
                left: 58703.6377,
                right: 3967387.5161,
                top: 11520588.9031,
              },
              srs: 'EPSG:3857',
            },
            showTimeSlider: true,
          },
        },
      };
      await api.saveViewPreset(preset.id!, preset);
      expect(spy).toHaveBeenCalledWith(`/viewpreset/${preset.id}/`, preset);
    });

    it('should call with the right params for saveViewPresetAs', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi();

      const preset: ViewPreset = {
        componentType: 'Map',
        id: 'Radar',
        keywords: 'WMS,Radar',
        title: 'My new radar',
        initialProps: {
          mapPreset: {
            layers: [
              {
                id: 'base-layer-1',
                name: 'OpenStreetMap_NL',
                type: 'twms',
                layerType: LayerType.baseLayer,
                enabled: true,
              },
              {
                enabled: true,
                format: 'image/png',
                layerType: LayerType.mapLayer,
                name: 'RAD_NL25_PCP_CM',
                service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
                style: 'precip-blue-transparent/nearest',
              },
            ],
            proj: {
              bbox: {
                bottom: 6408480.4514,
                left: 58703.6377,
                right: 3967387.5161,
                top: 11520588.9031,
              },
              srs: 'EPSG:3857',
            },
            showTimeSlider: true,
          },
        },
      };
      await api.saveViewPresetAs(preset);
      expect(spy).toHaveBeenCalledWith(`/viewpreset/`, preset);
    });
  });

  describe('extractWorkspacePresetFromJSON', () => {
    it('should return the relevant workspace preset', () => {
      expect(extractWorkspacePresetFromJSON('screenConfigRadarTemp')).toBe(
        workspacePresetsListFullContent[0],
      );
      expect(extractWorkspacePresetFromJSON('screenConfigTimeSeries')).toBe(
        workspacePresetsListFullContent[3],
      );
      expect(extractWorkspacePresetFromJSON('emptyMap')).toBe(
        workspacePresetsListFullContent[
          workspacePresetsListFullContent.length - 4
        ],
      );
      expect(extractWorkspacePresetFromJSON('screenConfigActions')).toBe(
        undefined,
      );
    });
  });

  describe('extractViewPresetFromJSON', () => {
    it('should return the relevant workspace preset', () => {
      expect(extractViewPresetFromJSON('radar')).toBe(
        viewPresetsListFullContent[0],
      );
      expect(extractViewPresetFromJSON('temp')).toBe(
        viewPresetsListFullContent[1],
      );
    });
  });

  describe('filterWorkspacePresetList', () => {
    it('should return all workspace presets if scope not passed in', () => {
      expect(filterWorkspacePresetList({})).toStrictEqual(workspacePresetsList);
    });
    it('should return no workspace presets if scope empty', () => {
      expect(filterWorkspacePresetList({ scope: '' })).toStrictEqual([]);
    });
    it('should return all user/system workspace presets for scope user/system', () => {
      expect(filterWorkspacePresetList({ scope: 'user' })).toStrictEqual([
        workspacePresetsList[8],
      ]);
      expect(filterWorkspacePresetList({ scope: 'system' })).toStrictEqual(
        workspacePresetsList.filter(
          (workspace) => workspace.scope === 'system',
        ),
      );
    });
    it('should return workspace when searching for title', () => {
      expect(
        filterWorkspacePresetList({ search: 'cap', scope: 'user,system' }),
      ).toEqual([
        workspacePresetsList.find(
          (workspace) => workspace.id === 'capWorkspace',
        ),
      ]);
      expect(
        filterWorkspacePresetList({
          search: 'pressure and wind 1:1',
          scope: 'user,system',
        }),
      ).toEqual([
        workspacePresetsList.find(
          (workspace) =>
            workspace.id ===
            'screenConfigHarmoniePrecipMSLRefTimePresetWithoutMultiMapLayerManager',
        ),
      ]);
      expect(
        filterWorkspacePresetList({
          search: 'radar sat',
          scope: 'system',
        }),
      ).toEqual([
        workspacePresetsList.find(
          (workspace) => workspace.id === 'screenConfigD',
        ),
      ]);
      expect(
        filterWorkspacePresetList({
          search: 'harmonie 1 all',
          scope: 'system',
        }),
      ).toEqual([
        workspacePresetsList.find(
          (workspace) =>
            workspace.id ===
            'screenConfigHarmoniePresetWithMultiMapLayerManager',
        ),
      ]);
    });

    it('should return workspace when searching for abstract', () => {
      expect(
        filterWorkspacePresetList({
          search: 'Radar and temperature maps',
          scope: 'user,system',
        }),
      ).toEqual([
        workspacePresetsList.find(
          (workspace) => workspace.id === 'screenConfigRadarTemp',
        ),
      ]);
      expect(
        filterWorkspacePresetList({
          search: 'SyncGroups synced BBOX',
          scope: 'user,system',
        }),
      ).toEqual([
        workspacePresetsList.find(
          (workspace) => workspace.id === 'screenConfigOnlyBBOXGroup',
        ),
      ]);

      expect(
        filterWorkspacePresetList({
          search: 'SyncGroups synced BBOX',
          scope: 'system',
        }),
      ).toEqual([
        workspacePresetsList.find(
          (workspace) => workspace.id === 'screenConfigOnlyBBOXGroup',
        ),
      ]);

      expect(
        filterWorkspacePresetList({
          search: 'Radar and temperature maps',
          scope: 'system',
        }),
      ).toEqual([
        workspacePresetsList.find(
          (workspace) => workspace.id === 'screenConfigRadarTemp',
        ),
      ]);

      expect(
        filterWorkspacePresetList({
          search: 'SyncGroups synced BBOX',
          scope: 'user',
        }),
      ).toEqual([]);
    });
  });

  describe('filterViewPresetList', () => {
    it('should return all view presets if scope not passed in', () => {
      expect(filterViewPresetList({})).toStrictEqual(viewPresetsList);
    });
    it('should return no view presets if scope empty', () => {
      expect(filterViewPresetList({ scope: '' })).toStrictEqual([]);
    });
    it('should return all user/system view presets for scope user/system', () => {
      expect(filterViewPresetList({ scope: 'user' })).toStrictEqual([
        viewPresetsList[2],
      ]);
      expect(filterViewPresetList({ scope: 'system' })).toStrictEqual(
        viewPresetsList.filter((viewpreset) => viewpreset.scope === 'system'),
      );
    });
  });

  describe('sortViewPresets', () => {
    const viewPresets: ViewPresetListItem[] = [
      {
        id: 'system-1',
        scope: 'system',
        title: 'Some system preset',
        date: '2022-06-01T12:34:27.787192',
      },
      {
        id: 'user-1',
        scope: 'user',
        title: 'Some user preset',
        date: '2022-06-01T12:34:27.787192',
      },
      {
        id: 'user-2',
        scope: 'user',
        title: 'A user preset',
        date: '2022-06-01T12:34:27.787192',
      },

      {
        id: 'system-2',
        scope: 'system',
        title: 'A system preset',
        date: '2022-06-01T12:34:27.787192',
      },
    ];

    it('should sort by scope and secondarily alphabetically by title', async () => {
      const sortedTitles = sortViewPresets(viewPresets).map(
        (preset) => preset.title,
      );
      expect(sortedTitles).toEqual([
        'A system preset',
        'Some system preset',
        'A user preset',
        'Some user preset',
      ]);
    });
  });
});
