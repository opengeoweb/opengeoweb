/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { getCleanSearchQuery, getPresetTitle, setDocumentTitle } from './utils';
import { i18n, initWorkspaceTestI18n } from './i18n';

beforeAll(() => {
  initWorkspaceTestI18n();
});

describe('getCleanSearchQuery', () => {
  it('should trim empty values of a search string', () => {
    expect(getCleanSearchQuery('test-1 test-2 test-3')).toEqual(
      'test-1,test-2,test-3',
    );
    expect(getCleanSearchQuery('test-1 test-2     test-3   ')).toEqual(
      'test-1,test-2,test-3',
    );
    expect(getCleanSearchQuery('   test-1 test 2     test-3   ')).toEqual(
      'test-1,test,2,test-3',
    );
    expect(getCleanSearchQuery('test-1  test-2 test 3')).toEqual(
      'test-1,test-2,test,3',
    );
  });
});

describe('getPresetTitle', () => {
  const { t } = i18n;

  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('should return the original title if hasChanges is false', () => {
    const title = 'Preset Title';
    const hasChanges = false;
    const isNew = false;

    const result = getPresetTitle(title, hasChanges, isNew, t);

    expect(result).toBe(title);
  });

  it('should return the title with modified suffix for an existing item', () => {
    const title = 'Preset Title';
    const hasChanges = true;
    const isNew = false;

    const result = getPresetTitle(title, hasChanges, isNew, t);

    expect(result).toBe(`${title} (${t('workspace-modified-new')})`);
  });

  it('should return the title with modified suffix for a new item', () => {
    const title = 'Preset Title';
    const hasChanges = true;
    const isNew = true;

    const result = getPresetTitle(title, hasChanges, isNew, t);

    expect(result).toBe(`${title} (${t('workspace-modified-existing')})`);
  });

  it('should handle empty title and return it with suffix when hasChanges is true', () => {
    const title = '';
    const hasChanges = true;
    const isNew = true;

    const result = getPresetTitle(title, hasChanges, isNew, t);

    expect(result).toBe(`${title} (${t('workspace-modified-existing')})`);
  });

  it('should return original title if hasChanges is false, regardless of isNew', () => {
    const title = 'Preset Title';
    const hasChanges = false;
    const isNew = true;
    const result = getPresetTitle(title, hasChanges, isNew, t);

    expect(result).toBe(title);
  });
});

describe('setDocumentTitle', () => {
  const originalTitle = document.title;

  beforeEach(() => {
    document.title = '';
  });

  afterEach(() => {
    document.title = originalTitle;
  });

  it('should set the document title with the workspace title and default app title', () => {
    const workspaceTitle = 'Workspace preset title';

    setDocumentTitle(workspaceTitle);

    expect(document.title).toBe(`${workspaceTitle} - GeoWeb`);
  });

  it('should set the document title with the workspace title and configured app title', () => {
    const workspaceTitle = 'Workspace preset title';
    const appTitle = 'Custom App Title';

    setDocumentTitle(workspaceTitle, appTitle);

    expect(document.title).toBe(`${workspaceTitle} - ${appTitle}`);
  });

  it('should set the document title to configured app title if workspaceTitle is empty', () => {
    const workspaceTitle = '';
    const appTitle = 'Custom App Title';

    setDocumentTitle(workspaceTitle, appTitle);

    expect(document.title).toBe(appTitle);
  });

  it('should set the document title to default app title if workspaceTitle is empty and app title not configured', () => {
    const workspaceTitle = '';

    setDocumentTitle(workspaceTitle);

    expect(document.title).toBe('GeoWeb');
  });
});
