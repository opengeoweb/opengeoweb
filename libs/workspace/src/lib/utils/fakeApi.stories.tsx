/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React, { useState } from 'react';
import { useApiContext } from '@opengeoweb/api';
import { Box, Grid2 as Grid } from '@mui/material';
import { PresetsApi } from './api';
import {
  WorkspacePresetFromBE,
  WorkspacePresetListItem,
  WorkspaceViewsFromBE,
} from '../store/workspace/types';
import { WorkspaceWrapperProviderWithStore } from '../components/Providers/Providers';
import { ViewPreset } from '../store/viewPresets/types';
import { createMockStore } from '../store';

export default { title: 'Examples/fakeApi' };

const WorkspacePresetsList: React.FC = () => {
  const { api } = useApiContext<PresetsApi>();
  const [workspacePresets, setWorkspacePresets] = useState<
    WorkspacePresetListItem[]
  >([]);
  const [workspacePresetFull, setPresetFull] = useState<WorkspacePresetFromBE>(
    {} as WorkspacePresetFromBE,
  );
  const [viewPresetsFull, setViewPresetsFull] = useState<ViewPreset[]>([]);

  const fetchWorkspacePresetList = (): void => {
    api
      .getWorkspacePresets()
      .then((result) => {
        setWorkspacePresets(result.data);
      })
      .catch(() => {
        setWorkspacePresets([]);
      });
  };

  const fetchWorkspacePresetFull = async (id: string): Promise<void> => {
    try {
      const result = await api.getWorkspacePreset(id);
      setPresetFull(result.data);
      setViewPresetsFull([]);
    } catch {
      setPresetFull({} as WorkspacePresetFromBE);
      setViewPresetsFull([]);
    }
  };

  const fetchViewPresetsFull = async (
    workspaceViews: WorkspaceViewsFromBE[],
  ): Promise<void> => {
    const viewPresets: ViewPreset[] = [];
    try {
      await Promise.all(
        workspaceViews.map(async (view) => {
          const result = await api.getViewPreset(view.viewPresetId);
          viewPresets.push(result.data);
        }),
      );
      setViewPresetsFull(viewPresets);
    } catch {
      setViewPresetsFull(viewPresets);
    }
  };

  React.useEffect(() => {
    fetchWorkspacePresetList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Box component="div">
      <Grid container>
        <Grid size={{ xs: 6 }} sx={{ border: '1px solid black' }}>
          <p>
            Result of <b>/workspacepreset</b> (list) call:
          </p>
          <p>Click on a workspace preset to retrieve the full JSON</p>
          {workspacePresets.map((workspacePreset) => {
            return (
              <Grid size={{ xs: 3 }} key={workspacePreset.id}>
                <Box
                  onClick={(): Promise<void> =>
                    fetchWorkspacePresetFull(workspacePreset.id)
                  }
                  sx={{ cursor: 'pointer' }}
                >
                  <pre>{JSON.stringify(workspacePreset, null, 2)}</pre>
                </Box>
              </Grid>
            );
          })}
        </Grid>
        <Grid size={{ xs: 6 }} sx={{ border: '1px solid black' }}>
          <Box
            onClick={(): Promise<void> =>
              fetchViewPresetsFull(workspacePresetFull.views)
            }
            sx={{ cursor: 'pointer' }}
          >
            <p>
              <p>
                Result of <b>/workspacepreset/workspaceId</b> call (Retrieves
                full workspace preset):
              </p>
              <p>Click to retrieve the full corresponding view presets</p>
            </p>
            <pre>{JSON.stringify(workspacePresetFull, null, 2)}</pre>
          </Box>

          <div>
            <p>
              Result of <b>/viewpreset/viewPresetId</b> call (Retrieves full
              view preset for each view preset in workspace):
            </p>
            <pre>{JSON.stringify(viewPresetsFull, null, 2)}</pre>
          </div>
        </Grid>
      </Grid>
    </Box>
  );
};

export const WorkspacePresetsListStory: React.FC = () => (
  <WorkspaceWrapperProviderWithStore store={createMockStore()}>
    <WorkspacePresetsList />
  </WorkspaceWrapperProviderWithStore>
);
