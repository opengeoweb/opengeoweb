/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { MosaicDirection } from 'react-mosaic-component';
import { mapSelectors, syncGroupsSelectors, types } from '@opengeoweb/store';
import { omit } from 'lodash';
import * as workspaceSelectors from './selectors';
import { WorkspaceError, WorkspaceErrorType, WorkspaceState } from './types';
import { AppStore } from '../types';
import { emptyViewPreset } from '../viewPresets/utils';
import { initialState } from './reducer';

const testIds = ['testviewA', 'testviewB', 'testviewC'];
const initialTripleWorkspaceState = {
  ...initialState,
  views: {
    allIds: testIds,
    byId: {
      testviewA: {
        componentType: 'bla',
      },
      testviewB: {
        componentType: 'bla',
      },
      testviewC: {
        componentType: 'bla',
      },
    },
  },
  mosaicNode: {
    direction: 'row' as MosaicDirection,
    first: 'testviewA',
    second: {
      direction: 'row' as MosaicDirection,
      first: 'testviewB',
      second: 'testviewC',
    },
  },
};
describe('store/selectors', () => {
  describe('getWorkspaceState', () => {
    it('should return workspace state', () => {
      const mockStore: AppStore = {
        workspace: initialTripleWorkspaceState,
      };

      expect(workspaceSelectors.getWorkspaceState(mockStore)).toEqual(
        mockStore.workspace,
      );
    });
  });
  describe('getMosaicNode', () => {
    it('should return mosaicNode from workspace state', () => {
      const mockStore: AppStore = {
        workspace: initialTripleWorkspaceState,
      };

      expect(workspaceSelectors.getMosaicNode(mockStore)).toEqual(
        mockStore.workspace!.mosaicNode,
      );
    });
  });

  describe('getViewById', () => {
    it('should return view from workspace state', () => {
      const mockStore: AppStore = {
        workspace: initialTripleWorkspaceState,
      };

      expect(workspaceSelectors.getViewById(mockStore, 'testviewA')).toEqual(
        mockStore.workspace!.views.byId!['testviewA'],
      );
      expect(workspaceSelectors.getViewById(mockStore, 'testviewB')).toEqual(
        mockStore.workspace!.views.byId!['testviewB'],
      );
      expect(workspaceSelectors.getViewById(mockStore, 'testviewC')).toEqual(
        mockStore.workspace!.views.byId!['testviewC'],
      );
    });

    it('should return null from workspace state when id is not present', () => {
      const mockStore: AppStore = {
        workspace: initialState,
      };

      expect(
        workspaceSelectors.getViewById(mockStore, 'testviewA'),
      ).toBeUndefined();
      expect(
        workspaceSelectors.getViewById(mockStore, 'testviewB'),
      ).toBeUndefined();
      expect(
        workspaceSelectors.getViewById(mockStore, 'testviewC'),
      ).toBeUndefined();
    });
  });

  describe('getViewIds', () => {
    it('should return viewIds from view state', () => {
      const testIds = ['testviewA', 'testviewB', 'testviewC'];
      const mockStore: AppStore = {
        workspace: initialTripleWorkspaceState,
      };
      expect(workspaceSelectors.getViewIds(mockStore)).toEqual(testIds);
    });
    it('should return empty list if no views are present', () => {
      expect(workspaceSelectors.getViewIds({})).toEqual([]);
    });
  });

  describe('shouldPreventClose', () => {
    it('should return shouldPreventClose from view state', () => {
      const mockStore: AppStore = {
        workspace: {
          ...initialTripleWorkspaceState,
          views: {
            allIds: testIds,
            byId: {
              testviewA: { componentType: 'bla', shouldPreventClose: true },
              testviewB: { componentType: 'bla', shouldPreventClose: false },
              testviewC: { componentType: 'bla', shouldPreventClose: true },
            },
          },
        },
      };
      expect(
        workspaceSelectors.getShouldPreventClose(mockStore, 'testviewA'),
      ).toBeTruthy();
      expect(
        workspaceSelectors.getShouldPreventClose(mockStore, 'testviewB'),
      ).toBeFalsy();
      expect(
        workspaceSelectors.getShouldPreventClose(mockStore, 'testviewC'),
      ).toBeTruthy();
    });
    it('should return shouldPreventClose false if not present', () => {
      const mockStore: AppStore = {
        workspace: initialTripleWorkspaceState,
      };
      expect(
        workspaceSelectors.getShouldPreventClose(mockStore, 'testviewA'),
      ).toBeFalsy();
      expect(
        workspaceSelectors.getShouldPreventClose(mockStore, 'testviewB'),
      ).toBeFalsy();
      expect(
        workspaceSelectors.getShouldPreventClose(mockStore, 'testviewC'),
      ).toBeFalsy();
    });
  });

  describe('isWorkspaceLoading', () => {
    it('should return isWorkspaceLoading from view state', () => {
      const mockStore: AppStore = {
        workspace: initialState,
      };
      expect(workspaceSelectors.isWorkspaceLoading(mockStore)).toBeFalsy();
    });
    it('should return isWorkspaceLoading from view state', () => {
      const mockStore: AppStore = {
        workspace: {
          ...initialState,
          isLoading: true,
        },
      };
      expect(workspaceSelectors.isWorkspaceLoading(mockStore)).toBeTruthy();
    });

    it('should return false if store is not present', () => {
      expect(workspaceSelectors.isWorkspaceLoading({})).toBeFalsy();
    });
  });

  describe('getWorkspaceError', () => {
    it('should return getWorkspaceError undefined if no error', () => {
      const mockStore: AppStore = {
        workspace: initialTripleWorkspaceState,
      };
      expect(workspaceSelectors.getWorkspaceError(mockStore)).toBeUndefined();
    });
    it('should return getWorkspaceError from view state', () => {
      const testError: WorkspaceError = {
        message: 'test error',
        type: WorkspaceErrorType.GENERIC,
      };
      const mockStore: AppStore = {
        workspace: {
          ...initialTripleWorkspaceState,
          error: testError,
        },
      };
      expect(workspaceSelectors.getWorkspaceError(mockStore)).toEqual(
        testError,
      );
    });

    it('should return undefined if store is not present', () => {
      expect(workspaceSelectors.getWorkspaceError({})).toBeUndefined();
    });
  });

  describe('hasWorkspaceChanges', () => {
    it('should return hasChanges from view state', () => {
      const mockStore: AppStore = {
        workspace: {
          ...initialState,
          hasChanges: true,
        },
      };
      expect(workspaceSelectors.hasWorkspaceChanges(mockStore)).toBeTruthy();
      const mockStore2: AppStore = {
        workspace: {
          ...initialState,
          hasChanges: false,
        },
      };
      expect(workspaceSelectors.hasWorkspaceChanges(mockStore2)).toBeFalsy();
    });
    it('should return hasChanges false if not present', () => {
      const mockStore: AppStore = {
        workspace: {
          ...initialState,
        },
      };
      expect(workspaceSelectors.hasWorkspaceChanges(mockStore)).toBeFalsy();
    });
  });

  describe('getViewType', () => {
    it('should return viewType from view state', () => {
      const mockStore: AppStore = {
        workspace: {
          ...initialState,
          viewType: 'multiWindow',
        },
      };
      expect(workspaceSelectors.getViewType(mockStore)).toEqual('multiWindow');
    });
    it('should return viewType empty if not present', () => {
      const mockStore: AppStore = {
        workspace: {
          ...initialState,
        },
      };
      expect(workspaceSelectors.getViewType(mockStore)).toEqual('');
    });
  });

  describe('getSelectedWorkspaceScope', () => {
    it('should return scope from view state', () => {
      const mockStore: AppStore = {
        workspace: {
          ...initialState,
          scope: 'system',
        },
      };
      expect(workspaceSelectors.getSelectedWorkspaceScope(mockStore)).toBe(
        'system',
      );
      const mockStore2: AppStore = {
        workspace: {
          ...initialState,
          scope: 'user',
        },
      };
      expect(workspaceSelectors.getSelectedWorkspaceScope(mockStore2)).toBe(
        'user',
      );
    });
    it('should return system false if not present', () => {
      const mockStore: AppStore = {
        workspace: {
          ...initialState,
        },
      };
      expect(workspaceSelectors.getSelectedWorkspaceScope(mockStore)).toBe(
        'system',
      );
    });
  });

  const mosaicNodeId = 'test';
  const viewPresetId = 'test-1';
  const mockedGroups: Record<string, types.SynchronizationGroup> = {
    'Default group for SYNCGROUPS_TYPE_SETBBOX': {
      title: 'Default group for SYNCGROUPS_TYPE_SETBBOX',
      type: 'SYNCGROUPS_TYPE_SETBBOX' as types.SyncType,
      payloadByType: {
        SYNCGROUPS_TYPE_SETTIME: null!,
        SYNCGROUPS_TYPE_SETBBOX: null!,
      },
      targets: {
        allIds: [],
        byId: {},
      },
    },
    'Default group for SYNCGROUPS_TYPE_SETTIME': {
      title: 'Default group for SYNCGROUPS_TYPE_SETTIME',
      type: 'SYNCGROUPS_TYPE_SETTIME' as types.SyncType,
      payloadByType: {
        SYNCGROUPS_TYPE_SETTIME: null!,
        SYNCGROUPS_TYPE_SETBBOX: null!,
      },
      targets: {
        allIds: [],
        byId: {},
      },
    },
    SYNCGROUPS_TYPE_SETTIME2: {
      title: 'SYNCGROUPS_TYPE_SETTIME2',
      type: 'SYNCGROUPS_TYPE_SETTIME' as types.SyncType,
      payloadByType: {
        SYNCGROUPS_TYPE_SETTIME: null!,
        SYNCGROUPS_TYPE_SETBBOX: null!,
      },
      targets: {
        allIds: [],
        byId: {},
      },
    },
  };
  const mockWorkspacePresetStore: AppStore = {
    workspace: {
      ...initialState,
      views: {
        allIds: [mosaicNodeId],
        byId: {
          [mosaicNodeId]: {
            id: viewPresetId,
            componentType: 'Map',
          },
        },
      },
      hasChanges: true,
    },
    syncGroups: {
      sources: {
        byId: {
          emptyMapView: {
            types: ['SYNCGROUPS_TYPE_SETTIME', 'SYNCGROUPS_TYPE_SETBBOX'],
            payloadByType: {
              SYNCGROUPS_TYPE_SETTIME: {
                sourceId: 'emptyMapView',
                origin:
                  'ORIGIN_REACTMAPVIEW_ONMAPCHANGEDIMENSION==> ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION',
                value: '2023-04-20T06:30:00Z',
              },
            },
          },
          screen_no_1: {
            types: ['SYNCGROUPS_TYPE_SETTIME', 'SYNCGROUPS_TYPE_SETBBOX'],
            payloadByType: {},
          },
        },
        allIds: ['emptyMapView', 'screen_no_1'],
      },
      groups: {
        byId: mockedGroups,
        allIds: Object.keys(mockedGroups),
      },
      viewState: {
        timeslider: {
          groups: [
            {
              id: 'Default group for SYNCGROUPS_TYPE_SETTIME',
              selected: [],
            },
          ],
          sourcesById: [
            {
              id: 'emptyMapView',
              name: 'emptyMapView',
            },
            {
              id: 'screen_no_1',
              name: 'screen_no_1',
            },
          ],
        },
        zoompane: {
          groups: [
            {
              id: 'Default group for SYNCGROUPS_TYPE_SETBBOX',
              selected: [],
            },
          ],
          sourcesById: [
            {
              id: 'emptyMapView',
              name: 'emptyMapView',
            },
            {
              id: 'screen_no_1',
              name: 'screen_no_1',
            },
          ],
        },
        level: {
          groups: [],
          sourcesById: [],
        },
      },
      isTimeScrollingEnabled: true,
      linkedState: {
        links: { [mosaicNodeId]: ['map1', 'map2'] },
        sharedData: {},
      },
    },
    viewPresets: {
      ids: [mosaicNodeId],
      entities: {
        [mosaicNodeId]: {
          panelId: mosaicNodeId,
          activeViewPresetId: 'screenSat',
          hasChanges: true,
          isFetching: false,
          error: undefined,
          isViewPresetListDialogOpen: false,
          filters: [],
          searchQuery: '',
          filterResults: { ids: [], entities: {} },
        },
      },
    },
  };

  describe('getWorkspaceData', () => {
    it('should return workspace data to send to the BE', () => {
      expect(
        workspaceSelectors.getWorkspaceData(mockWorkspacePresetStore),
      ).toEqual({
        abstract: undefined,
        id: '',
        mosaicNode: undefined,
        scope: undefined,
        syncGroups: [
          {
            id: 'Default group for SYNCGROUPS_TYPE_SETBBOX',
            type: 'SYNCGROUPS_TYPE_SETBBOX',
          },
          {
            id: 'Default group for SYNCGROUPS_TYPE_SETTIME',
            type: 'SYNCGROUPS_TYPE_SETTIME',
          },
          {
            id: 'SYNCGROUPS_TYPE_SETTIME2',
            type: 'SYNCGROUPS_TYPE_SETTIME',
          },
        ],
        linking: { test: ['map1', 'map2'] },
        title: '',
        viewType: undefined,
        views: [{ mosaicNodeId: 'test', viewPresetId: 'screenSat' }],
        isTimeScrollingEnabled: true,
      });
    });

    it('should return workspace data to send to the BE with emptymap if no active mapPresetId', () => {
      const omittedGroups = omit(mockedGroups, 'SYNCGROUPS_TYPE_SETTIME2');
      const mockStore: AppStore = {
        ...mockWorkspacePresetStore,
        syncGroups: {
          ...(mockWorkspacePresetStore.syncGroups as types.SynchronizationGroupState),
          groups: {
            byId: omittedGroups,
            allIds: Object.keys(omittedGroups),
          },
        },
        viewPresets: {
          ids: [mosaicNodeId],
          entities: {
            [mosaicNodeId]: {
              panelId: mosaicNodeId,
              activeViewPresetId: '',
              hasChanges: true,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
          },
        },
      };
      expect(workspaceSelectors.getWorkspaceData(mockStore)).toEqual({
        abstract: undefined,
        id: '',
        mosaicNode: undefined,
        scope: undefined,
        syncGroups: [
          {
            id: 'Default group for SYNCGROUPS_TYPE_SETBBOX',
            type: 'SYNCGROUPS_TYPE_SETBBOX',
          },
          {
            id: 'Default group for SYNCGROUPS_TYPE_SETTIME',
            type: 'SYNCGROUPS_TYPE_SETTIME',
          },
        ],
        linking: { test: ['map1', 'map2'] },
        title: '',
        viewType: undefined,
        views: [{ mosaicNodeId: 'test', viewPresetId: 'emptyMap' }],
        isTimeScrollingEnabled: true,
      });
    });

    it('should return empty object if store does not exist', () => {
      expect(workspaceSelectors.getWorkspaceData(null!)).toEqual({
        abstract: undefined,
        id: '',
        mosaicNode: undefined,
        scope: undefined,
        syncGroups: [],
        title: '',
        linking: {},
        viewType: undefined,
        views: [],
        isTimeScrollingEnabled: false,
      });
    });
  });

  describe('getViewComponentType', () => {
    it('should return componentType from view state', () => {
      const mockStore: AppStore = {
        workspace: {
          ...initialState,
          views: {
            allIds: ['testviewA'],
            byId: {
              testviewA: {
                componentType: 'TestComponentType',
              },
            },
          },
        },
      };
      expect(
        workspaceSelectors.getViewComponentType(mockStore, 'testviewA'),
      ).toBe('TestComponentType');
    });
    it('should return Map if not present', () => {
      const mockStore: AppStore = {
        workspace: initialState,
      };
      expect(
        workspaceSelectors.getViewComponentType(mockStore, 'testviewA'),
      ).toBe('Map');
    });
  });

  describe('getViewPresetToSendToBackend', () => {
    it('should return viewpreset data to send to the BE', () => {
      const mosaicNodeId = 'test';
      const viewPresetId = 'test-1';
      const workspaceId = 'workspace-id1';
      const workspace: WorkspaceState = {
        id: workspaceId,
        title: 'test title',
        scope: 'user',
        views: {
          allIds: [mosaicNodeId],
          byId: {
            [mosaicNodeId]: {
              id: viewPresetId,
              componentType: 'Map',
              scope: 'user',
              title: 'test view title',
            },
          },
        },
        mosaicNode: '',
        hasChanges: true,
      };
      const mockStore: AppStore = {
        workspace,
        syncGroups: {
          sources: {
            byId: {
              emptyMapView: {
                types: ['SYNCGROUPS_TYPE_SETTIME', 'SYNCGROUPS_TYPE_SETBBOX'],
                payloadByType: {
                  SYNCGROUPS_TYPE_SETTIME: {
                    sourceId: 'emptyMapView',
                    origin:
                      'ORIGIN_REACTMAPVIEW_ONMAPCHANGEDIMENSION==> ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION',
                    value: '2023-04-20T06:30:00Z',
                  },
                },
              },
              screen_no_1: {
                types: ['SYNCGROUPS_TYPE_SETTIME', 'SYNCGROUPS_TYPE_SETBBOX'],
                payloadByType: {},
              },
            },
            allIds: ['emptyMapView', 'screen_no_1'],
          },
          groups: {
            byId: {
              'Default group for SYNCGROUPS_TYPE_SETBBOX': {
                title: 'Default group for SYNCGROUPS_TYPE_SETBBOX',
                type: 'SYNCGROUPS_TYPE_SETBBOX',
                payloadByType: {
                  SYNCGROUPS_TYPE_SETTIME: null!,
                  SYNCGROUPS_TYPE_SETBBOX: null!,
                },
                targets: {
                  allIds: [],
                  byId: {},
                },
              },
              'Default group for SYNCGROUPS_TYPE_SETTIME': {
                title: 'Default group for SYNCGROUPS_TYPE_SETTIME',
                type: 'SYNCGROUPS_TYPE_SETTIME',
                payloadByType: {
                  SYNCGROUPS_TYPE_SETTIME: null!,
                  SYNCGROUPS_TYPE_SETBBOX: null!,
                },
                targets: {
                  allIds: [],
                  byId: {},
                },
              },
              SYNCGROUPS_TYPE_SETTIME2: {
                title: 'SYNCGROUPS_TYPE_SETTIME2',
                type: 'SYNCGROUPS_TYPE_SETTIME',
                payloadByType: {
                  SYNCGROUPS_TYPE_SETTIME: null!,
                  SYNCGROUPS_TYPE_SETBBOX: null!,
                },
                targets: {
                  allIds: [],
                  byId: {},
                },
              },
            },
            allIds: [
              'Default group for SYNCGROUPS_TYPE_SETBBOX',
              'Default group for SYNCGROUPS_TYPE_SETTIME',
              'SYNCGROUPS_TYPE_SETTIME2',
            ],
          },
          viewState: {
            timeslider: {
              groups: [
                {
                  id: 'Default group for SYNCGROUPS_TYPE_SETTIME',
                  selected: [],
                },
              ],
              sourcesById: [
                {
                  id: 'emptyMapView',
                  name: 'emptyMapView',
                },
                {
                  id: 'screen_no_1',
                  name: 'screen_no_1',
                },
              ],
            },
            zoompane: {
              groups: [
                {
                  id: 'Default group for SYNCGROUPS_TYPE_SETBBOX',
                  selected: [],
                },
              ],
              sourcesById: [
                {
                  id: 'emptyMapView',
                  name: 'emptyMapView',
                },
                {
                  id: 'screen_no_1',
                  name: 'screen_no_1',
                },
              ],
            },
            level: {
              groups: [],
              sourcesById: [],
            },
          },
          linkedState: {
            links: {
              test: ['test'],
            },
            sharedData: {
              test: {},
            },
          },
          isTimeScrollingEnabled: false,
        },
        webmap: {
          byId: {
            [mosaicNodeId]: {
              id: mosaicNodeId,
              shouldEndtimeOverride: false,
              isAnimating: true,
              isAutoUpdating: true,
              displayMapPin: true,
              shouldShowZoomControls: false,
              timeStep: 1234,
              animationDelay: 250,
              isTimestepAuto: false,
              mapLayers: [],
              overLayers: [],
              baseLayers: [],
              featureLayers: [],
              bbox: {
                left: 0,
                right: 0,
                bottom: 0,
                top: 0,
              },
              srs: '',
            },
          },
          allIds: [mosaicNodeId],
        },
        viewPresets: {
          ids: [mosaicNodeId],
          entities: {
            [mosaicNodeId]: {
              panelId: mosaicNodeId,
              activeViewPresetId: 'screenSat',
              hasChanges: true,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
          },
        },
      };
      expect(
        workspaceSelectors.getViewPresetToSendToBackend(
          mockStore,
          mosaicNodeId,
        ),
      ).toEqual({
        ...emptyViewPreset('Map'),
        id: viewPresetId,
        scope: workspace.views.byId![mosaicNodeId].scope,
        title: workspace.views.byId![mosaicNodeId].title,
        initialProps: {
          mapPreset: mapSelectors.getMapPreset(mockStore, mosaicNodeId),
          syncGroupsIds: syncGroupsSelectors.getAllTargetGroupsForSource(
            mockStore,
            mosaicNodeId,
          ),
        },
      });
    });

    it('should return correct title and scope if not in store yet', () => {
      const mosaicNodeId = 'test';
      const viewPresetId = 'test-1';
      const workspaceId = 'workspace-id1';
      const workspace: WorkspaceState = {
        id: workspaceId,
        title: 'test title',
        scope: 'user',
        views: {
          allIds: [mosaicNodeId],
          byId: {
            [mosaicNodeId]: {
              id: viewPresetId,
              componentType: 'Map',
            },
          },
        },
        mosaicNode: '',
        hasChanges: true,
      };
      const mockStore: AppStore = {
        workspace,
        syncGroups: {
          sources: {
            byId: {
              emptyMapView: {
                types: ['SYNCGROUPS_TYPE_SETTIME', 'SYNCGROUPS_TYPE_SETBBOX'],
                payloadByType: {
                  SYNCGROUPS_TYPE_SETTIME: {
                    sourceId: 'emptyMapView',
                    origin:
                      'ORIGIN_REACTMAPVIEW_ONMAPCHANGEDIMENSION==> ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION',
                    value: '2023-04-20T06:30:00Z',
                  },
                },
              },
              screen_no_1: {
                types: ['SYNCGROUPS_TYPE_SETTIME', 'SYNCGROUPS_TYPE_SETBBOX'],
                payloadByType: {},
              },
            },
            allIds: ['emptyMapView', 'screen_no_1'],
          },
          groups: {
            byId: {
              'Default group for SYNCGROUPS_TYPE_SETBBOX': {
                title: 'Default group for SYNCGROUPS_TYPE_SETBBOX',
                type: 'SYNCGROUPS_TYPE_SETBBOX',
                payloadByType: {
                  SYNCGROUPS_TYPE_SETTIME: null!,
                  SYNCGROUPS_TYPE_SETBBOX: null!,
                },
                targets: {
                  allIds: [],
                  byId: {},
                },
              },
              'Default group for SYNCGROUPS_TYPE_SETTIME': {
                title: 'Default group for SYNCGROUPS_TYPE_SETTIME',
                type: 'SYNCGROUPS_TYPE_SETTIME',
                payloadByType: {
                  SYNCGROUPS_TYPE_SETTIME: null!,
                  SYNCGROUPS_TYPE_SETBBOX: null!,
                },
                targets: {
                  allIds: [],
                  byId: {},
                },
              },
              SYNCGROUPS_TYPE_SETTIME2: {
                title: 'SYNCGROUPS_TYPE_SETTIME2',
                type: 'SYNCGROUPS_TYPE_SETTIME',
                payloadByType: {
                  SYNCGROUPS_TYPE_SETTIME: null!,
                  SYNCGROUPS_TYPE_SETBBOX: null!,
                },
                targets: {
                  allIds: [],
                  byId: {},
                },
              },
            },
            allIds: [
              'Default group for SYNCGROUPS_TYPE_SETBBOX',
              'Default group for SYNCGROUPS_TYPE_SETTIME',
              'SYNCGROUPS_TYPE_SETTIME2',
            ],
          },
          viewState: {
            timeslider: {
              groups: [
                {
                  id: 'Default group for SYNCGROUPS_TYPE_SETTIME',
                  selected: [],
                },
              ],
              sourcesById: [
                {
                  id: 'emptyMapView',
                  name: 'emptyMapView',
                },
                {
                  id: 'screen_no_1',
                  name: 'screen_no_1',
                },
              ],
            },
            zoompane: {
              groups: [
                {
                  id: 'Default group for SYNCGROUPS_TYPE_SETBBOX',
                  selected: [],
                },
              ],
              sourcesById: [
                {
                  id: 'emptyMapView',
                  name: 'emptyMapView',
                },
                {
                  id: 'screen_no_1',
                  name: 'screen_no_1',
                },
              ],
            },
            level: {
              groups: [],
              sourcesById: [],
            },
          },
          linkedState: {
            links: {
              test: ['test'],
            },
            sharedData: {
              test: {},
            },
          },
          isTimeScrollingEnabled: false,
        },
        webmap: {
          byId: {
            [mosaicNodeId]: {
              id: mosaicNodeId,
              shouldEndtimeOverride: false,
              isAnimating: true,
              isAutoUpdating: true,
              displayMapPin: true,
              shouldShowZoomControls: false,
              timeStep: 1234,
              animationDelay: 250,
              isTimestepAuto: false,
              mapLayers: [],
              overLayers: [],
              baseLayers: [],
              featureLayers: [],
              bbox: {
                left: 0,
                right: 0,
                bottom: 0,
                top: 0,
              },
              srs: '',
            },
          },
          allIds: [mosaicNodeId],
        },
        viewPresets: {
          ids: [mosaicNodeId],
          entities: {
            [mosaicNodeId]: {
              panelId: mosaicNodeId,
              activeViewPresetId: 'screenSat',
              hasChanges: true,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
          },
        },
      };
      expect(
        workspaceSelectors.getViewPresetToSendToBackend(
          mockStore,
          mosaicNodeId,
        ),
      ).toEqual({
        ...emptyViewPreset('Map'),
        id: viewPresetId,
        scope: 'system',
        title: undefined,
        initialProps: {
          mapPreset: mapSelectors.getMapPreset(mockStore, mosaicNodeId),
          syncGroupsIds: syncGroupsSelectors.getAllTargetGroupsForSource(
            mockStore,
            mosaicNodeId,
          ),
        },
      });
    });

    it('should return an empty view if store is empty', () => {
      const mosaicNodeId = 'test';
      const mockStore: AppStore = {};
      expect(
        workspaceSelectors.getViewPresetToSendToBackend(
          mockStore,
          mosaicNodeId,
        ),
      ).toEqual({
        ...emptyViewPreset('Map'),
        id: undefined,
        scope: 'system',
        title: undefined,
        initialProps: {
          mapPreset: mapSelectors.getMapPreset(mockStore, mosaicNodeId),
          syncGroupsIds: syncGroupsSelectors.getAllTargetGroupsForSource(
            mockStore,
            mosaicNodeId,
          ),
        },
      });
    });
  });

  describe('getComponentTypeFromActiveViews', () => {
    it('should return the component types for all windows used in the workspace preset', () => {
      const mockStore: AppStore = {
        workspace: {
          ...initialState,
          views: {
            allIds: ['testviewA', 'testviewB', 'testviewC'],
            byId: {
              testviewA: {
                componentType: 'map',
              },
              testviewB: {
                componentType: 'timeseries',
              },
              testviewC: {
                componentType: 'bla',
              },
            },
          },
          mosaicNode: {
            direction: 'row' as MosaicDirection,
            first: 'testviewA',
            second: {
              direction: 'row' as MosaicDirection,
              first: 'testviewB',
              second: 'testviewC',
            },
          },
        },
      };

      const componentTypes = ['map', 'timeseries', 'bla'];

      expect(
        workspaceSelectors.getComponentTypesFromActiveViews(mockStore),
      ).toEqual(componentTypes);
    });
  });
});
