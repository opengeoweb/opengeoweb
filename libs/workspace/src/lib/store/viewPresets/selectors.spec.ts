/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { LayerType } from '@opengeoweb/webmap';
import * as viewPresetSelectors from './selectors';
import { initialState } from './reducer';
import {
  PresetAction,
  ViewPresetDialog,
  ViewPresetError,
  ViewPresetErrorComponent,
  ViewPresetErrorType,
  ViewPresetsListFilter,
} from './types';
import { ViewPresetListItem } from '../viewPresetsList/types';
import { i18n, translateKeyOutsideComponents } from '../../utils/i18n';
import { AppStore } from '../types';

describe('store/viewPresets/selectors', () => {
  describe('getViewPresetsStore', () => {
    it('should return the viewPresetStore', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {},
          ids: [],
        },
      };

      expect(viewPresetSelectors.getViewPresetsStore(mockStore)).toEqual(
        mockStore.viewPresets,
      );

      expect(
        viewPresetSelectors.getViewPresetsStore(null as unknown as AppStore),
      ).toEqual(initialState);
    });
  });

  describe('getViewPresets', () => {
    it('should return viewPresets', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {
            'map-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
            'map-2': {
              panelId: 'viewPreset-2',
              activeViewPresetId: 'preset-2',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
          },
          ids: ['map-1', 'map-2'],
        },
      };

      expect(viewPresetSelectors.getViewPresets(mockStore)).toEqual(
        mockStore.viewPresets!.ids.map(
          (id) => mockStore.viewPresets!.entities[id],
        ),
      );
    });

    it('should return empty list when no viewPresets in store', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {},
          ids: [],
        },
      };

      expect(viewPresetSelectors.getViewPresets(mockStore)).toEqual([]);
    });
  });

  describe('getViewPresetById', () => {
    it('should return viewPreset by id', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {
            'map-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
            'map-2': {
              panelId: 'viewPreset-2',
              activeViewPresetId: 'preset-2',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
          },
          ids: ['map-1', 'map-2'],
        },
      };

      expect(viewPresetSelectors.getViewPresetById(mockStore, 'map-1')).toEqual(
        mockStore.viewPresets!.entities['map-1'],
      );
      expect(viewPresetSelectors.getViewPresetById(mockStore, 'map-2')).toEqual(
        mockStore.viewPresets!.entities['map-2'],
      );
      expect(
        viewPresetSelectors.getViewPresetById(mockStore, 'map-3'),
      ).toBeUndefined();
    });

    it('should return undefined when preset cannot be found', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {},
          ids: [],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetById(mockStore, 'map-1'),
      ).toBeUndefined();
    });
  });

  describe('getViewPresetsIsFetching', () => {
    it('should return isFetchingViewPresets', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {
            'map-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: false,
              isFetching: true,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
            'map-2': {
              panelId: 'viewPreset-2',
              activeViewPresetId: 'preset-2',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
          },
          ids: ['map-1', 'map-2'],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetsIsFetching(mockStore, 'map-1'),
      ).toBeTruthy();
      expect(
        viewPresetSelectors.getViewPresetsIsFetching(mockStore, 'map-2'),
      ).toBeFalsy();
      expect(
        viewPresetSelectors.getViewPresetsIsFetching(mockStore, 'map-3'),
      ).toBeFalsy();
    });

    it('should return false when preset cannot be found', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {},
          ids: [],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetsIsFetching(mockStore, 'map-1'),
      ).toBeFalsy();

      expect(
        viewPresetSelectors.getViewPresetsIsFetching({}, 'map-1'),
      ).toBeFalsy();
    });
  });

  describe('getViewPresetsError', () => {
    it('should return isFetchingViewPresets', () => {
      const testError = {
        component: ViewPresetErrorComponent.PRESET_LIST,
        message: 'test error',
        errorType: ViewPresetErrorType.GENERIC,
      };
      const mockStore: AppStore = {
        viewPresets: {
          entities: {
            'map-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: false,
              isFetching: true,
              error: testError,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
            'map-2': {
              panelId: 'viewPreset-2',
              activeViewPresetId: 'preset-2',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
          },
          ids: ['map-1', 'map-2'],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetsError(mockStore, 'map-1'),
      ).toEqual(testError);
      expect(
        viewPresetSelectors.getViewPresetsError(mockStore, 'map-2'),
      ).toBeUndefined();
      expect(
        viewPresetSelectors.getViewPresetsError(mockStore, 'map-3'),
      ).toBeUndefined();
    });

    it('should return empty string when preset cannot be found', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {},
          ids: [],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetsError(mockStore, 'map-1'),
      ).toBeUndefined();

      expect(
        viewPresetSelectors.getViewPresetsError({}, 'map-1'),
      ).toBeUndefined();
    });
  });

  describe('getViewPresetDetailError', () => {
    it('should return getViewPresetDetailError', () => {
      const testError: ViewPresetError = {
        component: ViewPresetErrorComponent.PRESET_DETAIL,
        message: 'test error',
        errorType: ViewPresetErrorType.GENERIC,
      };
      const mockStore: AppStore = {
        viewPresets: {
          entities: {
            'map-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: false,
              isFetching: true,
              error: testError,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
            'map-2': {
              panelId: 'viewPreset-2',
              activeViewPresetId: 'preset-2',
              hasChanges: false,
              isFetching: false,
              error: {
                component: ViewPresetErrorComponent.PRESET_LIST,
                message: 'test error',
                errorType: ViewPresetErrorType.GENERIC,
              },
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
          },
          ids: ['map-1', 'map-2'],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetDetailError(mockStore, 'map-1'),
      ).toEqual(testError.message);
      expect(
        viewPresetSelectors.getViewPresetDetailError(mockStore, 'map-2'),
      ).toBeUndefined();
      expect(
        viewPresetSelectors.getViewPresetDetailError(mockStore, 'map-3'),
      ).toBeUndefined();
    });

    it('should return undefined when preset cannot be found', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {},
          ids: [],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetDetailError(mockStore, 'map-1'),
      ).toBeUndefined();

      expect(
        viewPresetSelectors.getViewPresetDetailError({}, 'map-1'),
      ).toBeUndefined();
    });
  });

  describe('getViewPresetHasChanges', () => {
    it('should return if preset has changes', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {
            'map-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: true,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
            'map-2': {
              panelId: 'viewPreset-2',
              activeViewPresetId: 'preset-2',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
          },
          ids: ['map-1', 'map-2'],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetHasChanges(mockStore, 'map-1'),
      ).toBeTruthy();
      expect(
        viewPresetSelectors.getViewPresetHasChanges(mockStore, 'map-2'),
      ).toBeFalsy();
    });

    it('should return undefined when preset cannot be found', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {},
          ids: [],
        },
      };
      expect(
        viewPresetSelectors.getViewPresetHasChanges(mockStore, 'map-1'),
      ).toBeUndefined();
    });
  });

  describe('getViewPresetActiveId', () => {
    it('should return active preset id', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {
            'map-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: true,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
            'map-2': {
              panelId: 'viewPreset-2',
              activeViewPresetId: 'preset-2',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
          },
          ids: ['map-1', 'map-2'],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetActiveId(mockStore, 'map-1'),
      ).toEqual(mockStore.viewPresets!.entities['map-1']!.activeViewPresetId);
      expect(
        viewPresetSelectors.getViewPresetActiveId(mockStore, 'map-2'),
      ).toEqual(mockStore.viewPresets!.entities['map-2']!.activeViewPresetId);
    });

    it('should return undefined when preset cannot be found', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {},
          ids: [],
        },
      };
      expect(
        viewPresetSelectors.getViewPresetActiveId(mockStore, 'map-1'),
      ).toBeUndefined();
    });
  });

  describe('getDialogOptions', () => {
    it('should return dialogOptions', () => {
      const panelId = 'test-1';
      const testViewPresetDialog: ViewPresetDialog = {
        title: translateKeyOutsideComponents(
          i18n.t,
          'workspace-mappreset-save-as',
        ),
        action: PresetAction.SAVE_AS,
        panelId,
        viewPresetId: 'view-1',
        formValues: {
          title: 'my view preset',
          initialProps: {
            mapPreset: {
              layers: [
                {
                  name: 'WorldMap_Light_Grey_Canvas',
                  type: 'twms',
                  dimensions: [],
                  id: 'layerid_46',
                  opacity: 1,
                  enabled: true,
                  layerType: LayerType.baseLayer,
                },
                {
                  service:
                    'https://geoservices.knmi.nl/wms?DATASET=baselayers&',
                  name: 'countryborders',
                  format: 'image/png',
                  dimensions: [],
                  id: 'layerid_55',
                  opacity: 1,
                  enabled: true,
                  layerType: LayerType.overLayer,
                },
              ],
              activeLayerId: 'layerid_53',
              autoTimeStepLayerId: 'layerid_53',
              autoUpdateLayerId: 'layerid_53',
              proj: {
                bbox: {
                  left: -450651.2255879827,
                  bottom: 6490531.093143953,
                  right: 1428345.8183648037,
                  top: 7438773.776232235,
                },
                srs: 'EPSG:3857',
              },
              shouldAnimate: false,
              shouldAutoUpdate: false,
              showTimeSlider: true,
              displayMapPin: false,
              shouldShowZoomControls: true,
              animationPayload: {
                interval: 5,
                speed: 4,
              },
              toggleTimestepAuto: true,
              shouldShowLegend: false,
            },
            syncGroupsIds: [],
          },
        },
      };
      const mockStore: AppStore = {
        viewPresets: {
          entities: {},
          ids: [],
          viewPresetDialog: testViewPresetDialog,
        },
      };

      expect(viewPresetSelectors.getDialogOptions(mockStore)).toEqual(
        testViewPresetDialog,
      );
    });

    it('should return undefined when no viewPresetDialog in store', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {},
          ids: [],
        },
      };

      expect(viewPresetSelectors.getDialogOptions(mockStore)).toBeUndefined();
    });
  });

  describe('getIsViewPresetListDialogOpen', () => {
    it('should return isViewPresetListDialogOpen', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {
            'panel-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: true,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: true,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
            'panel-2': {
              panelId: 'viewPreset-2',
              activeViewPresetId: 'preset-2',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
          },
          ids: ['panel-1', 'panel-2'],
        },
      };

      expect(
        viewPresetSelectors.getIsViewPresetListDialogOpen(mockStore, 'panel-1'),
      ).toBeTruthy();
      expect(
        viewPresetSelectors.getIsViewPresetListDialogOpen(mockStore, 'panel-2'),
      ).toBeFalsy();
      expect(
        viewPresetSelectors.getIsViewPresetListDialogOpen(mockStore, 'panel-3'),
      ).toBeFalsy();
    });

    it('should return false when no viewpresets in store', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {},
          ids: [],
        },
      };

      expect(
        viewPresetSelectors.getIsViewPresetListDialogOpen(mockStore, 'test-1'),
      ).toBeFalsy();
    });
  });

  describe('getViewPresetListFiltersForView', () => {
    it('should return empty array if panelId doesnt exist or if no filters set', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {
            'panel-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: true,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: true,
              filters: [
                {
                  id: 'system',
                  label: 'system',
                  type: 'scope',
                  isSelected: true,
                },
              ],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
            'panel-2': {
              panelId: 'viewPreset-2',
              activeViewPresetId: 'preset-2',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
          },
          ids: ['panel-1', 'panel-2'],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetListFiltersForView(
          mockStore,
          'panel-2',
        ),
      ).toStrictEqual([]);
      expect(
        viewPresetSelectors.getViewPresetListFiltersForView(
          mockStore,
          'panel-2222',
        ),
      ).toStrictEqual([]);
    });

    it('should return filters for the passed view', () => {
      const filterView1: ViewPresetsListFilter[] = [
        {
          id: 'system',
          label: 'system',
          type: 'scope',
          isSelected: true,
        },
      ];
      const filtersView2: ViewPresetsListFilter[] = [
        {
          id: 'system',
          label: 'system',
          type: 'scope',
          isSelected: true,
        },
        {
          id: 'system',
          label: 'user',
          type: 'scope',
          isSelected: false,
        },
      ];
      const mockStore: AppStore = {
        viewPresets: {
          entities: {
            'panel-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: true,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: true,
              filters: filterView1,
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
            'panel-2': {
              panelId: 'viewPreset-2',
              activeViewPresetId: 'preset-2',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: filtersView2,
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
          },
          ids: ['panel-1', 'panel-2'],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetListFiltersForView(
          mockStore,
          'panel-1',
        ),
      ).toStrictEqual(filterView1);
      expect(
        viewPresetSelectors.getViewPresetListFiltersForView(
          mockStore,
          'panel-2',
        ),
      ).toStrictEqual(filtersView2);
    });

    it('should return [] when no viewpresets in store', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {},
          ids: [],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetListFiltersForView(
          mockStore,
          'test-1',
        ),
      ).toStrictEqual([]);
    });
  });

  describe('getViewPresetListSearchQueryForView', () => {
    it('should return empty string if panelId doesnt exist or if no search query set', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {
            'panel-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: true,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: true,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
            'panel-2': {
              panelId: 'viewPreset-2',
              activeViewPresetId: 'preset-2',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
          },
          ids: ['panel-1', 'panel-2'],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetListSearchQueryForView(
          mockStore,
          'panel-2',
        ),
      ).toStrictEqual('');
      expect(
        viewPresetSelectors.getViewPresetListSearchQueryForView(
          mockStore,
          'panel-2222',
        ),
      ).toStrictEqual('');
    });

    it('should return query for the passed view', () => {
      const searchQuery1 = 'Hello, search for me please';
      const searchQuery2 = '  Search  ';
      const mockStore: AppStore = {
        viewPresets: {
          entities: {
            'panel-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: true,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: true,
              filters: [],
              searchQuery: searchQuery1,
              filterResults: { ids: [], entities: {} },
            },
            'panel-2': {
              panelId: 'viewPreset-2',
              activeViewPresetId: 'preset-2',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: searchQuery2,
              filterResults: { ids: [], entities: {} },
            },
          },
          ids: ['panel-1', 'panel-2'],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetListSearchQueryForView(
          mockStore,
          'panel-1',
        ),
      ).toStrictEqual(searchQuery1);
      expect(
        viewPresetSelectors.getViewPresetListSearchQueryForView(
          mockStore,
          'panel-2',
        ),
      ).toStrictEqual(searchQuery2);
    });

    it('should return empty string when no viewpresets in store', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {},
          ids: [],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetListSearchQueryForView(
          mockStore,
          'test-1',
        ),
      ).toStrictEqual('');
    });
  });

  describe('getViewPresetListFilterResultsForView', () => {
    it('should return empty array if panelId doesnt exist or if no filtered list set', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {
            'panel-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: true,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: true,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
            'panel-2': {
              panelId: 'viewPreset-2',
              activeViewPresetId: 'preset-2',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: { ids: [], entities: {} },
            },
          },
          ids: ['panel-1', 'panel-2'],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetListFilterResultsForView(
          mockStore,
          'panel-2',
        ),
      ).toStrictEqual([]);
      expect(
        viewPresetSelectors.getViewPresetListFilterResultsForView(
          mockStore,
          'panel-2222',
        ),
      ).toStrictEqual([]);
    });

    it('should return list of viewpresets for the passed view', () => {
      const filterResultsPanel1 = {
        ids: ['view1'],
        entities: {
          view1: {
            id: 'view1',
            scope: 'system',
            title: 'Layer manager preset 2',
            date: '2022-06-01T12:34:27.787192',
          } as ViewPresetListItem,
        },
      };
      const filterResultsPanel2 = {
        ids: ['view1', 'view2'],
        entities: {
          view1: {
            id: 'view1',
            scope: 'system',
            title: 'Layer manager preset 2',
            date: '2022-06-01T12:34:27.787192',
          } as ViewPresetListItem,
          view2: {
            id: 'view2',
            scope: 'system',
            title: 'Layer manager preset 2',
            date: '2022-06-01T12:34:27.787192',
          } as ViewPresetListItem,
        },
      };
      const mockStore: AppStore = {
        viewPresets: {
          entities: {
            'panel-1': {
              panelId: 'viewPreset-1',
              activeViewPresetId: 'preset-1',
              hasChanges: true,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: true,
              filters: [],
              searchQuery: '',
              filterResults: filterResultsPanel1,
            },
            'panel-2': {
              panelId: 'viewPreset-2',
              activeViewPresetId: 'preset-2',
              hasChanges: false,
              isFetching: false,
              error: undefined,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: filterResultsPanel2,
            },
          },
          ids: ['panel-1', 'panel-2'],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetListFilterResultsForView(
          mockStore,
          'panel-1',
        ),
      ).toStrictEqual(Object.values(filterResultsPanel1.entities));
      expect(
        viewPresetSelectors.getViewPresetListFilterResultsForView(
          mockStore,
          'panel-2',
        ),
      ).toStrictEqual(Object.values(filterResultsPanel2.entities));
    });

    it('should return empty array when no viewpresets in store', () => {
      const mockStore: AppStore = {
        viewPresets: {
          entities: {},
          ids: [],
        },
      };

      expect(
        viewPresetSelectors.getViewPresetListFilterResultsForView(
          mockStore,
          'test-1',
        ),
      ).toStrictEqual([]);
    });
  });
});
