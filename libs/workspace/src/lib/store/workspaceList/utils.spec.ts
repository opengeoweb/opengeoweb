/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { WorkspacePresetAction } from '../workspace/types';
import { WorkspaceListFilter } from './types';
import { constructFilterParams, getSnackbarMessage } from './utils';

import { getCleanSearchQuery } from '../../utils/utils';

describe('getSnackbarMessage', () => {
  it('should return correct message for passed action', () => {
    const title = 'workspace title';
    expect(getSnackbarMessage(WorkspacePresetAction.DELETE, title).key).toBe(
      'workspace-list-success-delete',
    );
    expect(getSnackbarMessage(WorkspacePresetAction.DUPLICATE, title).key).toBe(
      'workspace-list-success-duplicate',
    );
    expect(getSnackbarMessage(WorkspacePresetAction.EDIT, title).key).toBe(
      'workspace-list-success-edit',
    );

    expect(
      getSnackbarMessage(WorkspacePresetAction.EDIT, title).params,
    ).toEqual({
      TITLE: title,
    });
    expect(
      getSnackbarMessage('FAKE_ACTION' as WorkspacePresetAction, title).key,
    ).toBe('workspace-list-success-save');
  });
});

describe('constructFilterParams', () => {
  it('should return empty params object if no selected presets', () => {
    expect(
      constructFilterParams(
        [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
        ],
        '',
      ),
    ).toStrictEqual({});
  });
  it('should return empty params object if no scope filters passed', () => {
    expect(constructFilterParams([], '')).toStrictEqual({});
  });
  it('should return string of scope filters', () => {
    expect(
      constructFilterParams(
        [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ],
        '',
      ),
    ).toStrictEqual({
      scope: 'user,system',
    });

    expect(
      constructFilterParams(
        [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ],
        '',
      ),
    ).toStrictEqual({
      scope: 'system',
    });
  });

  it('should ignore other types than scope', () => {
    expect(
      constructFilterParams(
        [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ],
        '',
      ),
    ).toStrictEqual({
      scope: 'user,system',
    });

    expect(
      constructFilterParams(
        [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'keyword',
            isSelected: true,
            isDisabled: false,
          },
        ],
        '',
      ),
    ).toStrictEqual({
      scope: 'user',
    });
  });

  it('should handle search query', () => {
    const testFilters: WorkspaceListFilter[] = [
      {
        label: 'My presets',
        id: 'user',
        type: 'scope',
        isSelected: true,
        isDisabled: false,
      },
      {
        label: 'System presets',
        id: 'system',
        type: 'scope',
        isSelected: true,
        isDisabled: false,
      },
    ];
    const testSearchQuery = 'testing, test';
    expect(constructFilterParams(testFilters, testSearchQuery)).toStrictEqual({
      scope: 'user,system',
      search: getCleanSearchQuery(testSearchQuery),
    });

    expect(constructFilterParams([], testSearchQuery)).toStrictEqual({
      search: getCleanSearchQuery(testSearchQuery),
    });
  });
});
