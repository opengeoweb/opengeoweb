/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { AppStore } from '../types';
import { WorkspacePresetAction } from '../workspace/types';
import * as viewPresetSelectors from './selectors';
import {
  WorkspaceListErrorCategory,
  WorkspaceDialogView,
  WorkspaceListErrorType,
} from './types';

describe('store/workspaceList/selectors', () => {
  describe('getWorkspaceListStore', () => {
    it('should return the viewstore', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: false,
          isWorkspaceListDialogOpen: false,
          entities: {},
          ids: [],
        },
      };

      expect(viewPresetSelectors.getWorkspaceListStore(mockStore)).toEqual(
        mockStore.workspaceList,
      );

      expect(viewPresetSelectors.getWorkspaceListStore(null!)).toBeNull();
    });
  });

  describe('getWorkspaceList', () => {
    it('should return workspaceList', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: false,
          isWorkspaceListDialogOpen: false,
          entities: {
            preset: {
              id: 'mapPreset-1',
              scope: 'system',
              title: 'Layer manager preset',
              date: '2022-06-01T12:34:27.787192',
              viewType: 'singleWindow',
              abstract: '',
            },
            preset_2: {
              id: 'mapPreset-2',
              scope: 'system',
              title: 'Layer manager preset 2',
              date: '2022-06-01T12:34:27.787192',
              viewType: 'singleWindow',
              abstract: '',
            },
          },
          ids: ['preset', 'preset_2'],
        },
      };

      expect(viewPresetSelectors.getWorkspaceList(mockStore)).toEqual(
        mockStore.workspaceList!.ids.map(
          (id) => mockStore.workspaceList!.entities[id],
        ),
      );
    });

    it('should return empty list when no workspaceList in store', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: false,
          isWorkspaceListDialogOpen: false,
          entities: {},
          ids: [],
        },
      };

      expect(viewPresetSelectors.getWorkspaceList(mockStore)).toEqual([]);
    });
  });

  describe('getIsWorkspaceListFetching', () => {
    it('should return isFetching', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: true,
          isWorkspaceListDialogOpen: false,
          entities: {
            preset: {
              id: 'mapPreset-1',
              scope: 'system',
              title: 'Layer manager preset',
              date: '2022-06-01T12:34:27.787192',
              viewType: 'singleWindow',
              abstract: '',
            },
            preset_2: {
              id: 'mapPreset-2',
              scope: 'system',
              title: 'Layer manager preset 2',
              date: '2022-06-01T12:34:27.787192',
              viewType: 'singleWindow',
              abstract: '',
            },
          },
          ids: ['preset', 'preset_2'],
        },
      };

      expect(
        viewPresetSelectors.getIsWorkspaceListFetching(mockStore),
      ).toBeTruthy();
    });
    it('should return isFetching false if store is not set', () => {
      const mockStore: AppStore = {};

      expect(
        viewPresetSelectors.getIsWorkspaceListFetching(mockStore),
      ).toBeFalsy();
    });
  });

  describe('getWorkspaceActionDialogDetails', () => {
    it('should return false if store is not set', () => {
      const mockStore: AppStore = {};

      expect(
        viewPresetSelectors.getWorkspaceActionDialogDetails(mockStore),
      ).toBeFalsy();
    });
    it('should return false if no details set', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: true,
          isWorkspaceListDialogOpen: false,
          entities: {
            preset: {
              id: 'mapPreset-1',
              scope: 'system',
              title: 'Layer manager preset',
              date: '2022-06-01T12:34:27.787192',
              viewType: 'singleWindow',
              abstract: '',
            },
          },
          ids: ['preset'],
        },
      };

      expect(
        viewPresetSelectors.getWorkspaceActionDialogDetails(mockStore),
      ).toBeFalsy();
    });

    it('should return dialog details if set', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: true,
          isWorkspaceListDialogOpen: false,
          workspaceActionDialog: {
            action: WorkspacePresetAction.DELETE,
            presetId: 'someId',
            formValues: { title: 'Workspace 1' },
          },
          entities: {
            preset: {
              id: 'mapPreset-1',
              scope: 'system',
              title: 'Layer manager preset',
              date: '2022-06-01T12:34:27.787192',
              viewType: 'singleWindow',
              abstract: '',
            },
          },
          ids: ['preset'],
        },
      };

      expect(
        viewPresetSelectors.getWorkspaceActionDialogDetails(mockStore),
      ).toEqual(mockStore.workspaceList!.workspaceActionDialog);
    });

    it('should return dialog details with views', () => {
      const mockStore: AppStore = {
        workspace: {
          title: 'Radar and temperature',
          views: {
            allIds: ['screen_no_1', '1_screen'],
            byId: {
              screen_no_1: {
                id: 'radar',
                title: 'Radar',
                componentType: 'Map',
                initialProps: {
                  mapPreset: {
                    layers: [
                      {
                        service:
                          'https://geoservices.knmi.nl/wms?dataset=RADAR',
                        name: 'RAD_NL25_PCP_CM',
                        format: 'image/png',
                        style: 'precip-blue-transparent/nearest',
                        enabled: true,
                        layerType: 'mapLayer',
                      },
                    ],
                    proj: {
                      bbox: {
                        left: 58703.6377,
                        bottom: 6408480.4514,
                        right: 3967387.5161,
                        top: 11520588.9031,
                      },
                      srs: 'EPSG:3857',
                    },
                    activeLayerId: 'radar_precipitation_intensity_nordic_id',
                    toggleTimestepAuto: false,
                    animationPayload: {
                      duration: 240,
                      interval: 5,
                      speed: 2,
                      endTime: 'NOW+PT2H00M',
                      shouldEndtimeOverride: true,
                    },
                    shouldAutoUpdate: false,
                    shouldAnimate: true,
                  },
                },
              },
              '1_screen': {
                componentType: 'Map',
                initialProps: {
                  mapPreset: {
                    proj: {
                      bbox: {
                        left: 1209123.7130317688,
                        bottom: 5572898.4807883985,
                        right: 5117807.591431769,
                        top: 10685006.9324884,
                      },
                      srs: 'EPSG:3857',
                    },
                  },
                  syncGroupsIds: [],
                },
              },
            },
          },
          mosaicNode: {
            direction: 'row',
            first: 'screen_no_1',
            second: '1_screen',
          },
          isLoading: false,
          hasChanges: true,
          abstract: '',
          scope: 'system',
          syncGroups: [
            {
              id: 'Area_RadarTemp',
              type: 'SYNCGROUPS_TYPE_SETBBOX',
            },
            {
              id: 'Time_RadarTemp',
              type: 'SYNCGROUPS_TYPE_SETTIME',
            },
          ],
          viewType: 'multiWindow',
        },
        workspaceList: {
          error: undefined,
          isFetching: false,
          isWorkspaceListDialogOpen: false,
          ids: ['screenConfigRadarTemp', 'screenConfigRadarRadar'],
          entities: {
            screenConfigRadarTemp: {
              id: 'screenConfigRadarTemp',
              title: 'Radar and temperature',
              date: '2022-06-01T12:34:27.787184',
              scope: 'system',
              abstract: 'Radar and temperature maps',
              viewType: 'multiWindow',
            },
            screenConfigRadarRadar: {
              id: 'screenConfigRadarRadar',
              title: 'Radar and Radar',
              date: '2022-06-01T12:34:27.787184',
              scope: 'system',
              abstract: '',
              viewType: 'multiWindow',
            },
          },
          searchQuery: '',
          workspaceListFilters: [],
          workspaceActionDialog: {
            presetId: 'te',
            action: WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS,
            formValues: {
              title: 'Radar and temperature',
              syncGroups: [
                {
                  id: 'Area_RadarTemp',
                  type: 'SYNCGROUPS_TYPE_SETBBOX',
                },
                {
                  id: 'Time_RadarTemp',
                  type: 'SYNCGROUPS_TYPE_SETTIME',
                },
              ],
              views: [
                {
                  mosaicNodeId: 'screen_no_1',
                  viewPresetId: 'radar',
                  isSelected: false,
                  scope: 'system',
                },
                {
                  mosaicNodeId: '1_screen',
                  viewPresetId: 'emptyMap',
                  isSelected: false,
                  scope: 'user',
                },
              ] as WorkspaceDialogView[],
              viewType: 'multiWindow',
              scope: 'system',
              abstract: '',
              mosaicNode: {
                direction: 'row',
                first: 'screen_no_1',
                second: '1_screen',
              },
            },
            isFetching: false,
            hasSaved: false,
          },
        },

        viewPresets: {
          ids: ['screen_no_1', '1_screen'],
          entities: {
            screen_no_1: {
              panelId: 'screen_no_1',
              activeViewPresetId: 'radar',
              hasChanges: true,
              isFetching: false,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: {
                ids: [],
                entities: {},
              },
              error: undefined,
            },
            '1_screen': {
              panelId: '1_screen',
              activeViewPresetId: '',
              hasChanges: true,
              isFetching: false,
              isViewPresetListDialogOpen: false,
              filters: [],
              searchQuery: '',
              filterResults: {
                ids: [],
                entities: {},
              },
              error: undefined,
            },
          },
        },
      };

      expect(
        viewPresetSelectors.getWorkspaceActionDialogDetails(mockStore),
      ).toEqual({
        ...mockStore.workspaceList!.workspaceActionDialog,
        formValues: {
          ...mockStore.workspaceList!.workspaceActionDialog?.formValues,
          views: [
            {
              error: undefined,
              hasChanges: true,
              isFetching: false,
              isSelected: false,
              mosaicNodeId: 'screen_no_1',
              title: 'Radar',
              viewPresetId: 'radar',
              scope: 'system',
            },
            {
              error: undefined,
              hasChanges: true,
              isFetching: false,
              isSelected: false,
              mosaicNodeId: '1_screen',
              title: undefined,
              viewPresetId: 'emptyMap',
              scope: 'user',
            },
          ],
        },
      });
    });
  });

  describe('getWorkspaceListError', () => {
    it('should return error', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: {
            category: WorkspaceListErrorCategory.GENERIC,
            message: 'Failed to fetch workspace list',
            type: WorkspaceListErrorType.VERBATIM_ERROR,
          },
          isFetching: true,
          isWorkspaceListDialogOpen: false,
          entities: {},
          ids: [],
        },
      };

      expect(viewPresetSelectors.getWorkspaceListError(mockStore)).toEqual(
        mockStore.workspaceList!.error,
      );
    });
    it('should return undefined if store is not set', () => {
      const mockStore: AppStore = {};

      expect(
        viewPresetSelectors.getWorkspaceListError(mockStore),
      ).toBeUndefined();
    });
  });

  describe('getIsWorkspaceListDialogOpen', () => {
    it('should isListDialogOpen for closed listDialog', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: true,
          isWorkspaceListDialogOpen: false,
          entities: {},
          ids: [],
        },
      };

      expect(
        viewPresetSelectors.getIsWorkspaceListDialogOpen(mockStore),
      ).toBeFalsy();
    });
    it('should isListDialogOpen for open listDialog', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: true,
          isWorkspaceListDialogOpen: true,
          entities: {},
          ids: [],
        },
      };

      expect(
        viewPresetSelectors.getIsWorkspaceListDialogOpen(mockStore),
      ).toBeTruthy();
    });
    it('should return false if store is not set', () => {
      const mockStore: AppStore = {};

      expect(
        viewPresetSelectors.getIsWorkspaceListDialogOpen(mockStore),
      ).toBeFalsy();
    });
  });

  describe('getWorkspaceListFilters', () => {
    it('should return [] if workspaceListFilters not set', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: true,
          isWorkspaceListDialogOpen: false,
          entities: {},
          ids: [],
        },
      };

      expect(
        viewPresetSelectors.getWorkspaceListFilters(mockStore),
      ).toStrictEqual([]);
    });
    it('should return filters if set', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: true,
          isWorkspaceListDialogOpen: true,
          entities: {},
          ids: [],
          workspaceListFilters: [
            { id: 'system', label: 'system', type: 'scope', isSelected: true },
            { id: 'user', label: 'user', type: 'scope', isSelected: true },
          ],
        },
      };

      expect(viewPresetSelectors.getWorkspaceListFilters(mockStore)).toBe(
        mockStore.workspaceList!.workspaceListFilters,
      );
    });

    it('should not return user filters for admin', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: true,
          isWorkspaceListDialogOpen: true,
          entities: {},
          ids: [],
          workspaceListFilters: [
            { id: 'system', label: 'system', type: 'scope', isSelected: true },
            { id: 'user', label: 'user', type: 'scope', isSelected: true },
          ],
        },
      };

      expect(
        viewPresetSelectors.getWorkspaceListFilters(mockStore, 'system'),
      ).toEqual([
        {
          ...mockStore.workspaceList!.workspaceListFilters![0],
          isDisabled: true,
        },
      ]);
    });

    it('should return empty array if store is not set', () => {
      const mockStore: AppStore = {};

      expect(
        viewPresetSelectors.getWorkspaceListFilters(mockStore),
      ).toStrictEqual([]);
    });
  });
});
