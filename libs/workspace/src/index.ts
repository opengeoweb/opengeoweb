/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import workspaceTranslations from '../locales/workspace.json';
import { workspaceListener } from './lib/store/workspace/listener';

export { workspaceListener };
export { workspaceListListener } from './lib/store/workspaceList/listener';
export { viewPresetsListener } from './lib/store/viewPresets/listener';
export {
  workspaceActions,
  workspaceReducer,
} from './lib/store/workspace/reducer';
export {
  workspaceListActions,
  workspaceListReducer,
} from './lib/store/workspaceList/reducer';
export { viewPresetActions, viewPresetsReducer } from './lib/store/viewPresets';
export { viewPresetsListReducer } from './lib/store/viewPresetsList';
export * as selectors from './lib/store/workspace/selectors';

export * from './lib/store/workspace/types';
export * from './lib/store/viewPresets/types';
export * from './lib/components/Providers';
export * from './lib/components/ComponentsLookUpWrapper';
export * from './lib/components/WorkspaceView';
export * from './lib/components/WorkspaceTopBar';
export * from './lib/components/WorkspaceDetail';
export { WORKSPACE_NAMESPACE } from './lib/utils/i18n';
export { workspaceTranslations };

export {
  routes as workspaceRoutes,
  getWorkspaceRouteUrl,
} from './lib/utils/routes';
export {
  getEmptyMapWorkspace,
  createCustomEmptyMapWorkspace,
} from './lib/store/workspaceList/utils';
export type { WorkspaceModuleStore } from './lib/store/types';
export { workspaceReducersMap, workspaceMiddlewares } from './lib/store';
