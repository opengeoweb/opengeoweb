/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { produce } from 'immer';
import { AnyAction } from '@reduxjs/toolkit';
import { uiReducer, initialState, moveToTop, uiActions } from './reducer';
import { DialogType, DialogTypes, UIStoreType } from './types';

describe('store/ui/reducer', () => {
  it('should move passed item to front of array', () => {
    const testList = [1, 2, 3];
    expect(moveToTop(testList, 3)).toEqual([3, 1, 2]);
    expect(moveToTop(testList, 1)).toEqual([1, 2, 3]);
    expect(moveToTop(testList, 2)).toEqual([2, 1, 3]);
    expect(moveToTop(['1', '2', '3'], '2')).toEqual(['2', '1', '3']);
  });

  it('should return initial state if no state and action passed in', () => {
    expect(uiReducer(undefined, {} as AnyAction)).toEqual(initialState);
  });

  it('should register a dialog', () => {
    const type = 'legend' as DialogType;
    const result = uiReducer(
      undefined,
      uiActions.registerDialog({
        type,
      }),
    );
    expect(Object.keys(result.dialogs).length).toBe(1);
    expect(result.dialogs[type]!.isOpen).toBe(false);
    expect(result.dialogs[type]!.activeMapId).toBe('');
    expect(result.dialogs[type]!.source).toBe('app');
    expect(result.order).toContain(type);
  });

  it('should register a dialog with a passed activeMapId and set to open', () => {
    const type = 'legend' as DialogType;
    const result = uiReducer(
      undefined,
      uiActions.registerDialog({
        type,
        mapId: 'map1',
        setOpen: true,
      }),
    );
    expect(Object.keys(result.dialogs).length).toBe(1);
    expect(result.dialogs[type]!.isOpen).toBe(true);
    expect(result.dialogs[type]!.activeMapId).toBe('map1');
  });

  it('should not register a dialog that already exists', () => {
    const mockStore: UIStoreType = {
      dialogs: {
        legend: {
          type: 'legend' as DialogType,
          activeMapId: 'map1',
          isOpen: false,
        },
        layerManager: {
          type: DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
      order: ['legend', DialogTypes.LayerManager],
    };
    const result = uiReducer(
      mockStore,
      uiActions.registerDialog({
        type: 'legend' as DialogType,
        setOpen: true,
        mapId: 'map1',
      }),
    );
    expect(Object.keys(result.dialogs).length).toBe(2);
    expect(Object.keys(result.dialogs)).toMatchObject([
      'legend',
      DialogTypes.LayerManager,
    ]);
    expect(result.order).toEqual(mockStore.order);
  });

  it('should unregister a dialog', () => {
    const mockStore: UIStoreType = {
      dialogs: {
        legend: {
          type: 'legend' as DialogType,
          activeMapId: 'map1',
          isOpen: false,
        },
        layerManager: {
          type: DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
      order: ['legend', DialogTypes.LayerManager],
    };
    const result = uiReducer(
      mockStore,
      uiActions.unregisterDialog({
        type: 'legend' as DialogType,
      }),
    );

    expect(Object.keys(result.dialogs).length).toBe(1);
    expect(Object.keys(result.dialogs)).toMatchObject([
      DialogTypes.LayerManager,
    ]);
    expect(result.order).toHaveLength(1);
    expect(result.order).toContain(DialogTypes.LayerManager);
    expect(result.order).not.toContain('legend');
  });

  it('should unregister nothing if passed in dialog is not present', () => {
    const mockStore = {
      dialogs: {
        legend: {
          type: 'legend' as DialogType,
          activeMapId: 'map1',
          isOpen: false,
        },
        layerManager: {
          type: DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
      order: [],
    };
    const result = uiReducer(
      mockStore,
      uiActions.unregisterDialog({
        type: 'dimensionSelect-notpresent' as DialogType,
      }),
    );

    expect(Object.keys(result.dialogs).length).toBe(2);
    expect(Object.keys(result.dialogs)).toMatchObject([
      'legend',
      DialogTypes.LayerManager,
    ]);
  });

  it('should set an active map id for a dialog', () => {
    const mockStore: UIStoreType = {
      dialogs: {
        legend: {
          type: 'legend' as DialogType,
          activeMapId: 'map1',
          isOpen: false,
          source: 'app',
        },
        layerManager: {
          type: DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
          source: 'app',
        },
      },
      order: [],
    };

    // Passing in an invalid dialogid should return the store
    const resultInvalid = uiReducer(
      mockStore,
      uiActions.setActiveMapIdForDialog({
        type: DialogTypes.DimensionSelectElevation,
        mapId: 'newMap',
      }),
    );
    expect(Object.keys(resultInvalid).length).toBe(2);
    expect(resultInvalid).toMatchObject(mockStore);

    const result = uiReducer(
      mockStore,
      uiActions.setActiveMapIdForDialog({
        type: 'legend',
        mapId: 'newMap',
      }),
    );

    expect(Object.keys(result).length).toBe(2);
    expect(result.dialogs['legend']!.isOpen).toBe(false);
    expect(result.dialogs['legend']!.activeMapId).toBe('newMap');
  });

  it('should set an active map id and open if setOpen is set to true for a dialog', () => {
    const mockStore = {
      dialogs: {
        legend: {
          type: 'legend' as DialogType,
          activeMapId: 'map1',
          isOpen: false,
        },
        layerManager: {
          type: DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
      order: [],
    };

    const result = uiReducer(
      mockStore,
      uiActions.setActiveMapIdForDialog({
        type: 'legend',
        mapId: 'newMap',
        setOpen: true,
      }),
    );

    expect(Object.keys(result.dialogs).length).toBe(2);
    expect(result.dialogs['legend']!.isOpen).toBe(true);
    expect(result.dialogs['legend']!.activeMapId).toBe('newMap');
    expect(result.order).toContain('legend');

    const result2 = uiReducer(
      mockStore,
      uiActions.setActiveMapIdForDialog({
        type: 'legend',
        mapId: 'newMap2',
        setOpen: true,
      }),
    );

    expect(Object.keys(result2.dialogs).length).toBe(2);
    expect(result2.dialogs['legend']!.isOpen).toBeTruthy();
    expect(result2.dialogs['legend']!.activeMapId).toBe('newMap2');
    expect(result2.order.indexOf('legend')).toBe(0);
  });

  it('should open/close a dialog', () => {
    const mockStore: UIStoreType = {
      dialogs: {
        legend: {
          type: 'legend' as DialogType,
          activeMapId: 'map1',
          isOpen: false,
        },
        layerManager: {
          type: DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
      order: [DialogTypes.LayerManager, 'legend'],
    };
    // Passing in an invalid dialogid should return the store
    const resultInvalid = uiReducer(
      mockStore,
      uiActions.setToggleOpenDialog({
        type: DialogTypes.DimensionSelectElevation,
        setOpen: false,
      }),
    );
    expect(Object.keys(resultInvalid.dialogs).length).toBe(2);
    expect(resultInvalid).toMatchObject(mockStore);

    const result = uiReducer(
      mockStore,
      uiActions.setToggleOpenDialog({
        type: 'legend',
        setOpen: true,
        sourcePanelId: 'panel-1',
      }),
    );

    expect(Object.keys(result.dialogs).length).toBe(2);
    expect(result.dialogs['legend']!.isOpen).toBe(true);
    expect(result.dialogs['legend']!.sourcePanelId).toBe('panel-1');
    // should move to top
    expect(result.order.indexOf('legend')).toBe(0);

    const result2 = uiReducer(
      mockStore,
      uiActions.setToggleOpenDialog({
        type: DialogTypes.LayerManager,
        setOpen: false,
        sourcePanelId: 'panel-2',
      }),
    );

    expect(Object.keys(result2.dialogs).length).toBe(2);
    expect(result2.dialogs[DialogTypes.LayerManager]!.isOpen).toBe(false);
    expect(result2.dialogs[DialogTypes.LayerManager]!.sourcePanelId).toBe(
      'panel-2',
    );
  });

  it('should order a dialog', () => {
    const mockStore: UIStoreType = {
      dialogs: {
        legend: {
          type: 'legend' as DialogType,
          activeMapId: 'map1',
          isOpen: false,
        },
        layerManager: {
          type: DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
      order: ['legend', DialogTypes.LayerManager],
    };
    const result = uiReducer(
      mockStore,
      uiActions.orderDialog({
        type: DialogTypes.LayerManager,
      }),
    );
    expect(result.order.indexOf(DialogTypes.LayerManager)).toEqual(0);
    expect(result.order.indexOf('legend')).toEqual(1);

    const result2 = uiReducer(
      mockStore,
      uiActions.orderDialog({
        type: 'legend',
      }),
    );

    expect(result2.order.indexOf(DialogTypes.LayerManager)).toEqual(1);
    expect(result2.order.indexOf('legend')).toEqual(0);
  });

  it('should set active window id', () => {
    const mockStore: UIStoreType = {
      dialogs: {
        layerManager: {
          type: DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
      order: [DialogTypes.LayerManager],
    };
    const expectedState = produce(mockStore, (draft) => {
      draft.activeWindowId = 'activeWindowId';
    });
    expect(
      uiReducer(
        mockStore,
        uiActions.setActiveWindowId({ activeWindowId: 'activeWindowId' }),
      ),
    ).toEqual(expectedState);
  });

  it('should toggle loading ', () => {
    const mockStore: UIStoreType = {
      dialogs: {
        layerManager: {
          type: DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
          isLoading: false,
        },
      },
      order: [DialogTypes.LayerManager],
    };

    expect(
      uiReducer(
        mockStore,
        uiActions.toggleIsLoadingDialog({
          type: DialogTypes.LayerManager,
          isLoading: true,
        }),
      ).dialogs.layerManager?.isLoading,
    ).toBeTruthy();
  });

  it('should toggle loading to false', () => {
    const mockStore: UIStoreType = {
      dialogs: {
        layerManager: {
          type: DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
          isLoading: true,
        },
      },
      order: [DialogTypes.LayerManager],
    };

    expect(
      uiReducer(
        mockStore,
        uiActions.toggleIsLoadingDialog({
          type: DialogTypes.LayerManager,
          isLoading: false,
        }),
      ).dialogs.layerManager?.isLoading,
    ).toBeFalsy();
  });

  it('should set error', () => {
    const mockStore: UIStoreType = {
      dialogs: {
        layerManager: {
          type: DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
      order: [DialogTypes.LayerManager],
    };
    const testError = 'this is a test error';

    expect(
      uiReducer(
        mockStore,
        uiActions.setErrorDialog({
          type: DialogTypes.LayerManager,
          error: testError,
        }),
      ).dialogs.layerManager?.error,
    ).toEqual(testError);
  });

  it('should clear error', () => {
    const mockStore: UIStoreType = {
      dialogs: {
        layerManager: {
          type: DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
          error: 'this is a test error',
        },
      },
      order: [DialogTypes.LayerManager],
    };

    expect(
      uiReducer(
        mockStore,
        uiActions.setErrorDialog({
          type: DialogTypes.LayerManager,
          error: '',
        }),
      ).dialogs.layerManager?.error,
    ).toEqual('');
  });
});
