/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { act, renderHook } from '@testing-library/react';
import React from 'react';
import { Provider } from 'react-redux';
import { SetupDialogReturnValue, useSetupDialog } from './useSetupDialog';
import { uiTypes, uiActions } from '../..';
import { createMockStore } from '../store';

describe('hooks/useSetupDialog/useSetupDialog', () => {
  const dialogType = uiTypes.DialogTypes.TimeSeriesManager;
  it('should register the dialog and return dialog order', async () => {
    const store = createMockStore();

    const { result } = renderHook(() => useSetupDialog(dialogType), {
      wrapper: ({ children }) => <Provider store={store}>{children}</Provider>,
    });

    // register dialog
    await act(() =>
      store.dispatch(
        uiActions.registerDialog({
          type: dialogType,
          source: 'app',
          setOpen: false,
        }),
      ),
    );

    const expected: SetupDialogReturnValue = {
      dialogOrder: 0,
      isDialogOpen: false,
      onCloseDialog: expect.any(Function),
      setDialogOrder: expect.any(Function),
      uiSource: 'app',
      uiIsLoading: false,
      uiError: '',
    };

    expect(result.current).toEqual(expected);

    result.current.setDialogOrder();

    await act(() =>
      store.dispatch(
        uiActions.orderDialog({
          type: dialogType,
        }),
      ),
    );

    result.current.onCloseDialog();

    await act(() =>
      store.dispatch(
        uiActions.setToggleOpenDialog({
          type: dialogType,
          setOpen: false,
        }),
      ),
    );

    expect(result.current.dialogOrder).toEqual(expected.dialogOrder);
  });
});
