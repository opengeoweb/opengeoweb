/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { setupServer } from 'msw/node';
import { configureStore } from '@reduxjs/toolkit';
import { act, waitFor } from '@testing-library/react';
import { http, HttpResponse } from 'msw';
import { serviceActions } from './reducer';
import { serviceTypes } from '../..';

import { serviceListener } from './listener';
import { createMockStore, storeReducerMap } from '../../store';

describe('fetchService', () => {
  const testService = {
    id: 'serviceid_12',
    name: 'msgrt',
    serviceUrl:
      'https://adaguc-server-msg-cpp-portal.pmc.knmi.cloud/wms?DATASET=msgrt&',
    scope: 'user' as const,
    abstract:
      'This service demonstrates how the ADAGUC server can be used to create OGC services.',
    layers: [
      {
        name: 'test-layer-1',
        title: 'test layer 1',
        leaf: true,
        path: ['path1', 'path2'],
        keywords: ['keyword1', 'keyword2'],
      },
    ],
  };

  it('should get layers from service and set serviceLayers', async () => {
    const store = configureStore({
      reducer: { ...storeReducerMap },
      middleware: [serviceListener.middleware],
    });

    act(() => {
      store.dispatch(
        serviceActions.serviceSetLayers({
          ...testService,
        }),
      );
    });

    await waitFor(() => {
      expect(store.getState().services.byId['serviceid_12']).toEqual(
        testService,
      );
    });
  });
});

describe('fetchInitialServices', () => {
  const testServices = [
    {
      id: 'serviceid_11',
      name: 'DWD GeoServer WMSs',
      url: 'https://maps.dwd.de/geoserver/dwd/Hubba/ows',
      scope: 'user' as const,
      abstract: 'This is the Web Map Server of DWD.',
    },
  ];

  it('should fetch all services', async () => {
    const mockServiceUrl = 'https://maps.dwd.de/geoserver/dwd/Hubba/ows';
    const mockLayer = {
      service: mockServiceUrl,
      name: 'test-dwd-layer-1',
      format: 'image/png',
      style: 'radar/nearest',
      enabled: true,
      layerType: 'mapLayer',
    };
    const REFERENCE_TIME = `reference_time`;
    const server = setupServer(
      ...[
        http.get(mockServiceUrl, () => {
          const capabilities = `
<WMS_Capabilities>
      <Capability>
          <Layer>
              <Layer>
                  <Name>${mockLayer.name}</Name>
                  <Dimension name="${REFERENCE_TIME}">
                      2024-08-19T12:00:00
                  </Dimension>
              </Layer>
          </Layer>
      </Capability>
  </WMS_Capabilities>
`;

          return HttpResponse.xml(capabilities);
        }),
      ],
    );
    server.listen();

    const store = createMockStore();

    await waitFor(() => {
      expect(store.getState().services.allIds.length).toBe(0);
    });

    act(() => {
      store.dispatch(
        serviceActions.fetchInitialServices({
          services: testServices as serviceTypes.InitialService[],
        }),
      );
    });

    await waitFor(() => {
      expect(store.getState().services.allIds.length).toBe(1);
    });

    expect(store.getState().services.byId['serviceid_11'].name).toEqual(
      'DWD GeoServer WMSs',
    );

    expect(store.getState().services.byId['serviceid_11'].serviceUrl).toEqual(
      'https://maps.dwd.de/geoserver/dwd/Hubba/ows',
    );

    expect(store.getState().services.byId['serviceid_11'].layers!.length).toBe(
      1,
    );

    expect(store.getState().services.byId['serviceid_11'].layers![0].name).toBe(
      'test-dwd-layer-1',
    );
  });
});
