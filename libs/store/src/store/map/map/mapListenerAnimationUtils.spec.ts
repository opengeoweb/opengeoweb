/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  WMImage,
  WMJSMap,
  WMLayer,
  mockGetCapabilities,
  webmapTestSettings,
  registerWMJSMap,
  setWMSGetCapabilitiesFetcher,
  LayerOptions,
} from '@opengeoweb/webmap';
import * as immer from 'immer';
import {
  MAX_NUMBER_STEPS_FORWARD_TO_PREFETCH,
  getCurrentStep,
  getNextStep,
  handleTimerDwell,
  prefetchAnimationTargetsForMetronome,
  setStep,
} from './mapListenerAnimationUtils';

describe('timerStepFunctions', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });
  it('getCurrentStep should return the first step when not initialized', () => {
    expect(getCurrentStep('timerId-notexistingB')).toBe(0);
  });
  it('getNextStep should return the next step when not initialized', () => {
    expect(getNextStep('timerId-notexistingA', 10)).toBe(1);
  });
  it('getNextStep should return the next step when initialized', () => {
    setStep('timerIdA', 5);
    expect(getNextStep('timerIdA', 10)).toBe(6);
  });
  it('getNextStep should return the first step when it reached the end', () => {
    setStep('timerIdA', 9);
    expect(getNextStep('timerIdA', 10)).toBe(0);
  });
  it('getNextStep should return the first step when it reached the end multiple times', () => {
    setStep('timerIdA', 5);
    expect(getNextStep('timerIdA', 10, 102)).toBe(7);
  });
  it('getNextStep should return the previous step when it reached the beginning once', () => {
    setStep('timerIdA', 0);
    expect(getNextStep('timerIdA', 10, -1)).toBe(9);
  });
  it('getNextStep should return the previous step when it reached the beginning multiple times', () => {
    setStep('timerIdA', 5);
    expect(getNextStep('timerIdA', 10, -102)).toBe(3);
  });
});

describe('prefetchAnimationTargetsForMetronome', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });
  const tStart = '2020-03-13T13:00:00.000Z';
  const tEnd = '2020-03-13T16:00:00.000Z';
  const animationList = [
    { name: 'time', value: tStart },
    { name: 'time', value: '2020-03-13T14:00:00.000Z' },
    { name: 'time', value: '2020-03-13T15:00:00.000Z' },
    { name: 'time', value: tEnd },
  ];
  const currentTime = '2020-03-13T13:00:00.000Z';
  const layer0oneHourTimeStep = immer.produce(
    webmapTestSettings.defaultReduxLayerRadarKNMIWithStyles as LayerOptions,
    (draft) => {
      const oneHourTimestep = `PT1H`;
      const dimensions = [
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: currentTime,
          // layer has images for 10, 11, 12, 13 clock
          values: `${tStart}/${tEnd}/${oneHourTimestep}`,
        },
      ];
      draft.dimensions = dimensions;
      draft.id = '0';
      draft.service = 'http://testservice.com';
    },
  );

  it('should prefetch images', async () => {
    // mock is also needed here so real implementation doesn't override tests below
    jest.spyOn(WMImage.prototype, 'isLoaded');
    const animationListValues = animationList.map((step) => step.value);
    const wmMap = new WMJSMap(document.createElement('test1'));
    const wmLayer0 = new WMLayer(layer0oneHourTimeStep);
    wmMap.setDimension('time', currentTime);
    await wmMap.addLayer(wmLayer0);
    const mapId = wmMap.getId();
    registerWMJSMap(wmMap, mapId);
    const targets = [
      {
        targetId: mapId,
        value: '',
      },
    ];
    // track calls to load images and avoid doing backend calls
    const imageLoadCalls = jest
      .spyOn(global.Image.prototype, 'src', 'set')
      .mockImplementation();
    prefetchAnimationTargetsForMetronome(mapId, animationListValues, targets);
    expect(imageLoadCalls).toHaveBeenCalledTimes(1);
  });
  it('should not prefetch images if the map is not registered', async () => {
    jest.spyOn(WMImage.prototype, 'isLoaded').mockImplementation(() => false);
    const animationListValues = animationList.map((step) => step.value);
    const wmMap = new WMJSMap(document.createElement('test2'));
    const wmLayer0 = new WMLayer(layer0oneHourTimeStep);
    wmMap.setDimension('time', currentTime);
    await wmMap.addLayer(wmLayer0);
    const mapId = wmMap.getId();
    const targets = [
      {
        targetId: mapId,
        value: '',
      },
    ];
    // mock calls to load images and avoid doing backend calls
    jest.spyOn(global.Image.prototype, 'src', 'set').mockImplementation();
    const canStepForward = prefetchAnimationTargetsForMetronome(
      mapId,
      animationListValues,
      targets,
    );
    expect(canStepForward).toBeTruthy();
  });
  it('should return false if the animation cannot step forward', async () => {
    jest.spyOn(WMImage.prototype, 'isLoaded').mockImplementation(() => false);
    const animationListValues = animationList.map((step) => step.value);
    const wmMap = new WMJSMap(document.createElement('test3'));
    const wmLayer0 = new WMLayer(layer0oneHourTimeStep);
    wmMap.setDimension('time', currentTime);
    await wmMap.addLayer(wmLayer0);
    const mapId = wmMap.getId();
    registerWMJSMap(wmMap, mapId);
    const targets = [
      {
        targetId: mapId,
        value: '',
      },
    ];
    // mock calls to load images and avoid doing backend calls
    jest.spyOn(global.Image.prototype, 'src', 'set').mockImplementation();
    const canStepForward = prefetchAnimationTargetsForMetronome(
      mapId,
      animationListValues,
      targets,
    );
    expect(canStepForward).toBeFalsy();
  });
  it('should return true if the animation can step forward', async () => {
    jest.spyOn(WMImage.prototype, 'isLoaded').mockImplementation(() => true);
    const animationListValues = animationList.map((step) => step.value);
    const wmMap = new WMJSMap(document.createElement('test4'));
    const wmLayer0 = new WMLayer(layer0oneHourTimeStep);
    wmMap.setDimension('time', currentTime);
    await wmMap.addLayer(wmLayer0);
    const mapId = wmMap.getId();
    registerWMJSMap(wmMap, mapId);
    const targets = [
      {
        targetId: mapId,
        value: '',
      },
    ];
    // mock calls to load images avoid doing backend calls
    jest.spyOn(global.Image.prototype, 'src', 'set').mockImplementation();
    const canStepForward = prefetchAnimationTargetsForMetronome(
      mapId,
      animationListValues,
      targets,
    );
    expect(canStepForward).toBeTruthy();
  });
  it('should reload stale images', async () => {
    jest.spyOn(WMImage.prototype, 'isLoaded').mockImplementation(() => true);
    // mark images as stale
    jest.spyOn(WMImage.prototype, 'isStale').mockImplementation(() => true);
    const forceReload = jest.spyOn(WMImage.prototype, 'forceReload');
    const animationListValues = animationList.map((step) => step.value);
    const wmMap = new WMJSMap(document.createElement('test5'));
    const wmLayer0 = new WMLayer(layer0oneHourTimeStep);
    wmMap.setDimension('time', currentTime);
    await wmMap.addLayer(wmLayer0);
    const mapId = wmMap.getId();
    registerWMJSMap(wmMap, mapId);
    const targets = [
      {
        targetId: mapId,
        value: '',
      },
    ];
    // track calls to load images and avoid doing backend calls
    const imageLoadCalls = jest
      .spyOn(global.Image.prototype, 'src', 'set')
      .mockImplementation();
    prefetchAnimationTargetsForMetronome(mapId, animationListValues, targets);
    expect(imageLoadCalls).toHaveBeenCalledTimes(
      MAX_NUMBER_STEPS_FORWARD_TO_PREFETCH,
    );
    expect(forceReload).toHaveBeenCalledTimes(
      MAX_NUMBER_STEPS_FORWARD_TO_PREFETCH,
    );
  });

  describe('handleTimerDwell', () => {
    it('handleTimerDwell should keep the last animation step the same for dwellTimes number of ticks', () => {
      const timerId = 'dwellTimerA';
      const stepsToCheck: number[] = [];
      const dwellTimes = 5;
      for (let j = 0; j < 50; j += 1) {
        // Save the current timestep in an array for the test
        stepsToCheck.push(getCurrentStep(timerId));

        // Check if timerShouldStepForward
        const timerShouldStepForward = !handleTimerDwell(
          timerId,
          12,
          dwellTimes,
        );

        // Set new step
        setStep(
          timerId,
          timerShouldStepForward
            ? getNextStep(timerId, 12)
            : getCurrentStep(timerId),
        );
      }

      // Check if the last step indeed is repeated.
      expect(stepsToCheck).toEqual([
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 11, 11, 11, 11, 11, 0, 1, 2, 3, 4,
        5, 6, 7, 8, 9, 10, 11, 11, 11, 11, 11, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
        10, 11, 11, 11, 11, 11,
      ]);
    });
    it('handleTimerDwell should not do anything when dwell is set to zero', () => {
      const timerId = 'dwellTimerB';
      const stepsToCheck: number[] = [];

      for (let j = 0; j < 50; j += 1) {
        // Save the current timestep in an array for the test
        stepsToCheck.push(getCurrentStep(timerId));

        // Check if timerShouldStepForward
        const timerShouldStepForward = !handleTimerDwell(timerId, 12, 0);

        // Set new step
        setStep(
          timerId,
          timerShouldStepForward
            ? getNextStep(timerId, 12)
            : getCurrentStep(timerId),
        );
      }

      expect(stepsToCheck).toEqual([
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
        11, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
        10, 11, 0, 1,
      ]);
    });

    it('handleTimerDwell should not do anything if there is no animation sequence', () => {
      const timerId = 'dwellTimerC';
      const stepsToCheck: number[] = [];
      const dwellTimes = 5;
      for (let j = 0; j < 50; j += 1) {
        // Save the current timestep in an array for the test
        stepsToCheck.push(getCurrentStep(timerId));

        // Check if timerShouldStepForward
        const timerShouldStepForward = !handleTimerDwell(
          timerId,
          0,
          dwellTimes,
        );

        // Set new step
        setStep(
          timerId,
          timerShouldStepForward
            ? getNextStep(timerId, 0)
            : getCurrentStep(timerId),
        );
      }

      // Check if the last step indeed is repeated.
      expect(stepsToCheck).toEqual([
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0,
      ]);
    });
  });
});
