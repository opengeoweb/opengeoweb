/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { produce } from 'immer';
import {
  Dimension,
  handleDateUtilsISOString,
  isProjectionSupported,
  IWMJSMap,
  LayerType,
  webmapUtils,
} from '@opengeoweb/webmap';
import { Store } from '@reduxjs/toolkit';
import { dateUtils } from '@opengeoweb/shared';
import { metronome } from '@opengeoweb/metronome';
import { act, render } from '@testing-library/react';
import {
  defaultAnimationDelayAtStart,
  defaultTimeStep,
  getSpeedDelay,
} from '@opengeoweb/timeslider';
import { Provider } from 'react-redux';
import { mapActions, mapSelectors } from '.';
import { layerActions } from '../layer';
import { Layer } from '../layer/types';
import { CoreAppStore, genericActions, syncConstants, uiActions } from '../..';
import { mapListener } from './listener';
import * as mapListenerAnimationUtils from './mapListenerAnimationUtils';
import { ReduxLayer, SetMapPresetPayload, WebMap } from '../types';
import { createMockStore } from '../../store';

describe('store/mapStore/map/listener', () => {
  // new Date().toISOString() returns a iso8601 string with milliseconds: `2000-01-23T10:00:00.000Z`
  // but in geoweb we save iso8601 without milliseconds in redux: `2000-01-23T10:00:00Z`
  const removeMilliseconds = (iso8601: string): string =>
    iso8601.replace(/\.[0-9]{3}/, '');

  // setup data for integration tests
  const mockNowTimeIso = `2000-01-23T10:00:00Z`;
  const mockNowTime = new Date(mockNowTimeIso);

  jest.useFakeTimers();
  jest.setSystemTime(mockNowTime);
  afterEach(() => {
    jest.setSystemTime(mockNowTime);
  });
  afterAll(() => {
    jest.useRealTimers();
  });

  // layer current time and layer max time is set to be now time
  // we want to test that the layer current time is updated
  // to the new max time when new max time arrives
  const initialLayerCurrentTime = mockNowTimeIso;
  const initialLayerMaxTime = mockNowTimeIso;

  const initialTimeDimension: Dimension = {
    name: 'time',
    units: 'ISO8601',
    currentValue: initialLayerCurrentTime,
    maxValue: initialLayerMaxTime,
    minValue: dateUtils.sub(mockNowTime, { days: 1 }).toISOString(),
    validSyncSelection: true,
  };

  const mapId = 'mapId';
  const layerId = 'layerId';

  const layer: Layer = {
    id: layerId,
    service: 'https://testservice',
    name: 'layerName',
    title: 'layerTitle',
    enabled: true,
    layerType: LayerType.mapLayer,
    dimensions: [initialTimeDimension],
  };
  const layerInRedux = {
    ...layer,
    acceptanceTimeInMinutes: 60,
    mapId,
    opacity: 1,
    status: 'default',
    values: undefined,
    useLatestReferenceTime: true,
  };

  const timeSyncGroupId = 'timeslider';
  const origin = 'origin';

  const animationStartTimeDefault = dateUtils.sub(
    new Date(initialLayerMaxTime),
    { hours: 4, minutes: 50 },
  );
  const layerNewCurrentTime = dateUtils
    .sub(new Date(initialLayerMaxTime), { hours: 1 })
    .toISOString();

  const layerWithCurrentTimeBeforeMaxTime = produce(layer, (draft) => {
    draft.dimensions![0].currentValue = layerNewCurrentTime;
  });

  const defaultStartTime = dateUtils.sub(new Date(initialLayerMaxTime), {
    hours: 5,
  });
  const defaultStartTimeString = removeMilliseconds(
    defaultStartTime.toISOString(),
  );
  const defaultEndTime = dateUtils.add(defaultStartTime, {
    hours: 4,
    minutes: 50,
  });
  const defaultEndTimeString = removeMilliseconds(defaultEndTime.toISOString());

  const start = mockNowTimeIso;
  const end = dateUtils.add(mockNowTime, { minutes: 15 }).toISOString();
  const minutesBetweenEachAnimationStep = 1;

  const mapValuesThatDontChangeDuringTesting = {
    id: mapId,
    featureLayers: [],
    disableMapPin: false,
    shouldEndtimeOverride: false,
    isTimeSliderHoverOn: false,
    isTimeSpanAuto: false,
    srs: 'EPSG:3857',
    isTimeSliderVisible: true,
    timeSliderCenterTime: expect.any(Number),
    timeSliderSecondsPerPx: 80,
    timeSliderSpan: 86400,
    timeSliderWidth: 440,
    bbox: expect.any(Object),
    shouldShowZoomControls: true,
    displayMapPin: false,
    animationDelay: defaultAnimationDelayAtStart,
    isTimestepAuto: true,
    isAnimationLengthAuto: false,
  };

  // mock wm map to make testing easier
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const animationDrawCall = jest.fn() as any;
  const mockWmMap = {
    getListener: () => ({
      suspendEvents: (): void => {},
      resumeEvents: (): void => {},
    }),
    stopAnimating: () => {},
    draw: animationDrawCall,
    initialAnimationStep: 0,
    isAnimating: true,
  } as IWMJSMap;

  const mapAfterSetupStore: WebMap = {
    ...mapValuesThatDontChangeDuringTesting,

    mapLayers: [layerId],
    baseLayers: [],
    overLayers: [],

    autoTimeStepLayerId: layerId,
    autoUpdateLayerId: layerId,

    dimensions: [{ name: 'time', currentValue: initialLayerMaxTime }],

    timeStep: 60,

    isAutoUpdating: true,

    isAnimating: false,

    animationStartTime: removeMilliseconds(
      animationStartTimeDefault.toISOString(),
    ),

    animationEndTime: initialLayerMaxTime,
  };

  const setupStore = (
    isMapSyncedWithLayer = true,
    layerToStore = layer,
  ): Store<CoreAppStore> => {
    const store = createMockStore();

    render(<Provider store={store}>hi</Provider>);

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));

      if (isMapSyncedWithLayer) {
        // setup sync group so map time changes when layer time changes
        store.dispatch(
          genericActions.syncGroupAddGroup({
            groupId: timeSyncGroupId,
            title: 'Timeslider',
            type: 'SYNCGROUPS_TYPE_SETTIME',
          }),
        );
        store.dispatch(
          genericActions.syncGroupAddTarget({
            groupId: timeSyncGroupId,
            targetId: mapId,
          }),
        );
      }

      store.dispatch(
        layerActions.addLayer({
          layerId: layerToStore.id!,
          mapId,
          layer: layerToStore,
          origin,
        }),
      );

      // set auto update to true so that new max time moves animation start and end time
      store.dispatch(
        mapActions.toggleAutoUpdate({ mapId, shouldAutoUpdate: true }),
      );
    });
    return store;
  };

  describe('metronomeHandler', () => {
    it('should update time for all syncgroup targets', () => {
      jest
        .spyOn(
          mapListenerAnimationUtils,
          'prefetchAnimationTargetsForMetronome',
        )
        .mockImplementation(() => true);
      jest
        .spyOn(mapListenerAnimationUtils, 'getCurrentStep')
        .mockImplementation(() => 0);

      jest
        .spyOn(mapListenerAnimationUtils, 'getNextStep')
        .mockImplementation(() => 1);

      metronome.init(); // Re-init to allow jest to fake the timer
      const store = setupStore();
      // add a second map and add it to the time sync group
      const mapId2 = 'test-map-2';
      act(() => {
        store.dispatch(mapActions.registerMap({ mapId: mapId2 }));
        store.dispatch(
          genericActions.syncGroupAddTarget({
            groupId: timeSyncGroupId,
            targetId: mapId2,
          }),
        );
      });
      act(() => {
        store.dispatch(
          mapActions.mapStartAnimation({
            mapId,
            start,
            end,
            interval: minutesBetweenEachAnimationStep,
          }),
        );
      });
      const startTimeMap1 =
        store.getState().webmap?.byId[mapId].dimensions![0].currentValue;

      const delay = mapSelectors.getMapAnimationDelay(store.getState(), mapId)!;
      act(() => {
        jest.advanceTimersByTime(delay);
      });

      const expectedTime = dateUtils
        .add(new Date(startTimeMap1!), {
          minutes: 1,
        })
        .toISOString();
      expect(
        store.getState().webmap?.byId[mapId].dimensions![0].currentValue,
      ).toBe(expectedTime);
      expect(
        store.getState().webmap?.byId[mapId2].dimensions![0].currentValue,
      ).toBe(expectedTime);

      metronome.unregisterAllTimers();
    });

    it('should update time for current map if no syncgroup targets are found', () => {
      jest
        .spyOn(
          mapListenerAnimationUtils,
          'prefetchAnimationTargetsForMetronome',
        )
        .mockImplementation(() => true);
      jest
        .spyOn(mapListenerAnimationUtils, 'getCurrentStep')
        .mockImplementation(() => 0);
      jest
        .spyOn(mapListenerAnimationUtils, 'getNextStep')
        .mockImplementation(() => 1);
      metronome.init(); // Re-init to allow jest to fake the timer
      const store = setupStore(false);
      act(() => {
        store.dispatch(
          mapActions.mapStartAnimation({
            mapId,
            start,
            end,
            interval: minutesBetweenEachAnimationStep,
          }),
        );
      });
      const delay = mapSelectors.getMapAnimationDelay(store.getState(), mapId)!;
      act(() => {
        jest.advanceTimersByTime(delay);
      });

      const expectedTime = dateUtils
        .add(new Date(start!), {
          minutes: 1,
        })
        .toISOString();
      expect(
        store.getState().webmap?.byId[mapId].dimensions![0].currentValue,
      ).toBe(expectedTime);
      metronome.unregisterAllTimers();
    });

    it('should not update time if prefetching is not ready', () => {
      jest
        .spyOn(
          mapListenerAnimationUtils,
          'prefetchAnimationTargetsForMetronome',
        )
        .mockImplementation(() => false);
      jest
        .spyOn(mapListenerAnimationUtils, 'getCurrentStep')
        .mockImplementation(() => 0);
      jest
        .spyOn(mapListenerAnimationUtils, 'getNextStep')
        .mockImplementation(() => 1);
      metronome.init(); // Re-init to allow jest to fake the timer
      const store = setupStore();
      act(() => {
        store.dispatch(
          mapActions.mapStartAnimation({
            mapId,
            start,
            end,
            interval: minutesBetweenEachAnimationStep,
          }),
        );
      });
      const delay = mapSelectors.getMapAnimationDelay(store.getState(), mapId)!;
      act(() => {
        jest.advanceTimersByTime(delay);
      });

      const expectedTime = new Date(start).toISOString();
      expect(
        store.getState().webmap?.byId[mapId].dimensions![0].currentValue,
      ).toBe(expectedTime);
      metronome.unregisterAllTimers();
    });

    it('should update the metronome speed', () => {
      const setSpeed = jest.spyOn(metronome, 'setSpeed');
      jest
        .spyOn(mapListenerAnimationUtils, 'getCurrentStep')
        .mockImplementation(() => 0);
      jest
        .spyOn(mapListenerAnimationUtils, 'getNextStep')
        .mockImplementation(() => 1);
      metronome.init(); // Re-init to allow jest to fake the timer
      const store = setupStore();
      act(() => {
        store.dispatch(
          mapActions.mapStartAnimation({
            mapId,
            start,
            end,
          }),
        );
      });
      expect(setSpeed).not.toHaveBeenCalled();
      const delay = mapSelectors.getMapAnimationDelay(store.getState(), mapId)!;
      // run metronome longer than needed to avoid off-by-one errors
      jest.advanceTimersByTime(delay);
      expect(setSpeed).toHaveBeenCalled();
      // jest.runOnlyPendingTimers();
      metronome.unregisterAllTimers();
    });
  });

  describe('toggleAutoUpdate listener', () => {
    it('should update active layer to latest timedimension when turning autoupdate on and update map animation', () => {
      const store = createMockStore();

      store.dispatch(mapActions.registerMap({ mapId }));

      // setup sync group so map time changes when layer time changes on autoupdate
      store.dispatch(
        genericActions.syncGroupAddGroup({
          groupId: timeSyncGroupId,
          title: 'Timeslider',
          type: syncConstants.SYNCGROUPS_TYPE_SETTIME,
        }),
      );
      store.dispatch(
        genericActions.syncGroupAddTarget({
          groupId: timeSyncGroupId,
          targetId: mapId,
        }),
      );

      store.dispatch(
        layerActions.addLayer({
          layerId,
          mapId,
          layer: layerWithCurrentTimeBeforeMaxTime,
          origin,
        }),
      );

      expect(
        store.getState().layers!.byId[layerId].dimensions![0].currentValue,
      ).toEqual(layerNewCurrentTime);

      store.dispatch(
        mapActions.toggleAutoUpdate({ mapId, shouldAutoUpdate: true }),
      );

      expect(store.getState().webmap!.byId[mapId].isAutoUpdating).toEqual(true);

      // layer should be updated
      expect(
        store.getState().layers!.byId[layerId].dimensions![0].currentValue,
      ).toEqual(initialLayerMaxTime);

      // map animation should be updated
      expect(store.getState().webmap!.byId[mapId].dimensions).toEqual([
        { name: 'time', currentValue: initialLayerMaxTime },
      ]);
      expect(store.getState().webmap!.byId[mapId].animationStartTime).toEqual(
        removeMilliseconds(animationStartTimeDefault.toISOString()),
      );
      expect(store.getState().webmap!.byId[mapId].animationEndTime).toEqual(
        initialLayerMaxTime,
      );
    });

    it('should not update active layer to latest timedimension when turning autoupdate off and not update map animation', () => {
      const store = createMockStore();

      store.dispatch(mapActions.registerMap({ mapId }));

      // setup sync group so map time changes when layer time changes on autoupdate
      store.dispatch(
        genericActions.syncGroupAddGroup({
          groupId: timeSyncGroupId,
          title: 'Timeslider',
          type: syncConstants.SYNCGROUPS_TYPE_SETTIME,
        }),
      );
      store.dispatch(
        genericActions.syncGroupAddTarget({
          groupId: timeSyncGroupId,
          targetId: mapId,
        }),
      );

      store.dispatch(
        layerActions.addLayer({
          layerId,
          mapId,
          layer: layerWithCurrentTimeBeforeMaxTime,
          origin,
        }),
      );

      expect(
        store.getState().layers!.byId[layerId].dimensions![0].currentValue,
      ).toEqual(layerNewCurrentTime);

      store.dispatch(
        mapActions.toggleAutoUpdate({ mapId, shouldAutoUpdate: false }),
      );

      expect(store.getState().webmap!.byId[mapId].isAutoUpdating).toEqual(
        false,
      );

      // layer should not be updated
      expect(
        store.getState().layers!.byId[layerId].dimensions![0].currentValue,
      ).toEqual(layerNewCurrentTime);

      // map animation should not be updated
      expect(store.getState().webmap!.byId[mapId].dimensions).toEqual([]);

      expect(store.getState().webmap!.byId[mapId].animationStartTime).toEqual(
        defaultStartTimeString,
      );
      expect(store.getState().webmap!.byId[mapId].animationEndTime).toEqual(
        defaultEndTimeString,
      );
    });

    it('should update active layer to latest timedimension when turning autoupdate on but not update map animation when enddtimeoverride is enabled', () => {
      const store = createMockStore();

      store.dispatch(mapActions.registerMap({ mapId }));

      store.dispatch(
        mapActions.setEndTimeOverriding({ mapId, shouldEndtimeOverride: true }),
      );
      // setup sync group so map time changes when layer time changes on autoupdate
      store.dispatch(
        genericActions.syncGroupAddGroup({
          groupId: timeSyncGroupId,
          title: 'Timeslider',
          type: syncConstants.SYNCGROUPS_TYPE_SETTIME,
        }),
      );
      store.dispatch(
        genericActions.syncGroupAddTarget({
          groupId: timeSyncGroupId,
          targetId: mapId,
        }),
      );

      store.dispatch(
        layerActions.addLayer({
          layerId,
          mapId,
          layer: layerWithCurrentTimeBeforeMaxTime,
          origin,
        }),
      );

      expect(
        store.getState().layers!.byId[layerId].dimensions![0].currentValue,
      ).toEqual(layerNewCurrentTime);

      store.dispatch(
        mapActions.toggleAutoUpdate({ mapId, shouldAutoUpdate: true }),
      );

      expect(store.getState().webmap!.byId[mapId].isAutoUpdating).toEqual(true);

      // layer should be updated
      expect(
        store.getState().layers!.byId[layerId].dimensions![0].currentValue,
      ).toEqual(initialLayerMaxTime);

      // map time should be updated
      expect(store.getState().webmap!.byId[mapId].dimensions).toEqual([
        { name: 'time', currentValue: initialLayerMaxTime },
      ]);

      // map animation should not be updated
      expect(store.getState().webmap!.byId[mapId].animationStartTime).toEqual(
        defaultStartTimeString,
      );
      expect(store.getState().webmap!.byId[mapId].animationEndTime).toEqual(
        defaultEndTimeString,
      );
    });

    it('should sync time in synced maps and turn autoupdate off for synced maps when turning autoupdate on', () => {
      const store = createMockStore();

      const mapId2 = 'mapId2';
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(mapActions.registerMap({ mapId: mapId2 }));

      // setup sync group with 2 maps synced in time
      store.dispatch(
        genericActions.syncGroupAddGroup({
          groupId: timeSyncGroupId,
          title: 'Timeslider',
          type: syncConstants.SYNCGROUPS_TYPE_SETTIME,
        }),
      );
      store.dispatch(
        genericActions.syncGroupAddSource({
          id: mapId,
          type: [syncConstants.SYNCGROUPS_TYPE_SETTIME],
        }),
      );
      store.dispatch(
        genericActions.syncGroupAddSource({
          id: mapId2,
          type: [syncConstants.SYNCGROUPS_TYPE_SETTIME],
        }),
      );
      store.dispatch(
        genericActions.syncGroupAddTarget({
          groupId: timeSyncGroupId,
          targetId: mapId2,
          linked: true,
        }),
      );
      store.dispatch(
        genericActions.syncGroupAddTarget({
          groupId: timeSyncGroupId,
          targetId: mapId,
          linked: true,
        }),
      );
      store.dispatch(
        genericActions.syncGroupAddTarget({
          groupId: timeSyncGroupId,
          targetId: mapId2,
          linked: true,
        }),
      );
      store.dispatch(
        layerActions.addLayer({
          layerId,
          mapId,
          layer: layerWithCurrentTimeBeforeMaxTime,
          origin,
        }),
      );

      // turn autoupdate on for map 1
      store.dispatch(
        mapActions.toggleAutoUpdate({ mapId, shouldAutoUpdate: true }),
      );
      expect(store.getState().webmap!.byId[mapId].isAutoUpdating).toEqual(true);
      expect(store.getState().webmap!.byId[mapId2].isAutoUpdating).toEqual(
        false,
      );

      // map times should be updated
      expect(store.getState().webmap!.byId[mapId].dimensions).toEqual([
        { name: 'time', currentValue: initialLayerMaxTime },
      ]);
      expect(store.getState().webmap!.byId[mapId2].dimensions).toEqual([
        { name: 'time', currentValue: initialLayerMaxTime },
      ]);

      // now turn autoupdate on for map 2
      store.dispatch(
        mapActions.toggleAutoUpdate({ mapId: mapId2, shouldAutoUpdate: true }),
      );
      expect(store.getState().webmap!.byId[mapId2].isAutoUpdating).toEqual(
        true,
      );
      // map 1 should be turned off
      expect(store.getState().webmap!.byId[mapId].isAutoUpdating).toEqual(
        false,
      );
    });
  });

  describe('mapStartAnimation listener', () => {
    it('should register a metronome', () => {
      const spyRegister = jest.spyOn(metronome, 'register');
      const store = createMockStore();
      act(() => {
        store.dispatch(mapActions.registerMap({ mapId }));
        store.dispatch(
          mapActions.mapStartAnimation({
            mapId,
          }),
        );
      });
      const delay = 1000 / defaultAnimationDelayAtStart;
      expect(spyRegister).toHaveBeenCalledWith(null, delay, mapId);
    });
    it('should call setStep to right location', () => {
      const spySetStep = jest.spyOn(mapListenerAnimationUtils, 'setStep');
      const store = createMockStore();
      const animationStartTime = '2020-03-13T13:30:00Z';
      const animationEndTime = '2020-03-13T14:00:00Z';
      const timeStep = 10;
      act(() => {
        store.dispatch(mapActions.registerMap({ mapId }));
        store.dispatch(
          mapActions.setAnimationStartTime({ mapId, animationStartTime }),
        );
        store.dispatch(
          mapActions.setAnimationEndTime({ mapId, animationEndTime }),
        );
        store.dispatch(mapActions.setTimeStep({ mapId, timeStep }));
      });
      const timeList = mapSelectors.getAnimationList(store.getState(), mapId);
      const initialTimeStepIndex = 1;
      const initialTime = timeList[initialTimeStepIndex];
      act(() => {
        store.dispatch(
          mapActions.mapStartAnimation({
            mapId,
            initialTime: initialTime.value,
          }),
        );
      });
      expect(spySetStep).toHaveBeenCalledWith(mapId, initialTimeStepIndex);
    });
  });

  describe('mapStopAnimation listener', () => {
    it('should unregister the metronome', () => {
      const spyUnregister = jest.spyOn(metronome, 'unregister');
      const store = createMockStore();
      act(() => {
        store.dispatch(mapActions.registerMap({ mapId }));
        store.dispatch(
          mapActions.mapStartAnimation({
            mapId,
          }),
        );
      });
      expect(metronome.timers.length).toBe(1);
      expect(metronome.timers[0].timerId).toBe(mapId);
      expect(spyUnregister).not.toHaveBeenCalled();
      act(() => {
        store.dispatch(
          mapActions.mapStopAnimation({
            mapId,
          }),
        );
      });
      expect(metronome.timers.length).toBe(0);
      expect(spyUnregister).toHaveBeenCalledWith(mapId);
    });
  });

  describe('layerDelete listener', () => {
    it('should stop animation after deleting the last layer', () => {
      const store = setupStore();

      // start animation
      act(() => {
        store.dispatch(
          mapActions.mapStartAnimation({
            mapId,
            start,
            end,
            interval: minutesBetweenEachAnimationStep,
          }),
        );
      });
      expect(store.getState().webmap!.byId[mapId].isAnimating).toEqual(true);

      // deleting layer should stop animation
      act(() => {
        store.dispatch(
          layerActions.layerDelete({
            mapId,
            layerId,
            layerIndex: 0,
          }),
        );
      });

      expect(store.getState().webmap!.byId[mapId].isAnimating).toEqual(false);
      expect(store.getState().layers!.allIds).toEqual([]);
    });

    it('should not stop animation after deleting when there are still layers', () => {
      const store = setupStore();

      // add second layer and start animation
      const layer2id = 'layer2id';
      act(() => {
        const layer2 = {
          ...layer,
          name: 'layer2name',
          id: layer2id,
        };
        store.dispatch(
          layerActions.addLayer({
            layerId: layer2id,
            mapId,
            layer: layer2,
            origin,
          }),
        );

        store.dispatch(
          mapActions.mapStartAnimation({
            mapId,
            start,
            end,
            interval: minutesBetweenEachAnimationStep,
          }),
        );
      });

      // deleting layer should not stop animation
      act(() => {
        store.dispatch(
          layerActions.layerDelete({
            mapId,
            layerId,
            layerIndex: 0,
          }),
        );
      });

      expect(store.getState().webmap!.byId[mapId].isAnimating).toEqual(true);
      expect(store.getState().layers!.allIds).toEqual([layer2id]);
    });
  });

  describe('onUpdateLayerInformation listener', () => {
    const newMaxValueHoursDifference = 3;
    const newMaxValue = removeMilliseconds(
      dateUtils
        .add(new Date(initialTimeDimension.maxValue!), {
          hours: newMaxValueHoursDifference,
        })
        .toISOString(),
    );
    const timeDimensionWithNewMaxValue = {
      ...initialTimeDimension,
      maxValue: newMaxValue,
    };

    it('should update the active layer and animation to new time dimension if there is new data and autoupdating is true', () => {
      const store = setupStore();

      // action with same time as before shouldn't change state
      act(() => {
        store.dispatch(
          layerActions.onUpdateLayerInformation({
            origin,
            layerDimensions: {
              layerId,
              dimensions: [initialTimeDimension],
              origin,
            },
          }),
        );
      });
      expect(store.getState().webmap!.byId[mapId]).toEqual(mapAfterSetupStore);
      expect(store.getState().layers!.byId[layerId]).toEqual(layerInRedux);

      // update layer with new max time should change animation end time
      act(() => {
        store.dispatch(
          layerActions.onUpdateLayerInformation({
            origin,
            layerDimensions: {
              layerId,
              dimensions: [timeDimensionWithNewMaxValue],
              origin,
            },
          }),
        );
      });

      expect(mapSelectors.getAnimationEndTime(store.getState(), mapId)).toEqual(
        timeDimensionWithNewMaxValue.maxValue,
      );
      expect(
        mapSelectors.getAnimationStartTime(store.getState(), mapId),
      ).toEqual(
        removeMilliseconds(
          dateUtils
            .add(animationStartTimeDefault, {
              hours: newMaxValueHoursDifference,
            })
            .toISOString(),
        ),
      );

      const updatedCurrentTime = removeMilliseconds(
        dateUtils
          .fromUnix(mapSelectors.getSelectedTime(store.getState(), mapId))
          .toISOString(),
      );
      expect(store.getState().layers!.byId[layerId]).toEqual({
        ...layerInRedux,
        dimensions: [
          {
            ...initialTimeDimension,
            currentValue: updatedCurrentTime,
            maxValue: timeDimensionWithNewMaxValue.maxValue!,
          },
        ],
      });
      expect(updatedCurrentTime).toEqual(timeDimensionWithNewMaxValue.maxValue);
    });

    it('should only update the active layer', () => {
      const store = setupStore();

      // add second layer which will not get updated
      const layer2id = 'layer2id';
      act(() => {
        const layer2 = {
          ...layer,
          name: 'layer2name',
          id: layer2id,
        };
        store.dispatch(
          layerActions.addLayer({
            layerId: layer2id!,
            mapId,
            layer: layer2,
            origin,
          }),
        );
      });

      // new max value will only be set for first layer
      act(() => {
        store.dispatch(
          layerActions.onUpdateLayerInformation({
            origin,
            layerDimensions: {
              layerId,
              dimensions: [timeDimensionWithNewMaxValue],
              origin,
            },
          }),
        );
      });

      expect(
        store.getState().layers!.byId[layerId].dimensions![0].maxValue,
      ).toEqual(timeDimensionWithNewMaxValue.maxValue);
      expect(
        store.getState().layers!.byId[layer2id].dimensions![0].maxValue,
      ).toEqual(initialTimeDimension.maxValue);
    });

    it('should not update the active layer and animation to new time dimension if there is new data but autoupdating is false', () => {
      const store = setupStore();

      // turn auto update off so new max value will not change map time
      act(() => {
        store.dispatch(
          mapActions.toggleAutoUpdate({ mapId, shouldAutoUpdate: false }),
        );
      });

      // dispatching time dimension with new max value
      act(() => {
        store.dispatch(
          layerActions.onUpdateLayerInformation({
            origin,
            layerDimensions: {
              layerId,
              dimensions: [timeDimensionWithNewMaxValue],
              origin,
            },
          }),
        );
      });
      // should not change map animation start and end
      expect(store.getState().webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupStore,
        isAutoUpdating: false,
      });
    });

    it('should auto update the active layer for multiple maps if there is new data', () => {
      const mapId2 = 'mapId2';
      const store = setupStore();

      // add a second map and add it to the time sync group
      act(() => {
        store.dispatch(mapActions.registerMap({ mapId: mapId2 }));

        store.dispatch(
          genericActions.syncGroupAddTarget({
            groupId: timeSyncGroupId,
            targetId: mapId2,
          }),
        );
      });

      // Check the animation start and end time for map 1 have not been updated yet
      expect(
        mapSelectors.getAnimationStartTime(store.getState(), mapId),
      ).toEqual('2000-01-23T05:10:00Z');
      expect(mapSelectors.getAnimationEndTime(store.getState(), mapId)).toEqual(
        '2000-01-23T10:00:00Z',
      );
      // Check the animation start and end time for map 2 as well
      expect(
        mapSelectors.getAnimationEndTime(store.getState(), mapId2),
      ).toEqual('2000-01-23T09:50:00Z');
      expect(
        mapSelectors.getAnimationEndTime(store.getState(), mapId2),
      ).toEqual('2000-01-23T09:50:00Z');
      // Selected time of map 1
      const initialSelectedTimeMap1 = removeMilliseconds(
        dateUtils
          .fromUnix(mapSelectors.getSelectedTime(store.getState(), mapId))
          .toISOString(),
      );
      expect(initialSelectedTimeMap1).toEqual(initialLayerCurrentTime);
      // Selected time of map 2
      const initialSelectedTimeMap2 = removeMilliseconds(
        dateUtils
          .fromUnix(mapSelectors.getSelectedTime(store.getState(), mapId2))
          .toISOString(),
      );
      expect(initialSelectedTimeMap2).toEqual(initialLayerCurrentTime);

      // Update layer dimension
      act(() => {
        store.dispatch(
          layerActions.onUpdateLayerInformation({
            origin,
            layerDimensions: {
              layerId,
              dimensions: [timeDimensionWithNewMaxValue],
              origin,
            },
          }),
        );
      });

      // Check that the animation start and end time for map 1 have been updated
      expect(
        mapSelectors.getAnimationStartTime(store.getState(), mapId),
      ).toEqual('2000-01-23T08:10:00Z');
      expect(mapSelectors.getAnimationEndTime(store.getState(), mapId)).toEqual(
        timeDimensionWithNewMaxValue.maxValue,
      );
      // Check that the animation start and end time for map 2 have been updated as well
      expect(
        mapSelectors.getAnimationStartTime(store.getState(), mapId2),
      ).toEqual('2000-01-23T08:10:00Z');
      expect(
        mapSelectors.getAnimationEndTime(store.getState(), mapId2),
      ).toEqual(timeDimensionWithNewMaxValue.maxValue);
      // Selected time of map 1
      const updatedSelectedTimeMap1 = removeMilliseconds(
        dateUtils
          .fromUnix(mapSelectors.getSelectedTime(store.getState(), mapId))
          .toISOString(),
      );
      expect(updatedSelectedTimeMap1).toEqual(
        timeDimensionWithNewMaxValue.maxValue,
      );
      // Selected time of map 2
      const updatedSelectedTimeMap2 = removeMilliseconds(
        dateUtils
          .fromUnix(mapSelectors.getSelectedTime(store.getState(), mapId2))
          .toISOString(),
      );
      expect(updatedSelectedTimeMap2).toEqual(
        timeDimensionWithNewMaxValue.maxValue,
      );
    });
  });

  describe('setMapPresetListener', () => {
    const legendId = 'legend';

    // mock generating of layer ids
    // so that we can test that they are mapped correctly to the layers
    const generatedLayerId0 = 'layer0';
    const generatedLayerId1 = 'layer1';
    const generatedLayerId2 = 'layer2';
    const generatedLayerId3 = 'layer3';
    const generatedLayerId4 = 'layer4';

    beforeEach(() => {
      jest.resetAllMocks();

      jest
        .spyOn(webmapUtils, 'generateLayerId')
        .mockReturnValueOnce(generatedLayerId0)
        .mockReturnValueOnce(generatedLayerId1)
        .mockReturnValueOnce(generatedLayerId2)
        .mockReturnValueOnce(generatedLayerId3)
        .mockReturnValueOnce(generatedLayerId4);
    });

    const setupStoreAndSetMapPreset = (
      mapPreset: SetMapPresetPayload,
    ): CoreAppStore => {
      const store = createMockStore();

      render(<Provider store={store}>hi</Provider>);

      act(() => {
        store.dispatch(mapActions.registerMap({ mapId }));
        store.dispatch(uiActions.registerDialog({ mapId, type: legendId }));
        store.dispatch(mapActions.setMapPreset(mapPreset));
        store.dispatch(
          mapActions.toggleAnimationLengthAuto({
            mapId,
            autoAnimationLength: false,
          }),
        );
      });
      return store.getState();
    };

    const defaultAnimationEndTime = dateUtils.sub(mockNowTime, { minutes: 10 });

    const mapAfterSetupIfMapPresetIsEmpty: WebMap = {
      ...mapValuesThatDontChangeDuringTesting,
      mapLayers: [],
      baseLayers: [generatedLayerId2],
      overLayers: [generatedLayerId1],
      autoTimeStepLayerId: undefined,
      autoUpdateLayerId: undefined,
      dimensions: [],
      timeStep: undefined,
      legendId,
      isAutoUpdating: false,
      isAnimating: false,
      animationStartTime: removeMilliseconds(
        dateUtils.sub(mockNowTime, { hours: 5 }).toISOString(),
      ),
      animationEndTime: removeMilliseconds(
        defaultAnimationEndTime.toISOString(),
      ),
      dockedLayerManagerSize: '',
    };

    it('should use default values if no value is provided in mapPreset', () => {
      const mapPreset: SetMapPresetPayload = {
        mapId,
        initialProps: {
          mapPreset: {},
        },
      };

      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual(
        mapAfterSetupIfMapPresetIsEmpty,
      );

      expect(state.ui!.dialogs[legendId]!.isOpen).toEqual(false);
    });

    it('should add given layers and use default base and overlayer', async () => {
      const layer0name = 'layer0name';
      const layer1name = 'layer1name';

      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            layers: [
              {
                // the layer id will be replaced with another id.
                // this is to prevent adding two layers that both have the same id.
                id: 'layer0idBefore',
                layerType: LayerType.mapLayer,
                name: layer0name,
              },
              {
                id: 'layer1idBefore',
                layerType: LayerType.mapLayer,
                name: layer1name,
              },
            ],
          },
        },
      };

      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,

        // if auto timestep or auto update id is not provided in mapPreset
        // it defaults to using the first layer in the array

        autoTimeStepLayerId: generatedLayerId0,
        autoUpdateLayerId: generatedLayerId0,
        mapLayers: [generatedLayerId0, generatedLayerId1],
        overLayers: [generatedLayerId3],
        baseLayers: [generatedLayerId4],
      });

      expect(state.layers!.allIds).toHaveLength(4);
      const layers = state.layers!.byId;
      // check that new layer ids are mapped to correct layer names
      expect(layers[generatedLayerId0].name).toEqual(layer0name);
      expect(layers[generatedLayerId1].name).toEqual(layer1name);
    });

    it('should set auto timestep and auto update layer id when provided in mapPreset', () => {
      const layer1idBefore = 'layer1idBefore';
      const layer1name = 'layer1name';
      const layer0name = 'layer0name';
      const mapPreset: SetMapPresetPayload = {
        mapId,
        initialProps: {
          mapPreset: {
            autoTimeStepLayerId: layer1idBefore,
            autoUpdateLayerId: layer1idBefore,
            layers: [
              {
                id: 'layer0idBefore',
                layerType: LayerType.mapLayer,
                name: layer0name,
              },
              {
                id: layer1idBefore,
                layerType: LayerType.mapLayer,
                name: layer1name,
              },
            ],
          },
        },
      };

      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,

        autoTimeStepLayerId: generatedLayerId0,
        autoUpdateLayerId: generatedLayerId0,
        mapLayers: [generatedLayerId1, generatedLayerId0],
        overLayers: [generatedLayerId3],
        baseLayers: [generatedLayerId4],
      });

      expect(state.layers!.allIds).toHaveLength(4);
      // check that new layer ids are mapped to correct layer names
      expect(state.layers!.byId[generatedLayerId0].name).toEqual(layer1name);
      expect(state.layers!.byId[generatedLayerId1].name).toEqual(layer0name);
    });

    it('should set baselayer.', () => {
      const baseLayerName = 'baseLayerName';
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            layers: [
              {
                id: 'baseLayerIdBefore',
                name: baseLayerName,
                type: 'twms',
                layerType: LayerType.baseLayer,
              },
            ],
          },
        },
      };

      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,
        baseLayers: [generatedLayerId2],
      });

      expect(state.layers!.allIds).toHaveLength(2);
      // check that new layer id is mapped to correct layer name
      expect(state.layers!.byId[generatedLayerId2].name).toEqual(baseLayerName);
    });

    it('should set baselayer and overlayer.', () => {
      const baseLayerName = 'baseLayerName';
      const overLayerName = 'overLayerName';
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            layers: [
              {
                id: 'baseLayerIdBefore',
                name: baseLayerName,
                type: 'twms',
                layerType: LayerType.baseLayer,
                dimensions: [
                  { currentValue: '2000-01-23T09:50:00Z', name: 'time' },
                ],
              },
              {
                id: 'overLayerIdBefore',
                name: overLayerName,
                type: 'twms',
                layerType: LayerType.overLayer,
              },
            ],
          },
        },
      };
      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,
        overLayers: [generatedLayerId1],
        baseLayers: [generatedLayerId2],
      });

      expect(state.layers!.allIds).toHaveLength(2);
      // check that new layer ids are mapped to correct layer names
      expect(state.layers!.byId[generatedLayerId1].name).toEqual(overLayerName);
      expect(state.layers!.byId[generatedLayerId2].name).toEqual(baseLayerName);
    });

    it('should use availableBaseLayer id if the name exists in the available baselayer list', () => {
      const baseLayer1Name = 'baseLayerName';
      const baseLayerPreexistingId = 'preexistingId';
      const baseLayer1: ReduxLayer = {
        id: baseLayerPreexistingId,
        name: baseLayer1Name,
        type: 'twms',
        layerType: LayerType.baseLayer,
        dimensions: [{ currentValue: '2000-01-23T09:50:00Z', name: 'time' }],
      };
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            layers: [baseLayer1],
          },
        },
      };
      const store = createMockStore();

      render(<Provider store={store}>hi</Provider>);
      act(() => {
        store.dispatch(mapActions.registerMap({ mapId }));
        store.dispatch(uiActions.registerDialog({ mapId, type: legendId }));
        // set current base layer as available beforehand
        store.dispatch(
          layerActions.setAvailableBaseLayers({
            layers: [{ ...baseLayer1, mapId }],
            mapId,
          }),
        );
        store.dispatch(
          mapActions.toggleAnimationLengthAuto({
            mapId,
            autoAnimationLength: false,
          }),
        );
        store.dispatch(mapActions.setMapPreset(mapPreset));
      });
      const state = store.getState();

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,
        // check that pre-existing id has been reused
        baseLayers: [baseLayerPreexistingId],
        overLayers: [generatedLayerId1],
      });
      expect(state.layers!.byId[baseLayerPreexistingId].name).toEqual(
        baseLayer1Name,
      );
    });

    it('should add a baseLayer to the available baselayer list if it does not exist in the existing list', () => {
      const baseLayerNameNew = 'baseLayerName';
      const preexistingBaseLayerId1 = 'pre-existing-base-layer-id-1';
      const preexistingBaseLayerId2 = 'pre-existing-base-layer-id-2';
      const baseLayer: ReduxLayer = {
        id: 'baseLayerIdBefore0',
        name: baseLayerNameNew,
        type: 'twms',
        layerType: LayerType.baseLayer,
        dimensions: [{ currentValue: '2000-01-23T09:50:00Z', name: 'time' }],
      };
      const preexistingBaseLayer1 = {
        id: preexistingBaseLayerId1,
        name: 'Pre-existing base layer 1',
        type: 'twms',
        mapId,
        layerType: LayerType.baseLayer,
        dimensions: [{ currentValue: '2000-01-23T09:50:00Z', name: 'time' }],
      };
      const preexistingBaseLayer2 = {
        id: preexistingBaseLayerId2,
        name: 'Pre-existing base layer 2',
        type: 'twms',
        mapId,
        layerType: LayerType.baseLayer,
        dimensions: [{ currentValue: '2000-01-23T09:50:00Z', name: 'time' }],
      };

      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            layers: [baseLayer],
          },
        },
      };
      const store = createMockStore();

      render(<Provider store={store}>hi</Provider>);
      act(() => {
        store.dispatch(mapActions.registerMap({ mapId }));
        store.dispatch(uiActions.registerDialog({ mapId, type: legendId }));
        // set two pre-existing baselayers that don't include current base layer
        store.dispatch(
          layerActions.setAvailableBaseLayers({
            layers: [preexistingBaseLayer1, preexistingBaseLayer2],
            mapId,
          }),
        );
        store.dispatch(
          mapActions.toggleAnimationLengthAuto({
            mapId,
            autoAnimationLength: false,
          }),
        );
        store.dispatch(mapActions.setMapPreset(mapPreset));
      });
      const state = store.getState();

      expect(state.webmap!.byId[mapId].baseLayers.length).toBe(1);
      const generatedBaseLayerId = state.webmap!.byId[mapId].baseLayers[0];
      expect(generatedBaseLayerId).toMatch(/layer[0-9]/);
      expect(state.layers!.byId[generatedBaseLayerId].name).toEqual(
        baseLayerNameNew,
      );
      expect(state.layers!.availableBaseLayers.allIds).toContain(
        preexistingBaseLayerId1,
      );
      expect(state.layers!.availableBaseLayers.allIds).toContain(
        preexistingBaseLayerId2,
      );
      expect(state.layers!.availableBaseLayers.allIds).toContain(
        generatedBaseLayerId,
      );
    });

    it('should save properties from the mapPreset in the store', () => {
      const duration = 99999;
      const interval = 88888;
      const animationDelay = 16;
      const bbox = {
        left: -7529663.50832266,
        bottom: 308359.5390525013,
        right: 7493930.85787452,
        top: 11742807.68245839,
      };
      const dockedLayerManagerSize = 'sizeSmall';
      const shouldAutoUpdate = true;
      const showTimeSlider = true;
      const shouldShowZoomControls = false;
      const displayMapPin = true;
      const shouldAnimate = true;
      const shouldShowLegend = true;
      const timeSliderSpan = 86400;
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            shouldAutoUpdate,
            showTimeSlider,
            proj: {
              bbox,
              srs: 'EPSG:3857',
            },
            shouldShowZoomControls,
            displayMapPin,
            shouldAnimate,
            animationPayload: {
              duration,
              interval,
              speed: animationDelay,
            },
            shouldShowLegend,
            dockedLayerManagerSize,
          },
        },
      } as const;

      jest.spyOn(webmapUtils, 'getWMJSMapById').mockReturnValue({
        ...mockWmMap,
        isProjectionSupported,
      } as IWMJSMap);

      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,
        isAutoUpdating: shouldAutoUpdate,
        isTimeSliderVisible: showTimeSlider,
        timeSliderSpan,
        bbox,
        shouldShowZoomControls,
        displayMapPin,
        isAnimating: shouldAnimate,
        animationStartTime: removeMilliseconds(
          dateUtils.sub(mockNowTime, { minutes: duration }).toISOString(),
        ),
        animationEndTime: mockNowTimeIso,
        timeStep: interval,
        isTimestepAuto: false,
        animationDelay: getSpeedDelay(animationDelay),
        dockedLayerManagerSize,
        dimensions: [
          {
            currentValue: '1999-11-14T23:11:00Z',
            name: 'time',
          },
        ],
      });

      expect(state.ui!.dialogs[legendId]!.isOpen).toEqual(shouldShowLegend);
    });

    it('should set animationEndTime from valid endTime of mapPreset', () => {
      const shouldAnimate = false;
      const duration = 300;
      const animationEndTime = `2000-01-23T10:00:00Z`;
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            shouldAnimate,
            animationPayload: {
              duration,
              endTime: animationEndTime,
              shouldEndtimeOverride: true,
            },
          },
        },
      } as const;

      const state = setupStoreAndSetMapPreset(mapPreset);

      const animationStartTime = handleDateUtilsISOString(
        dateUtils
          .sub(dateUtils.getUtcFromString(animationEndTime)!, {
            minutes: duration,
          })
          .toISOString(),
      );

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,
        isAnimating: shouldAnimate,
        shouldEndtimeOverride: true,
        animationEndTime,
        dimensions: [
          {
            currentValue: animationStartTime,
            name: 'time',
          },
        ],
      });
    });

    it('should set animationEndTime from valid NOW time of mapPreset', () => {
      const two = 2;
      const duration = 1000;
      const shouldAnimate = false;
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            shouldAnimate,
            animationPayload: {
              duration,
              endTime: 'NOW+PT2H00M',
              shouldEndtimeOverride: false,
            },
          },
        },
      } as const;

      const state = setupStoreAndSetMapPreset(mapPreset);

      const animationEndTimeDate = dateUtils.add(mockNowTime, { hours: two });
      const animationStartTime = handleDateUtilsISOString(
        dateUtils
          .sub(animationEndTimeDate, {
            minutes: duration,
          })
          .toISOString(),
      );

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,
        isAnimating: shouldAnimate,
        animationStartTime,
        animationEndTime: handleDateUtilsISOString(
          animationEndTimeDate.toISOString(),
        ),
        dimensions: [
          {
            currentValue: animationStartTime,
            name: 'time',
          },
        ],
      });
    });

    it('should set animationEndTime from valid TODAY time of mapPreset', () => {
      const two = 2;
      const duration = 1000;
      const shouldAnimate = false;
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            shouldAnimate,
            animationPayload: {
              duration,
              endTime: 'TODAY+PT2H00M',
              shouldEndtimeOverride: false,
            },
          },
        },
      } as const;

      const state = setupStoreAndSetMapPreset(mapPreset);

      const TODAY = dateUtils.startOfDayUTC(mockNowTime);
      const animationEndTime = dateUtils.add(TODAY, { hours: two });
      const animationStartTime = handleDateUtilsISOString(
        dateUtils
          .sub(animationEndTime, {
            minutes: duration,
          })
          .toISOString(),
      );

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,
        isAnimating: shouldAnimate,
        animationStartTime,
        animationEndTime: handleDateUtilsISOString(
          animationEndTime.toISOString(),
        ),
        dimensions: [
          {
            currentValue: animationStartTime,
            name: 'time',
          },
        ],
      });
    });

    it('should not set animationEndTime from invalid endTime of mapPreset', () => {
      const duration = 120;
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            shouldAnimate: false,
            animationPayload: {
              duration,
              endTime: '00:00', // this is not a valid time format
            },
          },
        },
      } as const;

      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,
        isAnimating: false,
        animationStartTime: handleDateUtilsISOString(
          dateUtils
            .sub(defaultAnimationEndTime, {
              minutes: duration,
            })
            .toISOString(),
        ),
        animationEndTime: handleDateUtilsISOString(
          defaultAnimationEndTime.toISOString(),
        ),
        dimensions: [
          {
            currentValue: '2000-01-23T07:50:00Z',
            name: 'time',
          },
        ],
      });
    });

    it('should start animation with valid endTime of mapPreset if shouldEndtimeOverride is true', async () => {
      const duration = 120;
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            shouldAnimate: true,
            shouldAutoUpdate: true, // this will not be saved in the store because shouldEndtimeOverride is true
            animationPayload: {
              duration,
              endTime: '2000-01-23T10:00:00Z',
              shouldEndtimeOverride: true,
            },
          },
        },
      } as const;

      const state = setupStoreAndSetMapPreset(mapPreset);

      const animationEndTime = dateUtils.parseISO(`2000-01-23T10:00:00Z`);
      const animationStartTime = handleDateUtilsISOString(
        dateUtils
          .sub(animationEndTime, {
            minutes: duration,
          })
          .toISOString(),
      );

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,
        isTimestepAuto: false,
        isAnimating: true,
        isAutoUpdating: false, // auto update is off because shouldEndtimeOverride is true
        shouldEndtimeOverride: true,
        animationStartTime,
        animationEndTime: handleDateUtilsISOString(
          animationEndTime.toISOString(),
        ),
        timeStep: defaultTimeStep,
        dimensions: [
          {
            currentValue: animationStartTime,
            name: 'time',
          },
        ],
        timeList: undefined,
      });
    });

    it('should not start animation with endTime of mapPreset if shouldEndtimeOverride is false', () => {
      const duration = 1000;
      const shouldAnimate = true;

      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            shouldAnimate,
            shouldAutoUpdate: true,
            animationPayload: {
              duration,
              endTime: '2000-01-23T10:00:00Z', // this value is ignored because shouldEndtimeOverride is false
              shouldEndtimeOverride: false,
            },
          },
        },
      } as const;

      const state = setupStoreAndSetMapPreset(mapPreset);
      const animationStartTime = removeMilliseconds(
        dateUtils
          .sub(mockNowTime, {
            minutes: duration,
          })
          .toISOString(),
      );

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,
        isTimestepAuto: false,
        timeStep: defaultTimeStep,
        isAnimating: shouldAnimate,
        isAutoUpdating: false,
        animationStartTime,
        animationEndTime: mockNowTimeIso,
        dimensions: [
          {
            currentValue: animationStartTime,
            name: 'time',
          },
        ],
        timeList: undefined,
      });
    });

    it('should fire toggleTimestepAuto', () => {
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            toggleTimestepAuto: false,
          },
        },
      };

      const state = setupStoreAndSetMapPreset(mapPreset);

      expect(state.webmap!.byId[mapId]).toEqual({
        ...mapAfterSetupIfMapPresetIsEmpty,
        isTimestepAuto: false,
      });
    });

    it('should trigger error when projection is not supported', () => {
      const bbox = {
        left: -7529663.50832266,
        bottom: 308359.5390525013,
        right: 7493930.85787452,
        top: 11742807.68245839,
      };
      const testSrs = 'EPSG:9999';
      const mapPreset = {
        mapId,
        initialProps: {
          mapPreset: {
            proj: {
              bbox,
              srs: testSrs,
            },
          },
        },
      } as const;

      // we have to implement this test by listening to actions
      // as the action only does changes to view preset store that is
      // outside of this library
      let errorCalled = false;
      mapListener.startListening({
        actionCreator: mapActions.setMapPresetError,
        effect: (_, listenerApi) => {
          errorCalled = true;
          listenerApi.unsubscribe();
        },
      });

      expect(errorCalled).toBeFalsy();
      setupStoreAndSetMapPreset(mapPreset);
      expect(errorCalled).toBeTruthy();
    });
  });

  describe('unregisterMapListener', () => {
    it('should delete layers for unregistered map', () => {
      const store = setupStore();

      // add second layer to map
      act(() => {
        const layer2id = 'layer2id';
        const layer2 = {
          ...layer,
          name: 'layer2name',
          id: layer2id,
        };
        store.dispatch(
          layerActions.addLayer({
            layerId: layer2id,
            mapId,
            layer: layer2,
            origin,
          }),
        );
      });

      act(() => {
        store.dispatch(
          mapActions.unregisterMap({
            mapId,
          }),
        );
      });

      expect(store.getState().layers!.byId).toEqual({});
    });
  });

  describe('setStepBackwardOrForward listener', () => {
    it('should update map time backward and forward and cancel animation', () => {
      const store = setupStore();
      act(() => {
        store.dispatch(
          mapActions.mapStartAnimation({
            mapId,
            start,
            end,
            interval: minutesBetweenEachAnimationStep,
          }),
        );
      });
      expect(store.getState().webmap!.byId[mapId].dimensions).toEqual([
        { name: 'time', currentValue: initialLayerMaxTime },
      ]);
      expect(store.getState().webmap?.byId[mapId].isAnimating).toBe(true);
      expect(store.getState().webmap?.byId[mapId].isAutoUpdating).toBe(true);

      // step backward
      act(() => {
        store.dispatch(
          mapActions.setStepBackwardOrForward({
            mapId,
            isForwardStep: false,
          }),
        );
      });

      const expectedPreviousTimeStep = handleDateUtilsISOString(
        dateUtils
          .sub(dateUtils.getUtcFromString(initialLayerMaxTime)!, {
            minutes: minutesBetweenEachAnimationStep,
          })
          .toISOString(),
      );
      expect(store.getState().webmap!.byId[mapId].dimensions).toEqual([
        { name: 'time', currentValue: expectedPreviousTimeStep },
      ]);
      expect(store.getState().webmap?.byId[mapId].isAnimating).toBe(false);
      expect(store.getState().webmap?.byId[mapId].isAutoUpdating).toBe(true);

      // step forward
      act(() => {
        store.dispatch(
          mapActions.setStepBackwardOrForward({
            mapId,
            isForwardStep: true,
          }),
        );
      });
      expect(store.getState().webmap!.byId[mapId].dimensions).toEqual([
        { name: 'time', currentValue: initialLayerMaxTime },
      ]);
    });
  });
});
