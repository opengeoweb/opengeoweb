/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { LayerType, WMLayer, webmapUtils } from '@opengeoweb/webmap';
import { Draft } from 'immer';
import { createLayersState } from '../storeTestUtils';
import { LayerState, ReduxLayer } from './types';
import {
  filterCurrentValueFromDimensions,
  isActionLayerSynced,
  produceDimensionActionForMapLayer,
  updateMinMaxValueWhenReferenceTimeChanges,
} from './utils';

describe('store/mapStore/layers/utils', () => {
  describe('isActionLayerSynced', () => {
    it('should return false if layerid is null or does not exist in the state', () => {
      expect(
        isActionLayerSynced({ byId: {} } as LayerState, 'layerid', {
          currentValue: '10',
        }),
      ).toBeFalsy();
      expect(
        isActionLayerSynced({ byId: {} } as LayerState, null!, {
          currentValue: '10',
        }),
      ).toBeFalsy();
    });
    it('should check if the layer passed is synced', () => {
      // If dimension contains synced: true/false - it should return that regardless of what it says in the state
      expect(
        isActionLayerSynced({ byId: {} } as LayerState, 'layerid', {
          currentValue: '10',
          synced: true,
        }),
      ).toBeTruthy();
      expect(
        isActionLayerSynced({ byId: {} } as LayerState, 'layerid', {
          currentValue: '10',
          synced: false,
        }),
      ).toBeFalsy();
    });
    it('should return false if dimension does not exist for layer', () => {
      const initialDimension = {
        name: 'test-start',
        currentValue: 'test-start',
      };
      expect(
        isActionLayerSynced(
          createLayersState('layer1', {
            mapId: 'mapid1',
            dimensions: [initialDimension],
          }),
          'layer1',
          {
            currentValue: '10',
            name: 'elevation',
          },
        ),
      ).toBeFalsy();
    });
    it('should return the value from the dimension from the sate if none of the above', () => {
      const initialDimension = {
        name: 'elevation',
        currentValue: '50',
        synced: true,
      };
      expect(
        isActionLayerSynced(
          createLayersState('layer1', {
            mapId: 'mapid1',
            dimensions: [initialDimension],
          }),
          'layer1',
          {
            currentValue: '10',
            name: 'elevation',
          },
        ),
      ).toBeTruthy();
      const initialDimension2 = {
        name: 'elevation',
        currentValue: '50',
        synced: false,
      };
      expect(
        isActionLayerSynced(
          createLayersState('layer1', {
            mapId: 'mapid1',
            dimensions: [initialDimension2],
          }),
          'layer1',
          {
            currentValue: '10',
            name: 'elevation',
          },
        ),
      ).toBeFalsy();
    });
  });
  describe('produceDimensionActionForMapLayer', () => {
    const dimensionLayer1 = {
      service: 'https://testservice',
      id: 'multiDimensionLayerMock',
      name: 'MULTI_DIMENSION_LAYER',
      title: 'MULTI_DIMENSION_LAYER',
      layerType: LayerType.mapLayer,
      enabled: true,
      dimensions: [
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
          synced: true,
        },
      ],
    };
    const WmMultiDimensionLayer = new WMLayer({
      service: 'https://testservice',
      id: 'multiDimensionLayerMock',
      name: 'MULTI_DIMENSION_LAYER',
      title: 'MULTI_DIMENSION_LAYER',
      layerType: LayerType.mapLayer,
      enabled: true,
      dimensions: [
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
        },
      ],
    });
    webmapUtils.registerWMLayer(
      WmMultiDimensionLayer,
      'multiDimensionLayerMock',
    );
    it('should set validSyncSelection to false if the new current value for the synced layer dimension is not present', () => {
      expect(
        produceDimensionActionForMapLayer(
          {
            name: 'elevation',
            units: 'meters',
            currentValue: '9000',
            synced: true,
          },
          webmapUtils.getWMJSDimensionForLayerAndDimension(
            dimensionLayer1.id,
            'elevation',
          )!,
          {
            name: 'elevation',
            units: 'meters',
            currentValue: '7000',
          },
        ),
      ).toStrictEqual({
        name: 'elevation',
        units: 'meters',
        currentValue: '9000',
        synced: true,
        validSyncSelection: false,
      });
    });
    it('should set validSyncSelection to true if the new current value for the synced layer dimension is equal to its current value', () => {
      expect(
        produceDimensionActionForMapLayer(
          {
            name: 'elevation',
            units: 'meters',
            currentValue: '9000',
            synced: true,
            validSyncSelection: false,
          },
          webmapUtils.getWMJSDimensionForLayerAndDimension(
            dimensionLayer1.id,
            'elevation',
          )!,
          {
            name: 'elevation',
            units: 'meters',
            currentValue: '9000',
          },
        ),
      ).toStrictEqual({
        name: 'elevation',
        units: 'meters',
        currentValue: '9000',
        synced: true,
        validSyncSelection: true,
      });
    });
  });
  describe('filterCurrentValueFromDimensions', () => {
    it('should filter currentValue from the dimensions', () => {
      expect(
        filterCurrentValueFromDimensions([
          {
            name: 'elevation',
            units: 'meters',
            currentValue: '9000',
            synced: true,
            validSyncSelection: false,
          },
        ]),
      ).toEqual([
        {
          currentValue: '',
          name: 'elevation',
          synced: true,
          units: 'meters',
          validSyncSelection: false,
        },
      ]);
    });
  });
  describe('updateMinMaxValueWhenReferenceTimeChanges', () => {
    it('should update minValue and maxValue properties in reduxlayer draft object', () => {
      const testLayer = new WMLayer({
        service: 'https://testservice',
        id: 'test',
        name: 'test',
        title: 'test',
        layerType: LayerType.mapLayer,
        enabled: true,
        dimensions: [
          {
            name: 'time',
            values: '2009-01-01T10:00:00Z/2009-01-01T16:00:00Z/PT1H',
            currentValue: '2009-01-01T12:00:00Z',
            units: 'ISO8601',
          },
          {
            name: 'reference_time',
            currentValue: '2009-01-01T12:00:00Z',
            values:
              '2009-01-01T10:00:00Z,2009-01-01T11:00:00Z,2009-01-01T12:00:00Z,2009-01-01T13:00:00Z',
            units: 'ISO8601',
          },
        ],
      });

      const draftL: Draft<ReduxLayer> = {
        id: 'test',
        dimensions: [
          {
            name: 'time',
            values: '2009-01-01T10:00:00Z/2009-01-01T16:00:00Z/PT1H',
            currentValue: '2009-01-01T12:00:00Z',
            units: 'ISO8601',
          },
        ],
      };

      webmapUtils.registerWMLayer(testLayer, testLayer.id);

      updateMinMaxValueWhenReferenceTimeChanges(draftL, {
        name: 'reference_time',
        currentValue: '2009-01-01T11:00:00Z',
      });

      expect(draftL?.dimensions![0].currentValue).toBe('2009-01-01T12:00:00Z');
      expect(draftL?.dimensions![0].minValue).toBe('2009-01-01T11:00:00Z');
      expect(draftL?.dimensions![0].maxValue).toBe('2009-01-01T14:00:00Z');

      updateMinMaxValueWhenReferenceTimeChanges(draftL, {
        name: 'reference_time',
        currentValue: '2009-01-01T11:00:00Z',
      });

      expect(draftL?.dimensions![0].currentValue).toBe('2009-01-01T12:00:00Z');
      expect(draftL?.dimensions![0].minValue).toBe('2009-01-01T11:00:00Z');
      expect(draftL?.dimensions![0].maxValue).toBe('2009-01-01T14:00:00Z');

      updateMinMaxValueWhenReferenceTimeChanges(draftL, {
        name: 'reference_time',
        currentValue: '2009-01-01T10:00:00Z',
      });

      expect(draftL?.dimensions![0].currentValue).toBe('2009-01-01T12:00:00Z');
      expect(draftL?.dimensions![0].minValue).toBe('2009-01-01T10:00:00Z');
      expect(draftL?.dimensions![0].maxValue).toBe('2009-01-01T13:00:00Z');

      webmapUtils.unRegisterAllWMJSLayersAndMaps();
    });
  });
});
