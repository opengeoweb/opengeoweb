/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { layerActions } from './reducer'; // assuming layerReducer is imported from your Redux setup
import { uiActions, uiTypes } from '../../ui';
import { createMockStore } from '../../store';

describe('layersListener middleware', () => {
  const preloadedState = {
    ui: {
      dialogs: {
        [uiTypes.DialogTypes.LayerInfo]: {
          isOpen: false,
          type: uiTypes.DialogTypes.LayerInfo,
          activeMapId: '',
        },
        [uiTypes.DialogTypes.LayerManager]: {
          isOpen: true,
          type: uiTypes.DialogTypes.LayerManager,
          activeMapId: '',
        },
      },
      order: [uiTypes.DialogTypes.LayerInfo, uiTypes.DialogTypes.LayerManager],
    },
  };

  const store = createMockStore(preloadedState);

  it('should open layerinfo dialog ', async () => {
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerInfo]?.isOpen,
    ).toBeFalsy();
    const source = 'app';
    const layerName = 'test-layer-name';
    const serviceUrl = 'test.url';
    const mapId = 'test-1';

    store.dispatch(
      layerActions.showLayerInfo({
        mapId,
        source,
        layerName,
        serviceUrl,
      }),
    );
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerInfo]?.isOpen,
    ).toBeTruthy();
  });

  it('should not open layerinfo dialog if already open', async () => {
    const mapId = 'test-1';

    store.dispatch(
      uiActions.setActiveMapIdForDialog({
        mapId,
        type: uiTypes.DialogTypes.LayerInfo,
        setOpen: true,
      }),
    );

    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerInfo]?.isOpen,
    ).toBeTruthy();
    const source = 'app';
    const layerName = 'test-layer-name';
    const serviceUrl = 'test.url';

    store.dispatch(
      layerActions.showLayerInfo({
        mapId,
        source,
        layerName,
        serviceUrl,
      }),
    );
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerInfo]?.isOpen,
    ).toBeTruthy();
  });

  it('should close layerinfo dialog if layermanager is closed', async () => {
    const mapId = 'test-1';

    store.dispatch(
      uiActions.setActiveMapIdForDialog({
        mapId,
        type: uiTypes.DialogTypes.LayerInfo,
        setOpen: true,
      }),
    );

    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerManager]?.isOpen,
    ).toBeTruthy();
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerInfo]?.isOpen,
    ).toBeTruthy();

    store.dispatch(
      uiActions.setToggleOpenDialog({
        mapId,
        type: uiTypes.DialogTypes.LayerManager,
        setOpen: false,
      }),
    );
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerInfo]?.isOpen,
    ).toBeTruthy();
  });
});
