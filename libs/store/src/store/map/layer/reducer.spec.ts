/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { LayerType, WMLayer, webmapUtils } from '@opengeoweb/webmap';
import { FeatureCollection, Point, LineString } from 'geojson';
import { AnyAction } from '@reduxjs/toolkit';
import {
  NEW_FEATURE_CREATED,
  NEW_LINESTRING_CREATED,
  NEW_POINT_CREATED,
  UpdateLayerInfoPayload,
  createInterSections,
  emptyGeoJSON,
  featurePoint,
  getGeoJson,
  intersectionFeatureNL,
} from '@opengeoweb/webmap-react';
import { layerReducer, initialState, createLayer } from './reducer';
import { layerActions } from '.';
import { LayerStatus, LayerActionOrigin } from './types';
import {
  createLayersState,
  createMultipleLayersState,
  testGeoJSON,
} from '../storeTestUtils';
import {
  WmMultiDimensionLayer,
  WmMultiDimensionLayer2,
} from '../storeTestSettings';
import { mapActions } from '../..';

describe('store/mapStore/layers/reducer', () => {
  it('should return initial state if no state and action passed in', () => {
    expect(layerReducer(undefined, {} as AnyAction)).toEqual(initialState);
  });

  it('should add layer', () => {
    const layer = {
      name: 'test',
      layerType: LayerType.mapLayer,
    };
    const layerId = 'test-1';
    const result = layerReducer(
      undefined,
      layerActions.addLayer({
        layerId,
        mapId: 'map-1',
        layer,
        origin: LayerActionOrigin.layerManager,
      }),
    );

    expect(result.byId[layerId]).toEqual(
      createLayer({ ...layer, id: layerId, mapId: 'map-1' }),
    );
    expect(result.allIds.includes(layerId)).toBeTruthy();
  });

  it('should add a featurelayer', () => {
    const layer = {
      name: 'test',
      layerType: LayerType.featureLayer,
      geojson: testGeoJSON,
    };
    const layerId = 'test-1';
    const result = layerReducer(
      undefined,
      layerActions.addLayer({
        layerId,
        mapId: 'map-1',
        layer,
        origin: LayerActionOrigin.layerManager,
      }),
    );

    const expectedLayerResult = createLayer({
      ...layer,
      id: layerId,
      mapId: 'map-1',
    });
    expect(result.byId[layerId]).toEqual(expectedLayerResult);

    expect(expectedLayerResult.geojson).toEqual(expectedLayerResult.geojson);
    expect(result.byId[layerId].defaultGeoJSONProperties).toBeUndefined();
    expect(result.allIds.includes(layerId)).toBeTruthy();
  });

  it('should add a featurelayer with default defaultGeoJSONProperties', () => {
    const layer = {
      name: 'test',
      layerType: LayerType.featureLayer,
      geojson: testGeoJSON,
      defaultGeoJSONProperties: {
        fill: '#FF00FF',
        selectionType: 'box',
      },
    };
    const layerId = 'test-1';
    const result = layerReducer(
      undefined,
      layerActions.addLayer({
        layerId,
        mapId: 'map-1',
        layer,
        origin: LayerActionOrigin.layerManager,
      }),
    );

    const expectedLayerResult = createLayer({
      ...layer,
      id: layerId,
      mapId: 'map-1',
    });
    expect(result.byId[layerId]).toEqual(expectedLayerResult);

    expect(result.byId[layerId].geojson).toEqual(layer.geojson);
    expect(result.byId[layerId].defaultGeoJSONProperties).toEqual(
      layer.defaultGeoJSONProperties,
    );
    expect(result.allIds.includes(layerId)).toBeTruthy();
  });

  it('should add baselayer', () => {
    const layer = {
      name: 'test',
      id: 'test-1',
      layerType: LayerType.baseLayer,
    };
    const layerId = 'test-1';
    const result = layerReducer(
      undefined,
      layerActions.addBaseLayer({
        layerId,
        mapId: 'map-1',
        layer,
      }),
    );

    expect(result.byId[layerId]).toEqual({
      ...layer,
      id: layerId,
      dimensions: [],
      layerType: LayerType.baseLayer,
      enabled: true,
      opacity: 1,
      acceptanceTimeInMinutes: 60,
      status: LayerStatus.default,
      useLatestReferenceTime: true,
    });
    expect(result.allIds.includes(layerId)).toBeTruthy();
  });

  it('should add overlayer', () => {
    const layer = {
      name: 'test',
      id: 'test-1',
      layerType: LayerType.overLayer,
    };
    const layerId = 'test-1';
    const result = layerReducer(
      undefined,
      layerActions.addBaseLayer({
        layerId,
        mapId: 'map-1',
        layer,
      }),
    );

    expect(result.byId[layerId]).toEqual({
      ...layer,
      id: layerId,
      dimensions: [],
      layerType: LayerType.overLayer,
      enabled: true,
      opacity: 1,
      acceptanceTimeInMinutes: 60,
      status: LayerStatus.default,
      useLatestReferenceTime: true,
    });
    expect(result.allIds.includes(layerId)).toBeTruthy();
  });

  it('addBaseLayer should only accept over and base layers', () => {
    const layer = {
      name: 'test',
      id: 'test-1',
      layerType: LayerType.mapLayer,
    };
    const layerId = 'test-1';
    const result = layerReducer(
      undefined,
      layerActions.addBaseLayer({
        layerId,
        mapId: 'map-1',
        layer,
      }),
    );

    expect(result).toEqual(initialState);
  });

  it('should clear all layers for a map when setting a mapPreset', () => {
    const initialMockState = {
      byId: {
        layer_1: {
          id: 'layer_1',
          mapId: 'map-1',
        },
        layer_2: {
          id: 'layer_2',
          mapId: 'map-1',
        },
        layer_3: {
          id: 'layer_3',
          mapId: 'map-2',
        },
      },
      allIds: ['layer_1', 'layer_2', 'layer_3'],
      availableBaseLayers: {
        byId: {},
        allIds: [],
      },
    };

    const result = layerReducer(
      initialMockState,
      mapActions.setMapPreset({
        mapId: 'map-1',
        initialProps: {
          mapPreset: {},
        },
      }),
    );

    expect(result.byId.layer_1).toBeUndefined();
    expect(result.byId.layer_2).toBeUndefined();
    expect(result.byId.layer_3).toEqual(initialMockState.byId.layer_3);
    expect(result.allIds.length).toEqual(1);
  });

  it('should change dimensions', () => {
    const layerId = 'test-1';
    const initialDimension = { name: 'test-dim', currentValue: 'initialvalue' };
    const initialMockState = createLayersState(layerId, {
      mapId: 'mapid1',
      dimensions: [initialDimension],
    });

    // initial value
    expect(initialMockState.byId[layerId].dimensions).toEqual([
      initialDimension,
    ]);

    // change dimensions
    const newDimension = { name: 'test-dim', currentValue: 'newvalue' };
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeDimension({
        origin: LayerActionOrigin.layerManager,
        layerId,
        dimension: newDimension,
      }),
    );

    expect(result.byId[layerId].dimensions).toEqual([
      { ...newDimension, validSyncSelection: true },
    ]);
  });

  it('should change dimension sync state if passed', () => {
    const layerId = 'test-1';
    const initialDimension = {
      name: 'test-dim',
      currentValue: 'initialvalue',
      synced: false,
    };
    const initialMockState = createLayersState(layerId, {
      mapId: 'mapid1',
      dimensions: [initialDimension],
    });

    // initial value
    expect(initialMockState.byId[layerId].dimensions).toEqual([
      initialDimension,
    ]);

    // change dimensions
    const newDimension = {
      name: 'test-dim',
      currentValue: 'initialvalue',
      synced: true,
    };
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeDimension({
        origin: LayerActionOrigin.layerManager,
        layerId,
        dimension: newDimension,
      }),
    );

    expect(result.byId[layerId].dimensions).toEqual([
      { ...newDimension, validSyncSelection: true },
    ]);

    const newDimension2 = {
      name: 'test-dim',
      currentValue: 'initialvalue',
      synced: false,
    };
    const result2 = layerReducer(
      initialMockState,
      layerActions.layerChangeDimension({
        origin: LayerActionOrigin.layerManager,
        layerId,
        dimension: newDimension2,
      }),
    );

    expect(result2.byId[layerId].dimensions).toEqual([
      { ...newDimension2, validSyncSelection: true },
    ]);
  });

  it('should extend dimensions', () => {
    const layerId = 'test-1';
    const initialDimension = { name: 'test-start', currentValue: 'test-start' };
    const initialMockState = createLayersState(layerId, {
      mapId: 'mapid1',
      dimensions: [initialDimension],
    });

    // initial value
    expect(initialMockState.byId[layerId].dimensions).toEqual([
      initialDimension,
    ]);

    // change dimensions
    const newDimension = { name: 'test-end', currentValue: 'test-end' };
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeDimension({
        origin: LayerActionOrigin.layerManager,
        layerId,
        dimension: newDimension,
      }),
    );

    expect(result.byId[layerId].dimensions).toEqual([
      initialDimension,
      { ...newDimension, validSyncSelection: true },
    ]);
  });

  it('should update the dimension for other layers on the map that have synced: true, layers on other maps should not change', () => {
    const dimensionLayer1 = {
      service: 'https://testservice',
      id: 'multiDimensionLayerMock',
      name: 'MULTI_DIMENSION_LAYER',
      title: 'MULTI_DIMENSION_LAYER',
      layerType: LayerType.mapLayer,
      enabled: true,
      dimensions: [
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
          synced: true,
        },
      ],
    };
    const dimensionLayer2 = {
      service: 'https://testservicedimensions',
      id: 'multiDimensionLayerMock2',
      name: 'netcdf_5dims',
      layerType: LayerType.mapLayer,
      dimensions: [
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
          synced: true,
        },
      ],
    };
    const dimensionLayer3 = {
      service: 'https://testservice',
      id: 'multiDimensionLayerMock3',
      name: 'MULTI_DIMENSION_LAYER',
      title: 'MULTI_DIMENSION_LAYER',
      layerType: LayerType.mapLayer,
      enabled: true,
      dimensions: [
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
          synced: true,
        },
      ],
    };
    const layers = [dimensionLayer1, dimensionLayer2, dimensionLayer3];
    webmapUtils.registerWMLayer(
      WmMultiDimensionLayer2,
      'multiDimensionLayerMock2',
    );
    webmapUtils.registerWMLayer(
      WmMultiDimensionLayer,
      'multiDimensionLayerMock',
    );
    const mapId = 'mapId_1';
    const initialMockState = createMultipleLayersState(layers, mapId);
    // Change mapId for third layer
    initialMockState.byId[dimensionLayer3.id].mapId = 'mapId_2';

    // initial value
    expect(initialMockState.byId[dimensionLayer1.id].dimensions).toEqual(
      dimensionLayer1.dimensions,
    );
    expect(initialMockState.byId[dimensionLayer2.id].dimensions).toEqual(
      dimensionLayer2.dimensions,
    );

    // change dimension
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeDimension({
        origin: LayerActionOrigin.layerManager,
        layerId: dimensionLayer1.id,
        dimension: {
          name: 'elevation',
          units: 'meters',
          currentValue: '5000',
        },
      }),
    );

    expect(result.byId[dimensionLayer1.id].dimensions![0].currentValue).toEqual(
      '5000',
    );
    expect(result.byId[dimensionLayer2.id].dimensions![0].currentValue).toEqual(
      '5000',
    );
    expect(result.byId[dimensionLayer3.id].dimensions![0].currentValue).toEqual(
      '9000',
    );
  });

  it('should not update the dimension for other layers on the map that have synced: false', () => {
    webmapUtils.unRegisterAllWMJSLayersAndMaps();
    const dimensionLayer1 = {
      service: 'https://testservice',
      id: 'multiDimensionLayerMock',
      name: 'MULTI_DIMENSION_LAYER',
      title: 'MULTI_DIMENSION_LAYER',
      layerType: LayerType.mapLayer,
      enabled: true,
      dimensions: [
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
          synced: true,
        },
      ],
    };
    const dimensionLayer2 = {
      service: 'https://testservicedimensions',
      id: 'multiDimensionLayerMock2',
      name: 'netcdf_5dims',
      layerType: LayerType.mapLayer,
      dimensions: [
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
          synced: false,
        },
      ],
    };
    const layers = [dimensionLayer1, dimensionLayer2];
    webmapUtils.registerWMLayer(
      WmMultiDimensionLayer2,
      'multiDimensionLayerMock2',
    );
    webmapUtils.registerWMLayer(
      WmMultiDimensionLayer,
      'multiDimensionLayerMock',
    );
    const mapId = 'mapId_1';
    const initialMockState = createMultipleLayersState(layers, mapId);

    // initial value
    expect(initialMockState.byId[dimensionLayer1.id].dimensions).toEqual(
      dimensionLayer1.dimensions,
    );
    expect(initialMockState.byId[dimensionLayer2.id].dimensions).toEqual(
      dimensionLayer2.dimensions,
    );

    expect(
      initialMockState.byId[dimensionLayer1.id].dimensions![0].currentValue,
    ).toEqual('9000');
    expect(
      initialMockState.byId[dimensionLayer2.id].dimensions![0].currentValue,
    ).toEqual('9000');

    // change dimension
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeDimension({
        origin: LayerActionOrigin.layerManager,
        layerId: dimensionLayer1.id,
        dimension: {
          name: 'elevation',
          units: 'meters',
          currentValue: '5000',
        },
      }),
    );

    expect(result.byId[dimensionLayer1.id].dimensions![0].currentValue).toEqual(
      '5000',
    );
    expect(result.byId[dimensionLayer2.id].dimensions![0].currentValue).toEqual(
      '9000',
    );
  });

  it('should also update linked dimensions', () => {
    const dimensionLayer1 = {
      service: 'https://testservice',
      id: 'multiDimensionLayerMock',
      name: 'MULTI_DIMENSION_LAYER',
      title: 'MULTI_DIMENSION_LAYER',
      layerType: LayerType.mapLayer,
      enabled: true,
      dimensions: [
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: '2017-01-01T00:25:00Z',
          values:
            '2017-01-01T00:25:00Z,2017-01-01T00:30:00Z,2017-01-01T00:35:00Z',
        },
      ],
    };
    const dimensionLayer2 = {
      service: 'https://testservicedimensions',
      id: 'multiDimensionLayerMock2',
      name: 'netcdf_5dims',
      layerType: LayerType.mapLayer,
      dimensions: [
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: '2017-01-01T00:25:00Z',
          values:
            '2017-01-01T00:25:00Z,2017-01-01T00:30:00Z,2017-01-01T00:35:00Z',
        },
      ],
    };
    const layers = [dimensionLayer1, dimensionLayer2];
    webmapUtils.registerWMLayer(
      new WMLayer(dimensionLayer2),
      'multiDimensionLayerMock2',
    );
    webmapUtils.registerWMLayer(
      new WMLayer(dimensionLayer1),
      'multiDimensionLayerMock',
    );
    const mapId = 'mapId_1';
    const initialMockState = createMultipleLayersState(layers, mapId);

    // initial value
    expect(initialMockState.byId[dimensionLayer1.id].dimensions).toEqual(
      dimensionLayer1.dimensions,
    );
    expect(initialMockState.byId[dimensionLayer2.id].dimensions).toEqual(
      dimensionLayer2.dimensions,
    );

    // change dimension
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeDimension({
        origin: LayerActionOrigin.layerManager,
        layerId: dimensionLayer1.id,
        dimension: {
          name: 'time',
          units: 'ISO8601',
          currentValue: '2017-01-01T00:35:00Z',
        },
      }),
    );

    expect(result.byId[dimensionLayer1.id].dimensions![0].currentValue).toEqual(
      '2017-01-01T00:35:00Z',
    );
    expect(result.byId[dimensionLayer2.id].dimensions![0].currentValue).toEqual(
      '2017-01-01T00:35:00Z',
    );
  });

  it('should enable layer', () => {
    const layerId = 'test-1';
    const initialMockState = createLayersState(layerId, {
      enabled: false,
    });

    // initial value
    expect(initialMockState.byId[layerId].enabled).toBeFalsy();

    // enable layer
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeEnabled({
        layerId,
        enabled: true,
      }),
    );

    expect(result.byId[layerId].enabled).toBeTruthy();
  });

  it('should disable layer', () => {
    const layerId = 'test-1';
    const initialMockState = createLayersState(layerId, {
      enabled: true,
    });

    // initial value
    expect(initialMockState.byId[layerId].enabled).toBeTruthy();

    // disable layer
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeEnabled({
        layerId,
        enabled: false,
      }),
    );

    expect(result.byId[layerId].enabled).toBeFalsy();
  });

  it('should change opacity of a layer', () => {
    const layerId = 'test-1';
    const initialMockState = createLayersState(layerId, {
      opacity: 0.3,
    });

    // initial state
    expect(initialMockState.byId[layerId].opacity).toEqual(0.3);

    // change opacity
    const opacity = 0.5;
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeOpacity({
        layerId,
        opacity,
      }),
    );

    expect(result.byId[layerId].opacity).toEqual(opacity);
  });

  it('should change style of a layer', () => {
    const layerId = 'test-1';
    const initialMockState = createLayersState(layerId, {
      style: 'initial-style',
    });

    // initial state
    expect(initialMockState.byId[layerId].style).toEqual('initial-style');

    // change style
    const style = 'new-style';
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeStyle({
        layerId,
        style,
      }),
    );

    expect(result.byId[layerId].style).toEqual(style);
  });

  it('should change name of a layer', () => {
    const layerId = 'test-1';
    const initialMockState = createLayersState(layerId, {
      name: 'initial-name',
    });

    // initial state
    expect(initialMockState.byId[layerId].name).toEqual('initial-name');

    // change name
    const name = 'new-name';
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeName({
        layerId,
        name,
      }),
    );

    expect(result.byId[layerId].name).toEqual(name);
  });

  it('should change geojson of layer', () => {
    const layerId = 'layerId';
    const geojsonBefore: FeatureCollection = {
      type: 'FeatureCollection',
      features: [],
    };
    const initialMockState = createLayersState(layerId, {
      geojson: geojsonBefore,
    });
    expect(initialMockState.byId[layerId].geojson).toEqual(geojsonBefore);

    const geojsonAfter: FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [1, 1] },
          properties: { name: 'name' },
        },
      ],
    };
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeGeojson({ layerId, geojson: geojsonAfter }),
    );

    expect(result.byId[layerId].geojson).toEqual(geojsonAfter);
  });

  it('should set geojson of layer based on other layer', () => {
    const targetLayerId = 'targetLayerId';
    const sourceLayerId = 'sourceLayerId';
    const targetGeojson: FeatureCollection = {
      type: 'FeatureCollection',
      features: [],
    };
    const sourceGeojson: FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [1, 1] },
          properties: { name: 'name' },
        },
      ],
    };
    const initialMockState = createMultipleLayersState(
      [
        {
          id: targetLayerId,
          geojson: targetGeojson,
        },
        { id: sourceLayerId, geojson: sourceGeojson },
      ],
      'map1',
    );
    const resultLayerState = layerReducer(
      initialMockState,
      layerActions.layerSetGeojsonFromLayer({
        layerId: targetLayerId,
        sourceLayerId,
      }),
    );
    expect(resultLayerState.byId[targetLayerId].geojson).toEqual(sourceGeojson);
  });

  it('should set geojson of layer to undefined based on other layer if other layer not found', () => {
    const targetLayerId = 'targetLayerId';
    const nonExistentLayerId = 'loremIpsum';
    const targetGeojson: FeatureCollection = {
      type: 'FeatureCollection',
      features: [],
    };
    const initialMockState = createLayersState(targetLayerId, {
      geojson: targetGeojson,
    });
    const resultLayerState = layerReducer(
      initialMockState,
      layerActions.layerSetGeojsonFromLayer({
        layerId: targetLayerId,
        sourceLayerId: nonExistentLayerId,
      }),
    );
    expect(resultLayerState.byId[targetLayerId].geojson).toBeUndefined();
  });

  it('should set layers', () => {
    const initialMockState = {
      byId: {
        'layer-1-map-1': createLayer({ id: 'layer-1-map-1', mapId: 'map-1' }),
        'layer-2-map-2': createLayer({ id: 'layer-2-map-2', mapId: 'map-2' }),
        'base-layer-1-map-1': createLayer({
          id: 'base-layer-1-map-1',
          mapId: 'map-1',
          layerType: LayerType.baseLayer,
        }),
        'base-layer-2-map-1': createLayer({
          id: 'base-layer-2-map-1',
          mapId: 'map-1',
          layerType: LayerType.baseLayer,
        }),
        'base-layer-1-map-2': createLayer({
          id: 'base-layer-1',
          mapId: 'map-2',
          layerType: LayerType.baseLayer,
        }),
      },
      allIds: [
        'layer-1-map-1',
        'layer-2-map-2',
        'base-layer-1-map-1',
        'base-layer-2-map-1',
        'base-layer-1-map-2',
      ],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(5);
    expect(initialMockState.allIds.includes('layer-1-map-1')).toBeTruthy();
    expect(initialMockState.byId['layer-1-map-1']).toBeDefined();
    expect(initialMockState.allIds.includes('layer-2-map-2')).toBeTruthy();
    expect(initialMockState.byId['layer-2-map-2']).toBeDefined();
    expect(initialMockState.allIds.includes('base-layer-1-map-1')).toBeTruthy();
    expect(initialMockState.byId['base-layer-1-map-1']).toBeDefined();

    // set layers
    const layers = [{ id: 'layer-3-id', layerType: LayerType.mapLayer }];

    const result = layerReducer(
      initialMockState,
      layerActions.setLayers({ mapId: 'map-1', layers }),
    );

    expect(result.allIds).toHaveLength(5);
    expect(result.allIds.includes('layer-1')).toBeFalsy();
    expect(result.byId['layer-1']).toBeUndefined();

    layers.forEach((layer) => {
      expect(result.allIds.includes(layer.id));
      expect(result.byId[layer.id]).toBeDefined();
    });

    // layers with different mapIds should be preserved
    expect(result.allIds.includes('layer-2-map-2')).toBeTruthy();
    expect(result.byId['layer-2-map-2']).toBeDefined();
    // baselayers should be preserved
    expect(result.allIds.includes('base-layer-1-map-1')).toBeTruthy();
    expect(result.byId['base-layer-1-map-1']).toBeDefined();
    expect(result.allIds.includes('base-layer-2-map-1')).toBeTruthy();
    expect(result.byId['base-layer-2-map-1']).toBeDefined();

    // Change multiple layers for map should preserve mapids
    const multipleLayers = [
      { id: 'layer-4-id', layerType: LayerType.mapLayer },
      { id: 'layer-5-id', layerType: LayerType.mapLayer },
      { id: 'layer-6-id', layerType: LayerType.mapLayer },
    ];

    const resultMultiple = layerReducer(
      result,
      layerActions.setLayers({ mapId: 'map-1', layers: multipleLayers }),
    );

    expect(resultMultiple.allIds).toHaveLength(7);
    expect(resultMultiple.allIds.includes('layer-3-id')).toBeFalsy();
    expect(resultMultiple.byId['layer-3-id']).toBeUndefined();

    multipleLayers.forEach((layer) => {
      expect(resultMultiple.allIds.includes(layer.id));
      expect(resultMultiple.byId[layer.id]).toBeDefined();
    });

    // layers with different mapIds should be preserved
    expect(resultMultiple.allIds.includes('layer-2-map-2')).toBeTruthy();
    expect(resultMultiple.byId['layer-2-map-2']).toBeDefined();
    // baselayers should be preserved
    expect(resultMultiple.allIds.includes('base-layer-1-map-1')).toBeTruthy();
    expect(resultMultiple.byId['base-layer-1-map-1']).toBeDefined();

    // Change multiple layers to less for map should preserve mapids
    const multipleLayersLess = [
      { id: 'layer-6-id', layerType: LayerType.mapLayer },
    ];

    const resultMultipleLess = layerReducer(
      resultMultiple,
      layerActions.setLayers({ mapId: 'map-1', layers: multipleLayersLess }),
    );

    multipleLayersLess.forEach((layer) => {
      expect(resultMultipleLess.allIds.includes(layer.id));
      expect(resultMultipleLess.byId[layer.id]).toBeDefined();
    });

    // Setting duplicate Id should do nothing
    const duplicateLayers = [
      { id: 'layer-5-id', layerType: LayerType.mapLayer },
      { id: 'layer-6-id', layerType: LayerType.mapLayer },
      { id: 'layer-5-id', layerType: LayerType.mapLayer },
    ];

    const resultduplicateLayers = layerReducer(
      initialMockState,
      layerActions.setLayers({ mapId: 'map-1', layers: duplicateLayers }),
    );
    duplicateLayers.forEach((layer) => {
      expect(resultduplicateLayers.byId[layer.id]).toBeUndefined();
    });

    // Setting duplicate Id should do nothing
    const layerAlreadyUsed = [
      { id: 'layer-1-map-1', mapId: 'map-2', layerType: LayerType.mapLayer },
    ];

    layerAlreadyUsed.forEach((layer) => {
      expect(
        initialMockState.byId[layer.id as keyof typeof initialMockState.byId],
      ).toBeDefined();
      expect(
        initialMockState.byId[layer.id as keyof typeof initialMockState.byId]
          .mapId,
      ).toBe('map-1');
    });

    const resultAlreadyUsedLayers = layerReducer(
      initialMockState,
      layerActions.setLayers({ mapId: 'map-1', layers: layerAlreadyUsed }),
    );
    layerAlreadyUsed.forEach((layer) => {
      expect(resultAlreadyUsedLayers.byId[layer.id]).toBeDefined();
      expect(resultAlreadyUsedLayers.byId[layer.id].mapId).toBe('map-1');
    });
  });

  it('should set baselayers', () => {
    const initialMockState = {
      byId: {
        'base-layer-1': createLayer({
          id: 'layer-1',
          mapId: 'map-1',
          layerType: LayerType.baseLayer,
        }),
        'over-layer-1': createLayer({
          id: 'over-layer-1',
          mapId: 'map-1',
          layerType: LayerType.overLayer,
        }),
        'base-layer-2': createLayer({
          id: 'layer-2',
          mapId: 'map-2',
          layerType: LayerType.baseLayer,
        }),
        'over-layer-2': createLayer({
          id: 'over-layer-2',
          mapId: 'map-2',
          layerType: LayerType.overLayer,
        }),
        'layer-1': createLayer({
          id: 'layer-1',
          mapId: 'map-3',
        }),
      },
      allIds: [
        'base-layer-1',
        'over-layer-1',
        'base-layer-2',
        'over-layer-2',
        'layer-1',
      ],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(5);
    expect(initialMockState.allIds.includes('base-layer-1')).toBeTruthy();
    expect(initialMockState.byId['over-layer-1']).toBeDefined();
    expect(initialMockState.allIds.includes('over-layer-1')).toBeTruthy();
    expect(initialMockState.byId['base-layer-1']).toBeDefined();
    expect(initialMockState.allIds.includes('base-layer-2')).toBeTruthy();
    expect(initialMockState.byId['base-layer-2']).toBeDefined();
    expect(initialMockState.allIds.includes('over-layer-2')).toBeTruthy();
    expect(initialMockState.byId['over-layer-2']).toBeDefined();
    expect(initialMockState.allIds.includes('layer-1')).toBeTruthy();
    expect(initialMockState.byId['layer-1']).toBeDefined();

    // set baselayers
    const layers = [
      { id: 'base-layer-4', layerType: LayerType.baseLayer },
      { id: 'base-layer-3', layerType: LayerType.baseLayer },
      { id: 'over-layer-3', layerType: LayerType.overLayer },
    ];

    const result = layerReducer(
      initialMockState,
      layerActions.setBaseLayers({ mapId: 'map-1', layers }),
    );

    expect(result.allIds).toHaveLength(6);
    expect(result.allIds.includes('base-layer-1')).toBeFalsy();
    expect(result.byId['base-layer-1']).toBeUndefined();
    expect(result.allIds.includes('over-layer-1')).toBeFalsy();
    expect(result.byId['over-layer-1']).toBeUndefined();

    layers.forEach((layer) => {
      expect(result.allIds.includes(layer.id));
      expect(result.byId[layer.id]).toBeDefined();
    });

    // baseLayers with different mapIds should be preserved
    expect(result.allIds.includes('base-layer-2')).toBeTruthy();
    expect(result.byId['base-layer-2']).toBeDefined();
    expect(result.allIds.includes('over-layer-2')).toBeTruthy();
    expect(result.byId['over-layer-2']).toBeDefined();
    expect(result.allIds.includes('base-layer-3')).toBeTruthy();
    expect(result.byId['base-layer-3']).toBeDefined();
    // layers should be preserved
    expect(result.allIds.includes('layer-1')).toBeTruthy();
    expect(result.byId['layer-1']).toBeDefined();

    // if wrong layerType is passed in, nothing should be done for that layer
    const result2 = layerReducer(
      initialMockState,
      layerActions.setBaseLayers({
        mapId: 'map-1',
        layers: [
          {
            id: 'faultyLayer1',
            name: 'faultyLayer1',
            layerType: LayerType.mapLayer,
          },
        ],
      }),
    );

    expect(result2.allIds).toHaveLength(5);
    expect(result2.allIds.includes('base-layer-1')).toBeTruthy();
    expect(result2.byId['over-layer-1']).toBeDefined();
    expect(result2.allIds.includes('over-layer-1')).toBeTruthy();
    expect(result2.byId['base-layer-1']).toBeDefined();
    expect(result2.allIds.includes('base-layer-2')).toBeTruthy();
    expect(result2.byId['base-layer-2']).toBeDefined();
    expect(result2.allIds.includes('over-layer-2')).toBeTruthy();
    expect(result2.byId['over-layer-2']).toBeDefined();
    expect(result2.allIds.includes('layer-1')).toBeTruthy();
    expect(result2.byId['layer-1']).toBeDefined();
  });

  it('should delete a layer', () => {
    const initialMockState = {
      byId: {
        'test-1': {
          id: 'test-1',
          layerType: LayerType.mapLayer,
        },
        'test-2': {
          id: 'test-2',
          layerType: LayerType.mapLayer,
        },
      },
      allIds: ['test-1', 'test-2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete layer
    const layerId = 'test-1';
    const result = layerReducer(
      initialMockState,
      layerActions.layerDelete({ layerId, mapId: 'test-1', layerIndex: 0 }),
    );

    expect(result.allIds).toHaveLength(1);
    expect(result.allIds.includes(layerId)).toBeFalsy();
    expect(result.byId[layerId]).toBeUndefined();

    expect(result.allIds.includes('test-2')).toBeTruthy();
    expect(result.byId['test-2']).toBeDefined();
  });

  it('should delete a baselayer', () => {
    const initialMockState = {
      byId: {
        'test-BaseLayer1': {
          id: 'test-BaseLayer1',
          layerType: LayerType.baseLayer,
        },
        'test-BaseLayer2': {
          id: 'test-BaseLayer2',
          layerType: LayerType.baseLayer,
        },
      },
      allIds: ['test-BaseLayer1', 'test-BaseLayer2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete baselayer
    const layerId = 'test-BaseLayer1';
    const result = layerReducer(
      initialMockState,
      layerActions.baseLayerDelete({
        layerId,
        mapId: 'test-BaseLayer1',
        layerIndex: 0,
      }),
    );

    expect(result.allIds).toHaveLength(1);
    expect(result.allIds.includes(layerId)).toBeFalsy();
    expect(result.byId[layerId]).toBeUndefined();

    expect(result.allIds.includes('test-BaseLayer2')).toBeTruthy();
    expect(result.byId['test-BaseLayer2']).toBeDefined();
  });

  it('should error a layer', () => {
    const initialMockState = {
      byId: {
        'test-1': {
          id: 'test-1',
          layerType: LayerType.mapLayer,
          status: LayerStatus.default,
        },
      },
      allIds: ['test-1'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.byId['test-1'].status).toEqual(LayerStatus.default);

    const result = layerReducer(
      initialMockState,
      layerActions.layerError({ layerId: 'test-1', error: 'error' }),
    );

    expect(result.byId['test-1'].status).toEqual(LayerStatus.error);
  });

  it('should delete a layer', () => {
    const initialMockState = {
      byId: {
        'test-1': {
          id: 'test-1',
          layerType: LayerType.mapLayer,
        },
        'test-2': {
          id: 'test-2',
          layerType: LayerType.mapLayer,
        },
      },
      allIds: ['test-1', 'test-2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete layer
    const layerId = 'test-1';
    const result = layerReducer(
      initialMockState,
      layerActions.layerDelete({ layerId, mapId: 'test-1', layerIndex: 0 }),
    );

    expect(result.allIds).toHaveLength(1);
    expect(result.allIds.includes(layerId)).toBeFalsy();
    expect(result.byId[layerId]).toBeUndefined();

    expect(result.allIds.includes('test-2')).toBeTruthy();
    expect(result.byId['test-2']).toBeDefined();
  });

  it('should delete a baselayer', () => {
    const initialMockState = {
      byId: {
        'test-BaseLayer1': {
          id: 'test-BaseLayer1',
          layerType: LayerType.baseLayer,
        },
        'test-BaseLayer2': {
          id: 'test-BaseLayer2',
          layerType: LayerType.baseLayer,
        },
      },
      allIds: ['test-BaseLayer1', 'test-BaseLayer2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete baselayer
    const layerId = 'test-BaseLayer1';
    const result = layerReducer(
      initialMockState,
      layerActions.baseLayerDelete({
        layerId,
        mapId: 'test-BaseLayer1',
        layerIndex: 0,
      }),
    );

    expect(result.allIds).toHaveLength(1);
    expect(result.allIds.includes(layerId)).toBeFalsy();
    expect(result.byId[layerId]).toBeUndefined();

    expect(result.allIds.includes('test-BaseLayer2')).toBeTruthy();
    expect(result.byId['test-BaseLayer2']).toBeDefined();
  });

  it('should delete an overlayer', () => {
    const initialMockState = {
      byId: {
        'test-overLayer1': {
          id: 'test-overLayer1',
          layerType: LayerType.overLayer,
        },
        'test-overLayer2': {
          id: 'test-overLayer2',
          layerType: LayerType.overLayer,
        },
      },
      allIds: ['test-overLayer1', 'test-overLayer2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete overlayer
    const layerId = 'test-overLayer1';
    const result = layerReducer(
      initialMockState,
      layerActions.baseLayerDelete({
        layerId,
        mapId: 'test-map1',
        layerIndex: 0,
      }),
    );

    expect(result.allIds).toHaveLength(1);
    expect(result.allIds.includes(layerId)).toBeFalsy();
    expect(result.byId[layerId]).toBeUndefined();

    expect(result.allIds.includes('test-overLayer2')).toBeTruthy();
    expect(result.byId['test-overLayer2']).toBeDefined();
  });

  it('baseLayerDelete should return state if no overlayer or baselayer is passed', () => {
    const initialMockState = {
      byId: {
        'test-overLayer1': {
          id: 'test-overLayer1',
          layerType: LayerType.overLayer,
        },
        'test-normalLayer2': {
          id: 'test-normalLayer2',
          layerType: LayerType.mapLayer,
        },
      },
      allIds: ['test-overLayer1', 'test-normalLayer2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete overlayer
    const layerId = 'test-normalLayer2';
    const result = layerReducer(
      initialMockState,
      layerActions.baseLayerDelete({
        layerId,
        mapId: 'test-map1',
        layerIndex: 0,
      }),
    );

    expect(result.allIds).toHaveLength(2);
    expect(result.allIds.includes('test-normalLayer2')).toBeTruthy();
    expect(result.byId['test-normalLayer2']).toBeDefined();

    expect(result.allIds.includes('test-overLayer1')).toBeTruthy();
    expect(result.byId['test-overLayer1']).toBeDefined();
  });

  it('should add an available baselayer', () => {
    const layer = {
      name: 'test',
      id: 'test-1',
      layerType: LayerType.baseLayer,
      mapId: 'mapid1',
    };
    const result = layerReducer(
      undefined,
      layerActions.addAvailableBaseLayer({
        layer,
      }),
    );

    expect(result.availableBaseLayers.byId[layer.id]).toEqual({
      ...layer,
      id: layer.id,
      dimensions: [],
      layerType: LayerType.baseLayer,
      enabled: true,
      opacity: 1,
      acceptanceTimeInMinutes: 60,
      status: LayerStatus.default,
      useLatestReferenceTime: true,
    });
    expect(result.availableBaseLayers.allIds.includes(layer.id)).toBeTruthy();

    // Adding the same base layer again should not result in any changes
    const result2 = layerReducer(
      result,
      layerActions.addAvailableBaseLayer({
        layer,
      }),
    );

    expect(result2.availableBaseLayers.allIds).toHaveLength(1);
    expect(result2.availableBaseLayers.byId[layer.id]).toEqual({
      ...layer,
      id: layer.id,
      dimensions: [],
      layerType: LayerType.baseLayer,
      enabled: true,
      opacity: 1,
      acceptanceTimeInMinutes: 60,
      status: LayerStatus.default,
      useLatestReferenceTime: true,
    });
    expect(result2.availableBaseLayers.allIds.includes(layer.id)).toBeTruthy();

    // Adding a new baselayer should not affect the exiting available base layers
    const layer3 = {
      name: 'test3',
      id: 'test-3',
      layerType: LayerType.baseLayer,
      mapId: 'mapid1',
    };

    const result3 = layerReducer(
      result2,
      layerActions.addAvailableBaseLayer({
        layer: layer3,
      }),
    );

    expect(result3.availableBaseLayers.allIds).toHaveLength(2);
    expect(result3.availableBaseLayers.allIds.includes('test-1')).toBeTruthy();
    expect(result3.availableBaseLayers.byId['test-1']).toBeDefined();
    expect(result3.availableBaseLayers.allIds.includes('test-3')).toBeTruthy();
    expect(result3.availableBaseLayers.byId['test-3']).toBeDefined();

    // Adding a new baselayer without mapId should not be added
    const layer4 = {
      name: 'test4',
      id: 'test-4',
      layerType: LayerType.baseLayer,
    };

    const result4 = layerReducer(
      result3,
      layerActions.addAvailableBaseLayer({
        layer: layer4,
      }),
    );

    expect(result4.availableBaseLayers.allIds).toHaveLength(2);
    expect(result4.availableBaseLayers.allIds.includes('test-1')).toBeTruthy();
    expect(result4.availableBaseLayers.byId['test-1']).toBeDefined();
    expect(result4.availableBaseLayers.allIds.includes('test-3')).toBeTruthy();
    expect(result4.availableBaseLayers.byId['test-3']).toBeDefined();
  });

  it('should add multiple available baselayers', () => {
    const layer = {
      name: 'test',
      id: 'test-1',
      layerType: LayerType.baseLayer,
      mapId: 'mapid1',
    };
    const layer2 = {
      name: 'test2',
      id: 'test-2',
      layerType: LayerType.baseLayer,
      mapId: 'mapid1',
    };
    const result = layerReducer(
      undefined,
      layerActions.addAvailableBaseLayers({
        layers: [layer, layer2],
      }),
    );

    expect(result.availableBaseLayers.allIds).toHaveLength(2);
    expect(result.availableBaseLayers.allIds.includes('test-1')).toBeTruthy();
    expect(result.availableBaseLayers.byId['test-1']).toBeDefined();
    expect(result.availableBaseLayers.allIds.includes('test-2')).toBeTruthy();
    expect(result.availableBaseLayers.byId['test-2']).toBeDefined();

    // Adding the same baselayer should not result in any changes
    const result2 = layerReducer(
      result,
      layerActions.addAvailableBaseLayers({
        layers: [layer2],
      }),
    );

    expect(result2.availableBaseLayers.allIds).toHaveLength(2);
    expect(result2.availableBaseLayers.allIds.includes('test-1')).toBeTruthy();
    expect(result2.availableBaseLayers.byId['test-1']).toBeDefined();
    expect(result2.availableBaseLayers.allIds.includes('test-2')).toBeTruthy();
    expect(result2.availableBaseLayers.byId['test-2']).toBeDefined();

    // Adding a new baselayer should not affect the exiting available base layers
    const layer3 = {
      name: 'test3',
      id: 'test-3',
      mapId: 'mapid1',
      layerType: LayerType.baseLayer,
    };

    const result3 = layerReducer(
      result2,
      layerActions.addAvailableBaseLayers({
        layers: [layer3],
      }),
    );
    expect(result3.availableBaseLayers.allIds).toHaveLength(3);
    expect(result3.availableBaseLayers.allIds.includes('test-1')).toBeTruthy();
    expect(result3.availableBaseLayers.byId['test-1']).toBeDefined();
    expect(result3.availableBaseLayers.allIds.includes('test-2')).toBeTruthy();
    expect(result3.availableBaseLayers.byId['test-2']).toBeDefined();
    expect(result3.availableBaseLayers.allIds.includes('test-3')).toBeTruthy();
    expect(result3.availableBaseLayers.byId['test-3']).toBeDefined();
  });

  it('should ignore a layer with already existing id', () => {
    const initialMockState = {
      byId: {
        'test-1': {
          id: 'test-1',
          layerType: LayerType.mapLayer,
        },
      },
      allIds: ['test-1'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    const layer = {
      name: 'test',
      id: 'test-1',
      layerType: LayerType.baseLayer,
      mapId: 'mapid1',
    };

    expect(initialMockState.allIds).toHaveLength(1);
    expect(initialMockState.allIds.includes('test-1')).toBeTruthy();

    const result = layerReducer(
      initialMockState,
      layerActions.addLayer({
        layer: { ...layer },
        layerId: 'test-1',
        mapId: 'mapid1',
        origin: LayerActionOrigin.layerManager,
      }),
    );

    expect(result.allIds).toHaveLength(1);
    expect(result.allIds.includes('test-1')).toBeTruthy();
  });

  it('should duplicate layer', () => {
    const testLayer = {
      id: 'test-1',
      mapId: 'mapid-1',
      layerType: LayerType.mapLayer,
    };
    const initialMockState = {
      byId: {
        'test-1': testLayer,
      },
      allIds: ['test-1'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    const newLayerId = 'test-2';
    const result = layerReducer(
      initialMockState,
      layerActions.duplicateMapLayer({
        oldLayerId: 'test-1',
        newLayerId,
        mapId: 'mapid-1',
        origin: LayerActionOrigin.layerManager,
      }),
    );

    expect(result.byId[newLayerId]).toEqual(
      createLayer({
        ...testLayer,
        id: newLayerId,
      }),
    );
  });
  it('should ignore duplicate layer with already existing id', () => {
    const testLayer = {
      id: 'test-1',
      mapId: 'mapid-1',
      layerType: LayerType.mapLayer,
    };
    const initialMockState = {
      byId: {
        'test-1': testLayer,
      },
      allIds: ['test-1'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    const newLayerId = 'test-1';
    const result = layerReducer(
      initialMockState,
      layerActions.duplicateMapLayer({
        oldLayerId: 'test-1',
        newLayerId,
        mapId: 'mapid-1',
        origin: LayerActionOrigin.layerManager,
      }),
    );

    expect(result).toEqual(initialMockState);
  });
  it('should not duplicate layer if old layer is not found', () => {
    const initialMockState = {
      byId: {},
      allIds: [],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    const newLayerId = 'test-2';
    const result = layerReducer(
      initialMockState,
      layerActions.duplicateMapLayer({
        oldLayerId: 'test-1',
        newLayerId,
        mapId: 'mapid-1',
        origin: LayerActionOrigin.layerManager,
      }),
    );

    expect(result).toEqual(initialMockState);
  });

  describe('setAvailableBaseLayers', () => {
    it('should set avaialable baseLayers for passed mapId', () => {
      const mapId1 = 'mapid1';
      const baseLayers = [
        {
          name: 'test',
          id: 'test-1',
          layerType: LayerType.baseLayer,
          mapId: mapId1,
        },
        {
          name: 'test2',
          id: 'test-2',
          layerType: LayerType.baseLayer,
          mapId: mapId1,
        },
        {
          name: 'test3',
          id: 'test-2',
          layerType: LayerType.overLayer,
          mapId: mapId1,
        },
      ];

      const result = layerReducer(
        undefined,
        layerActions.setAvailableBaseLayers({
          layers: baseLayers,
          mapId: mapId1,
        }),
      );

      expect(result.availableBaseLayers.allIds).toHaveLength(2);
      expect(result.availableBaseLayers.allIds).toEqual([
        baseLayers[0].id,
        baseLayers[1].id,
      ]);
      expect(result.availableBaseLayers.byId[baseLayers[0].id]).toEqual(
        createLayer(baseLayers[0]),
      );
    });

    it('should overwrite existing baseLayers for passed mapId but leave other maps unchanged', () => {
      const mapId1 = 'mapid1';
      const mapId2 = 'map2';
      const initialState = {
        byId: {},
        allIds: [],
        availableBaseLayers: {
          byId: {
            mapid2_1: {
              name: 'WorldMap_Light_Grey_Canvas',
              type: 'twms',
              mapId: mapId2,
              dimensions: [],
              id: 'mapid2_1',
              opacity: 1,
              enabled: true,
              layerType: LayerType.baseLayer,
            },
            mapid1_1: {
              name: 'WorldMap_Light_Grey_Canvas',
              type: 'twms',
              mapId: mapId1,
              dimensions: [],
              id: 'mapid1_1',
              opacity: 1,
              enabled: true,
              layerType: LayerType.baseLayer,
            },
          },
          allIds: ['mapid2_1', 'mapid1_1'],
        },
      };

      const result = layerReducer(initialState, {} as AnyAction);
      expect(result.availableBaseLayers.byId['mapid2_1']).toEqual(
        initialState.availableBaseLayers.byId['mapid2_1'],
      );
      expect(result.availableBaseLayers.byId['mapid1_1']).toEqual(
        initialState.availableBaseLayers.byId['mapid1_1'],
      );
      expect(result.availableBaseLayers.allIds).toHaveLength(2);

      const baseLayers = [
        {
          name: 'test',
          id: 'mapid1_2',
          layerType: LayerType.baseLayer,
          mapId: mapId1,
        },
        {
          name: 'test2',
          id: 'mapid1_3',
          layerType: LayerType.baseLayer,
          mapId: mapId1,
        },
      ];
      const result2 = layerReducer(
        result,
        layerActions.setAvailableBaseLayers({
          layers: baseLayers,
          mapId: mapId1,
        }),
      );
      // Original for map1 should have been removed
      expect(result2.availableBaseLayers.byId['mapid1_1']).toBeUndefined();
      // 2 new baselayers should be added for map1
      expect(result2.availableBaseLayers.byId['mapid1_2']).toEqual(
        createLayer(baseLayers[0]),
      );
      expect(result2.availableBaseLayers.byId['mapid1_3']).toEqual(
        createLayer(baseLayers[1]),
      );
      // map2 baselayers should remain as tey were
      expect(result.availableBaseLayers.byId['mapid2_1']).toEqual(
        initialState.availableBaseLayers.byId['mapid2_1'],
      );
      expect(result2.availableBaseLayers.allIds).toEqual([
        'mapid2_1',
        baseLayers[0].id,
        baseLayers[1].id,
      ]);
    });
  });

  describe('layerSetDimensions', () => {
    const layerId = 'test-1';
    const dimensions = [{ name: 'test-dim', currentValue: 'initialvalue' }];

    it('should set dimensions', () => {
      const initialMockState = createLayersState(layerId, {});
      const result = layerReducer(
        initialMockState,
        layerActions.layerSetDimensions({
          layerId,
          origin: LayerActionOrigin.layerManager,
          dimensions,
        }),
      );
      expect(result.byId[layerId].dimensions).toEqual(dimensions);
    });

    it('should not set dimensions if no state passed', () => {
      const result = layerReducer(
        undefined,
        layerActions.layerSetDimensions({
          layerId,
          origin: LayerActionOrigin.layerManager,
          dimensions,
        }),
      );
      expect(result.byId[layerId]).toBeFalsy();
    });
  });

  describe('update layer information', () => {
    const layerId = 'test-1';
    const layerStyle = {
      style: 'temperature-wow/shadedcontour',
      layerId,
    };

    const layerDimensions = {
      layerId,
      origin: LayerActionOrigin.layerManager,
      dimensions: [
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: '2023-06-21T03:00:00Z',
          maxValue: '2023-06-23T03:00:00Z',
          minValue: '2023-06-21T03:00:00Z',
          timeInterval: {
            year: 0,
            month: 0,
            day: 0,
            hour: 1,
            minute: 0,
            second: 0,
            isRegularInterval: true,
          },
          validSyncSelection: true,
          values: '2023-06-19T09:00:00Z/2023-06-23T03:00:00Z/PT1H',
          synced: true,
        },
      ],
    };
    it('should update the dimensions correctly', () => {
      const initialMockState = createLayersState(layerId, {});
      const result = layerReducer(
        initialMockState,
        layerActions.onUpdateLayerInformation({
          origin: LayerActionOrigin.layerManager,
          layerStyle,
          layerDimensions,
        }),
      );
      expect(result.byId[layerId].dimensions).toEqual(
        layerDimensions.dimensions,
      );
    });

    it('should set useLatestReferenceTime', () => {
      const initialState = createLayersState('test-1', {});
      const action = layerActions.setUseLatestReferenceTime({
        id: 'test-1',
        useLatestReferenceTime: true,
      });

      const nextState = layerReducer(initialState, action);

      expect(nextState.byId['test-1'].useLatestReferenceTime).toBe(true);
    });
  });

  describe('orderLayerToFront', () => {
    const mockState = createMultipleLayersState(
      [
        {
          id: 'layer-1',
        },
        {
          id: 'layer-2',
        },
        {
          id: 'layer-3',
        },
      ],
      'map-1',
    );

    it('should order layer to front', () => {
      const result = layerReducer(
        mockState,
        layerActions.orderLayerToFront({
          layerId: 'layer-3',
        }),
      );

      expect(result.allIds).toEqual(['layer-3', 'layer-1', 'layer-2']);

      const result2 = layerReducer(
        mockState,
        layerActions.orderLayerToFront({
          layerId: 'layer-2',
        }),
      );

      expect(result2.allIds).toEqual(['layer-2', 'layer-1', 'layer-3']);
    });

    it('should not order if layer can not be found', () => {
      const result = layerReducer(
        mockState,
        layerActions.orderLayerToFront({
          layerId: '',
        }),
      );

      expect(result.allIds).toEqual(['layer-1', 'layer-2', 'layer-3']);

      const result2 = layerReducer(
        mockState,
        layerActions.orderLayerToFront({
          layerId: 'layer-5',
        }),
      );

      expect(result2.allIds).toEqual(['layer-1', 'layer-2', 'layer-3']);
    });
  });

  describe('setSelectedFeatureIndex', () => {
    const layerId1 = 'test-1';
    const layerId2 = 'test-2';

    it('should return initial state if no state is passed', () => {
      const result = layerReducer(
        undefined,
        layerActions.setSelectedFeature({
          layerId: layerId1,
          selectedFeatureIndex: 1,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if layerId passed is invalid', () => {
      const mockState = createMultipleLayersState(
        [{ id: layerId1 }, { id: layerId2 }],
        'map1',
      );

      const result = layerReducer(
        mockState,
        layerActions.setSelectedFeature({
          layerId: 'invalidLayerId',
          selectedFeatureIndex: 1,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should set the selected feature index', () => {
      const mockState = createMultipleLayersState(
        [{ id: layerId1 }, { id: layerId2 }],
        'map1',
      );

      const result = layerReducer(
        mockState,
        layerActions.setSelectedFeature({
          layerId: layerId1,
          selectedFeatureIndex: 1,
        }),
      );

      expect(result.byId[layerId1].selectedFeatureIndex).toEqual(1);
    });
  });

  describe('updateFeature', () => {
    const layerId1 = 'test-1';
    const layerId2 = 'test-2';

    it('should return initial state if no state is passed', () => {
      const result = layerReducer(
        undefined,
        layerActions.updateFeature({
          layerId: layerId1,
          geojson: testGeoJSON,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should update geojson', () => {
      const mockState = createMultipleLayersState(
        [{ id: layerId1 }, { id: layerId2 }],
        'map1',
      );

      const result = layerReducer(
        mockState,
        layerActions.updateFeature({
          layerId: layerId1,
          geojson: testGeoJSON,
        }),
      );

      expect(result.byId[layerId1].geojson).toEqual(testGeoJSON);
    });

    it('should update geojson as single shape', () => {
      const mockState = createMultipleLayersState(
        [{ id: layerId1 }, { id: layerId2 }],
        'map1',
      );

      const result = layerReducer(
        mockState,
        layerActions.updateFeature({
          layerId: layerId1,
          geojson: {
            ...testGeoJSON,
            features: [
              {
                ...testGeoJSON.features[0],
                geometry: {
                  ...testGeoJSON.features[0].geometry,
                  coordinates: [
                    ...(
                      testGeoJSON
                        .features[0] as GeoJSON.Feature<GeoJSON.Polygon>
                    ).geometry.coordinates,
                    ...(
                      testGeoJSON
                        .features[0] as GeoJSON.Feature<GeoJSON.Polygon>
                    ).geometry.coordinates,
                    ...(
                      testGeoJSON
                        .features[0] as GeoJSON.Feature<GeoJSON.Polygon>
                    ).geometry.coordinates,
                  ],
                },
              } as GeoJSON.Feature,
            ],
          },
        }),
      );

      expect(result.byId[layerId1].geojson).toEqual(testGeoJSON);
    });

    it('should update geojson as multiple shapes', () => {
      const mockState = createMultipleLayersState(
        [{ id: layerId1, geojson: emptyGeoJSON }, { id: layerId2 }],
        'map1',
      );

      const updateGeoJSON = {
        ...testGeoJSON,
        features: [
          {
            ...testGeoJSON.features[0],
            geometry: {
              ...testGeoJSON.features[0].geometry,
              coordinates: [
                ...(testGeoJSON.features[0] as GeoJSON.Feature<GeoJSON.Polygon>)
                  .geometry.coordinates,
                ...(testGeoJSON.features[0] as GeoJSON.Feature<GeoJSON.Polygon>)
                  .geometry.coordinates,
                ...(testGeoJSON.features[0] as GeoJSON.Feature<GeoJSON.Polygon>)
                  .geometry.coordinates,
              ],
            },
          } as GeoJSON.Feature,
        ],
      };

      const result = layerReducer(
        mockState,
        layerActions.updateFeature({
          layerId: layerId1,
          geojson: updateGeoJSON,
          shouldAllowMultipleShapes: true,
        }),
      );

      expect(result.byId[layerId1].geojson).toEqual(updateGeoJSON);
      expect(result.byId[layerId1].selectedFeatureIndex).toBeUndefined();
    });

    describe('multi shape polygon', () => {
      const updateGeoJSON = {
        ...testGeoJSON,
        features: [
          {
            ...testGeoJSON.features[0],
            geometry: {
              ...testGeoJSON.features[0].geometry,
              coordinates: [
                ...(testGeoJSON.features[0] as GeoJSON.Feature<GeoJSON.Polygon>)
                  .geometry.coordinates,
                ...(testGeoJSON.features[0] as GeoJSON.Feature<GeoJSON.Polygon>)
                  .geometry.coordinates,
                ...(testGeoJSON.features[0] as GeoJSON.Feature<GeoJSON.Polygon>)
                  .geometry.coordinates,
              ],
            },
          } as GeoJSON.Feature,
        ],
      };
      it('should update geojson update selectedFeatureIndex for valid reason', () => {
        const mockState = createMultipleLayersState(
          [{ id: layerId1, geojson: updateGeoJSON }, { id: layerId2 }],
          'map1',
        );

        const polygonResult = layerReducer(
          mockState,
          layerActions.updateFeature({
            layerId: layerId1,
            geojson: updateGeoJSON,
            shouldAllowMultipleShapes: true,
            reason: NEW_FEATURE_CREATED,
          }),
        );

        expect(polygonResult.byId[layerId1].geojson).toEqual(updateGeoJSON);
        expect(polygonResult.byId[layerId1].selectedFeatureIndex).toEqual(
          updateGeoJSON.features.length - 1,
        );
      });

      it('should update geojson and not update selectedFeatureIndex for invalid reason', () => {
        const mockState = createMultipleLayersState(
          [{ id: layerId1, geojson: updateGeoJSON }, { id: layerId2 }],
          'map1',
        );

        const polygonResult = layerReducer(
          mockState,
          layerActions.updateFeature({
            layerId: layerId1,
            geojson: updateGeoJSON,
            shouldAllowMultipleShapes: true,
          }),
        );

        expect(polygonResult.byId[layerId1].geojson).toEqual(updateGeoJSON);
        expect(
          polygonResult.byId[layerId1].selectedFeatureIndex,
        ).toBeUndefined();
      });
    });

    describe('multi shape linestring', () => {
      const updateGeoJSON = {
        ...testGeoJSON,
        features: [
          {
            type: 'Feature',
            properties: {
              fill: '#ff7800',
              'fill-opacity': 0.2,
              stroke: '#ff7800',
              'stroke-width': 2,
              'stroke-opacity': 1,
              selectionType: 'linestring',
            },
            geometry: {
              type: 'LineString',
              coordinates: [
                [6.9935356639505635, 56.79342638523173],
                [2.2245842573976518, 54.112766322589636],
                [4.440860429737595, 52.912641301073315],
                [8.25997919100196, 53.98497349327805],
                [6.914382943509851, 55.548708956789355],
              ],
            },
          },
          {
            type: 'Feature',
            properties: {
              fill: '#ff7800',
              'stroke-opacity': 1,
              selectionType: 'linestring',
            },
            geometry: {
              type: 'LineString',
              coordinates: [
                [9.427481817502464, 56.80426199884583],
                [9.427481817502464, 56.80426199884583],
                [9.526422718053354, 53.83343849498333],
                [9.526422718053354, 53.83343849498333],
              ],
            },
          },
        ],
      } as GeoJSON.FeatureCollection<LineString>;
      it('should update geojson update selectedFeatureIndex for valid reason', () => {
        const mockState = createMultipleLayersState(
          [{ id: layerId1, geojson: updateGeoJSON }, { id: layerId2 }],
          'map1',
        );

        const polygonResult = layerReducer(
          mockState,
          layerActions.updateFeature({
            layerId: layerId1,
            geojson: updateGeoJSON,
            shouldAllowMultipleShapes: true,
            reason: NEW_LINESTRING_CREATED,
          }),
        );

        expect(polygonResult.byId[layerId1].geojson).toEqual(updateGeoJSON);
        expect(polygonResult.byId[layerId1].selectedFeatureIndex).toEqual(
          updateGeoJSON.features.length - 1,
        );
      });

      it('should update geojson and not update selectedFeatureIndex for invalid reason', () => {
        const mockState = createMultipleLayersState(
          [{ id: layerId1, geojson: updateGeoJSON }, { id: layerId2 }],
          'map1',
        );

        const polygonResult = layerReducer(
          mockState,
          layerActions.updateFeature({
            layerId: layerId1,
            geojson: updateGeoJSON,
            shouldAllowMultipleShapes: true,
          }),
        );

        expect(polygonResult.byId[layerId1].geojson).toEqual(updateGeoJSON);
        expect(
          polygonResult.byId[layerId1].selectedFeatureIndex,
        ).toBeUndefined();
      });
    });

    describe('multi shape point', () => {
      const updateGeoJSON = {
        ...testGeoJSON,
        features: [
          {
            type: 'Feature',
            properties: {
              fill: '#ff7800',
              'fill-opacity': 0.2,
              stroke: '#ff7800',
              'stroke-width': 2,
              'stroke-opacity': 1,
              selectionType: 'point',
            },
            geometry: {
              type: 'Point',
              coordinates: [5.39069307502614, 54.9963525972935],
            },
          },
          {
            type: 'Feature',
            properties: {
              fill: '#ff7800',
              'fill-opacity': 0.2,
              selectionType: 'point',
            },
            geometry: {
              type: 'Point',
              coordinates: [6.7956538628487815, 55.87200448046568],
            },
          },
        ],
      } as GeoJSON.FeatureCollection<Point>;
      it('should update geojson update selectedFeatureIndex for valid reason', () => {
        const mockState = createMultipleLayersState(
          [{ id: layerId1, geojson: updateGeoJSON }, { id: layerId2 }],
          'map1',
        );

        const polygonResult = layerReducer(
          mockState,
          layerActions.updateFeature({
            layerId: layerId1,
            geojson: updateGeoJSON,
            shouldAllowMultipleShapes: true,
            reason: NEW_POINT_CREATED,
          }),
        );

        expect(polygonResult.byId[layerId1].geojson).toEqual(updateGeoJSON);
        expect(polygonResult.byId[layerId1].selectedFeatureIndex).toEqual(
          updateGeoJSON.features.length - 1,
        );
      });

      it('should update geojson and not update selectedFeatureIndex for invalid reason', () => {
        const mockState = createMultipleLayersState(
          [{ id: layerId1, geojson: updateGeoJSON }, { id: layerId2 }],
          'map1',
        );

        const polygonResult = layerReducer(
          mockState,
          layerActions.updateFeature({
            layerId: layerId1,
            geojson: updateGeoJSON,
            shouldAllowMultipleShapes: true,
          }),
        );

        expect(polygonResult.byId[layerId1].geojson).toEqual(updateGeoJSON);
        expect(
          polygonResult.byId[layerId1].selectedFeatureIndex,
        ).toBeUndefined();
      });

      it('should add selectionType to geojson properties if given', () => {
        const mockState = createMultipleLayersState(
          [{ id: layerId1, geojson: updateGeoJSON }, { id: layerId2 }],
          'map1',
        );

        const testGeoJSON: GeoJSON.FeatureCollection = {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                fill: '#ff7800',
                'fill-opacity': 0.2,
              },
              geometry: {
                type: 'Point',
                coordinates: [6.7956538628487815, 55.87200448046568],
              },
            },
          ],
        };
        const testSelectionType = 'my-new-tool';

        const polygonResult = layerReducer(
          mockState,
          layerActions.updateFeature({
            layerId: layerId1,
            geojson: testGeoJSON,
            shouldAllowMultipleShapes: true,
            reason: NEW_POINT_CREATED,
            selectionType: testSelectionType,
          }),
        );

        expect(
          polygonResult.byId[layerId1].geojson?.features[0].properties,
        ).toEqual(testGeoJSON.features[0].properties);
        expect(
          polygonResult.byId[layerId1].geojson?.features[1].properties,
        ).toEqual({
          ...testGeoJSON.features[0].properties,
          selectionType: testSelectionType,
        });
      });
    });

    describe('intersection', () => {
      const intersectionBoundsFeature = {
        type: 'FeatureCollection',
        features: [intersectionFeatureNL],
      } as GeoJSON.FeatureCollection;
      it('should create an intersection', () => {
        const geoJSONIntersectionLayerId = 'intersection-layer';
        const geoJSONIntersectionBoundsLayerId = 'bounds-layer';
        const mockState = createMultipleLayersState(
          [
            { id: layerId1, geojson: testGeoJSON },
            { id: geoJSONIntersectionLayerId, geojson: emptyGeoJSON },
            {
              id: geoJSONIntersectionBoundsLayerId,
              geojson: intersectionBoundsFeature,
            },
          ],
          'map1',
        );

        const result = layerReducer(
          mockState,
          layerActions.updateFeature({
            layerId: layerId1,
            geojson: testGeoJSON,
            geoJSONIntersectionBoundsLayerId,
            geoJSONIntersectionLayerId,
          }),
        );

        expect(result.byId[layerId1].geojson).toEqual(testGeoJSON);
        expect(result.byId[geoJSONIntersectionLayerId].geojson).toEqual(
          createInterSections(
            getGeoJson(testGeoJSON, false),
            intersectionBoundsFeature,
            testGeoJSON.features[0].properties,
          ),
        );
        expect(result.byId[geoJSONIntersectionBoundsLayerId].geojson).toEqual(
          intersectionBoundsFeature,
        );
      });

      it('should do nothing if geoJSONIntersectionLayerId is missing', () => {
        const geoJSONIntersectionLayerId = 'intersection-layer';
        const geoJSONIntersectionBoundsLayerId = 'bounds-layer';
        const mockState = createMultipleLayersState(
          [
            { id: layerId1, geojson: testGeoJSON },
            {
              id: geoJSONIntersectionBoundsLayerId,
              geojson: intersectionBoundsFeature,
            },
          ],
          'map1',
        );

        const result = layerReducer(
          mockState,
          layerActions.updateFeature({
            layerId: layerId1,
            geojson: testGeoJSON,
            geoJSONIntersectionBoundsLayerId,
            geoJSONIntersectionLayerId,
          }),
        );

        expect(result.byId[layerId1].geojson).toEqual(testGeoJSON);
        expect(result.byId[geoJSONIntersectionLayerId]).toBeUndefined();
        expect(result.byId[geoJSONIntersectionBoundsLayerId].geojson).toEqual(
          intersectionBoundsFeature,
        );
      });

      it('should do nothing if geoJSONIntersectionBoundsLayerId is missing', () => {
        const geoJSONIntersectionLayerId = 'intersection-layer';
        const geoJSONIntersectionBoundsLayerId = 'bounds-layer';
        const mockState = createMultipleLayersState(
          [
            { id: layerId1, geojson: testGeoJSON },
            { id: geoJSONIntersectionLayerId, geojson: emptyGeoJSON },
          ],
          'map1',
        );

        const result = layerReducer(
          mockState,
          layerActions.updateFeature({
            layerId: layerId1,
            geojson: testGeoJSON,
            geoJSONIntersectionBoundsLayerId,
            geoJSONIntersectionLayerId,
          }),
        );

        expect(result.byId[layerId1].geojson).toEqual(testGeoJSON);
        expect(result.byId[geoJSONIntersectionLayerId].geojson).toEqual(
          emptyGeoJSON,
        );
        expect(result.byId[geoJSONIntersectionBoundsLayerId]).toBeUndefined();
      });
    });
  });

  describe('updateFeatureProperties', () => {
    const layerId1 = 'test-1';
    const layerId2 = 'test-2';
    const testProperties = { fill: '#FF00FF', 'stroke-opacity': 0.4 };

    it('should return initial state if no state is passed', () => {
      const result = layerReducer(
        undefined,
        layerActions.updateFeatureProperties({
          layerId: layerId1,
          properties: testProperties,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should update geojson properties', () => {
      const mockState = createMultipleLayersState(
        [{ id: layerId1 }, { id: layerId2 }],
        'map1',
      );

      mockState.byId[layerId1].geojson = testGeoJSON;

      const result = layerReducer(
        mockState,
        layerActions.updateFeatureProperties({
          layerId: layerId1,
          properties: testProperties,
        }),
      );

      expect(result.byId[layerId1].geojson).toEqual({
        ...testGeoJSON,
        features: [
          {
            ...testGeoJSON.features[0],
            properties: {
              ...testGeoJSON.features[0].properties,
              ...testProperties,
            },
          },
        ],
      });
    });

    it('should not update geojson if no geojson is present', () => {
      const mockState = createMultipleLayersState(
        [{ id: layerId1 }, { id: layerId2 }],
        'map1',
      );

      const result = layerReducer(
        mockState,
        layerActions.updateFeatureProperties({
          layerId: layerId1,
          properties: testProperties,
        }),
      );

      expect(result.byId[layerId1].geojson).toBeUndefined();
    });
  });

  describe('toggleFeatureMode', () => {
    const layerId1 = 'test-1';
    const layerId2 = 'test-2';

    it('should return initial state if no state is passed', () => {
      const result = layerReducer(
        undefined,
        layerActions.toggleFeatureMode({
          layerId: layerId1,
          isInEditMode: false,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should toggle isInEditMode', () => {
      const mockState = createMultipleLayersState(
        [{ id: layerId1 }, { id: layerId2 }],
        'map1',
      );

      const result = layerReducer(
        mockState,
        layerActions.toggleFeatureMode({
          layerId: layerId1,
          isInEditMode: true,
        }),
      );

      expect(result.byId[layerId1].isInEditMode).toBeTruthy();
      expect(result.byId[layerId2].isInEditMode).toBeFalsy();
    });

    it('should toggle drawMode', () => {
      const mockState = createMultipleLayersState(
        [{ id: layerId1 }, { id: layerId2 }],
        'map1',
      );

      const result = layerReducer(
        mockState,
        layerActions.toggleFeatureMode({
          layerId: layerId1,
          drawMode: 'Point',
        }),
      );

      expect(result.byId[layerId1].drawMode).toEqual('Point');
      expect(result.byId[layerId2].drawMode).toBeUndefined();
    });
  });

  describe('exitFeatureDrawMode', () => {
    const layerId1 = 'test-1';
    const layerId2 = 'test-2';

    it('should return initial state if no state is passed', () => {
      const result = layerReducer(
        undefined,
        layerActions.exitFeatureDrawMode({
          layerId: layerId1,
          reason: 'escaped',
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should exit drawmode', () => {
      const mockState = createMultipleLayersState(
        [{ id: layerId1 }, { id: layerId2 }],
        'map1',
      );

      mockState.byId[layerId1].isInEditMode = true;
      mockState.byId[layerId1].drawMode = 'Point';

      const result = layerReducer(
        mockState,
        layerActions.exitFeatureDrawMode({
          layerId: layerId1,
          reason: 'escaped',
        }),
      );

      expect(result.byId[layerId1].isInEditMode).toBeFalsy();
      expect(result.byId[layerId1].drawMode).toEqual('');
      expect(result.byId[layerId2].isInEditMode).toBeFalsy();
      expect(result.byId[layerId2].drawMode).toBeUndefined();
    });

    it('should exit drawmode on double click', () => {
      const mockState = createMultipleLayersState(
        [{ id: layerId1 }, { id: layerId2 }],
        'map1',
      );

      mockState.byId[layerId1].isInEditMode = true;
      mockState.byId[layerId1].drawMode = 'Point';

      const result = layerReducer(
        mockState,
        layerActions.exitFeatureDrawMode({
          layerId: layerId1,
          reason: 'doubleClicked',
        }),
      );

      expect(result.byId[layerId1].isInEditMode).toBeFalsy();
      expect(result.byId[layerId1].drawMode).toEqual('');
      expect(result.byId[layerId2].isInEditMode).toBeFalsy();
      expect(result.byId[layerId2].drawMode).toBeUndefined();
    });

    it('should exit drawmode and remove last empty shape on multishape', () => {
      const mockState = createMultipleLayersState(
        [{ id: layerId1 }, { id: layerId2 }],
        'map1',
      );
      mockState.byId[layerId1].selectedFeatureIndex = 1;
      mockState.byId[layerId1].geojson = {
        ...testGeoJSON,
        features: [testGeoJSON.features[0], featurePoint],
      };

      const result = layerReducer(
        mockState,
        layerActions.exitFeatureDrawMode({
          layerId: layerId1,
          reason: 'escaped',
          shouldAllowMultipleShapes: true,
        }),
      );

      expect(result.byId[layerId1].geojson).toEqual(testGeoJSON);
      expect(result.byId[layerId1].selectedFeatureIndex).toEqual(0);
    });

    it('should exit drawmode and not remove last shape with features on multishape', () => {
      const mockState = createMultipleLayersState(
        [{ id: layerId1 }, { id: layerId2 }],
        'map1',
      );
      mockState.byId[layerId1].selectedFeatureIndex = 1;
      const newGeoJSON = {
        ...testGeoJSON,
        features: [testGeoJSON.features[0], testGeoJSON.features[0]],
      };
      mockState.byId[layerId1].geojson = newGeoJSON;

      const result = layerReducer(
        mockState,
        layerActions.exitFeatureDrawMode({
          layerId: layerId1,
          reason: 'escaped',
          shouldAllowMultipleShapes: true,
        }),
      );

      expect(result.byId[layerId1].geojson).toEqual(newGeoJSON);
      expect(result.byId[layerId1].selectedFeatureIndex).toEqual(1);
    });
  });

  describe('onUpdateLayerInformation', () => {
    it('should set dimension and check if currentValue is preserved', () => {
      const layerId = 'test-1';

      const timeValueToKeep = '12345';
      // Create a layer with a time dimension already set
      const initialMockState = createLayersState(layerId, {
        dimensions: [{ name: 'time', currentValue: timeValueToKeep }],
      });

      const resultA = layerReducer(initialMockState, {} as AnyAction);

      expect(resultA).toEqual({
        byId: {
          'test-1': {
            dimensions: [{ currentValue: timeValueToKeep, name: 'time' }],
            id: 'test-1',
            opacity: 1,
            acceptanceTimeInMinutes: 60,
            enabled: true,
            layerType: 'mapLayer',
            status: 'default',
            useLatestReferenceTime: true,
          },
        },
        allIds: ['test-1'],
        availableBaseLayers: { allIds: [], byId: {} },
      });

      // Update the layer dimension, it should keep the original currentValue for time
      const onUpdateLayerInformationActionPayload: UpdateLayerInfoPayload = {
        layerDimensions: {
          dimensions: [
            {
              currentValue: '9000',
              maxValue: '9000',
              minValue: '1000',
              name: 'elevation',
              synced: false,
              timeInterval: undefined,
              units: 'meters',
              values: '1000,5000,9000',
            },
            {
              currentValue: '2020-03-13T14:40:00Z',
              maxValue: '2020-03-13T14:40:00Z',
              minValue: '2020-03-13T14:40:00Z',
              name: 'time',
              synced: false,
              timeInterval: undefined,
              units: 'ISO8601',
              values: undefined,
            },
          ],
          layerId,
          origin: 'ReactMapViewParseLayer',
        },
        layerStyle: {
          layerId,
          style: '',
        },
        mapDimensions: {
          dimensions: [
            {
              currentValue: '2020-03-13T14:40:00Z',
              name: 'time',
            },
          ],
          mapId: 'map1',
          origin: 'ReactMapViewParseLayer',
        },
        origin: 'ReactMapViewParseLayer',
      };

      const resultB = layerReducer(
        resultA,
        layerActions.onUpdateLayerInformation(
          onUpdateLayerInformationActionPayload,
        ),
      );
      expect(resultB).toEqual({
        byId: {
          'test-1': {
            dimensions: [
              {
                name: 'time',
                currentValue: timeValueToKeep,
                minValue: '2020-03-13T14:40:00Z',
                maxValue: '2020-03-13T14:40:00Z',
                units: 'ISO8601',
                values: undefined,
              },
              {
                name: 'elevation',
                units: 'meters',
                currentValue: '9000',
                minValue: '1000',
                maxValue: '9000',
                synced: false,
                timeInterval: undefined,
                validSyncSelection: undefined,
                values: '1000,5000,9000',
              },
            ],
            id: 'test-1',
            opacity: 1,
            enabled: true,
            layerType: 'mapLayer',
            status: 'default',
            style: '',
            acceptanceTimeInMinutes: 60,
            useLatestReferenceTime: true,
          },
        },
        allIds: ['test-1'],
        availableBaseLayers: { allIds: [], byId: {} },
      });
    });
  });

  describe('showLayerInfo', () => {
    it('should show layer info', () => {
      const serviceUrl = 'test.url';
      const layerName = 'test name';
      const mapId = 'test-map1';
      const result = layerReducer(
        undefined,
        layerActions.showLayerInfo({
          serviceUrl,
          layerName,
          mapId,
        }),
      );

      expect(result.activeLayerInfo?.layerName).toEqual(layerName);
      expect(result.activeLayerInfo?.serviceUrl).toEqual(serviceUrl);

      const serviceUrl2 = 'test.url';
      const layerName2 = 'test name';
      const result2 = layerReducer(
        result,
        layerActions.showLayerInfo({
          serviceUrl: serviceUrl2,
          layerName: layerName2,
          mapId,
        }),
      );

      expect(result2.activeLayerInfo?.layerName).toEqual(layerName2);
      expect(result2.activeLayerInfo?.serviceUrl).toEqual(serviceUrl2);
    });

    it('should hide layer info', () => {
      const serviceUrl = 'test.url';
      const layerName = 'test name';
      const initState = {
        activeLayerInfo: {
          serviceUrl,
          layerName,
        },
        byId: {},
        allIds: [],
        availableBaseLayers: { byId: {}, allIds: [] },
      };
      const result = layerReducer(initState, layerActions.hideLayerInfo());

      expect(result.activeLayerInfo).toBeUndefined();
    });
  });
});
