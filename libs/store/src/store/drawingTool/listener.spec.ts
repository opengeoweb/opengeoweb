/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { configureStore } from '@reduxjs/toolkit';
import { emptyGeoJSON, MapDrawToolOptions } from '@opengeoweb/webmap-react';
import { FeatureCollection, Polygon } from 'geojson';
import { layerReducer } from '../map';
import { drawingToolListener } from './listener';
import { drawingToolReducer, drawingToolActions } from './reducer';

describe('store/drawingtool/listener', () => {
  const registerDrawtoolPayload = {
    mapId: 'test-map-id',
    drawToolId: 'drawtool-1',
    geoJSONLayerId: 'draw-layer',
    geoJSONIntersectionLayerId: 'end-intersection-layer',
    geoJSONIntersectionBoundsLayerId: 'static-intersection-layer',
    defaultGeoJSONIntersectionBounds: {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [5, 55],
                [4.331914, 55.332644],
                [3.368817, 55.764314],
                [2.761908, 54.379261],
                [3.15576, 52.913554],
                [2.000002, 51.500002],
                [3.370001, 51.369722],
                [3.370527, 51.36867],
                [3.362223, 51.320002],
                [3.36389, 51.313608],
                [3.373613, 51.309999],
                [3.952501, 51.214441],
                [4.397501, 51.452776],
                [5.078611, 51.391665],
                [5.848333, 51.139444],
                [5.651667, 50.824717],
                [6.011797, 50.757273],
                [5.934168, 51.036386],
                [6.222223, 51.361666],
                [5.94639, 51.811663],
                [6.405001, 51.830828],
                [7.053095, 52.237764],
                [7.031389, 52.268885],
                [7.063612, 52.346109],
                [7.065557, 52.385828],
                [7.133055, 52.888887],
                [7.14218, 52.898244],
                [7.191667, 53.3],
                [6.5, 53.666667],
                [6.500002, 55.000002],
                [5, 55],
              ],
            ],
          },
          properties: {
            stroke: '#000000',
            'stroke-width': 1.5,
            'stroke-opacity': 1,
            fill: '#0075a9',
            'fill-opacity': 0,
          },
        },
      ],
    },
    defaultDrawModes: [
      {
        drawModeId: 'drawtools-polygon',
        value: 'POLYGON',
        title: 'Polygon',
        shape: {
          type: 'Feature',
          properties: {
            fill: '#6e1e91',
            'fill-opacity': 0.25,
            stroke: '#6e1e91',
            'stroke-width': 1.5,
            'stroke-opacity': 1,
          },
          geometry: {
            type: 'Polygon',
            coordinates: [[]],
          },
        },
        isSelectable: true,
        selectionType: 'poly',
      },
      {
        drawModeId: 'drawtools-box',
        value: 'BOX',
        title: 'Box',
        shape: {
          type: 'Feature',
          properties: {
            fill: '#6e1e91',
            'fill-opacity': 0.25,
            stroke: '#6e1e91',
            'stroke-width': 1.5,
            'stroke-opacity': 1,
          },
          geometry: {
            type: 'Polygon',
            coordinates: [[]],
          },
        },
        isSelectable: true,
        selectionType: 'box',
      },
      {
        drawModeId: 'fir-tool-NL',
        value: 'POLYGON',
        title: 'Custom FIR NL polygon',
        shape: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [5, 55],
                    [4.331914, 55.332644],
                    [3.368817, 55.764314],
                    [2.761908, 54.379261],
                    [3.15576, 52.913554],
                    [2.000002, 51.500002],
                    [3.370001, 51.369722],
                    [3.370527, 51.36867],
                    [3.362223, 51.320002],
                    [3.36389, 51.313608],
                    [3.373613, 51.309999],
                    [3.952501, 51.214441],
                    [4.397501, 51.452776],
                    [5.078611, 51.391665],
                    [5.848333, 51.139444],
                    [5.651667, 50.824717],
                    [6.011797, 50.757273],
                    [5.934168, 51.036386],
                    [6.222223, 51.361666],
                    [5.94639, 51.811663],
                    [6.405001, 51.830828],
                    [7.053095, 52.237764],
                    [7.031389, 52.268885],
                    [7.063612, 52.346109],
                    [7.065557, 52.385828],
                    [7.133055, 52.888887],
                    [7.14218, 52.898244],
                    [7.191667, 53.3],
                    [6.5, 53.666667],
                    [6.500002, 55.000002],
                    [5, 55],
                  ],
                ],
              },
              properties: {
                stroke: '#6e1e91',
                'stroke-width': 1.5,
                'stroke-opacity': 1,
                fill: '#6e1e91',
                'fill-opacity': 0.25,
              },
            },
          ],
        },
        isSelectable: false,
        selectionType: 'fir',
      },
      {
        drawModeId: 'drawtools-delete',
        value: 'DELETE',
        title: 'Delete',
        shape: {
          type: 'FeatureCollection',
          features: [],
        },
        isSelectable: false,
        selectionType: 'delete',
      },
    ],
    defaultGeoJSONIntersectionProperties: {
      stroke: '#6e1e91',
      'stroke-width': 1.5,
      'stroke-opacity': 1,
      fill: '#6e1e91',
      'fill-opacity': 0.25,
    },
    defaultGeoJSON: {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            stroke: '#6e1e91',
            'stroke-width': 1.5,
            'stroke-opacity': 1,
            fill: '#6e1e91',
            'fill-opacity': 0.25,
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [2.3408785046353606, 52.57033268070152],
                [8.870833501723242, 51.74975701659361],
                [-0.088053542798945, 50.72496422996941],
                [2.3408785046353606, 52.57033268070152],
              ],
            ],
          },
        },
      ],
    },
    defaultGeoJSONIntersection: {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            stroke: '#6e1e91',
            'stroke-width': 1.5,
            'stroke-opacity': 1,
            fill: '#6e1e91',
            'fill-opacity': 0.25,
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [2.000002, 51.500002],
                [3.370001, 51.369722],
                [3.362223, 51.320002],
                [3.36389, 51.313608],
                [3.373613, 51.309999],
                [3.952501, 51.214441],
                [4.397501, 51.452776],
                [5.078611, 51.391665],
                [5.249839243115953, 51.335557264081125],
                [6.173465572431757, 51.44120940537001],
                [5.94639, 51.811663],
                [6.405001, 51.830828],
                [6.708618091513499, 52.02146813052448],
                [2.8253551673020594, 52.50945174669971],
                [2.000002, 51.500002],
              ],
            ],
          },
        },
      ],
    },
  } as MapDrawToolOptions & {
    drawToolId: string;
    mapId?: string;
  };
  it('should register drawtool', () => {
    const store = configureStore({
      reducer: {
        layers: layerReducer,
        drawingtools: drawingToolReducer,
      },
      middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(drawingToolListener.middleware),
    });

    expect(store.getState().layers.allIds).toHaveLength(0);
    expect(store.getState().drawingtools.ids).toHaveLength(0);

    store.dispatch(
      drawingToolActions.registerDrawTool(registerDrawtoolPayload),
    );

    // geoJSON layer
    expect(
      store
        .getState()
        .layers.allIds.includes(registerDrawtoolPayload.geoJSONLayerId!),
    ).toBeTruthy();

    const geoJSONLayer =
      store.getState().layers.byId[registerDrawtoolPayload.geoJSONLayerId!];
    expect(geoJSONLayer.geojson).toEqual(
      registerDrawtoolPayload.defaultGeoJSON,
    );
    expect(geoJSONLayer.mapId).toEqual(registerDrawtoolPayload.mapId);
    expect(geoJSONLayer.id).toEqual(registerDrawtoolPayload.geoJSONLayerId);

    // intersection layer
    expect(
      store
        .getState()
        .layers.allIds.includes(
          registerDrawtoolPayload.geoJSONIntersectionLayerId!,
        ),
    ).toBeTruthy();

    const intersectionLayer =
      store.getState().layers.byId[
        registerDrawtoolPayload.geoJSONIntersectionLayerId!
      ];
    expect(intersectionLayer.geojson).toEqual(
      registerDrawtoolPayload.defaultGeoJSONIntersection,
    );
    expect(intersectionLayer.mapId).toEqual(registerDrawtoolPayload.mapId);
    expect(intersectionLayer.id).toEqual(
      registerDrawtoolPayload.geoJSONIntersectionLayerId,
    );
    expect(intersectionLayer.defaultGeoJSONProperties).toEqual(
      registerDrawtoolPayload.defaultGeoJSONIntersectionProperties,
    );

    // intersection bounds layer
    expect(
      store
        .getState()
        .layers.allIds.includes(
          registerDrawtoolPayload.geoJSONIntersectionLayerId!,
        ),
    ).toBeTruthy();

    const boundsLayer =
      store.getState().layers.byId[
        registerDrawtoolPayload.geoJSONIntersectionBoundsLayerId!
      ];

    expect(boundsLayer.geojson).toEqual(
      registerDrawtoolPayload.defaultGeoJSONIntersectionBounds,
    );
    expect(boundsLayer.mapId).toEqual(registerDrawtoolPayload.mapId);
    expect(boundsLayer.id).toEqual(
      registerDrawtoolPayload.geoJSONIntersectionBoundsLayerId!,
    );
  });

  it('should change drawtool', () => {
    const store = configureStore({
      reducer: {
        layers: layerReducer,
        drawingtools: drawingToolReducer,
      },
      middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(drawingToolListener.middleware),
    });

    store.dispatch(
      drawingToolActions.registerDrawTool(registerDrawtoolPayload),
    );

    expect(
      store.getState().drawingtools.entities[registerDrawtoolPayload.drawToolId]
        ?.activeDrawModeId,
    ).toEqual('');
    // change draw tool
    store.dispatch(
      drawingToolActions.changeDrawToolMode({
        drawToolId: registerDrawtoolPayload.drawToolId,
        drawModeId: registerDrawtoolPayload.defaultDrawModes![0].drawModeId,
      }),
    );

    expect(
      store.getState().drawingtools.entities[
        registerDrawtoolPayload.drawToolId
      ]!.activeDrawModeId,
    ).toEqual(registerDrawtoolPayload.defaultDrawModes![0].drawModeId);

    const layerReducerResult =
      store.getState().layers.byId[registerDrawtoolPayload.geoJSONLayerId!];

    expect(layerReducerResult.geojson).toEqual(
      registerDrawtoolPayload.defaultGeoJSON,
    );

    expect(layerReducerResult.isInEditMode).toEqual(true);

    // select same tool again
    store.dispatch(
      drawingToolActions.changeDrawToolMode({
        drawToolId: registerDrawtoolPayload.drawToolId,
        drawModeId: registerDrawtoolPayload.defaultDrawModes![0].drawModeId,
      }),
    );

    expect(
      store.getState().layers.byId[registerDrawtoolPayload.geoJSONLayerId!]
        .isInEditMode,
    ).toEqual(false);

    // delete shape
    store.dispatch(
      drawingToolActions.changeDrawToolMode({
        drawToolId: registerDrawtoolPayload.drawToolId,
        drawModeId: registerDrawtoolPayload.defaultDrawModes![3].drawModeId,
      }),
    );

    const layerReducerResult2 =
      store.getState().layers.byId[registerDrawtoolPayload.geoJSONLayerId!];

    expect(layerReducerResult2.drawMode).toEqual('');
    expect(layerReducerResult2.geojson).toEqual(emptyGeoJSON);
    expect(layerReducerResult2.isInEditMode).toEqual(false);

    // select non-selectable shape
    store.dispatch(
      drawingToolActions.changeDrawToolMode({
        drawToolId: registerDrawtoolPayload.drawToolId,
        drawModeId: registerDrawtoolPayload.defaultDrawModes![2].drawModeId,
      }),
    );
    const layerReducerResult3 =
      store.getState().layers.byId[registerDrawtoolPayload.geoJSONLayerId!];

    expect(layerReducerResult3.isInEditMode).toEqual(false);

    expect(layerReducerResult3.geojson!.features[0].properties).toEqual({
      ...(
        registerDrawtoolPayload.defaultDrawModes![2]
          .shape as FeatureCollection<Polygon>
      ).features[0].properties,
      selectionType: registerDrawtoolPayload.defaultDrawModes![2].selectionType,
    });
  });

  it('should change intersection', () => {
    const store = configureStore({
      reducer: {
        layers: layerReducer,
        drawingtools: drawingToolReducer,
      },
      middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(drawingToolListener.middleware),
    });

    expect(store.getState().layers.allIds).toHaveLength(0);
    expect(store.getState().drawingtools.ids).toHaveLength(0);

    store.dispatch(
      drawingToolActions.registerDrawTool(registerDrawtoolPayload),
    );
    const testGeoJSON = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            fill: '#ff7800',
            'fill-opacity': 0.2,
            stroke: '#ff7800',
            'stroke-width': 2,
            'stroke-opacity': 1,
            selectionType: 'box',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [9.451665078283622, 53.21804334226515],
                [4.734608638338736, 53.21804334226515],
                [4.734608638338736, 54.57650543915101],
                [9.451665078283622, 54.57650543915101],
                [9.451665078283622, 53.21804334226515],
              ],
            ],
          },
        },
      ],
    } as FeatureCollection<Polygon>;
    store.dispatch(
      drawingToolActions.changeIntersectionBounds({
        drawToolId: registerDrawtoolPayload.drawToolId,
        geoJSON: testGeoJSON,
      }),
    );

    const layerReducerResult = store.getState().layers.byId;
    // geoJSON layer
    expect(
      layerReducerResult[registerDrawtoolPayload.geoJSONLayerId!].id,
    ).toEqual(registerDrawtoolPayload.geoJSONLayerId);
    expect(
      layerReducerResult[registerDrawtoolPayload.geoJSONLayerId!].geojson,
    ).toEqual(emptyGeoJSON);
    // bounds layer
    expect(
      layerReducerResult[
        registerDrawtoolPayload.geoJSONIntersectionBoundsLayerId!
      ].id,
    ).toEqual(registerDrawtoolPayload.geoJSONIntersectionBoundsLayerId);
    expect(
      layerReducerResult[
        registerDrawtoolPayload.geoJSONIntersectionBoundsLayerId!
      ].geojson,
    ).toEqual(testGeoJSON);
    // intersection layer
    expect(
      layerReducerResult[registerDrawtoolPayload.geoJSONIntersectionLayerId!]
        .id,
    ).toEqual(registerDrawtoolPayload.geoJSONIntersectionLayerId);
    expect(
      layerReducerResult[registerDrawtoolPayload.geoJSONIntersectionLayerId!]
        .geojson,
    ).toEqual(emptyGeoJSON);
  });
});
