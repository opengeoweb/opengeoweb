/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { AnyAction, EntityState } from '@reduxjs/toolkit';
import { DRAWMODE, DrawMode } from '@opengeoweb/webmap-react';
import {
  drawingToolReducer,
  drawingToolActions,
  initialState,
} from './reducer';
import type { DrawToolItem, DrawingToolState } from './reducer';
import { testGeoJSON } from './testUtils';
import { layerActions } from '../map/layer';

export const testDefaultPoint: DrawMode = {
  drawModeId: 'drawtools-point',
  value: DRAWMODE.POINT,
  title: 'Point',
  shape: testGeoJSON.features[0],
  isSelectable: true,
  selectionType: 'point',
};
export const testDefaultPolygon: DrawMode = {
  drawModeId: 'drawtools-polygon',
  value: DRAWMODE.POLYGON,
  title: 'Polygon',
  shape: testGeoJSON.features[0],
  isSelectable: true,
  selectionType: 'polygon',
};
export const testDefaultDelete: DrawMode = {
  drawModeId: 'drawtools-delete',
  value: 'DELETE' as const,
  title: 'Delete',
  shape: testGeoJSON.features[0],
  isSelectable: false,
  selectionType: 'delete',
};
// test state
const drawToolId = 'drawtool-1';
const geoJSONLayerId = 'featurelayer-1';
const state: EntityState<DrawToolItem> = {
  entities: {
    [drawToolId]: {
      drawToolId,
      drawModes: [testDefaultPoint, testDefaultPolygon, testDefaultDelete],
      geoJSONLayerId,
      activeDrawModeId: '',
      shouldAllowMultipleShapes: false,
    },
  },
  ids: [drawToolId],
};

describe('store/drawingtool', () => {
  it('should return initial state if no state and action passed in', () => {
    expect(drawingToolReducer(undefined, {} as AnyAction)).toEqual(
      initialState,
    );
  });

  describe('registerDrawTool', () => {
    it('should register a drawtool instance', () => {
      const registerAction1 = {
        drawToolId,
        defaultDrawModes: [],
        mapId: 'map-1',
        geoJSONLayerId,
      };
      const result = drawingToolReducer(
        undefined,
        drawingToolActions.registerDrawTool(registerAction1),
      );
      expect(result.ids).toHaveLength(1);
      expect(result.entities[registerAction1.drawToolId]).toEqual({
        geoJSONLayerId,
        drawToolId: registerAction1.drawToolId,
        drawModes: registerAction1.defaultDrawModes,
        activeDrawModeId: '',
        shouldAllowMultipleShapes: false,
      });

      // register another one with options
      const registerAction2 = {
        drawToolId: 'drawtool-2',
        defaultDrawModes: [testDefaultPoint, testDefaultPolygon],
        mapId: 'map-1',
        shouldAllowMultipleShapes: true,
      };
      const result2 = drawingToolReducer(
        result,
        drawingToolActions.registerDrawTool(registerAction2),
      );
      expect(result2.ids).toHaveLength(2);
      expect(result2.entities[registerAction2.drawToolId]).toEqual({
        geoJSONLayerId: '',
        drawToolId: registerAction2.drawToolId,
        drawModes: registerAction2.defaultDrawModes,
        activeDrawModeId: '',
        shouldAllowMultipleShapes: true,
      });
    });

    it('should register a drawtool instance with intersection layers', () => {
      const registerAction1 = {
        drawToolId,
        defaultDrawModes: [],
        mapId: 'map-1',
        geoJSONLayerId,
        geoJSONIntersectionLayerId: 'test-intersection',
        geoJSONIntersectionBoundsLayerId: 'test-bound-layer',
      };
      const result = drawingToolReducer(
        undefined,
        drawingToolActions.registerDrawTool(registerAction1),
      );
      expect(result.ids).toHaveLength(1);
      expect(result.entities[registerAction1.drawToolId]).toEqual({
        geoJSONLayerId,
        drawToolId: registerAction1.drawToolId,
        drawModes: registerAction1.defaultDrawModes,
        activeDrawModeId: '',
        shouldAllowMultipleShapes: false,
        geoJSONIntersectionLayerId: registerAction1.geoJSONIntersectionLayerId,
        geoJSONIntersectionBoundsLayerId:
          registerAction1.geoJSONIntersectionBoundsLayerId,
      });
    });
  });

  describe('unregisterDrawTool', () => {
    it('should unregister a drawtool instance if exist', () => {
      const unregisterAction1 = {
        drawToolId,
      };
      const result = drawingToolReducer(
        state,
        drawingToolActions.unregisterDrawTool(unregisterAction1),
      );
      expect(result.ids).toHaveLength(0);
      expect(result.entities[unregisterAction1.drawToolId]).toBeUndefined();

      const unregisterAction2 = {
        drawToolId: 'non-exist',
      };
      const result2 = drawingToolReducer(
        state,
        drawingToolActions.unregisterDrawTool(unregisterAction2),
      );
      expect(result2.ids).toHaveLength(1);
      expect(result2.entities[unregisterAction1.drawToolId]).toBeDefined();
    });
  });

  describe('changeDrawTool', () => {
    it('should do nothing if drawtool does not exist', () => {
      expect(
        drawingToolReducer(
          undefined,
          drawingToolActions.changeDrawToolMode({
            drawModeId: 'drawtool-1',
            drawToolId,
          }),
        ),
      ).toEqual(initialState);
    });

    it('should deactive non selectable tool', () => {
      const result = drawingToolReducer(
        state,
        drawingToolActions.changeDrawToolMode({
          drawModeId: testDefaultDelete.drawModeId,
          drawToolId,
        }),
      );

      expect(result.entities[drawToolId]?.activeDrawModeId).toEqual('');
    });

    it('should active drawmode', () => {
      const result = drawingToolReducer(
        state,
        drawingToolActions.changeDrawToolMode({
          drawModeId: testDefaultPolygon.drawModeId,
          drawToolId,
        }),
      );

      expect(result.entities[drawToolId]?.activeDrawModeId).toEqual(
        testDefaultPolygon.drawModeId,
      );
      // change same tool should deactivate
      const result2 = drawingToolReducer(
        result,
        drawingToolActions.changeDrawToolMode({
          drawModeId: testDefaultPolygon.drawModeId,
          drawToolId,
        }),
      );
      expect(result2.entities[drawToolId]?.activeDrawModeId).toEqual('');
    });

    it('should always set activeDrawModeId when not shouldUpdateShape', () => {
      const result = drawingToolReducer(
        state,
        drawingToolActions.changeDrawToolMode({
          drawModeId: testDefaultPolygon.drawModeId,
          drawToolId,
          shouldUpdateShape: false,
        }),
      );

      expect(result.entities[drawToolId]?.activeDrawModeId).toEqual(
        testDefaultPolygon.drawModeId,
      );
      // change same tool should deactivate
      const result2 = drawingToolReducer(
        result,
        drawingToolActions.changeDrawToolMode({
          drawModeId: testDefaultPolygon.drawModeId,
          drawToolId,
          shouldUpdateShape: false,
        }),
      );
      expect(result2.entities[drawToolId]?.activeDrawModeId).toEqual(
        testDefaultPolygon.drawModeId,
      );
    });
  });

  describe('updateGeoJSONLayerId', () => {
    it('should do nothing if drawtool does not exist', () => {
      const geoJSONLayerId = 'geojson-layer-1';
      expect(
        drawingToolReducer(
          undefined,
          drawingToolActions.updateGeoJSONLayerId({
            geoJSONLayerId,
            drawToolId,
          }),
        ),
      ).toEqual(initialState);
    });

    it('should update geoJSONLayerId', () => {
      const result = drawingToolReducer(
        state,
        drawingToolActions.updateGeoJSONLayerId({
          geoJSONLayerId,
          drawToolId,
        }),
      );

      expect(result.entities[drawToolId]?.geoJSONLayerId).toEqual(
        geoJSONLayerId,
      );
    });
  });

  describe('updateDrawToolMode', () => {
    it('should update drawmode with new geoJSON', () => {
      const payload = {
        drawToolId,
        drawModeId: testDefaultPoint.drawModeId,
        shape: testGeoJSON,
      };
      const result = drawingToolReducer(
        state,
        drawingToolActions.updateDrawToolMode(payload),
      );

      const drawModeResult = result.entities[drawToolId]?.drawModes.find(
        (drawMode) => drawMode.drawModeId === testDefaultPoint.drawModeId,
      );

      expect(
        (drawModeResult!.shape as GeoJSON.FeatureCollection).features[0]
          .properties,
      ).toEqual(testGeoJSON.features[0].properties);
      expect(drawModeResult?.title).toEqual(testDefaultPoint.title);
    });

    it('should update drawmode with geoJSON and title', () => {
      const payload = {
        drawToolId,
        drawModeId: testDefaultPoint.drawModeId,
        shape: testGeoJSON,
        title: 'testing',
      };
      const result = drawingToolReducer(
        state,
        drawingToolActions.updateDrawToolMode(payload),
      );

      const drawModeResult = result.entities[drawToolId]?.drawModes.find(
        (drawMode) => drawMode.drawModeId === testDefaultPoint.drawModeId,
      );

      expect(
        (drawModeResult!.shape as GeoJSON.FeatureCollection).features[0]
          .properties,
      ).toEqual(testGeoJSON.features[0].properties);
      expect(drawModeResult?.title).toEqual(payload.title);
    });

    it('should not update when drawmode can not be found', () => {
      const payload = {
        drawToolId: 'non-existing',
        drawModeId: testDefaultPoint.drawModeId,
        shape: testGeoJSON,
        title: 'testing',
      };
      const result = drawingToolReducer(
        state,
        drawingToolActions.updateDrawToolMode(payload),
      );

      expect(result).toEqual(state);
    });
  });

  describe('layerActions.exitFeatureDrawMode', () => {
    it('should do nothing if drawtool does not exist', () => {
      expect(
        drawingToolReducer(
          undefined,
          layerActions.exitFeatureDrawMode({
            layerId: 'non-exist',
            reason: 'escaped',
          }),
        ),
      ).toEqual(initialState);
    });

    it('should reset active draw mode when escaped', () => {
      const result = drawingToolReducer(
        state,
        drawingToolActions.changeDrawToolMode({
          drawModeId: testDefaultPolygon.drawModeId,
          drawToolId,
        }),
      );

      expect(result.entities[drawToolId]?.activeDrawModeId).toEqual(
        testDefaultPolygon.drawModeId,
      );

      expect(
        drawingToolReducer(
          result,
          layerActions.exitFeatureDrawMode({
            layerId: geoJSONLayerId,
            reason: 'escaped',
          }),
        ).entities[drawToolId]?.activeDrawModeId,
      ).toEqual('');
    });

    it('should reset active draw mode on doubleClicked', () => {
      const result = drawingToolReducer(
        state,
        drawingToolActions.changeDrawToolMode({
          drawModeId: testDefaultPolygon.drawModeId,
          drawToolId,
        }),
      );

      expect(result.entities[drawToolId]?.activeDrawModeId).toEqual(
        testDefaultPolygon.drawModeId,
      );

      expect(
        drawingToolReducer(
          result,
          layerActions.exitFeatureDrawMode({
            layerId: geoJSONLayerId,
            reason: 'doubleClicked',
          }),
        ).entities[drawToolId]?.activeDrawModeId,
      ).toEqual('');
    });

    it('should only reset for multiple shapes when reason is "escaped"', () => {
      const stateWithMultipleShapes: DrawingToolState = {
        ...state,
        entities: {
          ...state.entities,
          [drawToolId]: {
            ...state.entities[drawToolId],
            shouldAllowMultipleShapes: true,
            activeDrawModeId: testDefaultPolygon.drawModeId,
          },
        },
      } as DrawingToolState;

      const result = drawingToolReducer(
        stateWithMultipleShapes,
        layerActions.exitFeatureDrawMode({
          layerId: geoJSONLayerId,
          reason: 'doubleClicked',
          shouldAllowMultipleShapes: true,
        }),
      );

      expect(result.entities[drawToolId]?.activeDrawModeId).toEqual(
        testDefaultPolygon.drawModeId,
      );

      expect(
        drawingToolReducer(
          result,
          layerActions.exitFeatureDrawMode({
            layerId: geoJSONLayerId,
            reason: 'escaped',
            shouldAllowMultipleShapes: true,
          }),
        ).entities[drawToolId]?.activeDrawModeId,
      ).toEqual('');
    });
  });

  describe('layerActions.updateFeatureProperties', () => {
    it('should do nothing if drawtool does not exist', () => {
      expect(
        drawingToolReducer(
          undefined,
          layerActions.updateFeatureProperties({
            layerId: 'non-exist',
            properties: {
              fill: '#FF0000',
            },
          }),
        ),
      ).toEqual(initialState);
    });

    it('should update draw modes with new properties', () => {
      const registerAction1 = {
        drawToolId,
        defaultDrawModes: [
          testDefaultPoint,
          testDefaultPolygon,
          testDefaultDelete,
        ],
        mapId: 'map-1',
        geoJSONLayerId,
      };
      const result = drawingToolReducer(
        undefined,
        drawingToolActions.registerDrawTool(registerAction1),
      );
      expect(result.ids).toHaveLength(1);
      expect(result.entities[registerAction1.drawToolId]).toEqual({
        geoJSONLayerId,
        drawToolId: registerAction1.drawToolId,
        drawModes: registerAction1.defaultDrawModes,
        activeDrawModeId: '',
        shouldAllowMultipleShapes: false,
      });

      const testProperties = { fill: '#FF0000' };

      const result2 = drawingToolReducer(
        result,
        layerActions.updateFeatureProperties({
          layerId: geoJSONLayerId,
          properties: testProperties,
        }),
      );

      expect(result2.entities[drawToolId]?.drawModes).toEqual(
        registerAction1.defaultDrawModes.map((tool) => ({
          ...tool,
          shape: {
            ...tool.shape,
            properties: {
              ...(tool.shape as GeoJSON.Feature).properties,
              ...testProperties,
            },
          },
        })),
      );
    });
  });
});
