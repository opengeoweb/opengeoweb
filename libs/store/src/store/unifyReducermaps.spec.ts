/* eslint-disable @typescript-eslint/default-param-last */
/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { unifyReducerMaps } from '.';

describe('unifyReducerMaps', () => {
  it('should unify multiple reducer maps into one with unique keys and values', () => {
    const uniqueReducer1 = (): string => 'reducer1';
    const uniqueReducer2 = (): string => 'reducer2';
    const uniqueReducer3 = (): string => 'reducer3';
    const uniqueReducer4 = (): string => 'reducer4';

    const reducersMap1 = {
      key1: uniqueReducer1,
      key2: uniqueReducer2,
    };

    const reducersMap2 = {
      key2: uniqueReducer2, // Duplicate key with the same reducer
      key3: uniqueReducer3,
    };

    const reducersMap3 = {
      key1: uniqueReducer1, // Duplicate key with the same reducer
      key4: uniqueReducer4,
    };

    const result = unifyReducerMaps([reducersMap1, reducersMap2, reducersMap3]);

    // Check that the keys are correct and unique
    expect(Object.keys(result)).toHaveLength(4);
    expect(Object.keys(result)).toEqual(
      expect.arrayContaining(['key1', 'key2', 'key3', 'key4']),
    );

    // Check that the reducers are the correct unique functions
    expect(result.key1).toBe(uniqueReducer1);
    expect(result.key2).toBe(uniqueReducer2);
    expect(result.key3).toBe(uniqueReducer3);
    expect(result.key4).toBe(uniqueReducer4);
  });

  it('should return an empty object when given an empty array', () => {
    const result = unifyReducerMaps([]);
    expect(result).toEqual({});
  });

  it('should handle a single reducer map correctly', () => {
    const uniqueReducer1 = (): string => 'reducer1';
    const uniqueReducer2 = (): string => 'reducer2';

    const reducersMap = {
      key1: uniqueReducer1,
      key2: uniqueReducer2,
    };

    const result = unifyReducerMaps([reducersMap]);

    // Check that the keys are correct
    expect(Object.keys(result)).toEqual(
      expect.arrayContaining(['key1', 'key2']),
    );

    // Check that the reducers are the correct unique functions
    expect(result.key1).toBe(uniqueReducer1);
    expect(result.key2).toBe(uniqueReducer2);
  });

  it('should throw an error when duplicate keys have different reducers', () => {
    const reducerA = (): string => 'reducerA';
    const reducerB = (): string => 'reducerB';

    const reducersMap1 = { key1: reducerA };
    const reducersMap2 = { key1: reducerB }; // Same key but different reducer

    expect(() => unifyReducerMaps([reducersMap1, reducersMap2])).toThrow(
      'Reducer key key1 is already defined with a different reducer',
    );
  });
});
