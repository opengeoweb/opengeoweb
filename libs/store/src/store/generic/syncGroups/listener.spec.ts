/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { PROJECTION } from '@opengeoweb/shared';
import { syncGroupsActions } from './reducer';
import { syncConstants } from '.';
import { setBboxSync } from '../synchronizationActions/actions';
import { createMockStore } from '../../store';

describe('store/generic/synchronizationGroups/listener', () => {
  it('should add target if not linked', () => {
    const store = createMockStore();
    store.dispatch(
      syncGroupsActions.syncGroupAddGroup({
        groupId: 'Area_1',
        title: 'Zoompane',
        type: syncConstants.SYNCGROUPS_TYPE_SETBBOX,
        origin: 'user',
      }),
    );
    const defaultPayload = {
      sourceId: '',
      bbox: {
        left: -10,
        bottom: -10,
        right: 10,
        top: 10,
      },
      srs: PROJECTION.EPSG_4325.value,
    };
    store.dispatch(setBboxSync(defaultPayload, [], ['Area_1']));
    store.dispatch(
      syncGroupsActions.syncGroupAddSource({
        id: 'map_A',
        type: [syncConstants.SYNCGROUPS_TYPE_SETBBOX],
        defaultPayload: { ...defaultPayload, sourceId: 'map_A' },
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddSource({
        id: 'map_B',
        type: [syncConstants.SYNCGROUPS_TYPE_SETBBOX],
        defaultPayload: { ...defaultPayload, sourceId: 'map_B' },
      }),
    );

    expect(
      store.getState().syncGroups.sources.byId['map_A'].payloadByType[
        syncConstants.SYNCGROUPS_TYPE_SETBBOX
      ],
    ).toEqual({ ...defaultPayload, sourceId: 'map_A' });
    expect(store.getState().syncGroups.groups.byId['Area_1'].targets).toEqual({
      allIds: [],
      byId: {},
    });

    expect(
      store.getState().syncGroups.groups.byId['Area_1'].payloadByType,
    ).toEqual({
      [syncConstants.SYNCGROUPS_TYPE_SETBBOX]: defaultPayload,
      [syncConstants.SYNCGROUPS_TYPE_SETTIME]: null,
    });

    store.dispatch(
      syncGroupsActions.syncGroupAddTarget({
        groupId: 'Area_1',
        targetId: 'map_A',
        origin: 'user',
      }),
    );
    expect(store.getState().syncGroups.groups.byId['Area_1'].targets).toEqual({
      allIds: ['map_A'],
      byId: { map_A: { linked: true } },
    });

    store.dispatch(
      syncGroupsActions.syncGroupAddTarget({
        groupId: 'Area_1',
        targetId: 'map_A',
        origin: 'user',
        linked: false,
      }),
    );
    expect(store.getState().syncGroups.groups.byId['Area_1'].targets).toEqual({
      allIds: ['map_A'],
      byId: { map_A: { linked: true } },
    });

    store.dispatch(
      syncGroupsActions.syncGroupAddTarget({
        groupId: 'Area_1',
        targetId: 'map_B',
        origin: 'user',
        linked: true,
      }),
    );
    expect(store.getState().syncGroups.groups.byId['Area_1'].targets).toEqual({
      allIds: ['map_A', 'map_B'],
      byId: { map_A: { linked: true }, map_B: { linked: true } },
    });
  });

  it('should not add target if group does not exist', () => {
    jest.spyOn(console, 'warn').mockImplementation(() => {});
    const store = createMockStore();
    store.dispatch(
      syncGroupsActions.syncGroupAddSource({
        id: 'map_C',
        type: [
          syncConstants.SYNCGROUPS_TYPE_SETTIME,
          syncConstants.SYNCGROUPS_TYPE_SETBBOX,
        ],
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddGroup({
        groupId: 'Area_3',
        title: 'Zoompane',
        type: 'SYNCGROUPS_TYPE_SETBBOX',
        origin: 'user',
      }),
    );
    expect(store.getState().syncGroups.groups.byId['Area_3'].targets).toEqual({
      allIds: [],
      byId: {},
    });

    store.dispatch(
      syncGroupsActions.syncGroupAddTarget({
        groupId: 'Area_4',
        targetId: 'map_C',
        origin: 'user',
        linked: true,
      }),
    );
    expect(store.getState().syncGroups.groups.byId['Area_3'].targets).toEqual({
      allIds: [],
      byId: {},
    });
    expect(store.getState().syncGroups.groups.byId['Area_4']).not.toBeDefined();
  });

  it('should update the viewstate when changing sync groups', () => {
    const store = createMockStore();
    store.dispatch(
      syncGroupsActions.syncGroupAddGroup({
        groupId: 'Area_1',
        title: 'Zoompane',
        type: syncConstants.SYNCGROUPS_TYPE_SETBBOX,
        origin: 'user',
      }),
    );

    store.dispatch(
      syncGroupsActions.syncGroupAddSource({
        id: 'map_A',
        type: [syncConstants.SYNCGROUPS_TYPE_SETBBOX],
      }),
    );

    expect(store.getState().syncGroups.viewState.zoompane).toEqual({
      groups: [{ id: 'Area_1', selected: [] }],
      sourcesById: [{ id: 'map_A', name: 'map_A' }],
    });
    store.dispatch(
      syncGroupsActions.syncGroupAddTarget({
        groupId: 'Area_1',
        targetId: 'map_A',
        origin: 'user',
      }),
    );
    expect(store.getState().syncGroups.viewState.zoompane).toEqual({
      groups: [{ id: 'Area_1', selected: ['map_A'] }],
      sourcesById: [{ id: 'map_A', name: 'map_A' }],
    });
  });
});
