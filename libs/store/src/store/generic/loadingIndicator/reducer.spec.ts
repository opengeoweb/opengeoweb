/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { AnyAction } from '@reduxjs/toolkit';
import {
  loadingIndicatorActions,
  loadingIndicatorAdapter,
  loadingIndicatorReducer,
} from './reducer';
import { ANY_EDR_SERVICE } from './constants';

describe('store/generic/loadingIndicator/reducer', () => {
  it('should return initial state if no state and action passed in', () => {
    expect(loadingIndicatorReducer(undefined, {} as AnyAction)).toEqual(
      loadingIndicatorAdapter.getInitialState(),
    );
  });

  describe('setGetMapIsLoading', () => {
    it('should set isGetMapLoading to true', () => {
      const result = loadingIndicatorReducer(
        undefined,
        loadingIndicatorActions.setGetMapIsLoading({
          id: 'newId',
          isGetMapLoading: true,
        }),
      );

      expect(result.entities.newId).toBeDefined();

      expect(result.entities.newId).toEqual({
        id: 'newId',
        isGetMapLoading: true,
        isGetCapabilitiesLoading: false,
        isEDRLoading: false,
      });

      expect(result.ids).toContain('newId');
    });
  });

  describe('setGetCapabilitiesLoading', () => {
    it('should set isGetCapabilitiesLoading to true', () => {
      const result = loadingIndicatorReducer(
        undefined,
        loadingIndicatorActions.setGetCapabilitiesIsLoading({
          id: 'newId',
          isGetCapabilitiesLoading: true,
        }),
      );

      expect(result.entities.newId).toBeDefined();

      expect(result.entities.newId).toEqual({
        id: 'newId',
        isGetMapLoading: false,
        isGetCapabilitiesLoading: true,
        isEDRLoading: false,
      });

      expect(result.ids).toContain('newId');
    });
  });

  describe('setEDRLoading', () => {
    it('should set isEDRsLoading to true', () => {
      const result = loadingIndicatorReducer(
        undefined,
        loadingIndicatorActions.setEDRIsLoading({
          id: 'newId',

          isEDRLoading: true,
        }),
      );

      expect(result.entities.newId).toBeDefined();

      expect(result.entities.newId).toEqual({
        id: 'newId',
        isGetMapLoading: false,
        isGetCapabilitiesLoading: false,
        isEDRLoading: true,
      });

      expect(result.ids).toContain('newId');
    });

    it('should set isEDRsLoading for Any EDR service to true', () => {
      const result = loadingIndicatorReducer(
        undefined,
        loadingIndicatorActions.setEDRIsLoading({
          id: ANY_EDR_SERVICE,
          isEDRLoading: true,
        }),
      );

      expect(result.entities[ANY_EDR_SERVICE]).toBeDefined();

      expect(result.entities[ANY_EDR_SERVICE]).toEqual({
        id: ANY_EDR_SERVICE,
        isGetMapLoading: false,
        isGetCapabilitiesLoading: false,
        isEDRLoading: true,
      });

      expect(result.ids).toContain(ANY_EDR_SERVICE);
    });

    it('should not set isEDRsLoading to true for ANY_EDR_SERVICE once a specified EDR by id is already set to true', () => {
      const resultA = loadingIndicatorReducer(
        undefined,
        loadingIndicatorActions.setEDRIsLoading({
          id: 'some_id',
          isEDRLoading: true,
        }),
      );

      const resultB = loadingIndicatorReducer(
        resultA,
        loadingIndicatorActions.setEDRIsLoading({
          id: ANY_EDR_SERVICE,
          isEDRLoading: true,
        }),
      );

      expect(resultB.entities['some_id']).toBeDefined();
      expect(resultB.entities[ANY_EDR_SERVICE]).toBeDefined();

      expect(resultB.entities['some_id']).toEqual({
        id: 'some_id',
        isGetMapLoading: false,
        isGetCapabilitiesLoading: false,
        isEDRLoading: true,
      });

      expect(resultB.entities[ANY_EDR_SERVICE]).toEqual({
        id: ANY_EDR_SERVICE,
        isGetMapLoading: false,
        isGetCapabilitiesLoading: false,
        isEDRLoading: false,
      });

      expect(resultB.ids).toContain('some_id');
      expect(resultB.ids).toContain(ANY_EDR_SERVICE);
    });
  });
});
