/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { LayerType } from '@opengeoweb/webmap';
import { syncGroupsActions } from './syncGroups/reducer';
import { genericActions, syncConstants } from '.';
import { layerActions, mapActions } from '..';
import { createMockStore } from '../store';

describe('store/generic/listener', () => {
  it('should sync setTime', () => {
    const store = createMockStore();
    // setup sync store with one timeslider group with 2 maps linked
    store.dispatch(
      syncGroupsActions.syncGroupAddGroup({
        groupId: 'Time_1',
        title: 'Timeslider',
        type: syncConstants.SYNCGROUPS_TYPE_SETTIME,
        origin: 'user',
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddSource({
        id: 'map_A',
        type: [syncConstants.SYNCGROUPS_TYPE_SETTIME],
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddSource({
        id: 'map_B',
        type: [syncConstants.SYNCGROUPS_TYPE_SETTIME],
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddTarget({
        groupId: 'Time_1',
        targetId: 'map_A',
        origin: 'user',
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddTarget({
        groupId: 'Time_1',
        targetId: 'map_B',
        origin: 'user',
      }),
    );

    // change time in map_A
    const payload = {
      sourceId: 'map_A',
      origin: 'listener.spec',
      value: '2020-11-13T01:32:00Z',
    };
    store.dispatch(genericActions.setTime(payload));

    // time should be updated for the group and both maps
    expect(
      store.getState().syncGroups.groups.byId['Time_1'].payloadByType[
        syncConstants.SYNCGROUPS_TYPE_SETTIME
      ],
    ).toEqual(payload);
    expect(
      store.getState().syncGroups.sources.byId['map_A'].payloadByType[
        syncConstants.SYNCGROUPS_TYPE_SETTIME
      ],
    ).toEqual(payload);
    expect(
      store.getState().syncGroups.sources.byId['map_B'].payloadByType[
        syncConstants.SYNCGROUPS_TYPE_SETTIME
      ],
    ).toEqual(payload);
  });

  it('should ignore setTime with invalid value', () => {
    const store = createMockStore();
    const payload = {
      sourceId: 'test-1',
      origin: 'listener.spec',
      value: '2020-11-13T01:32:00.000Z',
    };
    jest.spyOn(console, 'error').mockImplementation();
    store.dispatch(genericActions.setTime(payload));

    expect(console.error).toHaveBeenCalledWith(
      expect.stringContaining(
        'setTime value 2020-11-13T01:32:00.000Z does not conform to format [YYYY-MM-DDThh:mm:ssZ]. It was triggered by listener.spec',
      ),
    );
  });

  it('should sync setBbox', () => {
    const store = createMockStore();
    // setup sync store with one zoompane group with 2 maps linked
    store.dispatch(
      syncGroupsActions.syncGroupAddGroup({
        groupId: 'Area_1',
        title: 'ZoomPane',
        type: syncConstants.SYNCGROUPS_TYPE_SETBBOX,
        origin: 'user',
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddSource({
        id: 'map_A',
        type: [syncConstants.SYNCGROUPS_TYPE_SETBBOX],
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddSource({
        id: 'map_B',
        type: [syncConstants.SYNCGROUPS_TYPE_SETBBOX],
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddTarget({
        groupId: 'Area_1',
        targetId: 'map_A',
        origin: 'user',
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddTarget({
        groupId: 'Area_1',
        targetId: 'map_B',
        origin: 'user',
      }),
    );

    // change bbox in map_A
    const payload = {
      sourceId: 'map_A',
      origin: 'listener.spec',
      bbox: {
        left: -10,
        bottom: -20,
        right: 10,
        top: 20,
      },
      srs: 'EPSG10',
    };
    store.dispatch(genericActions.setBbox(payload));

    // bbox should be updated for the group and both maps
    expect(
      store.getState().syncGroups.groups.byId['Area_1'].payloadByType[
        syncConstants.SYNCGROUPS_TYPE_SETBBOX
      ],
    ).toEqual(payload);
    expect(
      store.getState().syncGroups.sources.byId['map_A'].payloadByType[
        syncConstants.SYNCGROUPS_TYPE_SETBBOX
      ],
    ).toEqual(payload);
    expect(
      store.getState().syncGroups.sources.byId['map_B'].payloadByType[
        syncConstants.SYNCGROUPS_TYPE_SETBBOX
      ],
    ).toEqual(payload);
  });

  it('should sync layer syncGroupsActions', () => {
    const layerId1 = 'layerId1';
    const layerId2 = 'layerId2';
    const store = createMockStore();
    // setup map and layer store for 2 maps with the same layer each
    store.dispatch(mapActions.registerMap({ mapId: 'map_A' }));
    store.dispatch(mapActions.registerMap({ mapId: 'map_B' }));
    const layer = {
      name: 'test',
      layerType: LayerType.mapLayer,
    };
    store.dispatch(
      layerActions.addLayer({
        layerId: layerId1,
        mapId: 'map_A',
        layer,
        origin: 'layerManager',
      }),
    );
    store.dispatch(
      layerActions.addLayer({
        layerId: layerId2,
        mapId: 'map_B',
        layer,
        origin: 'layerManager',
      }),
    );
    // setup sync store with one layeractions group with the 2 maps linked
    store.dispatch(
      syncGroupsActions.syncGroupAddGroup({
        groupId: 'Layers_1',
        title: 'LayerActions',
        type: syncConstants.SYNCGROUPS_TYPE_SETLAYERACTIONS,
        origin: 'user',
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddSource({
        id: 'map_A',
        type: [syncConstants.SYNCGROUPS_TYPE_SETLAYERACTIONS],
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddSource({
        id: 'map_B',
        type: [syncConstants.SYNCGROUPS_TYPE_SETLAYERACTIONS],
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddTarget({
        groupId: 'Layers_1',
        targetId: 'map_A',
        origin: 'user',
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddTarget({
        groupId: 'Layers_1',
        targetId: 'map_B',
        origin: 'user',
      }),
    );

    // change opacity in layer1
    const payload = {
      layerId: layerId1,
      opacity: 0.46,
      mapId: 'map_A',
    };
    store.dispatch(layerActions.layerChangeOpacity(payload));

    // both layers should be updated
    expect(store.getState().layers.byId[layerId1].opacity).toEqual(
      payload.opacity,
    );
    expect(store.getState().layers.byId[layerId2].opacity).toEqual(
      payload.opacity,
    );

    expect(store.getState().webmap.byId['map_A'].mapLayers.length).toEqual(1);
    expect(store.getState().webmap.byId['map_B'].mapLayers.length).toEqual(1);

    // add layer in map A
    store.dispatch(
      layerActions.addLayer({
        layerId: 'layer3',
        mapId: 'map_A',
        layer: {
          name: 'testNewLayer',
          layerType: LayerType.mapLayer,
        },
        origin: 'layerManager',
      }),
    );

    // layer should be added in both maps
    expect(store.getState().webmap.byId['map_A'].mapLayers.length).toEqual(2);
    expect(store.getState().webmap.byId['map_B'].mapLayers.length).toEqual(2);

    // duplicate layer in map A
    store.dispatch(
      layerActions.duplicateMapLayer({
        oldLayerId: layerId1,
        mapId: 'map_A',
        newLayerId: 'layerNew',
        origin: 'layerManager',
      }),
    );

    // layer should be added to both maps
    expect(store.getState().webmap.byId['map_A'].mapLayers.length).toEqual(3);
    expect(store.getState().webmap.byId['map_B'].mapLayers.length).toEqual(3);

    // move layer in map A
    store.dispatch(
      mapActions.layerMoveLayer({
        oldIndex: 0,
        mapId: 'map_A',
        newIndex: 1,
        origin: 'layerManager',
      }),
    );

    // layer should be moved in both maps
    expect(store.getState().webmap.byId['map_A'].mapLayers).toEqual([
      'layer3',
      'layerNew',
      layerId1,
    ]);
    expect(store.getState().webmap.byId['map_B'].mapLayers).toEqual([
      'layerid_42',
      'layerid_43',
      layerId2,
    ]);

    expect(store.getState().webmap.byId['map_A'].autoUpdateLayerId).toEqual(
      layerId1,
    );
    expect(store.getState().webmap.byId['map_B'].autoUpdateLayerId).toEqual(
      layerId2,
    );

    // change auto layerid in map A
    store.dispatch(
      mapActions.setAutoLayerId({
        mapId: 'map_A',
        autoUpdateLayerId: 'layerNew',
      }),
    );

    // id should be updated in both maps
    expect(store.getState().webmap.byId['map_A'].autoUpdateLayerId).toEqual(
      'layerNew',
    );

    expect(store.getState().webmap.byId['map_A'].autoUpdateLayerId).toEqual(
      store.getState().webmap.byId['map_A'].mapLayers[1],
    );

    expect(store.getState().webmap.byId['map_B'].autoUpdateLayerId).toEqual(
      store.getState().webmap.byId['map_B'].mapLayers[1],
    );

    expect(store.getState().webmap.byId['map_A'].baseLayers).toEqual([]);
    expect(store.getState().webmap.byId['map_B'].baseLayers).toEqual([]);

    const newBaseLayer = {
      id: 'base-layer-1',
      name: 'WorldMap_Light_Grey_Canvas',
      type: 'twms',
      layerType: LayerType.baseLayer,
      mapId: 'map_A',
    };

    // change baselayers in map A
    store.dispatch(
      layerActions.setBaseLayers({
        mapId: 'map_A',
        layers: [newBaseLayer],
      }),
    );

    // baselayer should be updated in both maps
    expect(store.getState().webmap.byId['map_A'].baseLayers).toEqual([
      newBaseLayer.id,
    ]);
    expect(store.getState().webmap.byId['map_B'].baseLayers).not.toEqual([]);
  });

  it('should sync delete layer syncGroupsActions', () => {
    const store = createMockStore();
    // setup map and layer store for 2 maps with the same layer each
    store.dispatch(mapActions.registerMap({ mapId: 'map_A' }));
    store.dispatch(mapActions.registerMap({ mapId: 'map_B' }));
    const layer = {
      name: 'test',
      layerType: LayerType.mapLayer,
    };
    store.dispatch(
      layerActions.addLayer({
        layerId: 'layer1',
        mapId: 'map_A',
        layer,
        origin: 'layerManager',
      }),
    );
    store.dispatch(
      layerActions.addLayer({
        layerId: 'layer2',
        mapId: 'map_B',
        layer,
        origin: 'layerManager',
      }),
    );
    // setup sync store with one layeractions group with the 2 maps linked
    store.dispatch(
      syncGroupsActions.syncGroupAddGroup({
        groupId: 'Layers_1',
        title: 'LayerActions',
        type: syncConstants.SYNCGROUPS_TYPE_SETLAYERACTIONS,
        origin: 'user',
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddSource({
        id: 'map_A',
        type: [syncConstants.SYNCGROUPS_TYPE_SETLAYERACTIONS],
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddSource({
        id: 'map_B',
        type: [syncConstants.SYNCGROUPS_TYPE_SETLAYERACTIONS],
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddTarget({
        groupId: 'Layers_1',
        targetId: 'map_A',
        origin: 'user',
      }),
    );
    store.dispatch(
      syncGroupsActions.syncGroupAddTarget({
        groupId: 'Layers_1',
        targetId: 'map_B',
        origin: 'user',
      }),
    );

    expect(store.getState().webmap.byId['map_A'].mapLayers.length).toEqual(1);
    expect(store.getState().webmap.byId['map_B'].mapLayers.length).toEqual(1);

    // delete layer in map A
    store.dispatch(
      layerActions.layerDelete({
        layerId: 'layer1',
        mapId: 'map_A',
        layerIndex: 0,
      }),
    );

    // layer should be deleted from both maps
    expect(store.getState().webmap.byId['map_A'].mapLayers.length).toEqual(0);
    expect(store.getState().webmap.byId['map_B'].mapLayers.length).toEqual(0);
  });
});
