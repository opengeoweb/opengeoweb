/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { configureStore } from '@reduxjs/toolkit';
import { FeatureCollection } from 'geojson';
import * as turf from '@turf/turf';
import { genericSelectors, syncGroupsActions } from '.';
import { createSyncGroupMockState } from './syncGroups/__mocks__/mockState';
import { syncGroupsReducer } from './syncGroups/reducer';
import { genericListener } from './listener';

describe('src/store/generic/selectors', () => {
  describe('getTime', () => {
    it('should return time value', () => {
      const mockStore = createSyncGroupMockState();
      expect(genericSelectors.getTime(mockStore, 'mapA')).toEqual(
        '2021-02-22T19:57:00Z',
      );
    });
    it('should not return time value', () => {
      const mockStore = createSyncGroupMockState();
      expect(genericSelectors.getTime(mockStore, 'NonExistingMap')).toEqual(
        null,
      );
    });
  });

  describe('selectLinkedFeatures', () => {
    it('should return linked features', () => {
      const store = configureStore({
        middleware: [genericListener.middleware],
        reducer: {
          syncGroups: syncGroupsReducer,
        },
      });
      store.dispatch(
        syncGroupsActions.addSharedData({
          panelId: 'panel1',
          data: {
            features: [
              {
                id: 'feature-1',
                geoJSON: { type: 'FeatureCollection', features: [] },
              },
              {
                id: 'feature-2',
                geoJSON: { type: 'FeatureCollection', features: [] },
              },
            ],
            formFeatures: [
              {
                id: 'form-feature-1',
                geoJSON: { type: 'FeatureCollection', features: [] },
              },
              {
                id: 'form-feature-2',
                geoJSON: { type: 'FeatureCollection', features: [] },
              },
            ],
          },
        }),
      );
      expect(
        store.getState().syncGroups.linkedState.sharedData['panel1'].features,
      ).toEqual([
        {
          id: 'feature-1',
          geoJSON: { type: 'FeatureCollection', features: [] },
        },
        {
          id: 'feature-2',
          geoJSON: { type: 'FeatureCollection', features: [] },
        },
      ]);
    });

    it('should return an empty array if no linked features', () => {
      const store = configureStore({
        middleware: [genericListener.middleware],
        reducer: {
          syncGroups: syncGroupsReducer,
        },
      });
      store.dispatch(
        syncGroupsActions.addSharedData({
          panelId: 'panel-1',
          data: {
            features: [],
          },
        }),
      );

      expect(
        store.getState().syncGroups.linkedState.sharedData['panel-1'].features,
      ).toEqual([]);
    });
  });

  describe('getSelectedFeature', () => {
    const mockFeatures = [
      {
        id: 'features',
        geoJSON: {
          type: 'FeatureCollection',
          features: [
            {
              id: 'feature-1',
              type: 'Feature',
              properties: { name: 'feature-1' },
              geometry: { type: 'Point', coordinates: [12.0, 15.0] },
            },
            {
              id: 'feature-2',
              type: 'Feature',
              properties: { name: 'feature-2' },
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [12.0, 15.0],
                    [15.0, 11.0],
                    [13.0, 14.0],
                  ],
                ],
              },
            },
          ],
        } as FeatureCollection,
      },
    ];

    it('should return a point feature', () => {
      const store = configureStore({
        middleware: [genericListener.middleware],
        reducer: {
          syncGroups: syncGroupsReducer,
        },
      });
      store.dispatch(
        syncGroupsActions.addSharedData({
          panelId: 'mapA',
          data: {
            selectedFeatureId: 'feature-1',
            features: mockFeatures,
          },
        }),
      );

      const selectedFeature = genericSelectors.getSelectedFeature(
        store.getState().syncGroups,
        'mapA',
      );
      expect(selectedFeature?.id).toBe('feature-1');
      expect(selectedFeature?.lon).toBe(12.0);
      expect(selectedFeature?.lat).toBe(15.0);
    });

    it('should return a polygon feature', () => {
      const store = configureStore({
        middleware: [genericListener.middleware],
        reducer: {
          syncGroups: syncGroupsReducer,
        },
      });
      store.dispatch(
        syncGroupsActions.addSharedData({
          panelId: 'mapA',
          data: {
            selectedFeatureId: 'feature-2',
            features: mockFeatures,
          },
        }),
      );

      const selectedFeature = genericSelectors.getSelectedFeature(
        store.getState().syncGroups,
        'mapA',
      );
      expect(selectedFeature?.id).toBe('feature-2');

      const center = turf.center(mockFeatures[0].geoJSON.features[1]);
      expect(selectedFeature?.lon).toBe(center.geometry.coordinates[0]);
      expect(selectedFeature?.lat).toBe(center.geometry.coordinates[1]);
    });
  });
});

describe('selectLinkedFormFeatures', () => {
  it('should return linked form features', () => {
    const store = configureStore({
      middleware: [genericListener.middleware],
      reducer: {
        syncGroups: syncGroupsReducer,
      },
    });
    store.dispatch(
      syncGroupsActions.addSharedData({
        panelId: 'panel-1',
        data: {
          formFeatures: [
            {
              id: 'form-feature-1',
              geoJSON: { type: 'FeatureCollection', features: [] },
            },
            {
              id: 'form-feature-2',
              geoJSON: { type: 'FeatureCollection', features: [] },
            },
          ],
        },
      }),
    );

    expect(
      store.getState().syncGroups.linkedState.sharedData['panel-1']
        .formFeatures,
    ).toEqual([
      {
        id: 'form-feature-1',
        geoJSON: { type: 'FeatureCollection', features: [] },
      },
      {
        id: 'form-feature-2',
        geoJSON: { type: 'FeatureCollection', features: [] },
      },
    ]);
  });

  it('should return an empty array if no linked form features', () => {
    const store = configureStore({
      middleware: [genericListener.middleware],
      reducer: {
        syncGroups: syncGroupsReducer,
      },
    });
    store.dispatch(
      syncGroupsActions.addSharedData({
        panelId: 'panel-1',
        data: {
          formFeatures: [],
        },
      }),
    );

    expect(
      store.getState().syncGroups.linkedState.sharedData['panel-1']
        .formFeatures,
    ).toEqual([]);
  });
});
