/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { act, render, waitFor, screen } from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';
import { InitialMapProps } from '@opengeoweb/core';
import { mapActions } from '@opengeoweb/store';
import {
  getWMLayerById,
  mockGetCapabilities,
  setWMSGetCapabilitiesFetcher,
  webmapUtils,
} from '@opengeoweb/webmap';
import { Store } from '@reduxjs/toolkit';
import { http, HttpResponse } from 'msw';
import { setupServer } from 'msw/node';
import { capActions } from '../../store/reducer';
import { CapApiProvider, CapThemeStoreProvider } from '../Providers';
import { MapViewCapConnect } from './MapViewCapConnect';
import { createApi } from '../../utils/fakeApi';
import { CapApi } from '../../utils/api';
import { mockGeoJson } from '../../utils/mockGeoJson';
import { CapWarnings, FeedLastUpdated } from '../types';
import {
  mockCapWarningPresets,
  mockCapWarnings,
  mockCapWarnings2,
  mockLastUpdated,
} from '../../utils/mockCapData';
import {
  createMockStore,
  mockCapStateWithAlerts,
  mockCapStateWithFeeds,
} from '../../store';

const dispatchFeedsWithLastUpdatedTimes = (store: Store): void => {
  act(() => {
    store.dispatch(
      capActions.addManyAlertsToFeed({
        feedId: mockCapStateWithFeeds.feeds.ids[0],
        alertIds: [],
      }),
    );
    store.dispatch(
      capActions.addManyAlertsToFeed({
        feedId: mockCapStateWithFeeds.feeds.ids[1],
        alertIds: [],
      }),
    );
    store.dispatch(
      capActions.setFeedLastUpdatedTimes([
        {
          id: mockCapStateWithFeeds.feeds.ids[0],
          changes: { lastUpdated: mockLastUpdated.lastUpdated },
        },
        {
          id: mockCapStateWithFeeds.feeds.ids[1],
          changes: { lastUpdated: mockLastUpdated.lastUpdated },
        },
      ]),
    );
  });
};

describe('src/lib/components/MapViewCap/MapViewCapConnect', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
    webmapUtils.unRegisterAllWMJSLayersAndMaps();
  });

  const srs = 'EPSG:3857';
  const bbox = {
    left: -2324980.5498391856,
    bottom: 5890854.775012179,
    right: 6393377.702660825,
    top: 11652109.058827976,
  };

  const mockGetFeedLinks = jest.fn().mockImplementation(() =>
    Promise.resolve({
      data: ['https://testurl.cap.fmi.fi/test'],
    }),
  );

  const createFakeApi = (): CapApi => ({
    ...createApi,
    getFeedLinks: mockGetFeedLinks,
    getSingleAlert: (): Promise<{ data: CapWarnings }> =>
      Promise.resolve({
        data: mockCapWarnings,
      }),
    getLastUpdated: (): Promise<{ data: FeedLastUpdated }> =>
      Promise.resolve({
        data: mockLastUpdated,
      }),
  });

  const mapId = 'testMapId';

  const props = {
    srs,
    bbox,
    geoJson: mockGeoJson,
    capWarningPresets: mockCapWarningPresets,
    id: mapId,
  };

  const mockAlert1WithFeed = {
    ...mockCapWarnings.alert,
    feedAddress: mockCapWarningPresets.feeds[0].feedAddress,
  };
  const mockAlert2WithFeed = {
    ...mockCapWarnings2.alert,
    feedAddress: mockCapWarningPresets.feeds[1].feedAddress,
  };

  it('should render succesfully and attempt to fetch cap data', async () => {
    const store = createMockStore();

    const testLinks = [
      'https://testurl.cap.fmi.fi/alert1',
      'https://testurl.cap.fmi.fi/alert2',
    ];

    const feedLinksFn = jest.fn(() =>
      Promise.resolve({
        data: testLinks,
      }),
    );
    const singleAlertFn = jest.fn(() =>
      Promise.resolve({
        data: mockCapWarnings,
      }),
    );

    const createMockApi = (): CapApi => ({
      ...createApi,
      getFeedLinks: feedLinksFn,
      getSingleAlert: singleAlertFn,
    });

    const { container } = render(
      <CapApiProvider createApi={createMockApi}>
        <CapThemeStoreProvider store={store}>
          <MapViewCapConnect capWarningPresets={mockCapWarningPresets} />
        </CapThemeStoreProvider>
      </CapApiProvider>,
    );
    await waitFor(() => expect(container).toBeTruthy());

    await waitFor(() => {
      expect(feedLinksFn).toHaveBeenCalledTimes(2);
    });
    expect(singleAlertFn).toHaveBeenNthCalledWith(1, testLinks[0]);
    expect(singleAlertFn).toHaveBeenNthCalledWith(2, testLinks[1]);
  });

  it('should render map if layers provided ', async () => {
    const mockServiceUrl = 'https://geoservices.knmi.nl/wms';
    const mockLayer = {
      service: mockServiceUrl,
      name: 'RAD_NL25_PCP_CM',
      format: 'image/png',
      style: 'radar/nearest',
      enabled: true,
      layerType: 'mapLayer',
    };
    const REFERENCE_TIME = `reference_time`;
    const mockLayers = [mockLayer] as InitialMapProps['mapPreset']['layers'];
    const server = setupServer(
      ...[
        http.get(mockServiceUrl, () => {
          const capabilities = `
<WMS_Capabilities>
      <Capability>
          <Layer>
              <Layer>
                  <Name>${mockLayer.name}</Name>
                  <Dimension name="${REFERENCE_TIME}">
                      2024-08-19T12:00:00
                  </Dimension>
              </Layer>
          </Layer>
      </Capability>
  </WMS_Capabilities>
`;

          return HttpResponse.xml(capabilities, { status: 200 });
        }),
      ],
    );
    server.listen();

    const store = createMockStore();

    const testLinks = [
      'https://testurl.cap.fmi.fi/alert1',
      'https://testurl.cap.fmi.fi/alert2',
    ];

    const feedLinksFn = jest.fn(() =>
      Promise.resolve({
        data: testLinks,
      }),
    );
    const singleAlertFn = jest.fn(() =>
      Promise.resolve({
        data: mockCapWarnings,
      }),
    );

    const createMockApi = (): CapApi => ({
      ...createApi,
      getFeedLinks: feedLinksFn,
      getSingleAlert: singleAlertFn,
    });

    render(
      <CapApiProvider createApi={createMockApi}>
        <CapThemeStoreProvider store={store}>
          <MapViewCapConnect
            capWarningPresets={mockCapWarningPresets}
            layers={mockLayers}
          />
        </CapThemeStoreProvider>
      </CapApiProvider>,
    );

    await waitFor(() => {
      expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    });
  });

  it('should still render map, even if no layers provided ', async () => {
    const store = createMockStore();
    const testLinks = [
      'https://testurl.cap.fmi.fi/alert1',
      'https://testurl.cap.fmi.fi/alert2',
    ];

    const feedLinksFn = jest.fn(() =>
      Promise.resolve({
        data: testLinks,
      }),
    );
    const singleAlertFn = jest.fn(() =>
      Promise.resolve({
        data: mockCapWarnings,
      }),
    );

    const createMockApi = (): CapApi => ({
      ...createApi,
      getFeedLinks: feedLinksFn,
      getSingleAlert: singleAlertFn,
    });

    const noLayers = [] as InitialMapProps['mapPreset']['layers'];

    render(
      <CapApiProvider createApi={createMockApi}>
        <CapThemeStoreProvider store={store}>
          <MapViewCapConnect
            capWarningPresets={mockCapWarningPresets}
            layers={noLayers}
          />
        </CapThemeStoreProvider>
      </CapApiProvider>,
    );

    await waitFor(() => {
      expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    });
  });

  it('should write received cap data to store', async () => {
    const store = createMockStore();

    const testLink = ['https://testurl.cap.fmi.fi/alert1'];

    const feedLinksFn = jest.fn(() =>
      Promise.resolve({
        data: testLink,
      }),
    );
    const singleAlertFn = jest
      .fn()
      .mockImplementationOnce(() =>
        Promise.resolve({
          data: mockCapWarnings,
        }),
      )
      .mockImplementationOnce(() =>
        Promise.resolve({
          data: mockCapWarnings2,
        }),
      );

    const createMockApi = (): CapApi => ({
      ...createApi,
      getFeedLinks: feedLinksFn,
      getSingleAlert: singleAlertFn,
    });

    render(
      <CapApiProvider createApi={createMockApi}>
        <CapThemeStoreProvider store={store}>
          <MapViewCapConnect capWarningPresets={mockCapWarningPresets} />
        </CapThemeStoreProvider>
      </CapApiProvider>,
    );

    await waitFor(() => {
      expect(store.getState().cap?.alerts.ids.length).toBe(2);
    });
    expect(store.getState().cap?.alerts.entities).toEqual({
      [mockCapWarnings.alert.identifier]: mockAlert1WithFeed,
      [mockCapWarnings2.alert.identifier]: mockAlert2WithFeed,
    });

    expect(store.getState().cap?.feeds.ids.length).toBe(2);
    const [id1, id2] = store.getState().cap!.feeds.ids;
    expect(store.getState().cap?.feeds.entities[id1]?.alertIds).toEqual([
      mockAlert1WithFeed.identifier,
    ]);
    expect(store.getState().cap?.feeds.entities[id2]?.alertIds).toEqual([
      mockAlert2WithFeed.identifier,
    ]);
  });

  it('should write to store when fetching a single alert fails', async () => {
    const store = createMockStore();

    const testLinks = [
      'https://testurl.cap.fmi.fi/alert',
      'https://testurl.cap.fmi.fi/failingalert',
    ];

    const feedLinksFn = jest.fn(() =>
      Promise.resolve({
        data: testLinks,
      }),
    );

    const singleAlertFn = jest
      .fn()
      .mockReturnValueOnce(
        Promise.resolve({
          data: mockCapWarnings,
        }),
      )
      .mockReturnValueOnce(Promise.reject());

    const createMockApi = (): CapApi => ({
      ...createApi,
      getFeedLinks: feedLinksFn,
      getSingleAlert: singleAlertFn,
    });

    render(
      <CapApiProvider createApi={createMockApi}>
        <CapThemeStoreProvider store={store}>
          <MapViewCapConnect capWarningPresets={mockCapWarningPresets} />
        </CapThemeStoreProvider>
      </CapApiProvider>,
    );

    await waitFor(() => {
      expect(store.getState().cap?.alerts.ids.length).toBe(1);
    });
    const [id] = store.getState().cap!.alerts.ids;
    expect(store.getState().cap?.alerts.entities[id]).toEqual(
      mockAlert1WithFeed,
    );

    const firstFeedId = store.getState().cap!.feeds.ids[0];
    expect(store.getState().cap?.feeds.entities[firstFeedId]?.alertIds).toEqual(
      [mockAlert1WithFeed.identifier],
    );
  });

  it('should update last update time after polling interval', async () => {
    jest.useFakeTimers();

    const store = createMockStore();

    const fakeCurrentTime = new Date('2023-04-01T12:00:00Z');
    jest.setSystemTime(fakeCurrentTime);
    const mockLastUpdated = {
      lastUpdated: dateUtils.unix(dateUtils.sub(fakeCurrentTime, { hours: 2 })),
    };

    const lastUpdatedFn = jest.fn(() =>
      Promise.resolve({
        data: mockLastUpdated,
      }),
    );

    const testLinks = [
      'https://testurl.cap.fmi.fi/alert',
      'https://testurl.cap.fmi.fi/failingalert',
    ];

    const feedLinksFn = jest.fn(() =>
      Promise.resolve({
        data: testLinks,
      }),
    );

    const mockApi = (): CapApi => ({
      getFeedLinks: feedLinksFn,
      getSingleAlert: (): Promise<{ data: CapWarnings }> =>
        Promise.resolve({
          data: mockCapWarnings,
        }),
      getLastUpdated: lastUpdatedFn,
    });

    render(
      <CapApiProvider createApi={mockApi}>
        <CapThemeStoreProvider store={store}>
          <MapViewCapConnect capWarningPresets={mockCapWarningPresets} />
        </CapThemeStoreProvider>
      </CapApiProvider>,
    );

    expect(lastUpdatedFn).not.toHaveBeenCalled();
    await act(async () => {
      jest.advanceTimersByTime(mockCapWarningPresets.pollInterval);
    });

    await waitFor(() => {
      const feed1LastUpdated =
        store.getState().cap?.feeds.entities[
          mockCapWarningPresets.feeds[0].feedAddress
        ]?.lastUpdated;
      expect(
        dateUtils.isAfter(feed1LastUpdated!, mockLastUpdated.lastUpdated),
      ).toBeTruthy();
    });
    const feed2LastUpdated =
      store.getState().cap?.feeds.entities[
        mockCapWarningPresets.feeds[1].feedAddress
      ]?.lastUpdated;
    expect(
      dateUtils.isAfter(feed2LastUpdated!, mockLastUpdated.lastUpdated),
    ).toBeTruthy();

    await act(async () => {
      jest.advanceTimersByTime(mockCapWarningPresets.pollInterval);
    });
    expect(lastUpdatedFn).toHaveBeenCalledTimes(4); // Called twice for each feed

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should render a feature (polygon) layer on top of map using demo geoJSON data', async () => {
    jest
      .spyOn(webmapUtils, 'generateLayerId')
      .mockReturnValueOnce('layerid_geojsonlayer');
    const store = createMockStore();

    render(
      <CapThemeStoreProvider store={store}>
        <CapApiProvider createApi={createFakeApi}>
          <MapViewCapConnect {...props} />
        </CapApiProvider>
      </CapThemeStoreProvider>,
    );

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(
        mapActions.mapChangeDimension({
          dimension: {
            name: 'time',
            currentValue:
              mockCapStateWithAlerts.alerts.entities[
                mockCapStateWithAlerts.alerts.ids[0]
              ]!.onset,
          },
          mapId,
          origin: 'test',
        }),
      );
    });

    const onePieceOfGeoJSONDemoData = 'Forest fire warning';

    await waitFor(() => {
      expect(getWMLayerById('layerid_geojsonlayer')).toBeDefined();
    });

    await waitFor(() => {
      expect(
        JSON.stringify(getWMLayerById('layerid_geojsonlayer').geojson).includes(
          onePieceOfGeoJSONDemoData,
        ),
      ).toBeTruthy();
    });
  });

  it('should render an empty feature (polygon) layer when alert is not in effect', async () => {
    jest
      .spyOn(webmapUtils, 'generateLayerId')
      .mockReturnValueOnce('layerid_geojsonlayer');
    const store = createMockStore();

    render(
      <CapThemeStoreProvider store={store}>
        <CapApiProvider createApi={createFakeApi}>
          <MapViewCapConnect {...props} />
        </CapApiProvider>
      </CapThemeStoreProvider>,
    );

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(
        mapActions.mapChangeDimension({
          dimension: {
            name: 'time',
            currentValue:
              mockCapStateWithAlerts.alerts.entities[
                mockCapStateWithAlerts.alerts.ids[0]
              ]!.expires,
          },
          mapId,
          origin: 'test',
        }),
      );
    });

    await waitFor(() => expect(mockGetFeedLinks).toHaveBeenCalledTimes(2));

    const onePieceOfGeoJSONDemoData = 'Forest fire warning';

    await waitFor(() => {
      expect(getWMLayerById('layerid_geojsonlayer')).toBeDefined();
    });

    await waitFor(() => {
      expect(
        JSON.stringify(getWMLayerById('layerid_geojsonlayer').geojson).includes(
          onePieceOfGeoJSONDemoData,
        ),
      ).toBeFalsy();
    });

    await waitFor(() => expect(mockGetFeedLinks).toHaveBeenCalledTimes(2));
  });

  it('should *not* show map warning properties component on top of map when no click has yet occurred', async () => {
    const store = createMockStore();

    jest
      .spyOn(webmapUtils, 'generateLayerId')
      .mockReturnValueOnce('layerid_geojsonlayer');

    render(
      <CapThemeStoreProvider store={store}>
        <CapApiProvider createApi={createFakeApi}>
          <MapViewCapConnect {...props} />
        </CapApiProvider>
      </CapThemeStoreProvider>,
    );

    await waitFor(() => expect(mockGetFeedLinks).toHaveBeenCalledTimes(2));

    await waitFor(() => {
      expect(getWMLayerById('layerid_geojsonlayer')).toBeDefined();
    });

    await waitFor(() => {
      // component for map warning properties should not be found there as no click has occurred
      expect(screen.queryByTestId('map-warning-properties')).toBeNull();
    });
  });

  it('should fetch new data if new data is available', async () => {
    jest.useFakeTimers();

    const testLinks = [
      'https://testurl.cap.fmi.fi/alert1',
      'https://testurl.cap.fmi.fi/alert2',
    ];

    const feedLinksFn = jest.fn();
    feedLinksFn.mockImplementation(() =>
      Promise.resolve({
        data: testLinks,
      }),
    );
    const lastUpdatedFn = jest.fn();
    // feed has new data
    lastUpdatedFn.mockResolvedValue({
      data: { lastUpdated: mockLastUpdated.lastUpdated + 1000 },
    });
    const singleAlertFn = jest.fn();
    singleAlertFn.mockImplementation(() =>
      Promise.resolve({
        data: mockCapWarnings,
      }),
    );
    const createMockApi = (): CapApi => ({
      ...createApi,
      getFeedLinks: feedLinksFn,
      getLastUpdated: lastUpdatedFn,
      getSingleAlert: singleAlertFn,
    });

    const store = createMockStore();

    render(
      <CapThemeStoreProvider store={store}>
        <CapApiProvider createApi={createMockApi}>
          <MapViewCapConnect
            {...props}
            capWarningPresets={mockCapWarningPresets}
          />
        </CapApiProvider>
      </CapThemeStoreProvider>,
    );

    // create feeds with lastUpdated times to store
    dispatchFeedsWithLastUpdatedTimes(store);

    // Expect feed links to be fetched for both feeds immediately
    await waitFor(() => {
      expect(feedLinksFn).toHaveBeenCalledTimes(2);
    });
    expect(lastUpdatedFn).toHaveBeenCalledTimes(0);

    // Expect the last update times to be updated after the poll interval
    await act(async () => {
      jest.advanceTimersByTime(mockCapWarningPresets.pollInterval);
    });
    await waitFor(() => {
      expect(lastUpdatedFn).toHaveBeenCalledTimes(2);
    });

    // Both feeds should be updated
    await waitFor(() => {
      expect(feedLinksFn).toHaveBeenCalledTimes(4);
    });

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should not fetch new data if new data is not available', async () => {
    jest.useFakeTimers();

    const testLinks = [
      'https://testurl.cap.fmi.fi/alert1',
      'https://testurl.cap.fmi.fi/alert2',
    ];

    const feedLinksFn = jest.fn();
    feedLinksFn.mockImplementation(() =>
      Promise.resolve({
        data: testLinks,
      }),
    );

    // feed does not have new data
    const lastUpdatedFn = jest
      .fn()
      .mockResolvedValue({ data: mockLastUpdated });

    const singleAlertFn = jest.fn().mockImplementation(() =>
      Promise.resolve({
        data: mockCapWarnings,
      }),
    );

    const createMockApi = (): CapApi => ({
      ...createApi,
      getFeedLinks: feedLinksFn,
      getLastUpdated: lastUpdatedFn,
      getSingleAlert: singleAlertFn,
    });

    const store = createMockStore();

    render(
      <CapThemeStoreProvider store={store}>
        <CapApiProvider createApi={createMockApi}>
          <MapViewCapConnect
            {...props}
            capWarningPresets={mockCapWarningPresets}
          />
        </CapApiProvider>
      </CapThemeStoreProvider>,
    );

    // create feeds with lastUpdated times to store
    dispatchFeedsWithLastUpdatedTimes(store);

    // Expect feed links to be fetched for both feeds immediately
    await waitFor(() => {
      expect(feedLinksFn).toHaveBeenCalledTimes(2);
    });
    expect(lastUpdatedFn).toHaveBeenCalledTimes(0);

    // Expect the last update times to be updated after the poll interval
    await act(async () => {
      jest.advanceTimersByTime(mockCapWarningPresets.pollInterval);
    });
    await waitFor(() => {
      expect(lastUpdatedFn).toHaveBeenCalledTimes(2);
    });
    // Neither feed should be updated
    expect(feedLinksFn).toHaveBeenCalledTimes(2);

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should update each feed independently of each other', async () => {
    jest.useFakeTimers();

    const testLinks = [
      'https://testurl.cap.fmi.fi/alert1',
      'https://testurl.cap.fmi.fi/alert2',
    ];

    const feedLinksFn = jest.fn();
    feedLinksFn.mockImplementation(() =>
      Promise.resolve({
        data: testLinks,
      }),
    );

    const lastUpdatedFn = jest.fn();
    // one feed has new data but the other has not
    lastUpdatedFn
      .mockResolvedValueOnce({
        data: { lastUpdated: mockLastUpdated.lastUpdated + 1000 },
      })
      .mockResolvedValueOnce({
        data: { lastUpdated: mockLastUpdated.lastUpdated },
      });

    const singleAlertFn = jest.fn();
    singleAlertFn.mockImplementation(() =>
      Promise.resolve({
        data: mockCapWarnings,
      }),
    );

    const createMockApi = (): CapApi => ({
      ...createApi,
      getFeedLinks: feedLinksFn,
      getLastUpdated: lastUpdatedFn,
      getSingleAlert: singleAlertFn,
    });

    const store = createMockStore();

    render(
      <CapThemeStoreProvider store={store}>
        <CapApiProvider createApi={createMockApi}>
          <MapViewCapConnect
            {...props}
            capWarningPresets={mockCapWarningPresets}
          />
        </CapApiProvider>
      </CapThemeStoreProvider>,
    );

    // create feeds with lastUpdated times to store
    dispatchFeedsWithLastUpdatedTimes(store);

    // Expect feed links to be fetched for both feeds immediately
    await waitFor(() => {
      expect(feedLinksFn).toHaveBeenCalledTimes(2);
    });

    // Expect the last update times to be updated after the poll interval
    await act(async () => {
      jest.advanceTimersByTime(mockCapWarningPresets.pollInterval);
    });
    await waitFor(() => {
      expect(lastUpdatedFn).toHaveBeenCalledTimes(2);
    });

    // Only the second feed should be updated
    await waitFor(() => {
      expect(feedLinksFn).toHaveBeenCalledTimes(3);
    });

    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
