/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Card } from '@mui/material';
import React from 'react';
import { LayerManagerConnect } from '@opengeoweb/core';
import { PROJECTION } from '@opengeoweb/shared';
import type { Meta, StoryObj } from '@storybook/react';
import { generateMapId } from '@opengeoweb/webmap';
import { CapApiProvider, CapThemeStoreProvider } from '../Providers';
import { MapViewCapConnect, MapViewCapConnectProps } from './MapViewCapConnect';
import { mockCapWarningPresets } from '../../utils/mockCapData';
import { createMockStore } from '../../store';

const meta: Meta<typeof MapViewCapConnect> = {
  title: 'components/MapViewCapConnect',
  component: MapViewCapConnect,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the user MapViewCapConnect',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof MapViewCapConnect>;

const Demo = (props: MapViewCapConnectProps): React.ReactElement => {
  const mapId = generateMapId();
  return (
    <CapThemeStoreProvider store={createMockStore()}>
      <LayerManagerConnect />
      <CapApiProvider>
        <Card sx={{ height: '100vh', width: '100%', padding: '20px' }}>
          <MapViewCapConnect {...props} id={mapId} />
        </Card>
      </CapApiProvider>
    </CapThemeStoreProvider>
  );
};

export const MapViewCapConnectDemo: Story = {
  render: (props) => <Demo {...props} />,
  args: {
    id: generateMapId(),
    bbox: {
      left: 58703.6377,
      bottom: 6408480.4514,
      right: 3967387.5161,
      top: 11520588.9031,
    },
    srs: PROJECTION.EPSG_3857.value,
    capWarningPresets: mockCapWarningPresets,
    title: 'map view cap connect demo',
  },
};
