/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { act, render, screen } from '@testing-library/react';
import { produce } from 'immer';
import i18n from 'i18next';
import { WarningListRowCapConnect } from './WarningListRowCapConnect';
import { CapThemeStoreProvider } from '../../Providers';
import { mockCapWarningPresets } from '../../../utils/mockCapData';
import {
  capActions,
  createMockStore,
  mockCapStateWithAlerts,
} from '../../../store';
import { initCapI18n } from '../../../utils/i18n';

const capPresets = {
  ...mockCapWarningPresets,
  feeds: [
    {
      feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
      feedType: 'rss',
    },
  ],
};

const props = {
  alertId: '2.49.0.1.246.0.0.2022',
  mockCapWarningPresets,
};

const mockAlert = mockCapStateWithAlerts.alerts.entities[props.alertId]!;

initCapI18n();

const changeLanguageTo = async (language: string): Promise<void> => {
  await act(async () => {
    await i18n.changeLanguage(language);
  });
};

describe('src/components/LayerList/WarningListRowCapConnect', () => {
  it('should use the language set in i18next if available', async () => {
    await changeLanguageTo('sv');
    const store = createMockStore();
    render(
      <CapThemeStoreProvider i18nInstance={i18n} store={store}>
        <WarningListRowCapConnect
          alertId={props.alertId}
          capWarningPresets={capPresets}
        />
      </CapThemeStoreProvider>,
    );
    await act(async () => {
      store.dispatch(capActions.addManyAlerts([mockAlert]));
    });
    expect((await screen.findByTestId('event')).textContent).toEqual(
      'Varning för skogsbrand',
    );
  });

  it('should use English when available if a warning is not available in the language set in i18next', async () => {
    await changeLanguageTo('dev');
    const store = createMockStore();
    render(
      <CapThemeStoreProvider i18nInstance={i18n} store={store}>
        <WarningListRowCapConnect
          alertId={props.alertId}
          capWarningPresets={capPresets}
        />
      </CapThemeStoreProvider>,
    );
    await act(async () => {
      store.dispatch(capActions.addManyAlerts([mockAlert]));
    });
    expect((await screen.findByTestId('event')).textContent).toEqual(
      'Forest fire warning',
    );
  });

  it('should use the first language available if the language set in i18next or English cannot be found', async () => {
    await changeLanguageTo('en');
    const store = createMockStore();
    const mockAlertNoEnglish = produce(mockAlert, (draft) => {
      draft.languages[2].language = 'fr-FR';
    });
    render(
      <CapThemeStoreProvider i18nInstance={i18n} store={store}>
        <WarningListRowCapConnect
          alertId={props.alertId}
          capWarningPresets={capPresets}
        />
      </CapThemeStoreProvider>,
    );
    await act(async () => {
      store.dispatch(capActions.addManyAlerts([mockAlertNoEnglish]));
    });
    expect((await screen.findByTestId('event')).textContent).toEqual(
      'Metsäpalovaroitus',
    );
  });
});
