/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';
import GanttWrapper from './GanttWrapper';
import { tasks } from './mockTasks';
import { Task } from './Task';
/* eslint-disable testing-library/no-node-access */
describe('components/GanttChart/GanttWrapper', () => {
  it('should not fail on empty list', () => {
    const { baseElement } = render(<GanttWrapper tasks={[]} />);
    expect(baseElement.querySelector('.gantt-container')).toBeTruthy();
  });

  it('should render tasks with default viewmode', () => {
    const { baseElement } = render(<GanttWrapper tasks={tasks} />);
    expect(baseElement.querySelector('.gantt-container')).toBeTruthy();
    expect(baseElement.querySelectorAll('.bar-wrapper').length).toEqual(
      tasks.length,
    );
    expect(screen.getByText('Day').classList).toContain('Mui-selected');
  });

  it('should switch between viewmodes', () => {
    const { baseElement } = render(<GanttWrapper tasks={tasks} />);
    expect(baseElement.querySelector('.gantt-container')).toBeTruthy();
    expect(screen.getByText('Day').classList).toContain('Mui-selected');
    expect(screen.getByText('Hour').classList).not.toContain('Mui-selected');
    fireEvent.click(screen.getByText('Hour'));
    expect(screen.getByText('Day').classList).not.toContain('Mui-selected');
    expect(screen.getByText('Hour').classList).toContain('Mui-selected');
    fireEvent.click(screen.getByText('Quarter Day'));
    expect(screen.getByText('Hour').classList).not.toContain('Mui-selected');
    expect(screen.getByText('Quarter Day').classList).toContain('Mui-selected');
    fireEvent.click(screen.getByText('Half Day'));
    expect(screen.getByText('Quarter Day').classList).not.toContain(
      'Mui-selected',
    );
    expect(screen.getByText('Half Day').classList).toContain('Mui-selected');
    fireEvent.click(screen.getByText('Week'));
    expect(screen.getByText('Half Day').classList).not.toContain(
      'Mui-selected',
    );
    expect(screen.getByText('Week').classList).toContain('Mui-selected');
    fireEvent.click(screen.getByText('Month'));
    expect(screen.getByText('Week').classList).not.toContain('Mui-selected');
    expect(screen.getByText('Month').classList).toContain('Mui-selected');
    fireEvent.click(screen.getByText('Year'));
    expect(screen.getByText('Month').classList).not.toContain('Mui-selected');
    expect(screen.getByText('Year').classList).toContain('Mui-selected');
  });

  it('should display info dialog on clicking task', () => {
    const task = {
      id: 'Task 1',
      name: 'Test task',
      start: dateUtils.createDate('2022-11-10T10:00:00Z'),
      end: dateUtils.createDate('2022-11-11T12:00:00Z'),
      progress: 100,
      dependencies: '',
      custom_class: 'geoweb-red',
      severity: 'Extreme',
      phenomenon: 'Rain',
      area: 'Måløy - Svinøy',
    } as unknown as Task;
    const { baseElement } = render(<GanttWrapper tasks={[task]} />);
    expect(baseElement.querySelector('.gantt-container')).toBeTruthy();
    expect(baseElement.querySelector('.details-container')).toBeFalsy();

    // click first task
    fireEvent.click(baseElement.querySelectorAll('.bar-wrapper')[0]);
    expect(baseElement.querySelector('.details-container')).toBeTruthy();
    expect(
      baseElement.querySelector('.details-container')!.textContent,
    ).toContain(task.name);
    expect(
      baseElement.querySelector('.details-container')!.textContent,
    ).toContain(task.phenomenon);
    expect(
      baseElement.querySelector('.details-container')!.textContent,
    ).toContain(task.area);
    expect(
      baseElement.querySelector('.details-container')!.textContent,
    ).toContain(task.severity);
    const startDate = `10-11-2022 10:00 UTC`;
    expect(
      baseElement.querySelector('.details-container')!.textContent,
    ).toContain(startDate);
    const endDate = `11-11-2022 12:00 UTC`;
    expect(
      baseElement.querySelector('.details-container')!.textContent,
    ).toContain(endDate);
  });
});
/* eslint-enable testing-library/no-node-access */
