/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
export class Task {
  private _dependencies: string[] | string = [];

  id = '';
  name = '';
  start = '';
  end = '';
  _end: Date = null!;
  _start: Date = null!;
  // custom props
  severity: 'Minor' | 'Moderate' | 'Severe' | 'Extreme' = null!;
  title = '';
  description = '';
  author = '';
  area = '';
  phenomenon = '';

  constructor(options: Partial<Task> = {}) {
    Object.assign(this, options);
  }

  /**
   * Progress in percentage
   */
  private _progress = 0.52;

  get progress(): number {
    return this._progress || 0.52;
  }

  set progress(value: number) {
    this._progress = value || 0.52;
  }

  /**
   * A css custom class for the task chart bar
   */
  custom_class?: string;

  setDependencies(value: string | string[]): void {
    this._dependencies = Array.isArray(value)
      ? value
      : value.split(',').map((d) => d.trim());
  }

  set dependencies(value: string[] | string) {
    this._dependencies = Array.isArray(value)
      ? value
      : value
          .split(',')
          .map((t) => t.trim())
          .filter(Boolean);
  }

  get dependencies(): string[] | string {
    return this._dependencies;
  }
}
