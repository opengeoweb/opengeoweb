/* eslint-disable no-param-reassign */
/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { ApiProvider } from '@opengeoweb/api';
import type { Meta, StoryObj } from '@storybook/react';
import { CapThemeStoreProvider } from '../Providers';
import { createApi } from '../../utils/api';
import { mockCapWarningPresets } from '../../utils/mockCapData';
import GanttModule from './GanttModule';
import { createMockStore } from '../../store';

const meta: Meta<typeof GanttModule> = {
  title: 'components/Gantt Chart',
  component: GanttModule,
};
export default meta;

type Story = StoryObj<typeof GanttModule>;

const baseURL = 'https://dev.opengeoweb.com/cap';

const Demo = (): React.ReactElement => (
  <CapThemeStoreProvider store={createMockStore()}>
    <ApiProvider createApi={createApi} config={{ baseURL }}>
      <GanttModule capWarningPresets={mockCapWarningPresets} />
    </ApiProvider>
  </CapThemeStoreProvider>
);

export const GanttDemoRealApi: Story = {
  render: Demo,
  tags: ['!autodocs'],
};
