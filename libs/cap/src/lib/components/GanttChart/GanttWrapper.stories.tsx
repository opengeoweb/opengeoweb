/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Card } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import GanttWrapper from './GanttWrapper';
import { tasks } from './mockTasks';
import { ViewMode } from './ViewMode';

const meta: Meta<typeof GanttWrapper> = {
  title: 'components/Gantt Chart',
  component: GanttWrapper,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the user GanttWrapper',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof GanttWrapper>;

export const Component: Story = {
  args: {
    tasks,
    defaultViewMode: ViewMode.Day,
  },
  tags: ['!dev'],
};

export const ComponentDark: Story = {
  ...Component,
  render: (props) => (
    <Card>
      <GanttWrapper {...props} />
    </Card>
  ),
  tags: ['!dev', 'dark'],
};

export const EmptyList: Story = {
  args: {
    tasks: [],
  },
  tags: ['!dev'],
};

export const GanttDemo: Story = {
  render: () => (
    <Card sx={{ height: '100vh', width: '100vw' }}>
      <GanttWrapper tasks={tasks} />
    </Card>
  ),
  tags: ['!autodocs'],
};

export const GanttDemoDark: Story = {
  render: () => (
    <Card sx={{ height: '100vh', width: '100vw' }}>
      <GanttWrapper tasks={tasks} />
    </Card>
  ),
  tags: ['dark', '!autodocs'],
};

export const GanttDemoEmptyList: Story = {
  render: () => (
    <Card sx={{ height: '100vh', width: '100vw' }}>
      <GanttWrapper tasks={[]} />
    </Card>
  ),
  tags: ['!autodocs'],
};
