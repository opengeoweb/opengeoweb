/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { Card, LinearProgress } from '@mui/material';
import { useApiContext, useApi } from '@opengeoweb/api';
import { CapApi } from '../../utils/api';
import GanttWrapper from './GanttWrapper';
import { convertFeaturesToTasks } from './utils';
import { Task } from './Task';
import { CapPresets } from '../types';

interface GanttModuleProps {
  capWarningPresets: CapPresets;
}

const GanttModule: React.FC<GanttModuleProps> = ({
  capWarningPresets,
}: GanttModuleProps) => {
  const [tasks, setTasks] = React.useState<Task[] | null>(null);
  const { api } = useApiContext<CapApi>();
  const { isLoading, result } = useApi(
    api.getAllAlerts,
    capWarningPresets.feeds[0].feedAddress,
  );

  React.useEffect(() => {
    if (result) {
      const newTasks = convertFeaturesToTasks(result.features);
      setTasks(newTasks);
    }
  }, [result]);

  return (
    <Card sx={{ height: '100vh', width: '100vw' }}>
      {isLoading && (
        <LinearProgress data-testid="loading-bar" color="secondary" />
      )}
      <GanttWrapper tasks={tasks!} />
    </Card>
  );
};

export default GanttModule;
