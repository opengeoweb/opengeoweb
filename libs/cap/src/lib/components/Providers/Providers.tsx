/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { ApiProvider } from '@opengeoweb/api';
import { Store } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import {
  ThemeProviderProps,
  lightTheme,
  ThemeWrapper,
} from '@opengeoweb/theme';

import { Theme } from '@mui/material';
import { I18nextProvider } from 'react-i18next';
import i18n from 'i18next';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { initCapI18n } from '../../utils/i18n';

export const CapThemeWrapper: React.FC<ThemeProviderProps> = ({
  theme,
  children,
}: ThemeProviderProps) => <ThemeWrapper theme={theme}>{children}</ThemeWrapper>;

interface CapApiProviderProps {
  children: React.ReactNode;
  createApi?: () => void;
}

interface CapThemeStoreProviderProps {
  children?: React.ReactNode;
  theme?: Theme;
  store: Store;
  i18nInstance?: typeof i18n;
}

interface CapTranslationsWrapperProps {
  children?: React.ReactNode;
  i18nInstance?: typeof i18n;
}

/**
 * A Provider component which returns the Api Provider for the Cap library
 * @param children
 * @returns
 */
export const CapApiProvider: React.FC<CapApiProviderProps> = ({
  children,
  createApi = createFakeApi,
}: CapApiProviderProps) => (
  <ApiProvider createApi={createApi}>{children}</ApiProvider>
);

export interface CapStoreProviderProps extends ThemeProviderProps {
  store: Store;
}

export const CapThemeStoreProvider: React.FC<CapThemeStoreProviderProps> = ({
  children,
  theme = lightTheme,
  store,
  i18nInstance = initCapI18n(),
}: CapThemeStoreProviderProps) => (
  <Provider store={store}>
    <CapI18nProvider i18nInstance={i18nInstance}>
      <CapThemeWrapper theme={theme}>{children}</CapThemeWrapper>
    </CapI18nProvider>
  </Provider>
);

export const CapI18nProvider: React.FC<CapTranslationsWrapperProps> = ({
  children,
  i18nInstance = initCapI18n(),
}) => {
  return <I18nextProvider i18n={i18nInstance}>{children}</I18nextProvider>;
};
