/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { CapAlert, CapFeed, CapModule } from './types';
import * as capSelectors from './selectors';

const sampleAlert1: CapAlert = {
  feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
  certainty: 'Likely',
  onset: '2022-07-07T00:00:00+03:00',
  expires: '2022-07-07T23:53:10+03:00',
  identifier: '2.49.0.1.246.0.0.2022',
  languages: [
    {
      areaDesc: 'Uusimaa and Kymenlaakso',
      language: 'en-GB',
      event: 'Forest fire warning',
      senderName: 'Finnish Meteorological Institute',
      headline:
        'Orange forest fire warning: Uusimaa and Kymenlaakso, Thu 0.00 - 23.53',
      description:
        'A risk of forest fires is very high on from the night between Wednesday and Thursday to the night between Thursday and Friday with probability of 30 %.',
    },
  ],
  severity: 'Extreme',
};

const sampleAlert2: CapAlert = {
  feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
  certainty: 'Likely',
  onset: '2022-07-07T00:00:00+03:00',
  expires: '2022-07-07T23:53:10+03:00',
  identifier: '2.49.0.1.246.0.0.1125',
  languages: [
    {
      areaDesc: 'Uusimaa and Kymenlaakso',
      language: 'en-GB',
      event: 'Flood',
      senderName: 'Finnish Meteorological Institute',
      headline: 'Flood',
      description: 'Flood',
    },
  ],
  severity: 'Extreme',
};

const sampleAlert3: CapAlert = {
  feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
  certainty: 'Likely',
  onset: '2022-07-07T17:00:00+03:00',
  expires: '2022-07-08T23:53:10+03:00',
  identifier: '2.49.0.1.246.0.0.9843',
  languages: [
    {
      areaDesc: 'Uusimaa and Kymenlaakso',
      language: 'en-GB',
      event: 'Windy',
      senderName: 'Finnish Meteorological Institute',
      headline: 'Windy',
      description: 'Windy',
    },
  ],
  severity: 'Extreme',
};

describe('store/selectors', () => {
  describe('getCapStore', () => {
    it('should return the cap store', () => {
      const mockStoreModule: CapModule = {
        cap: {
          alerts: {
            entities: {},
            ids: [],
          },
          feeds: {
            entities: {},
            ids: [],
          },
        },
      };

      expect(capSelectors.getCapStore(mockStoreModule)).toEqual(
        mockStoreModule.cap,
      );
    });
  });

  describe('getCapFeedById', () => {
    it('should return a cap feed matching a given id', () => {
      const mockFeed1: CapFeed = {
        id: 'id1',
        alertIds: [sampleAlert1.identifier, sampleAlert2.identifier],
        lastUpdated: 1662644119,
      };
      const mockFeed2: CapFeed = {
        id: 'id2',
        alertIds: [sampleAlert3.identifier],
        lastUpdated: 1662644120,
      };
      const mockStore: CapModule = {
        cap: {
          feeds: {
            entities: {
              id1: mockFeed1,
              id2: mockFeed2,
            },
            ids: ['id1', 'id2'],
          },
          alerts: {
            entities: {
              [sampleAlert1.identifier]: sampleAlert1,
              [sampleAlert2.identifier]: sampleAlert2,
              [sampleAlert3.identifier]: sampleAlert3,
            },
            ids: [
              sampleAlert1.identifier,
              sampleAlert2.identifier,
              sampleAlert3.identifier,
            ],
          },
        },
      };

      expect(capSelectors.getCapFeedById(mockStore, 'id1')).toEqual(mockFeed1);
    });
  });

  describe('getAlertById', () => {
    it('should return a cap alert matching a given id', () => {
      const mockStore: CapModule = {
        cap: {
          feeds: { entities: {}, ids: [] },
          alerts: {
            entities: {
              [sampleAlert1.identifier]: sampleAlert1,
              [sampleAlert2.identifier]: sampleAlert2,
            },
            ids: [sampleAlert1.identifier, sampleAlert2.identifier],
          },
        },
      };

      expect(
        capSelectors.getAlertById(mockStore, sampleAlert1.identifier),
      ).toEqual(sampleAlert1);
    });
  });

  describe('getAlerts', () => {
    it('should return all alerts', () => {
      const mockStore: CapModule = {
        cap: {
          feeds: { entities: {}, ids: [] },
          alerts: {
            entities: {
              [sampleAlert1.identifier]: sampleAlert1,
              [sampleAlert2.identifier]: sampleAlert2,
            },
            ids: [sampleAlert1.identifier, sampleAlert2.identifier],
          },
        },
      };

      expect(capSelectors.getAlerts(mockStore)).toEqual([
        sampleAlert1,
        sampleAlert2,
      ]);
    });
  });

  describe('getAlertIds', () => {
    it('should return all alert Ids', () => {
      const mockStore: CapModule = {
        cap: {
          feeds: { entities: {}, ids: [] },
          alerts: {
            entities: {
              [sampleAlert1.identifier]: sampleAlert1,
              [sampleAlert2.identifier]: sampleAlert2,
            },
            ids: [sampleAlert1.identifier, sampleAlert2.identifier],
          },
        },
      };
      expect(capSelectors.getAlertIds(mockStore)).toEqual(
        mockStore.cap!.alerts.ids,
      );
    });
  });

  describe('getAlertsInEffect', () => {
    const mockStore: CapModule = {
      cap: {
        feeds: { entities: {}, ids: [] },
        alerts: {
          entities: {
            [sampleAlert1.identifier]: sampleAlert1,
            [sampleAlert2.identifier]: sampleAlert2,
            [sampleAlert3.identifier]: sampleAlert3,
          },
          ids: [
            sampleAlert1.identifier,
            sampleAlert2.identifier,
            sampleAlert3.identifier,
          ],
        },
      },
    };

    it('should return alerts in effect at the selected time', () => {
      expect(
        capSelectors.getAlertsInEffect(mockStore, '2022-07-07T18:00:00+03:00'),
      ).toEqual([sampleAlert1, sampleAlert2, sampleAlert3]);
    });

    it('should return alerts in effect at the selected time with inclusive start time', () => {
      expect(
        capSelectors.getAlertsInEffect(mockStore, '2022-07-07T00:00:00+03:00'),
      ).toEqual([sampleAlert1, sampleAlert2]);
    });

    it('should return alerts in effect at the selected time with exclusive end time', () => {
      expect(
        capSelectors.getAlertsInEffect(mockStore, '2022-07-07T23:53:10+03:00'),
      ).toEqual([sampleAlert3]);
    });

    it('should return empty array when no alerts are in effect', () => {
      expect(
        capSelectors.getAlertsInEffect(mockStore, '2022-07-09T00:00:10+03:00'),
      ).toEqual([]);
    });
  });
});
