/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { CapFeatures } from '../components/types';

export const mockGeoJson: CapFeatures = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {
        fill: '',
        stroke: '',
        'stroke-width': 0,
        'stroke-opacity': 0,
        details: {
          identifier: '2.49.0.1.246.0.0.2022',
          feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
          senderName: 'FMI',
          severity: 'Extreme',
          certainty: 'Likely',
          onset: '2022-07-07T00:00:00+03:00',
          expires: '2022-07-07T23:53:10+03:00',
          languages: [
            {
              areaDesc: 'Uusimaa ja Kymenlaakso',
              language: 'fi-FI',
              event: 'Metsäpalovaroitus',
              senderName: 'Ilmatieteen laitos',
              headline:
                'Oranssi metsäpalovaroitus: Uusimaa ja Kymenlaakso, to 0.00 - 23.53',
              description:
                'Metsäpalojen vaara on erittäin suuri keskiviikon ja torstain välisestä yöstä torstain ja perjantain väliseen yöhön 30 % todennäköisyydellä.',
            },
            {
              areaDesc: 'Nyland och Kymmenedalen',
              language: 'sv-FI',
              event: 'Varning för skogsbrand',
              senderName: 'Meteorologiska institutet',
              headline:
                'Orange varning för skogsbrand: Nyland och Kymmenedalen, to 0.00 - 23.53',
              description:
                'Risken för skogsbränder är mycket hög på från och med natten mellan onsdag och torsdag fram till natten mellan torsdag och fredag med 30 % sannolikhet.',
            },
            {
              areaDesc: 'Uusimaa and Kymenlaakso',
              language: 'en-GB',
              event: 'Forest fire warning',
              senderName: 'Finnish Meteorological Institute',
              headline:
                'Orange forest fire warning: Uusimaa and Kymenlaakso, Thu 0.00 - 23.53',
              description:
                'A risk of forest fires is very high on from the night between Wednesday and Thursday to the night between Thursday and Friday with probability of 30 %.',
            },
          ],
        },
      },
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [21.9287109375, 59.689926220143356],
            [28.256835937499996, 59.689926220143356],
            [28.256835937499996, 62.30879369102805],
            [21.9287109375, 62.30879369102805],
            [21.9287109375, 59.689926220143356],
          ],
        ],
      },
    },
  ],
};

export const mockGeoJson2: CapFeatures = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {
        fill: '',
        stroke: '',
        'stroke-width': 0,
        'stroke-opacity': 0,
        details: {
          identifier: '2.49.0.1.246.0.0.1125',
          feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
          senderName: 'FMI',
          severity: 'Extreme',
          certainty: 'Likely',
          onset: '2022-07-07T00:00:00+03:00',
          expires: '2022-07-07T23:53:10+03:00',
          languages: [
            {
              areaDesc: 'Uusimaa ja Kymenlaakso',
              language: 'fi-FI',
              event: 'Tulva',
              senderName: 'Ilmatieteen laitos',
              headline: 'Tulva',
              description: 'Tulva',
            },
            {
              areaDesc: 'Nyland och Kymmenedalen',
              language: 'sv-FI',
              event: 'Översvämning',
              senderName: 'Meteorologiska institutet',
              headline: 'Översvämning',
              description: 'Översvämning.',
            },
            {
              areaDesc: 'Uusimaa and Kymenlaakso',
              language: 'en-GB',
              event: 'Flood',
              senderName: 'Finnish Meteorological Institute',
              headline: 'Flood',
              description: 'Flood',
            },
          ],
        },
      },
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [21.9287109375, 59.689926220143356],
            [28.256835937499996, 59.689926220143356],
            [28.256835937499996, 62.30879369102805],
            [21.9287109375, 62.30879369102805],
            [21.9287109375, 59.689926220143356],
          ],
        ],
      },
    },
  ],
};

export const mockGeoJson3: CapFeatures = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {
        fill: '',
        stroke: '',
        'stroke-width': 0,
        'stroke-opacity': 0,
        details: {
          identifier: '2.49.0.1.246.0.0.9843',
          feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
          senderName: 'FMI',
          severity: 'Extreme',
          certainty: 'Likely',
          onset: '2022-07-07T00:00:00+03:00',
          expires: '2022-07-07T23:53:10+03:00',
          languages: [
            {
              areaDesc: 'Uusimaa ja Kymenlaakso',
              language: 'fi-FI',
              event: 'Tuuli',
              senderName: 'Ilmatieteen laitos',
              headline: 'Tuuli',
              description: 'Tuuli',
            },
            {
              areaDesc: 'Nyland och Kymmenedalen',
              language: 'sv-FI',
              event: 'Vind',
              senderName: 'Meteorologiska institutet',
              headline: 'Vind',
              description: 'Vind',
            },
            {
              areaDesc: 'Uusimaa and Kymenlaakso',
              language: 'en-GB',
              event: 'Windy',
              senderName: 'Finnish Meteorological Institute',
              headline: 'Windy',
              description: 'Windy',
            },
          ],
        },
      },
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [21.9287109375, 59.689926220143356],
            [28.256835937499996, 59.689926220143356],
            [28.256835937499996, 62.30879369102805],
            [21.9287109375, 62.30879369102805],
            [21.9287109375, 59.689926220143356],
          ],
        ],
      },
    },
  ],
};
