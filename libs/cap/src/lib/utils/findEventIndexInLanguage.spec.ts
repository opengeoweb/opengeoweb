/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { findEventIndexInLanguage } from './findEventIndexInLanguage';
import { mockGeoJson } from './mockGeoJson';
import { mockCapWarningPresets } from './mockCapData';

describe('libs/cap/src/lib/utils/findEventIndexInLanguage', () => {
  const presets = {
    ...mockCapWarningPresets,
    feeds: [
      {
        feedAddress: 'https://test1',
        feedType: 'rss',
      },
      {
        feedAddress: 'https://test2',
        feedType: 'rss',
      },
    ],
  };

  describe('findEventIndexInLanguage', () => {
    it('should return the index of the language set in i18next', () => {
      expect(
        findEventIndexInLanguage(
          'sv',
          mockGeoJson.features[0].properties.details.languages,
          presets,
          'https://test1',
        ),
      ).toEqual(1);
    });

    it('should return the index of english if the language set in i18next is not available', () => {
      expect(
        findEventIndexInLanguage(
          'nonexistent',
          mockGeoJson.features[0].properties.details.languages,
          presets,
          'https://test1',
        ),
      ).toEqual(2);
    });

    it('should return 0 if neither english nor the language in i18next is available', () => {
      const geoJson = { ...mockGeoJson };
      geoJson.features[0].properties.details.languages =
        geoJson.features[0].properties.details.languages.filter(
          (l) => l.language !== 'en-GB',
        );

      expect(
        findEventIndexInLanguage(
          'en',
          mockGeoJson.features[0].properties.details.languages,
          presets,
          'https://test1',
        ),
      ).toEqual(0);
    });

    it('should return 0 if the function cannot find correct feed from presets using the feedAddress', () => {
      expect(
        findEventIndexInLanguage(
          'en',
          mockGeoJson.features[0].properties.details.languages,
          presets,
          'notAValidFeedAddress',
        ),
      ).toEqual(0);
    });
  });
});
