/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import '../../../libs/theme/src/lib/components/Theme/styles.css';
import * as previewHelpers from '../../../.storybook/preview';
import {
  CapI18nProvider,
  CapThemeWrapper,
} from '../src/lib/components/Providers/Providers';
import { lightTheme, darkTheme } from '@opengeoweb/theme';

export const decorators = [
  (Story, params) => {
    const theme = params.tags.indexOf('dark') !== -1 ? darkTheme : lightTheme;
    return (
      <CapThemeWrapper theme={theme}>
        <CapI18nProvider>
          <Story />
        </CapI18nProvider>
      </CapThemeWrapper>
    );
  },
];

const parameters = {
  ...previewHelpers.parameters,
  tags: ['autodocs'],
};

export { parameters };

export default parameters;
