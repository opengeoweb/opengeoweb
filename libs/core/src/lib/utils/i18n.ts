/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import i18n from 'i18next';
import {
  UseTranslationResponse,
  initReactI18next,
  useTranslation,
} from 'react-i18next';
import { SHARED_NAMESPACE, sharedTranslations } from '@opengeoweb/shared';
import {
  TIMESLIDER_NAMESPACE,
  timesliderTranslations,
} from '@opengeoweb/timeslider';
import {
  WEBMAP_NAMESPACE,
  WEBMAP_REACT_NAMESPACE,
  webmapReactTranslations,
  webmapTranslations,
} from '@opengeoweb/webmap-react';

import coreTranslations from '../../../locales/core.json';

export const CORE_NAMESPACE = 'core';

export const initCoreReactI18n = (): void => {
  void i18n.use(initReactI18next).init({
    lng: 'en',
    fallbackLng: 'en',
    ns: CORE_NAMESPACE,
    interpolation: {
      escapeValue: false,
    },
    resources: {
      en: {
        [CORE_NAMESPACE]: coreTranslations.en,
        [WEBMAP_REACT_NAMESPACE]: webmapReactTranslations.en,
        [WEBMAP_NAMESPACE]: webmapTranslations.en,
        [SHARED_NAMESPACE]: sharedTranslations.en,
        [TIMESLIDER_NAMESPACE]: timesliderTranslations.en,
      },
      fi: {
        [CORE_NAMESPACE]: coreTranslations.fi,
        [WEBMAP_REACT_NAMESPACE]: webmapReactTranslations.fi,
        [WEBMAP_NAMESPACE]: webmapTranslations.fi,
        [SHARED_NAMESPACE]: sharedTranslations.fi,
        [TIMESLIDER_NAMESPACE]: timesliderTranslations.fi,
      },
      no: {
        [CORE_NAMESPACE]: coreTranslations.no,
        [WEBMAP_REACT_NAMESPACE]: webmapReactTranslations.no,
        [WEBMAP_NAMESPACE]: webmapTranslations.no,
        [SHARED_NAMESPACE]: sharedTranslations.no,
        [TIMESLIDER_NAMESPACE]: timesliderTranslations.no,
      },
      nl: {
        [CORE_NAMESPACE]: coreTranslations.nl,
        [WEBMAP_REACT_NAMESPACE]: webmapReactTranslations.nl,
        [WEBMAP_NAMESPACE]: webmapTranslations.nl,
        [SHARED_NAMESPACE]: sharedTranslations.nl,
        [TIMESLIDER_NAMESPACE]: timesliderTranslations.nl,
      },
    },
  });
};

const ns = [CORE_NAMESPACE, TIMESLIDER_NAMESPACE];

export const translateKeyOutsideComponents = (
  key: string,
  params: Record<string, string | number> | undefined = undefined,
): string => i18n.t(key, { ns, ...params });

export const useCoreTranslation = (): UseTranslationResponse<
  typeof CORE_NAMESPACE,
  typeof i18n
> => useTranslation(ns);

export { i18n };
