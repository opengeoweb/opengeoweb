/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { LayerType } from '@opengeoweb/webmap';
import {
  InitialAppPreset,
  InitialAppPresetProps,
  filterMapPresets,
} from './jsonPresetFilter';
import defaultMapPresets from './initialPresets.json';

describe('utils/JsonPresetFilter', () => {
  const undefinedFilteredPresets: InitialAppPresetProps = {
    presetType: undefined,
    presetId: undefined,
    presetName: undefined,
    services: undefined,
    baseServices: undefined,
    timeSeriesServices: undefined,
  };

  const testPreset: InitialAppPreset = {
    preset: {
      presetType: 'mapPreset',
      presetId: 'test1',
      presetName: 'test',
      timeSeriesServices: [
        {
          name: 'KNMI OBS',
          url: 'https://gw-edr.pub.knmi.cloud/edr',
          id: 'knmi',
          description: 'KNMI EDR',
          type: 'EDR',
        },
      ],
      services: [
        {
          name: 'KNMI Radar',
          url: 'https://geoservices.knmi.nl/cgi-bin/RADNL_OPER_R___25PCPRR_L3.cgi?',
          id: 'knmi-radar',
        },
      ],
      baseServices: [
        {
          name: 'KNMIgeoservicesBaselayers',
          url: 'https://geoservices.knmi.nl/wms?DATASET=baselayers&',
          id: 'KNMIgeoservicesBaselayers',
        },
        {
          name: 'DWD',
          url: 'https://maps.dwd.de/geoserver/ows?',
          id: 'dwd',
        },
      ],
      layers: [
        {
          id: 'base-layer-1',
          name: 'WorldMap_Light_Grey_Canvas',
          type: 'twms',
          layerType: LayerType.baseLayer,
        },
        {
          service: 'https://geoservices.knmi.nl/cgi-bin/worldmaps.cgi?',
          name: 'ne_10m_admin_0_countries_simplified',
          format: 'image/png',
          enabled: true,
          layerType: LayerType.overLayer,
        },
        {
          service:
            'https://geoservices.knmi.nl/cgi-bin/RADNL_OPER_R___25PCPRR_L3.cgi?',
          name: 'RADNL_OPER_R___25PCPRR_L3_COLOR',
          id: 'RAD_NL25_PCP_CM',
          format: 'image/png',
          enabled: true,
          style: 'knmiradar/nearest',
          layerType: LayerType.mapLayer,
        },
        {
          service: 'https://geoservices.knmi.nl/adagucserver?dataset=HARM_N25',
          name: 'air_temperature__at_pl',
          id: 'air_temperature__at_pl',
          format: 'image/png',
          enabled: true,
          style: '',
          layerType: LayerType.mapLayer,
          dimensions: [
            {
              name: 'elevation',
              currentValue: '850',
            },
          ],
        },
      ],
    },
  };

  describe('filterMapPresets', () => {
    it('should return undefined values if presets is null or empty', () => {
      expect(filterMapPresets(null!)).toEqual(undefinedFilteredPresets);
      expect(filterMapPresets({ preset: {} })).toEqual(
        undefinedFilteredPresets,
      );

      expect(filterMapPresets({ preset: {} }).baseServices).toBeUndefined();
      expect(
        filterMapPresets({ preset: {} }).timeSeriesServices,
      ).toBeUndefined();
    });

    it('should have the correct default values', () => {
      const defaultPreset = filterMapPresets(
        defaultMapPresets as InitialAppPreset,
      );

      expect(defaultPreset.services).toHaveLength(8);
      expect(defaultPreset.baseServices).toHaveLength(2);
      expect(defaultPreset.baseLayers).toHaveLength(4);
    });

    it('should have the correct default timeseries values', () => {
      const defaultPreset = filterMapPresets(
        defaultMapPresets as InitialAppPreset,
      );

      expect(defaultPreset.timeSeriesServices).toHaveLength(4);
    });

    it('should return filtered presets', () => {
      const filteredTestPreset = filterMapPresets(testPreset);

      expect(filteredTestPreset.baseLayers).toHaveLength(1);

      expect(filteredTestPreset.mapLayers).toHaveLength(2);
      expect(filteredTestPreset.services).toHaveLength(1);
      expect(filteredTestPreset.baseServices).toHaveLength(2);
      expect(filteredTestPreset.baseServices).toEqual(
        testPreset.preset.baseServices,
      );
    });
    it('should return filtered timeserie presets', () => {
      const filteredTestPreset = filterMapPresets(testPreset);

      expect(filteredTestPreset.timeSeriesServices).toHaveLength(1);
    });
  });
});
