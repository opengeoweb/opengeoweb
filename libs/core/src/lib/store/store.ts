/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { SnackbarModuleStore, snackbarReducer } from '@opengeoweb/snackbar';
import {
  mapReducer,
  serviceReducer,
  layerReducer,
  uiReducer,
  mapListener,
  serviceListener,
  layersListener,
  WebMapStateModuleState,
  SynchronizationGroupModuleState,
  UIModuleState,
  syncGroupsReducer,
  genericListener,
} from '@opengeoweb/store';
import { combineReducers, configureStore } from '@reduxjs/toolkit';
import { ToolkitStore } from '@reduxjs/toolkit/dist/configureStore';

export const coreReducerMap = {
  webmap: mapReducer,
  services: serviceReducer,
  layers: layerReducer,
  syncGroups: syncGroupsReducer,
  ui: uiReducer,
  snackbar: snackbarReducer,
};

export const coreMiddlewares = [
  mapListener.middleware,
  serviceListener.middleware,
  layersListener.middleware,
  genericListener.middleware,
];

const coreRootReducer = combineReducers(coreReducerMap);

export const createMockStore = (
  mockState?:
    | WebMapStateModuleState
    | SynchronizationGroupModuleState
    | UIModuleState
    | SnackbarModuleStore,
): ToolkitStore =>
  configureStore({
    reducer: coreRootReducer,
    preloadedState: mockState,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(coreMiddlewares),
  });
