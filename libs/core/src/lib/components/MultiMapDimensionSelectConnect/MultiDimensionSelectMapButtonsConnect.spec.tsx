/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { mapUtils } from '@opengeoweb/store';
import { LayerType } from '@opengeoweb/webmap';
import MultiDimensionSelectMapButtonsConnect from './MultiDimensionSelectMapButtonsConnect';
import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

const mapId = 'map-1';
const mockMap = mapUtils.createMap({ id: mapId });
const mapId2 = 'map-2';
const mockMap2 = mapUtils.createMap({ id: mapId2 });
const layerId1 = 'layer-1';
const layerId5 = 'layer-5';
const mockStoreMapWithDimensions = {
  webmap: {
    byId: {
      [mapId]: {
        ...mockMap,
        mapLayers: [layerId1],
        dimensions: [
          {
            name: 'time',
            currentValue: '2021-11-11T12:50:00Z',
          },
          {
            name: 'reference_time',
            currentValue: '2021-11-11T09:00:00Z',
          },
          {
            name: 'elevation',
            currentValue: '850',
          },
        ],
      },
      [mapId2]: {
        ...mockMap2,
        mapLayers: [layerId5],
        dimensions: [
          {
            name: 'time',
            currentValue: '2021-11-11T12:50:00Z',
          },
          {
            name: 'reference_time',
            currentValue: '2021-11-11T09:00:00Z',
          },
        ],
      },
    },
    allIds: [mapId, mapId2],
  },
  layers: {
    byId: {
      [layerId1]: {
        mapId,
        service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
        name: 'RAD_NL25_PCP_CM',
        format: 'image/png',
        style: 'knmiradar/nearest',
        id: layerId1,
        opacity: 1,
        enabled: true,
        layerType: LayerType.mapLayer,
        dimensions: [
          { name: 'time', units: 'ISO8601', currentValue: 'someTimeVal' },
          { name: 'elevation', units: 'm', currentValue: '100' },
        ],
        [layerId5]: {
          mapId,
          service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
          name: 'RAD_NL25_PCP_CM',
          format: 'image/png',
          style: 'knmiradar/nearest',
          id: layerId1,
          opacity: 1,
          enabled: false,
          layerType: LayerType.mapLayer,
          dimensions: [
            { name: 'time', units: 'ISO8601', currentValue: 'someTimeVal' },
          ],
        },
      },
    },
    allIds: [layerId1, layerId5],
    availableBaseLayers: {
      byId: {},
      allIds: [],
    },
  },
};

describe('src/components/MultiMapDimensionSelect/MultiDimensionSelectMapButtonsConnect/', () => {
  it('should create buttons for every non-time dimensions a single map', () => {
    const store = createMockStore(mockStoreMapWithDimensions);
    const props = {
      mapId,
    };
    render(
      <DemoWrapperConnect store={store}>
        <MultiDimensionSelectMapButtonsConnect {...props} />
      </DemoWrapperConnect>,
    );

    // button should be present
    expect(screen.getByTestId('dimensionMapBtn-elevation')).toBeTruthy();
  });
  it('should create no buttons in case no non-time dimensions available for map', () => {
    const store = createMockStore(mockStoreMapWithDimensions);
    const props = {
      mapId: mapId2,
    };
    render(
      <DemoWrapperConnect store={store}>
        <MultiDimensionSelectMapButtonsConnect {...props} />
      </DemoWrapperConnect>,
    );

    // button should not be present
    expect(screen.queryByRole('button')).toBeFalsy();
  });
});
