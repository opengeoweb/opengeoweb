/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { storeTestSettings, uiTypes } from '@opengeoweb/store';
import { webmapUtils } from '@opengeoweb/webmap';
import { dimensionConfig } from '@opengeoweb/webmap-react';
import {
  flDimensionLayer,
  multiDimensionLayer,
  WmMultiDimensionLayer3,
} from '../../utils/defaultTestSettings';
import { mockStateMapWithDimensions } from '../../utils/testUtils';
import { createMockStore } from '../../store';
import { DemoWrapperConnect } from '../Providers/Providers';
import DimensionSelectSliderConnect from './DimensionSelectSliderConnect';
import { i18n, initCoreReactI18n } from '../../utils/i18n';

beforeAll(() => {
  initCoreReactI18n();
});

describe('src/components/MultiMapDimensionSelect/DimensionSelectSliderConnect', () => {
  it('should render a slider if the layer has the passed dimension', () => {
    const mapId = 'mapid_1';
    webmapUtils.registerWMLayer(
      storeTestSettings.WmMultiDimensionLayer,
      'multiDimensionLayerMock',
    );
    const mockState2 = mockStateMapWithDimensions(multiDimensionLayer, mapId);
    const mockState = {
      ...mockState2,
      ui: {
        order: [
          uiTypes.DialogTypes.DimensionSelectElevation,
          uiTypes.DialogTypes.DimensionSelectEnsembleMember,
        ],
        dialogs: {
          [uiTypes.DialogTypes.DimensionSelectElevation]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectElevation,
          },
          [uiTypes.DialogTypes.DimensionSelectEnsembleMember]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectEnsembleMember,
          },
        },
      },
    };
    const store = createMockStore(mockState);
    const dimConfig = dimensionConfig(i18n.t).find(
      (cnf) => cnf.name === 'elevation',
    )!;
    const props = {
      layerId: 'multiDimensionLayerMock',
      mapId,
      dimConfig,
      dimensionName: 'elevation',
      handleDimensionValueChanged: jest.fn(),
      handleSyncChanged: jest.fn(),
    };
    render(
      <DemoWrapperConnect store={store}>
        <DimensionSelectSliderConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(screen.getByTestId('slider-dimensionSelect')).toBeTruthy();
    expect(screen.getByTestId('syncButton')).toBeTruthy();
  });

  it('should render a slider if layer is enabled', () => {
    const mapId = 'mapid_1';
    webmapUtils.registerWMLayer(
      storeTestSettings.WmMultiDimensionLayer,
      'multiDimensionLayerMock',
    );
    const mockState2 = mockStateMapWithDimensions(multiDimensionLayer, mapId);
    const mockState = {
      ...mockState2,
      ui: {
        order: [
          uiTypes.DialogTypes.DimensionSelectElevation,
          uiTypes.DialogTypes.DimensionSelectEnsembleMember,
        ],
        dialogs: {
          [uiTypes.DialogTypes.DimensionSelectElevation]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectElevation,
          },
          [uiTypes.DialogTypes.DimensionSelectEnsembleMember]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectEnsembleMember,
          },
        },
      },
    };
    if (mockState2.layers && mockState2.layers.byId) {
      mockState2.layers.byId['multiDimensionLayerMock'].enabled = true;
    }
    const store = createMockStore(mockState);
    const dimConfig = dimensionConfig(i18n.t).find(
      (cnf) => cnf.name === 'elevation',
    )!;
    const props = {
      layerId: 'multiDimensionLayerMock',
      mapId,
      dimConfig,
      dimensionName: 'elevation',
      handleDimensionValueChanged: jest.fn(),
      handleSyncChanged: jest.fn(),
    };
    render(
      <DemoWrapperConnect store={store}>
        <DimensionSelectSliderConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(screen.getByTestId('slider-dimensionSelect')).toBeTruthy();
    expect(screen.getByTestId('syncButton')).toBeTruthy();
  });

  it('should not render a slider if layer is not enabled', () => {
    const mapId = 'mapid_1';
    webmapUtils.registerWMLayer(
      storeTestSettings.WmMultiDimensionLayer,
      'multiDimensionLayerMock',
    );
    const mockState2 = mockStateMapWithDimensions(multiDimensionLayer, mapId);
    const mockState = {
      ...mockState2,
      ui: {
        order: [
          uiTypes.DialogTypes.DimensionSelectElevation,
          uiTypes.DialogTypes.DimensionSelectEnsembleMember,
        ],
        dialogs: {
          [uiTypes.DialogTypes.DimensionSelectElevation]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectElevation,
          },
          [uiTypes.DialogTypes.DimensionSelectEnsembleMember]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectEnsembleMember,
          },
        },
      },
    };
    if (mockState2.layers && mockState2.layers.byId) {
      mockState2.layers.byId['multiDimensionLayerMock'].enabled = false;
    }
    const store = createMockStore(mockState);
    const dimConfig = dimensionConfig(i18n.t).find(
      (cnf) => cnf.name === 'elevation',
    )!;

    const props = {
      layerId: 'multiDimensionLayerMock',
      mapId,
      dimConfig,
      dimensionName: 'elevation',
      handleDimensionValueChanged: jest.fn(),
      handleSyncChanged: jest.fn(),
    };
    render(
      <DemoWrapperConnect store={store}>
        <DimensionSelectSliderConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(screen.queryByTestId('slider-dimensionSelect')).toBeFalsy();
    expect(screen.queryByTestId('syncButton')).toBeFalsy();
  });

  it('should only render slider for elevation for layers with elevation dimension', () => {
    const mapId = 'mapid_1';
    webmapUtils.registerWMLayer(
      WmMultiDimensionLayer3,
      'multiDimensionLayerMock',
    );
    const mockState = mockStateMapWithDimensions(flDimensionLayer, mapId);
    const store = createMockStore(mockState);
    const dimConfig = dimensionConfig(i18n.t).find(
      (cnf) => cnf.name === 'elevation',
    )!;
    const props = {
      layerId: 'multiDimensionLayerMock',
      mapId,
      dimConfig,
      dimensionName: 'elevation',
      handleDimensionValueChanged: jest.fn(),
      handleSyncChanged: jest.fn(),
    };

    render(
      <DemoWrapperConnect store={store}>
        <DimensionSelectSliderConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(screen.queryByTestId('slider-dimensionSelect')).toBeFalsy();
  });

  it('should call handleSyncChanged when clicking on the sync button', () => {
    const mapId = 'mapid_1';
    webmapUtils.registerWMLayer(
      storeTestSettings.WmMultiDimensionLayer,
      'multiDimensionLayerMock',
    );
    const mockState2 = mockStateMapWithDimensions(multiDimensionLayer, mapId);
    const mockState = {
      ...mockState2,
      ui: {
        order: [
          uiTypes.DialogTypes.DimensionSelectElevation,
          uiTypes.DialogTypes.DimensionSelectEnsembleMember,
        ],
        dialogs: {
          [uiTypes.DialogTypes.DimensionSelectElevation]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectElevation,
          },
          [uiTypes.DialogTypes.DimensionSelectEnsembleMember]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectEnsembleMember,
          },
        },
      },
    };
    const store = createMockStore(mockState);
    const dimConfig = dimensionConfig(i18n.t).find(
      (cnf) => cnf.name === 'elevation',
    )!;
    const props = {
      layerId: 'multiDimensionLayerMock',
      mapId,
      dimConfig,
      dimensionName: 'elevation',
      handleDimensionValueChanged: jest.fn(),
      handleSyncChanged: jest.fn(),
    };
    render(
      <DemoWrapperConnect store={store}>
        <DimensionSelectSliderConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(screen.getByTestId('slider-dimensionSelect')).toBeTruthy();
    expect(screen.getByTestId('syncButton')).toBeTruthy();

    fireEvent.click(screen.getByTestId('syncButton'));
    expect(props.handleSyncChanged).toHaveBeenCalled();
  });
});
