/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { uiTypes, mapUtils } from '@opengeoweb/store';
import { LayerType } from '@opengeoweb/webmap';
import DimensionSelectButtonConnect from './DimensionSelectButtonConnect';
import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

describe('src/components/MultiMapDimensionSelect/DimensionSelectButtonConnect', () => {
  const mapId = 'map-1';
  const otherMap = 'otherMap';
  const mockMap = mapUtils.createMap({ id: mapId });
  const layerId1 = 'layer-1';
  const layerId2 = 'layer-2';
  const layerId3 = 'layer-3';
  const layerId5 = 'layerId5';
  const otherMapLayer = 'otherMapLayer';
  const otherMapLayer2 = 'otherMapLayer2';
  const mockMapWithLayersStore = {
    webmap: {
      byId: {
        [mapId]: {
          ...mockMap,
          mapLayers: [layerId1, layerId5],
          baseLayers: [layerId2],
          overLayers: [layerId3],
          activeLayerId: layerId1,
        },
        [otherMap]: {
          ...mockMap,
          mapLayers: [otherMapLayer, otherMapLayer2],
          activeLayerId: otherMapLayer,
          mapId: otherMap,
        },
      },
      allIds: [mapId, otherMap],
    },
    layers: {
      byId: {
        [layerId1]: {
          mapId,
          service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
          name: 'RAD_NL25_PCP_CM',
          format: 'image/png',
          style: 'knmiradar/nearest',
          id: layerId1,
          opacity: 1,
          enabled: true,
          layerType: LayerType.mapLayer,
          dimensions: [
            { name: 'time', units: 'ISO8601', currentValue: 'someTimeVal' },
            { name: 'elevation', units: 'm', currentValue: '100' },
          ],
        },
        [layerId2]: {
          mapId,
          name: 'arcGisSat',
          title: 'arcGisSat',
          type: 'twms',
          id: layerId2,
          opacity: 1,
          enabled: true,
          layerType: LayerType.baseLayer,
        },
        [layerId3]: {
          mapId,
          name: 'someOverLayer',
          title: 'someOverLayer',
          type: 'twms',
          id: layerId3,
          opacity: 1,
          enabled: true,
          layerType: LayerType.overLayer,
        },
        [otherMapLayer]: {
          mapId: otherMap,
          name: 'someLayerOnOtherMap',
          title: 'someLayerOnOtherMap',
          type: 'twms',
          id: otherMapLayer,
          opacity: 1,
          enabled: false,
          layerType: LayerType.overLayer,
          dimensions: [
            { name: 'time', units: 'ISO8601', currentValue: 'someTimeVal' },
            { name: 'elevation', units: 'm', currentValue: '100' },
          ],
        },
        [otherMapLayer2]: {
          mapId: otherMap,
          name: 'someLayerOnOtherMap',
          title: 'someLayerOnOtherMap',
          type: 'twms',
          id: otherMapLayer2,
          opacity: 1,
          enabled: true,
          layerType: LayerType.overLayer,
          dimensions: [
            { name: 'time', units: 'ISO8601', currentValue: 'someTimeVal' },
          ],
        },
        [layerId5]: {
          mapId,
          service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
          name: 'RAD_NL25_PCP_CM',
          format: 'image/png',
          style: 'knmiradar/nearest',
          id: layerId1,
          opacity: 1,
          enabled: false,
          layerType: LayerType.mapLayer,
          dimensions: [{ name: 'elevation', units: 'm', currentValue: '100' }],
        },
      },
      allIds: [
        layerId1,
        layerId2,
        layerId3,
        otherMapLayer,
        otherMapLayer2,
        layerId5,
      ],
      availableBaseLayers: { allIds: [], byId: {} },
    },
  };

  it('should show button if one of the enabled layers in that map contains that dimension', () => {
    const mockState = {
      [uiTypes.DialogTypes.DimensionSelectElevation]: {
        type: uiTypes.DialogTypes.DimensionSelectElevation,
        activeMapId: mapId,
        isOpen: false,
        source: 'app' as const,
      },
    };
    const newMockState = {
      ui: { dialogs: mockState, order: [] },
      ...mockMapWithLayersStore,
    };
    const store = createMockStore(newMockState);
    const props = {
      mapId,
      buttonTopPosition: 0,
      dimension: 'elevation',
      source: 'module' as uiTypes.Source,
    };
    render(
      <DemoWrapperConnect store={store}>
        <DimensionSelectButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    // button should be present
    expect(screen.getByTestId('dimensionMapBtn-elevation')).toBeTruthy();
  });

  it('should dispatch action with passed in mapid when clicked, action should set isOpen to true if currently closed', () => {
    const mockState = {
      [uiTypes.DialogTypes.DimensionSelectElevation]: {
        type: uiTypes.DialogTypes.DimensionSelectElevation,
        activeMapId: '',
        isOpen: false,
        source: 'app' as const,
      },
    };
    const newMockState = {
      ui: {
        dialogs: mockState,
        order: [uiTypes.DialogTypes.DimensionSelectElevation],
      },
      ...mockMapWithLayersStore,
    };
    const store = createMockStore(newMockState);
    const props = {
      mapId,
      buttonTopPosition: 0,
      dimension: 'elevation',
      source: 'module' as uiTypes.Source,
    };

    const initialState =
      store.getState().ui.dialogs[uiTypes.DialogTypes.DimensionSelectElevation];

    expect(initialState.isOpen).toBeFalsy();
    expect(initialState.activeMapId).toEqual('');
    render(
      <DemoWrapperConnect store={store}>
        <DimensionSelectButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    // button should be present
    expect(screen.getByTestId('dimensionMapBtn-elevation')).toBeTruthy();

    // open the dimension dialog for a different map
    fireEvent.click(screen.getByTestId('dimensionMapBtn-elevation'));

    const result =
      store.getState().ui.dialogs[uiTypes.DialogTypes.DimensionSelectElevation];

    expect(result.isOpen).toBeTruthy();
    expect(result.activeMapId).toEqual(mapId);
  });

  it('should dispatch action with passed in mapid when clicked, action should set isOpen to false if currently opened', () => {
    const mockState = {
      [uiTypes.DialogTypes.DimensionSelectElevation]: {
        type: uiTypes.DialogTypes.DimensionSelectElevation,
        activeMapId: mapId,
        isOpen: true,
        source: 'app' as const,
      },
    };
    const newMockState = {
      ui: { dialogs: mockState, order: [] },
      ...mockMapWithLayersStore,
    };
    const store = createMockStore(newMockState);
    const props = {
      mapId,
      buttonTopPosition: 0,
      dimension: 'elevation',
    };

    const initialState =
      store.getState().ui.dialogs[uiTypes.DialogTypes.DimensionSelectElevation];

    expect(initialState.isOpen).toBeTruthy();
    expect(initialState.activeMapId).toEqual(mapId);

    render(
      <DemoWrapperConnect store={store}>
        <DimensionSelectButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    // button should be present
    expect(screen.getByTestId('dimensionMapBtn-elevation')).toBeTruthy();

    // close the dimension dialog
    fireEvent.click(screen.getByTestId('dimensionMapBtn-elevation'));

    const result =
      store.getState().ui.dialogs[uiTypes.DialogTypes.DimensionSelectElevation];
    expect(result.isOpen).toBeFalsy();
    expect(result.activeMapId).toEqual(mapId);
  });

  it('should not show button if only disabled layers in that map contain that dimension', () => {
    const mockState = {
      [uiTypes.DialogTypes.DimensionSelectElevation]: {
        type: uiTypes.DialogTypes.DimensionSelectElevation,
        activeMapId: otherMap,
        isOpen: false,
        source: 'app' as const,
      },
    };
    const newMockState = {
      ui: { dialogs: mockState, order: [] },
      ...mockMapWithLayersStore,
    };
    const store = createMockStore(newMockState);
    const props = {
      mapId: otherMap,
      buttonTopPosition: 0,
      dimension: 'elevation',
      source: 'module' as uiTypes.Source,
    };
    render(
      <DemoWrapperConnect store={store}>
        <DimensionSelectButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    // button should be present
    expect(screen.queryByTestId('dimensionMapBtn-elevation')).toBeFalsy();
  });

  it('should not show button if none of in that map contain that dimension', () => {
    const mockState = {
      [uiTypes.DialogTypes.DimensionSelectElevation]: {
        type: uiTypes.DialogTypes.DimensionSelectEnsembleMember,
        activeMapId: otherMap,
        isOpen: false,
        source: 'app' as const,
      },
    };
    const newMockState = {
      ui: { dialogs: mockState, order: [] },
      ...mockMapWithLayersStore,
    };
    const store = createMockStore(newMockState);
    const props = {
      mapId: otherMap,
      buttonTopPosition: 0,
      dimension: 'ensemble_member',
      source: 'module' as uiTypes.Source,
    };
    render(
      <DemoWrapperConnect store={store}>
        <DimensionSelectButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    // button should be present
    expect(screen.queryByTestId('dimensionMapBtn-ensemble_member')).toBeFalsy();
  });
});
