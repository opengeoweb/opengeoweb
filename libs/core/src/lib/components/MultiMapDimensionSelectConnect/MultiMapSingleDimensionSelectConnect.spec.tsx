/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import { mapUtils, uiTypes } from '@opengeoweb/store';
import MultiMapSingleDimensionSelectConnect, {
  getDimensionType,
} from './MultiMapSingleDimensionSelectConnect';
import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

const mapId = 'map-1';
const mockMap = mapUtils.createMap({ id: mapId });
const mapId2 = 'map-2';
const mockMap2 = mapUtils.createMap({ id: mapId2 });
const mockStoreMapWithDimensions = {
  webmap: {
    byId: {
      [mapId]: {
        ...mockMap,
        dimensions: [
          {
            name: 'time',
            currentValue: '2021-11-11T12:50:00Z',
          },
          {
            name: 'reference_time',
            currentValue: '2021-11-11T09:00:00Z',
          },
          {
            name: 'elevation',
            currentValue: '850',
          },
        ],
      },
      [mapId2]: {
        ...mockMap2,
        dimensions: [
          {
            name: 'time',
            currentValue: '2021-11-11T12:50:00Z',
          },
          {
            name: 'reference_time',
            currentValue: '2021-11-11T09:00:00Z',
          },
          {
            name: 'member',
            currentValue: '2',
          },
        ],
      },
    },
    allIds: [mapId, mapId2],
  },
};

describe('src/components/MultiMapDimensionSelect/MultiMapSingleDimensionSelectConnect', () => {
  it('should register draggable dialogs for the passed dimension', () => {
    const store = createMockStore(mockStoreMapWithDimensions);

    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.DimensionSelectElevation],
    ).toBeUndefined();
    render(
      <DemoWrapperConnect store={store}>
        <MultiMapSingleDimensionSelectConnect dimensionName="elevation" />
      </DemoWrapperConnect>,
    );

    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.DimensionSelectElevation],
    ).toBeDefined();
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.DimensionSelectElevation]
        .isOpen,
    ).toBeFalsy();
  });
  describe('getDimensionType', () => {
    it('should return the dimensionType as used for the UI Dialogs', () => {
      expect(getDimensionType('elevation')).toEqual(
        uiTypes.DialogTypes.DimensionSelectElevation,
      );
      expect(getDimensionType('member')).toEqual('dimensionSelect-member');
      expect(getDimensionType('someFakeDimension')).toEqual(
        'dimensionSelect-someFakeDimension',
      );
    });
  });
});
