/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { initReactI18next, useTranslation } from 'react-i18next';
import { darkTheme } from '@opengeoweb/theme';
import i18n, { Resource } from 'i18next';
import { Box } from '@mui/material';
import { CoreThemeProvider } from '../Providers/Providers';
import LanguageSelect from './LanguageSelect';

import coreTranslations from '../../../../locales/core.json';
import { CORE_NAMESPACE } from '../../utils/i18n';

export default { title: 'components/LanguageSelect' };

const NAMESPACE = 'translation-demo';
const demoTranslations: Resource = {
  en: {
    greeting: 'Good morning',
    greetingWithName: 'Good morning {{name}}!',
  },
  nl: {
    greeting: 'Goedemorgen',
    greetingWithName: 'Goedemorgen {{name}}!',
  },
  fi: {
    greeting: 'Hyvää huomenta',
    greetingWithName: 'Hyvää huomenta {{name}}!',
  },
  no: {
    greeting: 'God morgen',
    greetingWithName: 'God morgen {{name}}!',
  },
};

const addI18nResourceBundles = (
  languageTranslations: Resource,
  namespace: string,
): void => {
  Object.keys(languageTranslations).forEach((langKey): void => {
    i18n.addResourceBundle(langKey, namespace, languageTranslations[langKey]);
  });
};

if (!i18n.isInitialized) {
  void i18n.use(initReactI18next).init({
    ns: NAMESPACE,
    debug: true,
    lng: 'en',
    interpolation: {
      escapeValue: false,
    },
  });
}

addI18nResourceBundles(demoTranslations, NAMESPACE);
addI18nResourceBundles(coreTranslations, CORE_NAMESPACE);

const LanguageDemo: React.FC = () => {
  const { t } = useTranslation([NAMESPACE]);

  const changeLanguage = (language: string): void => {
    // eslint-disable-next-line no-console
    console.log('change language to', language);
  };

  const name = 'GeoWeb';

  return (
    <>
      <h1>Examples of different ways to use react-i18next</h1>
      <LanguageSelect onChangeLanguage={changeLanguage} />
      <h4>useTranslation hook</h4>
      <p>{t('greeting')}</p>
      <h4>Interpolation with useTranslation</h4>
      <p>{t('greetingWithName', { name })}</p>
    </>
  );
};

export const Languages = (): React.ReactElement => {
  return (
    <CoreThemeProvider>
      <LanguageDemo />
    </CoreThemeProvider>
  );
};

Languages.parameters = {
  layout: 'padded',
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60ec68c87ed4860e1d14f5dd',
    },
  ],
};

export const LanguagesDark = (): React.ReactElement => {
  return (
    <CoreThemeProvider theme={darkTheme}>
      <LanguageDemo />
    </CoreThemeProvider>
  );
};

LanguagesDark.parameters = {
  layout: 'padded',
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60ec68cb9e0ee610eaf967ec',
    },
  ],
};

export const LanguagesSnaphot = (): React.ReactElement => {
  return (
    <CoreThemeProvider>
      <Box sx={{ padding: 1, width: 400, height: 300 }}>
        <LanguageSelect onChangeLanguage={() => {}} isOpen />
      </Box>
    </CoreThemeProvider>
  );
};

LanguagesSnaphot.tags = ['snapshot'];

export const LanguagesSnapshotDark = (): React.ReactElement => {
  return (
    <CoreThemeProvider theme={darkTheme}>
      <Box sx={{ padding: 1, width: 400, height: 300 }}>
        <LanguageSelect onChangeLanguage={() => {}} isOpen />
      </Box>
    </CoreThemeProvider>
  );
};
LanguagesSnapshotDark.tags = ['snapshot'];
