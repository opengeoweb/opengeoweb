/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import i18next, { Resource } from 'i18next';
import { I18nextProvider } from 'react-i18next';
import LanguageSelect, {
  getLanguageFromLocalStorage,
  languageLocalStorageKey,
  languages,
  saveLanguageToLocalStorage,
} from './LanguageSelect';
import coreTranslations from '../../../../locales/core.json';
import { CORE_NAMESPACE, initCoreReactI18n } from '../../utils/i18n';

initCoreReactI18n();

// add core translation so we can test the title
Object.keys(coreTranslations).forEach((langKey): void => {
  i18next.addResourceBundle(
    langKey,
    CORE_NAMESPACE,
    (coreTranslations as Resource)[langKey],
  );
});

// to not cause any issues with other i18n instance, we create our own test wrapper for isolation
const TestWrapper: React.FC<{ children: React.ReactNode }> = ({ children }) => (
  <ThemeWrapper>
    <I18nextProvider i18n={i18next}>{children}</I18nextProvider>
  </ThemeWrapper>
);

describe('src/components/LanguageSelect/LanguageSelect', () => {
  beforeEach(() => {
    // Clear localStorage before each test
    window.localStorage.clear();
  });

  describe('getLanguageFromLocalStorage', () => {
    it('should return null if no language is saved in localStorage', () => {
      const language = getLanguageFromLocalStorage();
      expect(language).toBeNull();
    });

    it('should return the language if it is saved in localStorage', () => {
      const mockLanguage = 'en';
      window.localStorage.setItem(languageLocalStorageKey, mockLanguage);
      const language = getLanguageFromLocalStorage();
      expect(language).toBe(mockLanguage);
    });
  });

  describe('saveLanguageToLocalStorage', () => {
    it('should save the language to localStorage', () => {
      const mockLanguage = 'fr';
      saveLanguageToLocalStorage(mockLanguage);
      const storedLanguage = window.localStorage.getItem(
        languageLocalStorageKey,
      );
      expect(storedLanguage).toBe(mockLanguage);
    });

    it('should overwrite the language if one already exists in localStorage', () => {
      const initialLanguage = 'es';
      const newLanguage = 'de';
      window.localStorage.setItem(languageLocalStorageKey, initialLanguage);
      saveLanguageToLocalStorage(newLanguage);
      const storedLanguage = window.localStorage.getItem(
        languageLocalStorageKey,
      );
      expect(storedLanguage).toBe(newLanguage);
    });
  });
  const defaultProps = {
    onChangeLanguage: jest.fn(),
  };

  it('should show English by default', () => {
    const props = { ...defaultProps };
    render(
      <TestWrapper>
        <LanguageSelect {...props} />
      </TestWrapper>,
    );
    expect(
      screen.getByText(
        i18next.t('language-select', '', { ns: [CORE_NAMESPACE] }),
      ),
    ).toBeTruthy();
    expect(
      screen.getByRole('textbox', { hidden: true }).getAttribute('value'),
    ).toBe('en');
  });

  it('should show no selected language if the translation in another language is missed', () => {
    const props = { ...defaultProps };
    void i18next.changeLanguage('rus');

    render(
      <TestWrapper>
        <LanguageSelect {...props} />
      </TestWrapper>,
    );
    expect(
      screen.getByRole('textbox', { hidden: true }).getAttribute('value'),
    ).toBe('');
  });

  it('should show the language that it was initialized with when it exists', () => {
    const props = {
      ...defaultProps,
    };

    void i18next.changeLanguage('fi');

    render(
      <TestWrapper>
        <LanguageSelect {...props} />
      </TestWrapper>,
    );
    expect(
      screen.getByRole('textbox', { hidden: true }).getAttribute('value'),
    ).toBe('fi');
  });

  it('should call onChangeLanguage when a Menu item is clicked', async () => {
    const props = {
      ...defaultProps,
    };
    render(
      <TestWrapper>
        <LanguageSelect {...props} />
      </TestWrapper>,
    );
    const otherLanguage = languages[1].language;

    fireEvent.mouseDown(screen.getByTestId('select'));

    const menuItem = await screen.findByText(otherLanguage);
    fireEvent.click(menuItem);

    expect(props.onChangeLanguage).toHaveBeenCalledWith(languages[1].code);
    expect(screen.getByTestId('select').textContent).toContain(otherLanguage);
  });

  it('should render a menu-item for each language in the list', () => {
    const props = {
      ...defaultProps,
    };
    render(
      <TestWrapper>
        <LanguageSelect {...props} />
      </TestWrapper>,
    );
    expect(screen.queryByRole('option')).toBeFalsy();

    fireEvent.mouseDown(screen.getByTestId('select'));

    const menuItems = screen.getAllByRole('option');
    // total languages + header
    expect(menuItems.length).toEqual(languages.length + 1);
  });

  it('should render as open', () => {
    const props = {
      ...defaultProps,
      isOpen: true,
    };
    render(
      <TestWrapper>
        <LanguageSelect {...props} />
      </TestWrapper>,
    );

    const menuItems = screen.getAllByRole('option');
    // total languages + header
    expect(menuItems.length).toEqual(languages.length + 1);
  });
});
