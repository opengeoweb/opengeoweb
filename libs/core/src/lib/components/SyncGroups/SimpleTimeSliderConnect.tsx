/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { connect } from 'react-redux';

import {
  CoreAppStore,
  genericActions,
  genericSelectors,
  syncConstants,
} from '@opengeoweb/store';
import { handleDateUtilsISOString } from '@opengeoweb/webmap';
import { dateUtils } from '@opengeoweb/shared';
import { SimpleTimeSlider } from './SimpleTimeSlider';

interface SimpleTimeSliderConnectComponentProps {
  id: string;
  timeValue?: string | null;
  setTime?: typeof genericActions.setTime;
  syncGroupAddSource?: typeof genericActions.syncGroupAddSource;
  syncGroupRemoveSource?: typeof genericActions.syncGroupRemoveSource;
}

const connectReduxSimpleTimeSlider = connect(
  (store: CoreAppStore, props: SimpleTimeSliderConnectComponentProps) => ({
    timeValue: genericSelectors.getTime(
      genericSelectors.getSynchronizationGroupStore(store),
      props.id,
    ),
  }),
  {
    setTime: genericActions.setTime,
    syncGroupAddSource: genericActions.syncGroupAddSource,
    syncGroupRemoveSource: genericActions.syncGroupRemoveSource,
  },
);

const SimpleTimeSliderConnectComponent: React.FC<
  SimpleTimeSliderConnectComponentProps
> = ({
  id,
  timeValue,
  setTime,
  syncGroupAddSource,
  syncGroupRemoveSource,
}: SimpleTimeSliderConnectComponentProps) => {
  React.useEffect(() => {
    syncGroupAddSource!({
      id,
      type: [syncConstants.SYNCGROUPS_TYPE_SETTIME],
    });
    return (): void => {
      syncGroupRemoveSource!({ id });
    };
  }, [id, syncGroupAddSource, syncGroupRemoveSource]);

  return (
    <SimpleTimeSlider
      setTime={(time): void => {
        setTime!({
          sourceId: id,
          origin: 'SimpleTimeSliderConnect, 72',
          value: handleDateUtilsISOString(time),
        });
      }}
      timeValue={timeValue!}
      startValue={
        dateUtils.dateToString(dateUtils.sub(dateUtils.utc(), { hours: 6 }))!
      }
      endValue={
        dateUtils.dateToString(dateUtils.sub(dateUtils.utc(), { minutes: 5 }))!
      }
    />
  );
};

export const SimpleTimeSliderConnect = connectReduxSimpleTimeSlider(
  SimpleTimeSliderConnectComponent,
);
