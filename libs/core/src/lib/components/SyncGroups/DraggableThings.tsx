/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Paper, Typography } from '@mui/material';
import React from 'react';

import Draggable from 'react-draggable';
import { Close } from '@opengeoweb/theme';
import { CustomIconButton } from '@opengeoweb/shared';
import { publicLayers } from '@opengeoweb/webmap-react';
import { SimpleTimeSliderConnect } from './SimpleTimeSliderConnect';
import { ConfigurableMapConnect } from '../ConfigurableMapConnect';

export interface DraggableThingProps {
  id: string;
  type: string;
  width: number;
  height: number;
}

interface DraggableThingsProps {
  openDialogs: DraggableThingProps[];
  handleClose: (id: string) => void;
}

interface DraggableDialogProps {
  dialogProps: DraggableThingProps;
  handleClose: () => void;
}

const DraggableDialog: React.FC<DraggableDialogProps> = ({
  dialogProps,
  handleClose,
}: DraggableDialogProps) => {
  return (
    <Draggable
      bounds="body"
      handle=".handle"
      defaultPosition={{ x: 50, y: 50 }}
      position={null!}
    >
      <Paper
        style={{
          padding: '5px',
          width: `${dialogProps.width}px`,
          height: `${dialogProps.height + 30}px`,
          display: 'grid',
          gridTemplateAreas: '"header"\n"content"',
          gridTemplateColumns: '1fr',
          gridTemplateRows: '30px 1fr',
          gridGap: '0px',
          position: 'absolute',
          zIndex: 20000,
        }}
        sx={{
          boxShadow: 6,
        }}
      >
        <span
          className="handle"
          style={{
            cursor: 'grab',
            gridArea: 'header',
          }}
        >
          <Typography>
            {dialogProps.id}
            <CustomIconButton onClick={handleClose} style={{ float: 'right' }}>
              <Close />
            </CustomIconButton>
          </Typography>
        </span>

        <div style={{ gridArea: 'content' }}>
          {dialogProps.type === 'map' && (
            <ConfigurableMapConnect
              layers={[publicLayers.radarLayer]}
              id={dialogProps.id}
              showTimeSlider={false}
            />
          )}
          {dialogProps.type === 'slider' && (
            <SimpleTimeSliderConnect id={dialogProps.id} />
          )}
        </div>
      </Paper>
    </Draggable>
  );
};

export const DraggableThings: React.FC<DraggableThingsProps> = ({
  openDialogs,
  handleClose,
}: DraggableThingsProps) => {
  return (
    <div>
      {openDialogs.map((dialog) => {
        return (
          <DraggableDialog
            key={dialog.id}
            dialogProps={dialog}
            handleClose={(): void => {
              handleClose(dialog.id);
            }}
          />
        );
      })}
    </div>
  );
};
