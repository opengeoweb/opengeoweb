/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { darkTheme } from '@opengeoweb/theme';
import { connect } from 'react-redux';
import { Box, Button } from '@mui/material';
import { produce } from 'immer';

import {
  uiActions,
  uiTypes,
  syncConstants,
  genericActions,
} from '@opengeoweb/store';
import { publicLayers } from '@opengeoweb/webmap-react';
import { DraggableThings, DraggableThingProps } from './DraggableThings';
import { SyncGroupViewerConnect } from './SyncGroupViewerConnect';
import { DemoWrapperConnect } from '../Providers/Providers';
import { ConfigurableMapConnect } from '../ConfigurableMapConnect';
import { createMockStore } from '../../store';

export default {
  title: 'components/SyncGroups',
};

const store = createMockStore();

const styles = {
  container: {
    display: 'grid',
    gridTemplateAreas: '"nav content content"',
    gridTemplateColumns: '350px 1fr',
    gridTemplateRows: '1fr',
    gridGap: '1px',
    height: '100vh',
    background: 'black',
  },
  nav: {
    gridArea: 'nav',
    margin: '0 0 0 1px',
    background: 'white',
    overflowY: 'scroll',
  },
  content: {
    gridArea: 'content',
    margin: '0 1px 0px 0px',
    display: 'grid',
    gridTemplateAreas: '"mapA mapB""mapC mapD"',
    gridTemplateColumns: '1fr 1fr',
    gridTemplateRows: '1fr 1fr',
    gridGap: '1px',
    height: '100%',
  },

  mapA: {
    gridArea: 'mapA',
    background: 'lightgray',
    height: '100%',
  },
  mapB: {
    gridArea: 'mapB',
    background: 'gray',
  },
  mapC: {
    gridArea: 'mapC',
    background: 'darkgray',
  },
  mapD: {
    gridArea: 'mapD',
    background: 'darkgray',
  },
};

interface InitSyncGroupsProps {
  syncGroupAddGroup: typeof genericActions.syncGroupAddGroup;
  syncGroupAddTarget: typeof genericActions.syncGroupAddTarget;
}
const InitSyncGroups = connect(null, {
  syncGroupAddGroup: genericActions.syncGroupAddGroup,
  syncGroupAddTarget: genericActions.syncGroupAddTarget,
})(({ syncGroupAddGroup, syncGroupAddTarget }: InitSyncGroupsProps) => {
  React.useEffect(() => {
    syncGroupAddGroup({
      groupId: 'Time_A',
      title: 'Group 1 for time',
      type: syncConstants.SYNCGROUPS_TYPE_SETTIME,
    });
    syncGroupAddGroup({
      groupId: 'Area_A',
      title: 'Group 2 for area',
      type: syncConstants.SYNCGROUPS_TYPE_SETBBOX,
    });
    syncGroupAddGroup({
      groupId: 'Time_B',
      title: 'Group 3 for time',
      type: syncConstants.SYNCGROUPS_TYPE_SETTIME,
    });
    syncGroupAddGroup({
      groupId: 'Area_B',
      title: 'Group 4 for area',
      type: syncConstants.SYNCGROUPS_TYPE_SETBBOX,
    });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_A' });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_B' });
    syncGroupAddTarget({ groupId: 'Area_B', targetId: 'map_C' });
    syncGroupAddTarget({ groupId: 'Area_B', targetId: 'map_D' });

    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_A' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_B' });
    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'map_C' });
    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'map_D' });
  }, [syncGroupAddGroup, syncGroupAddTarget]);
  return null;
});

export const FourMapsSynced = (): React.ReactElement => {
  const [openDialogs, setOpenDialogs] = React.useState<DraggableThingProps[]>(
    [],
  );

  const handleCloseDialog = (mapId: string): void => {
    setOpenDialogs(
      produce(openDialogs, (draft) => {
        draft.splice(
          draft.findIndex((a) => a.id === mapId),
          1,
        );
      }),
    );
  };

  const openSyncgroupsDialog = React.useCallback(() => {
    store.dispatch(
      uiActions.setToggleOpenDialog({
        type: uiTypes.DialogTypes.SyncGroups,
        setOpen: true,
      }),
    );
  }, []);

  return (
    <DemoWrapperConnect store={store} theme={darkTheme}>
      <InitSyncGroups />
      <DraggableThings
        openDialogs={openDialogs}
        handleClose={handleCloseDialog}
      />
      <Box sx={styles.container}>
        <Box sx={styles.nav}>
          <Button
            variant="contained"
            color="primary"
            onClick={(): void => {
              openSyncgroupsDialog();
            }}
          >
            open Sync
          </Button>
          <SyncGroupViewerConnect />
        </Box>
        <Box sx={styles.content}>
          <Box sx={styles.mapA}>
            <ConfigurableMapConnect
              bbox={{
                left: -39778.71186052833,
                right: 1097179.2546884636,
                bottom: 5424668.742925083,
                top: 8176241.573212288,
              }}
              id="map_A"
              layers={[publicLayers.radarLayer]}
              showTimeSlider={true}
              shouldShowZoomControls={false}
            />
          </Box>
          <Box sx={styles.mapB}>
            <ConfigurableMapConnect
              bbox={{
                left: -39778.71186052833,
                right: 1097179.2546884636,
                bottom: 5424668.742925083,
                top: 8176241.573212288,
              }}
              id="map_B"
              layers={[publicLayers.obsAirTemperature]}
              showTimeSlider={true}
              shouldShowZoomControls={false}
            />
          </Box>
          <Box sx={styles.mapC}>
            <ConfigurableMapConnect
              bbox={{
                left: -39778.71186052833,
                right: 1097179.2546884636,
                bottom: 5424668.742925083,
                top: 8176241.573212288,
              }}
              id="map_C"
              layers={[publicLayers.radarLayer]}
              showTimeSlider={true}
              shouldShowZoomControls={false}
            />
          </Box>
          <Box sx={styles.mapD}>
            <ConfigurableMapConnect
              bbox={{
                left: -39778.71186052833,
                right: 1097179.2546884636,
                bottom: 5424668.742925083,
                top: 8176241.573212288,
              }}
              id="map_D"
              layers={[publicLayers.obsAirTemperature]}
              showTimeSlider={true}
              shouldShowZoomControls={false}
            />
          </Box>
        </Box>
      </Box>
    </DemoWrapperConnect>
  );
};
