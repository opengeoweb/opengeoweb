/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  waitForElementToBeRemoved,
  screen,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { storeTestUtils } from '@opengeoweb/store';
import AddLayersButton from './AddLayersButton';
import { defaultReduxLayerRadarColor } from '../../../utils/defaultTestSettings';
import { DemoWrapper, DemoWrapperConnect } from '../../Providers/Providers';
import { createMockStore } from '../../../store';

describe('src/components/LayerManager/AddLayersButton/AddLayersButton', () => {
  const props = {
    mapId: 'mapId_1',
    tooltip: 'Add layers',
  };
  const mapId = 'mapid_1';
  const layer = defaultReduxLayerRadarColor;
  const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
  const store = createMockStore(mockState);
  const user = userEvent.setup();

  it('should render correct component', async () => {
    render(
      <DemoWrapper>
        <AddLayersButton />
      </DemoWrapper>,
    );
    expect(screen.getByTestId('addLayersButton')).toBeTruthy();

    const tooltip = screen.queryByRole('tooltip');
    expect(tooltip).toBeFalsy();
  });

  it('should show tooltip when hovering', async () => {
    render(
      <DemoWrapperConnect store={store}>
        <AddLayersButton {...props} />
      </DemoWrapperConnect>,
    );
    const addLayersButton = screen.getByTestId('addLayersButton');
    expect(addLayersButton).toBeTruthy();
    await user.hover(addLayersButton);
    // Wait until tooltip appears
    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip.textContent).toContain(props.tooltip);

    await user.unhover(addLayersButton);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
      timeout: 3000,
    });
  });

  it('should open addLayersPopup on click', async () => {
    render(
      <DemoWrapperConnect store={store}>
        <AddLayersButton {...props} />
      </DemoWrapperConnect>,
    );
    expect(screen.queryByTestId('addLayersPopup')).toBeFalsy();
    fireEvent.click(screen.queryByTestId('addLayersButton')!);
    await screen.findByTestId('addLayersPopup');
  });

  it('should have autofocus', async () => {
    render(
      <DemoWrapperConnect store={store}>
        <AddLayersButton {...props} shouldFocus />
      </DemoWrapperConnect>,
    );
    expect(screen.queryByTestId('addLayersPopup')).toBeFalsy();

    await waitFor(() =>
      expect(
        screen.getByTestId('addLayersButton')!.matches(':active'),
      ).toBeTruthy(),
    );

    expect(
      screen.getByTestId('addLayersButton').matches(':focus'),
    ).toBeTruthy();
  });
});
