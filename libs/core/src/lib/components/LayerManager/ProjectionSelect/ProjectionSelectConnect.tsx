/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  mapSelectors,
  CoreAppStore,
  mapActions,
  layerTypes,
} from '@opengeoweb/store';
import ProjectionSelect, { ProjectionDetails } from './ProjectionSelect';

export interface ProjectionSelectConnectProps {
  mapId: string;
}

const ProjectionSelectConnect: React.FC<ProjectionSelectConnectProps> = ({
  mapId,
}: ProjectionSelectConnectProps) => {
  const dispatch = useDispatch();

  const currentProjection = useSelector((store: CoreAppStore) =>
    mapSelectors.getSrs(store, mapId),
  );

  const setSrs = (projectionDetails: ProjectionDetails): void => {
    dispatch(
      mapActions.setBbox({
        mapId,
        srs: projectionDetails.value,
        bbox: projectionDetails.bbox,
        origin: layerTypes.LayerActionOrigin.layerManager,
      }),
    );
  };

  return (
    <ProjectionSelect
      currentProjection={currentProjection}
      onChangeProjection={setSrs}
    />
  );
};

export default ProjectionSelectConnect;
