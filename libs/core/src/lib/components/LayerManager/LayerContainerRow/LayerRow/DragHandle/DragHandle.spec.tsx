/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { translateKeyOutsideComponents } from '../../../../../utils/i18n';
import DragHandle from './DragHandle';
import { DemoWrapper } from '../../../../Providers/Providers';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/DragHandle', () => {
  it('should render correct component with tooltip', () => {
    render(
      <DemoWrapper>
        <DragHandle hideTooltip={false} />
      </DemoWrapper>,
    );
    const dragHandleButton = screen.queryByTestId('dragHandle')!;
    expect(dragHandleButton).toBeTruthy();
    expect(dragHandleButton.getAttribute('aria-label')).toEqual(
      translateKeyOutsideComponents('layermanager-layer-drag'),
    );
  });

  it('should hide tooltip', () => {
    render(
      <DemoWrapper>
        <DragHandle hideTooltip />
      </DemoWrapper>,
    );
    expect(
      screen.queryByTestId('dragHandle')!.getAttribute('aria-label'),
    ).toEqual('');
  });
  it('should show default tooltip on hover', async () => {
    render(
      <DemoWrapper>
        <DragHandle hideTooltip={false} />
      </DemoWrapper>,
    );
    fireEvent.mouseOver(screen.getByTestId('dragHandle'));
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('layermanager-layer-drag'),
      ),
    ).toBeTruthy();
  });
  it('should show custom tooltip on hover', async () => {
    const tooltipTitle = 'Custom tooltip';
    render(
      <DemoWrapper>
        <DragHandle hideTooltip={false} tooltipTitle={tooltipTitle} />
      </DemoWrapper>,
    );
    fireEvent.mouseOver(screen.getByTestId('dragHandle'));
    expect(await screen.findByText(tooltipTitle)).toBeTruthy();
  });
  it('should show default icon', async () => {
    render(
      <DemoWrapper>
        <DragHandle />
      </DemoWrapper>,
    );
    expect(screen.getByTestId('dragHandleIcon')).toBeTruthy();
  });
  it('should show custom icon', async () => {
    const testId = 'customIcon';
    render(
      <DemoWrapper>
        <DragHandle icon={<span data-testid={testId} />} />
      </DemoWrapper>,
    );
    expect(screen.getByTestId(testId)).toBeTruthy();
  });
});
