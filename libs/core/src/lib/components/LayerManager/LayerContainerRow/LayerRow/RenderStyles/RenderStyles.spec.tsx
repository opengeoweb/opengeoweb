/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import type { Style } from '@opengeoweb/webmap';
import { webmapTestSettings } from '@opengeoweb/webmap';
import RenderStyles, { RenderStylesProps } from './RenderStyles';
import { DemoWrapper } from '../../../../Providers/Providers';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/RenderStyles/RenderStyles', () => {
  const defaultProps: RenderStylesProps = {
    onChangeLayerStyle: jest.fn(),
    layerStyles: webmapTestSettings.defaultReduxLayerRadarKNMIWithStyles
      .styles as Style[],
    currentLayerStyle:
      webmapTestSettings.defaultReduxLayerRadarKNMIWithStyles.style!,
  };

  it('should show default style when layer not yet loaded', () => {
    const mockProps = {
      ...defaultProps,
      layerStyles: [],
      currentLayerStyle: '',
    };
    render(
      <DemoWrapper>
        <RenderStyles {...mockProps} />
      </DemoWrapper>,
    );
    const styleSelect = screen.getByTestId('selectStyle');

    expect(styleSelect).toBeTruthy();
    expect(styleSelect.textContent).toEqual('default');
  });

  it('should show the select style component with the first style selected', () => {
    render(
      <DemoWrapper>
        <RenderStyles {...defaultProps} />
      </DemoWrapper>,
    );
    const styleSelect = screen.getByTestId('selectStyle');

    expect(styleSelect).toBeTruthy();
    expect(styleSelect.textContent).toEqual(defaultProps.layerStyles[0].name);
  });

  it('should trigger onChangeLayerStyle when a new style is selected', async () => {
    render(
      <DemoWrapper>
        <RenderStyles {...defaultProps} />
      </DemoWrapper>,
    );
    const newStyleName = defaultProps.layerStyles[1].name;
    const styleSelect = screen.getByTestId('selectStyle');

    fireEvent.mouseDown(styleSelect);

    const menuItem = await screen.findByText(newStyleName);
    fireEvent.click(menuItem);

    expect(defaultProps.onChangeLayerStyle).toHaveBeenCalledWith(newStyleName);
    // showing the newStyleName in the select only works when this component is connected to the store, so no checking on that here
  });

  it('should call onChangeLayerStyle on wheel scroll only if the Ctrl key is pressed', async () => {
    render(
      <DemoWrapper>
        <RenderStyles {...defaultProps} />
      </DemoWrapper>,
    );
    const select = screen.getByTestId('selectStyle');

    fireEvent.wheel(select, { ctrlKey: true, deltaY: 1 });
    expect(defaultProps.onChangeLayerStyle).toHaveBeenCalledTimes(1);
    expect(defaultProps.onChangeLayerStyle).toHaveBeenCalledWith(
      defaultProps.layerStyles[1].name,
    );

    fireEvent.wheel(select, { ctrlKey: true, deltaY: -1 });
    expect(defaultProps.onChangeLayerStyle).toHaveBeenCalledTimes(2);
    expect(defaultProps.onChangeLayerStyle).toHaveBeenCalledWith('default');

    fireEvent.wheel(select, { deltaY: -1 });
    expect(defaultProps.onChangeLayerStyle).toHaveBeenCalledTimes(2);
  });

  it('should show the style name in the tooltip', async () => {
    render(
      <DemoWrapper>
        <RenderStyles {...defaultProps} />
      </DemoWrapper>,
    );
    const styleSelect = screen.getByTestId('selectStyle');
    fireEvent.mouseOver(styleSelect);

    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip.textContent).toContain(defaultProps.currentLayerStyle);
  });
  it('should show custom tooltip prefix and dropdown icon', async () => {
    const TestIcon: () => React.ReactElement = () => (
      <span data-testid="testIcon" />
    );

    render(
      <DemoWrapper>
        <RenderStyles
          {...defaultProps}
          tooltipPrefix="Test tooltip:"
          icon={TestIcon}
        />
      </DemoWrapper>,
    );
    expect(screen.getByTestId('testIcon')).toBeTruthy();
    const styleSelect = screen.getByTestId('selectStyle');
    fireEvent.mouseOver(styleSelect);

    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip.textContent).toContain('Test tooltip:');
  });
});
