/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen, act, within } from '@testing-library/react';
import { layerActions, mapActions } from '@opengeoweb/store';

import userEvent from '@testing-library/user-event';
import { translateKeyOutsideComponents } from '../../../../../utils/i18n';
import DimensionSelectConnect from './DimensionSelectConnect';
import { DemoWrapperConnect } from '../../../../Providers/Providers';
import { createMockStore } from '../../../../../store';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/DimensionSelect/DimensionSelectConnect', () => {
  it('should set dimension value', async () => {
    const layerId = 'layerId';
    const mapId = 'mapId';

    const store = createMockStore();

    render(
      <DemoWrapperConnect store={store}>
        <DimensionSelectConnect layerId={layerId} mapId={mapId} />
      </DemoWrapperConnect>,
    );

    // setup store
    const date = `2020-10-20T`;
    const t17 = `${date}17:00:00Z`;
    const t18 = `${date}18:00:00Z`;
    const t19 = `${date}19:00:00Z`;
    const iso8601 = 'ISO8601';
    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(
        layerActions.addLayer({
          mapId,
          layerId,
          origin: 'origin',
          layer: {
            useLatestReferenceTime: true,
            dimensions: [
              {
                name: 'elevation',
                units: 'meters',
                currentValue: '5000',
                values: '1000,5000,9000',
              },
              {
                name: 'reference_time',
                currentValue: t17,
                values: `${t17},${t18},${t19}`,
                minValue: t17,
                maxValue: t19,
                units: iso8601,
              },
            ],
          },
        }),
      );
    });

    const user = userEvent.setup();

    // elevation is shown.
    // change dimension value by clicking.
    // latest option is not available for this dimension
    screen.getByRole('combobox', {
      name: 'dimensionSelectName',
    });
    await user.click(
      screen.getByRole('combobox', {
        name: 'dimensionSelectValue',
      }),
    );
    await user.click(screen.getByRole('option', { name: '9000 meters' }));
    expect(
      screen.queryByRole('option', { name: 'Latest' }),
    ).not.toBeInTheDocument();

    screen.getByRole('combobox', {
      name: 'dimensionSelectValue',
    });

    // change dimension value from 9000 to 5000 by scrolling
    fireEvent.wheel(
      screen.getByRole('combobox', {
        name: 'dimensionSelectValue',
      }),
      { deltaY: -1 },
    );
    expect(
      within(
        screen.getByRole('combobox', {
          name: 'dimensionSelectValue',
        }),
      ).getByText('5000'),
    ).toBeTruthy();

    // change to reference time dimension
    fireEvent.wheel(
      screen.getByRole('combobox', {
        name: 'dimensionSelectName',
      }),
      { deltaY: 1 },
    );
    expect(
      within(
        screen.getByRole('combobox', {
          name: 'dimensionSelectName',
        }),
      ).getByText('reference_time'),
    ).toBeTruthy();

    // tooltip shows current option
    const t17option = `${t17} ${iso8601}`;
    const dimensionValue17 = screen.getByRole('combobox', {
      name: 'dimensionSelectValue',
    });
    await user.hover(dimensionValue17);
    const tooltipPrefix = `${translateKeyOutsideComponents('layermanager-dimensions-title')}: reference_time`;
    await screen.findByRole('tooltip', {
      name: `${tooltipPrefix} ${t17option}`,
    });

    // change to latest option
    // tooltip shows latest available time
    await user.click(dimensionValue17);
    await user.click(screen.getByRole('option', { name: 'Latest' }));
    const dimensionValueLatest = screen.getByRole('combobox', {
      name: 'dimensionSelectValue',
    });
    await user.hover(dimensionValueLatest);
    await screen.findByRole('tooltip', {
      name: `${tooltipPrefix} ${t19} ${iso8601}`,
    });

    // scroll to t19
    const newValue = `${t19} ${iso8601}`;
    fireEvent.wheel(dimensionValueLatest, { deltaY: 1 });
    const dimensionValuet19 = screen.getByRole('combobox', {
      name: 'dimensionSelectValue',
    });
    await user.hover(dimensionValuet19);
    await screen.findByRole('tooltip', {
      name: `${tooltipPrefix} ${newValue}`,
    });
  });
});
