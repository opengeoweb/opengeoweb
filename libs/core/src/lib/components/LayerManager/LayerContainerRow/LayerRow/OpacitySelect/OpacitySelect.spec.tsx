/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  render,
  fireEvent,
  waitFor,
  act,
  screen,
} from '@testing-library/react';
import { defaultDelay } from '@opengeoweb/shared';
import { translateKeyOutsideComponents } from '../../../../../utils/i18n';
import OpacitySelect from './OpacitySelect';
import { DemoWrapper } from '../../../../Providers/Providers';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/OpacitySelect/OpacitySelect', () => {
  const props = {
    currentOpacity: 0.8,
    onLayerChangeOpacity: jest.fn(),
  };

  it('should display passed opacity value', async () => {
    render(
      <DemoWrapper>
        <OpacitySelect {...props} />
      </DemoWrapper>,
    );

    expect(await screen.findByText('80 %')).toBeTruthy();
  });

  it('should call onChangeOpacity on mouseClick', async () => {
    render(
      <DemoWrapper>
        <OpacitySelect {...props} />
      </DemoWrapper>,
    );
    const select = screen.getByTestId('selectOpacity');
    fireEvent.click(select);
    const slider = screen.getByTestId('opacitySlider');
    expect(slider).toBeTruthy();
    // fireEvent.click does not call a function, have to use here mouseDown - mouseUp
    fireEvent.mouseDown(slider, { clientY: 1 });
    fireEvent.mouseUp(slider);
    expect(props.onLayerChangeOpacity).toHaveBeenCalledTimes(1);
    expect(props.onLayerChangeOpacity).toHaveBeenCalledWith(0);

    fireEvent.mouseDown(slider, { clientY: -1 });
    fireEvent.mouseUp(slider);
    expect(props.onLayerChangeOpacity).toHaveBeenCalledTimes(2);
    expect(props.onLayerChangeOpacity).toHaveBeenCalledWith(1);
  });

  it('should call onChangeOpacity on wheel scroll when Ctrl or meta key is pressed', async () => {
    render(
      <DemoWrapper>
        <OpacitySelect {...props} />
      </DemoWrapper>,
    );
    const select = screen.getByTestId('selectOpacity');

    fireEvent.wheel(select, { ctrlKey: true, deltaY: 1 });
    await waitFor(() => {
      expect(props.onLayerChangeOpacity).toHaveBeenCalledTimes(1);
    });
    expect(props.onLayerChangeOpacity).toHaveBeenCalledWith(0.79);

    fireEvent.wheel(select, { metaKey: true, deltaY: -1 });
    await waitFor(() => {
      expect(props.onLayerChangeOpacity).toHaveBeenCalledTimes(2);
    });
    expect(props.onLayerChangeOpacity).toHaveBeenCalledWith(0.81);
  });

  it('should call onChangeOpacity with 10% steps on wheel scroll while alt key is pressed', async () => {
    render(
      <DemoWrapper>
        <OpacitySelect {...props} />
      </DemoWrapper>,
    );
    const select = screen.getByTestId('selectOpacity');

    fireEvent.wheel(select, { altKey: true, ctrlKey: true, deltaY: 1 });
    await waitFor(() => {
      expect(props.onLayerChangeOpacity).toHaveBeenCalledTimes(1);
    });
    const calls1 = props.onLayerChangeOpacity.mock.calls;
    expect(calls1[0][0]).toBeCloseTo(0.7);

    fireEvent.wheel(select, { altKey: true, ctrlKey: true, deltaY: -1 });
    await waitFor(() => {
      expect(props.onLayerChangeOpacity).toHaveBeenCalledTimes(2);
    });
    const calls2 = props.onLayerChangeOpacity.mock.calls;
    expect(calls2[1][0]).toBeCloseTo(0.9);
  });

  it('should have the correct tooltip after default delay', async () => {
    jest.useFakeTimers();
    render(
      <DemoWrapper>
        <OpacitySelect {...props} />
      </DemoWrapper>,
    );
    fireEvent.mouseOver(screen.getByTestId('selectOpacity'));
    act(() => {
      jest.advanceTimersByTime(defaultDelay);
    });
    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip.textContent).toMatch(
      `${Math.round(props.currentOpacity * 100)}  %`,
    );
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should focus on opacity slider after opening dialog', () => {
    jest.useFakeTimers();
    render(
      <DemoWrapper>
        <OpacitySelect {...props} />
      </DemoWrapper>,
    );
    const select = screen.getByTestId('selectOpacity');
    fireEvent.click(select);
    const slider = screen.getByTestId('opacitySlider');
    expect(slider).toBeTruthy();

    expect(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-opacity-title'),
      ),
    ).toBeTruthy();

    // activeElement is a child of slider
    // eslint-disable-next-line testing-library/no-node-access
    expect(document.activeElement?.parentElement?.parentElement).toEqual(
      slider,
    );
  });

  it('should show custom tooltip prefix', async () => {
    jest.useFakeTimers();

    render(
      <DemoWrapper>
        <OpacitySelect {...props} tooltipPrefix="Test prefix:" />
      </DemoWrapper>,
    );
    fireEvent.mouseOver(screen.getByTestId('selectOpacity'));
    act(() => {
      jest.advanceTimersByTime(defaultDelay);
    });
    expect(await screen.findByText(/^Test prefix:.*/)).toBeTruthy();
  });
});
