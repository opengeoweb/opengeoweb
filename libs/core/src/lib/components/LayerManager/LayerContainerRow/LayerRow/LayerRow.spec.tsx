/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { storeTestSettings } from '@opengeoweb/store';
import { translateKeyOutsideComponents } from '../../../../utils/i18n';
import LayerRow from './LayerRow';
import { DemoWrapper } from '../../../Providers/Providers';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/LayerRow', () => {
  const props = {
    mapId: 'mapId_1',
    layer: storeTestSettings.layerWithoutTimeDimension,
    layerId: storeTestSettings.layerWithoutTimeDimension.id,
    onLayerChangeDimension: jest.fn(),
  };

  const user = userEvent.setup();

  it('should render correct component and subcomponents', () => {
    render(
      <DemoWrapper>
        <LayerRow {...props} />
      </DemoWrapper>,
    );
    expect(
      screen.getByTestId(
        `layerRow-${storeTestSettings.layerWithoutTimeDimension.id}`,
      ),
    ).toBeTruthy();
    expect(screen.getAllByTestId('deleteButton')).toBeTruthy();
    expect(screen.getByRole('button', { name: 'layer info' })).toBeTruthy();
  });

  it('should be able to click on a row', async () => {
    const defaultProps = {
      services: storeTestSettings.defaultReduxServices,
      layer: storeTestSettings.layerWithoutTimeDimension,
      mapId: 'map_1',
      layerId: 'test-1',
      onLayerRowClick: jest.fn(),
      onLayerChangeDimension: jest.fn(),
    };

    render(
      <DemoWrapper>
        <LayerRow {...defaultProps} />
      </DemoWrapper>,
    );

    const button = screen.getAllByRole('button', {
      name: translateKeyOutsideComponents('layermanager-layer-leading'),
    })[1];
    await user.click(button);
    await user.click(
      screen.getByRole('menuitem', {
        name: translateKeyOutsideComponents(
          'layermanager-layer-leading-auto-update',
        ),
      }),
    );
    expect(defaultProps.onLayerRowClick).toHaveBeenCalledWith(
      defaultProps.layerId,
    );
  });

  it('should be able to pass a draghandle', async () => {
    const testTitle = 'test draghandle';
    const TestComponent: React.ReactElement = <div>{testTitle}</div>;
    const testProps = {
      ...props,
      dragHandle: TestComponent,
    };
    render(
      <DemoWrapper>
        <LayerRow {...testProps} />
      </DemoWrapper>,
    );
    expect(await screen.findAllByText('test draghandle')).toBeTruthy();
  });

  it('should contain an alert banner if selected layer is missing from service', () => {
    const propsWithMissingLayer = { ...props, isLayerMissing: true };
    render(
      <DemoWrapper>
        <LayerRow {...propsWithMissingLayer} />
      </DemoWrapper>,
    );
    expect(screen.getByRole('alert')).toBeTruthy();
  });

  it('should contain no alert banner if selected layer is not missing from service', () => {
    const propsWithNoMissingLayer = { ...props, isLayerMissing: false };
    render(
      <DemoWrapper>
        <LayerRow {...propsWithNoMissingLayer} />
      </DemoWrapper>,
    );
    expect(screen.queryByRole('alert')).toBeNull();
  });
  it('should render custom ActivateLayer', () => {
    const testId = 'testActivateLayer';
    const testProps = {
      ...props,
      layerActiveLayout: <span data-testid={testId} />,
    };
    render(
      <DemoWrapper>
        <LayerRow {...testProps} />
      </DemoWrapper>,
    );
    expect(screen.getAllByTestId(testId).length).toEqual(2);
  });
  it('should disable ActivateLayer', () => {
    const testId = 'testActivateLayer';
    const testProps = {
      ...props,
      layerActiveLayout: <span data-testid={testId} />,
    };
    render(
      <DemoWrapper>
        <LayerRow {...testProps} disableActivateLayer={true} />
      </DemoWrapper>,
    );
    expect(screen.queryAllByTestId(testId).length).toEqual(0);
  });
  it('should render custom LoadDuration', () => {
    const testId = 'testLoadDuration';
    const testProps = {
      ...props,
      layerLoadDurationLayout: <span data-testid={testId} />,
    };
    render(
      <DemoWrapper>
        <LayerRow {...testProps} />
      </DemoWrapper>,
    );
    expect(screen.getAllByTestId(testId).length).toEqual(2);
  });
});
