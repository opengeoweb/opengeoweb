/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  WMImage,
  WMJSMap,
  WMLayer,
  registerWMJSMap,
  unRegisterWMJSMap,
  webmapUtils,
} from '@opengeoweb/webmap';
import { act, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import LoadDuration, {
  DECIMALS_IN_LABEL,
  DECIMALS_IN_TOOLTIP,
  MAX_DURATION_IN_SECONDS,
  MIN_DURATION_IN_SECONDS,
} from './LoadDuration';
import { layerWithSingleDimensionValue } from '../../../../../utils/defaultTestSettings';
import { createMockStore } from '../../../../../store';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/LoadDuration/LoadDuration', () => {
  let mockDuration: number | undefined;
  let mockMap: WMJSMap;
  let mockImg: WMImage;

  beforeEach(() => {
    mockDuration = undefined;

    mockMap = new WMJSMap(document.createElement('div'));
    registerWMJSMap(mockMap, '1');

    mockImg = {
      getSrc() {
        return 'fake-img.jpeg';
      },
      getLoadDuration() {
        return mockDuration;
      },
    } as WMImage;

    jest
      .spyOn(mockMap.getImageStore(), 'getImageForSrc')
      .mockReturnValue(mockImg);

    const radarLayer = new WMLayer(layerWithSingleDimensionValue);
    radarLayer.parentMap = mockMap;
    radarLayer.setCurrentRequestedGetMapURL(mockImg.getSrc());
    webmapUtils.registerWMLayer(radarLayer, layerWithSingleDimensionValue.id);
  });

  afterEach(() => {
    unRegisterWMJSMap('1');
  });

  function triggerImageLoad(): void {
    mockMap.getListener().triggerEvent('onimagebufferimageload', mockImg);
  }

  it('should show empty result when no load duration is known', async () => {
    const store = createMockStore();

    render(
      <Provider store={store}>
        <LoadDuration layerId={layerWithSingleDimensionValue.id} mapId="1" />
      </Provider>,
    );

    expect(await screen.findByTestId('loadDuration')).toHaveTextContent('-');
  });

  it('should show the load duration rounded down to seconds', async () => {
    const store = createMockStore();

    render(
      <Provider store={store}>
        <LoadDuration layerId={layerWithSingleDimensionValue.id} mapId="1" />
      </Provider>,
    );

    mockDuration = 2400;

    const expectedDuration = (mockDuration / 1000).toFixed(DECIMALS_IN_LABEL);

    await act(async () => {
      triggerImageLoad();
    });

    expect(await screen.findByTestId('loadDuration')).toHaveTextContent(
      `${expectedDuration} s`,
    );
  });

  it('should clamp to MIN_DURATION_IN_SECONDS', async () => {
    const store = createMockStore();

    render(
      <Provider store={store}>
        <LoadDuration layerId={layerWithSingleDimensionValue.id} mapId="1" />
      </Provider>,
    );

    mockDuration = MIN_DURATION_IN_SECONDS * 1e3 - 0.001;

    await act(async () => {
      triggerImageLoad();
    });

    expect(await screen.findByTestId('loadDuration')).toHaveTextContent(
      `< ${MIN_DURATION_IN_SECONDS} s`,
    );
  });

  it('should clamp to MAX_DURATION_IN_SECONDS', async () => {
    const store = createMockStore();

    render(
      <Provider store={store}>
        <LoadDuration layerId={layerWithSingleDimensionValue.id} mapId="1" />
      </Provider>,
    );

    mockDuration = MAX_DURATION_IN_SECONDS * 1e3 + 0.001;

    await act(async () => {
      triggerImageLoad();
    });

    expect(await screen.findByTestId('loadDuration')).toHaveTextContent(
      `> ${MAX_DURATION_IN_SECONDS} s`,
    );
  });

  it('should show the non rounded load duration in the tooltip', async () => {
    const store = createMockStore();

    render(
      <Provider store={store}>
        <LoadDuration layerId={layerWithSingleDimensionValue.id} mapId="1" />
      </Provider>,
    );

    mockDuration = 123;

    const expectedDuration = (mockDuration / 1000).toFixed(DECIMALS_IN_TOOLTIP);

    await act(async () => {
      triggerImageLoad();
    });

    await userEvent.hover(await screen.findByTestId('loadDuration'));

    await screen.findByText(`${expectedDuration} s`);
  });
});
