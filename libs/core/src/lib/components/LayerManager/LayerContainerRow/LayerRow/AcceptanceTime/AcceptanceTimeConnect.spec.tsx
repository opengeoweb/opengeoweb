/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { produce } from 'immer';
import { layerTypes } from '@opengeoweb/store';
import userEvent from '@testing-library/user-event';

import { AcceptanceTimeConnect } from './AcceptanceTimeConnect';
import { DemoWrapperConnect } from '../../../../Providers/Providers';
import { defaultReduxLayerRadarKNMI } from '../../../../../utils/defaultTestSettings';
import { createMockStore } from '../../../../../store';

describe('src/lib/components/LayerManager/LayerContainerRow/LayerRow/AcceptanceTime/AcceptanceTimeConnect', () => {
  it('doesnt show if layer doesnt have time dimension', async () => {
    const layerId = defaultReduxLayerRadarKNMI.id!;
    const layer = produce(
      defaultReduxLayerRadarKNMI,
      (draft: layerTypes.ReduxLayer) => {
        draft.dimensions = [];
      },
    );
    const store = createMockStore({
      layers: {
        availableBaseLayers: undefined!,
        allIds: [layerId],
        byId: { [layerId]: layer },
      },
    });
    render(
      <DemoWrapperConnect store={store}>
        <AcceptanceTimeConnect layerId={layerId} />
      </DemoWrapperConnect>,
    );
    const user = userEvent.setup();

    const noTimeDimension = screen.getByTestId('noTimeDimension');
    await user.hover(noTimeDimension);

    expect(
      await screen.findByRole('tooltip', {
        name: 'Layer has no time dimension',
      }),
    ).toBeTruthy();
  });

  it('changes acceptance time', () => {
    const layerId = defaultReduxLayerRadarKNMI.id!;
    const layer = produce(
      defaultReduxLayerRadarKNMI,
      (draft: layerTypes.ReduxLayer) => {
        draft.acceptanceTimeInMinutes = 60;
      },
    );
    const store = createMockStore({
      layers: {
        availableBaseLayers: undefined!,
        allIds: [layerId],
        byId: { [layerId]: layer },
      },
    });
    render(
      <DemoWrapperConnect store={store}>
        <AcceptanceTimeConnect layerId={layerId} />
      </DemoWrapperConnect>,
    );

    const acceptanceTimeButton = screen.getByTestId('acceptanceTimeButton');
    expect(acceptanceTimeButton).toHaveTextContent('01:00');

    fireEvent.click(acceptanceTimeButton);
    Array.from({ length: 6 }).forEach(() => {
      fireEvent.wheel(acceptanceTimeButton, { ctrlKey: true, deltaY: -1 });
    });

    expect(acceptanceTimeButton).toHaveTextContent('off');
  });
});
