/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { storeTestUtils } from '@opengeoweb/store';
import { defaultReduxLayerRadarColor } from '../../../../../utils/defaultTestSettings';
import OpacitySelectConnect from './OpacitySelectConnect';
import { DemoWrapperConnect } from '../../../../Providers/Providers';
import { createMockStore } from '../../../../../store';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/OpacitySelect/OpacitySelectConnect', () => {
  it('should change opacity of layer', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStore(mockState);
    const props = {
      layerId: layer.id,
      mapId: 'test-1',
    };

    render(
      <DemoWrapperConnect store={store}>
        <OpacitySelectConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(store.getState().layers.byId[layer.id].opacity).toEqual(1);

    const select = screen.getByTestId('selectOpacity');
    fireEvent.click(select);
    const slider = screen.getByTestId('opacitySlider');
    expect(slider).toBeTruthy();

    fireEvent.mouseDown(slider, { clientY: 1 });
    fireEvent.mouseUp(slider);

    expect(store.getState().layers.byId[layer.id].opacity).toEqual(0);
  });
});
