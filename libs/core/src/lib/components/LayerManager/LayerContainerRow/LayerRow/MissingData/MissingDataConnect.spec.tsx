/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { act, render, screen } from '@testing-library/react';
import React from 'react';
import userEvent from '@testing-library/user-event';
import { layerActions, mapActions, serviceActions } from '@opengeoweb/store';
import { dateUtils } from '@opengeoweb/shared';
import { MissingDataConnect } from './MissingDataConnect';
import { DemoWrapperConnect } from '../../../../Providers/Providers';
import { GREEN, ORANGE, RED } from './MissingData';
import { createMockStore } from '../../../../../store';

describe('/components/LayerManager/LayerContainerRow/LayerRow/MissingData/MissingDataConnect', () => {
  const layerTime = new Date(`2000-06-15T08:00:00Z`);

  it('displays correct color and tooltip', async () => {
    const store = createMockStore();
    const mapId = 'mapId';
    const layerId = 'layerId';

    render(
      <DemoWrapperConnect store={store}>
        <MissingDataConnect mapId={mapId} layerId={layerId} />
      </DemoWrapperConnect>,
    );

    // setup store with equal layer and map time
    const serviceId = 'serviceId';
    const serviceUrl = 'serviceUrl';

    const layerName = 'layerName';
    const origin = 'origin';
    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(
        mapActions.mapChangeDimension({
          mapId,
          origin,
          // layer and map time is the same
          dimension: { name: 'time', currentValue: layerTime.toISOString() },
        }),
      );

      store.dispatch(
        layerActions.addLayer({
          mapId,
          layerId,
          origin,
          layer: {
            acceptanceTimeInMinutes: 5,
            service: serviceUrl,
            dimensions: [
              { name: 'time', currentValue: layerTime.toISOString() },
            ],
            name: layerName,
          },
        }),
      );

      store.dispatch(
        serviceActions.serviceSetLayers({
          id: serviceId,
          serviceUrl,
          name: 'serviceName',
          scope: 'system',
          layers: [
            {
              leaf: true,
              name: layerName,
              path: [],
              title: 'layerTitle',
            },
          ],
        }),
      );
    });

    const component = screen.getByTestId('MissingDataColor');
    expect(component).toHaveStyle(GREEN);

    await userEvent.hover(component);
    await screen.findByRole('tooltip', {
      name: /updated/i,
    });

    // change acceptance time to be exact
    act(() => {
      store.dispatch(
        layerActions.layerChangeAcceptanceTime({
          layerId,
          acceptanceTime: 0,
        }),
      );
    });

    expect(component).toHaveStyle(GREEN);

    // change map time to not be the same as layer image time
    act(() => {
      store.dispatch(
        mapActions.mapChangeDimension({
          mapId,
          dimension: {
            name: 'time',
            currentValue: dateUtils
              .add(layerTime, {
                seconds: 1,
              })
              .toISOString(),
          },
          origin,
        }),
      );
    });

    expect(component).toHaveStyle(RED);

    await screen.findByRole('tooltip', {
      name: /out of date/i,
    });

    // change acceptance time back to 5 minutes and
    // change map time to be within layer acceptance time
    act(() => {
      store.dispatch(
        layerActions.layerChangeAcceptanceTime({
          layerId,
          acceptanceTime: 5,
        }),
      );

      store.dispatch(
        mapActions.mapChangeDimension({
          mapId,
          dimension: {
            name: 'time',
            currentValue: dateUtils
              .add(layerTime, {
                minutes: 5,
              })
              .toISOString(),
          },
          origin,
        }),
      );
    });

    expect(component).toHaveStyle(ORANGE);

    await screen.findByRole('tooltip', {
      name: /within the data acceptance window/i,
    });

    // change map time to be outside layer acceptance time
    act(() => {
      store.dispatch(
        mapActions.mapChangeDimension({
          mapId,
          dimension: {
            name: 'time',
            currentValue: dateUtils
              .add(layerTime, {
                minutes: 5,
                seconds: 1,
              })
              .toISOString(),
          },
          origin,
        }),
      );
    });

    expect(component).toHaveStyle(RED);

    // turn off acceptance time
    act(() => {
      store.dispatch(
        layerActions.layerChangeAcceptanceTime({
          layerId,
          acceptanceTime: undefined,
        }),
      );
    });

    expect(component).toHaveStyle(ORANGE);

    // change map time to be same as layer time
    act(() => {
      store.dispatch(
        mapActions.mapChangeDimension({
          mapId,
          dimension: {
            name: 'time',
            currentValue: layerTime.toISOString(),
          },
          origin,
        }),
      );
    });

    expect(component).toHaveStyle(GREEN);

    // remove time dimension from layer
    act(() => {
      store.dispatch(
        layerActions.layerSetDimensions({ layerId, origin, dimensions: [] }),
      );
    });

    expect(component).toHaveStyle(GREEN);

    // remove service
    act(() => {
      store.dispatch(
        serviceActions.mapStoreRemoveService({ id: serviceId, serviceUrl }),
      );
    });

    expect(component).toHaveStyle(RED);
  });
});
