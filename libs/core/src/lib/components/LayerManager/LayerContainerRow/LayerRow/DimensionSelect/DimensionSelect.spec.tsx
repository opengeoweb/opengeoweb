/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, screen, within } from '@testing-library/react';
import { layerWithSingleDimensionValue } from '../../../../../utils/defaultTestSettings';
import { DemoWrapper } from '../../../../Providers/Providers';
import DimensionSelect from './DimensionSelect';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/DimensionSelect/DimensionSelect', () => {
  it('should show custom dropdown icon', async () => {
    const TestIcon: React.FC = () => <span data-testid="testIcon" />;
    const mockProps = {
      layerId: layerWithSingleDimensionValue.id!,
      mapId: 'map_1',
      onLayerChangeDimension: jest.fn(),
      layerDimensions: [
        {
          name: 'reference_time',
          units: 'ISO8601',
          currentValue: '2020-03-13T14:40:00Z',
          values: '2020-03-13T14:40:00Z',
        },
      ],
      icon: TestIcon,
    };

    render(
      <DemoWrapper>
        <DimensionSelect {...mockProps} />
      </DemoWrapper>,
    );

    expect(screen.getAllByTestId('testIcon').length).toEqual(2);
  });

  it('should show custom name for dimension', async () => {
    const mockProps = {
      layerId: layerWithSingleDimensionValue.id!,
      mapId: 'map_1',
      onLayerChangeDimension: jest.fn(),
      layerDimensions: [
        {
          name: 'reference_time',
          units: 'ISO8601',
          currentValue: '2020-03-13T14:40:00Z',
          values: '2020-03-13T14:40:00Z',
        },
      ],
      nameMappings: { reference_time: 'TEST_NAME' },
    };

    render(
      <DemoWrapper>
        <DimensionSelect {...mockProps} />
      </DemoWrapper>,
    );

    expect(screen.getByText('TEST_NAME')).toBeTruthy();
  });

  it('should show custom formatting for dimension value', async () => {
    const mockProps = {
      layerId: layerWithSingleDimensionValue.id!,
      mapId: 'map_1',
      onLayerChangeDimension: jest.fn(),
      layerDimensions: [
        {
          name: 'reference_time',
          units: 'ISO8601',
          currentValue: '2020-03-13T14:40:00Z',
          values: '2020-03-13T14:40:00Z',
        },
      ],
      valueMappings: {
        reference_time: (value: string): string => `${value}_TEST`,
      },
    };

    render(
      <DemoWrapper>
        <DimensionSelect {...mockProps} />
      </DemoWrapper>,
    );

    expect(screen.getByText('2020-03-13T14:40:00Z_TEST')).toBeTruthy();
  });

  it('should show custom latest reference time label', async () => {
    const mockProps = {
      layerId: layerWithSingleDimensionValue.id!,
      mapId: 'map_1',
      onLayerChangeDimension: jest.fn(),
      layerDimensions: [
        {
          name: 'reference_time',
          units: 'ISO8601',
          currentValue: 'Latest',
          values:
            '2020-03-13T14:40:00Z,2020-03-12T14:40:00Z,2020-03-11T14:40:00Z',
        },
      ],
      valueMappings: {
        reference_time: (value: string): string => {
          return value === 'Latest' ? 'Most recent' : value;
        },
      },
      setUseLatestReferenceTime: jest.fn(),
      useLatestReferenceTime: true,
    };

    render(
      <DemoWrapper>
        <DimensionSelect {...mockProps} />
      </DemoWrapper>,
    );

    expect(screen.getByText('Most recent')).toBeTruthy();
  });

  it('should limit number of values to 100 and show most recent first', async () => {
    const zeroPad = (num: number, places: number): string => {
      const zero = places - num.toString().length + 1;
      return Array(+(zero > 0 && zero)).join('0') + num;
    };

    const mockProps = {
      layerId: layerWithSingleDimensionValue.id!,
      mapId: 'map_1',
      onLayerChangeDimension: jest.fn(),
      layerDimensions: [
        {
          name: 'reference_time',
          units: 'ISO8601',
          currentValue: 'Latest',
          values: [...new Array(200)]
            .fill(null)
            .map((_, i) => `timevalue_${zeroPad(i, 3)}`)
            .join(','),
        },
      ],
      valueMappings: {
        reference_time: (value: string): string => {
          return value === 'Latest' ? 'Most recent' : value;
        },
      },
      setUseLatestReferenceTime: jest.fn(),
      useLatestReferenceTime: false,
    };

    render(
      <DemoWrapper>
        <DimensionSelect {...mockProps} />
      </DemoWrapper>,
    );

    const dimensionSelectValueElement = screen.getByTestId(
      'dimensionSelectValue',
    );
    const button = within(dimensionSelectValueElement).getByRole('combobox');
    fireEvent.mouseDown(button);

    const listbox = within(screen.getByRole('presentation')).getByRole(
      'listbox',
    );

    const options = within(listbox).getAllByRole('option');

    const optionValues = options
      .filter((li) => li.getAttribute('data-value'))
      .map((li) => li.getAttribute('data-value'));

    expect(optionValues.length).toBe(101);
    expect(optionValues).toEqual([
      'Latest',
      'timevalue_199',
      'timevalue_198',
      'timevalue_197',
      'timevalue_196',
      'timevalue_195',
      'timevalue_194',
      'timevalue_193',
      'timevalue_192',
      'timevalue_191',
      'timevalue_190',
      'timevalue_189',
      'timevalue_188',
      'timevalue_187',
      'timevalue_186',
      'timevalue_185',
      'timevalue_184',
      'timevalue_183',
      'timevalue_182',
      'timevalue_181',
      'timevalue_180',
      'timevalue_179',
      'timevalue_178',
      'timevalue_177',
      'timevalue_176',
      'timevalue_175',
      'timevalue_174',
      'timevalue_173',
      'timevalue_172',
      'timevalue_171',
      'timevalue_170',
      'timevalue_169',
      'timevalue_168',
      'timevalue_167',
      'timevalue_166',
      'timevalue_165',
      'timevalue_164',
      'timevalue_163',
      'timevalue_162',
      'timevalue_161',
      'timevalue_160',
      'timevalue_159',
      'timevalue_158',
      'timevalue_157',
      'timevalue_156',
      'timevalue_155',
      'timevalue_154',
      'timevalue_153',
      'timevalue_152',
      'timevalue_151',
      'timevalue_150',
      'timevalue_149',
      'timevalue_148',
      'timevalue_147',
      'timevalue_146',
      'timevalue_145',
      'timevalue_144',
      'timevalue_143',
      'timevalue_142',
      'timevalue_141',
      'timevalue_140',
      'timevalue_139',
      'timevalue_138',
      'timevalue_137',
      'timevalue_136',
      'timevalue_135',
      'timevalue_134',
      'timevalue_133',
      'timevalue_132',
      'timevalue_131',
      'timevalue_130',
      'timevalue_129',
      'timevalue_128',
      'timevalue_127',
      'timevalue_126',
      'timevalue_125',
      'timevalue_124',
      'timevalue_123',
      'timevalue_122',
      'timevalue_121',
      'timevalue_120',
      'timevalue_119',
      'timevalue_118',
      'timevalue_117',
      'timevalue_116',
      'timevalue_115',
      'timevalue_114',
      'timevalue_113',
      'timevalue_112',
      'timevalue_111',
      'timevalue_110',
      'timevalue_109',
      'timevalue_108',
      'timevalue_107',
      'timevalue_106',
      'timevalue_105',
      'timevalue_104',
      'timevalue_103',
      'timevalue_102',
      'timevalue_101',
      'timevalue_100',
    ]);
  });
});
