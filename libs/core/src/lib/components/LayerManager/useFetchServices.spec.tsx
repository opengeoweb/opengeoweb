/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { waitFor, renderHook } from '@testing-library/react';
import { produce } from 'immer';
import { mapUtils, serviceTypes, uiTypes } from '@opengeoweb/store';
import {
  setWMSGetCapabilitiesFetcher,
  mockGetCapabilities,
} from '@opengeoweb/webmap';
import {
  getServicesToLoad,
  mergePresetsAndUserAddedServices,
  useFetchServices,
} from './useFetchServices';
import { DemoWrapperConnect } from '../Providers/Providers';
import { preloadedDefaultMapServices } from '../../utils/defaultConfigurations';
import { createMockStore } from '../../store';

describe('src/components/LayerManager/useFetchServices', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });

  const dialogType = uiTypes.DialogTypes.LayerSelect;
  const stateTemplate = {
    webmap: {
      byId: {
        mapIdA: {
          ...mapUtils.createMap({ id: 'mapIdA' }),
        },
      },
      allIds: ['mapIdA'],
    },
    ui: {
      dialogs: {
        layerSelect: {
          type: dialogType,
          activeMapId: 'mapIdA',
          isOpen: true,
          source: 'module' as const,
        },
      },
      order: ['layerSelect'],
    },
  };

  describe('useFetchServices', () => {
    it('should fetch initial services', async () => {
      const mockState = produce(stateTemplate, (draft) => {
        draft.ui.dialogs.layerSelect.isOpen = true;
      });
      const store = createMockStore(mockState);

      renderHook(() => useFetchServices(dialogType), {
        wrapper: ({ children }) => (
          <DemoWrapperConnect store={store}>{children}</DemoWrapperConnect>
        ),
      });

      await waitFor(() => {
        expect(store.getState().services.allIds).toHaveLength(
          preloadedDefaultMapServices!.length,
        );
      });

      const servicesToLoad = getServicesToLoad(
        mergePresetsAndUserAddedServices(
          preloadedDefaultMapServices as serviceTypes.InitialService[],
          {},
        ),
      );

      (await servicesToLoad).forEach((service) => {
        expect(store.getState().services.byId[service.id]).toBeDefined();
      });
    });

    it('should not fetch initial services when layerSelect is closed', async () => {
      const mockState = produce(stateTemplate, (draft) => {
        draft.ui.dialogs.layerSelect.isOpen = false;
      });
      const store = createMockStore(mockState);

      renderHook(() => useFetchServices(dialogType), {
        wrapper: ({ children }) => (
          <DemoWrapperConnect store={store}>{children}</DemoWrapperConnect>
        ),
      });

      await waitFor(() => {
        expect(store.getState().services.allIds).toHaveLength(0);
      });
    });

    it('should not fetch initial services for docked layer manager', async () => {
      const dialogType = uiTypes.DialogTypes.DockedLayerManager;
      const dialogTitle = `${dialogType}-mapIdA`;
      const mockState = {
        webmap: {
          byId: {
            mapIdA: {
              ...mapUtils.createMap({ id: 'mapIdA' }),
            },
          },
          allIds: ['mapIdA'],
        },
        ui: {
          dialogs: {
            [dialogTitle]: {
              type: dialogTitle,
              activeMapId: 'mapIdA',
              isOpen: true,
              source: 'module' as const,
            },
          },
          order: [dialogTitle],
        },
      };
      const store = createMockStore(mockState);

      // mock the WMXMLParser fetch
      const headers = new Headers();
      window.fetch = jest.fn().mockResolvedValueOnce({
        text: () =>
          Promise.resolve(
            mockGetCapabilities.WMS111GetCapabilitiesGeoServicesRADAR,
          ),
        headers,
      });

      renderHook(() => useFetchServices(dialogTitle, undefined, true), {
        wrapper: ({ children }) => (
          <DemoWrapperConnect store={store}>{children}</DemoWrapperConnect>
        ),
      });

      await waitFor(() => {
        expect(store.getState().services.allIds).toHaveLength(0);
      });
    });

    it('should read user-added services from localstorage when opened', async () => {
      const store = createMockStore(stateTemplate);

      // mock the WMXMLParser fetch
      const headers = new Headers();
      window.fetch = jest.fn().mockResolvedValueOnce({
        text: () =>
          Promise.resolve(
            mockGetCapabilities.WMS111GetCapabilitiesGeoServicesRADAR,
          ),
        headers,
      });
      jest.spyOn(Object.getPrototypeOf(window.localStorage), 'getItem');

      const preloadedServices = [
        {
          name: 'Testservice1',
          url: 'https://testservice1/wms?',
          id: 'testservice1',
        },
      ];

      expect(window.localStorage.getItem).not.toHaveBeenCalled();
      renderHook(() => useFetchServices(dialogType, preloadedServices), {
        wrapper: ({ children }) => (
          <DemoWrapperConnect store={store}>{children}</DemoWrapperConnect>
        ),
      });
      await waitFor(() => {
        expect(window.localStorage.getItem).toHaveBeenCalled();
      });
    });

    it('should not fetch initial services when services are in store', async () => {
      const dialogType = uiTypes.DialogTypes.DockedLayerManager;
      const dialogTitle = `${dialogType}-mapIdA`;
      const mockState = {
        webmap: {
          byId: {
            mapIdA: {
              ...mapUtils.createMap({ id: 'mapIdA' }),
            },
          },
          allIds: ['mapIdA'],
        },
        ui: {
          dialogs: {
            [dialogTitle]: {
              type: dialogTitle,
              activeMapId: 'mapIdA',
              isOpen: true,
              source: 'module' as const,
            },
          },
          order: [dialogTitle],
        },
        services: {
          byId: {
            serviceid_1: {
              id: 'serviceid_1',
              name: 'RADAR',
              serviceUrl:
                'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserver?dataset=RADAR',
              scope: 'user' as const,
              abstract:
                'This service demonstrates how the ADAGUC server can be used to create OGC services.',
              layers: [],
            },
            serviceid_18: {
              id: 'serviceid_18',
              name: 'BES Seismic',
              serviceUrl:
                'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserver?dataset=BES_SEISMIC',
              scope: 'system' as const,
              layers: [],
            },
          },
          allIds: ['serviceid_1', 'serviceid_18'],
        },
      };
      const store = createMockStore(mockState);

      expect(store.getState().services.allIds).toHaveLength(2);
      expect(store.getState().services.allIds[0]).toEqual('serviceid_1');
      expect(store.getState().services.allIds[1]).toEqual('serviceid_18');

      renderHook(() => useFetchServices(dialogTitle, undefined, false), {
        wrapper: ({ children }) => (
          <DemoWrapperConnect store={store}>{children}</DemoWrapperConnect>
        ),
      });

      await waitFor(() => {
        expect(store.getState().services.allIds).toHaveLength(2);
      });
      await waitFor(() => {
        expect(store.getState().services.allIds[0]).toEqual('serviceid_1');
      });

      expect(store.getState().services.allIds[1]).toEqual('serviceid_18');
    });
  });
});

describe('LayerManager/mergePresetsAndUserAddedServices', () => {
  const preloadedService1 = {
    name: 'Testservice1',
    url: 'https://testservice1/wms?',
    id: 'testservice1',
  };
  const preloadedService2 = {
    name: 'Testservice2',
    url: 'https://testservice2/wms?',
    id: 'testservice2',
  };
  const preloadedServices: serviceTypes.InitialService[] = [
    preloadedService1,
    preloadedService2,
  ];
  const userAddedService1 = {
    name: 'Customised name for service 1',
    url: 'https://testservice1/wms?',
  };
  const userAddedService2 = {
    name: 'Customised name for service 3',
    url: 'https://testservice3/wms?',
  };
  const userAddedServices: serviceTypes.UserAddedServices = {
    'https://testservice1/wms?': userAddedService1,
    'https://testservice3/wms?': userAddedService2,
  };
  it('should merge duplicate ids by preferring the user-added entry', () => {
    expect(
      mergePresetsAndUserAddedServices(preloadedServices, userAddedServices),
    ).toEqual([userAddedService1, preloadedService2, userAddedService2]);
  });
  it('should return only preloaded services when only preloaded services are given', () => {
    expect(mergePresetsAndUserAddedServices(preloadedServices, {})).toEqual(
      preloadedServices as serviceTypes.NoIdService[],
    );
  });
  it('should return only user-added services when only user-added services are given', () => {
    expect(mergePresetsAndUserAddedServices([], userAddedServices)).toEqual([
      userAddedService1,
      userAddedService2,
    ]);
  });
  it('should return empty list on empty inputs', () => {
    expect(mergePresetsAndUserAddedServices([], {})).toEqual([]);
  });
});
