/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { darkTheme, lightTheme } from '@opengeoweb/theme';

import { publicLayers, MapControls } from '@opengeoweb/webmap-react';
import { useDefaultMapSettings } from '@opengeoweb/webmap-redux';
import { DemoWrapperConnect } from '../../Providers/Providers';
import { MapViewConnect } from '../../MapViewConnect';
import LayerManagerConnect from '../LayerManagerConnect';
import LayerManagerMapButtonConnect from '../LayerManagerMapButtonConnect';
import { TimeSliderConnect } from '../../TimeSliderConnect';
import { createMockStore } from '../../../store';

export default { title: 'components/LayerSelect' };

const store = createMockStore();

interface LayerSelectProps {
  mapId: string;
}

const MapWithLayerSelect: React.FC<LayerSelectProps> = ({
  mapId,
}: LayerSelectProps) => {
  useDefaultMapSettings({
    mapId,
    layers: [
      { ...publicLayers.radarLayer, id: `radar-${mapId}` },
      { ...publicLayers.harmonieWindPl, id: `harmonieWindPl-${mapId}` },
      { ...publicLayers.harmonieAirTemperature, id: `temp-${mapId}` },
    ],
    baseLayers: [
      { ...publicLayers.defaultLayers.baseLayerGrey, id: `baseGrey-${mapId}` },
      publicLayers.defaultLayers.overLayer,
    ],
  });
  return (
    <div style={{ height: '100vh' }}>
      <LayerManagerConnect />
      <MapControls>
        <LayerManagerMapButtonConnect mapId={mapId} />
      </MapControls>

      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          zIndex: 50,
          width: '100%',
        }}
      >
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </div>
      <MapViewConnect mapId={mapId} />
    </div>
  );
};

export const MapWithLayerSelectDemoLightTheme: React.FC = () => (
  <DemoWrapperConnect theme={lightTheme} store={store}>
    <MapWithLayerSelect mapId="mapid_1" />
  </DemoWrapperConnect>
);

export const MapWithLayerSelectDemoDarkTheme: React.FC = () => (
  <DemoWrapperConnect theme={darkTheme} store={store}>
    <MapWithLayerSelect mapId="mapid_1" />
  </DemoWrapperConnect>
);
