/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { LayerType, WMLayer, webmapUtils } from '@opengeoweb/webmap';
import { darkTheme, lightTheme } from '@opengeoweb/theme';

import { storeTestUtils } from '@opengeoweb/store';
import LayerManager from './LayerManager';
import { DemoWrapperConnect } from '../Providers/Providers';
import {
  Size,
  sizeExtraLarge,
  sizeMedium,
  sizeSmall,
} from './LayerManagerHeaderOptions';
import { createMockStore } from '../../store';
import { columnClasses } from './LayerManagerUtils';

export default { title: 'components/LayerManager' };

interface LayerManagerDemoProps {
  mapId: string;
  isLoading?: boolean;
  error?: string;
  size?: Size;
  isDocked?: boolean;
  defaultCollapsedColumns?: Record<string, boolean>;
}

const LayerManagerDemo: React.FC<LayerManagerDemoProps> = ({
  mapId,
  isLoading = false,
  error,
  size,
  isDocked = false,
  defaultCollapsedColumns,
}: LayerManagerDemoProps) => {
  const [isOpen, setIsOpen] = React.useState(true);
  const onClose = (): void => {
    setIsOpen(false);
  };

  return (
    <LayerManager
      mapId={mapId}
      isOpen={isOpen}
      onClose={onClose}
      isLoading={isLoading}
      error={error}
      size={size}
      isDockedLayerManager={isDocked}
      defaultCollapsedColumns={defaultCollapsedColumns}
    />
  );
};

const mapId = 'mapid_1';
const mockBaseLayer = {
  mapId,
  name: 'WorldMap_Light_Grey_Canvas',
  type: 'twms',
  dimensions: [],
  id: 'baseGrey_mapid_1',
  opacity: 1,
  enabled: true,
  layerType: LayerType.baseLayer,
};

const mockRadarLayer = {
  mapId,
  service: 'serviceId',
  name: 'RAD_NL25_PCP_CM',
  format: 'image/png',
  style: 'radar/nearest',
  dimensions: [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2021-12-13T16:00:00Z',
      maxValue: '2021-12-13T16:00:00Z',
      minValue: '2021-03-31T09:25:00Z',
      values: '2021-03-31T09:25:00Z/2021-12-13T16:00:00Z/PT5M',
    },
  ],
  styles: [
    {
      title: 'radar/nearest',
      name: 'radar/nearest',
      legendURL: 'url',
      abstract: 'No abstract available',
    },
    {
      title: 'precip-rainbow/nearest',
      name: 'precip-rainbow/nearest',
      legendURL: 'url',
      abstract: 'No abstract available',
    },
  ],
  id: 'layerid_41',
  opacity: 1,
  enabled: true,
  layerType: LayerType.mapLayer,
};

const mockElevationLayer = {
  mapId,
  service: 'serviceId',
  name: 'ELEVATION_LAYER',
  format: 'image/png',
  style: 'temp',
  dimensions: [
    {
      name: 'elevation',
      units: 'hPa',
      currentValue: '850',
      maxValue: '1000',
      minValue: '200',
      timeInterval: null!,
      values: '200,300,400,500,600,700,800,850,900,925,950,1000',
      synced: false,
    },
  ],
  styles: [
    {
      title: 'Temperature (wow) shaded + contours',
      name: 'temp',
      legendURL: 'url',
      abstract: 'No abstract available',
    },
    {
      title: 'Automatic minimum and maximum',
      name: 'min-max',
      legendURL: 'url',
      abstract: 'No abstract available',
    },
  ],
  id: 'layerid_42',
  opacity: 0.8,
  enabled: false,
  layerType: LayerType.mapLayer,
};

const mockPressureLayer = {
  mapId,
  service: 'serviceId',
  name: 'air_pressure_at_sea_level',
  dimensions: [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2022-03-29T09:00:00Z',
      minValue: '2022-03-29T09:00:00Z',
      maxValue: '2022-03-31T09:00:00Z',
      synced: false,
    },
    {
      name: 'reference_time',
      units: 'ISO8601',
      currentValue: '2022-03-29T09:00:00Z',
      minValue: '2022-03-27T00:00:00Z',
      maxValue: '2022-03-29T09:00:00Z',
      timeInterval: null!,
      synced: false,
    },
  ],
  id: 'layerid_20',
  opacity: 1,
  enabled: true,
  layerType: LayerType.mapLayer,
  style: 'pressure_cwk/contour',
};

const wmMockElevationLayer = new WMLayer(mockElevationLayer);
webmapUtils.registerWMLayer(wmMockElevationLayer, mockElevationLayer.id);

const mockState1 = storeTestUtils.mockStateMapWithMultipleLayers(
  [mockRadarLayer, mockElevationLayer, mockPressureLayer, mockBaseLayer],
  mapId,
);

const mockState = {
  ...mockState1,
  services: {
    byId: {
      serviceId: {
        serviceUrl: mockRadarLayer.service,
        layers: [
          {
            name: 'RAD_NL25_PCP_CM',
            title: 'Precipitation Radar NL',
            leaf: true,
            path: [],
            keywords: [],
            styles: [
              {
                title: 'radar/nearest',
                name: 'radar/nearest',
                legendURL: 'url',
                abstract: 'No abstract available',
              },
              {
                title: 'precip-rainbow/nearest',
                name: 'precip-rainbow/nearest',
                legendURL: 'url',
                abstract: 'No abstract available',
              },
            ],
          },
          {
            name: 'ELEVATION_LAYER',
            title: 'Temperature (PL)',
            leaf: true,
            path: [],
            keywords: [],
            styles: [
              {
                title: 'Temperature (wow) shaded + contours',
                name: 'temp',
                legendURL: 'url',
                abstract: 'No abstract available',
              },
              {
                title: 'Automatic minimum and maximum',
                name: 'min-max',
                legendURL: 'url',
                abstract: 'No abstract available',
              },
            ],
          },
        ],
      },
    },
    allIds: ['serviceId'],
  },
};
const store = createMockStore(mockState);

export const LayerManagerLightTheme = (): React.ReactElement => (
  <DemoWrapperConnect theme={lightTheme} store={store}>
    <LayerManagerDemo mapId={mapId} />
  </DemoWrapperConnect>
);

LayerManagerLightTheme.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60992c1ecde3bf10bec429d2',
    },
  ],
};
LayerManagerLightTheme.tags = ['snapshot'];

export const DockedLayerManagerLightTheme = (): React.ReactElement => (
  <DemoWrapperConnect theme={lightTheme} store={store}>
    <LayerManagerDemo mapId={mapId} isDocked />
  </DemoWrapperConnect>
);

DockedLayerManagerLightTheme.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60992c1ecde3bf10bec429d2',
    },
  ],
};
DockedLayerManagerLightTheme.tags = ['snapshot'];

export const LayerManagerLightThemeSmall = (): React.ReactElement => (
  <DemoWrapperConnect theme={lightTheme} store={store}>
    <LayerManagerDemo mapId={mapId} size={sizeSmall} />
  </DemoWrapperConnect>
);
LayerManagerLightThemeSmall.tags = ['snapshot'];

export const LayerManagerLightThemeMedium = (): React.ReactElement => (
  <DemoWrapperConnect theme={lightTheme} store={store}>
    <LayerManagerDemo mapId={mapId} size={sizeMedium} />
  </DemoWrapperConnect>
);
LayerManagerLightThemeMedium.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/609a3c365f9e6f17e6c558b5/version/635a7ca5b9377332fde3a8e2',
    },
  ],
};
LayerManagerLightThemeMedium.tags = ['snapshot'];

export const LayerManagerDarkTheme = (): React.ReactElement => (
  <DemoWrapperConnect theme={darkTheme} store={store}>
    <LayerManagerDemo mapId={mapId} />
  </DemoWrapperConnect>
);
LayerManagerDarkTheme.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6058ba63e21b5d181e3f01df',
    },
  ],
};
LayerManagerDarkTheme.tags = ['snapshot'];

export const DockedLayerManagerDarkTheme = (): React.ReactElement => (
  <DemoWrapperConnect theme={darkTheme} store={store}>
    <LayerManagerDemo mapId={mapId} isDocked />
  </DemoWrapperConnect>
);
DockedLayerManagerDarkTheme.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6058ba63e21b5d181e3f01df',
    },
  ],
};
DockedLayerManagerDarkTheme.tags = ['snapshot'];

export const LayerManagerDarkThemeSmall = (): React.ReactElement => (
  <DemoWrapperConnect theme={darkTheme} store={store}>
    <LayerManagerDemo mapId={mapId} size={sizeSmall} />
  </DemoWrapperConnect>
);
LayerManagerDarkThemeSmall.tags = ['snapshot'];

export const LayerManagerDarkThemeMedium = (): React.ReactElement => (
  <DemoWrapperConnect theme={darkTheme} store={store}>
    <LayerManagerDemo mapId={mapId} size={sizeMedium} />
  </DemoWrapperConnect>
);
LayerManagerDarkThemeMedium.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6058bae9f62c9412775080ab/version/635a88524be1e77f2606ed7e',
    },
  ],
};
LayerManagerDarkThemeMedium.tags = ['snapshot'];

export const LayerManagerLoadingState = (): React.ReactElement => (
  <DemoWrapperConnect theme={lightTheme} store={store}>
    <LayerManagerDemo mapId={mapId} isLoading={true} />
  </DemoWrapperConnect>
);

export const LayerManagerWithErrorLight = (): React.ReactElement => (
  <DemoWrapperConnect theme={lightTheme} store={store}>
    <LayerManagerDemo
      mapId={mapId}
      error="Preset could not be loaded: Select a different one or try again."
      size={sizeExtraLarge}
    />
  </DemoWrapperConnect>
);
LayerManagerWithErrorLight.tags = ['snapshot'];

export const LayerManagerWithErrorDark = (): React.ReactElement => (
  <DemoWrapperConnect theme={darkTheme} store={store}>
    <LayerManagerDemo
      mapId={mapId}
      error="Preset could not be loaded: Select a different one or try again."
      size={sizeExtraLarge}
    />
  </DemoWrapperConnect>
);

LayerManagerWithErrorDark.tags = ['snapshot'];

export const LayerManagerCollapsedLight = (): React.ReactElement => (
  <DemoWrapperConnect theme={lightTheme} store={store}>
    <LayerManagerDemo
      mapId={mapId}
      defaultCollapsedColumns={{
        [columnClasses.column3]: true,
        [columnClasses.column4]: true,
        [columnClasses.acceptanceTime]: true,
      }}
    />
  </DemoWrapperConnect>
);

LayerManagerCollapsedLight.tags = ['snapshot'];

export const LayerManagerCollapsedDark = (): React.ReactElement => (
  <DemoWrapperConnect theme={darkTheme} store={store}>
    <LayerManagerDemo
      mapId={mapId}
      defaultCollapsedColumns={{
        [columnClasses.column3]: true,
        [columnClasses.column4]: true,
        [columnClasses.acceptanceTime]: true,
      }}
    />
  </DemoWrapperConnect>
);

LayerManagerCollapsedDark.tags = ['snapshot'];
