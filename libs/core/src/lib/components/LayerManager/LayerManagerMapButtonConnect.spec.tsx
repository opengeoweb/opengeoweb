/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { uiTypes } from '@opengeoweb/store';
import LayerManagerMapButtonConnect from './LayerManagerMapButtonConnect';
import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

describe('src/components/LayerManager/LayerManagerMapButtonConnect', () => {
  it('should dispatch action with passed in mapid when clicked', () => {
    const mockState = {
      ui: {
        dialogs: {
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: 'map1',
            isOpen: false,
          },
        },
        order: [uiTypes.DialogTypes.LayerManager],
      },
    };
    const store = createMockStore(mockState);
    const props = {
      mapId: 'mapId_123',
    };
    render(
      <DemoWrapperConnect store={store}>
        <LayerManagerMapButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    const initialResult =
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerManager];
    expect(initialResult.isOpen).toEqual(false);
    expect(initialResult.activeMapId).toEqual('map1');

    // button should be present
    expect(screen.getByTestId('layerManagerButton')).toBeTruthy();

    // close the legend dialog
    fireEvent.click(screen.getByTestId('layerManagerButton'));

    const result =
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerManager];
    expect(result.isOpen).toEqual(true);
    expect(result.activeMapId).toEqual(props.mapId);
  });

  it('should dispatch action to close dialog when dialog is already opened for corresponding map', () => {
    const mockState = {
      ui: {
        dialogs: {
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: 'mapId_123',
            isOpen: true,
            source: 'app' as const,
          },
        },
        order: [uiTypes.DialogTypes.LayerManager],
      },
    };
    const store = createMockStore(mockState);
    const props = {
      mapId: 'mapId_123',
    };
    render(
      <DemoWrapperConnect store={store}>
        <LayerManagerMapButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    const initialResult =
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerManager];
    expect(initialResult.isOpen).toEqual(true);
    expect(initialResult.activeMapId).toEqual('mapId_123');

    // button should be present
    expect(screen.getByTestId('layerManagerButton')).toBeTruthy();

    // close the legend dialog
    fireEvent.click(screen.getByTestId('layerManagerButton'));

    const result =
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerManager];
    expect(result.isOpen).toEqual(false);
    expect(result.activeMapId).toEqual(props.mapId);
  });

  it('should dispatch action with correct mapId when clicked with isMultiMap', () => {
    const mapId1 = 'mapId123';
    const mapId2 = 'mapId1234';

    const dialogLegend1 = `${uiTypes.DialogTypes.LayerManager}-${mapId1}`;
    const dialogLegend2 = `${uiTypes.DialogTypes.LayerManager}-${mapId2}`;
    const mockState = {
      ui: {
        dialogs: {
          layerSelect: {
            type: `${uiTypes.DialogTypes.LayerSelect}-${mapId2}`,
            activeMapId: 'map1',
            isOpen: false,
          },
          [dialogLegend1]: {
            type: dialogLegend1,
            activeMapId: '',
            isOpen: false,
          },
          [dialogLegend2]: {
            type: dialogLegend2,
            activeMapId: '',
            isOpen: false,
          },
        },
        order: [uiTypes.DialogTypes.LayerManager, dialogLegend1, dialogLegend2],
      },
    };
    const store = createMockStore(mockState);

    render(
      <>
        <DemoWrapperConnect store={store}>
          <LayerManagerMapButtonConnect mapId={mapId1} isMultiMap />
        </DemoWrapperConnect>
        <DemoWrapperConnect store={store}>
          <LayerManagerMapButtonConnect mapId={mapId2} isMultiMap />
        </DemoWrapperConnect>
      </>,
    );

    expect(store.getState().ui.dialogs[dialogLegend1].isOpen).toEqual(false);
    expect(store.getState().ui.dialogs[dialogLegend2].isOpen).toEqual(false);

    // button should be present
    for (const button of screen.getAllByTestId('layerManagerButton')) {
      expect(button).toBeTruthy();
      // open the legend dialog
      fireEvent.click(button);
    }

    expect(store.getState().ui.dialogs[dialogLegend1].isOpen).toEqual(true);
    expect(store.getState().ui.dialogs[dialogLegend1].activeMapId).toEqual(
      mapId1,
    );
    expect(store.getState().ui.dialogs[dialogLegend2].isOpen).toEqual(true);
    expect(store.getState().ui.dialogs[dialogLegend2].activeMapId).toEqual(
      mapId2,
    );
  });
});
