/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  render,
  fireEvent,
  waitFor,
  act,
  screen,
} from '@testing-library/react';
import { storeTestUtils } from '@opengeoweb/store';

import {
  mockGetCapabilities,
  setWMSGetCapabilitiesFetcher,
} from '@opengeoweb/webmap';
import AddLayersPopup from './AddLayersPopup';
import {
  defaultReduxLayerRadarColor,
  defaultTestServices,
} from '../../../utils/defaultTestSettings';
import { preloadedDefaultMapServices } from '../../../utils/defaultConfigurations';
import { DemoWrapperConnect } from '../../Providers/Providers';
import { translateKeyOutsideComponents } from '../../../utils/i18n';
import { createMockStore } from '../../../store';

describe('src/components/LayerManager/AddLayersPopup/AddLayersPopup', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });
  const props = {
    open: true,
    handleClose: jest.fn(),
    preloadedServices: defaultTestServices,
  };
  const mapId = 'mapid_1';
  const layer = defaultReduxLayerRadarColor;
  const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
  const store = createMockStore(mockState);

  it('should render correct component', async () => {
    render(
      <DemoWrapperConnect store={store}>
        <AddLayersPopup {...props} />
      </DemoWrapperConnect>,
    );
    await screen.findByTestId('addLayersPopup');
  });

  it('should close the dialog when clicking close button', async () => {
    render(
      <DemoWrapperConnect store={store}>
        <AddLayersPopup {...props} />
      </DemoWrapperConnect>,
    );
    const dialog = screen.getByRole('dialog');
    expect(dialog).toBeTruthy();
    expect(props.handleClose).toHaveBeenCalledTimes(0);
    const closeButton = screen.getByText(
      translateKeyOutsideComponents('actions-close'),
    );
    fireEvent.click(closeButton);
    await waitFor(() => expect(props.handleClose).toHaveBeenCalledTimes(1));
  });

  it('should add a new service to the list when entering a valid service url and pressing the add button', async () => {
    render(
      <DemoWrapperConnect store={store}>
        <AddLayersPopup {...props} />
      </DemoWrapperConnect>,
    );
    // simulate typing a service url in the inputfield
    const inputServiceUrl = screen.getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: mockGetCapabilities.MOCK_URL_WITH_CHILDREN },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
      ),
    );
    // add the service to the list by clicking the add button
    fireEvent.click(screen.getByTestId('add-service'));
    expect(
      await screen.findByText(mockGetCapabilities.mockLayersWithChildren.title),
    ).toBeTruthy();
    // the service should be added to the list in alphabetical order
    const serviceList = screen.getAllByTestId('service-item');
    expect(serviceList[2].textContent).toEqual(
      mockGetCapabilities.mockLayersWithChildren.title,
    );
  });

  it('should add a new service to the list when entering a valid service url and using the enter key', async () => {
    render(
      <DemoWrapperConnect store={store}>
        <AddLayersPopup {...props} />
      </DemoWrapperConnect>,
    );
    // simulate typing a service url in the inputfield
    const inputServiceUrl = screen.getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: mockGetCapabilities.MOCK_URL_WITH_CHILDREN },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
      ),
    );
    // add the service to the list by pressing the Enter key
    fireEvent.keyPress(inputServiceUrl, {
      key: 'Enter',
      code: 'Enter',
      charCode: 13,
    });
    expect(
      await screen.findByText(mockGetCapabilities.mockLayersWithChildren.title),
    ).toBeTruthy();
    // the service should be added to the list in alphabetical order
    const serviceList = screen.getAllByTestId('service-item');
    expect(serviceList[2].textContent).toEqual(
      mockGetCapabilities.mockLayersWithChildren.title,
    );
  });

  it('should add a new service to the list using name if no title available', async () => {
    render(
      <DemoWrapperConnect store={store}>
        <AddLayersPopup {...props} />
      </DemoWrapperConnect>,
    );
    // simulate typing a service url in the inputfield
    const inputServiceUrl = screen.getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: mockGetCapabilities.MOCK_URL_WITH_NO_TITLE },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        mockGetCapabilities.MOCK_URL_WITH_NO_TITLE,
      ),
    );
    // add the service to the list by pressing the Enter key
    fireEvent.keyPress(inputServiceUrl, {
      key: 'Enter',
      code: 'Enter',
      charCode: 13,
    });
    expect(
      await screen.findByText(mockGetCapabilities.mockLayersWithNoTitle.name!),
    ).toBeTruthy();
    // the service should be added to the list in alphabetical order
    const serviceList = screen.getAllByTestId('service-item');
    expect(serviceList[2].textContent).toEqual(
      mockGetCapabilities.mockLayersWithNoTitle.name,
    );
  });

  it('should add a new service to the list using url if no title or name available', async () => {
    render(
      <DemoWrapperConnect store={store}>
        <AddLayersPopup {...props} />
      </DemoWrapperConnect>,
    );
    // simulate typing a service url in the inputfield
    const inputServiceUrl = screen.getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: mockGetCapabilities.MOCK_URL_WITH_NO_TITLE_OR_NAME },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        mockGetCapabilities.MOCK_URL_WITH_NO_TITLE_OR_NAME,
      ),
    );
    // add the service to the list by pressing the Enter key
    fireEvent.keyPress(inputServiceUrl, {
      key: 'Enter',
      code: 'Enter',
      charCode: 13,
    });

    const shortenedURL =
      mockGetCapabilities.MOCK_URL_WITH_NO_TITLE_OR_NAME.substring(
        mockGetCapabilities.MOCK_URL_WITH_NO_TITLE_OR_NAME.indexOf('//') + 2,
        mockGetCapabilities.MOCK_URL_WITH_NO_TITLE_OR_NAME.indexOf('?'),
      );
    expect(await screen.findByText(shortenedURL)).toBeTruthy();
    // the service should be added to the list in alphabetical order
    const serviceList = screen.getAllByTestId('service-item');
    expect(serviceList[2].textContent).toEqual(shortenedURL);
  });

  it('should update the active service after clicking a service', async () => {
    render(
      <DemoWrapperConnect store={store}>
        <AddLayersPopup {...props} />
      </DemoWrapperConnect>,
    );
    const serviceList = await screen.findAllByTestId('service-item');
    // first one should be active
    expect(serviceList[0].getAttribute('class')).toContain('Mui-selected');
    // click the second one
    fireEvent.click(serviceList[1]);
    // now second one should be active
    await waitFor(() =>
      expect(serviceList[1].getAttribute('class')).toContain('Mui-selected'),
    );
  });

  it('should show an error message when the given service url is invalid', async () => {
    render(
      <DemoWrapperConnect store={store}>
        <AddLayersPopup {...props} />
      </DemoWrapperConnect>,
    );
    // simulate typing an invalid service url in the inputfield
    const inputServiceUrl = screen.getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: 'some-test-url' },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual('some-test-url'),
    );
    // try to add the service to the list
    fireEvent.click(screen.getByTestId('add-service'));
    // error message should be displayed
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('wms-loader-error-valid-url'),
      ),
    ).toBeTruthy();
  });

  it('should show an error message when the given service url is not a wms service', async () => {
    render(
      <DemoWrapperConnect store={store}>
        <AddLayersPopup {...props} />
      </DemoWrapperConnect>,
    );
    // simulate typing a service url that is not a wms service
    const inputServiceUrl = screen.getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: mockGetCapabilities.MOCK_URL_INVALID },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        mockGetCapabilities.MOCK_URL_INVALID,
      ),
    );
    // try to add the service to the list
    fireEvent.click(screen.getByTestId('add-service'));
    // error message should be displayed
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('wms-loader-error-valid-service'),
      ),
    ).toBeTruthy();
  });

  it('should set the service as active when trying to add a service to the list twice', async () => {
    render(
      <DemoWrapperConnect store={store}>
        <AddLayersPopup {...props} />
      </DemoWrapperConnect>,
    );
    // add a service to the list
    const inputServiceUrl = screen.getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: mockGetCapabilities.MOCK_URL_WITH_CHILDREN },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
      ),
    );
    fireEvent.click(screen.getByTestId('add-service'));
    expect(
      await screen.findByText(mockGetCapabilities.mockLayersWithChildren.title),
    ).toBeTruthy();
    // check that it is not active yet
    const serviceList = screen.getAllByTestId('service-item');
    expect(serviceList[2].getAttribute('class')).not.toContain('Mui-selected');
    const expectedListLength = serviceList.length;
    // add the same service again
    fireEvent.change(inputServiceUrl, {
      target: { value: mockGetCapabilities.MOCK_URL_WITH_CHILDREN },
    });
    expect(inputServiceUrl.getAttribute('value')).toEqual(
      mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
    );
    fireEvent.click(screen.getByTestId('add-service'));
    // check that it is active now
    expect(serviceList[2].getAttribute('class')).toContain('Mui-selected');
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('add-layer-popup-available-layers', {
          serviceName: serviceList[2].textContent!,
        }),
      ),
    ).toBeTruthy();
    // check that it wasn't added twice
    expect(screen.getAllByTestId('service-item').length).toEqual(
      expectedListLength,
    );
  });

  it('should be able to pass renderProp onRenderTree', async () => {
    const props2 = {
      onRenderTree: jest.fn(),
      open: true,
    };
    render(
      <DemoWrapperConnect store={store}>
        <AddLayersPopup {...props2} />
      </DemoWrapperConnect>,
    );
    expect(props2.onRenderTree).toHaveBeenCalledWith(
      preloadedDefaultMapServices![0],
    );
  });

  it('should use the preselected list of services if none passed in', async () => {
    const props2 = {
      open: true,
      handleClose: jest.fn(),
    };
    render(
      <DemoWrapperConnect store={store}>
        <AddLayersPopup {...props2} />
      </DemoWrapperConnect>,
    );
    await waitFor(async () => {
      const list = await screen.findAllByTestId('service-item');
      expect(list.length).toEqual(preloadedDefaultMapServices!.length);
    });
    for (const layerSelectService of preloadedDefaultMapServices!) {
      expect(screen.getByText(layerSelectService.name)).toBeTruthy();
    }
  });

  it('should use the passed in list of services', async () => {
    render(
      <DemoWrapperConnect store={store}>
        <AddLayersPopup {...props} preloadedServices={defaultTestServices} />
      </DemoWrapperConnect>,
    );
    const list = await screen.findAllByTestId('service-item');
    expect(list.length).toEqual(defaultTestServices.length);
    for (const defaultTestService of defaultTestServices) {
      expect(screen.getByText(defaultTestService.name)).toBeTruthy();
    }
  });

  it('should show the layers of the new service after selecting a new service and the previous service loads successfully in the background', async () => {
    jest.useFakeTimers();
    const services = [
      {
        name: 'Meteo',
        url: mockGetCapabilities.MOCK_URL_WITH_SUBCATEGORY,
        id: 'Meteo',
      },
      {
        name: 'Slow service',
        url: mockGetCapabilities.MOCK_URL_SLOW,
        id: 'SlowService',
      },
    ];

    render(
      <DemoWrapperConnect store={store}>
        <AddLayersPopup {...props} preloadedServices={services} />
      </DemoWrapperConnect>,
    );
    const serviceList = await screen.findAllByTestId('service-item');
    // click the second service, for which loading is slow
    fireEvent.click(serviceList[1]);
    expect(screen.getByRole('progressbar')).toBeTruthy();
    // click the first service, for which loading is fast
    fireEvent.click(serviceList[0]);
    // wait for both services to finish loading
    await act(async () => {
      jest.advanceTimersByTime(500);
    });
    await waitFor(() => expect(screen.queryByRole('progressbar')).toBeFalsy());
    // the content of the layer list should correspond to the first service
    const layerList = screen.getByTestId('layer-list');
    const { children } = mockGetCapabilities.mockLayersWithSubcategory;

    expect(layerList.textContent).toContain(children[0].title);
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should show the layers of the new service after selecting a new service and the previous service gives an error in the background', async () => {
    jest.useFakeTimers();
    const services = [
      {
        name: 'Meteo',
        url: mockGetCapabilities.MOCK_URL_WITH_SUBCATEGORY,
        id: 'Meteo',
      },
      {
        name: 'Slow service',
        url: mockGetCapabilities.MOCK_URL_SLOW_FAILS,
        id: 'SlowService',
      },
    ];

    render(
      <DemoWrapperConnect store={store}>
        <AddLayersPopup {...props} preloadedServices={services} />
      </DemoWrapperConnect>,
    );
    const serviceList = await screen.findAllByTestId('service-item');
    // click the second service, for which loading is slow
    fireEvent.click(serviceList[1]);
    expect(screen.getByRole('progressbar')).toBeTruthy();
    // click the first service, for which loading is fast
    fireEvent.click(serviceList[0]);
    // wait for both services to finish loading
    await act(async () => {
      jest.advanceTimersByTime(500);
    });
    await waitFor(() => expect(screen.queryByRole('progressbar')).toBeFalsy());
    // the content of the layer list should correspond to the first service
    const layerList = screen.getByTestId('layer-list');
    const { children } = mockGetCapabilities.mockLayersWithSubcategory;
    expect(layerList.textContent).toContain(children[0].title);
    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
