/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import * as reactRouterDom from 'react-router-dom';
import { routerUtils } from '@opengeoweb/store';
import { DemoWrapperConnect } from '../Providers/Providers';
import { RouterWrapperConnect } from './RouterWrapperConnect';
import { createMockStore } from '../../store';

jest.mock('react-router-dom', () => ({
  useNavigate: jest.fn(),
}));

describe('src/lib/components/RouterWrapper/RouterWrapperConnect', () => {
  it('should render', () => {
    const mockState = {
      snackbar: {
        entities: {},
        ids: [],
      },
    };
    const store = createMockStore(mockState);
    const testTitle = 'I am a child';
    render(
      <DemoWrapperConnect store={store}>
        <RouterWrapperConnect>
          <div>{testTitle}</div>
        </RouterWrapperConnect>
      </DemoWrapperConnect>,
    );

    expect(screen.getByText(testTitle)).toBeTruthy();

    expect(routerUtils.historyDict.navigate).toEqual(
      reactRouterDom.useNavigate(),
    );
  });
});
