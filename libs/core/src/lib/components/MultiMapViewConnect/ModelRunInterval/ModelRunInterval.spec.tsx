/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React, { ReactElement } from 'react';
import { render, renderHook, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import {
  LayerType,
  mockGetCapabilities,
  setWMSGetCapabilitiesFetcher,
} from '@opengeoweb/webmap';
import { publicLayers } from '@opengeoweb/webmap-react';
import { initialState } from '@opengeoweb/store';
import {
  makeMapPreset,
  ModelRunInterval,
  useHasSyncGroupWithLayerActions,
} from './ModelRunInterval';
import { DemoWrapperConnect } from '../../Providers/Providers';
import { createMockStore } from '../../../store';

interface WrapperProps {
  children: React.ReactNode;
}

const mockOverlay = {
  service: 'http://mock-service-url',
  name: 'overlay_id',
  enabled: true,
  id: 'test-id',
  layerType: LayerType.overLayer,
};

const syncGroupMockStateToTestLayerActions = {
  syncGroups: {
    ...initialState,
    groups: {
      byId: {
        areaLayerGroupA: {
          title: 'areaLayerGroupA',
          payloadByType: {},
          targets: {
            byId: {},
            allIds: [],
          },
          type: 'SYNCGROUPS_TYPE_SETBBOX' as const,
        },
        timeLayerGroupA: {
          title: 'timeLayerGroupA',
          payloadByType: {},
          targets: {
            byId: {},
            allIds: [],
          },
          type: 'SYNCGROUPS_TYPE_SETTIME' as const,
        },
        layerGroupA: {
          title: 'layerGroupA',
          payloadByType: {},
          targets: {
            byId: {},
            allIds: [],
          },
          type: 'SYNCGROUPS_TYPE_SETLAYERACTIONS' as const,
        },
      },
      allIds: ['areaLayerGroupA', 'timeLayerGroupA', 'layerGroupA'],
    },
  },
};

describe('components/MultiMapView/ModelRunInterval', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });
  describe('useHasSyncGroupWithLayerActions', () => {
    it('should return true if a syncgroup of type SYNCGROUPS_TYPE_SETLAYERACTIONS is included', () => {
      const wrapper = ({ children }: WrapperProps): ReactElement => (
        <Provider store={store}>{children}</Provider>
      );
      const store = createMockStore(syncGroupMockStateToTestLayerActions);
      const { result } = renderHook(
        () =>
          useHasSyncGroupWithLayerActions([
            'bla',
            'areaLayerGroupA',
            'timeLayerGroupA',
            'layerGroupA',
            'anotherbla',
          ]),
        { wrapper },
      );

      expect(result.current).toEqual(true);
    });

    it('should return false if a syncgroup of type SYNCGROUPS_TYPE_SETLAYERACTIONS is not included', () => {
      const wrapper = ({ children }: WrapperProps): ReactElement => (
        <Provider store={store}>{children}</Provider>
      );
      const store = createMockStore(syncGroupMockStateToTestLayerActions);
      const { result } = renderHook(
        () =>
          useHasSyncGroupWithLayerActions([
            'bla',
            'areaLayerGroupA',
            'timeLayerGroupA',
            'anotherbla',
          ]),
        { wrapper },
      );

      expect(result.current).toEqual(false);
    });

    it('should return false if a syncgroup store is empty', () => {
      const wrapper = ({ children }: WrapperProps): ReactElement => (
        <Provider store={store}>{children}</Provider>
      );
      const store = createMockStore();
      const { result: resultA } = renderHook(
        () =>
          useHasSyncGroupWithLayerActions([
            'bla',
            'areaLayerGroupA',
            'timeLayerGroupA',
            'anotherbla',
          ]),
        { wrapper },
      );

      expect(resultA.current).toEqual(false);

      const { result: resultB } = renderHook(
        () => useHasSyncGroupWithLayerActions([]),
        { wrapper },
      );

      expect(resultB.current).toEqual(false);

      const { result: resultC } = renderHook(
        () => useHasSyncGroupWithLayerActions(undefined),
        { wrapper },
      );

      expect(resultC.current).toEqual(false);
    });
  });
  describe('makeMapPreset', () => {
    it('should return correct global props if no layers passed in', () => {
      expect(
        makeMapPreset(
          6,
          'unique-id',
          '2020-12-11T12:00:00Z',
          [],
          ['layerGroupA'],
          true,
          true,
        ),
      ).toEqual({
        title: `FC +6`,
        displayTimeInMap: true,
        syncGroupsIds: ['layerGroupA'],
        id: 'unique-id',
        bbox: {
          left: -638936.0386164065,
          bottom: 6068439.661512798,
          right: 1613044.3568348314,
          top: 7876599.103115981,
        },
        displayLayerManagerAndLegendButtonInMap: true,
        displayDimensionSelectButtonInMap: true,
        srs: 'EPSG:3857',
        componentType: 'MultiMap',
        layers: [],
      });
    });
    it('should return correct global props + layer props if one layer passed in', () => {
      expect(
        makeMapPreset(
          6,
          'unique-id',
          '2020-12-11T12:00:00Z',
          [publicLayers.harmoniePressure],
          ['layerGroupA'],
          true,
          true,
        ),
      ).toEqual({
        title: `FC +6`,
        displayTimeInMap: true,
        id: 'unique-id',
        syncGroupsIds: ['layerGroupA'],
        bbox: {
          left: -638936.0386164065,
          bottom: 6068439.661512798,
          right: 1613044.3568348314,
          top: 7876599.103115981,
        },
        displayLayerManagerAndLegendButtonInMap: true,
        displayDimensionSelectButtonInMap: true,
        srs: 'EPSG:3857',
        componentType: 'MultiMap',
        layers: [
          {
            ...publicLayers.harmoniePressure,
            id: 'unique-id-0',
            dimensions: [
              {
                name: 'reference_time',
                currentValue: '2020-12-11T12:00:00Z',
              },
              {
                name: 'time',
                currentValue: '2020-12-11T18:00:00Z',
              },
            ],
          },
        ],
      });
    });
    it('should return correct global props + layer props if multiple layers passed in', () => {
      expect(
        makeMapPreset(
          8,
          'unique-id',
          '2020-12-11T12:00:00Z',
          [
            publicLayers.harmoniePressure,
            publicLayers.harmoniePrecipitation,
            publicLayers.harmonieWindFlags,
          ],
          ['layerGroupA'],
          true,
          true,
        ),
      ).toEqual({
        title: `FC +8`,
        displayTimeInMap: true,
        syncGroupsIds: ['layerGroupA'],
        id: 'unique-id',
        bbox: {
          left: -638936.0386164065,
          bottom: 6068439.661512798,
          right: 1613044.3568348314,
          top: 7876599.103115981,
        },
        displayLayerManagerAndLegendButtonInMap: true,
        displayDimensionSelectButtonInMap: true,
        srs: 'EPSG:3857',
        componentType: 'MultiMap',
        layers: [
          {
            ...publicLayers.harmoniePressure,
            id: 'unique-id-0',
            dimensions: [
              {
                name: 'reference_time',
                currentValue: '2020-12-11T12:00:00Z',
              },
              {
                name: 'time',
                currentValue: '2020-12-11T20:00:00Z',
              },
            ],
          },
          {
            ...publicLayers.harmoniePrecipitation,
            id: 'unique-id-1',
            dimensions: [
              {
                name: 'reference_time',
                currentValue: '2020-12-11T12:00:00Z',
              },
              {
                name: 'time',
                currentValue: '2020-12-11T20:00:00Z',
              },
            ],
          },
          {
            ...publicLayers.harmonieWindFlags,
            id: 'unique-id-2',
            dimensions: [
              {
                name: 'reference_time',
                currentValue: '2020-12-11T12:00:00Z',
              },
              {
                name: 'time',
                currentValue: '2020-12-11T20:00:00Z',
              },
            ],
          },
        ],
      });
    });
    it('should return correct global props + layer props if multiple layers and an overlayer are passed in', () => {
      expect(
        makeMapPreset(
          8,
          'unique-id',
          '2020-12-11T12:00:00Z',
          [
            mockOverlay,
            publicLayers.harmoniePressure,
            publicLayers.harmoniePrecipitation,
            publicLayers.harmonieWindFlags,
          ],
          ['layerGroupA'],
          true,
          true,
        ),
      ).toEqual({
        title: `FC +8`,
        displayTimeInMap: true,
        syncGroupsIds: ['layerGroupA'],
        id: 'unique-id',
        bbox: {
          left: -638936.0386164065,
          bottom: 6068439.661512798,
          right: 1613044.3568348314,
          top: 7876599.103115981,
        },
        displayLayerManagerAndLegendButtonInMap: true,
        displayDimensionSelectButtonInMap: true,
        srs: 'EPSG:3857',
        componentType: 'MultiMap',
        layers: [
          {
            ...mockOverlay,
            id: 'unique-id-0',
            dimensions: [],
          },
          {
            ...publicLayers.harmoniePressure,
            id: 'unique-id-1',
            dimensions: [
              {
                name: 'reference_time',
                currentValue: '2020-12-11T12:00:00Z',
              },
              {
                name: 'time',
                currentValue: '2020-12-11T20:00:00Z',
              },
            ],
          },
          {
            ...publicLayers.harmoniePrecipitation,
            id: 'unique-id-2',
            dimensions: [
              {
                name: 'reference_time',
                currentValue: '2020-12-11T12:00:00Z',
              },
              {
                name: 'time',
                currentValue: '2020-12-11T20:00:00Z',
              },
            ],
          },
          {
            ...publicLayers.harmonieWindFlags,
            id: 'unique-id-3',
            dimensions: [
              {
                name: 'reference_time',
                currentValue: '2020-12-11T12:00:00Z',
              },
              {
                name: 'time',
                currentValue: '2020-12-11T20:00:00Z',
              },
            ],
          },
        ],
      });
    });
  });

  describe('ModelRunInterval', () => {
    const layerWithRefTimes = {
      service: 'https://testservice',
      id: 'refTimesLayer',
      name: 'ref_times_layer',
      title: 'ref_times_layer',
      layerType: LayerType.mapLayer,
      enabled: true,
      dimensions: [
        {
          values: '2022-06-07T06:00:00Z,2022-06-07T05:00:00Z',
          currentValue: '2022-06-07T06:00:00Z',
          name: 'reference_time',
          units: 'ISO8601',
        },
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: '2022-06-07T06:00:00Z',
        },
      ],
      styles: [],
    };

    it('should render 15 maps with default intervals', async () => {
      const store = createMockStore();

      render(
        <DemoWrapperConnect store={store}>
          <ModelRunInterval syncGroupsIds={[]} layers={[layerWithRefTimes]} />
        </DemoWrapperConnect>,
      );
      expect(
        await screen.findByTestId('MultiMapViewSliderConnect'),
      ).toBeTruthy();
      const mapTitles = screen.getAllByTestId('mapTitle');
      expect(mapTitles.length).toEqual(15);
      expect(mapTitles[0].textContent).toEqual('FC +3');
      expect(mapTitles[mapTitles.length - 1].textContent).toEqual('FC +45');
    });

    it('should render 15 maps with custom interval and starting increment', async () => {
      const store = createMockStore();

      render(
        <DemoWrapperConnect store={store}>
          <ModelRunInterval
            syncGroupsIds={[]}
            layers={[layerWithRefTimes]}
            interval={1}
            startTimeIncrement={0}
          />
        </DemoWrapperConnect>,
      );
      expect(
        await screen.findByTestId('MultiMapViewSliderConnect'),
      ).toBeTruthy();
      const mapTitles = screen.getAllByTestId('mapTitle');
      expect(mapTitles.length).toEqual(15);
      expect(mapTitles[0].textContent).toEqual('FC +0');
      expect(mapTitles[mapTitles.length - 1].textContent).toEqual('FC +14');
    });
  });
});
