/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { LayerType, WMJSMap, WMLayer } from '@opengeoweb/webmap';

import {
  HarmonieTempAndPrecipPreset,
  InitialHarmTempAndPrecipProps,
} from './HarmonieTempAndPrecipPreset';
import { DemoWrapperConnect } from '../../Providers/Providers';
import { createMockStore } from '../../../store';

describe('HarmonieTempAndPrecipPreset', () => {
  const defaultProps: InitialHarmTempAndPrecipProps = {
    layers: {
      topRow: {} as WMLayer,
      bottomRow: {} as WMLayer,
      topRowSyncGroups: [],
      bottomRowSyncGroups: [],
    },
    multiLegend: false,
  };

  it('should display an error message when there is an error fetching reference times', async () => {
    render(
      <DemoWrapperConnect store={createMockStore()}>
        <HarmonieTempAndPrecipPreset {...defaultProps} />
      </DemoWrapperConnect>,
    );
    expect(screen.getByText('Loading...')).toBeTruthy();
    expect(
      await screen.findByText(/No Capability element found in service/),
    ).toBeTruthy();
    expect(screen.getByRole('alert')).toBeInTheDocument();
    expect(screen.queryByTestId('MultiMapViewSliderConnect')).toBeFalsy();
  });

  it('should display loading message when reference times are being fetched', async () => {
    const wmLayer1 = new WMLayer({
      id: 'layer-1',
      layerType: LayerType.baseLayer,
      service: 'serviceId',
      name: 'ELEVATION_LAYER',
      format: 'image/png',
      style: 'temp',
      dimensions: [
        {
          name: 'reference_time',
          units: 'ISO8601',
          currentValue: '2022-03-29T09:00:00Z',
          minValue: '2022-03-27T00:00:00Z',
          maxValue: '2022-03-29T09:00:00Z',
          values:
            '2022-03-29T09:00:00Z,' +
            '2022-03-28T00:00:00Z,' +
            '2022-03-28T12:00:00Z,' +
            '2022-03-28T12:00:00Z,' +
            '2022-03-27T00:00:00Z',
          timeInterval: null!,
          synced: false,
        },
      ],
    });
    wmLayer1.setDimension('reference_time', '2022-10-05,2022-10-06');

    jest.spyOn(WMLayer.prototype, 'parseLayer').mockResolvedValue(wmLayer1);

    const wmMap = new WMJSMap(document.createElement('test'));
    wmMap.setDimension('reference_time', '2022-10-05,2022-10-06');

    render(
      <DemoWrapperConnect store={createMockStore()}>
        <HarmonieTempAndPrecipPreset {...defaultProps} />
      </DemoWrapperConnect>,
    );

    expect(screen.getByText('Loading...')).toBeTruthy();

    expect(await screen.findByTestId('MultiMapViewSliderConnect')).toBeTruthy();
  });

  // ...additional tests for other functionalities...
});
