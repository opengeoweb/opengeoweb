/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { publicLayers } from '@opengeoweb/webmap-react';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { useDefaultMapSettings } from '@opengeoweb/webmap-redux';
import { MapViewConnect } from '../MapViewConnect';
import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

const store = createMockStore();

const SearchDialogConnectComp = (): React.ReactElement => {
  const mapId1 = 'mapid_1';

  useDefaultMapSettings({
    mapId: mapId1,
    layers: [{ ...publicLayers.obsAirTemperature, id: `temp-${mapId1}` }],
  });

  return (
    <div style={{ height: '100vh' }}>
      <MapViewConnect
        mapId={mapId1}
        controls={{
          search: true,
          zoomControls: true,
          mapControlsPositionTop: 8,
        }}
      />
    </div>
  );
};

export default {
  title: 'components/SearchDialogConnect',
};

export const SearchDialogConnectLight = (): React.ReactElement => (
  <DemoWrapperConnect store={store} theme={lightTheme}>
    <SearchDialogConnectComp />
  </DemoWrapperConnect>
);

export const SearchDialogConnectDark = (): React.ReactElement => (
  <DemoWrapperConnect store={store} theme={darkTheme}>
    <SearchDialogConnectComp />
  </DemoWrapperConnect>
);
