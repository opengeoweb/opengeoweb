/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { DemoWrapper } from '../Providers/Providers';
import { MyMapLocation } from './MyMapLocation';
import { translateKeyOutsideComponents } from '../../utils/i18n';

describe('src/components/Search/MapLocation', () => {
  it('should find my location', async () => {
    const props = {
      mapId: 'mapId123',
      isSelected: false,
      onUpdateMyLocation: jest.fn(),
    };

    Object.defineProperty(global.navigator, 'geolocation', {
      value: {
        getCurrentPosition: (success: PositionCallback) =>
          success({
            coords: {
              latitude: 1,
              longitude: 2,
            } as GeolocationCoordinates,
          } as GeolocationPosition),
      },
      configurable: true,
    });

    render(
      <DemoWrapper>
        <MyMapLocation {...props} />
      </DemoWrapper>,
    );

    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('search-my-location'),
      }),
    );
    const defaultIcon = screen.getByLabelText('location default');
    expect(defaultIcon).toBeVisible();
    expect(props.onUpdateMyLocation).toHaveBeenCalledWith({
      id: 'My location',
      lat: 1,
      lon: 2,
      name: 'My location',
      source: 'MyMapLocation',
    });
  });

  it('should remove my location', async () => {
    const props = {
      mapId: 'mapId122',
      isSelected: true,
      onUpdateMyLocation: jest.fn(),
    };

    render(
      <DemoWrapper>
        <MyMapLocation {...props} />
      </DemoWrapper>,
    );

    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('search-my-location'),
      }),
    );

    expect(props.onUpdateMyLocation).toHaveBeenCalledWith(undefined);
  });

  it('should show loader while fetching location', async () => {
    const props = {
      mapId: 'mapId124',
      isSelected: false,
      onUpdateMyLocation: jest.fn(),
    };

    Object.defineProperty(global.navigator, 'geolocation', {
      value: {
        getCurrentPosition: jest.fn((success) => {
          setTimeout(() => {
            success({ coords: { latitude: 40.7128, longitude: -74.006 } });
          }, 3000);
        }),
      },
      configurable: true,
    });

    render(
      <DemoWrapper>
        <MyMapLocation {...props} />
      </DemoWrapper>,
    );

    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('search-my-location'),
      }),
    );
    await screen.findByLabelText('Location loading');
  });

  it('should show fail icon if location error occurs', async () => {
    const props = {
      mapId: 'mapId125',
      isSelected: false,
      onUpdateMyLocation: jest.fn(),
    };
    Object.defineProperty(global.navigator, 'geolocation', {
      value: {
        getCurrentPosition: jest.fn().mockImplementation((_success, error) => {
          error({ message: 'get position failed' });
        }),
      },
      configurable: true,
    });

    render(
      <DemoWrapper>
        <MyMapLocation {...props} />
      </DemoWrapper>,
    );

    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('search-my-location'),
      }),
    );
    await screen.findByLabelText('location error');
    expect(props.onUpdateMyLocation).toHaveBeenCalledWith(undefined);
  });
});
