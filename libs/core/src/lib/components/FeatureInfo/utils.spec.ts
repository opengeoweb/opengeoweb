/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as webmapUtils from '@opengeoweb/webmap';

import { LayerType, WMJSMap } from '@opengeoweb/webmap';
import { defaultReduxLayerRadarColor } from '../../utils/defaultTestSettings';

import { getLayersToUpdate } from './utils';

describe('components/GetFeatureInfo/utils', () => {
  describe('getLayersToUpdate', () => {
    const mockLayer = {
      id: 'layer1',
      enabled: true,
      dimensions: [{ name: 'time', currentValue: '2023-01-01T00:00:00Z' }],
      name: 'Layer 1',
      layerType: LayerType.mapLayer,
      title: 'Layer title',
      service: 'https://testservice',
    };
    const mockWmLayer = new webmapUtils.WMLayer(mockLayer);

    const baseElement = document.createElement('div');
    const wmjsmap = new WMJSMap(baseElement);
    const mapId1 = 'mapId12';
    webmapUtils.registerWMJSMap(wmjsmap, mapId1);
    webmapUtils.registerWMLayer(mockWmLayer, mockWmLayer.id);

    beforeEach(() => {
      jest.clearAllMocks();
    });

    it('should return an empty array if wmjsMap is not found', () => {
      const result = getLayersToUpdate([mockLayer], 'map1');
      expect(result).toEqual([]);
    });

    it('should return an empty array if layers are not provided', () => {
      const result = getLayersToUpdate(null!, 'map1');
      expect(result).toEqual([]);
    });

    it('should return an empty array if layers array is empty', () => {
      const result = getLayersToUpdate([], 'map2');
      expect(result).toEqual([]);
    });

    it('should filter out disabled layers', () => {
      const disabledLayer = { ...mockLayer, enabled: false };

      const result = getLayersToUpdate([disabledLayer], 'map1');
      expect(result).toEqual([]);
    });
  });

  describe('getFeatureInfo', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
    it('getLayersToUpdate should return no layers if no layers are defined', () => {
      const layers = getLayersToUpdate([], 'mapId');
      expect(layers).toHaveLength(0);
    });
    it('getLayersToUpdate should return no layers no wmjslayer is registered', async () => {
      const baseElement = document.createElement('div');
      const wmjsmap = new WMJSMap(baseElement);
      const mapId1 = 'mapId1';
      webmapUtils.registerWMJSMap(wmjsmap, mapId1);
      const layers = getLayersToUpdate([defaultReduxLayerRadarColor], 'mapId1');
      expect(layers).toHaveLength(0);
    });
  });
});
