/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import { mapUtils } from '@opengeoweb/store';
import { GetFeatureInfoConnect } from '.';
import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

describe('src/components/GetFeatureInfo/GetFeatureInfoConnect', () => {
  it('should register dialog and set it active', async () => {
    const mockState = {
      webmap: {
        byId: {
          mapId123: {
            ...mapUtils.createMap({ id: 'mapId123' }),
            baseLayers: [],
            mapLayers: [],
          },
        },
        allIds: ['mapId123'],
      },
      ui: {
        dialogs: {
          'getfeatureinfo-mapId123': {
            isOpen: true,
            activeMapId: '',
            type: 'getfeatureinfo-mapId123',
          },
        },
        order: ['getfeatureinfo-mapId123'],
      },
      layers: {
        byId: {},
        allIds: [],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
    };
    const store = createMockStore(mockState);

    expect(store.getState().webmap.byId['mapId123'].displayMapPin).toBeFalsy();
    expect(store.getState().webmap.byId['mapId123'].disableMapPin).toBeFalsy();

    render(
      <DemoWrapperConnect store={store}>
        <GetFeatureInfoConnect mapId="mapId123" />
      </DemoWrapperConnect>,
    );

    expect(store.getState().ui.dialogs['getfeatureinfo-mapId123']).toBeTruthy();
    expect(
      store.getState().ui.dialogs['getfeatureinfo-mapId123'].isOpen,
    ).toEqual(true);
    expect(store.getState().webmap.byId['mapId123'].displayMapPin).toBeTruthy();
    expect(store.getState().webmap.byId['mapId123'].disableMapPin).toBeFalsy();
  });

  it('should register 2 x multiGetFeatureInfos', async () => {
    const mockState = {
      webmap: {
        byId: {
          mapId123: {
            ...mapUtils.createMap({ id: 'mapId123' }),
            baseLayers: [],
            mapLayers: [],
          },
          mapId456: {
            ...mapUtils.createMap({ id: 'mapId456' }),
            baseLayers: [],
            mapLayers: [],
          },
        },
        allIds: ['mapId123', 'mapId456'],
      },
      ui: {
        dialogs: {
          getfeatureinfo: {
            isOpen: true,
            activeMapId: '',
            type: 'getfeatureinfo',
          },
        },
        order: ['getfeatureinfo'],
      },
      layers: {
        byId: {},
        allIds: [],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
    };
    const store = createMockStore(mockState);

    expect(
      store.getState().ui.dialogs['getfeatureinfo-mapId123'],
    ).toBeUndefined();
    expect(
      store.getState().ui.dialogs['getfeatureinfo-mapId456'],
    ).toBeUndefined();
    render(
      <DemoWrapperConnect store={store}>
        <GetFeatureInfoConnect mapId="mapId123" />
        <GetFeatureInfoConnect mapId="mapId456" />
      </DemoWrapperConnect>,
    );

    expect(
      store.getState().ui.dialogs['getfeatureinfo-mapId123'],
    ).toBeDefined();
    expect(
      store.getState().ui.dialogs['getfeatureinfo-mapId456'],
    ).toBeDefined();

    expect(
      store.getState().ui.dialogs['getfeatureinfo-mapId123'].isOpen,
    ).toBeFalsy();
    expect(
      store.getState().ui.dialogs['getfeatureinfo-mapId456'].isOpen,
    ).toBeFalsy();

    expect(store.getState().webmap.byId['mapId123'].displayMapPin).toBeFalsy();
    expect(store.getState().webmap.byId['mapId456'].displayMapPin).toBeFalsy();
  });
});
