/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { uiTypes } from '@opengeoweb/store';
import GetFeatureInfoButtonConnect from './GetFeatureInfoButtonConnect';
import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

describe('src/components/FeatureInfo/GetFeatureInfoButtonConnect', () => {
  it('should dispatch action with passed in mapid when clicked, action should set isOpen to true if currently closed', () => {
    const featureInfoDialogId = 'getfeatureinfo-mapId_123';
    const mockState = {
      ui: {
        dialogs: {
          [featureInfoDialogId]: {
            type: featureInfoDialogId,
            activeMapId: 'map1',
            isOpen: false,
            source: 'app' as const,
          },
        },
        order: [featureInfoDialogId],
      },
    };
    const store = createMockStore(mockState);

    const props = {
      mapId: 'mapId_123',
    };
    render(
      <DemoWrapperConnect store={store}>
        <GetFeatureInfoButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(
      store.getState().ui.dialogs[featureInfoDialogId].activeMapId,
    ).toEqual('map1');
    expect(store.getState().ui.dialogs[featureInfoDialogId].isOpen).toEqual(
      false,
    );

    // button should be present
    expect(screen.getByTestId('open-getfeatureinfo')).toBeTruthy();

    // open the getfeatureinfo dialog
    fireEvent.click(screen.getByTestId('open-getfeatureinfo'));

    expect(
      store.getState().ui.dialogs[featureInfoDialogId].activeMapId,
    ).toEqual(props.mapId);
    expect(store.getState().ui.dialogs[featureInfoDialogId].isOpen).toEqual(
      true,
    );
  });
  it('should dispatch action with passed in mapid when clicked, action should update source and mapId when already open', () => {
    const featureInfoDialogId = 'getfeatureinfo-mapId_123';

    const mockState = {
      ui: {
        dialogs: {
          [featureInfoDialogId]: {
            type: featureInfoDialogId,
            activeMapId: 'map1',
            isOpen: true,
            source: 'app' as const,
          },
        },
        order: [featureInfoDialogId],
      },
    };
    const store = createMockStore(mockState);

    const props = {
      mapId: 'mapId_123',
      source: 'module' as uiTypes.Source,
    };

    expect(
      store.getState().ui.dialogs[featureInfoDialogId].activeMapId,
    ).toEqual('map1');
    expect(store.getState().ui.dialogs[featureInfoDialogId].isOpen).toEqual(
      true,
    );
    expect(store.getState().ui.dialogs[featureInfoDialogId].source).toEqual(
      'app',
    );

    render(
      <DemoWrapperConnect store={store}>
        <GetFeatureInfoButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    // button should be present
    expect(screen.getByTestId('open-getfeatureinfo')).toBeTruthy();

    // open the getfeatureinfo dialog for another map
    fireEvent.click(screen.getByTestId('open-getfeatureinfo'));

    expect(
      store.getState().ui.dialogs[featureInfoDialogId].activeMapId,
    ).toEqual(props.mapId);
    expect(store.getState().ui.dialogs[featureInfoDialogId].isOpen).toEqual(
      false,
    );
    expect(store.getState().ui.dialogs[featureInfoDialogId].source).toEqual(
      props.source,
    );
  });
  it('should dispatch action with passed in mapid when clicked, action should close the dialog when currently open for this map', () => {
    const featureInfoDialogId = 'getfeatureinfo-map3';

    const mockState = {
      ui: {
        dialogs: {
          [featureInfoDialogId]: {
            type: featureInfoDialogId,
            activeMapId: 'map3',
            isOpen: true,
            source: 'app' as const,
          },
        },
        order: [featureInfoDialogId],
      },
    };
    const store = createMockStore(mockState);

    const props = {
      mapId: 'map3',
    };
    render(
      <DemoWrapperConnect store={store}>
        <GetFeatureInfoButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(
      store.getState().ui.dialogs[featureInfoDialogId].activeMapId,
    ).toEqual('map3');
    expect(store.getState().ui.dialogs[featureInfoDialogId].isOpen).toEqual(
      true,
    );
    expect(store.getState().ui.dialogs[featureInfoDialogId].source).toEqual(
      'app',
    );

    // button should be present
    expect(screen.getByTestId('open-getfeatureinfo')).toBeTruthy();

    // close the getfeatureinfo dialog
    fireEvent.click(screen.getByTestId('open-getfeatureinfo'));

    expect(
      store.getState().ui.dialogs[featureInfoDialogId].activeMapId,
    ).toEqual(props.mapId);
    expect(store.getState().ui.dialogs[featureInfoDialogId].isOpen).toEqual(
      false,
    );
    expect(store.getState().ui.dialogs[featureInfoDialogId].source).toEqual(
      'app',
    );
  });
});
