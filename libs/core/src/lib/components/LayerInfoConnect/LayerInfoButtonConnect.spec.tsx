/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';
import { uiTypes } from '@opengeoweb/store';
import { LayerInfoButtonConnect } from './LayerInfoButtonConnect';
import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

const props = {
  serviceName: 'test-service',
  mapId: '124',
  serviceUrl: 'http://test.url',
  layerName: 'test-name',
};

describe('src/components/LayerSelect/layerInfo/LayerInfoButtonConnect', () => {
  it('should render the component', () => {
    const store = createMockStore();

    render(
      <DemoWrapperConnect theme={lightTheme} store={store}>
        <LayerInfoButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    const button = screen.getByRole('button', { name: 'layer info' });
    expect(button).toBeTruthy();
  });

  it('should set active layer and open dialog when dialog is not open yet', () => {
    const mockState = {
      ui: {
        dialogs: {
          layerInfo: {
            type: uiTypes.DialogTypes.LayerInfo,
            activeMapId: '',
            isOpen: false,
            source: 'app' as const,
          },
        },
        order: [uiTypes.DialogTypes.LayerInfo],
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LayerInfoButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerInfo].isOpen,
    ).toBeFalsy();
    expect(store.getState().layers.activeLayerInfo).toBeUndefined();

    const button = screen.getByRole('button', { name: 'layer info' });
    expect(button).toBeTruthy();
    fireEvent.click(button);

    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerInfo].isOpen,
    ).toBeTruthy();
    expect(store.getState().layers.activeLayerInfo).toEqual({
      layerName: props.layerName,
      serviceUrl: props.serviceUrl,
    });
  });

  it('should update the active layer info when dialog is already open for another layer', () => {
    const mockState = {
      ui: {
        dialogs: {
          layerInfo: {
            type: uiTypes.DialogTypes.LayerInfo,
            activeMapId: '',
            isOpen: true,
            source: 'app' as const,
          },
        },
        order: [uiTypes.DialogTypes.LayerInfo],
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LayerInfoButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerInfo].isOpen,
    ).toBeTruthy();
    expect(store.getState().layers.activeLayerInfo).toBeUndefined();

    const button = screen.getByRole('button', { name: 'layer info' });
    expect(button).toBeTruthy();
    fireEvent.click(button);

    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerInfo].isOpen,
    ).toBeTruthy();
    expect(store.getState().layers.activeLayerInfo).toEqual({
      layerName: props.layerName,
      serviceUrl: props.serviceUrl,
    });
  });

  it('should close the dialog when dialog is open for current layer', () => {
    const mockStateUi = {
      dialogs: {
        layerInfo: {
          type: uiTypes.DialogTypes.LayerInfo,
          activeMapId: props.mapId,
          isOpen: true,
          source: 'app' as const,
        },
      },
      order: [uiTypes.DialogTypes.LayerInfo],
    };

    const mockState = {
      ui: mockStateUi,
      layers: {
        byId: {},
        allIds: [],
        activeLayerInfo: {
          serviceUrl: props.serviceUrl,
          layerName: props.layerName,
        },
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LayerInfoButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(store.getState().layers.activeLayerInfo).toBeTruthy();

    const button = screen.getByRole('button', { name: 'layer info' });
    expect(button).toBeTruthy();

    fireEvent.click(button);

    expect(store.getState().layers.activeLayerInfo).toBeUndefined();
  });

  it('should update the active layer info when dialog is open for a layer with the same name but from a different service', () => {
    const mockStateUi = {
      dialogs: {
        layerInfo: {
          type: uiTypes.DialogTypes.LayerInfo,
          activeMapId: '',
          isOpen: true,
          source: 'app' as const,
        },
      },
      order: [uiTypes.DialogTypes.LayerInfo],
    };

    const mockState = {
      ui: mockStateUi,
      layers: {
        byId: {},
        allIds: [],
        activeLayerInfo: {
          serviceUrl: 'other.service.url',
          layerName: props.layerName,
        },
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LayerInfoButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(store.getState().layers.activeLayerInfo).toEqual({
      serviceUrl: 'other.service.url',
      layerName: props.layerName,
    });

    const button = screen.getByRole('button', { name: 'layer info' });
    expect(button).toBeTruthy();
    fireEvent.click(button);

    expect(store.getState().layers.activeLayerInfo).toEqual({
      layerName: props.layerName,
      serviceUrl: props.serviceUrl,
    });
  });
});
