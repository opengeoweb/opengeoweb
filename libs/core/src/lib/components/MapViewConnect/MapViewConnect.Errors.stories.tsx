/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import {
  FormControlLabel,
  FormControl,
  FormHelperText,
  ListItem,
  Checkbox,
  List,
  Paper,
  Grid2 as Grid,
} from '@mui/material';
import {
  layerActions,
  layerSelectors,
  mapSelectors,
  layerTypes,
  CoreAppStore,
} from '@opengeoweb/store';
import { publicLayers } from '@opengeoweb/webmap-react';
import { useDefaultMapSettings } from '@opengeoweb/webmap-redux';
import MapViewConnect from './MapViewConnect';

import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

export default {
  title: 'components/MapViewConnect/MapViewConnect',
  component: MapViewConnect,
};
const store = createMockStore();
const connectLayerControls = connect(
  (state: CoreAppStore, props: { layerId: string }) => ({
    hasError:
      layerSelectors.getLayerStatus(state, props.layerId) ===
      layerTypes.LayerStatus.error,
    name: layerSelectors.getLayerName(state, props.layerId),
    isEnabled: layerSelectors.getLayerEnabled(state, props.layerId),
  }),
  {
    layerChangeEnabled: layerActions.layerChangeEnabled,
  },
);

type Props = ConnectedProps<typeof connectLayerControls> & { layerId: string };

const LayerControl = connectLayerControls(
  ({ layerId, isEnabled, name, layerChangeEnabled, hasError }: Props) => (
    <ListItem dense>
      <Grid container direction="column">
        <Grid
          size={{
            sm: 12,
          }}
        >
          <FormControl error={hasError}>
            <FormControlLabel
              control={
                <Checkbox
                  color="primary"
                  inputProps={{ 'aria-label': 'secondary checkbox' }}
                  checked={isEnabled}
                  disabled={hasError}
                  onChange={(): void => {
                    layerChangeEnabled({ layerId, enabled: !isEnabled });
                  }}
                />
              }
              label={name}
            />
            {hasError ? (
              <FormHelperText>Error loading layer</FormHelperText>
            ) : null}
          </FormControl>
        </Grid>
      </Grid>
    </ListItem>
  ),
);

const connectRedux = connect(
  (state: CoreAppStore, props: { mapId: string }) => ({
    layerIds: mapSelectors.getLayerIds(state, props.mapId),
  }),
);

const MapWithError = connectRedux(
  ({ mapId, layerIds }: { mapId: string; layerIds: string[] }) => {
    useDefaultMapSettings({
      mapId,
      layers: [
        publicLayers.harmoniePressure,
        publicLayers.radarLayerWithError,
        publicLayers.radarLayer,
      ],
    });

    return (
      <Paper>
        <List>
          {layerIds.map((layerId) => (
            <LayerControl key={layerId} layerId={layerId} />
          ))}
        </List>
      </Paper>
    );
  },
);

export const MapError = (): React.ReactElement => (
  <DemoWrapperConnect store={store}>
    <div style={{ height: '100vh' }}>
      <MapViewConnect mapId="mapid_1" />
    </div>
    <div
      style={{
        position: 'absolute',
        left: '50px',
        top: '10px',
        zIndex: 99910000,
      }}
    >
      <MapWithError mapId="mapid_1" />
    </div>
  </DemoWrapperConnect>
);
