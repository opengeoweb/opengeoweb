/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import { layerActions, mapActions, storeTestUtils } from '@opengeoweb/store';
import {
  invalidateWMSGetCapabilities,
  LayerType,
  webmapUtils,
} from '@opengeoweb/webmap';
import { MapViewLayer } from '@opengeoweb/webmap-react';
import { http, HttpResponse } from 'msw';
import { setupServer } from 'msw/node';

import {
  defaultReduxLayerRadarKNMI,
  defaultReduxLayerRadarColor,
  WmdefaultReduxLayerRadarKNMI,
} from '../../utils/defaultTestSettings';
import {
  baseLayerGrey,
  baseLayerWorldMap,
  overLayer,
  overLayer2,
  simplePolygonGeoJSON,
} from '../../utils/testLayers';
import { DemoWrapperConnect } from '../Providers/Providers';
import MapViewConnect from './MapViewConnect';
import { createMockStore } from '../../store';

describe('src/components/MapViewConnect/MapViewConnect', () => {
  it('should use latest reference time', async () => {
    const date = `2023-10-09T`;
    const oldReferenceTime = `${date}09:00:00Z`;
    const oldReferenceTimeEncoded = encodeURIComponent(oldReferenceTime);
    const newReferenceTime = `${date}12:00:00Z`;
    const newReferenceTimeEncoded = encodeURIComponent(newReferenceTime);
    const mapTime = `${date}18:00:00Z`;
    const mapId = 'mapId';

    const mockServiceUrl = 'https://geoservices.knmi.nl/adagucserver';
    const layerName = 'layerName';
    const layerId = 'layerId';
    const REFERENCE_TIME = `reference_time`;

    const capabilities = `
<WMS_Capabilities>
      <Capability>
          <Layer>
              <Layer>
                  <Name>${layerName}</Name>
                  <Dimension name="${REFERENCE_TIME}">
                      ${oldReferenceTime}
                  </Dimension>
              </Layer>
          </Layer>
      </Capability>
  </WMS_Capabilities>
`;

    const capabilitiesNew = `
<WMS_Capabilities>
      <Capability>
          <Layer>
              <Layer>
                  <Name>${layerName}</Name>
                  <Dimension name="${REFERENCE_TIME}">
                      ${newReferenceTime}
                  </Dimension>
              </Layer>
          </Layer>
      </Capability>
  </WMS_Capabilities>
`;
    const server = setupServer(
      http.get(
        mockServiceUrl,
        () => {
          return HttpResponse.xml(capabilities);
        },
        { once: true },
      ),
      http.get(mockServiceUrl, () => {
        return HttpResponse.xml(capabilitiesNew);
      }),
    );
    server.listen();

    // calls to load layer image will show which reference time was used
    const imageLoadCalls = jest
      .spyOn(global.Image.prototype, 'src', 'set')
      .mockImplementation();

    const store = createMockStore();

    // setup store with old reference time
    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(
        mapActions.mapChangeDimension({
          mapId,
          origin: 'origin',
          dimension: { name: 'time', currentValue: mapTime },
        }),
      );

      store.dispatch(
        layerActions.addLayer({
          mapId,
          layerId,
          origin: 'origin',
          layer: {
            name: layerName,
            service: mockServiceUrl,
            dimensions: [
              { name: REFERENCE_TIME, currentValue: oldReferenceTime },
            ],
            useLatestReferenceTime: true,
          },
        }),
      );
    });

    render(
      <DemoWrapperConnect store={store}>
        <MapViewConnect mapId={mapId} shouldAutoFetch={true} />
      </DemoWrapperConnect>,
    );

    // set map size so that layer images are loaded
    const wmMap = webmapUtils.getWMJSMapById(mapId);
    act(() => {
      wmMap!.setSize(10, 10);
    });

    await waitFor(() => {
      expect(imageLoadCalls).toHaveBeenLastCalledWith(
        expect.stringContaining(
          `DIM_REFERENCE_TIME=${oldReferenceTimeEncoded}`,
        ),
      );
    });
    expect(imageLoadCalls).toHaveBeenCalledTimes(1);
    expect(
      store.getState().layers.byId[layerId].dimensions[0].currentValue,
    ).toEqual(oldReferenceTime);

    await invalidateWMSGetCapabilities(mockServiceUrl);
    // change map size to force refetch
    act(() => {
      wmMap!.setSize(11, 11);
    });

    await waitFor(() => {
      expect(imageLoadCalls).toHaveBeenCalledTimes(2);
    });

    await waitFor(() => {
      expect(imageLoadCalls).toHaveBeenLastCalledWith(
        expect.stringContaining(
          `DIM_REFERENCE_TIME=${newReferenceTimeEncoded}`,
        ),
      );
    });

    await waitFor(() => {
      expect(
        store.getState().layers.byId[layerId].dimensions[0].currentValue,
      ).toEqual(newReferenceTime);
    });

    server.close();
  });
  it('should show a layer for each maplayer, baselayer and overlayer', () => {
    const mapId = 'map-1';
    const testLayers = [
      defaultReduxLayerRadarKNMI,
      baseLayerGrey,
      overLayer,
      defaultReduxLayerRadarColor,
      baseLayerWorldMap,
      overLayer2,
    ];
    const mockState = storeTestUtils.mockStateMapWithMultipleLayers(
      testLayers,
      mapId,
    );
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <MapViewConnect mapId={mapId} />
      </DemoWrapperConnect>,
    );

    const layerList = screen.queryAllByTestId('mapViewLayer');

    expect(layerList.length).toEqual(testLayers.length);

    // baselayers should be shown first
    expect(layerList[0].textContent).toContain(baseLayerGrey.id);
    expect(layerList[1].textContent).toContain(baseLayerWorldMap.id);

    // maplayers should be shown next
    expect(layerList[2].textContent).toContain(defaultReduxLayerRadarKNMI.id);
    expect(layerList[3].textContent).toContain(defaultReduxLayerRadarColor.id);

    // overlayers should be shown la
    expect(layerList[4].textContent).toContain(overLayer.id);
    expect(layerList[5].textContent).toContain(overLayer2.id);
  });

  it('should be able to render layers as children', async () => {
    const mapId = 'map-1';
    const featureLayer = {
      id: 'test-id-1',
      layerType: LayerType.featureLayer,
      geojson: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [5.0, 55.0],
                  [4.331914, 55.332644],
                  [3.368817, 55.764314],
                  [2.761908, 54.379261],
                  [3.15576, 52.913554],
                  [2.000002, 51.500002],
                  [3.370001, 51.369722],
                  [3.370527, 51.36867],
                  [3.362223, 51.320002],
                  [3.36389, 51.313608],
                  [3.373613, 51.309999],
                  [3.952501, 51.214441],
                  [4.397501, 51.452776],
                  [5.078611, 51.391665],
                  [5.848333, 51.139444],
                  [5.651667, 50.824717],
                  [6.011797, 50.757273],
                  [5.934168, 51.036386],
                  [6.222223, 51.361666],
                  [5.94639, 51.811663],
                  [6.405001, 51.830828],
                  [7.053095, 52.237764],
                  [7.031389, 52.268885],
                  [7.063612, 52.346109],
                  [7.065557, 52.385828],
                  [7.133055, 52.888887],
                  [7.14218, 52.898244],
                  [7.191667, 53.3],
                  [6.5, 53.666667],
                  [6.500002, 55.000002],
                  [5.0, 55.0],
                ],
              ],
            },
          },
        ],
      } as GeoJSON.FeatureCollection,
    };
    const layers = [
      defaultReduxLayerRadarKNMI,
      baseLayerGrey,
      overLayer,
      featureLayer,
    ];

    webmapUtils.registerWMLayer(
      WmdefaultReduxLayerRadarKNMI,
      defaultReduxLayerRadarKNMI.id!,
    );
    const mockState = storeTestUtils.mockStateMapWithMultipleLayers(
      layers,
      mapId,
    );
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <MapViewConnect mapId={mapId}>
          <MapViewLayer id="test-layer-A" geojson={simplePolygonGeoJSON} />
          <MapViewLayer id="test-layer-B" geojson={simplePolygonGeoJSON} />
        </MapViewConnect>
      </DemoWrapperConnect>,
    );

    const layerList = screen.queryAllByTestId('mapViewLayer');
    const numberOfRenderedChildren = 2;

    expect(layerList.length).toEqual(layers.length + numberOfRenderedChildren);

    // order: baselayers, maplayers, overlayers
    expect(layerList[0].textContent).toContain(baseLayerGrey.id);
    expect(layerList[1].textContent).toContain(defaultReduxLayerRadarKNMI.id);
    expect(layerList[2].textContent).toContain(featureLayer.id);
    expect(layerList[3].textContent).toContain(overLayer.id);

    // child as last
    expect(layerList[4].textContent).toContain('test-layer-A');
    expect(layerList[5].textContent).toContain('test-layer-B');
  });

  describe('linkedFeatures', () => {
    const mapPinLocation = { lon: 5.0, lat: 52.0 };
    const handleFeatureSelect = jest.fn();

    beforeEach(() => {
      handleFeatureSelect.mockClear();
    });

    it('should handle click outside a feature', () => {
      render(
        <div>
          <div
            data-testid="mapViewLayer"
            role="button"
            tabIndex={0}
            onClick={() =>
              handleFeatureSelect('panelId1', undefined, mapPinLocation)
            }
            onKeyPress={(e) => {
              if (e.key === 'Enter' || e.key === ' ') {
                handleFeatureSelect('panelId1', undefined, mapPinLocation);
              }
            }}
          >
            Mock MapViewLayer
          </div>
        </div>,
      );

      const mapViewLayer = screen.getByTestId('mapViewLayer');

      fireEvent.click(mapViewLayer);

      expect(handleFeatureSelect).toHaveBeenCalledTimes(1);

      expect(handleFeatureSelect).toHaveBeenCalledWith(
        'panelId1',
        undefined,
        mapPinLocation,
      );
    });

    it('should handle click on a feature', () => {
      const mockState = storeTestUtils.mockLinkedState();
      const store = createMockStore(mockState);

      render(
        <DemoWrapperConnect store={store}>
          <MapViewConnect
            mapPinLocation={mapPinLocation}
            disableMapPin={false}
            mapId="mapId1"
          >
            <div
              data-testid="mapViewLayer"
              role="button"
              tabIndex={0}
              onClick={() =>
                handleFeatureSelect('mapId1', 'feature1', {
                  lon: 5.0,
                  lat: 52.0,
                })
              }
              onKeyPress={(e) => {
                if (e.key === 'Enter' || e.key === ' ') {
                  handleFeatureSelect('panelId1', undefined, mapPinLocation);
                }
              }}
            >
              Mock MapViewLayer
            </div>
          </MapViewConnect>
        </DemoWrapperConnect>,
      );

      const mapViewLayer = screen.getByTestId('mapViewLayer');

      fireEvent.click(mapViewLayer);

      expect(handleFeatureSelect).toHaveBeenCalledTimes(1);

      expect(handleFeatureSelect).toHaveBeenCalledWith('mapId1', 'feature1', {
        lon: 5.0,
        lat: 52.0,
      });
    });
  });
});
