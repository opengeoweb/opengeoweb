/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  Button,
  Select,
  FormControl,
  InputLabel,
  MenuItem,
  Grid2 as Grid,
  Box,
  SelectChangeEvent,
} from '@mui/material';
import {
  CoreAppStore,
  layerActions,
  layerSelectors,
  layerTypes,
} from '@opengeoweb/store';
import {
  drawPolyStoryStyles,
  MapViewLayer,
  publicLayers,
  simplePolygonGeoJSON,
  useGeoJSON,
} from '@opengeoweb/webmap-react';
import { useDefaultMapSettings, ActionCard } from '@opengeoweb/webmap-redux';
import MapViewConnect from './MapViewConnect';

import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

const store = createMockStore();
export default {
  title: 'components/MapViewConnect/MapViewConnect',
  component: MapViewConnect,
};

interface PresetsConnectProps {
  mapId: string;
}

const PresetsConnect: React.FC<PresetsConnectProps> = ({
  mapId,
}: PresetsConnectProps) => {
  const dispatch = useDispatch();
  const onSetLayers = ({
    layers,
  }: {
    layers: layerTypes.ReduxLayer[];
  }): void => {
    dispatch(layerActions.setLayers({ layers, mapId }));
  };

  return (
    <ActionCard
      name="setLayers"
      exampleLayers={[
        {
          layers: [publicLayers.radarLayer],
          title: 'Radar',
        },
        {
          layers: [
            publicLayers.harmoniePrecipitation,
            publicLayers.harmoniePressure,
          ],
          title: 'Precip + Obs',
        },
        {
          layers: [publicLayers.radarLayer, publicLayers.dwdWarningLayer],
          title: 'Radar + DWD Warnings',
        },
      ]}
      description="sets new layers on a map while removing all current ones"
      onClickBtn={onSetLayers}
    />
  );
};

const Demo: React.FC = () => {
  const mapId = 'mapid_1';
  const layerId = 'layerId123';
  const {
    geojson,
    isInEditMode,
    currentFeatureNrToEdit,
    drawMode,
    changeDrawMode,
    editModes,
    setGeojson,
    setEditMode,
  } = useGeoJSON();

  useDefaultMapSettings({
    mapId,
    bbox: {
      left: -2226405.5515279276,
      bottom: 3505518.443190464,
      right: 5163689.822338379,
      top: 13024282.593224784,
    },
  });

  const selectedFeatureIndex = useSelector((store: CoreAppStore) =>
    layerSelectors.getSelectedFeatureIndex(store, layerId),
  );

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(
      layerActions.addLayer({
        mapId,
        layer: { geojson: simplePolygonGeoJSON },
        layerId,
        origin: 'MapViewConnect.FeatureLayers.stories.tsx',
      }),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Box sx={drawPolyStoryStyles.MapDrawGeoJSONContainer}>
      <Box sx={drawPolyStoryStyles.MapDrawGeoJSONMapContainer}>
        <MapViewConnect mapId={mapId}>
          <MapViewLayer
            id="geojson-layer"
            geojson={geojson}
            isInEditMode={isInEditMode}
            drawMode={drawMode}
            updateGeojson={(
              updatedGeojson: GeoJSON.FeatureCollection,
            ): void => {
              setGeojson(updatedGeojson);
            }}
            exitDrawModeCallback={(): void => {
              setEditMode(!isInEditMode);
            }}
            selectedFeatureIndex={currentFeatureNrToEdit}
          />
        </MapViewConnect>
      </Box>
      <Box sx={drawPolyStoryStyles.MapDrawGeoJSONControlsContainer}>
        <Box p={2}>
          <Grid spacing={2} container>
            <Grid
              size={{
                sm: 12,
              }}
            >
              <FormControl variant="filled" style={{ minWidth: 120 }}>
                <InputLabel id="demo-feature-type">Feature type</InputLabel>
                <Select
                  labelId="demo-feature-type"
                  value={drawMode}
                  onChange={(event: SelectChangeEvent<string>): void => {
                    changeDrawMode(event.target.value);
                  }}
                >
                  {editModes.map((mode) => (
                    <MenuItem key={mode.key} value={mode.key}>
                      {mode.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>

            <Grid
              size={{
                sm: 12,
              }}
            >
              <Button
                variant="contained"
                color="primary"
                onClick={(): void => {
                  setEditMode(!isInEditMode);
                  if (!isInEditMode) {
                    // reset
                    setGeojson({
                      type: 'FeatureCollection',
                      features: [
                        {
                          geometry: { type: 'LineString', coordinates: [] },
                          properties: {
                            stroke: '#66F',
                            'stroke-opacity': '1',
                            'stroke-width': 5,
                          },
                          type: 'Feature',
                        },
                      ],
                    });
                  }
                }}
              >
                {isInEditMode ? 'Finish edit' : 'Start edit'}
              </Button>
            </Grid>
            <Grid
              size={{
                sm: 12,
              }}
            >
              <PresetsConnect mapId={mapId} />
            </Grid>
            {selectedFeatureIndex !== undefined && (
              <Grid
                size={{
                  sm: 12,
                }}
              >
                {JSON.stringify(
                  simplePolygonGeoJSON.features[selectedFeatureIndex],
                )}
              </Grid>
            )}
          </Grid>
        </Box>
      </Box>
    </Box>
  );
};

export const FeatureLayers = (): React.ReactElement => {
  return (
    <DemoWrapperConnect store={store}>
      <Demo />
    </DemoWrapperConnect>
  );
};
