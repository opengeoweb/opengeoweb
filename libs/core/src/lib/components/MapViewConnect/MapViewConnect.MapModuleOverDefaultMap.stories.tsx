/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { connect } from 'react-redux';
import { Button, Paper, DialogContent, Grid2 as Grid } from '@mui/material';
import { defaultLayers, layerActions, mapActions } from '@opengeoweb/store';
import { webmapUtils } from '@opengeoweb/webmap';
import {
  MapControls,
  MapView,
  MapViewLayer,
  publicLayers,
} from '@opengeoweb/webmap-react';

import { PROJECTION } from '@opengeoweb/shared';
import { MapViewConnect } from '.';
import { LegendConnect, LegendMapButtonConnect } from '../LegendConnect';

import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
} from '../LayerManager';

import { DemoWrapperConnect } from '../Providers/Providers';
import {
  MultiDimensionSelectMapButtonsConnect,
  MultiMapDimensionSelectConnect,
} from '../MultiMapDimensionSelectConnect';
import GetFeatureInfoButtonConnect from '../FeatureInfo/GetFeatureInfoButtonConnect';
import { GetFeatureInfoConnect } from '../FeatureInfo';
import { TimeSliderConnect } from '../TimeSliderConnect';
import { createMockStore } from '../../store';

export default {
  title: 'components/MapViewConnect/MapViewConnect',
  component: MapViewConnect,
};

const initialBbox = {
  srs: PROJECTION.EPSG_3857.value,
  bbox: {
    left: -450651.2255879827,
    bottom: 6490531.093143953,
    right: 1428345.8183648037,
    top: 7438773.776232235,
  },
};

const connectRedux = connect(null, {
  setLayers: layerActions.setLayers,
  setBaseLayers: layerActions.setBaseLayers,
  setBbox: mapActions.setBbox,
});

interface MapWithGeoWebLayerSelectProps {
  mapId: string;
  setLayers: typeof layerActions.setLayers;
  setBaseLayers: typeof layerActions.setBaseLayers;
  setBbox: typeof mapActions.setBbox;
}

const GeowebFakeModuleDemo: React.FC<MapWithGeoWebLayerSelectProps> = ({
  mapId,
  setLayers,
  setBaseLayers,
  setBbox,
}: MapWithGeoWebLayerSelectProps) => {
  React.useEffect(() => {
    setLayers({ layers: [publicLayers.radarLayer], mapId });
    setBaseLayers({
      mapId,
      layers: [defaultLayers.baseLayerGrey, defaultLayers.overLayer],
    });
    setBbox({
      bbox: initialBbox.bbox,
      srs: initialBbox.srs,
      mapId,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div style={{ height: '100vh' }}>
      <MapControls>
        <LayerManagerMapButtonConnect mapId={mapId} />
        <LegendMapButtonConnect mapId={mapId} />
        <MultiDimensionSelectMapButtonsConnect mapId={mapId} />
        <GetFeatureInfoButtonConnect mapId={mapId} />
      </MapControls>

      <LegendConnect mapId={mapId} />

      <GetFeatureInfoConnect mapId={mapId} />

      <MultiMapDimensionSelectConnect />
      <MapViewConnect mapId={mapId} displayTimeInMap />
      <LayerManagerConnect />
      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          zIndex: 1000,
          width: '100%',
        }}
      >
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </div>
    </div>
  );
};

const ConnectedGeoweb = connectRedux(GeowebFakeModuleDemo);

interface ModuleProps {
  handleClose: () => void;
}
const FakeModuleDialog: React.FC<ModuleProps> = ({
  handleClose,
}: ModuleProps) => {
  const mapId = React.useRef(`sigmet-${webmapUtils.generateMapId()}`).current;

  return (
    <Paper
      sx={{
        position: 'absolute',
        top: '5%',
        left: '10%',
        maxWidth: '90%',
        maxHeight: '95%',
        display: 'flex',
        flexDirection: 'column',
        overflow: 'hidden',
        zIndex: 650,
      }}
      elevation={24}
    >
      <Button onClick={handleClose} color="secondary" size="medium">
        CLOSE
      </Button>
      <DialogContent
        sx={{
          display: 'flex',
          padding: '20px',
          minHeight: 200,
          minWidth: 400,
          height: '100%',
        }}
      >
        <Grid container>
          <MapView mapId={mapId} srs={initialBbox.srs} bbox={initialBbox.bbox}>
            <MapViewLayer
              {...defaultLayers.baseLayerGrey}
              id={webmapUtils.generateLayerId()}
            />
          </MapView>
        </Grid>
      </DialogContent>
    </Paper>
  );
};

const store = createMockStore();
export const MapModuleOverDefaultMap = (): React.ReactElement => {
  const [showSigmet, setShowSigmet] = React.useState(false);
  const mapId = React.useRef(`app-${webmapUtils.generateMapId()}`).current;
  return (
    <DemoWrapperConnect store={store}>
      <ConnectedGeoweb mapId={mapId} />
      <div
        style={{
          position: 'absolute',
          left: '50px',
          top: '10px',
          zIndex: 51,
        }}
      >
        <Button
          variant="contained"
          color="primary"
          onClick={(): void => setShowSigmet(true)}
        >
          Open Module
        </Button>
      </div>
      {showSigmet && (
        <FakeModuleDialog handleClose={(): void => setShowSigmet(false)} />
      )}
    </DemoWrapperConnect>
  );
};
