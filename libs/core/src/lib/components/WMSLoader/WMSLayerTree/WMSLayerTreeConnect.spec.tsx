/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';

import { serviceTypes, storeTestUtils } from '@opengeoweb/store';
import * as webmap from '@opengeoweb/webmap';
import WMSLayerTreeConnect, {
  WMSLayerTreeConnectProps,
} from './WMSLayerTreeConnect';
import { defaultReduxLayerRadarColor } from '../../../utils/defaultTestSettings';
import { DemoWrapperConnect } from '../../Providers/Providers';
import { createMockStore } from '../../../store';

const { mockGetCapabilities, webmapUtils, LayerType } = webmap;

describe('src/components/WMSLoader/WMSLayerTree/WMSLayerTreeConnect', () => {
  jest
    .spyOn(webmap, 'queryWMSLayersTree')
    .mockImplementation(mockGetCapabilities.mockGetLayersFromService);

  it('should add a layer', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);

    const store = createMockStore(mockState);

    const mockProps: WMSLayerTreeConnectProps = {
      service: {
        url: mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
      } as serviceTypes.InitialService,
      mapId,
    };

    const newGeneratedLayerId = 'layer_2';
    jest
      .spyOn(webmapUtils, 'generateLayerId')
      .mockReturnValueOnce(newGeneratedLayerId);

    render(
      <DemoWrapperConnect store={store}>
        <WMSLayerTreeConnect {...mockProps} />
      </DemoWrapperConnect>,
    );

    expect(store.getState().layers.byId[newGeneratedLayerId]).toBeUndefined();

    const list = await screen.findAllByTestId('selectableLayer');
    expect(list).toBeTruthy();

    fireEvent.click(list[0]);

    const result = store.getState().layers.byId[newGeneratedLayerId];
    expect(result).toBeDefined();
    expect(result.id).toEqual(newGeneratedLayerId);
    expect(result.layerType).toEqual(layer.layerType);
    expect(result.name).toEqual(layer.name);
    expect(result.service).toEqual(mockGetCapabilities.MOCK_URL_WITH_CHILDREN);
  });

  it('should add a baselayer if layerType baselayer is passed', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);

    const store = createMockStore(mockState);

    const mockProps = {
      service: {
        url: mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
      },
      mapId,
      layerType: LayerType.baseLayer,
    } as WMSLayerTreeConnectProps;
    render(
      <DemoWrapperConnect store={store}>
        <WMSLayerTreeConnect {...mockProps} />
      </DemoWrapperConnect>,
    );

    const newGeneratedLayerId = 'layer_2';
    jest
      .spyOn(webmapUtils, 'generateLayerId')
      .mockReturnValueOnce(newGeneratedLayerId)
      .mockReturnValueOnce(newGeneratedLayerId);

    expect(store.getState().layers.byId[newGeneratedLayerId]).toBeUndefined();

    const initialState = store.getState().layers;
    expect(initialState.byId[newGeneratedLayerId]).toBeUndefined();
    expect(initialState.availableBaseLayers.allIds).toHaveLength(0);

    const list = await screen.findAllByTestId('selectableLayer');
    expect(list).toBeTruthy();

    fireEvent.click(list[0]);

    const result = store.getState().layers;

    expect(result.byId[newGeneratedLayerId]).toBeDefined();
    expect(result.byId[newGeneratedLayerId].id).toEqual(newGeneratedLayerId);
    expect(result.byId[newGeneratedLayerId].layerType).toEqual(
      LayerType.baseLayer,
    );
    expect(result.byId[newGeneratedLayerId].name).toEqual(layer.name);
    expect(result.byId[newGeneratedLayerId].service).toEqual(
      mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
    );

    expect(result.availableBaseLayers.allIds).toHaveLength(1);
    expect(result.availableBaseLayers.byId[newGeneratedLayerId]).toBeDefined();
    expect(result.availableBaseLayers.byId[newGeneratedLayerId].name).toEqual(
      layer.name,
    );
    expect(
      result.availableBaseLayers.byId[newGeneratedLayerId].service,
    ).toEqual(mockGetCapabilities.MOCK_URL_WITH_CHILDREN);
  });
});
