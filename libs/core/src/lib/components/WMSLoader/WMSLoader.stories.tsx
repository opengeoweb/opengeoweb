/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { connect } from 'react-redux';
import { Typography, Paper, Grid2 as Grid } from '@mui/material';

import { mapActions, layerActions, defaultLayers } from '@opengeoweb/store';
import { LayerType } from '@opengeoweb/webmap';
import { publicServices } from '@opengeoweb/webmap-react';
import { initialBbox } from '@opengeoweb/webmap-redux';
import { MapViewConnect } from '../MapViewConnect';

import WMSLoaderConnect from './WMSLoaderConnect';

import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

export default { title: 'components/WMSLoader' };
const store = createMockStore();
const services = [publicServices.MeteoCanada, publicServices.KNMImsgcpp];
interface WMSLoadProps {
  setBbox: typeof mapActions.setBbox;
  setBaseLayers: typeof layerActions.setBaseLayers;
}

const connectRedux = connect(null, {
  setLayers: layerActions.setLayers,
  setBbox: mapActions.setBbox,
  setBaseLayers: layerActions.setBaseLayers,
});

// --- example 1
const WMSLoad = connectRedux(({ setBbox, setBaseLayers }: WMSLoadProps) => {
  React.useEffect(() => {
    setBbox({
      bbox: initialBbox.bbox,
      srs: initialBbox.srs,
      mapId: 'mapid_1',
    });
    setBaseLayers({
      mapId: 'mapid_1',
      layers: [defaultLayers.baseLayerGrey, defaultLayers.overLayer],
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div>
      <DemoWrapperConnect store={store}>
        <div style={{ height: '100vh' }}>
          <MapViewConnect mapId="mapid_1" />
        </div>
        <div
          style={{
            position: 'absolute',
            left: '50px',
            top: '20px',
            zIndex: 10000,
            right: '200px',
          }}
        >
          <Paper
            style={{
              padding: '5px',
              width: '400px',
            }}
          >
            <Grid container>
              <Grid size={6}>
                <Typography>Load WMS Layer:</Typography>
                <WMSLoaderConnect mapId="mapid_1" />
              </Grid>
              <Grid size={6}>
                <Typography>Load WMS BaseLayer:</Typography>
                <WMSLoaderConnect
                  mapId="mapid_1"
                  layerType={LayerType.baseLayer}
                />
              </Grid>
              <Grid size={12}>
                <Typography>
                  Load WMS Layer with preconfigured services:
                </Typography>
                <WMSLoaderConnect
                  mapId="mapid_1"
                  preloadedServices={services}
                />
              </Grid>
            </Grid>
          </Paper>
        </div>
      </DemoWrapperConnect>
    </div>
  );
});

export const DemoWMSLoad: React.FC = () => (
  <DemoWrapperConnect store={store}>
    <WMSLoad />
  </DemoWrapperConnect>
);
