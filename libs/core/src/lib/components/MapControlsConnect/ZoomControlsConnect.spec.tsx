/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { webmapUtils } from '@opengeoweb/webmap';
import { mapUtils } from '@opengeoweb/store';
import ZoomControlConnect from './ZoomControlsConnect';
import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

describe('src/components/ZoomControls/ZoomControlsConnect', () => {
  it('should render zoom controls', () => {
    const mapId = 'test-1';
    const props = {
      mapId,
    };
    const store = createMockStore();
    const spy = jest
      .spyOn(webmapUtils, 'getWMJSMapById')
      .mockImplementation(jest.fn());

    render(
      <DemoWrapperConnect store={store}>
        <ZoomControlConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(spy).toHaveBeenCalledWith(props.mapId);

    expect(screen.getByTestId('zoom-reset')).toBeTruthy();
    expect(screen.getByTestId('zoom-in')).toBeTruthy();
    expect(screen.getByTestId('zoom-out')).toBeTruthy();
  });

  it('should render zoom controls if visible in store', () => {
    const mapId = 'test-1';
    const props = {
      mapId,
    };
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            ...mapUtils.createMap({ id: mapId }),
            shouldShowZoomControls: true,
            mapLayers: [],
            baseLayers: [],
            overLayers: [],
            dimensions: [],
          },
        },
        allIds: [mapId],
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <ZoomControlConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(screen.getByTestId('zoom-reset')).toBeTruthy();
    expect(screen.getByTestId('zoom-in')).toBeTruthy();
    expect(screen.getByTestId('zoom-out')).toBeTruthy();
  });

  it('should hide zoom controls if not visible in store', () => {
    const mapId = 'test-1';
    const props = {
      mapId,
    };
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            ...mapUtils.createMap({ id: mapId }),
            shouldShowZoomControls: false,
            mapLayers: [],
            baseLayers: [],
            overLayers: [],
            dimensions: [],
          },
        },
        allIds: [mapId],
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <ZoomControlConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(screen.queryByTestId('zoom-reset')).toBeFalsy();
    expect(screen.queryByTestId('zoom-in')).toBeFalsy();
    expect(screen.queryByTestId('zoom-out')).toBeFalsy();
  });
});
