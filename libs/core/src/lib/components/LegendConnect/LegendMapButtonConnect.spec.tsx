/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { uiTypes } from '@opengeoweb/store';
import LegendMapButtonConnect from './LegendMapButtonConnect';
import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

describe('src/components/Legend/LegendMapButtonConnect', () => {
  it('should dispatch action with passed in mapid when clicked, action should set isOpen to true if currently closed', () => {
    const mockState = {
      ui: {
        dialogs: {
          legend: {
            activeMapId: 'map1',
            isOpen: false,
            type: 'legend' as const,
            source: 'app' as const,
          },
        },
        order: ['legend'],
      },
    };
    const store = createMockStore(mockState);
    const props = {
      mapId: 'mapId_123',
    };
    render(
      <DemoWrapperConnect store={store}>
        <LegendMapButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    const initialState = store.getState().ui.dialogs.legend;
    expect(initialState.isOpen).toBeFalsy();
    expect(initialState.activeMapId).toEqual('map1');

    // button should be present
    expect(screen.getByTestId('open-Legend')).toBeTruthy();

    // open the legend dialog
    fireEvent.click(screen.getByTestId('open-Legend'));

    const result = store.getState().ui.dialogs.legend;
    expect(result.isOpen).toBeTruthy();
    expect(result.activeMapId).toEqual(props.mapId);
  });
  it('should dispatch action with passed in mapid when clicked, action should update source and mapId when already open', () => {
    const mockState = {
      ui: {
        dialogs: {
          legend: {
            type: 'legend' as const,
            activeMapId: 'map1',
            isOpen: true,
            source: 'app' as const,
          },
        },
        order: ['legend'],
      },
    };
    const store = createMockStore(mockState);
    const props = {
      mapId: 'mapId_123',
      source: 'module' as uiTypes.Source,
    };
    render(
      <DemoWrapperConnect store={store}>
        <LegendMapButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    const initialState = store.getState().ui.dialogs.legend;
    expect(initialState.isOpen).toBeTruthy();
    expect(initialState.activeMapId).toEqual('map1');
    expect(initialState.source).toEqual('app');

    // button should be present
    expect(screen.getByTestId('open-Legend')).toBeTruthy();

    // open the legend dialog for another map
    fireEvent.click(screen.getByTestId('open-Legend'));

    const result = store.getState().ui.dialogs.legend;
    expect(result.isOpen).toBeTruthy();
    expect(result.activeMapId).toEqual(props.mapId);
    expect(result.source).toEqual(props.source);
  });
  it('should dispatch action with passed in mapid when clicked, action should close the dialog when currently open for this map', () => {
    const mockState = {
      ui: {
        dialogs: {
          legend: {
            type: 'legend' as const,
            activeMapId: 'map3',
            isOpen: true,
            source: 'app' as const,
          },
        },
        order: ['legend'],
      },
    };
    const store = createMockStore(mockState);
    const props = {
      mapId: 'map3',
    };
    render(
      <DemoWrapperConnect store={store}>
        <LegendMapButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    const initialState = store.getState().ui.dialogs.legend;
    expect(initialState.isOpen).toBeTruthy();
    expect(initialState.activeMapId).toEqual('map3');
    expect(initialState.source).toEqual('app');

    // button should be present
    expect(screen.getByTestId('open-Legend')).toBeTruthy();

    // close the legend dialog
    fireEvent.click(screen.getByTestId('open-Legend'));

    const result = store.getState().ui.dialogs.legend;
    expect(result.isOpen).toBeFalsy();
    expect(result.activeMapId).toEqual(props.mapId);
    expect(result.source).toEqual('app');
  });
});
