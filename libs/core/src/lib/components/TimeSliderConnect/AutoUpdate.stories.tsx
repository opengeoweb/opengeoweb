/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { connect } from 'react-redux';
import { Button, ButtonGroup } from '@mui/material';

import {
  CoreAppStore,
  defaultLayers,
  layerActions,
  mapActions,
  mapSelectors,
} from '@opengeoweb/store';
import { publicLayers, MapControls } from '@opengeoweb/webmap-react';
import { useDefaultMapSettings } from '@opengeoweb/webmap-redux';
import { DemoWrapperConnect } from '../Providers/Providers';

import { MapViewConnect } from '../MapViewConnect';
import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
} from '../LayerManager';
import { TimeSliderConnect } from './TimeSliderConnect';
import { createMockStore } from '../../store';

export default {
  title: 'components/TimeSliderConnect/AutoUpdate',
};
const store = createMockStore();
interface RadarPresetProps {
  setLayers?: typeof layerActions.setLayers;
  toggleAutoUpdate?: typeof mapActions.toggleAutoUpdate;
  setAutoLayerId?: typeof mapActions.setAutoLayerId;
  mapId: string;
}

const enhance = connect(
  (state: CoreAppStore, props: RadarPresetProps) => ({
    layers: mapSelectors.getMapLayers(state, props.mapId),
  }),
  {
    setLayers: layerActions.setLayers,
    toggleAutoUpdate: mapActions.toggleAutoUpdate,
    setAutoLayerId: mapActions.setAutoLayerId,
  },
);

const RadarPresetWithAutoUpdate: React.FC<RadarPresetProps> = ({
  setLayers,
  mapId,
  toggleAutoUpdate,
  setAutoLayerId,
}: RadarPresetProps) => {
  useDefaultMapSettings({
    mapId,
    baseLayers: [
      { ...defaultLayers.baseLayerGrey, id: `baseGrey-${mapId}` },
      {
        ...defaultLayers.overLayer,
        id: `overLayer-1-${mapId}`,
      },
    ],
  });

  return (
    <ButtonGroup
      variant="contained"
      color="primary"
      aria-label="outlined primary button group"
    >
      <Button
        onClick={(): void => {
          setLayers!({
            layers: [publicLayers.radarLayer],
            mapId,
          });
          toggleAutoUpdate!({ mapId, shouldAutoUpdate: true });
        }}
      >
        Radar with auto-update
      </Button>
      <Button
        onClick={(): void => {
          setLayers!({
            layers: [
              publicLayers.radarLayer,
              publicLayers.harmonieRelativeHumidityPl,
            ],
            mapId,
          });
          setAutoLayerId!({
            mapId,
            autoTimeStepLayerId: publicLayers.harmonieRelativeHumidityPl.id,
            autoUpdateLayerId: publicLayers.harmonieRelativeHumidityPl.id,
          });
          toggleAutoUpdate!({ mapId, shouldAutoUpdate: true });
        }}
      >
        Multiple layers with auto-update
      </Button>
    </ButtonGroup>
  );
};
const ConnectedRadarPresetWithAutoUpdate = enhance(RadarPresetWithAutoUpdate);

export const SetLayersWithAutoUpdate = (): React.ReactElement => {
  const mapId = 'mapid_1';
  const preloadedAvailableBaseLayers = [
    { ...defaultLayers.baseLayerGrey, id: `baseGrey-${mapId}` },
    publicLayers.baseLayerOpenStreetMapNL,
  ];

  return (
    <DemoWrapperConnect store={store}>
      <div style={{ height: '100vh' }}>
        <MapViewConnect mapId={mapId} />
      </div>

      <MapControls>
        <LayerManagerMapButtonConnect mapId={mapId} />
      </MapControls>
      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          zIndex: 500,
          width: '100%',
        }}
      >
        <TimeSliderConnect mapId={mapId} sourceId="timeslider_1" />
      </div>
      <div
        style={{
          position: 'absolute',
          left: '12px',
          top: '50px',
          zIndex: 500,
        }}
      >
        <LayerManagerConnect
          preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
        />
      </div>
      <div
        style={{
          position: 'absolute',
          left: '50px',
          top: '10px',
          zIndex: 50,
        }}
      >
        <ConnectedRadarPresetWithAutoUpdate mapId={mapId} />
      </div>
    </DemoWrapperConnect>
  );
};
