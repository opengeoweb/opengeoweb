/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  act,
  fireEvent,
  render,
  renderHook,
  screen,
  within,
} from '@testing-library/react';
import { produce } from 'immer';
import {
  genericActions,
  genericSelectors,
  mapActions,
  mapTypes,
  mapUtils,
  storeTestSettings,
  storeTestUtils,
  syncConstants,
} from '@opengeoweb/store';
import { webmapTestSettings } from '@opengeoweb/webmap';
import { defaultTimeSpan } from '@opengeoweb/timeslider';
import userEvent from '@testing-library/user-event';
import { Store } from '@reduxjs/toolkit';

import { Box } from '@mui/material';
import {
  TimeSliderConnect,
  useUpdateTimeSpan,
  useUpdateTimestep,
} from './TimeSliderConnect';
import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

describe('src/components/TimeSlider/TimeSliderConnect', () => {
  describe('TimeSliderConnect / moveAnimationUsingCalendar', () => {
    const mapId = 'mapid_3';
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            ...mapUtils.createMap({ id: mapId }),
            mapLayers: ['layerid_53'],
            isTimeSliderVisible: true,
            animationStartTime: '2024-11-04T12:00:00Z',
            animationEndTime: '2024-11-04T14:00:00Z',
          },
        },
        allIds: [mapId],
      },
      layers: {
        byId: {
          layerid_53: {
            mapId: 'mapid_3',
            name: 'wind_speed_components_hagl',
            style: 'windbarbs_kts/barb',
            format: 'image/png',
            service:
              'https://geoservices.knmi.nl/adaguc-server?DATASET=uwcw_ha43_dini_5p5km',
            dimensions: [
              {
                name: 'time',
                units: 'ISO8601',
                currentValue: '2024-11-06T05:00:00Z',
                minValue: '2024-11-04T05:00:00Z',
                maxValue: '2024-11-04T17:00:00Z',
              },
            ],
          },
        },
        allIds: ['layerid_53'],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
    };

    it('should move animation using calendar', async () => {
      const store = createMockStore(mockState);
      render(
        <DemoWrapperConnect store={store}>
          <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
        </DemoWrapperConnect>,
      );

      const calendarButton = screen.getByRole('button', {
        name: 'Marker placement',
      });
      expect(calendarButton).toBeTruthy();

      fireEvent.click(calendarButton);
      const dateTextField = within(
        // eslint-disable-next-line testing-library/no-node-access
        screen.getByText(/Select date and time/i).closest('div') as HTMLElement,
      ).getByRole('textbox');
      expect(dateTextField).toBeTruthy();

      await userEvent.clear(dateTextField);
      await userEvent.type(dateTextField, '04/11/2024 15:00');
      fireEvent.keyDown(dateTextField, { key: 'Enter', code: 'Enter' });

      const updatedState = store.getState().webmap.byId[mapId];
      expect(updatedState.animationEndTime).toEqual('2024-11-04T15:00:00Z');
      expect(updatedState.animationStartTime).toEqual('2024-11-04T13:00:00Z');
    });

    it('animation end time should stop at time dimension min value, to prevent moving into a range where there is no data', async () => {
      const store = createMockStore(mockState);
      render(
        <DemoWrapperConnect store={store}>
          <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
        </DemoWrapperConnect>,
      );

      const calendarButton = screen.getByRole('button', {
        name: 'Marker placement',
      });
      expect(calendarButton).toBeTruthy();

      fireEvent.click(calendarButton);
      const dateTextField = within(
        // eslint-disable-next-line testing-library/no-node-access
        screen.getByText(/Select date and time/i).closest('div') as HTMLElement,
      ).getByRole('textbox');
      expect(dateTextField).toBeTruthy();

      await userEvent.clear(dateTextField);
      await userEvent.type(dateTextField, '04/11/2024 04:00');
      fireEvent.keyDown(dateTextField, { key: 'Enter', code: 'Enter' });

      // To keep animation within data range, animation end time is set to time dimension min value + animation duration
      const updatedState = store.getState().webmap.byId[mapId];
      expect(updatedState.animationEndTime).toEqual('2024-11-04T07:00:00Z');
      expect(updatedState.animationStartTime).toEqual('2024-11-04T05:00:00Z');
    });

    it('animation end time should stop at time dimension max value, to prevent moving into a range where there is no data', async () => {
      const store = createMockStore(mockState);
      render(
        <DemoWrapperConnect store={store}>
          <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
        </DemoWrapperConnect>,
      );

      const calendarButton = screen.getByRole('button', {
        name: 'Marker placement',
      });
      expect(calendarButton).toBeTruthy();

      fireEvent.click(calendarButton);
      const dateTextField = within(
        // eslint-disable-next-line testing-library/no-node-access
        screen.getByText(/Select date and time/i).closest('div') as HTMLElement,
      ).getByRole('textbox');
      expect(dateTextField).toBeTruthy();

      await userEvent.clear(dateTextField);
      await userEvent.type(dateTextField, '04/11/2024 18:00');
      fireEvent.keyDown(dateTextField, { key: 'Enter', code: 'Enter' });

      const updatedState = store.getState().webmap.byId[mapId];
      expect(updatedState.animationEndTime).toEqual('2024-11-04T17:00:00Z');
      expect(updatedState.animationStartTime).toEqual('2024-11-04T15:00:00Z');
    });
  });

  describe('useUpdateTimestep', () => {
    it('should update timestep if auto is on and interval is a day', async () => {
      const mapId = 'mapid_1';
      const layer = produce(
        webmapTestSettings.defaultReduxLayerRadarKNMI,
        (draft) => {
          (draft.dimensions![0] as mapTypes.Dimension).timeInterval = {
            day: 1,
            hour: 0,
            isRegularInterval: true,
            minute: 0,
            month: 0,
            second: 0,
            year: 0,
          };
        },
      );
      const mockState = produce(
        storeTestUtils.mockStateMapWithLayer(layer, mapId),
        (draft) => {
          draft.webmap!.byId[mapId].isTimestepAuto = true;
        },
      );
      const store = createMockStore(mockState);

      expect(store.getState().webmap.byId[mapId].timeStep).toEqual(60);

      renderHook(() => useUpdateTimestep(mapId), {
        wrapper: ({ children }): React.ReactElement => (
          <DemoWrapperConnect store={store}>{children}</DemoWrapperConnect>
        ),
      });

      expect(store.getState().webmap.byId[mapId].timeStep).toEqual(24 * 60);
    });

    it('should update timestep if auto is on and interval is an hour', async () => {
      const mapId = 'mapid_2';
      const layer = produce(
        webmapTestSettings.defaultReduxLayerRadarKNMI,
        (draft) => {
          (draft.dimensions![0] as mapTypes.Dimension).timeInterval = {
            day: 0,
            hour: 1,
            isRegularInterval: true,
            minute: 0,
            month: 0,
            second: 0,
            year: 0,
          };
        },
      );

      const mockState2 = produce(
        storeTestUtils.mockStateMapWithLayer(layer, mapId),
        (draft) => {
          draft.webmap!.byId[mapId].isTimestepAuto = true;
        },
      );

      const store2 = createMockStore(mockState2);

      expect(store2.getState().webmap.byId[mapId].timeStep).toEqual(60);

      renderHook(() => useUpdateTimestep(mapId), {
        wrapper: ({ children }): React.ReactElement => (
          <DemoWrapperConnect store={store2}>{children}</DemoWrapperConnect>
        ),
      });
      expect(store2.getState().webmap.byId[mapId].timeStep).toEqual(60);
    });

    it('should not update timestep if auto is off', async () => {
      const mapId = 'mapid_2';
      const layer = produce(
        webmapTestSettings.defaultReduxLayerRadarKNMI,
        (draft) => {
          (draft.dimensions![0] as mapTypes.Dimension).timeInterval = {
            day: 0,
            hour: 1,
            isRegularInterval: true,
            minute: 0,
            month: 0,
            second: 0,
            year: 0,
          };
        },
      );

      const mockState2 = produce(
        storeTestUtils.mockStateMapWithLayer(layer, mapId),
        (draft) => {
          draft.webmap!.byId[mapId].isTimestepAuto = false;
        },
      );
      const store2 = createMockStore(mockState2);

      expect(store2.getState().webmap.byId[mapId].timeStep).toEqual(60);

      renderHook(() => useUpdateTimestep(mapId), {
        wrapper: ({ children }): React.ReactElement => (
          <DemoWrapperConnect store={store2}>{children}</DemoWrapperConnect>
        ),
      });
      expect(store2.getState().webmap.byId[mapId].timeStep).toEqual(60);
    });
  });
  describe('useUpdateTimeSpan', () => {
    it('should set span from data when timespan auto is on', async () => {
      const now = '2022-01-09T14:00:00Z';
      jest.useFakeTimers().setSystemTime(new Date(now));

      const mapId = 'mapid_2';
      const layer = produce(
        webmapTestSettings.defaultReduxLayerRadarKNMI,
        (draft) => {
          (draft.dimensions![0] as mapTypes.Dimension).maxValue =
            '2020-03-14T12:00:00Z';
          (draft.dimensions![0] as mapTypes.Dimension).minValue =
            '2020-02-13T10:00:00Z';
        },
      );
      const mockState2 = produce(
        storeTestUtils.mockStateMapWithLayer(layer, mapId),
        (draft) => {
          draft.webmap!.byId[mapId].autoTimeStepLayerId = 'layerid_2';
          draft.webmap!.byId[mapId].isTimeSpanAuto = true;
        },
      );
      const store2 = createMockStore(mockState2);
      const onSetTimeSliderSpan = jest.fn();

      renderHook(() => useUpdateTimeSpan(mapId, onSetTimeSliderSpan), {
        wrapper: ({ children }): React.ReactElement => (
          <DemoWrapperConnect store={store2}>{children}</DemoWrapperConnect>
        ),
      });

      expect(onSetTimeSliderSpan).toHaveBeenCalledWith(
        2599200,
        1582887600,
        5907.272727272727,
      );
    });
    it('should set default span when update timespan auto is off', async () => {
      const now = '2022-01-09T14:00:00Z';
      jest.useFakeTimers().setSystemTime(new Date(now));

      const mapId = 'mapid_2';
      const layer = produce(
        webmapTestSettings.defaultReduxLayerRadarKNMI,
        (draft) => {
          (draft.dimensions![0] as mapTypes.Dimension).maxValue =
            '2020-03-14T12:00:00Z';
          (draft.dimensions![0] as mapTypes.Dimension).minValue =
            '2020-02-13T10:00:00Z';
        },
      );
      const mockState2 = produce(
        storeTestUtils.mockStateMapWithLayer(layer, mapId),
        (draft) => {
          draft.webmap!.byId[mapId].autoTimeStepLayerId = 'layerid_2';
          draft.webmap!.byId[mapId].isTimeSpanAuto = false;
        },
      );
      const store2 = createMockStore(mockState2);
      const onSetTimeSliderSpan = jest.fn();

      renderHook(() => useUpdateTimeSpan(mapId, onSetTimeSliderSpan), {
        wrapper: ({ children }): React.ReactElement => (
          <DemoWrapperConnect store={store2}>{children}</DemoWrapperConnect>
        ),
      });

      expect(onSetTimeSliderSpan).toHaveBeenCalledWith(
        defaultTimeSpan,
        1725563127.2727273,
        196.36363636363637,
      );
    });
  });

  describe('time scrolling with mouse wheel', () => {
    const getTime = (store: Store, mapId: string): string | null => {
      return genericSelectors.getTime(
        genericSelectors.getSynchronizationGroupStore(store.getState()),
        mapId,
      );
    };

    it('should move current time when mouse scrolled on legend', async () => {
      jest.useFakeTimers();

      const mapId = 'mapid_1';
      const layer = webmapTestSettings.defaultReduxLayerRadarKNMI;
      const mockState = {
        ...storeTestUtils.mockStateMapWithLayer(layer, mapId),
        syncGroups: {
          ...genericActions.initialSyncState,
        },
      };
      const store = createMockStore(mockState);
      render(
        <DemoWrapperConnect store={store}>
          <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
        </DemoWrapperConnect>,
      );

      act(() => {
        store.dispatch(
          genericActions.syncGroupAddSource({
            id: mapId,
            type: [syncConstants.SYNCGROUPS_TYPE_SETTIME],
          }),
        );
        store.dispatch(
          genericActions.setTime({
            sourceId: 'test',
            origin: 'test',
            value: '2020-03-13T13:30:00Z',
          }),
        );
        jest.runOnlyPendingTimers();
      });
      expect(getTime(store, mapId)).toEqual('2020-03-13T13:30:00Z');

      const timeSliderLegend = screen.getByTestId('timeSliderLegend');
      expect(timeSliderLegend).toBeTruthy();
      const canvas = within(timeSliderLegend).getByRole('presentation', {
        name: 'canvas',
      }).firstChild as HTMLElement;

      // deltaY values equivalent to one movement of a mouse scroll wheel
      // should move one timestep forward or backward.
      const deltaY1 = 100;
      const deltaY2 = -200;

      fireEvent.wheel(canvas, { deltaY: deltaY1 });
      act(() => {
        jest.runOnlyPendingTimers();
      });
      expect(getTime(store, mapId)).toEqual('2020-03-13T12:40:00Z');

      fireEvent.wheel(canvas, { deltaY: deltaY2 });
      act(() => {
        jest.runOnlyPendingTimers();
      });
      expect(getTime(store, mapId)).toEqual('2020-03-13T14:20:00Z');
    });

    it('should not move current time when workspace mouse scroll is off and mouse scrolled on map', async () => {
      jest.useFakeTimers();

      const mapId = 'mapid_1';
      const layer = webmapTestSettings.defaultReduxLayerRadarKNMI;
      const mockState = {
        ...storeTestUtils.mockStateMapWithLayer(layer, mapId),
        syncGroups: {
          ...genericActions.initialSyncState,
        },
      };
      const store = createMockStore(mockState);
      const mapWindowRef = React.createRef<HTMLDivElement | null>();
      render(
        <DemoWrapperConnect store={store}>
          <Box ref={mapWindowRef} data-testid="testWindow">
            <TimeSliderConnect
              sourceId="timeslider-1"
              mapId={mapId}
              mapWindowRef={mapWindowRef}
            />
          </Box>
        </DemoWrapperConnect>,
      );

      act(() => {
        store.dispatch(
          genericActions.syncGroupAddSource({
            id: mapId,
            type: [syncConstants.SYNCGROUPS_TYPE_SETTIME],
          }),
        );
        store.dispatch(
          genericActions.setTime({
            sourceId: 'test',
            origin: 'test',
            value: '2020-03-13T13:30:00Z',
          }),
        );
        store.dispatch(
          genericActions.syncGroupToggleIsTimeScrollingEnabled({
            isTimeScrollingEnabled: false,
          }),
        );
        jest.runOnlyPendingTimers();
      });
      const initialTime = '2020-03-13T13:30:00Z';
      expect(getTime(store, mapId)).toEqual(initialTime);

      const testWindow = screen.getByTestId('testWindow');
      expect(testWindow).toBeTruthy();

      // deltaY values equivalent to one movement of a mouse scroll wheel
      // should move one timestep forward or backward.
      const deltaY1 = 100;
      const deltaY2 = -200;

      fireEvent.wheel(testWindow, { deltaY: deltaY1 });
      act(() => {
        jest.runOnlyPendingTimers();
      });
      expect(getTime(store, mapId)).toEqual(initialTime);

      fireEvent.wheel(testWindow, { deltaY: deltaY2 });
      act(() => {
        jest.runOnlyPendingTimers();
      });
      expect(getTime(store, mapId)).toEqual(initialTime);
    });

    it('should move current time when workspace mouse scroll is on and mouse scrolled on map', async () => {
      jest.useFakeTimers();

      const mapId = 'mapid_1';
      const layer = webmapTestSettings.defaultReduxLayerRadarKNMI;
      const mockState = {
        ...storeTestUtils.mockStateMapWithLayer(layer, mapId),
        syncGroups: {
          ...genericActions.initialSyncState,
        },
      };
      const store = createMockStore(mockState);
      const mapWindowRef = React.createRef<HTMLDivElement | null>();
      render(
        <DemoWrapperConnect store={store}>
          <Box ref={mapWindowRef} data-testid="testWindow">
            <TimeSliderConnect
              sourceId="timeslider-1"
              mapId={mapId}
              mapWindowRef={mapWindowRef}
            />
          </Box>
        </DemoWrapperConnect>,
      );

      act(() => {
        store.dispatch(
          genericActions.syncGroupAddSource({
            id: mapId,
            type: [syncConstants.SYNCGROUPS_TYPE_SETTIME],
          }),
        );
        store.dispatch(
          genericActions.setTime({
            sourceId: 'test',
            origin: 'test',
            value: '2020-03-13T13:30:00Z',
          }),
        );
        store.dispatch(
          genericActions.syncGroupToggleIsTimeScrollingEnabled({
            isTimeScrollingEnabled: true,
          }),
        );
        jest.runOnlyPendingTimers();
      });
      expect(getTime(store, mapId)).toEqual('2020-03-13T13:30:00Z');

      const testWindow = screen.getByTestId('testWindow');
      expect(testWindow).toBeTruthy();

      // deltaY values equivalent to one movement of a mouse scroll wheel
      // should move one timestep forward or backward.
      const deltaY1 = 100;
      const deltaY2 = -200;

      fireEvent.wheel(testWindow, { deltaY: deltaY1 });
      act(() => {
        jest.runOnlyPendingTimers();
      });
      expect(getTime(store, mapId)).toEqual('2020-03-13T12:40:00Z');

      fireEvent.wheel(testWindow, { deltaY: deltaY2 });
      act(() => {
        jest.runOnlyPendingTimers();
      });
      expect(getTime(store, mapId)).toEqual('2020-03-13T14:20:00Z');
    });

    it('should move current time when workspace mouse scroll is on and TimeSlider is hidden and mouse scrolled on map', async () => {
      jest.useFakeTimers();

      const mapId = 'mapid_1';
      const layer = webmapTestSettings.defaultReduxLayerRadarKNMI;
      const mockState = {
        ...storeTestUtils.mockStateMapWithLayer(layer, mapId),
        syncGroups: {
          ...genericActions.initialSyncState,
        },
      };
      const store = createMockStore(mockState);
      const mapWindowRef = React.createRef<HTMLDivElement | null>();
      render(
        <DemoWrapperConnect store={store}>
          <Box ref={mapWindowRef} data-testid="testWindow">
            <TimeSliderConnect
              sourceId="timeslider-1"
              mapId={mapId}
              mapWindowRef={mapWindowRef}
            />
          </Box>
        </DemoWrapperConnect>,
      );

      act(() => {
        store.dispatch(
          genericActions.syncGroupAddSource({
            id: mapId,
            type: [syncConstants.SYNCGROUPS_TYPE_SETTIME],
          }),
        );
        store.dispatch(
          genericActions.setTime({
            sourceId: 'test',
            origin: 'test',
            value: '2020-03-13T13:30:00Z',
          }),
        );
        store.dispatch(
          genericActions.syncGroupToggleIsTimeScrollingEnabled({
            isTimeScrollingEnabled: true,
          }),
        );
        store.dispatch(
          mapActions.toggleTimeSliderIsVisible({
            mapId,
            isTimeSliderVisible: false,
          }),
        );
        jest.runOnlyPendingTimers();
      });
      expect(getTime(store, mapId)).toEqual('2020-03-13T13:30:00Z');

      const testWindow = screen.getByTestId('testWindow');
      expect(testWindow).toBeTruthy();

      // deltaY values equivalent to one movement of a mouse scroll wheel
      // should move one timestep forward or backward.
      const deltaY1 = 100;
      const deltaY2 = -200;

      fireEvent.wheel(testWindow, { deltaY: deltaY1 });
      act(() => {
        jest.runOnlyPendingTimers();
      });
      expect(getTime(store, mapId)).toEqual('2020-03-13T12:40:00Z');

      fireEvent.wheel(testWindow, { deltaY: deltaY2 });
      act(() => {
        jest.runOnlyPendingTimers();
      });
      expect(getTime(store, mapId)).toEqual('2020-03-13T14:20:00Z');
    });
  });

  describe('other functions', () => {
    it('should render the component when the map has a layer with time dimension', () => {
      const mapId = 'mapid_1';
      const layer = webmapTestSettings.defaultReduxLayerRadarKNMI;
      const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
      const store = createMockStore(mockState);
      render(
        <DemoWrapperConnect store={store}>
          <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
        </DemoWrapperConnect>,
      );

      expect(screen.getByTestId('timeSliderButtons')).toBeTruthy();
      expect(screen.getByTestId('timeSliderTimeBox')).toBeTruthy();
      expect(screen.getByTestId('timeSliderLegend')).toBeTruthy();
      expect(screen.getByTestId('timeSlider')).toBeTruthy();
    });
    it('should render the component when the layer on the map does not have a time dimension', () => {
      const mapId = 'mapid_2';
      const layer = storeTestSettings.layerWithoutTimeDimension;
      const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
      const store = createMockStore(mockState);
      render(
        <DemoWrapperConnect store={store}>
          <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
        </DemoWrapperConnect>,
      );

      expect(screen.getByTestId('timeSliderButtons')).toBeTruthy();
      expect(screen.getByTestId('timeSliderTimeBox')).toBeTruthy();
      expect(screen.getByTestId('timeSliderLegend')).toBeTruthy();
      expect(screen.getByTestId('timeSlider')).toBeTruthy();
    });
    it('should toggle time slider hover when ctrl + alt + h is pressed', async () => {
      const mapId = 'mapid_3';
      const layer = webmapTestSettings.defaultReduxLayerRadarKNMI;
      const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
      const store = createMockStore(mockState);
      render(
        <DemoWrapperConnect store={store}>
          <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
        </DemoWrapperConnect>,
      );
      const timeSlider = screen.getByTestId('timeSlider');
      expect(timeSlider).toBeTruthy();

      expect(
        store.getState().webmap.byId[mapId].isTimeSliderHoverOn,
      ).toBeFalsy();
      timeSlider.focus();

      fireEvent.keyDown(timeSlider, {
        ctrlKey: true,
        altKey: true,
        key: 'h',
        code: 'KeyH',
      });

      expect(
        store.getState().webmap.byId[mapId].isTimeSliderHoverOn,
      ).toBeTruthy();
    });

    it('should be visible even when isTimeSliderVisible is true', () => {
      const mapId = 'mapid_3';
      const mockState = {
        webmap: {
          byId: {
            [mapId]: {
              ...mapUtils.createMap({ id: mapId }),
              baseLayers: [],
              mapLayers: [],
              overLayers: [],
              isTimeSliderVisible: true,
            },
          },
          allIds: [mapId],
        },

        syncGroups: {
          ...genericActions.initialSyncState,
        },
      };
      const store = createMockStore(mockState);
      render(
        <DemoWrapperConnect store={store}>
          <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
        </DemoWrapperConnect>,
      );
      const timeSlider = screen.getByTestId('timeSlider');
      expect(timeSlider).toBeTruthy();
    });
  });

  it('should be hidden even when isTimeSliderVisible is false', () => {
    const mapId = 'mapid_3';
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            ...mapUtils.createMap({ id: mapId }),
            baseLayers: [],
            mapLayers: [],
            overLayers: [],
            isTimeSliderVisible: false,
          },
        },
        allIds: [mapId],
      },
      syncGroups: {
        ...genericActions.initialSyncState,
      },
    };
    const store = createMockStore(mockState);
    render(
      <DemoWrapperConnect store={store}>
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </DemoWrapperConnect>,
    );
    const timeSlider = screen.queryByTestId('timeSlider');
    expect(timeSlider).toBeFalsy();
  });
  it('should be visible even when isTimeSliderVisible is false but has isAlwaysVisible as prop', () => {
    const mapId = 'mapid_3';
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            ...mapUtils.createMap({ id: mapId }),
            baseLayers: [],
            mapLayers: [],
            overLayers: [],
            isTimeSliderVisible: false,
          },
        },
        allIds: [mapId],
      },
      syncGroups: {
        ...genericActions.initialSyncState,
      },
    };
    const store = createMockStore(mockState);
    render(
      <DemoWrapperConnect store={store}>
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </DemoWrapperConnect>,
    );
    const timeSlider = screen.queryByTestId('timeSlider');
    expect(timeSlider).toBeFalsy();
  });

  describe('hiding timeslider', () => {
    it('should toggle slider visibility when clicking the hide button', () => {
      const mapId = 'mapid_3';
      const mockState = {
        webmap: {
          byId: {
            [mapId]: {
              ...mapUtils.createMap({ id: mapId }),
              baseLayers: [],
              mapLayers: [],
              overLayers: [],
              isTimeSliderVisible: true,
            },
          },
          allIds: [mapId],
        },

        syncGroups: {
          ...genericActions.initialSyncState,
        },
      };
      const store = createMockStore(mockState);
      render(
        <DemoWrapperConnect store={store}>
          <TimeSliderConnect
            sourceId="timeslider-1"
            mapId={mapId}
            isAlwaysVisible
          />
        </DemoWrapperConnect>,
      );
      const timeSlider = screen.getByTestId('timeSlider');
      expect(timeSlider).toBeTruthy();
    });
    it('should toggle slider visibility when clicking the hide button', () => {
      const mapId = 'mapid_3';
      const mockState = {
        webmap: {
          byId: {
            [mapId]: {
              ...mapUtils.createMap({ id: mapId }),
              baseLayers: [],
              mapLayers: [],
              overLayers: [],
              isTimeSliderVisible: true,
            },
          },
          allIds: [mapId],
        },
        syncGroups: { ...genericActions.initialSyncState },
      };
      const store = createMockStore(mockState);
      render(
        <DemoWrapperConnect store={store}>
          <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
        </DemoWrapperConnect>,
      );

      expect(
        store.getState().webmap.byId[mapId].isTimeSliderVisible,
      ).toBeTruthy();
      fireEvent.click(screen.getByTestId('hideTimeSliderButton'));

      expect(
        store.getState().webmap.byId[mapId].isTimeSliderVisible,
      ).toBeFalsy();
    });
  });
});
