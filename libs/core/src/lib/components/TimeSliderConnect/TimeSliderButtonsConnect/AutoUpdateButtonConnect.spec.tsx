/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { mapUtils, storeTestUtils } from '@opengeoweb/store';
import { AutoUpdateButtonConnect } from './AutoUpdateButtonConnect';
import { DemoWrapperConnect } from '../../Providers/Providers';
import { translateKeyOutsideComponents } from '../../../utils/i18n';
import { createMockStore } from '../../../store';

describe('src/components/TimeSlider/TimeSliderButtons/AutoUpdateButton/AutoUpdateButtonConnect.tsx', () => {
  const mapId = 'mapid_1';
  const props = {
    mapId,
  };
  it('should render inactive auto update button by default', async () => {
    const mockState = storeTestUtils.mockStateMapWithTimeStepWithoutLayers(
      mapId,
      5,
    );
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <AutoUpdateButtonConnect {...props} />
      </DemoWrapperConnect>,
    );
    const autoUpdateButton = screen.getByRole('button');
    expect(autoUpdateButton).toBeTruthy();
    expect(screen.getByTestId('autoUpdateButton').textContent).toContain(
      'AUTO',
    );
    const button = screen.getByTestId('autoUpdateButton');
    expect(button.classList.contains('Mui-disabled')).toBeFalsy();

    fireEvent.focus(screen.getByTestId('autoUpdateButton'));
    expect(
      await screen.findByLabelText(
        translateKeyOutsideComponents('timeslider-auto-update-off'),
      ),
    ).toBeTruthy();
  });

  it('autoupdate button is clicked, the autoupdate state should change in the store', async () => {
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            ...mapUtils.createMap({ id: mapId }),
            shouldAutoUpdate: false,
          },
        },
        allIds: [mapId],
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <AutoUpdateButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(store.getState().webmap.byId[mapId].isAutoUpdating).toBeFalsy();

    const button = screen.getByRole('button');

    fireEvent.click(button);
    expect(store.getState().webmap.byId[mapId].isAutoUpdating).toBeTruthy();
  });
});
