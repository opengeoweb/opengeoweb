/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';

import userEvent from '@testing-library/user-event';
import { mapUtils, storeTestUtils } from '@opengeoweb/store';
import { ThemeProvider } from '@opengeoweb/theme';
import { defaultDelay, getSpeedDelay } from '@opengeoweb/timeslider';
import { SpeedButtonConnect } from './SpeedButtonConnect';
import { DemoWrapper } from '../../Providers/Providers';
import { translateKeyOutsideComponents } from '../../../utils/i18n';
import { createMockStore } from '../../../store';

describe('src/components/TimeSlider/TimeSliderButtons/SpeedButton/SpeedButtonConnect', () => {
  const mapId = 'mapid_1';
  const props = {
    mapId,
  };
  const user = userEvent.setup();
  it('should render an enabled speed button and how a correct speed text', () => {
    const mockState =
      storeTestUtils.mockStateMapWithAnimationDelayWithoutLayers(mapId);
    const store = createMockStore(mockState);

    render(
      <DemoWrapper>
        <Provider store={store}>
          <SpeedButtonConnect {...props} />
        </Provider>
      </DemoWrapper>,
    );
    const button = screen.getByRole('button', {
      name: translateKeyOutsideComponents('timeslider-speed'),
    });
    expect(button).toBeEnabled();
    expect(button).toHaveTextContent('x4');
  });

  it('Speed menu renders, when clicked open and should change speed delay value in store', async () => {
    const store = createMockStore({
      webmap: {
        byId: {
          [mapId]: {
            ...mapUtils.createMap({ id: mapId }),
            animationDelay: defaultDelay,
          },
        },
        allIds: [mapId],
      },
    });

    render(
      <ThemeProvider>
        <Provider store={store}>
          <SpeedButtonConnect {...props} />
        </Provider>
      </ThemeProvider>,
    );

    expect(store.getState().webmap.byId[mapId].animationDelay).toEqual(
      defaultDelay,
    );

    await user.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('timeslider-speed'),
      }),
    );
    await user.click(
      screen.getByRole('menuitem', {
        name: /x0.1/i,
      }),
    );

    expect(store.getState().webmap.byId[mapId].animationDelay).toEqual(
      getSpeedDelay(0.1),
    );

    await user.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('timeslider-speed'),
      }),
    );
    await user.click(
      screen.getByRole('menuitem', {
        name: /x16/i,
      }),
    );

    expect(store.getState().webmap.byId[mapId].animationDelay).toEqual(
      getSpeedDelay(16),
    );
  });
});
