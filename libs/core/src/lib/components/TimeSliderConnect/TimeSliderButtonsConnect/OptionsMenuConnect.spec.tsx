/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { storeTestUtils } from '@opengeoweb/store';
import { webmapTestSettings } from '@opengeoweb/webmap';
import { defaultAnimationDelayAtStart } from '@opengeoweb/timeslider';
import { OptionsMenuConnect } from './OptionsMenuConnect';
import { DemoWrapperConnect } from '../../Providers/Providers';
import { translateKeyOutsideComponents } from '../../../utils/i18n';
import { createMockStore } from '../../../store';

describe('src/components/TimeSlider/TimeSliderButtons/OptionsMenuConnect', () => {
  it('should render the component and fire action', () => {
    const mapId = 'mapid_1';
    const mockState = storeTestUtils.mockStateMapWithLayer(
      webmapTestSettings.defaultReduxLayerRadarKNMI,
      mapId,
    );
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <OptionsMenuConnect mapId={mapId} />
      </DemoWrapperConnect>,
    );

    expect(store.getState().webmap.byId[mapId].animationDelay).toEqual(
      defaultAnimationDelayAtStart,
    );

    fireEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('timeslider-speed'),
      }),
    );

    fireEvent.click(screen.getByRole('menuitem', { name: /x16/i }));
    expect(store.getState().webmap.byId[mapId].animationDelay).toEqual(62.5);
  });
});
