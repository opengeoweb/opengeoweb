/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { act, fireEvent, render, screen } from '@testing-library/react';
import { mapActions, mapEnums, storeTestUtils } from '@opengeoweb/store';
import { webmapTestSettings } from '@opengeoweb/webmap';
import { defaultSecondsPerPx, defaultTimeSpan } from '@opengeoweb/timeslider';
import { dateUtils } from '@opengeoweb/shared';
import {
  TimeSpanButtonConnect,
  TimeSpanButtonConnectProps,
} from './TimeSpanButtonConnect';
import { DemoWrapperConnect } from '../../Providers/Providers';
import { translateKeyOutsideComponents } from '../../../utils/i18n';
import { createMockStore } from '../../../store';

describe('src/components/TimeSlider/TimeSliderButtons/TimeSpanButtonConnect', () => {
  const mapId = 'mapid_1';
  const props = {
    mapId,
    layers: webmapTestSettings.defaultReduxLayerRadarColor,
  } as unknown as TimeSpanButtonConnectProps;

  it('should render an enabled button and show correct label', () => {
    const mockState =
      storeTestUtils.mockStateMapWithTimeSliderSpanWithoutLayers(mapId, 3600);
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <TimeSpanButtonConnect {...props} />
      </DemoWrapperConnect>,
    );
    expect(
      screen.getByLabelText(
        translateKeyOutsideComponents('timeslider-time-span'),
      ),
    ).toBeInTheDocument();
    expect(
      screen.getByLabelText(
        translateKeyOutsideComponents('timeslider-time-span'),
      ),
    ).toHaveTextContent(translateKeyOutsideComponents('timeslider-hour'));
  });

  it('TimeSpanButton opens and fire action to change span in store', async () => {
    const now = new Date('2023-02-03T14:00:00Z');
    jest.spyOn(dateUtils, 'utc').mockReturnValue(now);
    const mockState = storeTestUtils.mockStateMapWithLayer(
      webmapTestSettings.defaultReduxLayerRadarColor,
      mapId,
    );
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <TimeSpanButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    const initialState = store.getState().webmap.byId[mapId];
    expect(initialState.timeSliderSecondsPerPx).toEqual(defaultSecondsPerPx);
    expect(initialState.timeSliderSpan).toEqual(defaultTimeSpan);
    expect(initialState.timeSliderCenterTime).toEqual(
      dateUtils.unix(dateUtils.utc()),
    );
    // open span slider
    fireEvent.click(
      screen.getByLabelText(
        translateKeyOutsideComponents('timeslider-time-span'),
      ),
    );
    const menuList = screen.getByRole('menu');
    expect(menuList).toBeInTheDocument();

    // select 6h time span
    const option6h = screen.getByText(
      `6 ${translateKeyOutsideComponents('timeslider-hour')}`,
    );
    expect(menuList).toContainElement(option6h);
    fireEvent.click(option6h);

    // check that expected map actions are fired
    const sixHours = 6 * 3600;
    const result = store.getState().webmap.byId[mapId];
    expect(result.timeSliderSecondsPerPx).toEqual(49.09090909090909);
    expect(result.timeSliderSpan).toEqual(sixHours);
    expect(result.timeSliderCenterTime).toEqual(expect.any(Number));
  });

  it('TimeSpanButton opens and fire action to set auto span to true in store', async () => {
    const mockState = storeTestUtils.mockStateMapWithLayer(
      webmapTestSettings.defaultReduxLayerRadarColor,
      mapId,
    );
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <TimeSpanButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(
      store.getState().webmap.byId[props.mapId].isTimeSpanAuto,
    ).toBeFalsy();

    // open span slider
    fireEvent.click(
      screen.getByLabelText(
        translateKeyOutsideComponents('timeslider-time-span'),
      ),
    );
    const menuList = screen.getByRole('menu');
    expect(menuList).toBeInTheDocument();

    // select 6h time span
    const option6h = screen.getByText(
      `6 ${translateKeyOutsideComponents('timeslider-hour')}`,
    );
    expect(menuList).toContainElement(option6h);
    fireEvent.click(option6h);
    expect(
      store.getState().webmap.byId[props.mapId].isTimeSpanAuto,
    ).toBeFalsy();

    // select auto time span
    const optionAuto = screen.getByText(
      translateKeyOutsideComponents('timeslider-auto'),
    );
    expect(menuList).toContainElement(optionAuto);
    fireEvent.click(optionAuto);
    expect(
      store.getState().webmap.byId[props.mapId].isTimeSpanAuto,
    ).toBeTruthy();
  });

  it('TimeSpanButton should turn off auto span if set to true and choosing another value', async () => {
    const store = createMockStore();

    render(
      <DemoWrapperConnect store={store}>
        <TimeSpanButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(
        mapActions.toggleTimeSpanAuto({
          mapId,
          timeSpanAuto: true,
          origin: mapEnums.MapActionOrigin.map,
        }),
      );
    });

    const state = store.getState();
    expect(state.webmap!.byId[mapId].isTimeSpanAuto).toEqual(true);

    // open span slider
    fireEvent.click(
      screen.getByLabelText(
        translateKeyOutsideComponents('timeslider-time-span'),
      ),
    );
    const menuList = screen.getByRole('menu');
    expect(menuList).toBeInTheDocument();

    // select 6h time span
    const option6h = screen.getByText(
      `6 ${translateKeyOutsideComponents('timeslider-hour')}`,
    );
    expect(menuList).toContainElement(option6h);
    fireEvent.click(option6h);

    const newState = store.getState();
    expect(newState.webmap!.byId[mapId].isTimeSpanAuto).toEqual(false);
  });

  it('TimeSpanButton should do nothing if auto timespan is clicked but already set to true', async () => {
    const store = createMockStore();

    render(
      <DemoWrapperConnect store={store}>
        <TimeSpanButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(
        mapActions.toggleTimeSpanAuto({
          mapId,
          timeSpanAuto: true,
          origin: mapEnums.MapActionOrigin.map,
        }),
      );
    });

    const state = store.getState();
    expect(state.webmap!.byId[mapId].isTimeSpanAuto).toEqual(true);

    // open span slider
    fireEvent.click(
      screen.getByLabelText(
        translateKeyOutsideComponents('timeslider-time-span'),
      ),
    );
    const menuList = screen.getByRole('menu');
    expect(menuList).toBeInTheDocument();

    // select auto time span
    const optionAuto = screen.getByText(
      translateKeyOutsideComponents('timeslider-auto'),
    );
    expect(menuList).toContainElement(optionAuto);
    fireEvent.click(optionAuto);

    const newState = store.getState();
    expect(newState.webmap!.byId[mapId].isTimeSpanAuto).toEqual(true);
  });
});
