/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { storeTestSettings, storeTestUtils } from '@opengeoweb/store';
import { webmapTestSettings } from '@opengeoweb/webmap';
import { TimeSliderCurrentTimeBoxConnect } from './TimeSliderCurrentTimeBoxConnect';
import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

describe('src/components/TimeSlider/TimeSliderCurrentTimeBox/TimeSliderCurrentTimeBoxConnect', () => {
  it('should render the component when the map has a time dimension', () => {
    const mapId = 'mapid_1';
    const layer = webmapTestSettings.defaultReduxLayerRadarKNMI;
    const mockState = storeTestUtils.mockStateMapWithDimensions(layer, mapId);
    const store = createMockStore(mockState);
    render(
      <DemoWrapperConnect store={store}>
        <TimeSliderCurrentTimeBoxConnect
          mapId={mapId}
          unfilteredSelectedTime={0}
          setUnfilteredSelectedTime={() => {}}
          onCalendarSelect={() => {}}
        />
      </DemoWrapperConnect>,
    );

    expect(screen.getByTestId('canvas-container')).toBeTruthy();
  });

  it('should render the component when the map does not have a time dimension', () => {
    const mapId = 'mapid_1';
    const layer = storeTestSettings.layerWithoutTimeDimension;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStore(mockState);
    render(
      <DemoWrapperConnect store={store}>
        <TimeSliderCurrentTimeBoxConnect
          mapId={mapId}
          unfilteredSelectedTime={0}
          setUnfilteredSelectedTime={() => {}}
          onCalendarSelect={() => {}}
        />
      </DemoWrapperConnect>,
    );

    expect(screen.getByTestId('canvas-container')).toBeTruthy();
  });
});
