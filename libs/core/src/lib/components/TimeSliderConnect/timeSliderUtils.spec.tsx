/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { dateUtils } from '@opengeoweb/shared';
import {
  calculateNewAnimationEndTime,
  calculateNewAnimationTimeBounds,
  getTimeBounds,
} from './timesliderUtils';

describe('src/components/TimeSlider/timeSliderUtils', () => {
  describe('getTimeBounds', () => {
    it('should return the proper time bounds', () => {
      const dimensions = [
        {
          name: 'time',
          currentValue: '2020-07-01T10:00:00Z',
          maxValue: '2020-07-01T08:00:00Z',
          minValue: '2020-07-02T10:00:00Z',
        },
        {
          name: 'someOtherDim',
          currentValue: '12',
          maxValue: 'gigantisch',
          minValue: 'anders',
        },
      ];
      expect(getTimeBounds(dimensions)).toStrictEqual({
        startTime: dateUtils.unix(dateUtils.utc(dimensions[0].minValue)),
        endTime: dateUtils.unix(dateUtils.utc(dimensions[0].maxValue)),
      });
    });
    it('should return the default time bounds if no time dimension passed', () => {
      const currentTime = dateUtils.unix(
        dateUtils.set(dateUtils.utc(), { seconds: 0, milliseconds: 0 }),
      );
      const dimensions = [
        {
          name: 'someOtherDim',
          currentValue: '12',
          maxValue: 'gigantisch',
          minValue: 'anders',
        },
      ];
      expect(getTimeBounds(dimensions)).toStrictEqual({
        startTime: currentTime,
        endTime: currentTime,
      });
    });
  });

  describe('calculateNewAnimationEndTime', () => {
    it('should return dataStartTimeInMilliseconds if newTimeInMilliseconds is less than dataStartTimeInMilliseconds', () => {
      const newTime = 1000;
      const dataStartTime = 2000;
      const dataEndTime = 3000;
      const animationDuration = 1000;
      const result = calculateNewAnimationEndTime(
        newTime,
        dataStartTime,
        dataEndTime,
        animationDuration,
      );
      expect(result).toBe(dataStartTime + animationDuration);
    });

    it('should return dataEndTimeInMilliseconds if newTimeInMilliseconds is greater than dataEndTimeInMilliseconds', () => {
      const newTime = 4000;
      const dataStartTime = 2000;
      const dataEndTime = 3000;
      const animationDuration = 1000;
      const result = calculateNewAnimationEndTime(
        newTime,
        dataStartTime,
        dataEndTime,
        animationDuration,
      );
      expect(result).toBe(dataEndTime);
    });

    it('should return newTimeInMilliseconds if it is within the data start and end time range', () => {
      const newTime = 2500;
      const dataStartTime = 2000;
      const dataEndTime = 3000;
      const animationDuration = 1000;
      const result = calculateNewAnimationEndTime(
        newTime,
        dataStartTime,
        dataEndTime,
        animationDuration,
      );
      expect(result).toBe(newTime);
    });
  });

  describe('calculateNewAnimationTimeBounds', () => {
    it('should return correct bounds when newTime is within data bounds', () => {
      const result = calculateNewAnimationTimeBounds(
        2000,
        '2023-10-30T00:00:00.000Z',
        '2023-10-30T01:00:00.000Z',
        1500,
        3000,
      );
      const expectedDuration =
        new Date('2023-10-30T01:00:00.000Z').getTime() -
        new Date('2023-10-30T00:00:00.000Z').getTime();
      expect(result.newAnimationEndTime).toBe(2000000);
      expect(result.newAnimationStartTime).toBe(2000000 - expectedDuration);
    });
  });
});
