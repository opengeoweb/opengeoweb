/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import * as Sentry from '@sentry/react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import SentryRecordButton from './SentryRecordButton';
import { CoreI18nProvider } from '../Providers/Providers';

describe('components/SentryRecordButton/SentryRecordButton', () => {
  beforeEach(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (Sentry as any).getReplay = jest.fn().mockReturnValue({
      start: jest.fn(),
      stop: jest.fn(),
    });
  });

  it('should render the button', () => {
    render(
      <CoreI18nProvider>
        <SentryRecordButton />
      </CoreI18nProvider>,
    );
    expect(screen.getByTestId('sentryRecordButton')).toBeTruthy();
  });

  it('should start and stop recording for replay on click', async () => {
    render(
      <CoreI18nProvider>
        <SentryRecordButton />
      </CoreI18nProvider>,
    );
    const recordButton = screen.getByTestId('sentryRecordButton');

    fireEvent.click(recordButton);
    expect(Sentry.getReplay()?.start).toHaveBeenCalledTimes(1);
    expect(Sentry.getReplay()?.stop).not.toHaveBeenCalled();

    fireEvent.click(recordButton);
    await waitFor(
      () => {
        expect(Sentry.getReplay()?.stop).toHaveBeenCalledTimes(1);
      },
      { timeout: 3000 },
    );
  });
});
