/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2023 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { initialize as mswInitialize, mswLoader } from 'msw-storybook-addon';
import { lightTheme, darkTheme } from '@opengeoweb/theme';
import * as previewHelpers from '../../../.storybook/preview';
import { TafThemeWrapper } from '../src/lib/components/Providers';
import { tafEndPoints } from '../src/lib/utils/taf-api/fakeApi';

mswInitialize({
  serviceWorker: {
    url: './mockServiceWorker.js',
  },
});

export const parameters = {
  ...previewHelpers.parameters,
  msw: {
    handlers: {
      taf: tafEndPoints,
    },
  },
  tags: ['autodocs'],
};

export const decorators = [
  (Story, params) => {
    const isDarkTheme = params.tags.indexOf('dark') !== -1 ? true : false;
    return (
      <TafThemeWrapper theme={isDarkTheme ? darkTheme : lightTheme}>
        <Story />
      </TafThemeWrapper>
    );
  },
];

export const loaders = [mswLoader];

export default parameters;
