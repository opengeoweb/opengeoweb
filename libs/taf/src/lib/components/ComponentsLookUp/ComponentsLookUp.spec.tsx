/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { Credentials } from '@opengeoweb/api';
import { render, waitFor, screen } from '@testing-library/react';
import * as React from 'react';
import { setupServer } from 'msw/node';
import { TafThemeApiProvider } from '../Providers';

import {
  ApiModuleProps,
  componentsLookUp,
  ComponentsLookUpPayload,
  TafApiWrapper,
} from './ComponentsLookUp';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { tafEndPoints } from '../../utils/taf-api/fakeApi';

jest.mock('../../utils/api');

const server = setupServer(...tafEndPoints);

describe('components/ComponentsLookUp/componentsLookUp', () => {
  beforeAll(() => server.listen());
  beforeEach(() => {
    jest.useFakeTimers();
    Element.prototype.scrollTo = jest.fn();
  });
  afterEach(() => {
    server.resetHandlers();
    jest.clearAllTimers();
    jest.useRealTimers();
  });
  afterAll(() => server.close());

  it('should return TAF module', () => {
    const testPayload = {
      componentType: 'TafModule',
      id: 'test',
      initialProps: {},
    } as ComponentsLookUpPayload;

    const testConfig = {
      baseURL: 'test',
      appURL: 'test',
      authTokenURL: 'test',
      authClientId: 'test',
    };

    const testAuth: Credentials = {
      username: 'test user',
      token: '',
      refresh_token: '',
    };
    const testOnSetAuth = jest.fn();

    const result = componentsLookUp(testPayload, {
      config: testConfig,
      auth: testAuth,
      onSetAuth: testOnSetAuth,
      initialProps: {},
    });

    expect(result.props['data-testid']).toEqual('tafmodule');
    expect(result.props.config).toEqual(testConfig);
    expect(result.props.auth).toEqual(testAuth);
  });

  it('should render TafApiWrapper with initialProps', async () => {
    const config = {
      baseURL: 'test',
      appURL: 'test',
      authTokenURL: 'test',
      authClientId: 'test',
    };

    const auth: Credentials = {
      username: 'test user',
      token: '',
      refresh_token: '',
    };
    const onSetAuth = jest.fn();
    const props = {
      config,
      auth,
      onSetAuth,
      initialProps: {
        isAutoTafEnabled: true,
      },
    };
    render(
      <TafThemeApiProvider>
        <TafApiWrapper {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(screen.getByTestId('tafmodule')).toBeTruthy();
    });
    await screen.findByText(translateKeyOutsideComponents('autotaf'));
  });

  it('should render TafApiWrapper', async () => {
    const config = {
      baseURL: 'test',
      appURL: 'test',
      authTokenURL: 'test',
      authClientId: 'test',
    };

    const auth: Credentials = {
      username: 'test user',
      token: '',
      refresh_token: '',
    };
    const onSetAuth = jest.fn();
    const props = {
      config,
      auth,
      onSetAuth,
      initialProps: {},
    };
    render(
      <TafThemeApiProvider>
        <TafApiWrapper {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(screen.getByTestId('tafmodule')).toBeTruthy();
    });
  });

  it('should not render TafApiWrapper when config is not provided', async () => {
    const config = undefined!;
    const auth: Credentials = {
      username: 'test user',
      token: '',
      refresh_token: '',
    };
    const onSetAuth = jest.fn();
    const props: ApiModuleProps = {
      config,
      auth,
      onSetAuth,
      initialProps: {},
    };
    render(
      <TafThemeApiProvider>
        <TafApiWrapper {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('tafmodule')).toBeFalsy();
    });
  });
});
