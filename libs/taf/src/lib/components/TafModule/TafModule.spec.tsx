/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  act,
  within,
  screen,
} from '@testing-library/react';
import { AxiosError, AxiosResponseHeaders } from 'axios';

import userEvent from '@testing-library/user-event';
import { dateUtils } from '@opengeoweb/shared';
import { setupServer } from 'msw/node';
import { http, HttpResponse } from 'msw';
import { AuthenticationProvider } from '@opengeoweb/authentication';
import TafModule, { getCurrentDate } from './TafModule';
import { TafThemeApiProvider } from '../Providers';
import {
  fakeDraftTaf,
  fakePublishedFixedTaf,
  fakeDraftFixedTaf,
  fakePublishedTaf,
  MOCK_USERNAME,
} from '../../utils/mockdata/fakeTafList';
import { fakeTestTac, createApi } from '../../utils/__mocks__/api';
import { TafFromBackend } from '../../types';
import { TafApi } from '../../utils/api';
import { tafEndPoints } from '../../utils/taf-api/fakeApi';
import { translateKeyOutsideComponents } from '../../utils/i18n';

const server = setupServer(...tafEndPoints);

describe('components/TafModule/TafModule', () => {
  beforeAll(() => server.listen());
  beforeEach(() => {
    Element.prototype.scrollTo = jest.fn();
    // set current time to 14h so tests work at any moment
    const date = `${dateUtils.dateToString(
      dateUtils.utc(),
      dateUtils.DATE_FORMAT_YEAR,
    )}T14:00:00Z`;

    jest.useFakeTimers().setSystemTime(new Date(date));
  });
  afterEach(() => {
    server.resetHandlers();
    jest.clearAllTimers();
    jest.useRealTimers();
  });
  afterAll(() => server.close());

  it('should load a taf list', async () => {
    render(
      <TafThemeApiProvider createApiFunc={createApi}>
        <TafModule />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(screen.getByTestId('tafmodule')).toBeTruthy();
    });
    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });
    expect(screen.getByTestId('taf-form')).toBeTruthy();
    await waitFor(() => {
      expect(
        screen.queryAllByTestId('adverse-weather-tac')[0].textContent,
      ).toContain(fakeTestTac);
    });
  });

  describe('doing a patch request to the backend', () => {
    it('should do a patch request when switching from editor to viewer mode and refetch the list when done', async () => {
      const mockPostTaf = jest.fn(() => {
        return new Promise<void>((resolve) => {
          setTimeout(() => {
            resolve();
          }, 1000);
        });
      });
      const mockPatchTaf = jest.fn();
      const mockGetTafList = jest
        .fn()
        .mockResolvedValue(
          HttpResponse.json([
            { ...fakePublishedFixedTaf, editor: MOCK_USERNAME },
          ]),
        );
      server.use(http.get('taflist', mockGetTafList));
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        patchTaf: mockPatchTaf,
        postTaf: mockPostTaf,
      });
      render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      const user = userEvent.setup({
        advanceTimers: jest.advanceTimersByTime,
      });

      // wait for loading the list to be done
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          within(screen.getByTestId('location-tabs')).getAllByTestId(
            'status-upcoming',
          ).length,
        ).toBeTruthy(),
      );

      // open first published taf
      await user.click(
        within(screen.getByTestId('location-tabs')).getAllByTestId(
          'status-upcoming',
        )[0],
      );

      // make sure we are in editor mode
      expect(screen.queryByTestId('switchMode')!.classList).toContain(
        'Mui-checked',
      );

      // switch mode
      await user.click(
        within(screen.getByTestId('switchMode')).getByRole('checkbox'),
      );
      // get list action should be called
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(3));

      // patch action should be called
      await waitFor(() =>
        expect(mockPatchTaf).toHaveBeenCalledWith({
          ...fakePublishedFixedTaf,
          editor: '',
        }),
      );
    });

    it('should do a patch request when switching from viewer to editor mode and refetch the list when done', async () => {
      const mockPostTaf = jest.fn(() => {
        return new Promise<void>((resolve) => {
          setTimeout(() => {
            resolve();
          }, 1000);
        });
      });
      const mockPatchTaf = jest.fn();

      const mockGetTafList = jest
        .fn()
        .mockResolvedValue(
          HttpResponse.json([{ ...fakePublishedFixedTaf, editor: null! }]),
        );
      server.use(http.get('taflist', mockGetTafList));
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        patchTaf: mockPatchTaf,
        postTaf: mockPostTaf,
      });
      render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          within(screen.getByTestId('location-tabs')).getAllByTestId(
            'status-upcoming',
          ).length,
        ).toBeTruthy(),
      );
      const user = userEvent.setup({ advanceTimers: jest.advanceTimersByTime });

      // open first published taf
      await user.click(
        within(screen.getByTestId('location-tabs')).getAllByTestId(
          'status-upcoming',
        )[0],
      );

      // make sure we are in viewer mode
      expect(screen.queryByTestId('switchMode')!.classList).not.toContain(
        'Mui-checked',
      );

      // switch mode
      await user.click(
        within(screen.getByTestId('switchMode')).getByRole('checkbox'),
      );

      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(3));

      // patch action should be called
      await waitFor(() =>
        expect(mockPatchTaf).toHaveBeenCalledWith({
          ...fakePublishedFixedTaf,
          editor: MOCK_USERNAME,
        }),
      );
    });
  });

  it('should show a dialog when switching from viewer to editor mode and editor is already asigned and refresh tafList', async () => {
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        setTimeout(() => {
          resolve();
        }, 1000);
      });
    });
    const mockPatchTaf = jest.fn();

    const mockGetTafList = jest
      .fn()
      .mockResolvedValue(
        HttpResponse.json([
          { ...fakePublishedFixedTaf, editor: 'micheal.jackson' },
        ]),
      );
    server.use(http.get('taflist', mockGetTafList));
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      patchTaf: mockPatchTaf,
      postTaf: mockPostTaf,
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafModule />
      </TafThemeApiProvider>,
    );

    // wait for loading the list to be done
    await waitFor(() =>
      expect(screen.queryByTestId('loading-bar')).toBeFalsy(),
    );
    await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
    await waitFor(() =>
      expect(
        within(screen.getByTestId('location-tabs')).getAllByTestId(
          'status-upcoming',
        ).length,
      ).toEqual(1),
    );

    // make sure we are in viewer mode
    await waitFor(() =>
      expect(screen.queryByTestId('switchMode')!.classList).not.toContain(
        'Mui-checked',
      ),
    );

    // switch mode
    // eslint-disable-next-line testing-library/no-unnecessary-act
    await act(async () => {
      fireEvent.click(
        within(screen.getByTestId('switchMode')).getByRole('checkbox'),
      );
    });

    await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(2));

    expect(
      await screen.findByText(
        'This TAF is already in edit mode and important information could get lost in the switching process.',
      ),
    ).toBeTruthy();

    // confirm takeover
    fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);

    // patch action should be called
    await waitFor(() =>
      expect(mockPatchTaf).toHaveBeenCalledWith({
        ...fakePublishedFixedTaf,
        editor: MOCK_USERNAME,
      }),
    );

    // get list action should be called
    await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(3));
  });

  it('should show a dialog when switching from viewer to editor mode and editor is already asigned and not refresh tafList after cancelling', async () => {
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        setTimeout(() => {
          resolve();
        }, 1000);
      });
    });
    const mockPatchTaf = jest.fn();
    const mockGetTafList = jest
      .fn()
      .mockResolvedValue(
        HttpResponse.json([
          { ...fakePublishedFixedTaf, editor: 'micheal.jackson' },
        ]),
      );
    server.use(http.get('taflist', mockGetTafList));
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      patchTaf: mockPatchTaf,
      postTaf: mockPostTaf,
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafModule />
      </TafThemeApiProvider>,
    );

    // wait for loading the list to be done
    await waitFor(() =>
      expect(screen.queryByTestId('loading-bar')).toBeFalsy(),
    );
    await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
    await waitFor(() =>
      expect(
        within(screen.getByTestId('location-tabs')).getAllByTestId(
          'status-upcoming',
        ).length,
      ).toEqual(1),
    );

    // make sure we are in viewer mode
    await waitFor(() =>
      expect(screen.queryByTestId('switchMode')!.classList).not.toContain(
        'Mui-checked',
      ),
    );

    // eslint-disable-next-line testing-library/no-unnecessary-act
    await act(async () => {
      // switch mode
      fireEvent.click(
        within(screen.getByTestId('switchMode')).getByRole('checkbox'),
      );
    });

    await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(2));

    expect(
      await screen.findByText(
        'This TAF is already in edit mode and important information could get lost in the switching process.',
      ),
    ).toBeTruthy();

    // cancel takeover
    fireEvent.click(screen.queryByTestId('confirmationDialog-cancel')!);

    // patch action should not be called
    expect(mockPatchTaf).not.toHaveBeenCalled();

    // get list action should not be called again
    expect(mockGetTafList).toHaveBeenCalledTimes(2);
  });

  describe('ensure error and refetch works correctly', () => {
    it('should show error when user is not logged in', async () => {
      render(
        <TafThemeApiProvider createApiFunc={createApi}>
          <AuthenticationProvider
            value={{
              isLoggedIn: false,
              auth: null,
              onLogin: (): void => null!,
              onSetAuth: (): void => null!,
              sessionStorageProvider: null!,
            }}
          >
            <TafModule />
          </AuthenticationProvider>
        </TafThemeApiProvider>,
      );

      await screen.findByTestId('alert-banner');
      expect(
        screen.getByText(translateKeyOutsideComponents('error-auth')),
      ).toBeTruthy();
      expect(screen.queryByText('TRY AGAIN')).toBeFalsy();
    });
    it('should show error when fetching the list fails and user should be able to try again', async () => {
      const mockGetTafList = jest
        .fn()
        .mockRejectedValue(
          HttpResponse.json({ message: 'error' }, { status: 500 }),
        );
      server.use(http.get('taflist', mockGetTafList));

      render(
        <TafThemeApiProvider createApiFunc={createApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      await waitFor(() =>
        expect(screen.queryByTestId('loading-bar')).toBeFalsy(),
      );
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await screen.findByTestId('alert-banner');

      // user should be able to try again
      fireEvent.click(screen.getByText('TRY AGAIN'));

      await waitFor(() =>
        expect(screen.queryByTestId('error-message')).toBeFalsy(),
      );
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(2));
    });

    it('should refresh values of current taf in viewer mode', async () => {
      let counter = 0;
      const testTaf = { ...fakeDraftFixedTaf, editor: '' };
      const emptyTestTaf: TafFromBackend = {
        ...testTaf,
        taf: {
          ...testTaf.taf,
          baseForecast: {
            valid: testTaf.taf.baseForecast.valid,
          },
          changeGroups: [],
        },
      };
      // returns empty taf every second fetch to simulate new data
      const mockGetTafList = jest.fn().mockImplementation(() => {
        counter += 1;
        const data = counter % 2 === 0 ? [emptyTestTaf] : [testTaf];
        return HttpResponse.json(data);
      });
      server.use(http.get('taflist', mockGetTafList));

      render(
        <TafThemeApiProvider createApiFunc={createApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      await waitFor(() =>
        expect(screen.queryByTestId('loading-bar')).toBeFalsy(),
      );
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await screen.findByTestId('location-tabs');

      expect(screen.queryByTestId('lastupdated-time')!.textContent).toEqual(
        `Last updated ${getCurrentDate()}`,
      );

      const windField = screen.getByRole('textbox', {
        name: 'baseForecast.wind',
      });

      expect(windField!.getAttribute('disabled')).toBeDefined();
      expect(windField!.getAttribute('value')).toEqual('15005');

      // update taf list
      fireEvent.click(screen.queryByTestId('refresh-button')!);
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(2));
      expect(windField!.getAttribute('disabled')).toBeDefined();
      await waitFor(() =>
        expect(windField!.getAttribute('value')).toEqual('-'),
      );
      // update taf list again
      fireEvent.click(screen.queryByTestId('refresh-button')!);
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(3));
      expect(windField!.getAttribute('disabled')).toBeDefined();
      await waitFor(() =>
        expect(windField!.getAttribute('value')).toEqual('15005'),
      );
    });

    it('should not refresh values of current taf in editor mode', async () => {
      const list = [{ ...fakeDraftFixedTaf, editor: MOCK_USERNAME }];
      const emptyList: TafFromBackend[] = list.map((tafFromBackend) => ({
        ...tafFromBackend,
        editor: MOCK_USERNAME,
        taf: {
          ...tafFromBackend.taf,
          baseForecast: {
            valid: tafFromBackend.taf.baseForecast.valid,
          },
          changeGroups: [],
        },
      }));

      let counter = 0;
      // returns different list every time it fetches to simulate new data
      const mockGetTafList = jest.fn().mockImplementation(() => {
        counter += 1;
        const data = counter % 2 === 0 ? emptyList : list;
        return HttpResponse.json(data);
      });
      server.use(http.get('taflist', mockGetTafList));

      render(
        <TafThemeApiProvider createApiFunc={createApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      await waitFor(() =>
        expect(screen.queryByTestId('loading-bar')).toBeFalsy(),
      );
      await screen.findByTestId('location-tabs');
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));

      expect(screen.queryByTestId('lastupdated-time')!.textContent).toEqual(
        `Last updated ${getCurrentDate()}`,
      );

      const windField = screen.getByRole('textbox', {
        name: 'baseForecast.wind',
      });
      expect(windField!.getAttribute('disabled')).toBeDefined();
      expect(windField!.getAttribute('value')).toEqual('15005');

      // make sure we are in editor mode
      expect(screen.queryByTestId('switchMode')!.classList).toContain(
        'Mui-checked',
      );
      expect(windField!.getAttribute('disabled')).toBeNull();

      const newValue = '18050';
      fireEvent.change(windField!, { target: { value: newValue } });
      await waitFor(() =>
        expect(windField!.getAttribute('value')).toEqual(newValue),
      );

      // update taf list
      fireEvent.click(screen.queryByTestId('refresh-button')!);
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(2));
      await waitFor(() =>
        expect(windField!.getAttribute('value')).toEqual(newValue),
      );

      // update taf list again
      fireEvent.click(screen.queryByTestId('refresh-button')!);
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(3));
      await waitFor(() =>
        expect(windField!.getAttribute('value')).toEqual(newValue),
      );
    });
  });

  describe('showing spinner when doing a post request to the backend', () => {
    it('should show the spinner when cancelling a taf from the list and refetch the list when done', async () => {
      const mockPostTaf = jest.fn(() => {
        return new Promise<void>((resolve) => {
          setTimeout(() => {
            resolve();
          }, 1000);
        });
      });

      const mockGetTafList = jest
        .fn()
        .mockResolvedValue(
          HttpResponse.json([
            { ...fakePublishedFixedTaf, editor: MOCK_USERNAME },
          ]),
        );
      server.use(http.get('taflist', mockGetTafList));
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        postTaf: mockPostTaf,
      });
      render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() =>
        expect(screen.queryByTestId('loading-bar')).toBeFalsy(),
      );
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          within(screen.getByTestId('location-tabs')).getAllByTestId(
            'status-upcoming',
          ).length,
        ).toEqual(1),
      );

      // make sure we are in editor mode
      expect(screen.queryByTestId('switchMode')!.classList).toContain(
        'Mui-checked',
      );

      await waitFor(() => {
        expect(screen.getByTestId('canceltaf')).toBeTruthy();
      });

      const user = userEvent.setup({ advanceTimers: jest.advanceTimersByTime });
      // click cancel
      await user.click(screen.queryByTestId('canceltaf')!);

      // click confirm in the confirmation dialog
      const confirmButton = screen.getByTestId('confirmationDialog-confirm');
      fireEvent.click(confirmButton!);

      // confirmation dialog should be closed
      await waitFor(() => {
        expect(screen.queryByTestId('confirmationDialog')).toBeFalsy();
      });

      // spinner should be shown
      await waitFor(() => {
        expect(screen.getByTestId('taf-loading')).toBeTruthy();
      });

      // post action should be called
      expect(mockPostTaf).toHaveBeenCalledWith({
        changeStatusTo: 'CANCELLED',
        taf: expect.any(Object),
      });
      expect(mockGetTafList).toHaveBeenCalledTimes(1);

      act(() => {
        jest.advanceTimersToNextTimer();
      });

      // spinner should be gone
      await waitFor(() => {
        expect(screen.queryByTestId('taf-loading')).toBeFalsy();
      });
      expect(screen.queryByTestId('taf-error')).toBeFalsy();

      // get list action should be called
      expect(mockGetTafList).toHaveBeenCalledTimes(2);
    });

    it('should show the spinner when publishing a taf from the dialog and refetch the list when done', async () => {
      const mockPostTaf = jest.fn(() => {
        return new Promise<void>((resolve) => {
          setTimeout(() => {
            resolve();
          }, 1000);
        });
      });
      const mockGetTafList = jest
        .fn()
        .mockResolvedValue(
          HttpResponse.json([{ ...fakeDraftTaf, editor: MOCK_USERNAME }]),
        );
      server.use(http.get('taflist', mockGetTafList));
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        postTaf: mockPostTaf,
      });
      render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() =>
        expect(screen.queryByTestId('loading-bar')).toBeFalsy(),
      );
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          within(screen.getByTestId('location-tabs')).getAllByTestId(
            'status-draft',
          ).length,
        ).toEqual(1),
      );

      // make sure we are in editor mode
      await waitFor(() =>
        expect(screen.queryByTestId('switchMode')!.classList).toContain(
          'Mui-checked',
        ),
      );

      await waitFor(() => {
        expect(screen.queryByTestId('taf-panel-active')!.textContent).toContain(
          'draft',
        );
      });
      expect(screen.getByTestId('publishtaf')).toBeTruthy();
      expect(screen.getByText('0 issues')).toBeTruthy();
      await waitFor(() => {
        // form should be in edit mode
        expect(
          screen.getByRole('textbox', { name: 'baseForecast.wind' }).classList,
        ).not.toContain('Mui-disabled');
      });

      const user = userEvent.setup({
        advanceTimers: jest.advanceTimersByTime,
      });
      // click publish
      await user.click(screen.queryByTestId('publishtaf')!);
      await waitFor(() =>
        expect(screen.queryByTestId('issuesButton')!.textContent).toContain(
          '0 issues',
        ),
      );

      const confirmButton = await screen.findByTestId(
        'confirmationDialog-confirm',
      );
      await user.click(confirmButton!);
      // confirmation dialog should be closed
      await waitFor(() => {
        expect(screen.queryByTestId('confirmationDialog')).toBeFalsy();
      });

      // spinner should be shown
      await waitFor(() => {
        expect(screen.getByTestId('taf-loading')).toBeTruthy();
      });

      // post action should be called
      expect(mockPostTaf).toHaveBeenCalled();
      expect(mockGetTafList).toHaveBeenCalledTimes(1);

      act(() => {
        jest.advanceTimersToNextTimer();
      });

      // spinner should be gone
      await waitFor(() => {
        expect(screen.queryByTestId('taf-loading')).toBeFalsy();
      });
      expect(screen.queryByTestId('taf-error')).toBeFalsy();

      expect(
        screen.getByRole('textbox', { name: 'baseForecast.wind' }).classList,
      ).toContain('Mui-disabled');

      // get list action should be called
      expect(mockGetTafList).toHaveBeenCalledTimes(2);
    });
  });

  describe('show error message when post to BE fails', () => {
    it('should show the error message when cancelling a taf from the list and disappear after 10 secs', async () => {
      const mockPostTaf = jest.fn(() => {
        return new Promise<void>((resolve, reject) => {
          setTimeout(() => {
            reject(new Error('something went wrong'));
          }, 1000);
        });
      });

      const mockGetTafList = jest
        .fn()
        .mockResolvedValue(
          HttpResponse.json([
            { ...fakePublishedFixedTaf, editor: MOCK_USERNAME },
          ]),
        );
      server.use(http.get('taflist', mockGetTafList));
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        postTaf: mockPostTaf,
      });
      render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() =>
        expect(screen.queryByTestId('loading-bar')).toBeFalsy(),
      );
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          within(screen.getByTestId('location-tabs')).getAllByTestId(
            'status-upcoming',
          ).length,
        ).toEqual(1),
      );

      // make sure we are in editor mode
      expect(screen.queryByTestId('switchMode')!.classList).toContain(
        'Mui-checked',
      );

      await waitFor(() => {
        expect(screen.getByTestId('canceltaf')).toBeTruthy();
      });

      // click cancel
      fireEvent.click(screen.queryByTestId('canceltaf')!);

      // click cancel in the confirmation dialog

      fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);
      // confirmation dialog should be closed
      await waitFor(() => {
        expect(screen.queryByTestId('confirmationDialog')).toBeFalsy();
      });

      // spinner should be shown
      await waitFor(() => {
        expect(screen.getByTestId('taf-loading')).toBeTruthy();
      });

      await act(async () => {
        jest.advanceTimersByTime(1000);
      });
      await waitFor(() => {
        expect(screen.queryByTestId('taf-loading')).toBeFalsy();
      });
      // alert should be shown
      expect(screen.getByTestId('taf-error')).toBeTruthy();

      await act(async () => {
        jest.advanceTimersByTime(10000);
      });
      // alert should be gone
      expect(screen.queryByTestId('taf-error')).toBeFalsy();
      // we should still be in editor mode
      expect(screen.queryByTestId('switchMode')!.classList).toContain(
        'Mui-checked',
      );
    });

    it('should not show the error message when successfully cancelling a taf from the list', async () => {
      const mockPostTaf = jest.fn();
      const mockGetTafList = jest
        .fn()
        .mockResolvedValue(
          HttpResponse.json([
            { ...fakePublishedFixedTaf, editor: MOCK_USERNAME },
          ]),
        );
      server.use(http.get('taflist', mockGetTafList));
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        postTaf: mockPostTaf,
      });
      render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() =>
        expect(screen.queryByTestId('loading-bar')).toBeFalsy(),
      );
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          within(screen.getByTestId('location-tabs')).getAllByTestId(
            'status-upcoming',
          ).length,
        ).toEqual(1),
      );

      // make sure we are in editor mode
      expect(screen.queryByTestId('switchMode')!.classList).toContain(
        'Mui-checked',
      );

      await waitFor(() => {
        expect(screen.getByTestId('canceltaf')).toBeTruthy();
      });

      // click cancel
      fireEvent.click(screen.queryByTestId('canceltaf')!);

      // click cancel in the confirmation dialog
      fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);

      // confirmation dialog should be closed
      await waitFor(() => {
        expect(screen.queryByTestId('confirmationDialog')).toBeFalsy();
      });

      await waitFor(() => expect(mockPostTaf).toHaveBeenCalledTimes(1));
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(2));

      // alert should not be there
      await waitFor(() => {
        expect(screen.queryByTestId('taf-error')).toBeFalsy();
      });
    });

    it('should show the error message when posting a taf to backend returns error', async () => {
      const mockPostTaf = jest.fn(() => {
        return new Promise<void>((resolve, reject) => {
          setTimeout(() => {
            reject(new Error('something went wrong'));
          }, 1000);
        });
      });
      const mockGetTafList = jest
        .fn()
        .mockResolvedValue(
          HttpResponse.json([{ ...fakeDraftTaf, editor: MOCK_USERNAME }]),
        );
      server.use(http.get('taflist', mockGetTafList));

      const createFakeApi = (): TafApi => ({
        ...createApi(),
        postTaf: mockPostTaf,
      });
      render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() =>
        expect(screen.queryByTestId('loading-bar')).toBeFalsy(),
      );
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          within(screen.getByTestId('location-tabs')).getAllByTestId(
            'status-draft',
          ).length,
        ).toEqual(1),
      );

      // make sure we are in editor mode
      expect(screen.queryByTestId('switchMode')!.classList).toContain(
        'Mui-checked',
      );

      await waitFor(() => {
        expect(screen.getByTestId('publishtaf')).toBeTruthy();
      });
      // form should be in edit mode
      expect(
        screen.getByRole('textbox', { name: 'baseForecast.wind' }).classList,
      ).not.toContain('Mui-disabled');

      // click publish taf
      fireEvent.click(screen.queryByTestId('publishtaf')!);

      // confirm in the confirmation dialog
      await waitFor(() => {
        // eslint-disable-next-line testing-library/prefer-presence-queries
        expect(screen.queryByTestId('confirmationDialog-confirm')).toBeTruthy();
      });
      fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);

      // confirmation dialog should be closed
      await waitFor(() => {
        expect(screen.queryByTestId('confirmationDialog')).toBeFalsy();
      });

      // spinner should be shown
      await waitFor(() => {
        expect(screen.getByTestId('taf-loading')).toBeTruthy();
      });

      await act(async () => {
        jest.advanceTimersByTime(1000);
      });
      await waitFor(() => {
        expect(screen.queryByTestId('taf-loading')).toBeFalsy();
      });
      // alert should be shown
      expect(screen.getByTestId('taf-error')).toBeTruthy();

      // form should still be in edit mode
      expect(screen.queryByTestId('switchMode')!.classList).toContain(
        'Mui-checked',
      );
      expect(
        screen.getByRole('textbox', { name: 'baseForecast.wind' }).classList,
      ).not.toContain('Mui-disabled');

      // user should be able to close the alert
      fireEvent.click(screen.getByText('CLOSE'));
      await waitFor(() =>
        expect(screen.queryByTestId('taf-error')).toBeFalsy(),
      );
    });

    it('should show the error message for a takeover and refresh the list when posting a taf to backend returns takeover error', async () => {
      interface ResponseData {
        WarningMessage: string;
      }

      const fakeBackendTakeOverWarning: AxiosError<ResponseData> = {
        isAxiosError: true,
        config: undefined!,
        toJSON: undefined!,
        name: 'API error',
        message: '',
        response: {
          data: {
            WarningMessage: 'You are no longer the editor for this TAF',
          },
          status: 400,
          statusText: '',
          config: undefined!,
          headers: [] as unknown as AxiosResponseHeaders,
        },
      };

      const mockPostTaf = jest.fn(() => {
        return new Promise<void>((resolve, reject) => {
          setTimeout(() => {
            reject(fakeBackendTakeOverWarning);
          }, 1000);
        });
      });
      const mockGetTafList = jest
        .fn()
        .mockResolvedValue(
          HttpResponse.json([{ ...fakeDraftTaf, editor: MOCK_USERNAME }]),
        );
      server.use(http.get('taflist', mockGetTafList));
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        postTaf: mockPostTaf,
      });
      render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() =>
        expect(screen.queryByTestId('loading-bar')).toBeFalsy(),
      );
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          within(screen.getByTestId('location-tabs')).getAllByTestId(
            'status-draft',
          ).length,
        ).toEqual(1),
      );

      // make sure we are in editor mode
      expect(screen.queryByTestId('switchMode')!.classList).toContain(
        'Mui-checked',
      );

      await waitFor(() => {
        expect(screen.getByTestId('publishtaf')).toBeTruthy();
      });
      // form should be in edit mode
      expect(
        screen.getByRole('textbox', { name: 'baseForecast.wind' }).classList,
      ).not.toContain('Mui-disabled');

      // click publish taf
      fireEvent.click(screen.queryByTestId('publishtaf')!);

      // confirm in the confirmation dialog
      await waitFor(() => {
        expect(
          // eslint-disable-next-line testing-library/prefer-presence-queries
          screen.queryByTestId('confirmationDialog-confirm')!,
        ).toBeTruthy();
      });

      fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);

      // confirmation dialog should be closed
      await waitFor(() => {
        expect(screen.queryByTestId('confirmationDialog')).toBeFalsy();
      });

      // spinner should be shown
      await waitFor(() => {
        expect(screen.getByTestId('taf-loading')).toBeTruthy();
      });

      await act(async () => {
        jest.advanceTimersByTime(1000);
      });
      await waitFor(() => {
        expect(screen.queryByTestId('taf-loading')).toBeFalsy();
      });
      // alert should be shown
      expect(screen.getByTestId('taf-error')).toBeTruthy();
      expect(
        screen.getByText(
          fakeBackendTakeOverWarning.response!.data.WarningMessage,
        ),
      ).toBeTruthy();

      // list should be reloaded
      expect(mockGetTafList).toHaveBeenCalledTimes(2);

      // user should be able to close the alert
      fireEvent.click(screen.getByText('CLOSE'));
      await waitFor(() =>
        expect(screen.queryByTestId('taf-error')).toBeFalsy(),
      );
    });
  });

  describe('show error message when patch to BE fails', () => {
    it('should show the error message when patching a taf to backend returns error', async () => {
      const mockPatchTaf = jest.fn(() => {
        return new Promise<void>((resolve, reject) => {
          setTimeout(() => {
            reject(new Error('Patch went wrong'));
          }, 1000);
        });
      });
      const mockGetTafList = jest
        .fn()
        .mockResolvedValue(
          HttpResponse.json([
            { ...fakeDraftTaf, editor: MOCK_USERNAME },
            fakePublishedTaf,
          ]),
        );
      server.use(http.get('taflist', mockGetTafList));
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        patchTaf: mockPatchTaf,
      });
      render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() =>
        expect(screen.queryByTestId('loading-bar')).toBeFalsy(),
      );
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          within(screen.getByTestId('location-tabs')).getAllByTestId(
            'status-draft',
          ).length,
        ).toEqual(1),
      );

      // make sure we are in editor mode
      expect(screen.queryByTestId('switchMode')!.classList).toContain(
        'Mui-checked',
      );

      await waitFor(() => {
        expect(screen.getByTestId('publishtaf')).toBeTruthy();
      });
      // form should be in edit mode
      expect(
        screen.getByRole('textbox', { name: 'baseForecast.wind' }).classList,
      ).not.toContain('Mui-disabled');

      // switch to viewer mode
      // eslint-disable-next-line testing-library/no-unnecessary-act
      await act(async () => {
        fireEvent.click(
          within(screen.getByTestId('switchMode')).getByRole('checkbox'),
        );
      });

      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(2));
      await waitFor(() => expect(mockPatchTaf).toHaveBeenCalledTimes(1));

      await act(async () => {
        jest.advanceTimersByTime(1000);
      });

      // alert should be shown
      await waitFor(() => {
        expect(screen.getByTestId('taf-error')).toBeTruthy();
      });

      // form should still be in edit mode
      expect(screen.queryByTestId('switchMode')!.classList).toContain(
        'Mui-checked',
      );
      expect(
        screen.getByRole('textbox', { name: 'baseForecast.wind' }).classList,
      ).not.toContain('Mui-disabled');

      // user should be able to close the alert
      fireEvent.click(screen.getByText('CLOSE'));
      await waitFor(() =>
        expect(screen.queryByTestId('taf-error')).toBeFalsy(),
      );

      // when trying again alert should be shown again
      // eslint-disable-next-line testing-library/no-unnecessary-act
      await act(async () => {
        fireEvent.click(
          within(screen.getByTestId('switchMode')).getByRole('checkbox'),
        );
      });
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(3));
      await waitFor(() => expect(mockPatchTaf).toHaveBeenCalledTimes(2));

      await act(async () => {
        jest.advanceTimersByTime(1000);
      });

      await waitFor(() => {
        expect(screen.getByTestId('taf-error')).toBeTruthy();
      });

      // change location
      const locations = within(
        screen.getByTestId('location-tabs'),
      ).getAllByRole('menuitem');

      const lastLocation = locations[locations.length - 1];
      fireEvent.click(lastLocation);

      // the alert should be gone
      await waitFor(() => {
        expect(screen.queryByTestId('taf-error')).toBeFalsy();
      });
    });

    it('should show the error for the last action that went wrong', async () => {
      const mockPostTaf = jest.fn(() => {
        return new Promise<void>((resolve, reject) => {
          setTimeout(() => {
            reject(new Error('Post went wrong'));
          }, 1000);
        });
      });
      const mockPatchTaf = jest.fn(() => {
        return new Promise<void>((resolve, reject) => {
          setTimeout(() => {
            reject(new Error('Patch went wrong'));
          }, 1000);
        });
      });
      const mockGetTafList = jest
        .fn()
        .mockResolvedValue(
          HttpResponse.json([{ ...fakeDraftTaf, editor: MOCK_USERNAME }]),
        );
      server.use(http.get('taflist', mockGetTafList));
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        patchTaf: mockPatchTaf,
        postTaf: mockPostTaf,
      });
      render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() =>
        expect(screen.queryByTestId('loading-bar')).toBeFalsy(),
      );
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          within(screen.getByTestId('location-tabs')).getAllByTestId(
            'status-draft',
          ).length,
        ).toEqual(1),
      );

      // make sure we are in editor mode
      expect(screen.queryByTestId('switchMode')!.classList).toContain(
        'Mui-checked',
      );

      await waitFor(() => {
        expect(screen.getByTestId('publishtaf')).toBeTruthy();
      });
      // form should be in edit mode
      expect(
        screen.getByRole('textbox', { name: 'baseForecast.wind' }).classList,
      ).not.toContain('Mui-disabled');

      // switch to viewer mode
      // eslint-disable-next-line testing-library/no-unnecessary-act
      await act(async () => {
        fireEvent.click(
          within(screen.getByTestId('switchMode')).getByRole('checkbox'),
        );
      });
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(2));
      await waitFor(() => expect(mockPatchTaf).toHaveBeenCalledTimes(1));

      await act(async () => {
        jest.advanceTimersByTime(1000);
      });

      // alert should be shown
      await waitFor(() => {
        expect(screen.getByTestId('taf-error')).toBeTruthy();
      });
      expect(screen.getByText('Patch went wrong')).toBeTruthy();

      // when publishing fails a new alert should be shown
      fireEvent.click(screen.queryByTestId('publishtaf')!);

      // confirm in the confirmation dialog
      await waitFor(() => {
        expect(
          // eslint-disable-next-line testing-library/prefer-presence-queries
          screen.queryByTestId('confirmationDialog-confirm')!,
        ).toBeTruthy();
      });
      fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);
      // confirmation dialog should be closed
      await waitFor(() => {
        expect(screen.queryByTestId('confirmationDialog')).toBeFalsy();
      });

      // spinner should be shown
      await waitFor(() => {
        expect(screen.getByTestId('taf-loading')).toBeTruthy();
      });

      await act(async () => {
        jest.advanceTimersByTime(1000);
      });
      await waitFor(() => {
        expect(screen.queryByTestId('taf-loading')).toBeFalsy();
      });
      // alert should be shown
      expect(screen.getByTestId('taf-error')).toBeTruthy();
      expect(screen.getByText('Post went wrong')).toBeTruthy();
      expect(screen.queryByText('Patch went wrong')).toBeFalsy();
    });
  });
});
