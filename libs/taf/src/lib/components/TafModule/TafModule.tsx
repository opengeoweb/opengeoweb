/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { AlertBanner, dateUtils } from '@opengeoweb/shared';
import * as React from 'react';
import LinearProgress from '@mui/material/LinearProgress';
import { Box } from '@mui/material';
import { useAuthenticationContext } from '@opengeoweb/authentication';
import TafLayout from '../TafLayout/TafLayout';
import { TafWrapper } from '../TafWrapper';
import { TafModuleProviderProps } from './TafModuleProvider';
import { ResponsiveWrapper } from '../ResponsiveWrapper/ResponsiveWrapper';
import { useTafTranslation } from '../../utils/i18n';
import { useTafList } from '../../utils/taf-api/hooks';

export const getCurrentDate = (): string =>
  dateUtils.dateToString(dateUtils.utc(), dateUtils.DATE_FORMAT_HOURS)!;

const TafModule: React.FC<{ isAutoTafEnabled: boolean }> = ({
  isAutoTafEnabled,
}) => {
  const { t } = useTafTranslation();
  const [errorMessage, setErrorMessage] = React.useState<{
    title: string;
    info?: string;
  } | null>(null);
  const { isLoggedIn } = useAuthenticationContext();
  const ERROR_AUTH = t('error-auth');
  const ERROR_AUTH_MESSAGE = t('error-auth-message');

  const {
    isFetching,
    error: tafListError,
    data: tafList,
    dataUpdatedAt,
    refetch: fetchNewTafList,
  } = useTafList();

  const lastUpdateTime = dateUtils.dateToString(
    new Date(dataUpdatedAt),
    dateUtils.DATE_FORMAT_HOURS,
  );

  const onUpdateTaf = async (): Promise<void> => {
    await fetchNewTafList();
  };

  React.useEffect(() => {
    if (!isLoggedIn) {
      setErrorMessage({
        title: ERROR_AUTH,
        info: ERROR_AUTH_MESSAGE,
      });
    } else if (tafListError) {
      setErrorMessage({
        title: t('error-list'),
        info: tafListError.message,
      });
    } else {
      setErrorMessage(null);
    }
  }, [t, tafListError, isLoggedIn, ERROR_AUTH, ERROR_AUTH_MESSAGE]);

  return (
    <ResponsiveWrapper>
      <Box
        component="div"
        id="tafmodule"
        data-testid="tafmodule"
        sx={{
          height: '100%',
        }}
      >
        {isLoggedIn && isFetching && (
          <LinearProgress
            data-testid="loading-bar"
            color="secondary"
            sx={{ marginBottom: '-4px' }}
          />
        )}
        {errorMessage ? (
          <AlertBanner
            severity="error"
            title={errorMessage.title}
            info={errorMessage.info}
            data-testid="error-message"
            actionButtonProps={
              errorMessage.title !== ERROR_AUTH
                ? {
                    title: t('error-title-retry'),
                    onClick: onUpdateTaf,
                  }
                : undefined
            }
          />
        ) : null}
        <TafLayout
          tafList={tafList || []}
          onUpdateTaf={onUpdateTaf}
          lastUpdateTime={lastUpdateTime}
          isAutoTafEnabled={isAutoTafEnabled}
        />
      </Box>
    </ResponsiveWrapper>
  );
};

const Wrapper: React.FC<
  TafModuleProviderProps & { isAutoTafEnabled?: boolean }
> = ({ isAutoTafEnabled = false, ...props }) => {
  return (
    <TafWrapper {...props}>
      <TafModule isAutoTafEnabled={isAutoTafEnabled} />
    </TafWrapper>
  );
};

export default Wrapper;
