/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { FieldError } from 'react-hook-form';
import { Paper } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import IssuesPane from './IssuesPane';
import IssuesButton from './IssuesButton';
import { CustomErrorField } from './types';

const multipleErrors = [
  'Wind: incorrect format (e.g. format: 24015G25)',
  'Visibility: incorrect format',
  'Wind: incorrect format (e.g. format: 24015G25)',
  'Visibility: incorrect format',
  'Wind: incorrect format (e.g. format: 24015G25)',
  'Visibility: incorrect format',
  'Wind: incorrect format (e.g. format: 24015G25)',
  'Visibility: incorrect format',
].map((error) => ({
  ref: { name: 'test' },
  message: error,
  isChangeGroup: false,
  rowIndex: -1,
})) as CustomErrorField[];

const meta: Meta<typeof IssuesPane> = {
  title: 'components/Issues Pane',
  component: IssuesPane,
  parameters: {
    docs: {
      description: {
        component:
          'Issue pane showing the errors of the taf form if there are any. Press the isOpen toggle button to show the dialog',
      },
    },
  },
  tags: ['!autodocs'],
};
export default meta;

type Story = StoryObj<typeof IssuesPane>;

export const Component: Story = {
  args: {
    isOpen: false,
    errorList: multipleErrors,
    startPosition: {
      top: 260,
      left: 60,
    },
  },
  render: (props) => (
    <Paper sx={{ height: '300px' }}>
      <IssuesPane {...props} />
    </Paper>
  ),
  tags: ['!dev'],
};

export const NoIssues: Story = {
  render: () => (
    <Paper sx={{ height: '300px', width: '700px' }}>
      <IssuesButton />
      <IssuesPane
        // eslint-disable-next-line no-console
        handleClose={(): void => console.log('close')}
        errorList={[]}
        isOpen
      />
    </Paper>
  ),
  tags: ['snapshot', '!autodocs'],
};

const windError = {
  type: 'test wind error',
  ref: {
    name: 'wind',
  },
  message: 'incorrect format (e.g. format: 24015G25)',
  isChangeGroup: false,
  rowIndex: -1,
};
const oneError = {
  baseForecast: {
    wind: windError,
  },
} as unknown as Record<string, FieldError>;

export const OneIssue: Story = {
  render: () => (
    <Paper sx={{ height: '300px', width: '700px' }}>
      <IssuesButton errors={oneError} />
      <IssuesPane
        // eslint-disable-next-line no-console
        handleClose={(): void => console.log('close')}
        errorList={[windError]}
        isOpen
      />
    </Paper>
  ),

  tags: ['snapshot', '!autodocs'],
};

export const MultipleIssues: Story = {
  render: () => (
    <IssuesPane
      // eslint-disable-next-line no-console
      handleClose={(): void => console.log('close')}
      errorList={multipleErrors}
      isOpen
    />
  ),
  tags: ['!autodocs'],
};

MultipleIssues.storyName = 'IssuesPane multiple issues';
