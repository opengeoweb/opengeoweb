/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { fireEvent, render, screen } from '@testing-library/react';
import * as React from 'react';

import { TafThemeApiProvider } from '../Providers';

import IssuesPaneWrapper from './IssuesPaneWrapper';

describe('components/IssuesPane/IssuesPaneWrapper', () => {
  it('should wrap the IssuesPane', () => {
    const props = {
      handleClose: jest.fn(),
      isOpen: true,
      startPosition: { left: 0, top: 0 },
    };

    render(
      <TafThemeApiProvider>
        <IssuesPaneWrapper {...props} />
      </TafThemeApiProvider>,
    );

    expect(screen.getByTestId('moveable-issues-pane')).toBeTruthy();

    fireEvent.click(screen.getByTestId('closeBtn'));

    expect(props.handleClose).toHaveBeenCalled();
  });
});
