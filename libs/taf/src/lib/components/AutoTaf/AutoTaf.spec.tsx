/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { render, screen, waitFor } from '@testing-library/react';
import { AutoTaf } from './AutoTaf';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { TafThemeApiProvider } from '../Providers';
import { fakeTafList } from '../../utils/mockdata/fakeTafList';
import { createApi as createFakeApi } from '../../utils/fakeApi';

import { getAutoTafLocations } from '../TafLayout/utils';

jest.mock('../TafLayout/utils', () => ({
  ...jest.requireActual('../TafLayout/utils'),
  getAutoTafLocations: jest.fn(),
}));

describe('AutoTaf', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  const fakeAutoTafList = fakeTafList.filter(
    ({ taf }) => taf.status === 'AUTO',
  );
  const fakeLocations = ['EHAM', 'EHRD', 'EHLE', 'EHGG', 'EHBK'];

  it('should render empty AutoTaf component', () => {
    (getAutoTafLocations as jest.Mock).mockReturnValue(fakeLocations);

    render(
      <TafThemeApiProvider>
        <AutoTaf tafList={[]} />
      </TafThemeApiProvider>,
    );
    expect(
      screen.getByText(translateKeyOutsideComponents('autotaf')),
    ).toBeTruthy();
    expect(
      screen.getByText(translateKeyOutsideComponents('autotaf-no-data')),
    ).toBeTruthy();
    expect(screen.queryAllByRole('listitem')).toHaveLength(0);
  });

  it('should render AutoTaf component with data', async () => {
    (getAutoTafLocations as jest.Mock).mockReturnValue(fakeLocations);
    Element.prototype.scrollTo = jest.fn();

    render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <AutoTaf tafList={fakeAutoTafList} />
      </TafThemeApiProvider>,
    );

    expect(
      screen.getByText(translateKeyOutsideComponents('autotaf')),
    ).toBeTruthy();

    await waitFor(() =>
      expect(
        screen.queryByText(translateKeyOutsideComponents('autotaf-no-data')),
      ).toBeFalsy(),
    );

    expect(screen.getAllByRole('listitem')).toHaveLength(5);
    expect(Element.prototype.scrollTo).not.toHaveBeenCalled();
  });

  it('should render AutoTaf with selected taf', async () => {
    (getAutoTafLocations as jest.Mock).mockReturnValue(fakeLocations);
    Element.prototype.scrollTo = jest.fn();

    render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <AutoTaf tafList={fakeAutoTafList} activeTaf={fakeAutoTafList[2]} />
      </TafThemeApiProvider>,
    );

    expect(
      screen.getByText(translateKeyOutsideComponents('autotaf')),
    ).toBeTruthy();

    expect(
      screen.queryByText(translateKeyOutsideComponents('autotaf-no-data')),
    ).toBeFalsy();

    await waitFor(() => {
      const activeTafs = screen.queryAllByRole('listitem', {
        name: fakeAutoTafList[2].taf.location,
      });
      expect(activeTafs).toHaveLength(1);
      activeTafs.forEach((listItem) =>
        expect(listItem.className).toContain('active'),
      );
    });
    expect(Element.prototype.scrollTo).toHaveBeenCalled();
  });

  it('should render AutoTaf with missing location data', async () => {
    (getAutoTafLocations as jest.Mock).mockReturnValue(fakeLocations);
    Element.prototype.scrollTo = jest.fn();

    const missingDataList = [
      fakeAutoTafList[0],
      fakeAutoTafList[1],
      fakeAutoTafList[2],
    ];

    render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <AutoTaf tafList={missingDataList} activeTaf={missingDataList[2]} />
      </TafThemeApiProvider>,
    );

    expect(
      screen.getByText(translateKeyOutsideComponents('autotaf')),
    ).toBeTruthy();

    expect(
      screen.queryByText(translateKeyOutsideComponents('autotaf-no-data')),
    ).toBeFalsy();

    expect(screen.queryAllByRole('listitem')).toHaveLength(5);

    expect(
      screen.queryAllByText(
        translateKeyOutsideComponents('autotaf-no-location'),
      ),
    ).toHaveLength(2);

    await waitFor(() => {
      const activeTafs = screen.queryAllByRole('listitem', {
        name: fakeAutoTafList[2].taf.location,
      });
      expect(activeTafs).toHaveLength(1);
      activeTafs.forEach((listItem) =>
        expect(listItem.className).toContain('active'),
      );
    });
  });

  it('should show missing config key when autoTafLocations is not given', async () => {
    (getAutoTafLocations as jest.Mock).mockReturnValue(null);
    Element.prototype.scrollTo = jest.fn();

    const missingDataList = [
      fakeAutoTafList[0],
      fakeAutoTafList[1],
      fakeAutoTafList[2],
    ];

    render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <AutoTaf tafList={missingDataList} activeTaf={missingDataList[2]} />
      </TafThemeApiProvider>,
    );

    expect(
      screen.getByText(translateKeyOutsideComponents('autotaf-no-config')),
    ).toBeTruthy();

    expect(
      screen.getByText(translateKeyOutsideComponents('autotaf')),
    ).toBeTruthy();

    expect(screen.queryAllByRole('listitem')).toHaveLength(0);

    expect(
      screen.queryAllByText(
        translateKeyOutsideComponents('autotaf-no-location'),
      ),
    ).toHaveLength(0);
  });
});
