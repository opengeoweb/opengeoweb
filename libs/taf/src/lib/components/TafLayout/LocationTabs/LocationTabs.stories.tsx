/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Box, Paper, Typography } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import { TafThemeApiProvider } from '../../Providers';
import {
  fakeAmendmentFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeDraftFixedTaf,
  fakeNewFixedTaf,
  fakePublishedFixedTaf,
} from '../../../utils/mockdata/fakeTafList';
import LocationTabs from './LocationTabs';

const meta: Meta<typeof LocationTabs> = {
  title: 'components/Taf Layout/LocationTabs',
  component: LocationTabs,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the LocationTabs',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof LocationTabs>;

export const Component: Story = {
  render: (props) => (
    <TafThemeApiProvider shouldIncludeTheme={false}>
      <Paper>
        <LocationTabs {...props} />
      </Paper>
    </TafThemeApiProvider>
  ),
  args: {
    tafList: [fakeDraftFixedTaf, fakePublishedFixedTaf, fakeNewFixedTaf],
    activeIndex: 0,
    timeSlot: 'UPCOMING',
  },
  tags: ['!dev'],
};

const LocationTabsDemo = (): React.ReactElement => {
  return (
    <Paper sx={{ padding: '10px' }}>
      <Box sx={{ width: '56px' }}>
        <Typography>Upcoming</Typography>
        <LocationTabs
          tafList={[fakeDraftFixedTaf, fakePublishedFixedTaf, fakeNewFixedTaf]}
          activeIndex={0}
          timeSlot="UPCOMING"
        />
      </Box>
      <Box sx={{ width: '56px' }}>
        <Typography>Current</Typography>
        <LocationTabs
          tafList={[fakeDraftAmendmentFixedTaf, fakeAmendmentFixedTaf]}
          activeIndex={0}
          timeSlot="ACTIVE"
        />
      </Box>
    </Paper>
  );
};

export const LocationTabsLight: Story = {
  render: () => (
    <div style={{ width: '100px', height: '440px' }}>
      <TafThemeApiProvider shouldIncludeTheme={false}>
        <LocationTabsDemo />
      </TafThemeApiProvider>
    </div>
  ),
  tags: ['snapshot'],
};

export const LocationTabsDark: Story = {
  render: () => (
    <div style={{ width: '100px', height: '440px' }}>
      <TafThemeApiProvider shouldIncludeTheme={false}>
        <LocationTabsDemo />
      </TafThemeApiProvider>
    </div>
  ),
  tags: ['snapshot', 'dark'],
};
