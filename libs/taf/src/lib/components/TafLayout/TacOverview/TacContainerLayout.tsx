/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { Box, Typography } from '@mui/material';
import { getLocationSetting } from '../utils';

interface TacContainerLayoutProps {
  location: string;
  isActive?: boolean;
  exportBtnSlot?: React.ReactNode;
  statusSlot?: React.ReactNode;
  children?: React.ReactNode;
}

const TacContainerLayout: React.FC<TacContainerLayoutProps> = ({
  location,
  isActive = false,
  exportBtnSlot,
  statusSlot,
  children,
}: TacContainerLayoutProps) => {
  const locationLabel = getLocationSetting(location).label;

  return (
    <Box
      sx={{
        marginBottom: '20px',
      }}
      data-location={location}
      className={isActive ? 'active' : undefined}
      role="listitem"
      aria-label={location}
    >
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          height: 24,
        }}
      >
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <Box
            sx={{
              width: 24,
              height: 24,
              display: 'flex',
              marginRight: '2px',
              paddingTop: '6px',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            {statusSlot}
          </Box>

          <Typography
            sx={{
              lineHeight: 1,
              ...(isActive && {
                backgroundColor:
                  'geowebColors.functional.warningHighlight.rgba',
                padding: '0 2px',
                marginLeft: '-2px',
              }),
            }}
          >
            <Typography
              sx={{
                fontSize: '12px',
                color: 'geowebColors.captions.captionInformation.rgba',
              }}
              data-testid="tafLocation"
              component="span"
            >
              {location}
            </Typography>

            <Typography
              sx={{
                fontSize: '12px',
                marginLeft: '10px',
              }}
              data-testid="tafLocationLabel"
              component="span"
            >
              {locationLabel}
            </Typography>
          </Typography>
        </Box>
        {exportBtnSlot}
      </Box>
      <Box
        sx={{
          fontSize: '12px',
          letterSpacing: '0.4px',
          color: 'geowebColors.captions.captionStatus.rgba',
        }}
      >
        {children}
      </Box>
    </Box>
  );
};

export default TacContainerLayout;
