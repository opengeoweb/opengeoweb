/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import userEvent from '@testing-library/user-event';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import ExportButton from './ExportButton';
import { translateKeyOutsideComponents } from '../../../utils/i18n';
import { TafThemeApiProvider } from '../../Providers';

describe('components/TafLayout/TacOverview/ExportButton', () => {
  const user = userEvent.setup();
  it('should not show button when hidden', () => {
    const props = {
      onClick: jest.fn(),
      isHidden: true,
    };
    render(
      <TafThemeApiProvider>
        <ExportButton {...props} />
      </TafThemeApiProvider>,
    );
    expect(screen.queryByTestId('export-tac-button')).toBeFalsy();
  });
  it('should show button and handle onClick when clicked', () => {
    const props = {
      onClick: jest.fn(),
      isHidden: false,
    };
    render(
      <TafThemeApiProvider>
        <ExportButton {...props} />
      </TafThemeApiProvider>,
    );
    expect(screen.getByTestId('export-tac-button')).toBeTruthy();
    fireEvent.click(screen.queryByTestId('export-tac-button')!);
    expect(props.onClick).toHaveBeenCalled();
  });

  it('should show and hide tooltip', async () => {
    const props = {
      onClick: jest.fn(),
      isHidden: false,
    };
    render(
      <TafThemeApiProvider>
        <ExportButton {...props} />
      </TafThemeApiProvider>,
    );
    expect(screen.getByTestId('export-tac-button')).toBeTruthy();
    expect(screen.queryByRole('tooltip')).toBeFalsy();
    expect(
      screen.queryByText(translateKeyOutsideComponents('taf-import-title')),
    ).toBeFalsy();

    await user.hover(screen.queryByTestId('export-tac-button')!);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(
      screen.getByText(translateKeyOutsideComponents('taf-import-title')),
    ).toBeTruthy();

    await user.unhover(screen.queryByTestId('export-tac-button')!);
    await waitFor(() => expect(screen.queryByRole('tooltip')).toBeFalsy());
    expect(
      screen.queryByText(translateKeyOutsideComponents('taf-import-title')),
    ).toBeFalsy();
  });
});
