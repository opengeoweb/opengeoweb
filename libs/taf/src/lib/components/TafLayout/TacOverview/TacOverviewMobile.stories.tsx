/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { TafThemeApiProvider } from '../../Providers';
import {
  fakeAmendmentFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeDraftFixedTaf,
  fakeNewFixedTaf,
  fakePublishedFixedTaf,
} from '../../../utils/mockdata/fakeTafList';
import TacOverviewMobile from './TacOverviewMobile';
import { createApi as createFakeApi } from '../../../utils/fakeApi';

const meta: Meta<typeof TacOverviewMobile> = {
  title: 'components/Taf Layout/TacOverview mobile',
  component: TacOverviewMobile,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the TacOverviewMobile',
      },
    },
  },
  tags: ['!autodocs'],
};
export default meta;

type Story = StoryObj<typeof TacOverviewMobile>;

export const TacOverviewMobileLight: Story = {
  args: {
    upcomingTafs: [fakeDraftFixedTaf, fakePublishedFixedTaf, fakeNewFixedTaf],
    currentTafs: [fakeDraftAmendmentFixedTaf, fakeAmendmentFixedTaf],
    expiredTafs: [],
    activeTaf: fakePublishedFixedTaf,
    defaultOpen: true,
  },
  render: (props) => (
    <TafThemeApiProvider
      createApiFunc={createFakeApi}
      shouldIncludeTheme={false}
    >
      <div style={{ height: '100vh' }}>
        <TacOverviewMobile {...props} />
      </div>
    </TafThemeApiProvider>
  ),
  tags: ['snapshot'],
  parameters: {
    viewport: {
      defaultViewport: 'mobile',
    },
  },
};

export const TacOverviewMobileDark: Story = {
  ...TacOverviewMobileLight,
  tags: ['snapshot', 'dark'],
};

export const TacOverviewMobileAutoTaf: Story = {
  ...TacOverviewMobileLight,
  args: {
    upcomingTafs: [fakeDraftFixedTaf, fakePublishedFixedTaf, fakeNewFixedTaf],
    currentTafs: [fakeDraftAmendmentFixedTaf, fakeAmendmentFixedTaf],
    expiredTafs: [],
    activeTaf: fakePublishedFixedTaf,
    autoTafs: [
      {
        ...fakeDraftFixedTaf,
        taf: { ...fakeDraftFixedTaf.taf, location: 'EHAM' },
      },
      fakeNewFixedTaf,
      {
        ...fakePublishedFixedTaf,
        taf: { ...fakeDraftFixedTaf.taf, location: 'EHLE' },
      },
    ],
    defaultOpen: true,
  },
  tags: ['snapshot'],
};

export const TacOverviewMobileAutoTafDark: Story = {
  ...TacOverviewMobileAutoTaf,
  tags: ['snapshot', 'dark'],
};
