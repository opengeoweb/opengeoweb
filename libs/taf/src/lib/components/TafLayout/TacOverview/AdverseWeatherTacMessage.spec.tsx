/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { TafThemeApiProvider } from '../../Providers';
import { AdverseWeatherTACMessage } from './AdverseWeatherTacMessage';
import adverseWeatherTAC from '../../../utils/mockdata/fakeTAC.json';

jest.mock('../../../utils/api');

describe('components/TafLayout/TacOverview/AdverseWeatherTACMessage', () => {
  it('should display a TAC with adverse weather', async () => {
    render(
      <TafThemeApiProvider>
        <AdverseWeatherTACMessage TAC={adverseWeatherTAC} isActive />
      </TafThemeApiProvider>,
    );
    const windStyle = getComputedStyle(await screen.findByText('08020KT'));
    expect(windStyle.color).toEqual('rgb(192, 0, 0)');
    expect(windStyle.backgroundColor).toEqual('rgba(255, 168, 0, 0.2)');
  });
});
