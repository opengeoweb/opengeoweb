/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Card } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import { AdverseWeatherTACMessage } from './AdverseWeatherTacMessage';
import adverseWeatherTAC from '../../../utils/mockdata/fakeTAC.json';

const meta: Meta<typeof AdverseWeatherTACMessage> = {
  title: 'components/Taf Layout/TacOverview AdverseWeather',
  component: AdverseWeatherTACMessage,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the AdverseWeatherTACMessage',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof AdverseWeatherTACMessage>;

export const Component: Story = {
  args: {
    TAC: adverseWeatherTAC,
    isActive: true,
  },
};

const AdverseWeatherDemo = (): React.ReactElement => (
  <Card sx={{ width: '264px', padding: 1 }}>
    <AdverseWeatherTACMessage TAC={adverseWeatherTAC} isActive />
    <br />
    <AdverseWeatherTACMessage TAC={adverseWeatherTAC} isActive={false} />
  </Card>
);

const docs = {
  description: {
    story: 'Multiple AdverseWeather example',
  },
};

export const TacContainerLight: Story = {
  render: () => <AdverseWeatherDemo />,
  parameters: {
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61f2bfb7c86d02b398b51a68/version/639739b468ac941c776513a7',
      },
    ],
    docs,
  },
};

TacContainerLight.storyName = 'Tac Container Adverse Weather Light';

export const TacContainerDark: Story = {
  render: () => <AdverseWeatherDemo />,
  tags: ['dark'],
  parameters: {
    zeplinLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61f2bfbe6dfe44b1ef08d746/version/639739c614c696166a7de223',
      },
    ],
    docs,
  },
};

TacContainerDark.storyName = 'Tac Container Adverse Weather Dark';
