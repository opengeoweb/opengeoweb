/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { TFunction } from 'i18next';

import TafLayout, { getSnackbarMessage } from './TafLayout';
import {
  fakeAmendmentFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeDraftFixedTaf,
  fakeNewFixedTaf,
  fakePublishedFixedTaf,
  MOCK_USERNAME,
  previousTafForfakeDraftAmendmentFixedTaf,
} from '../../utils/mockdata/fakeTafList';
import { TafThemeApiProvider } from '../Providers';
import { createApi } from '../../utils/__mocks__/api';
import { TafApi } from '../../utils/api';
import { translateKeyOutsideComponents } from '../../utils/i18n';

jest.mock('../../utils/api');

describe('getSnackbarMessage', () => {
  const t = ((key: string) => key) as unknown as TFunction;
  it('should return correct message', () => {
    expect(
      getSnackbarMessage(
        {
          changeStatusTo: 'DRAFT',
          taf: fakeNewFixedTaf.taf,
        },
        false,
        t,
      ),
    ).toEqual('snackbar-draft');
    expect(
      getSnackbarMessage(
        {
          changeStatusTo: 'DRAFT_AMENDED',
          taf: fakeNewFixedTaf.taf,
        },
        false,
        t,
      ),
    ).toEqual('snackbar-draft-amend');
    expect(
      getSnackbarMessage(
        {
          changeStatusTo: 'DRAFT_CORRECTED',
          taf: fakeNewFixedTaf.taf,
        },
        false,
        t,
      ),
    ).toEqual('snackbar-draft-correct');
    expect(
      getSnackbarMessage(
        {
          changeStatusTo: 'CANCELLED',
          taf: fakeNewFixedTaf.taf,
        },
        false,
        t,
      ),
    ).toEqual('snackbar-cancel');
    expect(
      getSnackbarMessage(
        {
          changeStatusTo: 'AMENDED',
          taf: fakeNewFixedTaf.taf,
        },
        false,
        t,
      ),
    ).toEqual('snackbar-amend');
    expect(
      getSnackbarMessage(
        { changeStatusTo: 'DRAFT', taf: fakeNewFixedTaf.taf },
        true,
        t,
      ),
    ).toEqual('snackbar-auto-save');
  });
});

describe('components/TafLayout/TafLayout - test snackbar', () => {
  beforeEach(() => {
    jest.useFakeTimers();
    Element.prototype.scrollTo = jest.fn();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should show snackbar for saving a taf as draft', async () => {
    const props = {
      tafList: [{ ...fakeDraftFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };

    const createFakeApi = (): TafApi => ({
      ...createApi(),
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await screen.findByTestId('savedrafttaf');
    fireEvent.click(screen.queryByTestId('savedrafttaf')!);
    await waitFor(() => {
      // test notification
      expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    });

    expect(
      await screen.findByText(translateKeyOutsideComponents('snackbar-draft')),
    ).toBeTruthy();
  });

  it('should show snackbar for saving a taf as draft amendment', async () => {
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };

    const createFakeApi = (): TafApi => ({
      ...createApi(),
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );
    await screen.findByTestId('savedrafttaf');
    fireEvent.click(screen.queryByTestId('savedrafttaf')!);
    await waitFor(() => {
      // test notification
      expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    });
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('snackbar-draft-amend'),
      ),
    ).toBeTruthy();
  });

  it('should show snackbar for publish a taf', async () => {
    const props = {
      tafList: [{ ...fakeDraftFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };

    const createFakeApi = (): TafApi => ({
      ...createApi(),
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );
    await screen.findByTestId('publishtaf');
    fireEvent.click(screen.queryByTestId('publishtaf')!);
    await waitFor(() => {
      expect(screen.getByTestId('confirmationDialog-confirm')).toBeTruthy();
    });
    fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);

    // test notification
    await waitFor(() => {
      expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    });
    const { location, messageType } = fakeDraftFixedTaf.taf;
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('snackbar-publish', {
          location,
          formattedDate: '18:00 06 Jan',
          messageType,
        }),
      ),
    ).toBeTruthy();
  });

  it('should show snackbar for correct a taf', async () => {
    const props = {
      tafList: [{ ...fakeAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const createFakeApi = (): TafApi => ({
      ...createApi(),
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );
    await screen.findByTestId('correcttaf');
    fireEvent.click(screen.queryByTestId('correcttaf')!);
    fireEvent.click(screen.queryByTestId('publishtaf')!);
    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);

    // test notification
    await waitFor(() => {
      expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    });
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('snackbar-correct', {
          location: fakeAmendmentFixedTaf.taf.location,
          formattedDate: '14:00 06 Jan',
          messageType: 'COR',
        }),
      ),
    ).toBeTruthy();
  });

  it('should show snackbar for cancelled a taf', async () => {
    const props = {
      tafList: [{ ...fakePublishedFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };

    const createFakeApi = (): TafApi => ({
      ...createApi(),
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );
    await screen.findByTestId('canceltaf');
    fireEvent.click(screen.queryByTestId('canceltaf')!);
    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);

    // test notification
    await waitFor(() => {
      expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    });
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('snackbar-cancel', {
          location: fakePublishedFixedTaf.taf.location,
          formattedDate: '12:00 06 Jan',
          messageType: 'CNL',
        }),
      ),
    ).toBeTruthy();
  });

  it('should show snackbar for amend a taf', async () => {
    const props = {
      tafList: [
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
        previousTafForfakeDraftAmendmentFixedTaf,
      ],
      onUpdateTaf: jest.fn(),
    };

    const createFakeApi = (): TafApi => ({
      ...createApi(),
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );
    await screen.findByTestId('publishtaf');
    fireEvent.click(screen.queryByTestId('publishtaf')!);
    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);

    // test notification
    await waitFor(() => {
      expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    });
    const { location, messageType } = fakeDraftAmendmentFixedTaf.taf;
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('snackbar-amend', {
          location,
          formattedDate: '14:00 06 Jan',
          messageType,
        }),
      ),
    ).toBeTruthy();
  });
});
