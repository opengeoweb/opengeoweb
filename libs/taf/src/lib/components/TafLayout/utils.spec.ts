/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { dateUtils } from '@opengeoweb/shared';
import { Taf, TafFormData, TafFromBackend } from '../../types';
import {
  getBaseTime,
  prepareTafExportValues,
  sortTafsOnIssueDate,
  sortTafsOnLocation,
  sortTafTimeSlots,
  getNewTafLocationIndex,
  getPreviousTaf,
  getAutoTafLocations,
  scrollToPosition,
} from './utils';

import {
  fakeAmendmentFixedTaf,
  fakeCancelledFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeDraftFixedTaf,
  fakeExpiredTaf,
  fakeNewFixedTaf,
  fakePublishedFixedTaf,
  previousTafForfakeDraftAmendmentFixedTaf,
} from '../../utils/mockdata/fakeTafList';
import { emptyBaseForecast, emptyChangegroup } from '../TafForm/utils';
import { WeatherPhenomena } from '../../utils/weatherPhenomena';

describe('components/TafLayout/utils', () => {
  describe('sortTafsOnLocation', () => {
    it('should sort tafList based on location', () => {
      const expextedResult = [
        { taf: { location: 'EHAM' } },
        { taf: { location: 'EHRD' } },
        { taf: { location: 'EHLE' } },
        { taf: { location: 'EHLE' } },
        { taf: { location: 'EHGG' } },
        { taf: { location: 'EHGG' } },
        { taf: { location: 'EHBK' } },
        { taf: { location: 'TNCB' } },
      ];
      expect(
        sortTafsOnLocation([
          { taf: { location: 'EHLE' } },
          { taf: { location: 'EHGG' } },
          { taf: { location: 'EHLE' } },
          { taf: { location: 'TNCB' } },
          { taf: { location: 'EHRD' } },
          { taf: { location: 'EHGG' } },
          { taf: { location: 'EHAM' } },
          { taf: { location: 'EHBK' } },
        ] as TafFromBackend[]),
      ).toEqual(expextedResult);
      expect(
        sortTafsOnLocation([
          { taf: { location: 'EHLE' } },
          { taf: { location: 'TNCB' } },
          { taf: { location: 'EHRD' } },
          { taf: { location: 'EHGG' } },
          { taf: { location: 'EHLE' } },
          { taf: { location: 'EHGG' } },
          { taf: { location: 'EHAM' } },
          { taf: { location: 'EHBK' } },
        ] as TafFromBackend[]),
      ).toEqual(expextedResult);
      expect(
        sortTafsOnLocation([
          { taf: { location: 'TNCB' } },
          { taf: { location: 'EHRD' } },
          { taf: { location: 'EHGG' } },
          { taf: { location: 'EHAM' } },
          { taf: { location: 'EHBK' } },
          { taf: { location: 'EHLE' } },
          { taf: { location: 'EHGG' } },
          { taf: { location: 'EHLE' } },
        ] as TafFromBackend[]),
      ).toEqual(expextedResult);
    });
  });

  describe('sortTafsOnIssueDate', () => {
    it('should sort tafList based on issueDate', () => {
      const expextedResult = [
        { taf: {} },
        { taf: { issueDate: '2022-09-20T07:00:00Z' } },
        { taf: { issueDate: '2022-09-20T06:00:00Z' } },
      ];
      expect(
        sortTafsOnIssueDate([
          { taf: { issueDate: '2022-09-20T06:00:00Z' } },
          { taf: { issueDate: '2022-09-20T07:00:00Z' } },
          { taf: {} },
        ] as TafFromBackend[]),
      ).toEqual(expextedResult);
      expect(
        sortTafsOnIssueDate([
          { taf: { issueDate: '2022-09-20T07:00:00Z' } },
          { taf: { issueDate: '2022-09-20T06:00:00Z' } },
          { taf: {} },
        ] as TafFromBackend[]),
      ).toEqual(expextedResult);
      expect(
        sortTafsOnIssueDate([
          { taf: {} },
          { taf: { issueDate: '2022-09-20T07:00:00Z' } },
          { taf: { issueDate: '2022-09-20T06:00:00Z' } },
        ] as TafFromBackend[]),
      ).toEqual(expextedResult);
      expect(
        sortTafsOnIssueDate([
          { taf: { issueDate: '2022-09-20T07:00:00Z' } },
          { taf: {} },
          { taf: { issueDate: '2022-09-20T06:00:00Z' } },
        ] as TafFromBackend[]),
      ).toEqual(expextedResult);
    });
  });

  describe('getBaseTime', () => {
    it('should get current and nextbasetime of taf', () => {
      const result = getBaseTime(
        fakeNewFixedTaf.taf.baseTime,
        fakeNewFixedTaf.taf.location,
      );
      expect(
        dateUtils.dateToString(
          result.currentBaseTime,
          dateUtils.DATE_FORMAT_UTC,
        ),
      ).toEqual('2022-01-06T12:00:00Z');
      expect(
        dateUtils.dateToString(result.nextBaseTime, dateUtils.DATE_FORMAT_UTC),
      ).toEqual('2022-01-06T18:00:00Z');

      const result2 = getBaseTime(
        fakeDraftFixedTaf.taf.baseTime,
        fakeDraftFixedTaf.taf.location,
      );
      expect(
        dateUtils.dateToString(
          result2.currentBaseTime,
          dateUtils.DATE_FORMAT_UTC,
        ),
      ).toEqual('2022-01-06T18:00:00Z');
      expect(
        dateUtils.dateToString(result2.nextBaseTime, dateUtils.DATE_FORMAT_UTC),
      ).toEqual('2022-01-07T00:00:00Z');

      const result3 = getBaseTime(
        fakePublishedFixedTaf.taf.baseTime,
        fakePublishedFixedTaf.taf.location,
      );
      expect(
        dateUtils.dateToString(
          result3.currentBaseTime,
          dateUtils.DATE_FORMAT_UTC,
        ),
      ).toEqual('2022-01-06T12:00:00Z');
      expect(
        dateUtils.dateToString(result3.nextBaseTime, dateUtils.DATE_FORMAT_UTC),
      ).toEqual('2022-01-06T18:00:00Z');
    });
  });

  describe('sortTafTimeSlots', () => {
    it('should sort tafList into current, upcoming and expired tafs', () => {
      const tafList = [
        fakeNewFixedTaf,
        fakeDraftFixedTaf,
        fakePublishedFixedTaf,
        fakeDraftAmendmentFixedTaf,
        fakeAmendmentFixedTaf,
        fakeExpiredTaf,
        fakeCancelledFixedTaf,
      ];

      const result = sortTafTimeSlots(tafList);
      // dates are fixed, so current is empty
      expect(result.current).toEqual([]);
      expect(result.upcoming).toEqual([
        fakeAmendmentFixedTaf,
        fakeDraftAmendmentFixedTaf,
        fakeDraftFixedTaf,
        fakePublishedFixedTaf,
        fakeNewFixedTaf,
        fakeCancelledFixedTaf,
      ]);

      expect(result.expired).toEqual([fakeExpiredTaf]);
    });
  });

  describe('prepareTafExportValues', () => {
    it('should copy only editable form values to export', () => {
      const currentTaf: TafFormData = {
        messageType: 'ORG',
        status: 'NEW',
        uuid: 'testTafExport',
        baseTime: `2022-01-06T12:00:00Z`,
        validDateStart: `2022-01-06T12:00:00Z`,
        validDateEnd: `2022-01-07T18:00:00Z`,
        location: 'EHBK',
        baseForecast: { valid: '0612/0718' },
        changeGroups: [],
      };
      const tafToCopy = fakeExpiredTaf.taf;
      const result = prepareTafExportValues(tafToCopy, currentTaf);
      expect(result).toEqual({
        ...currentTaf,
        baseForecast: {
          ...emptyBaseForecast,
          valid: currentTaf.baseForecast.valid,
          cloud: {
            cloud1: 'FEW015',
            cloud2: 'SCT025',
            cloud3: 'BKN030',
            cloud4: 'OVC050CB',
          },
          visibility: '0500',
          weather: {
            weather1: 'BCFG',
            weather2: 'SNDZ',
            weather3: '',
          },
          wind: '05005',
        },
        changeGroups: [
          {
            ...emptyChangegroup,
            change: 'BECMG',
            valid: '0709/0712',
            visibility: '1000',
            weather: {
              ...emptyChangegroup.weather,
              weather1: 'BR',
            },
          },
          {
            ...emptyChangegroup,
            change: 'FM',
            cloud: {
              ...emptyChangegroup.cloud,
              cloud1: 'FEW015',
              cloud2: 'BKN030',
            },
            valid: '071500',
            visibility: '6000',
            weather: {
              ...emptyChangegroup.weather,
              weather1: 'DZ',
            },
          },
        ],
      });
    });

    it('should return an empty changeGroups array when the taf to copy does not have changeGroups', () => {
      const currentTaf: TafFormData = {
        messageType: 'ORG',
        status: 'AMENDED',
        uuid: 'testTafExport',
        baseTime: `2022-01-06T12:00:00Z`,
        validDateStart: `2022-01-06T12:00:00Z`,
        validDateEnd: `2022-01-07T18:00:00Z`,
        location: 'EHBK',
        baseForecast: {
          valid: '0612/0614',
          wind: '10010',
          visibility: '1000',
          weather: {
            weather1: 'RA',
          },
          cloud: {
            cloud1: 'FEW001',
          },
        },
        changeGroups: [
          {
            change: 'BECMG',
            valid: '0612/0718',
            visibility: '1000',
            weather: { weather1: 'BR' },
          },
        ],
      };

      const tafToCopy: Taf = {
        messageType: 'AMD',
        status: 'DRAFT',
        uuid: 'copyTaf',
        issueDate: '2020-12-07T06:00:00Z',
        baseTime: '2020-12-07T06:00:00Z',
        validDateStart: '2020-12-07T06:00:00Z',
        validDateEnd: '2020-12-08T12:00:00Z',
        location: 'EHAM',
        baseForecast: {
          valid: {
            start: '2020-12-07T06:00:00Z',
            end: '2020-12-08T12:00:00Z',
          },
          wind: { direction: 300, speed: 30, unit: 'KT' },
          visibility: { range: 2000, unit: 'M' },
        },
      };

      const result = prepareTafExportValues(tafToCopy, currentTaf);
      expect(result).toEqual({
        ...currentTaf,
        baseForecast: {
          ...emptyBaseForecast,
          valid: currentTaf.baseForecast.valid,
          wind: '30030',
          visibility: '2000',
        },
        changeGroups: [],
      });
    });

    it('should return empty values for empty baseForecast weather and cloud fields', () => {
      const currentTaf: TafFormData = {
        messageType: 'ORG',
        status: 'AMENDED',
        uuid: 'testTafExport',
        baseTime: `2022-01-06T12:00:00Z`,
        validDateStart: `2022-01-06T12:00:00Z`,
        validDateEnd: `2022-01-07T18:00:00Z`,
        location: 'EHBK',
        baseForecast: {
          valid: '0612/0614',
          wind: '10010',
          visibility: '1000',
          weather: {
            weather1: 'RA',
            weather2: 'SS',
            weather3: 'SN',
          },
          cloud: {
            cloud1: 'FEW001',
            cloud2: 'BKN002',
            cloud3: 'BKN005',
            cloud4: 'BKN006CB',
          },
        },
        changeGroups: [
          {
            change: 'BECMG',
            valid: '0612/0718',
            visibility: '1000',
            weather: {
              weather1: 'RA',
              weather2: 'SS',
              weather3: 'SN',
            },
            cloud: {
              cloud1: 'FEW001',
              cloud2: 'BKN002',
              cloud3: 'BKN005',
              cloud4: 'BKN006CB',
            },
          },
        ],
      };

      const tafToCopy: Taf = {
        messageType: 'AMD',
        status: 'DRAFT',
        uuid: 'copyTaf',
        issueDate: '2020-12-07T06:00:00Z',
        baseTime: '2020-12-07T06:00:00Z',
        validDateStart: '2020-12-07T06:00:00Z',
        validDateEnd: '2020-12-08T12:00:00Z',
        location: 'EHAM',
        baseForecast: {
          valid: {
            start: '2020-12-07T06:00:00Z',
            end: '2020-12-08T12:00:00Z',
          },
          wind: { direction: 300, speed: 30, unit: 'KT' },
          visibility: { range: 2000, unit: 'M' },
          weather: {
            weather1: 'SN' as WeatherPhenomena,
          },
          cloud: {
            cloud1: {
              coverage: 'BKN',
              height: 1,
            },
          },
        },
        changeGroups: [
          {
            change: 'BECMG',
            valid: {
              start: '2020-12-07T06:00:00Z',
              end: '2020-12-07T08:00:00Z',
            },
            visibility: { range: 1000, unit: 'M' },
            weather: {
              weather1: 'SN' as WeatherPhenomena,
            },
            cloud: {
              cloud1: {
                coverage: 'BKN',
                height: 1,
              },
            },
          },
        ],
      };

      const result = prepareTafExportValues(tafToCopy, currentTaf);
      expect(result).toEqual({
        ...currentTaf,
        baseForecast: {
          valid: currentTaf.baseForecast.valid,
          wind: '30030',
          visibility: '2000',
          weather: {
            ...emptyBaseForecast.weather,
            weather1: 'SN',
          },
          cloud: {
            ...emptyBaseForecast.cloud,
            cloud1: 'BKN001',
          },
        },
        changeGroups: [
          {
            ...emptyChangegroup,
            change: 'BECMG',
            cloud: {
              ...emptyChangegroup.cloud,
              cloud1: 'BKN001',
            },
            valid: '0706/0708',
            visibility: '1000',
            weather: {
              ...emptyChangegroup.weather,
              weather1: 'SN',
            },
          },
        ],
      });
    });
  });

  describe('getNewTafLocationIndex', () => {
    it('should return index 0 if the uuid does not match any taf in the list', () => {
      expect(getNewTafLocationIndex([], '299299')).toEqual(0);
      const taf1 = {
        taf: { previousId: '12121', uuid: '3434343' },
      } as TafFromBackend;
      const taf2 = {
        taf: { previousId: '232323', uuid: '4545454' },
      } as TafFromBackend;
      expect(getNewTafLocationIndex([taf1, taf2], '299299')).toEqual(0);
    });
    it('should return the index of the taf if the uuid matches the previousId', () => {
      const uuid = '299299';
      const taf1 = {
        taf: { previousId: '112211', uuid },
      } as TafFromBackend;
      const taf2 = {
        taf: { previousId: uuid, uuid: '3434343' },
      } as TafFromBackend;
      expect(getNewTafLocationIndex([taf1, taf2], uuid)).toEqual(1);
    });
    it('should return the index of the taf if the uuid matches the uuid and it does not match any previousId', () => {
      const uuid = '299299';
      const taf1 = {
        taf: { previousId: '6767676', uuid: '3434343' },
      } as TafFromBackend;
      const taf2 = {
        taf: { previousId: '112211', uuid },
      } as TafFromBackend;
      expect(getNewTafLocationIndex([taf1, taf2], uuid)).toEqual(1);
    });
  });
});

describe('getPreviousTaf', () => {
  it('should return null if list empty or previousId not found', () => {
    // Send TAF with previous Id
    expect(getPreviousTaf([], fakeDraftAmendmentFixedTaf)).toBe(null);
    expect(getPreviousTaf([fakeNewFixedTaf], fakeDraftAmendmentFixedTaf)).toBe(
      null,
    );
    // Without previous Id
    expect(getPreviousTaf([], fakeNewFixedTaf)).toBe(null);
  });
  it('should find the previous taf in the list and return the inner taf prop', () => {
    expect(
      getPreviousTaf(
        [fakeDraftAmendmentFixedTaf, previousTafForfakeDraftAmendmentFixedTaf],
        fakeDraftAmendmentFixedTaf,
      ),
    ).toBe(previousTafForfakeDraftAmendmentFixedTaf.taf);
  });
});

describe('getAutoTafLocations', () => {
  it('should return autoTAFlocations from the config', () => {
    expect(getAutoTafLocations()).toEqual([
      'EHAM',
      'EHRD',
      'EHLE',
      'EHGG',
      'EHBK',
    ]);
  });

  it('should return null', () => {
    expect(getAutoTafLocations(null!)).toEqual(null);
  });
  it('should be able to overwrite default locations', () => {
    const newLocations = ['EHRD', 'EHLE'];
    expect(getAutoTafLocations(newLocations)).toEqual(newLocations);
  });
});

const mockBoundingClientRect = (
  element: HTMLElement,
  values: Partial<DOMRect>,
): void => {
  // eslint-disable-next-line no-param-reassign
  element.getBoundingClientRect = jest.fn(() => ({
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: 0,
    height: 0,
    x: 0,
    y: 0,
    toJSON: jest.fn(),
    ...values,
  }));
};

describe('scrollToPosition', () => {
  let container: HTMLDivElement;
  let targetElement: HTMLDivElement;
  let scrollRef: React.RefObject<HTMLDivElement>;

  beforeEach(() => {
    container = document.createElement('div');
    container.style.paddingTop = '20px';
    container.style.overflow = 'auto';
    container.style.height = '500px';
    document.body.appendChild(container);

    targetElement = document.createElement('div');
    targetElement.dataset.location = 'target';
    container.appendChild(targetElement);

    scrollRef = { current: container };

    Object.defineProperty(window.HTMLElement.prototype, 'scrollTo', {
      writable: true,
      value: jest.fn(),
    });
  });

  afterEach(() => {
    document.body.removeChild(container);
  });

  it('should not throw if scrollRef is null', () => {
    expect(() => scrollToPosition({ current: null }, 'target')).not.toThrow();
  });

  it('should scroll to the correct position', () => {
    mockBoundingClientRect(container, { top: 100 });
    mockBoundingClientRect(targetElement, { top: 300 });
    jest
      .spyOn(window, 'getComputedStyle')
      .mockReturnValue({ paddingTop: '20px' } as CSSStyleDeclaration);
    jest.spyOn(HTMLElement.prototype, 'scrollTo').mockImplementation(() => {});

    scrollToPosition(scrollRef, 'target');

    expect(container.scrollTo).toHaveBeenCalledWith(180, 180);
  });

  it('should do nothing if the target location is not found', () => {
    jest.spyOn(HTMLElement.prototype, 'scrollTo').mockImplementation(() => {});

    scrollToPosition({ current: null }, 'non-existent');

    expect(container.scrollTo).not.toHaveBeenCalled();
  });
});
