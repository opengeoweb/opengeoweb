/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Paper } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import { TafThemeApiProvider } from '../Providers';
import TafLayout from './TafLayout';
import {
  fakeFixedTafList,
  fakeTafList,
} from '../../utils/mockdata/fakeTafList';
import { TafFromBackend } from '../../types';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { ResponsiveWrapper } from '../ResponsiveWrapper/ResponsiveWrapper';

const meta: Meta<typeof TafLayout> = {
  title: 'components/Taf Layout',
  component: TafLayout,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the TafLayout',
      },
    },
  },
  tags: ['!autodocs'],
};
export default meta;
type Story = StoryObj<typeof TafLayout>;

interface TafLayoutComponentProps {
  tafList?: TafFromBackend[];
  lastUpdateTime?: string;
}

const TafLayoutComponent = ({
  tafList = fakeTafList,
  lastUpdateTime,
}: TafLayoutComponentProps): React.ReactElement => {
  return (
    <Paper
      sx={{
        maxHeight: '100%',
        height: '100vh',
        overflow: 'auto',
      }}
    >
      <ResponsiveWrapper>
        <TafLayout
          tafList={tafList}
          onUpdateTaf={(): Promise<void> => null!}
          lastUpdateTime={lastUpdateTime}
        />
      </ResponsiveWrapper>
    </Paper>
  );
};

export const DemoTafLayoutLightTheme: Story = {
  render: () => (
    <TafThemeApiProvider
      shouldIncludeTheme={false}
      createApiFunc={createFakeApi}
    >
      <TafLayoutComponent />
    </TafThemeApiProvider>
  ),
};

export const DemoTafLayoutDarkTheme: Story = {
  render: () => (
    <TafThemeApiProvider
      shouldIncludeTheme={false}
      createApiFunc={createFakeApi}
    >
      <TafLayoutComponent />
    </TafThemeApiProvider>
  ),
  tags: ['dark'],
};

export const TafLayoutLight: Story = {
  render: () => (
    <TafThemeApiProvider
      createApiFunc={createFakeApi}
      shouldIncludeTheme={false}
    >
      <div style={{ minWidth: 1440 }}>
        <TafLayoutComponent tafList={fakeFixedTafList} lastUpdateTime="11:12" />
      </div>
    </TafThemeApiProvider>
  ),
  tags: ['snapshot'],
};

export const TafLayoutDark: Story = {
  render: () => (
    <TafThemeApiProvider
      createApiFunc={createFakeApi}
      shouldIncludeTheme={false}
    >
      <div style={{ minWidth: 1440 }}>
        <TafLayoutComponent tafList={fakeFixedTafList} lastUpdateTime="11:12" />
      </div>
    </TafThemeApiProvider>
  ),
  tags: ['snapshot', 'dark'],
};

const fakeFixedWithAutoTaf = fakeFixedTafList.map((taf, index) => ({
  ...taf,
  taf: { ...taf.taf, status: index % 2 === 0 ? 'AUTO' : taf.taf.status },
}));
export const TafLayoutAutoTaf: Story = {
  args: {
    tafList: fakeFixedWithAutoTaf,
    onUpdateTaf: (): Promise<void> => null!,
    lastUpdateTime: '11:12',
    isAutoTafEnabled: true,
  },
  render: (props) => (
    <TafThemeApiProvider
      createApiFunc={createFakeApi}
      shouldIncludeTheme={false}
    >
      <Paper sx={{ maxHeight: '100%', height: '100vh', overflow: 'auto' }}>
        <ResponsiveWrapper>
          <TafLayout {...props} />
        </ResponsiveWrapper>
      </Paper>
    </TafThemeApiProvider>
  ),
  tags: ['snapshot'],
};

export const TafLayoutAutoTafDark: Story = {
  ...TafLayoutAutoTaf,
  tags: ['snapshot', 'dark'],
};
