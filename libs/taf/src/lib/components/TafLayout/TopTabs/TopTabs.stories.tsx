/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { StorybookDocsWrapper } from '@opengeoweb/shared';
import { TafThemeApiProvider } from '../../Providers';

import TopTabs from './TopTabs';
import { fakeAmendmentFixedTaf } from '../../../utils/mockdata/fakeTafList';

const meta: Meta<typeof TopTabs> = {
  title: 'components/Taf Layout/TopTabs',
  component: TopTabs,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the TopTabs',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof TopTabs>;

export const Component: Story = {
  render: (props) => (
    <div style={{ width: 600, padding: 10, height: 100 }}>
      <TafThemeApiProvider shouldIncludeTheme={false}>
        <TopTabs {...props} />
      </TafThemeApiProvider>
    </div>
  ),
  args: {
    timeSlot: 'UPCOMING',
    tafFromBackend: fakeAmendmentFixedTaf,
  },
  tags: ['!dev', '!autodocs'],
};

const TopTabsDemo = (): React.ReactElement => (
  <TopTabs timeSlot="UPCOMING" tafFromBackend={fakeAmendmentFixedTaf} />
);

export const TopTabsLight: Story = {
  render: () => (
    <div style={{ width: 600, padding: 10, height: 100 }}>
      <TafThemeApiProvider shouldIncludeTheme={false}>
        <TopTabsDemo />
      </TafThemeApiProvider>
    </div>
  ),
  tags: ['snapshot'],
};

export const TopTabsDark: Story = {
  render: () => (
    <div style={{ width: 680, padding: 10, height: 100 }}>
      <StorybookDocsWrapper isDark>
        <TafThemeApiProvider shouldIncludeTheme={false}>
          <TopTabsDemo />
        </TafThemeApiProvider>
      </StorybookDocsWrapper>
    </div>
  ),
  tags: ['snapshot', 'dark'],
};
