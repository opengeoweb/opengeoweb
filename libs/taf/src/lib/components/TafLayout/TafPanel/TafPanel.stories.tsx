/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Grid2 as Grid, Paper } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import { TafThemeApiProvider } from '../../Providers';
import TafPanel, { TafError } from './TafPanel';
import { fakeDraftFixedTaf } from '../../../utils/mockdata/fakeTafList';
import { Taf, TafFromBackend, TafActions, ChangeGroup } from '../../../types';
import { createApi as createFakeApi } from '../../../utils/fakeApi';
import { ResponsiveWrapper } from '../../ResponsiveWrapper/ResponsiveWrapper';
import { emptyChangegroup } from '../../TafForm/utils';

const meta: Meta<typeof TafPanel> = {
  title: 'components/Taf Layout/TafPanel',
  component: TafPanel,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the TafPanel',
      },
    },
  },
  tags: ['!autodocs'],
};
export default meta;

type Story = StoryObj<typeof TafPanel>;

const mockSaveError: TafError = {
  type: 'SERVER',
  title: 'An error has occurred while saving, please try again',
  message: 'Status change not allowed',
};

const mockTakeOverWarning: TafError = {
  type: 'WARNING',
  title: 'You are no longer the editor for this TAF',
};

interface TafPanelComponentProps {
  tafFromBackend?: TafFromBackend;
  previousTaf?: Taf;
  tafAction?: TafActions;
  disabled?: boolean;
  error?: TafError;
}

const TafPanelComponent = ({
  tafFromBackend = fakeDraftFixedTaf,
  previousTaf = null!,
  tafAction = 'DRAFT',
  disabled = false,
  error,
}: TafPanelComponentProps): React.ReactElement => {
  return (
    <Paper sx={{ position: 'relative', width: '1024px' }}>
      <ResponsiveWrapper>
        <Grid container>
          <Grid size={{ xs: 12 }}>
            <TafPanel
              tafFromBackend={tafFromBackend}
              activeTafIndex={0}
              index={0}
              isFormDisabled={disabled}
              error={error}
              previousTaf={previousTaf}
              tafAction={tafAction}
            />
          </Grid>
        </Grid>
      </ResponsiveWrapper>
    </Paper>
  );
};

export const TafPanelSaveErrorLight: Story = {
  render: () => (
    <TafThemeApiProvider
      shouldIncludeTheme={false}
      createApiFunc={createFakeApi}
    >
      <TafPanelComponent error={mockSaveError} />
    </TafThemeApiProvider>
  ),
  tags: ['snapshot'],
};

export const TafPanelSaveErrorDark: Story = {
  render: () => (
    <TafThemeApiProvider
      shouldIncludeTheme={false}
      createApiFunc={createFakeApi}
    >
      <TafPanelComponent error={mockSaveError} />
    </TafThemeApiProvider>
  ),
  tags: ['snapshot', 'dark'],
};

export const TafPanelTakeOverErrorLight: Story = {
  render: () => (
    <TafThemeApiProvider
      shouldIncludeTheme={false}
      createApiFunc={createFakeApi}
    >
      <TafPanelComponent
        tafFromBackend={{ ...fakeDraftFixedTaf, editor: 'other.user' }}
        error={mockTakeOverWarning}
        disabled
      />
    </TafThemeApiProvider>
  ),
  tags: ['snapshot'],
};

export const TafPanelTakeOverErrorDark: Story = {
  render: () => (
    <TafThemeApiProvider
      shouldIncludeTheme={false}
      createApiFunc={createFakeApi}
    >
      <TafPanelComponent
        tafFromBackend={{ ...fakeDraftFixedTaf, editor: 'other.user' }}
        error={mockTakeOverWarning}
        disabled
      />
    </TafThemeApiProvider>
  ),
  tags: ['snapshot', 'dark'],
};

export const TafPanelMultipleRowsWithSameDates = (): React.ReactElement => {
  const emptyChangeGroupWithData = {
    ...emptyChangegroup,
    valid: {
      start: `2022-01-06T18:00:00Z`,
      end: `2022-01-07T23:59:00Z`,
    },
  } as unknown as ChangeGroup;
  //
  return (
    <TafThemeApiProvider
      shouldIncludeTheme={false}
      createApiFunc={createFakeApi}
    >
      <TafPanelComponent
        tafFromBackend={{
          ...fakeDraftFixedTaf,
          taf: {
            ...fakeDraftFixedTaf.taf,
            changeGroups: [
              emptyChangeGroupWithData,
              emptyChangeGroupWithData,
              emptyChangeGroupWithData,
              emptyChangeGroupWithData,
              emptyChangeGroupWithData,
              emptyChangeGroupWithData,
              emptyChangeGroupWithData,
              emptyChangeGroupWithData,
            ],
          },
        }}
      />
    </TafThemeApiProvider>
  );
};
