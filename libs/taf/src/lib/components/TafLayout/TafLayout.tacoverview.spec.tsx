/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';

import userEvent from '@testing-library/user-event';
import TafLayout from './TafLayout';
import {
  fakeAmendmentFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeFixedTafList,
  fakePublishedFixedTaf,
  MOCK_USERNAME,
} from '../../utils/mockdata/fakeTafList';
import { TafThemeApiProvider } from '../Providers';
import {
  createApi,
  fakeTestTac,
  fakeTestTacJson,
} from '../../utils/__mocks__/api';
import { TafApi } from '../../utils/api';
import { AdverseWeatherTAC } from '../../types';

jest.mock('../../utils/api');

describe('components/TafLayout/TafLayout - test tac overview', () => {
  const user = userEvent.setup({ delay: null });

  beforeEach(() => {
    jest.useFakeTimers();
    Element.prototype.scrollTo = jest.fn();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should export a TAF from the TAC overview into the edit form and trigger error on invalid validity', async () => {
    const props = {
      tafList: [
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }, // EHRD
        fakePublishedFixedTaf, // EHGG
      ],
      onUpdateTaf: jest.fn(),
    };
    const mockGetTac = jest
      .fn()
      .mockImplementation(
        (
          taf,
          getJSON: boolean,
        ): Promise<{ data: string | AdverseWeatherTAC }> => {
          return new Promise((resolve) => {
            resolve({ data: getJSON ? fakeTestTacJson : fakeTestTac });
          });
        },
      );
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      getTAC: mockGetTac,
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        within(screen.queryByTestId('location-tabs')!).getAllByRole('menuitem')
          .length,
      ).toBe(2),
    );
    await waitFor(() => expect(mockGetTac).toHaveBeenCalledTimes(3));
    await screen.findByText('draft amendment');

    await waitFor(() => {
      // check editor mode
      expect(screen.queryByTestId('switchMode')!.classList).toContain(
        'Mui-checked',
      );
    });
    expect(screen.queryAllByText('Retrieving TAC').length).toBeFalsy();
    expect(screen.queryAllByTestId('export-tac-button').length).toEqual(2);

    const oldWind = screen
      .getByRole('textbox', { name: 'baseForecast.wind' })
      .getAttribute('value');

    expect(oldWind).toEqual('18010');

    // import TAC
    await user.click(screen.getAllByTestId('export-tac-button')[1]);
    await waitFor(() => {
      expect(screen.queryByTestId('row-changeGroups[2]')).toBeFalsy();
    });

    await waitFor(() => {
      expect(
        screen
          .getByRole('textbox', { name: 'baseForecast.wind' })
          .getAttribute('value'),
      ).not.toEqual(oldWind);
    });
    await waitFor(() => {
      expect(
        screen
          .getByRole('textbox', { name: 'baseForecast.wind' })
          .getAttribute('value'),
      ).toEqual('05005');
    });

    // wait on snackbar
    await waitFor(() => {
      expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    });
    expect(screen.queryByTestId('snackbarComponent')!.textContent).toEqual(
      'TAF EHGG Eelde was imported successfully',
    );

    await waitFor(() => {
      expect(
        screen
          .getByRole('textbox', { name: 'baseForecast.valid' })
          .getAttribute('value'),
      ).toEqual('0614/0718');
    });
    expect(
      screen
        .getByRole('textbox', { name: 'changeGroups[0].valid' })
        .getAttribute('value'),
    ).toEqual('061200'); // this one is invalid
    expect(
      screen
        .getByRole('textbox', { name: 'changeGroups[1].valid' })
        .getAttribute('value'),
    ).toEqual('0623/0624');

    // validations should be triggered
    const changeGroupValid = screen.getByRole('textbox', {
      name: 'changeGroups[0].valid',
    });

    await waitFor(() => {
      expect(
        changeGroupValid.getAttribute('aria-invalid') === 'true',
      ).toBeTruthy();
    });
  });

  it('should open the mobile TAC Overview', async () => {
    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
    };

    render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await screen.findByTestId('location-tabs');
    expect(screen.getByTestId('tac-overview-mobile-button')).toBeTruthy();

    const style = window.getComputedStyle(
      screen.queryByTestId('tac-overview-mobile-button')!,
    );
    expect(style.display).toBe('inline-flex');
    expect(screen.queryByTestId('tac-overview-mobile')).toBeFalsy();

    fireEvent.click(screen.queryByTestId('tac-overview-mobile-button')!);
    await screen.findByTestId('tac-overview-mobile');
  });

  it('should activate selected location in TAC Overview', async () => {
    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
    };

    render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(
        within(screen.getByTestId('location-tabs')).getAllByRole('menuitem'),
      ).toBeTruthy();
    });

    const locations = within(screen.getByTestId('location-tabs')).getAllByRole(
      'menuitem',
    );

    const lastLocation = locations[locations.length - 1];

    fireEvent.click(lastLocation);
    await waitFor(() => {
      expect(Element.prototype.scrollTo).toHaveBeenCalled();

      const activeLocation =
        within(lastLocation).getByTestId('taf-location').textContent!;
      const activeLocations = screen.getAllByRole('listitem', {
        name: activeLocation,
      });

      activeLocations.forEach((location) =>
        expect(location.className).toContain('active'),
      );
    });
  });

  it('should show the TAC export buttons only when form is in edit mode', async () => {
    const props = {
      tafList: [{ ...fakeAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };

    render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        within(screen.getByTestId('location-tabs')).getAllByRole('menuitem')
          .length,
      ).toBe(1),
    );
    // make sure we are in editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    expect(screen.queryAllByTestId('export-tac-button').length).toBeFalsy();

    fireEvent.click(screen.getByTestId('correcttaf'));
    await waitFor(() =>
      expect(screen.getAllByTestId('export-tac-button').length).toBeTruthy(),
    );

    fireEvent.click(screen.getByTestId('cleartaf'));
    await waitFor(() =>
      expect(screen.getAllByTestId('export-tac-button').length).toBeTruthy(),
    );
  });

  it('should export a TAF from the TAC overview into the edit form and not show errors on empty weather and cloud fields', async () => {
    const props = {
      tafList: [
        fakeAmendmentFixedTaf, // EHAM
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }, // EHRD
        fakePublishedFixedTaf, // EHGG
      ],
      onUpdateTaf: jest.fn(),
    };
    const createFakeApi = (): TafApi => ({
      ...createApi(),
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        within(screen.queryByTestId('location-tabs')!).getAllByRole('menuitem')
          .length,
      ).toBe(3),
    );
    expect(screen.queryAllByTestId('export-tac-button').length).toBeFalsy();

    await user.click(screen.queryAllByTestId('status-draft')[0]);
    await screen.findByText('draft amendment');
    const exportButton = within(
      within(screen.getByTestId('tab-panel-0')).getByRole('listitem', {
        name: /EHGG/i,
      }),
    ).getByTestId('export-tac-button');
    await waitFor(() => expect(exportButton).toBeTruthy());

    await waitFor(() => {
      // check editor mode
      expect(screen.queryByTestId('switchMode')!.classList).toContain(
        'Mui-checked',
      );
    });

    const oldWeather = screen
      .getByRole('textbox', { name: 'baseForecast.weather.weather1' })
      .getAttribute('value');
    expect(oldWeather).toEqual('SN');

    const oldCloud = screen
      .getByRole('textbox', { name: 'baseForecast.cloud.cloud1' })
      .getAttribute('value');
    expect(oldCloud).toEqual('BKN035');

    await user.click(exportButton);

    await waitFor(() => {
      const newWeather = screen
        .getByRole('textbox', { name: 'baseForecast.weather.weather1' })
        .getAttribute('value');
      expect(newWeather).not.toEqual(oldWeather);
    });
    await waitFor(() => {
      const newWeather = screen
        .getByRole('textbox', { name: 'baseForecast.weather.weather1' })
        .getAttribute('value');
      expect(newWeather).toEqual('');
    });

    const newCloud = screen
      .getByRole('textbox', { name: 'baseForecast.cloud.cloud1' })
      .getAttribute('value');
    expect(newCloud).not.toEqual(oldCloud);
    expect(newCloud).toEqual('');
    await waitFor(() => {
      expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    });

    // validations should be triggered
    await waitFor(() => {
      expect(
        screen
          .getByRole('textbox', {
            name: 'changeGroups[0].valid',
          })
          .getAttribute('aria-invalid') === 'true',
      ).toBeTruthy();
    });

    // there should be no errors on weather and cloud

    expect(
      screen
        .queryByRole('textbox', {
          name: 'baseForecast.weather.weather1',
        })!
        .getAttribute('aria-invalid') === 'true',
    ).toBeFalsy();

    expect(
      screen
        .queryByRole('textbox', {
          name: 'baseForecast.cloud.cloud1',
        })!
        .getAttribute('aria-invalid') === 'true',
    ).toBeFalsy();
  });
});
