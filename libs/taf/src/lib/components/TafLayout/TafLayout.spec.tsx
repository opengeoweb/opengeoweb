/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import {
  act,
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import TafLayout from './TafLayout';
import {
  fakeAmendmentFixedTaf,
  fakeCancelledFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeDraftAmendmentTaf,
  fakeFixedTafList,
  fakeNewFixedTaf,
  fakePublishedTaf,
  fakeTafList,
  MOCK_USERNAME,
  previousTafForfakeDraftAmendmentFixedTaf,
} from '../../utils/mockdata/fakeTafList';
import { createTestTafStore, TafThemeApiProvider } from '../Providers';
import { createApi } from '../../utils/__mocks__/api';
import { TafApi } from '../../utils/api';
import { TafFromBackend } from '../../types';
import { translateKeyOutsideComponents } from '../../utils/i18n';

jest.mock('../../utils/api');

describe('components/TafLayout/TafLayout', () => {
  const user = userEvent.setup({
    advanceTimers: jest.advanceTimersByTime,
  });

  beforeEach(() => {
    jest.useFakeTimers();
    Element.prototype.scrollTo = jest.fn();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should render with default props', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
    };

    render(
      <TafThemeApiProvider store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    expect(screen.queryByTestId('lastupdated-time')).toBeFalsy();

    await screen.findByTestId('location-tabs');
  });

  it('should render with default auto taf enabled', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
      isAutoTafEnabled: true,
    };

    render(
      <TafThemeApiProvider store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    expect(screen.queryByTestId('lastupdated-time')).toBeFalsy();

    await screen.findByTestId('location-tabs');

    expect(
      screen.getByText(translateKeyOutsideComponents('autotaf')),
    ).toBeTruthy();
  });

  it('should not show anything when taflist is an empty list', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: [],
      onUpdateTaf: jest.fn(),
    };

    const { baseElement } = render(
      <TafThemeApiProvider store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    expect(baseElement).toBeTruthy();
    await waitFor(() =>
      expect(screen.queryByTestId('timeslot-tabs')).toBeNull(),
    );
  });

  it('should not ask for confirmation for switching taf location when form has been saved as draft', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: fakeTafList,
      onUpdateTaf: jest.fn(),
    };

    render(
      <TafThemeApiProvider store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    const locations = within(screen.getByTestId('location-tabs')).getAllByRole(
      'menuitem',
    );

    await waitFor(() => {
      expect(locations).toBeTruthy();
    });

    const lastLocation = locations[locations.length - 1];

    // first location is active
    expect(locations[0].classList).toContain('Mui-selected');
    expect(lastLocation.classList).not.toContain('Mui-selected');

    // make sure we are in editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    // edit the selected taf
    fireEvent.change(
      screen.getByRole('textbox', { name: 'baseForecast.wind' })!,
      {
        target: { value: '01015G35' },
      },
    );
    fireEvent.blur(screen.getByRole('textbox', { name: 'baseForecast.wind' })!);

    // change location without saving
    fireEvent.click(lastLocation);

    await waitFor(() => expect(props.onUpdateTaf).toHaveBeenCalled());

    // change timeslot
    fireEvent.click(screen.getByTestId('toptab-1'));

    // active timeslot is changed
    await waitFor(() => {
      expect(screen.getByTestId('toptab-1').classList).toContain(
        'Mui-selected',
      );
    });
    expect(screen.getByTestId('toptab-0').classList).not.toContain(
      'Mui-selected',
    );
  });

  it('should not ask for confirmation when switching location tabs when form is not changed', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: fakeTafList,
      onUpdateTaf: jest.fn(),
    };

    render(
      <TafThemeApiProvider store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    const locations = within(screen.getByTestId('location-tabs')).getAllByRole(
      'menuitem',
    );

    await waitFor(() => {
      expect(locations).toBeTruthy();
    });
    const lastLocation = locations[locations.length - 1];

    // first location is active
    expect(locations[0].classList).toContain('Mui-selected');
    expect(lastLocation.classList).not.toContain('Mui-selected');

    // make sure we are in editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    // change location
    fireEvent.click(lastLocation);

    // active location is changed
    await waitFor(() => {
      expect(lastLocation.classList).toContain('Mui-selected');
    });
    expect(props.onUpdateTaf).not.toHaveBeenCalled();
  });

  it('should not ask for confirmation when switching location tabs when form is edited and saved', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: fakeTafList,
      onUpdateTaf: jest.fn(),
    };

    render(
      <TafThemeApiProvider store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    const locations = within(screen.getByTestId('location-tabs')).getAllByRole(
      'menuitem',
    );

    await waitFor(() => {
      expect(locations).toBeTruthy();
    });
    const lastLocation = locations[locations.length - 1];

    // first location is active
    expect(locations[0].classList).toContain('Mui-selected');
    expect(lastLocation.classList).not.toContain('Mui-selected');

    // make sure we are in editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    // edit the selected taf
    fireEvent.change(
      screen.getByRole('textbox', { name: 'baseForecast.wind' })!,
      {
        target: { value: '20010' },
      },
    );
    fireEvent.blur(screen.getByRole('textbox', { name: 'baseForecast.wind' })!);

    fireEvent.click(screen.getByTestId('savedrafttaf'));
    await waitFor(() => expect(props.onUpdateTaf).toHaveBeenCalledTimes(1));
    await waitFor(() =>
      expect(screen.getByTestId('issuesButton').textContent).toEqual(
        '0 issues',
      ),
    );
    await waitFor(() => expect(props.onUpdateTaf).toHaveBeenCalledTimes(1));

    // change location again
    fireEvent.click(lastLocation);

    // active location is changed
    await waitFor(() => {
      expect(lastLocation.classList).toContain('Mui-selected');
    });
    expect(props.onUpdateTaf).toHaveBeenCalledTimes(1);
  });

  it('should update the location tabs when switching timeslots', async () => {
    const store = createTestTafStore();
    const now = '2022-01-06T14:00:00Z';
    jest.useFakeTimers().setSystemTime(new Date(now));

    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
    };

    render(
      <TafThemeApiProvider store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(
        within(screen.getByTestId('timeslot-tabs')).getAllByRole('tab'),
      ).toBeTruthy();
    });

    // first timeslot tab is active
    await waitFor(() => {
      expect(screen.getByTestId('toptab-0').classList).toContain(
        'Mui-selected',
      );
    });
    expect(screen.getByTestId('toptab-1').classList).not.toContain(
      'Mui-selected',
    );

    // change timeslot
    fireEvent.click(screen.getByTestId('toptab-1'));

    // active timeslot is changed
    await waitFor(() => {
      expect(screen.getByTestId('toptab-1').classList).toContain(
        'Mui-selected',
      );
    });
    expect(screen.getByTestId('toptab-0').classList).not.toContain(
      'Mui-selected',
    );

    // check that current timeslot has 5 locations
    await waitFor(() => {
      expect(
        within(screen.getByTestId('location-tabs')).getAllByRole('menuitem')
          .length,
      ).toBe(5);
    });
    // change timeslot back
    fireEvent.click(screen.getByTestId('toptab-0'));

    // active timeslot is changed
    await waitFor(() => {
      expect(screen.getByTestId('toptab-1').classList).not.toContain(
        'Mui-selected',
      );
    });
    expect(screen.getByTestId('toptab-0').classList).toContain('Mui-selected');

    // check that selected timeslot has locations
    expect(
      within(screen.getByTestId('location-tabs')).getAllByRole('menuitem')
        .length,
    ).toBeGreaterThan(0);
  });

  it('should update the location tabs when changing timeslots and new tafList is loaded', async () => {
    const store = createTestTafStore();
    const now = '2022-01-06T14:00:00Z';
    jest.useFakeTimers().setSystemTime(new Date(now));

    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
    };

    const { rerender } = render(
      <TafThemeApiProvider store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        within(screen.getByTestId('location-tabs')).getAllByRole('menuitem')
          .length,
      ).toBe(1),
    );

    // first timeslot tab is active
    expect(screen.getByTestId('toptab-0').classList).toContain('Mui-selected');
    expect(screen.getByTestId('toptab-1').classList).not.toContain(
      'Mui-selected',
    );

    // change timeslot
    fireEvent.click(screen.getByTestId('toptab-1'));

    // second timeslot tab is active
    await waitFor(() => {
      expect(screen.getByTestId('toptab-0').classList).not.toContain(
        'Mui-selected',
      );
    });
    expect(screen.getByTestId('toptab-1').classList).toContain('Mui-selected');

    rerender(
      <TafThemeApiProvider store={store}>
        <TafLayout
          tafList={[fakeNewFixedTaf]}
          onUpdateTaf={props.onUpdateTaf}
        />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        within(screen.getByTestId('location-tabs')).getAllByRole('menuitem')
          .length,
      ).toBe(1),
    );

    // change timeslot
    fireEvent.click(screen.getByTestId('toptab-0'));
    await waitFor(() => {
      // first timeslot tab is active
      expect(screen.getByTestId('toptab-0').classList).toContain(
        'Mui-selected',
      );
    });
    expect(screen.getByTestId('toptab-1').classList).not.toContain(
      'Mui-selected',
    );

    await waitFor(() =>
      expect(
        within(screen.getByTestId('location-tabs')).queryAllByRole('menuitem')
          .length,
      ).toBe(0),
    );
  });

  it('should select the previously selected location when new tafList is loaded', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
    };

    const { rerender } = render(
      <TafThemeApiProvider store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    const locations = within(
      screen.getByTestId('location-tabs'),
    ).queryAllByRole('menuitem');
    await waitFor(() => expect(locations.length).toBe(6));

    const lastLocation = locations[locations.length - 1];

    // first location is active
    expect(locations[0].classList).toContain('Mui-selected');
    expect(lastLocation.classList).not.toContain('Mui-selected');

    // select last location
    fireEvent.click(lastLocation);
    await waitFor(() => {
      expect(locations[0].classList).not.toContain('Mui-selected');
    });
    expect(lastLocation.classList).toContain('Mui-selected');

    const locationName = lastLocation.textContent;

    rerender(
      <TafThemeApiProvider store={store}>
        <TafLayout
          tafList={[fakeCancelledFixedTaf, fakeNewFixedTaf]}
          onUpdateTaf={props.onUpdateTaf}
        />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        within(screen.getByTestId('location-tabs')).queryAllByRole('menuitem')
          .length,
      ).toBe(2),
    );

    expect(lastLocation.classList).toContain('Mui-selected');
    expect(lastLocation.textContent).toEqual(locationName);
  });

  it('should select the location corresponding to the previousId when new tafList is loaded', async () => {
    const store = createTestTafStore();
    const tafPublished: TafFromBackend = {
      creationDate: `2022-01-06T12:00:00Z`,
      canbe: ['CANCELLED', 'AMENDED', 'CORRECTED'],
      editor: MOCK_USERNAME,
      taf: {
        ...fakePublishedTaf.taf,
        uuid: 'sowi3u4o-lsdjflsk',
        location: 'TNCB',
        baseTime: `2022-01-06T12:00:00Z`,
        issueDate: `2022-01-06T12:00:00Z`,
        validDateStart: `2022-01-06T12:00:00Z`,
        validDateEnd: `2022-01-07T18:00:00Z`,
        baseForecast: {
          ...fakePublishedTaf.taf.baseForecast,
          valid: {
            start: `2022-01-06T12:00:00Z`,
            end: `2022-01-07T18:00:00Z`,
          },
        },
      },
    };
    const tafDraftAmended: TafFromBackend = {
      creationDate: `2022-01-06T14:00:00Z`,
      canbe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      editor: MOCK_USERNAME,
      taf: {
        ...fakeDraftAmendmentTaf.taf,
        uuid: '23920102',
        previousId: tafPublished.taf.uuid,
        location: 'TNCB',
        baseTime: `2022-01-06T12:00:00Z`,
        validDateStart: `2022-01-06T14:00:00Z`,
        validDateEnd: `2022-01-07T18:00:00Z`,
        previousValidDateStart: `2022-01-06T12:00:00Z`,
        previousValidDateEnd: `2022-01-07T18:00:00Z`,
        baseForecast: {
          ...fakeDraftAmendmentTaf.taf.baseForecast,
          valid: {
            start: `2022-01-06T14:00:00Z`,
            end: `2022-01-07T18:00:00Z`,
          },
        },
        changeGroups: [],
      },
    };
    const tafAmended: TafFromBackend = {
      creationDate: `2022-01-06T14:00:00Z`,
      canbe: ['CANCELLED', 'AMENDED', 'CORRECTED'],
      editor: null!,
      taf: {
        ...tafDraftAmended.taf,
        status: 'AMENDED',
        issueDate: `2022-01-06T14:00:00Z`,
      },
    };
    const fakeCancelledTaf: TafFromBackend = {
      ...fakeCancelledFixedTaf,
      taf: {
        ...fakeCancelledFixedTaf.taf,
        location: 'EHAM',
      },
    };
    const fakeCancelledTaf2: TafFromBackend = {
      ...fakeCancelledFixedTaf,
      taf: {
        ...fakeCancelledFixedTaf.taf,
        uuid: '146456546434343',
        location: 'EHRD',
      },
    };
    const props = {
      tafList: [fakeCancelledTaf, tafDraftAmended, tafPublished],
      onUpdateTaf: jest.fn(),
    };

    const { rerender } = render(
      <TafThemeApiProvider store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );
    const locations = within(screen.getByTestId('location-tabs')).getAllByRole(
      'menuitem',
    );
    await waitFor(() => expect(locations.length).toBe(3));

    // select second location
    fireEvent.click(locations[1]);
    await waitFor(() => {
      expect(locations[0].classList).not.toContain('Mui-selected');
    });
    expect(locations[1].classList).toContain('Mui-selected');

    expect(screen.getByTestId('taf-panel-active').textContent).toContain(
      'draft amendment',
    );

    rerender(
      <TafThemeApiProvider store={store}>
        <TafLayout
          tafList={[fakeCancelledTaf, fakeCancelledTaf2, tafAmended]}
          onUpdateTaf={props.onUpdateTaf}
        />
      </TafThemeApiProvider>,
    );

    await waitFor(() => expect(locations.length).toBe(3));

    expect(screen.getByTestId('taf-panel-active').textContent).toContain(
      'published amendment',
    );
    const updatedLocations = within(
      screen.getByTestId('location-tabs'),
    ).getAllByRole('menuitem');

    // third location should be active
    expect(updatedLocations[2].classList).toContain('Mui-selected');
    expect(updatedLocations[1].classList).not.toContain('Mui-selected');
  });

  it('should display the correct dates in headers', async () => {
    const store = createTestTafStore();
    const now = '2022-01-06T14:00:00Z';
    jest.useFakeTimers().setSystemTime(new Date(now));

    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
    };

    render(
      <TafThemeApiProvider store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(
        within(screen.getByTestId('timeslot-tabs')).getAllByRole('tab'),
      ).toBeTruthy();
    });

    expect(screen.getByTestId('toptab-0').textContent).toBe(
      'Upcoming 18:00 06 Jan',
    );
    expect(screen.getByTestId('toptab-1').textContent).toBe(
      'Current 12:00 06 Jan',
    );
  });

  it('should reset form values when changing TAFs', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: [
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
        previousTafForfakeDraftAmendmentFixedTaf,
        { ...fakeNewFixedTaf, editor: MOCK_USERNAME },
      ],
      onUpdateTaf: jest.fn(),
    };

    const createFakeApi = (): TafApi => ({
      ...createApi(),
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi} store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );
    const tafLocations = within(
      screen.getByTestId('location-tabs'),
    ).getAllByRole('menuitem');

    await waitFor(() => expect(tafLocations.length).toBe(3));

    // select new taf
    fireEvent.click(tafLocations[2]);
    await waitFor(() => {
      const tafLocation = tafLocations[2];
      expect(tafLocation.classList).toContain('Mui-selected');
    });
    await screen.findByText('new');

    // make sure we are in editor mode
    await waitFor(() => {
      expect(screen.getByTestId('switchMode').classList).toContain(
        'Mui-checked',
      );
    });
    const windField = screen.getByRole('textbox', {
      name: 'baseForecast.wind',
    });

    expect(windField.getAttribute('aria-invalid') === 'true').toBeFalsy();

    // try to publish it
    fireEvent.click(screen.getByTestId('publishtaf'));
    await waitFor(() => {
      expect(screen.getByTestId('issuesButton').textContent).toEqual(
        '4 issues',
      );
    });
    await waitFor(() => {
      expect(windField.getAttribute('aria-invalid') === 'true').toBeTruthy();
    });
    expect(props.onUpdateTaf).toHaveBeenCalledTimes(0);
    const locations = within(screen.getByTestId('location-tabs')).getAllByRole(
      'menuitem',
    );

    // select draft TAF
    fireEvent.click(tafLocations[1]);
    await waitFor(() => {
      const tafLocation = locations[1];
      expect(tafLocation.classList).toContain('Mui-selected');
    });
    await screen.findByText('draft amendment');

    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    expect(
      screen
        .getByRole('textbox', {
          name: 'baseForecast.wind',
        })
        .getAttribute('value'),
    ).toEqual('18010');

    // eslint-disable-next-line testing-library/no-unnecessary-act
    await act(async () => {
      // publish draft
      fireEvent.click(screen.getByTestId('publishtaf'));
    });
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(props.onUpdateTaf).toHaveBeenCalledTimes(1);
    });

    // back to new taf
    fireEvent.click(tafLocations[2]);
    await waitFor(() => {
      const tafLocation = tafLocations[2];
      expect(tafLocation.classList).toContain('Mui-selected');
    });
    await screen.findByText('new');

    await waitFor(() => {
      // make sure we are in editor mode
      expect(screen.getByTestId('switchMode').classList).toContain(
        'Mui-checked',
      );
    });

    expect(
      screen
        .queryByRole('textbox', { name: 'baseForecast.wind' })!
        .getAttribute('aria-invalid') === 'true',
    ).toBeFalsy();
    expect(
      screen
        .queryByRole('textbox', { name: 'baseForecast.wind' })!
        .getAttribute('value'),
    ).toBeFalsy();

    // try to publish it again
    fireEvent.click(screen.getByTestId('publishtaf'));
    await waitFor(() => {
      expect(screen.getByTestId('issuesButton').textContent).toEqual(
        '4 issues',
      );
    });
    await waitFor(() => {
      const { parentElement } = screen.getByRole('textbox', {
        name: 'baseForecast.wind',
      })!;
      expect(parentElement!.classList).toContain('Mui-error');
    });
    expect(props.onUpdateTaf).toHaveBeenCalledTimes(1);
  });

  it('should be able to post a taf', async () => {
    const store = createTestTafStore();
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        setTimeout(() => {
          resolve();
        }, 300);
      });
    });
    const props = {
      tafList: [
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
        previousTafForfakeDraftAmendmentFixedTaf,
      ],
      onUpdateTaf: jest.fn(),
    };
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi} store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        within(screen.getByTestId('location-tabs')).getAllByRole('menuitem')
          .length,
      ).toBe(2),
    );
    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    // click publish
    await user.click(screen.getByTestId('publishtaf'));
    // click publish in the confirmation dialog
    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);

    // confirmation dialog should be closed
    await waitFor(() => {
      expect(screen.queryByTestId('confirmationDialog-confirm')).toBeFalsy();
    });

    // should show loading
    await waitFor(() => {
      expect(screen.getByTestId('taf-loading')).toBeTruthy();
    });
    expect(mockPostTaf).toHaveBeenCalled();

    await act(async () => {
      jest.advanceTimersByTime(300);
    });
    await waitFor(() => {
      expect(screen.queryByTestId('taf-loading')).toBeFalsy();
    });
    // onPostTaf callback
    expect(props.onUpdateTaf).toHaveBeenCalled();
  });

  it('should show an error for a cancelled TAF, and remove it after a period of time', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: [fakeAmendmentFixedTaf],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve, reject) => {
        reject(new Error('something went wrong'));
      });
    });
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi} store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await screen.findByTestId('location-tabs');

    // make sure we are in editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    // click cancel
    const cancelButton = await screen.findByTestId('canceltaf');
    fireEvent.click(cancelButton);

    // click cancel in the confirmation dialog
    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);

    // confirmation dialog should be closed
    await waitFor(() => {
      expect(screen.queryByTestId('confirmationDialog-confirm')).toBeFalsy();
    });
    expect(mockPostTaf).toHaveBeenCalled();

    await waitFor(() => {
      // alert should be shown
      expect(screen.getByTestId('taf-error')).toBeTruthy();
    });
    // no callback should have fired
    expect(props.onUpdateTaf).not.toHaveBeenCalled();

    expect(
      await screen.findByText('Cancelling this TAF failed, please try again'),
    ).toBeTruthy();

    // we should still be in editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    // fast forward 10s in time
    await act(async () => {
      jest.advanceTimersByTime(10000);
    });
    await waitFor(() => {
      // alert should be gone
      expect(screen.queryByTestId('taf-error')).toBeFalsy();
    });
  });

  it('should save a taf as draft when switching tabs', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi} store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        within(screen.getByTestId('location-tabs')).getAllByRole('menuitem')
          .length,
      ).toBe(1),
    );
    // make sure we are in editor mode
    expect(screen.queryByTestId('switchMode')!.classList).toContain(
      'Mui-checked',
    );

    // change form value
    fireEvent.change(
      screen.getByRole('textbox', { name: 'baseForecast.wind' })!,
      {
        target: { value: '01015G35' },
      },
    );
    fireEvent.blur(screen.getByRole('textbox', { name: 'baseForecast.wind' })!);
    expect(mockPostTaf).not.toHaveBeenCalled();

    // change tabs
    fireEvent.click(screen.queryByTestId('toptab-1')!);
    await waitFor(() => {
      expect(mockPostTaf).toHaveBeenCalled();
    });

    // test notification
    expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('snackbar-auto-save'),
      ),
    ).toBeTruthy();
  });

  it('should auto save a taf as draft when importing a TAC and then switching tabs', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi} store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        within(screen.getByTestId('location-tabs')).getAllByRole('menuitem')
          .length,
      ).toBe(1),
    );
    // make sure we are in editor mode
    expect(screen.queryByTestId('switchMode')!.classList).toContain(
      'Mui-checked',
    );

    // import TAC
    await user.click(screen.getAllByTestId('export-tac-button')[0]);
    await waitFor(() => {
      expect(screen.queryByTestId('row-changeGroups[0]')).toBeFalsy();
    });

    expect(mockPostTaf).not.toHaveBeenCalled();

    // change tabs
    await user.click(screen.queryByTestId('toptab-1')!);
    await waitFor(() => {
      expect(mockPostTaf).toHaveBeenCalled();
    });

    // test notification
    expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('snackbar-auto-save'),
      ),
    ).toBeTruthy();
  });

  it('should auto save a taf as draft when clearing the form and then switching tabs', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi} store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        within(screen.getByTestId('location-tabs')).getAllByRole('menuitem')
          .length,
      ).toBe(1),
    );
    // make sure we are in editor mode
    expect(screen.queryByTestId('switchMode')!.classList).toContain(
      'Mui-checked',
    );

    // make sure the form has data
    expect(
      screen
        .getByRole('textbox', { name: 'baseForecast.wind' })
        .getAttribute('value'),
    ).toEqual('18010');

    fireEvent.click(screen.getByTestId('cleartaf'));
    await screen.findByTestId('confirmationDialog');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    // test notification
    await screen.findByTestId('snackbarComponent');
    expect(await screen.findByText('TAF has been cleared')).toBeTruthy();

    // form should be reset
    await waitFor(() => {
      expect(
        screen
          .getByRole('textbox', { name: 'baseForecast.wind' })
          .getAttribute('value'),
      ).toEqual('');
    });

    expect(mockPostTaf).not.toHaveBeenCalled();

    // change tabs
    fireEvent.click(screen.queryByTestId('toptab-1')!);
    await waitFor(() => {
      expect(mockPostTaf).toHaveBeenCalled();
    });

    // test notification
    expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('snackbar-auto-save'),
      ),
    ).toBeTruthy();
  });

  it('should not save a taf as draft when importing a TAC, adding a row and entering an invalid value', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi} store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        within(screen.getByTestId('location-tabs')).getAllByRole('menuitem')
          .length,
      ).toBe(1),
    );
    // make sure we are in editor mode
    expect(screen.queryByTestId('switchMode')!.classList).toContain(
      'Mui-checked',
    );

    // import TAC
    await user.click(screen.getAllByTestId('export-tac-button')[0]);
    await waitFor(() => {
      expect(screen.queryByTestId('row-changeGroups[0]')).toBeFalsy();
    });

    // add a row
    await user.click(screen.getByTestId('tafFormOptions[-1]'));
    await screen.findByText('Insert 1 row below');

    await user.click(screen.getByText('Insert 1 row below'));

    await waitFor(() => {
      expect(screen.getByTestId('row-changeGroups[0]')).toBeTruthy();
    });

    // enter invalid value
    const windInput = screen.getByRole('textbox', {
      name: 'changeGroups[0].wind',
    });
    expect(windInput.getAttribute('aria-invalid') === 'true').toBeFalsy();
    // fill in invalid value
    fireEvent.change(windInput, { target: { value: 'HELLO' } });

    // click Save
    fireEvent.click(screen.getByTestId('savedrafttaf'));

    // error should show
    await waitFor(() => {
      expect(screen.getByTestId('issuesButton').textContent).toEqual('1 issue');
    });
    await waitFor(() => {
      expect(windInput.getAttribute('aria-invalid') === 'true').toBeTruthy();
    });

    expect(mockPostTaf).not.toHaveBeenCalled();
  });

  it('should clear the alert banner when switching locations', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: [
        fakeAmendmentFixedTaf,
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
      ],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve, reject) => {
        reject(new Error('something went wrong'));
      });
    });
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi} store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    const locations = within(screen.getByTestId('location-tabs')).getAllByRole(
      'menuitem',
    );

    await waitFor(() => expect(locations.length).toBe(2));
    // open first draft taf
    fireEvent.click(screen.queryAllByTestId('status-draft')[0]);

    // check editor mode
    expect(screen.queryByTestId('switchMode')!.classList).toContain(
      'Mui-checked',
    );

    // try to save
    fireEvent.click(screen.queryByTestId('savedrafttaf')!);

    await waitFor(() => {
      // alert should be shown
      expect(screen.getByTestId('taf-error')).toBeTruthy();
    });
    // no callback should have fired
    expect(props.onUpdateTaf).not.toHaveBeenCalled();

    expect(screen.queryByTestId('snackbarComponent')).toBeFalsy();

    // change location
    const lastLocation = locations[locations.length - 1];

    fireEvent.click(lastLocation);

    await waitFor(() => {
      expect(lastLocation.classList).toContain('Mui-selected');
    });
    await waitFor(() => {
      expect(lastLocation.classList).toContain('Mui-selected');
    });
    await waitFor(() => {
      // alert should be gone
      expect(screen.queryByTestId('taf-error')).toBeFalsy();
    });
  });

  it('should not save a taf as draft when switching tabs when form is not dirty', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: [
        fakeAmendmentFixedTaf,
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
      ],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi} store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        within(screen.getByTestId('location-tabs')).getAllByRole('menuitem')
          .length,
      ).toBe(2),
    ); // open first draft taf
    fireEvent.click(screen.queryAllByTestId('status-draft')[0]);
    // make sure we are in editor mode
    await waitFor(() =>
      expect(screen.queryByTestId('switchMode')!.classList).toContain(
        'Mui-checked',
      ),
    );

    // change tabs
    fireEvent.click(screen.queryByTestId('toptab-1')!);
    await waitFor(() => {
      expect(mockPostTaf).not.toHaveBeenCalled();
    });
    expect(screen.queryByTestId('snackbarComponent')).toBeFalsy();
  });

  it('should not save a taf as draft when switching tabs when form has errors', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: [
        fakeAmendmentFixedTaf,
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
      ],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi} store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        within(screen.getByTestId('location-tabs')).getAllByRole('menuitem')
          .length,
      ).toBe(2),
    );
    // open first draft taf
    fireEvent.click(screen.queryAllByTestId('status-draft')[0]);
    // make sure we are in editor mode
    expect(screen.queryByTestId('switchMode')!.classList).toContain(
      'Mui-checked',
    );

    // change form value
    fireEvent.change(
      screen.getByRole('textbox', { name: 'baseForecast.wind' })!,
      {
        target: { value: 'faultyvalue' },
      },
    );
    fireEvent.blur(screen.getByRole('textbox', { name: 'baseForecast.wind' })!);
    expect(mockPostTaf).not.toHaveBeenCalled();

    // change tabs
    fireEvent.click(screen.queryByTestId('toptab-1')!);

    expect(mockPostTaf).not.toHaveBeenCalled();

    // confirmation should show
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('confirm-dialog-confirm-label'),
      ),
    ).toBeTruthy();

    // click confirm
    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);

    // tab should switch and form not saved
    await waitFor(() => {
      expect(screen.queryByTestId('toptab-1')!.classList).toContain(
        'Mui-selected',
      );
    });
    expect(screen.queryByTestId('toptab-0')!.classList).not.toContain(
      'Mui-selected',
    );
    expect(mockPostTaf).not.toHaveBeenCalled();
    expect(screen.queryByTestId('snackbarComponent')).toBeFalsy();
  });

  it('should clear the alert banner when switching locations when automatic saving failed and user decides to exit without saving', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: [
        fakeAmendmentFixedTaf,
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
      ],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve, reject) => {
        reject(new Error('something went wrong'));
      });
    });
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi} store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );
    const locations = within(screen.getByTestId('location-tabs')).getAllByRole(
      'menuitem',
    );

    await waitFor(() => expect(locations.length).toBe(2));
    // open first draft taf
    fireEvent.click(screen.queryAllByTestId('status-draft')[0]);

    // check editor mode
    expect(screen.queryByTestId('switchMode')!.classList).toContain(
      'Mui-checked',
    );

    // change form value
    fireEvent.change(
      screen.getByRole('textbox', { name: 'baseForecast.wind' })!,
      {
        target: { value: '20020' },
      },
    );
    fireEvent.blur(screen.getByRole('textbox', { name: 'baseForecast.wind' })!);
    expect(mockPostTaf).not.toHaveBeenCalled();

    // change location
    const lastLocation = locations[locations.length - 1];
    fireEvent.click(lastLocation);

    await waitFor(() => {
      expect(mockPostTaf).toHaveBeenCalled();
    });
    // alert should be shown
    expect(screen.getByTestId('taf-error')).toBeTruthy();
    // no callback should have fired
    expect(props.onUpdateTaf).not.toHaveBeenCalled();

    // confirmation should show
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('confirm-dialog-confirm-label'),
      ),
    ).toBeTruthy();

    // click confirm
    await screen.findByTestId('confirmationDialog-confirm');
    fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);

    // active location is changed
    await waitFor(() => {
      expect(lastLocation.classList).toContain('Mui-selected');
    });

    // alert should be gone
    await waitFor(() => {
      expect(screen.queryByTestId('taf-error')).toBeFalsy();
    });
  });

  it('should show an error when posting to BE fails', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve, reject) => {
        reject(new Error('something went wrong'));
      });
    });
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi} store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        within(screen.getByTestId('location-tabs')).getAllByRole('menuitem')
          .length,
      ).toBe(1),
    );
    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    // click save
    fireEvent.click(screen.getByTestId('savedrafttaf'));

    await waitFor(() => {
      expect(mockPostTaf).toHaveBeenCalled();
    });

    await waitFor(() => {
      // alert should be shown
      expect(screen.getByTestId('taf-error')).toBeTruthy();
    });
    // no callback should have fired
    expect(props.onUpdateTaf).not.toHaveBeenCalled();

    // user should be able to close the alert
    fireEvent.click(screen.getByText('CLOSE'));
    await waitFor(() => expect(screen.queryByTestId('taf-error')).toBeFalsy());
  });

  it('should show an error when posting to BE fails the second time and user has clicked the first error away', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve, reject) => {
        reject(new Error('something went wrong'));
      });
    });
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi} store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        within(screen.getByTestId('location-tabs')).getAllByRole('menuitem')
          .length,
      ).toBe(1),
    );
    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    // click save
    fireEvent.click(screen.getByTestId('savedrafttaf'));

    await waitFor(() => {
      expect(mockPostTaf).toHaveBeenCalledTimes(1);
    });

    await waitFor(() => {
      // alert should be shown
      expect(screen.getByTestId('taf-error')).toBeTruthy();
    });
    // no callback should have fired
    expect(props.onUpdateTaf).not.toHaveBeenCalled();

    // user should be able to close the alert
    fireEvent.click(screen.getByText('CLOSE'));
    await waitFor(() => expect(screen.queryByTestId('taf-error')).toBeFalsy());

    // try to save again
    fireEvent.click(screen.getByTestId('savedrafttaf'));

    await waitFor(() => {
      expect(mockPostTaf).toHaveBeenCalledTimes(2);
    });

    await waitFor(() => {
      // alert should be shown again
      expect(screen.getByTestId('taf-error')).toBeTruthy();
    });
    // no callback should have fired
    expect(props.onUpdateTaf).not.toHaveBeenCalled();
  });

  it('should be able to import an autotaf', async () => {
    const store = createTestTafStore();
    const props = {
      tafList: fakeTafList,
      onUpdateTaf: jest.fn(),
      isAutoTafEnabled: true,
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    render(
      <TafThemeApiProvider createApiFunc={createFakeApi} store={store}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        within(screen.getByTestId('autotaf')).getAllByRole('listitem').length,
      ).toBe(5),
    );
    // make sure we are in editor mode
    expect(screen.queryByTestId('switchMode')!.classList).toContain(
      'Mui-checked',
    );

    // import TAC
    await user.click(
      within(screen.getByTestId('autotaf')).getAllByTestId(
        'export-tac-button',
      )[0],
    );

    expect(
      screen.getByText('TAF EHRD Rotterdam was imported successfully'),
    ).toBeTruthy();

    await waitFor(() => {
      expect(screen.queryByTestId('row-changeGroups[0]')).toBeFalsy();
    });

    expect(mockPostTaf).not.toHaveBeenCalled();

    // change tabs
    await user.click(screen.queryByTestId('toptab-1')!);
    await waitFor(() => {
      expect(mockPostTaf).toHaveBeenCalled();
    });

    // test notification
    expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('snackbar-auto-save'),
      ),
    ).toBeTruthy();
  });
});
