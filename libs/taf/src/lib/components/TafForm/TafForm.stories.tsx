/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { Card } from '@mui/material';
import type { StoryObj } from '@storybook/react';
import {
  fakeDraftFixedTaf,
  fakeNewFixedTaf,
  fakePublishedFixedTaf,
  fakeDraftTafWithSameDatesFixed,
  fakeDraftTafWithFM,
} from '../../utils/mockdata/fakeTafList';

import { TafThemeApiProvider } from '../Providers';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import TafForm from './TafForm';
import { ResponsiveWrapper } from '../ResponsiveWrapper/ResponsiveWrapper';
import { TafFromBackend } from '../../types';

interface WrapperProps {
  tafFromBackend: TafFromBackend;
  isDisabled?: boolean;
}

const Wrapper: React.FC<WrapperProps> = ({
  tafFromBackend,
  isDisabled = false,
}: WrapperProps) => (
  <TafThemeApiProvider createApiFunc={createFakeApi} shouldIncludeTheme={false}>
    <div
      style={{
        maxWidth: '1024px',
        caretColor: 'transparent',
      }}
    >
      <ResponsiveWrapper>
        <Card
          variant="outlined"
          sx={{
            padding: 3,
          }}
        >
          <TafForm isDisabled={isDisabled} tafFromBackend={tafFromBackend} />
        </Card>
      </ResponsiveWrapper>
    </div>
  </TafThemeApiProvider>
);

export default {
  title: 'components/Taf Form',
  component: Wrapper,
  tags: ['!autodocs'],
};

type Story = StoryObj<typeof Wrapper>;

export const NewTafFormLight: Story = {
  args: {
    tafFromBackend: fakeNewFixedTaf,
  },
  tags: ['snapshot'],
};

export const NewTafFormDark: Story = {
  args: {
    tafFromBackend: fakeNewFixedTaf,
  },
  tags: ['snapshot', 'dark'],
};

export const EditTafFormLight: Story = {
  args: {
    tafFromBackend: fakeDraftFixedTaf,
  },
  tags: ['snapshot'],
};

export const EditTafFormDark: Story = {
  args: {
    tafFromBackend: fakeDraftFixedTaf,
  },
  tags: ['snapshot', 'dark'],
};

export const ViewTafFormLight: Story = {
  args: {
    tafFromBackend: fakePublishedFixedTaf,
    isDisabled: true,
  },
  tags: ['snapshot'],
};

export const ViewTafFormDark: Story = {
  args: {
    tafFromBackend: fakePublishedFixedTaf,
    isDisabled: true,
  },
  tags: ['snapshot', 'dark'],
};

export const EditTafFormWithMultipleSameStartDatesLight: Story = {
  args: { tafFromBackend: fakeDraftTafWithSameDatesFixed },
  tags: ['snapshot'],
};

export const EditTafFormWithMultipleSameStartDatesDark: Story = {
  args: { tafFromBackend: fakeDraftTafWithSameDatesFixed },
  tags: ['snapshot', 'dark'],
};

export const EditTafFormWithFMOverlapLight: Story = {
  args: { tafFromBackend: fakeDraftTafWithFM },
  tags: ['snapshot'],
};

export const EditTafFormWithFMOverlapDark: Story = {
  args: { tafFromBackend: fakeDraftTafWithFM },
  tags: ['snapshot', 'dark'],
};
