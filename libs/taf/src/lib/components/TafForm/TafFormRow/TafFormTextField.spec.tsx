/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import TafFormTextField, { getDisplayValue } from './TafFormTextField';
import { TafThemeFormProvider } from '../../Providers';

describe('TafForm/TafFormRow/TafFormTextField', () => {
  describe('getDisplayValue', () => {
    it('should return a dash when disabled and no value', () => {
      expect(getDisplayValue('', true)).toEqual('-');
      expect(getDisplayValue(undefined!, true)).toEqual('-');
    });

    it('should return the value when enabled or disabled and value', () => {
      expect(getDisplayValue('50', false)).toEqual('50');
      expect(getDisplayValue('50', true)).toEqual('50');
      expect(getDisplayValue('', false)).toEqual('');
    });
  });

  describe('TafFormTextField', () => {
    it('should autoComplete', async () => {
      render(
        <TafThemeFormProvider
          options={{
            mode: 'onChange',
            reValidateMode: 'onChange',
          }}
        >
          <TafFormTextField
            autoComplete={[
              {
                key: 'R',
                value: 'testing-autocomplete-string',
              },
            ]}
            name="testing"
            label="testing"
          />
        </TafThemeFormProvider>,
      );

      const input = screen.getByRole('textbox');

      fireEvent.keyDown(input, { key: 'R' });

      await waitFor(() => {
        expect(screen.getByRole('textbox').getAttribute('value')).toEqual(
          'testing-autocomplete-string',
        );
      });
    });

    it('should prevent user from typing after autocomplete', async () => {
      render(
        <TafThemeFormProvider
          options={{
            mode: 'onChange',
            reValidateMode: 'onChange',
          }}
        >
          <TafFormTextField
            autoComplete={[
              {
                key: 'R',
                value: 'testing-autocomplete-string',
              },
            ]}
            name="testing"
            label="testing"
          />
        </TafThemeFormProvider>,
      );

      const input = screen.getByRole('textbox');

      fireEvent.keyDown(input, { key: 'R' });

      await waitFor(() => {
        expect(input.getAttribute('value')).toEqual(
          'testing-autocomplete-string',
        );
      });
      expect(input.getAttribute('maxLength')).toEqual(
        'testing-autocomplete-string'.length.toString(),
      );

      // try to change
      fireEvent.keyDown(input, { key: 's' });

      await waitFor(() => {
        expect(input.getAttribute('value')).toEqual(
          'testing-autocomplete-string',
        );
      });
    });

    it('should capitalize on blur', async () => {
      render(
        <TafThemeFormProvider
          options={{
            mode: 'onChange',
            reValidateMode: 'onChange',
          }}
        >
          <TafFormTextField name="testing" label="testing" />
        </TafThemeFormProvider>,
      );

      const input = screen.getByRole('textbox');

      fireEvent.change(input, { target: { value: 'r' } });
      expect(input.getAttribute('value')).toEqual('r');

      fireEvent.blur(input);
      await waitFor(() => expect(input.getAttribute('value')).toEqual('R'));
    });

    it('should prevent uppercase when using prop shouldPreventUppercase', async () => {
      render(
        <TafThemeFormProvider
          options={{
            mode: 'onChange',
            reValidateMode: 'onChange',
            defaultValues: {
              testing: 'Not issued',
            },
          }}
        >
          <TafFormTextField
            name="testing"
            label="testing"
            shouldPreventUppercase
          />
        </TafThemeFormProvider>,
      );

      const input = screen.getByRole('textbox');

      expect(input.getAttribute('value')).toEqual('Not issued');

      fireEvent.blur(input);
      expect(input.getAttribute('value')).toEqual('Not issued');
    });

    it('should not set value on blur if value is same', async () => {
      render(
        <TafThemeFormProvider
          options={{
            mode: 'onChange',
            reValidateMode: 'onChange',
            defaultValues: {
              testing: 'ERROR',
            },
          }}
        >
          <TafFormTextField name="testing" label="testing" />
        </TafThemeFormProvider>,
      );

      const input = screen.getByRole('textbox');

      // update with same value
      fireEvent.change(input, { target: { value: 'ERROR' } });
      expect(input.getAttribute('value')).toEqual('ERROR');

      fireEvent.blur(input);
      expect(input.getAttribute('value')).toEqual('ERROR');

      // update with lowercase
      fireEvent.change(input, { target: { value: 'error' } });
      expect(input.getAttribute('value')).toEqual('error');
      fireEvent.blur(input);

      await act(() => expect(input.getAttribute('value')).toEqual('ERROR'));
    });

    it('should not autocomplete when input field already has a value', async () => {
      render(
        <TafThemeFormProvider
          options={{
            mode: 'onChange',
            reValidateMode: 'onChange',
            defaultValues: {
              testing: '1',
            },
          }}
        >
          <TafFormTextField
            autoComplete={[
              {
                key: 'R',
                value: 'testing-autocomplete-string',
              },
            ]}
            name="testing"
            label="testing"
          />
        </TafThemeFormProvider>,
      );

      const input = screen.getByRole('textbox');
      fireEvent.keyDown(input, { key: 'R' });

      expect(screen.getByRole('textbox').getAttribute('value')).toEqual('1');
    });

    it('should trigger autocomplete when input field has value, but text is selected', async () => {
      render(
        <TafThemeFormProvider
          options={{
            mode: 'onChange',
            reValidateMode: 'onChange',
            defaultValues: {
              testing: 'testing',
            },
          }}
        >
          <TafFormTextField
            autoComplete={[
              {
                key: 'R',
                value: 'testing-autocomplete-string',
              },
            ]}
            name="testing"
            label="testing"
          />
        </TafThemeFormProvider>,
      );

      const input = screen.getByRole<HTMLInputElement>('textbox');

      // trigger selection
      input!.selectionStart = 0;
      input!.selectionEnd = 7;

      fireEvent.keyDown(input, { key: 'R' });
      await waitFor(() => {
        expect(screen.getByRole('textbox').getAttribute('value')).toEqual(
          'testing-autocomplete-string',
        );
      });
    });

    it('should validate when text is selected and user triggers autoComplete', async () => {
      const validTestValue = 'testing-autocomplete-string';
      const testValidate = (value: string): boolean => {
        return value.toUpperCase() === validTestValue.toUpperCase();
      };

      render(
        <TafThemeFormProvider
          options={{
            mode: 'onChange',
            reValidateMode: 'onChange',
            defaultValues: {
              testing: 'testing',
            },
          }}
        >
          <TafFormTextField
            autoComplete={[
              {
                key: 'R',
                value: validTestValue,
              },
            ]}
            rules={{ validate: { testValidate } }}
            name="testing"
            label="testing"
          />
        </TafThemeFormProvider>,
      );

      const input = screen.getByRole<HTMLInputElement>('textbox');

      const faultyTextValue = 'someFaultyValue';
      fireEvent.change(input, { key: faultyTextValue });
      fireEvent.blur(input);
      await waitFor(() => {
        expect(input.getAttribute('aria-invalid') === 'true').toBeTruthy();
      });

      // trigger selection
      input.selectionStart = 0;
      input.selectionEnd = faultyTextValue.length;
      fireEvent.keyDown(input, { key: 'R' });
      await waitFor(() => {
        expect(screen.getByRole('textbox').getAttribute('value')).toEqual(
          'testing-autocomplete-string',
        );
      });
      expect(input.getAttribute('aria-invalid') === 'true').toBeFalsy();
    });

    it('should trigger onchange when given', async () => {
      const mockOnChange = jest.fn();
      render(
        <TafThemeFormProvider
          options={{
            mode: 'onChange',
            reValidateMode: 'onChange',
          }}
        >
          <TafFormTextField
            name="testing"
            label="testing"
            onChange={mockOnChange}
          />
        </TafThemeFormProvider>,
      );

      const input = screen.getByRole('textbox');
      fireEvent.change(input, { target: { value: 'r' } });

      expect(mockOnChange).toHaveBeenCalled();
    });
  });
});
