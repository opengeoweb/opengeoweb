/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { getInitials } from '@opengeoweb/shared';
import { setupServer } from 'msw/node';

import { http, HttpResponse } from 'msw';
import { fakeDraftTaf, MOCK_USERNAME } from '../../utils/mockdata/fakeTafList';
import { TafThemeApiProvider } from '../Providers';
import TafForm, { TafFormProps } from './TafForm';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { tafEndPoints } from '../../utils/taf-api/fakeApi';

jest.mock('../../utils/api');

const server = setupServer(...tafEndPoints);

describe('components/TafForm/TafForm - test editor', () => {
  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  it('should show snackbar when toggling between editor and viewer mode', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
      onSwitchEditor: jest.fn().mockImplementation(() => Promise.resolve(true)),
    };
    server.use(
      http.get('taflist', () => {
        return HttpResponse.json([props.tafFromBackend]);
      }),
    );
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    // make sure we are in editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    // add weather2 input
    const weather2Input = screen.getByRole('textbox', {
      name: 'baseForecast.weather.weather2',
    });
    fireEvent.change(weather2Input!, { target: { value: 'RA' } });
    // switch to viewer mode
    fireEvent.click(screen.getByTestId('switchMode').firstChild!);
    await screen.findByTestId('confirmationDialog');
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(props.onFormAction).not.toHaveBeenCalled();
    });
    expect(props.onSwitchEditor).toHaveBeenCalled();
    // snackbar should be shown to inform you are no longer the editor
    expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('editor-off', {
          location: fakeDraftTaf.taf.location,
        }),
      ),
    ).toBeTruthy();
  });
  it('should show snackbar when toggling between viewer and editor mode', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: null!,
      },
      onFormAction: jest.fn(),
      onSwitchEditor: jest.fn().mockImplementation(() => Promise.resolve(true)),
    };
    server.use(
      http.get('taflist', () => {
        return HttpResponse.json([props.tafFromBackend]);
      }),
    );
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    // make sure we are in viewer mode
    expect(screen.getByTestId('switchMode').classList).not.toContain(
      'Mui-checked',
    );
    // switch to editor mode
    fireEvent.click(screen.getByTestId('switchMode').firstChild!);
    await waitFor(() => {
      expect(props.onSwitchEditor).toHaveBeenCalled();
    });
    // snackbar should be shown to inform you are no longer the editor
    expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('editor-on', {
          location: fakeDraftTaf.taf.location,
        }),
      ),
    ).toBeTruthy();
  });
  it('should show the confirmation dialog when switching from editor to viewer mode with some unsaved changes and reset the form and show snackbar when switched', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
      onSwitchEditor: jest.fn().mockImplementation(() => Promise.resolve(true)),
    };
    server.use(
      http.get('taflist', () => {
        return HttpResponse.json([props.tafFromBackend]);
      }),
    );
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    // make sure we are in editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    // add weather2 input
    const weather2Input = screen.getByRole('textbox', {
      name: 'baseForecast.weather.weather2',
    });
    fireEvent.change(weather2Input!, { target: { value: 'RA' } });
    // switch to viewer mode
    fireEvent.click(screen.getByTestId('switchMode').firstChild!);
    await screen.findByTestId('confirmationDialog');
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(props.onFormAction).not.toHaveBeenCalled();
    });
    expect(props.onSwitchEditor).toHaveBeenCalled();
    // snackbar should be shown to inform you are no longer the editor
    expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('editor-off', {
          location: fakeDraftTaf.taf.location,
        }),
      ),
    ).toBeTruthy();
    // form should be reset
    await waitFor(() =>
      expect(weather2Input.getAttribute('value')).toEqual(''),
    );
  });
  it('should not reset the form when switching from editor to viewer mode failed', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
      onSwitchEditor: jest
        .fn()
        .mockImplementation(() => Promise.resolve(false)),
    };
    server.use(
      http.get('taflist', () => {
        return HttpResponse.json([props.tafFromBackend]);
      }),
    );
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    // make sure we are in editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    // add weather2 input
    const weather2Input = screen.getByRole('textbox', {
      name: 'baseForecast.weather.weather2',
    });
    fireEvent.change(weather2Input!, { target: { value: 'RA' } });
    // switch to viewer mode
    fireEvent.click(screen.getByTestId('switchMode').firstChild!);
    await screen.findByTestId('confirmationDialog');
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(props.onFormAction).not.toHaveBeenCalled();
    });
    expect(props.onSwitchEditor).toHaveBeenCalled();
    // snackbar should not be shown
    expect(screen.queryByTestId('snackbarComponent')).toBeFalsy();
    // form should not be reset
    await waitFor(() =>
      expect(weather2Input.getAttribute('value')).toEqual('RA'),
    );
  });
  it('should show the avatar when user is editor', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
    };
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    await waitFor(() =>
      expect(screen.getByTestId('avatar').textContent).toEqual(
        getInitials(props.tafFromBackend.editor),
      ),
    );
  });
  it('should not show the avatar when taf has no editor', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: null!,
      },
      onFormAction: jest.fn(),
    };
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    await waitFor(() => expect(screen.queryByTestId('avatar')).toBeFalsy());
  });
  it('should show the confirmation dialog when switching from viewer to editor mode when the fetchNewTafList returns another editor', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: '',
      },
      onFormAction: jest.fn(),
      onSwitchEditor: jest.fn().mockImplementation(() => Promise.resolve(true)),
    };
    const mockGetTafList = jest
      .fn()
      .mockResolvedValue(
        HttpResponse.json([{ ...fakeDraftTaf, editor: 'other.user' }]),
      );
    server.use(http.get('taflist', mockGetTafList));
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    // make sure we are in viewer mode
    expect(screen.getByTestId('switchMode').classList).not.toContain(
      'Mui-checked',
    );
    // switch to editor mode
    fireEvent.click(screen.getByTestId('switchMode').firstChild!);
    await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
    await screen.findByTestId('confirmationDialog');
    expect(screen.getByTestId('switchMode').classList).not.toContain(
      'Mui-checked',
    );
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(props.onFormAction).not.toHaveBeenCalled();
    });
    expect(props.onSwitchEditor).toHaveBeenCalled();
    // snackbar should be shown to inform you are now the editor
    expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('editor-on', {
          location: fakeDraftTaf.taf.location,
        }),
      ),
    ).toBeTruthy();
  });
  it('should not call onSwitchEditor when switching from editor to viewer mode when the fetchNewTafList returns another editor', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
      onSwitchEditor: jest.fn().mockImplementation(() => Promise.resolve(true)),
    };
    const mockGetTafList = jest
      .fn()
      .mockResolvedValue(
        HttpResponse.json([{ ...fakeDraftTaf, editor: 'other.user' }]),
      );
    server.use(http.get('taflist', mockGetTafList));
    render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    // make sure we are in editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    // switch to viewer mode
    fireEvent.click(screen.getByTestId('switchMode').firstChild!);
    await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
    expect(props.onSwitchEditor).not.toHaveBeenCalled();
  });
});
