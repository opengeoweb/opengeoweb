/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Theme } from '@mui/material';
import { lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import { ApiProvider } from '@opengeoweb/api';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import { AuthenticationProvider } from '@opengeoweb/authentication';
import { combineReducers, configureStore, Store } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { UseFormProps } from 'react-hook-form';
import { ConfirmationServiceProvider } from '@opengeoweb/shared';
import {
  snackbarMiddlewares,
  snackbarReducersMap,
  SnackbarWrapperConnect,
} from '@opengeoweb/snackbar';
import { I18nextProvider } from 'react-i18next';
import i18n from 'i18next';
import { ToolkitStore } from '@reduxjs/toolkit/dist/configureStore';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { initTafI18n } from '../../utils/i18n';
import { createApi, TafApi } from '../../utils/api';
import { TafWrapper } from '../TafWrapper';
import { MOCK_USERNAME } from '../../utils/mockdata/fakeTafList';

const rootReducer = combineReducers({
  ...snackbarReducersMap,
});

export const createTestTafStore = (): ToolkitStore => {
  return configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat([...snackbarMiddlewares]),
  });
};

export interface TafThemeApiProviderProps {
  createApiFunc?: () => TafApi;
  children: React.ReactNode;
  theme?: Theme;
  store?: Store;
  shouldIncludeTheme?: boolean;
}

interface TafTranslationWrapperProps {
  children?: React.ReactNode;
}

export const TafThemeWrapper: React.FC<{
  children: React.ReactNode;
  theme?: Theme;
}> = ({ theme = lightTheme, children }) => {
  return (
    <TafI18nProvider>
      <ThemeWrapper theme={theme}>{children}</ThemeWrapper>
    </TafI18nProvider>
  );
};

export const TafThemeApiProvider: React.FC<TafThemeApiProviderProps> = ({
  children,
  createApiFunc = null!,
  theme = lightTheme,
  store = createTestTafStore(),
  shouldIncludeTheme = true,
}: TafThemeApiProviderProps) => {
  const queryClient = new QueryClient({
    defaultOptions: { queries: { retry: false }, mutations: { retry: false } },
  });

  const content = (
    <AuthenticationProvider
      value={{
        isLoggedIn: true,
        auth: {
          username: MOCK_USERNAME,
          token: '1223344',
          refresh_token: '33455214',
        },
        onLogin: (): void => null!,
        onSetAuth: (): void => null!,
        sessionStorageProvider: null!,
      }}
    >
      <ApiProvider
        createApi={createApiFunc !== null ? createApiFunc : createApi}
      >
        <QueryClientProvider client={queryClient}>
          <Provider store={store}>
            <SnackbarWrapperConnect>
              <ConfirmationServiceProvider>
                <TafWrapper>{children}</TafWrapper>
              </ConfirmationServiceProvider>
            </SnackbarWrapperConnect>
          </Provider>
        </QueryClientProvider>
      </ApiProvider>
    </AuthenticationProvider>
  );

  return shouldIncludeTheme ? (
    <TafThemeWrapper theme={theme}>{content}</TafThemeWrapper>
  ) : (
    content
  );
};

interface TafThemeFormProviderProps {
  children: React.ReactNode;
  options?: UseFormProps;
  theme?: Theme;
}

export const TafThemeFormProvider: React.FC<TafThemeFormProviderProps> = ({
  children,
  options = {},
  theme = lightTheme,
}: TafThemeFormProviderProps) => (
  <TafThemeWrapper theme={theme}>
    <ReactHookFormProvider options={options}>{children}</ReactHookFormProvider>
  </TafThemeWrapper>
);

const TafI18nProvider: React.FC<TafTranslationWrapperProps> = ({
  children,
}) => {
  initTafI18n();
  return <I18nextProvider i18n={i18n}>{children}</I18nextProvider>;
};
