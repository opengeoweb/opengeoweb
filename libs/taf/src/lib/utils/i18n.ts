/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import i18n from 'i18next';
import {
  UseTranslationResponse,
  initReactI18next,
  useTranslation,
} from 'react-i18next';
import { SHARED_NAMESPACE, sharedTranslations } from '@opengeoweb/shared';
import tafTranslations from '../../../locales/taf.json';

export const TAF_NAMESPACE = 'taf';

export const initTafI18n = (): void => {
  void i18n.use(initReactI18next).init({
    lng: 'en',
    fallbackLng: 'en',
    ns: [TAF_NAMESPACE],
    interpolation: {
      escapeValue: false,
    },
    resources: {
      en: {
        [TAF_NAMESPACE]: tafTranslations.en,
        [SHARED_NAMESPACE]: sharedTranslations.en,
      },
      fi: {
        [TAF_NAMESPACE]: tafTranslations.fi,
        [SHARED_NAMESPACE]: sharedTranslations.fi,
      },
      no: {
        [TAF_NAMESPACE]: tafTranslations.no,
        [SHARED_NAMESPACE]: sharedTranslations.no,
      },
      nl: {
        [TAF_NAMESPACE]: tafTranslations.nl,
        [SHARED_NAMESPACE]: sharedTranslations.nl,
      },
    },
  });
};

const ns = [TAF_NAMESPACE, SHARED_NAMESPACE];

export const translateKeyOutsideComponents = (
  key: string,
  params: Record<string, string | number> | undefined = undefined,
): string => i18n.t(key, { ns, ...params });

export const useTafTranslation = (): UseTranslationResponse<
  typeof TAF_NAMESPACE,
  typeof i18n
> => useTranslation(ns);
