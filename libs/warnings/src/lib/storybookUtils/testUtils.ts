/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  PublicWarning,
  PublicWarningStatus,
} from '../store/publicWarningForm/types';

export const testGeoJSON: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [5.0, 55.0],
            [4.331914, 55.332644],
            [3.368817, 55.764314],
            [2.761908, 54.379261],
            [3.15576, 52.913554],
            [2.000002, 51.500002],
            [3.370001, 51.369722],
            [3.370527, 51.36867],
            [3.362223, 51.320002],
            [3.36389, 51.313608],
            [3.373613, 51.309999],
            [3.952501, 51.214441],
            [4.397501, 51.452776],
            [5.078611, 51.391665],
            [5.848333, 51.139444],
            [5.651667, 50.824717],
            [6.011797, 50.757273],
            [5.934168, 51.036386],
            [6.222223, 51.361666],
            [5.94639, 51.811663],
            [6.405001, 51.830828],
            [7.053095, 52.237764],
            [7.031389, 52.268885],
            [7.063612, 52.346109],
            [7.065557, 52.385828],
            [7.133055, 52.888887],
            [7.14218, 52.898244],
            [7.191667, 53.3],
            [6.5, 53.666667],
            [6.500002, 55.000002],
            [5.0, 55.0],
          ],
        ],
      },
      properties: {
        selectionType: 'fir',
      },
    },
  ],
};

export function fakeErrorRequest<ReturnType>(
  error: string,
): Promise<ReturnType> {
  const fakeError = new Error(error);
  return new Promise((_, reject) => {
    setTimeout(() => reject(fakeError), 600);
  });
}

export const fakePostError = `2 validation errors for Request
body -> warningDetail -> phenomenon
  value is not a valid enumeration member; permitted: 'wind', 'fog', 'thunderstorm', 'snowIce', 'rain', 'highTemp', 'funnelCloud', 'lowTemp', 'coastalEvent' (type=type_error.enum; enum_values=[<PhenomenonEnum.WIND: 'wind'>, <PhenomenonEnum.FOG: 'fog'>, <PhenomenonEnum.THUNDERSTORM: 'thunderstorm'>, <PhenomenonEnum.SNOW_ICE: 'snowIce'>, <PhenomenonEnum.RAIN: 'rain'>, <PhenomenonEnum.HIGH_TEMP: 'highTemp'>, <PhenomenonEnum.FUNNEL_CLD: 'funnelCloud'>, <PhenomenonEnum.LOW_TEMP: 'lowTemp'>, <PhenomenonEnum.COASTAL_EVENT: 'coastalEvent'>])
body -> warningDetail -> level
  value is not a valid enumeration member; permitted: 'moderate', 'severe', 'extreme' (type=type_error.enum; enum_values=[<LevelEnum.MODERATE: 'moderate'>, <LevelEnum.SEVERE: 'severe'>, <LevelEnum.EXTREME: 'extreme'>])`;

export const testPublicWarning: PublicWarning = {
  warningDetail: {
    id: '923723984872338768743',
    phenomenon: 'coastalEvent',
    validFrom: '2022-06-01T15:00:00Z',
    validUntil: '2022-06-01T18:00:00Z',
    level: 'extreme',
    probability: 80,
    descriptionOriginal:
      'Some pretty intense coastal weather is coming our way',
    descriptionTranslation: 'And this would be the translation',
    areas: [{ geoJSON: testGeoJSON, objectName: 'test name' }],
  },
  status: PublicWarningStatus.DRAFT,
  type: 'public',
};
