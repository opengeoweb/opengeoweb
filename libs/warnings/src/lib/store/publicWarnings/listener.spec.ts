/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { getLastUpdatedTimeFormatted } from './listener';

describe('store/publicWarnings/listeners', () => {
  describe('getLastUpdatedTimeFormatted', () => {
    it('should return formatted lastUpdatedTime', () => {
      const now = '2024-01-23T10:37:33Z';
      jest.useFakeTimers().setSystemTime(new Date(now));
      expect(getLastUpdatedTimeFormatted()).toEqual('10:37');

      const now2 = '2023-01-22T12:14:33Z';
      jest.useFakeTimers().setSystemTime(new Date(now2));
      expect(getLastUpdatedTimeFormatted()).toEqual('12:14');
    });
  });
});
