/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { WarningModuleStore } from '../types';
import {
  getPublicWarning,
  getPublicWarningFormState,
  getPublicWarningFormError,
  getPublicWarningObject,
  getPublicWarningStatus,
  getPublicWarningStore,
  getSelectedWarningId,
  getSelectedPublicIsFormDirty,
  getPublicWarningId,
  getPublicWarningAreas,
  getWarningType,
} from './selectors';
import { initialState } from './reducer';
import { testGeoJSON, testPublicWarning } from '../../storybookUtils/testUtils';

describe('public warning selectors', () => {
  const initialTestState: WarningModuleStore = {
    publicWarningForm: {
      object: {
        id: '923723984872338768743',
        objectName: 'Drawing object 101',
        lastUpdatedTime: '2022-06-01T12:34:27Z',
        scope: 'user',
        geoJSON: testGeoJSON,
      },
      warning: {
        ...testPublicWarning,
        editor: 'user.name',
      },
      formState: 'publishing',
    },
  };

  it('should return draw store using getPublicWarningStore selector', () => {
    const publicWarningStore = getPublicWarningStore(initialTestState);
    expect(publicWarningStore).toEqual(initialTestState.publicWarningForm);
  });

  it('should return public warning object', () => {
    const publicWarningStore = getPublicWarningObject(initialTestState);
    expect(publicWarningStore).toEqual(
      initialTestState.publicWarningForm?.object,
    );
    expect(
      getPublicWarningObject({ publicWarningForm: initialState }),
    ).toBeUndefined();
  });

  it('should return public warning', () => {
    const publicWarningStore = getPublicWarning(initialTestState);
    expect(publicWarningStore).toEqual(
      initialTestState.publicWarningForm?.warning,
    );
    expect(
      getPublicWarning({ publicWarningForm: initialState }),
    ).toBeUndefined();
  });

  it('should return public warning status', () => {
    const publicWarningStore = getPublicWarningStatus(initialTestState);
    expect(publicWarningStore).toEqual(
      initialTestState.publicWarningForm?.warning?.status,
    );
    expect(
      getPublicWarningStatus({ publicWarningForm: initialState }),
    ).toBeUndefined();
  });

  it('should return public warning form action', () => {
    const publicWarningStore = getPublicWarningFormState(initialTestState);
    expect(publicWarningStore).toEqual(
      initialTestState.publicWarningForm?.formState,
    );
    expect(
      getPublicWarningFormState({ publicWarningForm: initialState }),
    ).toEqual('');
  });

  it('should return public warning form error', () => {
    expect(getPublicWarningFormError(initialTestState)).toBeUndefined();
    const testError = 'something went wrong';
    expect(
      getPublicWarningFormError({
        publicWarningForm: {
          ...initialState,
          error: { message: testError, severity: 'error' },
        },
      }),
    ).toEqual({ message: testError, severity: 'error' });
  });

  it('should return selected public warning id', () => {
    expect(getSelectedWarningId(initialTestState)).toEqual('');
    const testId = 'some id';
    expect(
      getSelectedWarningId({
        publicWarningForm: { ...initialState, selectedWarningId: testId },
      }),
    ).toEqual(testId);
  });

  it('should return isFormDirty', () => {
    expect(
      getSelectedPublicIsFormDirty({
        ...initialTestState,
        publicWarningForm: {
          ...initialTestState.publicWarningForm,
          isFormDirty: true,
        },
      }),
    ).toBeTruthy();

    expect(getSelectedPublicIsFormDirty(initialTestState)).toBeFalsy();
    expect(getSelectedPublicIsFormDirty({})).toBeFalsy();
  });
  it('should return public warning id', () => {
    const publicWarningId = getPublicWarningId(initialTestState);
    expect(publicWarningId).toEqual(
      initialTestState.publicWarningForm?.warning?.warningDetail.id,
    );
    expect(
      getPublicWarningId({ publicWarningForm: initialState }),
    ).toBeUndefined();
  });

  it('should return public warning areas', () => {
    const publicWarningId = getPublicWarningAreas(initialTestState);
    expect(publicWarningId).toEqual(
      initialTestState.publicWarningForm?.warning?.warningDetail.areas,
    );
    expect(getPublicWarningAreas({ publicWarningForm: initialState })).toEqual(
      [],
    );
  });

  it('should return warningType', () => {
    expect(
      getWarningType({
        ...initialTestState,
        publicWarningForm: {
          ...initialTestState.publicWarningForm,
          warningType: 'sigmet',
        },
      }),
    ).toEqual('sigmet');

    expect(getWarningType(initialTestState)).toBeFalsy();
    expect(getWarningType({})).toBeFalsy();
  });
});
