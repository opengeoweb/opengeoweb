/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { uiActions, uiTypes } from '@opengeoweb/store';
import {
  initialState,
  publicWarningFormActions,
  WarningFormState,
  publicWarningFormReducer,
} from './reducer';
import { publicWarningDialogType } from './utils';
import { DrawingListItem } from '../warningsDrawings/types';
import { Warning, PublicWarningDetail, PublicWarningStatus } from './types';
import { testGeoJSON } from '../../storybookUtils/testUtils';

const object: DrawingListItem = {
  id: '923723984872338768743',
  objectName: 'Drawing object 101',
  lastUpdatedTime: '2022-06-01T12:34:27Z',
  scope: 'user',
  geoJSON: testGeoJSON,
};

const publicWarningDetail: PublicWarningDetail = {
  id: '923723984872338768743',
  phenomenon: 'coastalEvent',
  validFrom: '2022-06-01T15:00:00Z',
  validUntil: '2022-06-01T18:00:00Z',
  level: 'extreme',
  probability: 80,
  descriptionOriginal: 'Some pretty intense coastal weather is coming our way',
  descriptionTranslation: 'And this would be the translation',
  areas: [{ geoJSON: testGeoJSON, objectName: 'test name' }],
};

const draftPublicWarning: Warning = {
  warningDetail: publicWarningDetail,
  status: PublicWarningStatus.DRAFT,
  type: 'public',
};

describe('public warning publicWarningFormReducer', () => {
  describe('setFormValues', () => {
    const actionPayload = {
      object,
      warning: draftPublicWarning,
    };
    it('should set setFormValues', () => {
      const result = publicWarningFormReducer(
        initialState,
        publicWarningFormActions.setFormValues(actionPayload),
      );

      expect(result.object?.id).toEqual(actionPayload.object.id);
      expect(result.object?.objectName).toEqual(
        actionPayload.object.objectName,
      );
      expect(result.object?.lastUpdatedTime).toEqual(
        actionPayload.object.lastUpdatedTime,
      );
      expect(result.warning).toEqual(actionPayload.warning);
      expect(result.formState).toBeUndefined();

      const actionPayLoadWithFormAction = {
        formState: 'readonly' as const,
      };
      const resultWithFormAction = publicWarningFormReducer(
        initialState,
        publicWarningFormActions.setFormValues(actionPayLoadWithFormAction),
      );
      expect(resultWithFormAction.formState).toEqual(
        actionPayLoadWithFormAction.formState,
      );

      const actionPayLoadWithFormAction2 = {
        formState: '' as const,
      };
      const resultWithFormAction2 = publicWarningFormReducer(
        initialState,
        publicWarningFormActions.setFormValues(actionPayLoadWithFormAction2),
      );
      expect(resultWithFormAction2.formState).toEqual(
        actionPayLoadWithFormAction2.formState,
      );
    });

    it('should reset values when public warning dialog closes', () => {
      const state = {
        ...actionPayload,
        error: { message: 'something went wrong' },
        formState: 'readonly' as const,
        warning: {
          ...actionPayload.warning,
          status: PublicWarningStatus.DRAFT,
        },
        isFormDirty: true,
      };

      const result = publicWarningFormReducer(
        state,
        uiActions.setToggleOpenDialog({
          type: uiTypes.DialogTypes.DrawingTool,
          setOpen: false,
        }),
      );

      expect(result.object?.id).toEqual(actionPayload.object.id);

      expect(result.object?.objectName).toEqual(
        actionPayload.object.objectName,
      );
      expect(result.warning).toEqual(actionPayload.warning);

      expect(result.object?.lastUpdatedTime).toEqual(
        actionPayload.object.lastUpdatedTime,
      );
      expect(result.error).toEqual(state.error);
      expect(result.warning?.status).toEqual(state.warning.status);
      expect(result.formState).toEqual(state.formState);
      expect(result.isFormDirty).toEqual(state.isFormDirty);

      const result2 = publicWarningFormReducer(
        state,
        uiActions.setToggleOpenDialog({
          type: publicWarningDialogType,
          setOpen: false,
        }),
      );

      expect(result2.object).toBeUndefined();
      expect(result2.warning).toBeUndefined();
      expect(result2.formState).toBeUndefined();
      expect(result2.error).toBeUndefined();
      expect(result2.formState).toBeUndefined();
      expect(result2.isFormDirty).toBeUndefined();
    });
  });

  describe('openPublicWarningFormDialog', () => {
    const actionPayload = {
      object,
      warning: draftPublicWarning,
      formState: 'readonly' as const,
    };
    it('should open dialog and set selectedWarningId', () => {
      const result = publicWarningFormReducer(
        initialState,
        publicWarningFormActions.openPublicWarningFormDialog(actionPayload),
      );

      expect(result.object?.id).toEqual(actionPayload.object.id);
      expect(result.object?.objectName).toEqual(
        actionPayload.object.objectName,
      );
      expect(result.object?.lastUpdatedTime).toEqual(
        actionPayload.object.lastUpdatedTime,
      );
      expect(result.warning).toEqual(actionPayload.warning);
      expect(result.formState).toEqual(actionPayload.formState);
      expect(result.selectedWarningId).toEqual(actionPayload.warning.id);

      expect(result.warning?.status).toEqual(actionPayload.warning.status);

      const closeDialogResult = publicWarningFormReducer(
        result,
        uiActions.setToggleOpenDialog({
          type: publicWarningDialogType,
          setOpen: false,
        }),
      );

      expect(closeDialogResult.selectedWarningId).toBeUndefined();
    });

    it('should open dialog and not set selectedWarningId', () => {
      const result = publicWarningFormReducer(
        initialState,
        publicWarningFormActions.openPublicWarningFormDialog({}),
      );

      expect(result.object?.id).toBeUndefined();
      expect(result.object?.objectName).toBeUndefined();
      expect(result.warning).toBeUndefined();
      expect(result.formState).toEqual('');
      expect(result.selectedWarningId).toBeUndefined();
    });
  });
  describe('setFormError', () => {
    it('should set error', () => {
      const error = {
        message: 'something went wrong',
      };
      const result = publicWarningFormReducer(
        initialState,
        publicWarningFormActions.setFormError(error),
      );
      expect(result.formState).toEqual('');
      expect(result.error).toEqual({
        message: error.message,
        severity: 'error',
      });
    });

    it('should set error as warning', () => {
      const error = {
        message: 'something went wrong',
        severity: 'warning' as const,
      };
      const result = publicWarningFormReducer(
        initialState,
        publicWarningFormActions.setFormError(error),
      );
      expect(result.formState).toEqual('');
      expect(result.error).toEqual(error);
    });

    it('should set error and reset formAction', () => {
      const testInitialState = {
        ...initialState,
        error: { message: 'test error' },
        formState: 'publishing' as const,
      };
      const error = {
        message: 'something went wrong',
      };
      const result = publicWarningFormReducer(
        testInitialState,
        publicWarningFormActions.setFormError(error),
      );
      expect(result.formState).toEqual('');
      expect(result.error).toEqual({
        message: error.message,
        severity: 'error',
      });
    });
  });

  describe('setFormDirty', () => {
    it('should set isFormDirty', () => {
      const result = publicWarningFormReducer(
        initialState,
        publicWarningFormActions.setFormDirty({
          isFormDirty: true,
        }),
      );
      expect(result.isFormDirty).toBeTruthy();

      const result2 = publicWarningFormReducer(
        result,
        publicWarningFormActions.setFormDirty({
          isFormDirty: false,
        }),
      );
      expect(result2.isFormDirty).toBeFalsy();
    });
  });

  describe('addArea', () => {
    it('should add area on a non existing warning', () => {
      const payload = {
        area: { geoJSON: testGeoJSON, objectName: 'test name' },
      };
      const result = publicWarningFormReducer(
        initialState,
        publicWarningFormActions.addArea(payload),
      );
      expect(result.warning?.warningDetail).toEqual({
        areas: [payload.area],
      });
    });

    it('should add area on an existing warning', () => {
      const payload = {
        area: { geoJSON: testGeoJSON, objectName: 'test name' },
      };

      const testState: WarningFormState = {
        ...initialState,
        warning: {
          warningDetail: publicWarningDetail,
          type: 'public',
        },
      };

      const result = publicWarningFormReducer(
        testState,
        publicWarningFormActions.addArea(payload),
      );
      expect(result.warning?.warningDetail).toEqual({
        ...testState.warning?.warningDetail,
        areas: [payload.area],
      });
    });

    it('should add area on an existing warning without any areas', () => {
      const payload = {
        area: { geoJSON: testGeoJSON, objectName: 'test name' },
      };

      const testState: WarningFormState = {
        ...initialState,
        warning: {
          warningDetail: { ...publicWarningDetail, areas: undefined },
          type: 'public',
        },
      };

      const result = publicWarningFormReducer(
        testState,
        publicWarningFormActions.addArea(payload),
      );
      expect(result.warning?.warningDetail).toEqual({
        ...testState.warning?.warningDetail,
        areas: [payload.area],
      });
    });

    it('should preserve uuid of previous area when changing area', () => {
      const payload = {
        area: {
          geoJSON: testGeoJSON,
          objectName: 'new name',
        },
      };

      const testId = 'id-01';

      const testState: WarningFormState = {
        ...initialState,
        warning: {
          warningDetail: {
            ...publicWarningDetail,
            areas: [
              {
                geoJSON: testGeoJSON,
                objectName: 'test name',
                published_warning_uuid: testId,
              },
            ],
          },
          type: 'public',
        },
      };

      const result = publicWarningFormReducer(
        testState,
        publicWarningFormActions.addArea(payload),
      );
      expect(result.warning?.warningDetail).toEqual({
        ...testState.warning?.warningDetail,
        areas: [{ ...payload.area, published_warning_uuid: testId }],
      });

      expect(
        result.warning?.warningDetail.areas?.[0].published_warning_uuid,
      ).toEqual(testId);
      expect(result.warning?.warningDetail.areas?.[0].objectName).toEqual(
        payload.area.objectName,
      );
    });
  });

  const testArea = { geoJSON: testGeoJSON, objectName: 'test name' };
  const otherArea = { geoJSON: testGeoJSON, objectName: 'other object' };
  describe('removeArea', () => {
    it('should not fail on a non existing warning', () => {
      const payload = {
        area: { geoJSON: testGeoJSON, objectName: 'test name' },
      };
      const result = publicWarningFormReducer(
        initialState,
        publicWarningFormActions.removeArea(payload),
      );
      expect(result).toEqual(initialState);
    });

    it('should not fail on a non existing area', () => {
      const payload = {
        area: testArea,
      };

      const testState: WarningFormState = {
        ...initialState,
        warning: {
          warningDetail: {
            ...publicWarningDetail,
            areas: [otherArea],
          },
          type: 'public',
        },
      };

      const result = publicWarningFormReducer(
        testState,
        publicWarningFormActions.removeArea(payload),
      );
      expect(result.warning?.warningDetail).toEqual({
        ...testState.warning?.warningDetail,

        areas: [otherArea],
      });
    });

    it('should remove given area on an existing warning', () => {
      const payload = {
        area: testArea,
      };

      const testState: WarningFormState = {
        ...initialState,
        warning: {
          warningDetail: {
            ...publicWarningDetail,
            areas: [payload.area, otherArea],
          },
          type: 'public',
        },
      };

      const result = publicWarningFormReducer(
        testState,
        publicWarningFormActions.removeArea(payload),
      );
      expect(result.warning?.warningDetail).toEqual({
        ...testState.warning?.warningDetail,
        areas: [otherArea],
      });
    });
  });

  describe('removeAllAreas', () => {
    it('should not fail on a non existing warning', () => {
      const result = publicWarningFormReducer(
        initialState,
        publicWarningFormActions.removeAllAreas(),
      );
      expect(result).toEqual(initialState);
    });

    it('should remove all areas on an existing warning', () => {
      const testState: WarningFormState = {
        ...initialState,
        warning: {
          warningDetail: {
            ...publicWarningDetail,
            areas: [testArea, otherArea],
          },
          type: 'public',
        },
      };

      const result = publicWarningFormReducer(
        testState,
        publicWarningFormActions.removeAllAreas(),
      );
      expect(result.warning?.warningDetail).toEqual({
        ...testState.warning?.warningDetail,
        areas: [],
      });
    });
  });
});
