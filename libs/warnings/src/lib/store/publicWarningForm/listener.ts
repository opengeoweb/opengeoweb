/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { uiActions } from '@opengeoweb/store';
import { createListenerMiddleware } from '@reduxjs/toolkit';
import { publicWarningFormActions } from './reducer';
import { publicWarningDialogType } from './utils';

import { WarningModuleStore } from '../types';
import { areaObjectLoaderType } from '../warningsDrawings/utils';

export const publicWarningFormListener =
  createListenerMiddleware<WarningModuleStore>();

publicWarningFormListener.startListening({
  actionCreator: publicWarningFormActions.openPublicWarningFormDialog,
  effect: async ({ payload }, listenerApi) => {
    listenerApi.cancelActiveListeners();

    const { panelId } = payload;

    listenerApi.dispatch(
      uiActions.setToggleOpenDialog({
        setOpen: true,
        type: publicWarningDialogType,
        sourcePanelId: panelId,
      }),
    );
  },
});

publicWarningFormListener.startListening({
  actionCreator: uiActions.setToggleOpenDialog,
  effect: async ({ payload }, listenerApi) => {
    if (!payload.setOpen && payload.type === publicWarningDialogType) {
      listenerApi.dispatch(
        uiActions.setToggleOpenDialog({
          setOpen: false,
          type: areaObjectLoaderType,
        }),
      );
    }
  },
});
