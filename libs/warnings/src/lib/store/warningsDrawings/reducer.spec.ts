/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { uiActions, uiTypes } from '@opengeoweb/store';
import {
  warningsDrawingsActions,
  drawAdapter,
  initialState,
  warningsDrawingsReducer,
} from './reducer';
import { selectDrawByInstanceId } from './selectors';
import { DrawItem, SubmitDrawValuesPayload } from './types';
import { testGeoJSON } from '../../storybookUtils/testUtils';
import { drawingDialogType } from './utils';

describe('drawings reducer', () => {
  it('should handle setDrawValues when mapId exists', () => {
    const drawingInstanceId = 'existingMapId';
    const initialDrawItem: DrawItem = {
      id: '',
      drawingInstanceId,
      objectName: '',
    };

    const prevState = drawAdapter.setAll(initialState, [initialDrawItem]);

    const actionPayload = {
      drawingInstanceId,
      geoJSON: testGeoJSON,
      objectName: 'New Object Name',
      id: 'testid',
    };

    const nextState = warningsDrawingsReducer(
      prevState,
      warningsDrawingsActions.setDrawValues(actionPayload),
    );

    const result = selectDrawByInstanceId(
      { drawings: nextState },
      drawingInstanceId,
    );

    expect(result?.objectName).toEqual(actionPayload.objectName);
    expect(result?.id).toEqual(actionPayload.id);
  });

  it('should not modify state when setDrawValues called with unknown mapId', () => {
    const unknownMapId = 'unknownMapId';
    const actionPayload: DrawItem = {
      drawingInstanceId: unknownMapId,
      objectName: 'New Object Name',
      id: '',
    };

    const nextState = warningsDrawingsReducer(
      initialState,
      warningsDrawingsActions.setDrawValues(actionPayload),
    );

    expect(nextState).toEqual(initialState);
  });

  it('should register a drawing entity when drawingTool dialog is registered correctly', () => {
    const drawingInstanceId = uiTypes.DialogTypes.DrawingTool;
    const actionPayload = { drawingInstanceId, type: drawingInstanceId };
    const nextState = warningsDrawingsReducer(
      initialState,
      uiActions.registerDialog(actionPayload),
    );

    const updatedDrawItem = selectDrawByInstanceId(
      { drawings: nextState },
      drawingInstanceId,
    );

    expect(updatedDrawItem).toBeTruthy();
  });

  it('should unregister a drawing entity when drawingTool dialog is unregistered', () => {
    const drawingInstanceId = uiTypes.DialogTypes.DrawingTool;
    const actionPayload = { drawingInstanceId, type: drawingInstanceId };
    const initialDrawItem: DrawItem = {
      drawingInstanceId,
      objectName: '',
      id: '',
    };
    const prevState = drawAdapter.setAll(initialState, [initialDrawItem]);

    const nextState = warningsDrawingsReducer(
      prevState,
      uiActions.unregisterDialog(actionPayload),
    );

    const removedDrawItem = selectDrawByInstanceId(
      { drawings: nextState },
      drawingInstanceId,
    );

    expect(removedDrawItem).toBeUndefined();
  });

  it('should handle setFormDirty', () => {
    const actionPayload = {
      drawingInstanceId: drawingDialogType,
      isFormDirty: true,
    };

    const initialDrawItem: DrawItem = {
      id: '',
      drawingInstanceId: drawingDialogType,
      objectName: '',
    };

    const state = drawAdapter.setAll(initialState, [initialDrawItem]);

    const nextState = warningsDrawingsReducer(
      state,
      warningsDrawingsActions.setFormDirty(actionPayload),
    );
    expect(nextState.entities[drawingDialogType]!.isFormDirty).toBeTruthy();
  });

  it('should handle submitDrawValues correctly', () => {
    const mapId = 'newMapId';

    const actionPayload = {
      mapId,
      geoJSON: testGeoJSON,
      objectName: 'testing',
      id: '',
      scope: 'user',
    } as SubmitDrawValuesPayload;

    const nextState = warningsDrawingsReducer(
      initialState,
      warningsDrawingsActions.submitDrawValues(actionPayload),
    );

    expect(nextState).toBeTruthy();
    expect(nextState).toEqual(initialState);
  });

  it('should reset formDirty when dialog closes', () => {
    const initialDrawItem: DrawItem = {
      id: '',
      drawingInstanceId: drawingDialogType,
      objectName: '',
    };

    const state = drawAdapter.setAll(initialState, [initialDrawItem]);

    const nextState = warningsDrawingsReducer(
      state,
      uiActions.setActiveMapIdForDialog({
        type: drawingDialogType,
        setOpen: false,
        mapId: '',
      }),
    );
    expect(nextState.entities[drawingDialogType]!.isFormDirty).toBeFalsy();
  });
});
