/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { WarningsThemeProvider } from '../Providers/Providers';

import { translateKeyOutsideComponents } from '../../utils/i18n';
import ExpandableFilterChip, {
  ExpandableFilterChipProps,
} from './ExpandableFilterChip';
import { emptyFilterId } from './filter-utils';

describe('src/lib/components/FilterList/ExpandableFilterChip', () => {
  it('should show disabled when filterList is empty', async () => {
    const props: ExpandableFilterChipProps = {
      id: 'incident',
      options: {},
      updateFilter: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <ExpandableFilterChip {...props} />
      </WarningsThemeProvider>,
    );
    await screen.findByText(
      translateKeyOutsideComponents(`filter-${props.id}`),
    );
    expect(
      screen.getByRole('button').getAttribute('aria-disabled'),
    ).toBeTruthy();
  });
  it('should show number of selected filters when not all are selected', async () => {
    const props: ExpandableFilterChipProps = {
      id: 'phenomenon',
      updateFilter: jest.fn(),
      options: {
        fog: true,
        wind: false,
      },
    };
    render(
      <WarningsThemeProvider>
        <ExpandableFilterChip {...props} />
      </WarningsThemeProvider>,
    );
    await screen.findByText(
      translateKeyOutsideComponents(`filter-${props.id}`),
    );
    await screen.findByText(1);
  });
  it('should show number of selected filters when all are selected', async () => {
    const props: ExpandableFilterChipProps = {
      id: 'phenomenon',
      updateFilter: jest.fn(),
      options: {
        fog: true,
        wind: true,
      },
    };
    render(
      <WarningsThemeProvider>
        <ExpandableFilterChip {...props} />
      </WarningsThemeProvider>,
    );
    await screen.findByText(
      translateKeyOutsideComponents(`filter-${props.id}`),
    );
    expect(screen.getByText(2)).toBeTruthy();
  });
  it('should not show number of selected filters when none are selected', async () => {
    const props: ExpandableFilterChipProps = {
      id: 'phenomenon',
      updateFilter: jest.fn(),
      options: {
        fog: false,
        wind: false,
      },
    };
    render(
      <WarningsThemeProvider>
        <ExpandableFilterChip {...props} />
      </WarningsThemeProvider>,
    );
    await screen.findByText(
      translateKeyOutsideComponents(`filter-${props.id}`),
    );
    expect(screen.queryByText(0)).toBeFalsy();
  });
  it('should open and close options list', async () => {
    const props: ExpandableFilterChipProps = {
      id: 'phenomenon',
      updateFilter: jest.fn(),
      options: {
        fog: true,
        wind: false,
      },
    };
    render(
      <WarningsThemeProvider>
        <ExpandableFilterChip {...props} />
      </WarningsThemeProvider>,
    );
    await screen.findByText(
      translateKeyOutsideComponents(`filter-${props.id}`),
    );
    fireEvent.click(await screen.findByText(1));
    expect(screen.getByText('Fog')).toBeTruthy();
    fireEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('close-dialog-confirm'),
      }),
    );
    await waitFor(() => expect(screen.queryByText('Fog')).toBeFalsy());
  });
  it('should handle click on select all', async () => {
    const props: ExpandableFilterChipProps = {
      id: 'phenomenon',
      updateFilter: jest.fn(),
      options: {
        fog: true,
        wind: false,
      },
    };
    render(
      <WarningsThemeProvider>
        <ExpandableFilterChip {...props} />
      </WarningsThemeProvider>,
    );

    fireEvent.click(
      await screen.findByText(
        translateKeyOutsideComponents(`filter-${props.id}`),
      ),
    );
    expect(screen.getByText('Fog')).toBeTruthy();
    expect(props.updateFilter).toHaveBeenCalledTimes(0);
    fireEvent.click(
      screen.getByLabelText(translateKeyOutsideComponents('filter-select-all')),
    );
    expect(props.updateFilter).toHaveBeenCalledTimes(1);
    expect(props.updateFilter).toHaveBeenCalledWith(props.id, 'ALL', true);
  });

  it('should handle click on single option', async () => {
    const props: ExpandableFilterChipProps = {
      id: 'phenomenon',
      updateFilter: jest.fn(),
      options: {
        fog: true,
        wind: false,
      },
    };
    render(
      <WarningsThemeProvider>
        <ExpandableFilterChip {...props} />
      </WarningsThemeProvider>,
    );

    fireEvent.click(
      await screen.findByText(
        translateKeyOutsideComponents(`filter-${props.id}`),
      ),
    );
    expect(screen.getByText('Fog')).toBeTruthy();
    expect(props.updateFilter).toHaveBeenCalledTimes(0);
    fireEvent.click(screen.getByText('Fog'));
    expect(props.updateFilter).toHaveBeenCalledTimes(1);
    expect(props.updateFilter).toHaveBeenCalledWith(
      props.id,
      'fog',
      false,
      undefined,
    );
  });

  it('should show icons for phenomenon', async () => {
    const props: ExpandableFilterChipProps = {
      id: 'phenomenon',
      updateFilter: jest.fn(),
      options: {
        fog: true,
        wind: false,
      },
    };
    render(
      <WarningsThemeProvider>
        <ExpandableFilterChip {...props} />
      </WarningsThemeProvider>,
    );

    fireEvent.click(
      await screen.findByText(
        translateKeyOutsideComponents(`filter-${props.id}`),
      ),
    );

    expect(
      (await screen.findByTestId('wrapper-fog')).firstChild!.nodeName,
    ).toBe('svg');
    expect(
      (await screen.findByTestId('wrapper-wind')).firstChild!.nodeName,
    ).toBe('svg');
  });

  it('should show disabled when filterList only contains the empty option', async () => {
    const props: ExpandableFilterChipProps = {
      id: 'incident',
      options: { [emptyFilterId]: true },
      updateFilter: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <ExpandableFilterChip {...props} />
      </WarningsThemeProvider>,
    );
    await screen.findByText(
      translateKeyOutsideComponents(`filter-${props.id}`),
    );
    expect(
      screen.getByRole('button').getAttribute('aria-disabled'),
    ).toBeTruthy();
  });
});
