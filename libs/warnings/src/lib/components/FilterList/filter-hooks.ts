/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { pick } from 'lodash';
import { useCallback, useEffect, useMemo } from 'react';
import { Warning } from '../../store/publicWarningForm/types';
import {
  AllPhenomenaKeys,
  defaultDomainFilter,
  defaultEmptyFilter,
  defaultLevelFilter,
  defaultPhenomenonFilter,
  Domain,
  domainLevels,
  domainPhenomena,
  emptyFilterId,
  getFiltersFromWarnings,
  getSelectedOptions,
  getValue,
  initialFilters,
  Level,
  updateState,
} from './filter-utils';

export interface PickedFilterState {
  incident: Partial<typeof defaultEmptyFilter>;
  source: Partial<typeof defaultEmptyFilter>;
  domain: Partial<typeof defaultDomainFilter>;
  phenomenon: Partial<typeof defaultPhenomenonFilter>;
  level: Partial<typeof defaultLevelFilter>;
}
export interface FilterState extends PickedFilterState {
  incident: typeof defaultEmptyFilter;
  source: typeof defaultEmptyFilter;
  domain: typeof defaultDomainFilter;
  phenomenon: typeof defaultPhenomenonFilter;
  level: typeof defaultLevelFilter;
}

export type FilterKeyType = keyof FilterState;
export type OptionKey<T extends FilterKeyType> = keyof FilterState[T];

export type UpdateFilter = <T extends FilterKeyType>(
  filterKey: T | 'ALL',
  optionKey: OptionKey<T> | 'ALL',
  value: boolean,
  only?: boolean,
) => void;

export interface FilterHookState {
  allSelected: boolean;
  updateFilter: UpdateFilter;
  byFilterState: (warning: Warning) => boolean;
  filterState: PickedFilterState;
}

const getLevelsForDomainFilter = (
  domainFilter: Partial<Record<Domain, boolean>>,
): Level[] => {
  return (Object.keys(domainFilter) as Domain[]).flatMap((domain) =>
    domainFilter[domain] ? domainLevels[domain] : [],
  );
};

export const getPhenomenaForDomainFilter = (
  domainFilter: Record<Domain, boolean>,
): (keyof typeof AllPhenomenaKeys)[] => {
  return (Object.keys(domainFilter) as Domain[]).flatMap((domain) =>
    domainFilter[domain] ? domainPhenomena[domain] : [],
  ) as (keyof typeof AllPhenomenaKeys)[];
};

/** Get filter object
 * @param savedFilter The savedFilter with all options
 * @param visibleOptions The options that should be shown
 * @returns pickedFilterObject - The filter object with only the relevant options
 * @returns allSelected - A value for whether or not every visible option is selected
 */
const getFilterOptions = <T extends FilterKeyType>(
  savedFilter: FilterState[T],
  visibleOptions: OptionKey<T>[],
): [Partial<FilterState[T]>, boolean] => {
  const pickedFilterObject = pick(savedFilter, [
    emptyFilterId,
    ...visibleOptions,
  ]);

  const { allSelected } = getSelectedOptions(pickedFilterObject);
  return [pickedFilterObject, allSelected];
};

export const useWarningFilters = (
  warnings: Warning[],
  onUpdateFilters: (
    filterState: Partial<FilterState>,
    systemUpdate?: boolean,
  ) => void,
  savedFilterState: FilterState,
  presetFilterState?: PickedFilterState,
): FilterHookState => {
  const {
    domain: savedDomainFilter,
    phenomenon: savedPhenomenonFilter,
    level: savedLevelFilter,
    source: savedSourceFilter,
    incident: savedIncidentFilter,
  } = savedFilterState;

  /** Levels for selected domain(s) */
  const relevantLevels = useMemo(
    () => getLevelsForDomainFilter(savedDomainFilter),
    [savedDomainFilter],
  );

  const [pickedLevelFilter, allLevelsSelected] = getFilterOptions<'level'>(
    savedLevelFilter,
    relevantLevels,
  );

  /** Phenomena for selected domain(s) */
  const domainPhenomena = useMemo(
    () =>
      getPhenomenaForDomainFilter(savedDomainFilter as Record<Domain, boolean>),
    [savedDomainFilter],
  );

  /** Phenomena in the warnings */
  const warningsPhenomena = useMemo(
    () => getFiltersFromWarnings(warnings, 'phenomenon'),
    [warnings],
  );

  const relevantPhenomena = useMemo(
    () =>
      domainPhenomena.filter((phenomenon) =>
        warningsPhenomena.includes(phenomenon),
      ),
    [warningsPhenomena, domainPhenomena],
  );

  const [pickedPhenomenaFilter, allPhenomenaSelected] =
    getFilterOptions<'phenomenon'>(savedPhenomenonFilter, relevantPhenomena);

  // source
  const relevantSources = useMemo(
    () => getFiltersFromWarnings(warnings, 'warningProposalSource'),
    [warnings],
  );
  const defaultSourceFilter = useMemo(
    () =>
      Object.fromEntries(
        relevantSources.map((key) => {
          const presetValue = presetFilterState?.source[key];
          return [key, presetValue === undefined ? true : presetValue];
        }),
      ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [relevantSources],
  );
  const { allSelected: allSourcesSelected } =
    getSelectedOptions(savedSourceFilter);

  /** Incidents in the warnings */
  const relevantIncidents = useMemo(
    () => getFiltersFromWarnings(warnings, 'incident'),
    [warnings],
  );
  /** FilterObject based on relevant incidents with values from the preset */
  const defaultIncidentFilter = useMemo(
    () =>
      Object.fromEntries(
        relevantIncidents.map((key) => {
          const presetValue = presetFilterState?.incident[key];
          return [key, presetValue === undefined ? true : presetValue];
        }),
      ),
    [relevantIncidents, presetFilterState?.incident],
  );
  const { allSelected: allIncidentsSelected } =
    getSelectedOptions(savedIncidentFilter);

  const populatedIncidentAndSourceFilter: Partial<FilterState> = useMemo(
    () => ({
      incident: defaultIncidentFilter,
      source: defaultSourceFilter,
    }),
    [defaultIncidentFilter, defaultSourceFilter],
  );
  const initialFiltersWithPresets = useMemo(
    () =>
      ({
        incident: defaultIncidentFilter || defaultEmptyFilter,
        source: defaultSourceFilter || defaultEmptyFilter,
        domain: {
          ...initialFilters.domain,
          ...presetFilterState?.domain,
        },
        phenomenon: {
          ...initialFilters.phenomenon,
          ...presetFilterState?.phenomenon,
        } as Record<string, boolean>,
        level: {
          ...initialFilters.level,
          ...presetFilterState?.level,
        },
      }) as FilterState,
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [presetFilterState],
  );
  useEffect(() => {
    onUpdateFilters(populatedIncidentAndSourceFilter, true);
  }, [populatedIncidentAndSourceFilter, onUpdateFilters]);

  useEffect(() => {
    // when the preset changes, update the filters
    onUpdateFilters(initialFiltersWithPresets, true);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [initialFiltersWithPresets, onUpdateFilters]);

  const { allSelected: allDomainSelected } =
    getSelectedOptions(savedDomainFilter);

  const allSelected =
    allDomainSelected &&
    allLevelsSelected &&
    allPhenomenaSelected &&
    allIncidentsSelected &&
    allSourcesSelected;

  /**
   * @param filterKey - Name of the filter | "ALL" filters
   * @param optionKey - Name of the option | "ALL" options
   * @param value - The new value
   * @param only - Flag that decides if all other options should be disabled
   */
  const updateFilter: UpdateFilter = (filterKey, optionKey, value, only) => {
    const levelFilter = value ? defaultLevelFilter : defaultEmptyFilter;
    if (filterKey === 'ALL') {
      onUpdateFilters({
        domain: updateState(savedDomainFilter, 'ALL', value),
        level: updateState(levelFilter, 'ALL' as OptionKey<'level'>, value),
        phenomenon: updateState(
          savedPhenomenonFilter,
          'ALL' as OptionKey<'phenomenon'>,
          value,
        ),
        incident: updateState(savedIncidentFilter, 'ALL', value),
        source: updateState(savedSourceFilter, 'ALL', value),
      });
    } else if (filterKey === 'domain') {
      const newDomain = updateState(
        savedDomainFilter,
        optionKey as OptionKey<'domain'>,
        value,
        only,
      );
      onUpdateFilters({ domain: newDomain });
    } else if (filterKey === 'level') {
      onUpdateFilters({
        level: updateState(
          savedLevelFilter,
          optionKey as OptionKey<'level'>,
          value,
          only,
        ),
      });
    } else if (filterKey === 'phenomenon') {
      onUpdateFilters({
        phenomenon: updateState(
          savedPhenomenonFilter,
          optionKey as OptionKey<'phenomenon'>,
          value,
          only,
        ),
      });
    } else if (filterKey === 'source') {
      onUpdateFilters({
        source: updateState(
          savedSourceFilter,
          optionKey as OptionKey<'source'>,
          value,
          only,
        ),
      });
    } else if (filterKey === 'incident') {
      onUpdateFilters({
        incident: updateState(
          savedIncidentFilter,
          optionKey as OptionKey<'incident'>,
          value,
          only,
        ),
      });
    }
  };

  const byFilterState = useCallback(
    (warning: Warning): boolean => {
      const levelDomain = ['SIG', 'AIR'].find(
        (dom) => dom === warning.warningDetail.level,
      )
        ? Domain.aviation
        : Domain.public;
      if (!savedDomainFilter[levelDomain]) {
        return false;
      }
      const level = getValue(Level, warning.warningDetail.level);
      if (!pickedLevelFilter[level]) {
        return false;
      }
      const phenomenon = getValue(
        AllPhenomenaKeys,
        warning.warningDetail.phenomenon,
      );
      if (
        !pickedPhenomenaFilter[phenomenon as keyof typeof pickedPhenomenaFilter]
      ) {
        return false;
      }
      const source =
        relevantSources.find(
          (source) => source === warning.warningDetail.warningProposalSource,
        ) || emptyFilterId;
      if (!savedSourceFilter[source]) {
        return false;
      }
      const incident =
        relevantIncidents.find(
          (incident) => incident === warning.warningDetail.incident,
        ) || emptyFilterId;
      if (!savedIncidentFilter[incident]) {
        return false;
      }
      return true;
    },
    [
      savedDomainFilter,
      pickedLevelFilter,
      pickedPhenomenaFilter,
      relevantSources,
      savedSourceFilter,
      relevantIncidents,
      savedIncidentFilter,
    ],
  );

  return {
    allSelected,
    updateFilter,
    byFilterState,
    filterState: {
      domain: savedDomainFilter,
      level: pickedLevelFilter,
      phenomenon: pickedPhenomenaFilter,
      incident: savedIncidentFilter,
      source: savedSourceFilter,
    },
  };
};
