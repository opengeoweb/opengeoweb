/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { emptyFilterId, getFiltersFromWarnings } from './filter-utils';
import warningListJSON from '../../utils/fakeApi/warningList.json';
import { Warning } from '../../store/publicWarningForm/types';

const warningList = warningListJSON as unknown as Warning[];

describe('filter-utils', () => {
  describe('getFiltersFromWarnings', () => {
    it('should return default list when no warnings', () => {
      const result = getFiltersFromWarnings([], 'warningProposalSource');
      expect(result.length).toEqual(1);
      expect(result).toEqual([emptyFilterId]);

      const result2 = getFiltersFromWarnings([], '');
      expect(result2.length).toEqual(1);
      expect(result2).toEqual([emptyFilterId]);
    });
    it('should fill the list of filter options based on the warnings content', () => {
      const list = [warningList[0], warningList[8]];

      const result = getFiltersFromWarnings(list, 'warningProposalSource');
      expect(result.length).toEqual(2);
      expect(result).toEqual([emptyFilterId, 'PASCAL-ECMWF-EPS']);

      const result2 = getFiltersFromWarnings(list, 'incident');
      expect(result2.length).toEqual(1);
      expect(result2).toEqual([emptyFilterId]);
    });
  });
});
