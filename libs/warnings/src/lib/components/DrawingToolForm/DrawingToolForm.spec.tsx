/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { defaultDelay } from '@opengeoweb/shared';
import {
  currentlySupportedDrawModes,
  defaultDelete,
  defaultPolygon,
  rewindGeometry,
} from '@opengeoweb/webmap-react';

import { DrawingToolForm, getObjectName } from './DrawingToolForm';
import { WarningsThemeProvider } from '../Providers/Providers';
import { translateKeyOutsideComponents } from '../../utils/i18n';

describe('src/components/MapDraw/DrawingTool/DrawingTool', () => {
  const testGeoJSON: GeoJSON.FeatureCollection = {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [5.0, 55.0],
              [4.331914, 55.332644],
              [3.368817, 55.764314],
              [2.761908, 54.379261],
              [3.15576, 52.913554],
              [2.000002, 51.500002],

              [7.14218, 52.898244],
              [7.191667, 53.3],
              [6.5, 53.666667],
              [6.500002, 55.000002],
              [5.0, 55.0],
            ],
          ],
        },
        properties: {
          fill: '#FF00FF',
        },
      },
    ],
  };

  const props = {
    onSubmitForm: jest.fn(),
    drawModes: currentlySupportedDrawModes,
    onChangeDrawMode: jest.fn(),
    activeDrawModeId: '',
    onDeactivateTool: jest.fn(),
    isInEditMode: false,
    geoJSONProperties: {},
    onChangeProperties: jest.fn(),
    onChangeName: jest.fn(),
    geoJSON: undefined,
  };

  describe('getObjectName', () => {
    it('returns the provided objectName if it is not empty', () => {
      const objectName = 'Test Object';
      const result = getObjectName(objectName);
      expect(result).toBe(objectName);
    });

    it('generates a default objectName when objectName is empty', () => {
      jest.useFakeTimers().setSystemTime(new Date(1692782720720));
      const result = getObjectName('');

      expect(result).toBe('23/08/2023 09:25 UTC');
      jest.useRealTimers();
    });
  });

  it('should render required component', async () => {
    render(
      <WarningsThemeProvider>
        <DrawingToolForm {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByTestId('drawingToolForm')).toBeTruthy();
    expect(
      screen.getByText(translateKeyOutsideComponents('drawing-tool-form-save')),
    ).toBeTruthy();
  });

  it('should render correct save button title for existing object', async () => {
    const testProps = {
      ...props,
      onSubmitForm: jest.fn(),
      id: 'test123',
    };
    render(
      <WarningsThemeProvider>
        <DrawingToolForm {...testProps} />
      </WarningsThemeProvider>,
    );
    expect(screen.getByTestId('drawingToolForm')).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('drawing-tool-form-update'),
      ),
    ).toBeTruthy();
  });

  it('should call onSubmitForm with default values when pressing save', async () => {
    const newProps = {
      ...props,
      id: 'test-id-1',
      geoJSON: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [6.673542618750517, 51.77828698943017],
                  [4.506215269489237, 53.442721496909385],
                  [5.711880814773656, 55.12656218004421],
                ],
              ],
            },
          },
        ],
      } as GeoJSON.FeatureCollection,
      objectName: 'testing',
      onSubmitForm: jest.fn(),
    };

    render(
      <WarningsThemeProvider>
        <DrawingToolForm {...newProps} />
      </WarningsThemeProvider>,
    );

    fireEvent.click(screen.getByTestId('saveDrawingFormButton'));
    await waitFor(() => {
      expect(newProps.onSubmitForm).toHaveBeenCalledWith({
        id: newProps.id,
        geoJSON: newProps.geoJSON,
        objectName: newProps.objectName,
      });
    });
  });

  it('should call onSubmitForm with default values when pressing save and rewind geometry if clockwise', async () => {
    const newProps = {
      ...props,
      id: 'test-id-1',
      geoJSON: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              stroke: '#f24a00',
              'stroke-width': 1.5,
              'stroke-opacity': 1,
              fill: '#f24a00',
              'fill-opacity': 0.25,
              selectionType: 'poly',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [3.9727570104176655, 52.798401500118025],
                  [3.345035685215513, 53.65515457833232],
                  [3.9727570104176655, 54.741996695611284],
                  [5.906138692040295, 54.33411924173588],
                  [5.680159014967519, 53.13111016712073],
                  [3.9727570104176655, 52.798401500118025],
                ],
              ],
            },
          },
        ],
      } as GeoJSON.FeatureCollection,
      objectName: 'testing',
      onSubmitForm: jest.fn(),
    };

    render(
      <WarningsThemeProvider>
        <DrawingToolForm {...newProps} />
      </WarningsThemeProvider>,
    );

    fireEvent.click(screen.getByTestId('saveDrawingFormButton'));
    await waitFor(() => {
      expect(newProps.onSubmitForm).toHaveBeenCalledWith({
        id: newProps.id,
        geoJSON: rewindGeometry(newProps.geoJSON),
        objectName: newProps.objectName,
      });
    });
  });

  it('should call change opacity', async () => {
    const objectName = getObjectName('');

    const testProps = {
      ...props,
      geoJSON: testGeoJSON,
    };

    render(
      <WarningsThemeProvider>
        <DrawingToolForm {...testProps} />
      </WarningsThemeProvider>,
    );

    fireEvent.mouseDown(screen.getByRole('combobox', { name: 'Opacity' }));
    const menuItem = await screen.findByText('50 %');
    fireEvent.click(menuItem);
    await waitFor(() => {
      expect(props.onChangeProperties).toHaveBeenCalledWith({
        'fill-opacity': 0.5,
        'stroke-opacity': 1,
      });
    });

    fireEvent.click(screen.getByTestId('saveDrawingFormButton'));
    await waitFor(() => {
      expect(props.onSubmitForm).toHaveBeenCalledWith({
        id: '',
        objectName,
        geoJSON: testGeoJSON,
      });
    });
  });

  it('should disable all inputs when loading', () => {
    const testProps = {
      ...props,
      geoJSON: testGeoJSON,
    };

    render(
      <WarningsThemeProvider>
        <DrawingToolForm {...testProps} isLoading />
      </WarningsThemeProvider>,
    );

    const buttons = screen.getAllByRole('button');
    buttons.forEach((button) => {
      expect(button.getAttribute('disabled')).toBeDefined();
    });

    const objectInputName = screen.getByRole('textbox', {
      name: translateKeyOutsideComponents('drawing-tool-form-object-name'),
    });
    expect(objectInputName.getAttribute('disabled')).toBeDefined();
  });

  it('should disable submit when errors in form', () => {
    render(
      <WarningsThemeProvider>
        <DrawingToolForm {...props} />
      </WarningsThemeProvider>,
    );

    const submitButton = screen.getByRole('button', {
      name: translateKeyOutsideComponents('drawing-tool-form-save'),
    });
    const objectName = screen.getByRole('textbox', {
      name: translateKeyOutsideComponents('drawing-tool-form-object-name'),
    });

    expect(submitButton.getAttribute('disabled')).toBeNull();

    fireEvent.change(objectName, { target: { value: '' } });

    expect(submitButton.getAttribute('disabled')).toBeDefined();

    fireEvent.change(objectName, { target: { value: 'my new object name' } });

    expect(submitButton.getAttribute('disabled')).toBeNull();
  });

  it('should disable submit button when no object drawn', async () => {
    render(
      <WarningsThemeProvider>
        <DrawingToolForm {...props} />
      </WarningsThemeProvider>,
    );

    const submitButton = screen.getByRole('button', {
      name: translateKeyOutsideComponents('drawing-tool-form-save'),
    });

    expect(submitButton.getAttribute('disabled')).toBeDefined();
  });

  it('should call onFormDirty and reset after successfull request', async () => {
    const testProps = {
      ...props,
      geoJSON: testGeoJSON,
      onFormDirty: jest.fn(),
    };

    const { rerender } = render(
      <WarningsThemeProvider>
        <DrawingToolForm {...testProps} />
      </WarningsThemeProvider>,
    );

    fireEvent.mouseDown(screen.getByRole('combobox', { name: 'Opacity' }));
    const menuItem = await screen.findByText('50 %');
    fireEvent.click(menuItem);
    await waitFor(() => {
      expect(props.onChangeProperties).toHaveBeenCalledWith({
        'fill-opacity': 0.5,
        'stroke-opacity': 1,
      });
    });

    expect(testProps.onFormDirty).toHaveBeenCalledWith(true);
    expect(testProps.onFormDirty).toHaveBeenCalledTimes(1);

    // trigger request by setting isLoading
    rerender(
      <WarningsThemeProvider>
        <DrawingToolForm {...{ ...testProps, isLoading: true }} />
      </WarningsThemeProvider>,
    );
    // trigger success isLoading false
    rerender(
      <WarningsThemeProvider>
        <DrawingToolForm {...{ ...testProps, isLoading: false }} />
      </WarningsThemeProvider>,
    );

    await waitFor(() =>
      expect(testProps.onFormDirty).toHaveBeenCalledWith(false),
    );
    expect(testProps.onFormDirty).toHaveBeenCalledTimes(2);
  });

  it('should call onFormDirty and not reset after unsuccessfull request', async () => {
    const testProps = {
      ...props,
      geoJSON: testGeoJSON,
      onFormDirty: jest.fn(),
    };

    const { rerender } = render(
      <WarningsThemeProvider>
        <DrawingToolForm {...testProps} />
      </WarningsThemeProvider>,
    );

    fireEvent.mouseDown(screen.getByRole('combobox', { name: 'Opacity' }));
    const menuItem = await screen.findByText('50 %');
    fireEvent.click(menuItem);
    await waitFor(() => {
      expect(props.onChangeProperties).toHaveBeenCalledWith({
        'fill-opacity': 0.5,
        'stroke-opacity': 1,
      });
    });

    expect(testProps.onFormDirty).toHaveBeenCalledWith(true);
    expect(testProps.onFormDirty).toHaveBeenCalledTimes(1);

    // trigger request by setting isLoading
    rerender(
      <WarningsThemeProvider>
        <DrawingToolForm {...{ ...testProps, isLoading: true }} />
      </WarningsThemeProvider>,
    );
    // trigger done isLoading and error
    const dialogError = 'something went wrong';
    rerender(
      <WarningsThemeProvider>
        <DrawingToolForm {...{ ...testProps, isLoading: false, dialogError }} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(dialogError)).toBeTruthy();

    await waitFor(() => expect(testProps.onFormDirty).toHaveBeenCalledTimes(1));
  });

  describe('tools', () => {
    describe('polygon', () => {
      it('should render polygon tool', async () => {
        jest.useFakeTimers();

        render(
          <WarningsThemeProvider>
            <DrawingToolForm {...props} />
          </WarningsThemeProvider>,
        );
        await waitFor(() => {
          const toolButton = screen.getByTestId(defaultPolygon.drawModeId);
          expect(toolButton).toBeTruthy();
        });
      });

      it('should have correct tooltip', async () => {
        jest.useFakeTimers();

        render(
          <WarningsThemeProvider>
            <DrawingToolForm {...props} />
          </WarningsThemeProvider>,
        );

        const toolButton = screen.getByTestId(defaultPolygon.drawModeId);

        // test tooltip
        const user = userEvent.setup({
          advanceTimers: jest.advanceTimersByTime,
        });
        await user.hover(toolButton!);

        act(() => {
          jest.advanceTimersByTime(defaultDelay);
        });

        const tooltip = screen.getByRole('tooltip');

        expect(tooltip!.textContent).toContain(defaultPolygon.title);
      });

      it('should be able to activate polygon tool', async () => {
        jest.useFakeTimers();

        render(
          <WarningsThemeProvider>
            <DrawingToolForm {...props} />
          </WarningsThemeProvider>,
        );
        const toolButton = screen.getByTestId(defaultPolygon.drawModeId);
        fireEvent.click(toolButton);
        await waitFor(() => {
          expect(props.onChangeDrawMode).toHaveBeenCalledWith(defaultPolygon);
        });
      });

      it('should show esc message when a tool is active and disable it on esc key', () => {
        const testProps = {
          ...props,
          isInEditMode: true,
          activeDrawModeId: defaultPolygon.drawModeId,
        };
        render(
          <WarningsThemeProvider>
            <DrawingToolForm {...testProps} />
          </WarningsThemeProvider>,
        );

        const toolButton = screen.getByTestId(defaultPolygon.drawModeId);
        expect(toolButton.classList.contains('Mui-selected')).toBeTruthy();
        expect(
          screen.getByText(
            translateKeyOutsideComponents('drawing-tool-form-exit-draw-mode'),
          ),
        ).toBeTruthy();

        fireEvent.keyDown(toolButton, { key: 'Escape' });
      });
    });

    describe('delete', () => {
      it('should render delete tool', async () => {
        jest.useFakeTimers();

        render(
          <WarningsThemeProvider>
            <DrawingToolForm {...props} />
          </WarningsThemeProvider>,
        );

        const toolButton = screen.getByTestId(defaultDelete.drawModeId);
        expect(toolButton).toBeTruthy();
      });

      it('should have correct tooltip', async () => {
        jest.useFakeTimers();

        render(
          <WarningsThemeProvider>
            <DrawingToolForm {...props} />
          </WarningsThemeProvider>,
        );

        const toolButton = screen.getByTestId(defaultDelete.drawModeId);

        // test tooltip
        const user = userEvent.setup({
          advanceTimers: jest.advanceTimersByTime,
        });
        await user.hover(toolButton!);

        act(() => {
          jest.advanceTimersByTime(defaultDelay);
        });

        const tooltip = screen.getByRole('tooltip');
        expect(tooltip!.textContent).toContain(defaultDelete.title);
      });

      it('should be able to click delete tool but not activate it', async () => {
        jest.useFakeTimers();

        render(
          <WarningsThemeProvider>
            <DrawingToolForm {...props} />
          </WarningsThemeProvider>,
        );

        const toolButton = screen.getByTestId(defaultDelete.drawModeId);
        fireEvent.click(toolButton);
        await waitFor(() => {
          expect(toolButton.classList.contains('Mui-selected')).toBeFalsy();
        });
      });
    });
  });
});
