/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mapActions, uiTypes } from '@opengeoweb/store';
import { defaultPolygon } from '@opengeoweb/webmap-react';
import { ObjectManagerConnect } from '../ObjectManager/ObjectManagerConnect';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WarningsApi } from '../../utils/api';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { drawingList } from '../../utils/fakeApi/index';
import { DrawingListItem } from '../../store/warningsDrawings/types';
import { ObjectManagerMapButtonConnect } from '../ObjectManager/ObjectManagerMapButtonConnect';
import { createMockStore } from '../../store';
import DrawingToolConnect from '../DrawingTool/DrawingToolConnect';
import DrawingToolMapButtonConnect from '../DrawingTool/DrawingToolMapButtonConnect';
import { translateKeyOutsideComponents } from '../../utils/i18n';

const createApi = (): WarningsApi => {
  return {
    ...createFakeApi(),
    getDrawings: (): Promise<{ data: DrawingListItem[] }> => {
      return new Promise((resolve) => {
        resolve({ data: drawingList });
      });
    },
  };
};

describe('src/components/DrawingToolForm/DrawingToolFormConnect', () => {
  it('should ask for confirmation if there are changes in the form and closing the draw dialog', async () => {
    const mapId = 'map129';
    const store = createMockStore();
    const user = userEvent.setup();
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <WarningsThemeStoreProvider store={store} createApi={createApi}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
        <DrawingToolConnect showMapIdInTitle />
        <DrawingToolMapButtonConnect mapId={mapId} />
      </WarningsThemeStoreProvider>,
    );

    // open drawing tool
    await user.click(
      screen.getByRole('button', {
        name: `Drawing Toolbox`,
      }),
    );
    const drawtoolboxDialogTitle = `Drawing Toolbox ${mapId}`;
    await screen.findByText(drawtoolboxDialogTitle);
    expect(screen.queryByText(`Object Manager for ${mapId}`)).toBeFalsy();

    // check that drawing tool is active
    const drawingButton = screen.getByRole('button', {
      name: 'Drawing Toolbox',
    });
    expect(drawingButton.classList).toContain('Mui-selected');

    await user.type(
      screen.getByRole('textbox', {
        name: `Object name`,
      }),
      'some new value',
    );

    await user.click(
      screen.getByRole('button', {
        name: /Close/i,
      }),
    );

    // should show confirm dialog
    await screen.findByText(
      translateKeyOutsideComponents('close-draw-dialog-title'),
    );
    await screen.findByText(
      translateKeyOutsideComponents('close-dialog-description'),
    );

    // press cancel
    await user.click(
      screen.getByRole('button', {
        name: /Cancel/i,
      }),
    );

    await waitFor(() =>
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('close-draw-dialog-title'),
        ),
      ).toBeFalsy(),
    );
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('close-dialog-description'),
      ),
    ).toBeFalsy();

    // open object manager
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );

    // should show confirm dialog
    await screen.findByText(
      translateKeyOutsideComponents('close-draw-dialog-title'),
    );
    await screen.findByText(
      translateKeyOutsideComponents('close-dialog-description'),
    );

    // press close
    await user.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() =>
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('close-draw-dialog-title'),
        ),
      ).toBeFalsy(),
    );
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('close-dialog-description'),
      ),
    ).toBeFalsy();
    expect(screen.queryByText(drawtoolboxDialogTitle)).toBeFalsy();
  });

  it('should open default draw mode as polygon', async () => {
    const mapId = 'map129';
    const store = createMockStore();
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <WarningsThemeStoreProvider store={store} createApi={createApi}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
        <DrawingToolConnect showMapIdInTitle />
        <DrawingToolMapButtonConnect mapId={mapId} />
      </WarningsThemeStoreProvider>,
    );

    // open drawing tool
    await userEvent.click(
      screen.getByRole('button', {
        name: `Drawing Toolbox`,
      }),
    );
    const drawtoolboxDialogTitle = `Drawing Toolbox ${mapId}`;
    expect(await screen.findByText(drawtoolboxDialogTitle)).toBeTruthy();

    // check that drawing tool is active
    const drawingButton = screen.getByRole('button', {
      name: 'Drawing Toolbox',
    });
    expect(drawingButton.classList).toContain('Mui-selected');

    // check polygon is active
    const polygonToolButton = screen.getByRole('button', {
      name: translateKeyOutsideComponents(defaultPolygon.title),
    });
    expect(polygonToolButton.classList).toContain('Mui-selected');
    expect(
      store.getState().drawingtools.entities[uiTypes.DialogTypes.DrawingTool]!
        .activeDrawModeId,
    ).toEqual(defaultPolygon.drawModeId);
  });
});
