/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  HIDDEN_INPUT_HELPER_IS_DRAFT,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';
import {
  fireEvent,
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import React from 'react';
import {
  ProbabilityField,
  defaultProbability,
  probablityOptions,
} from './ProbabilityField';
import { WarningsThemeProvider } from '../../Providers/Providers';
import { translateKeyOutsideComponents } from '../../../utils/i18n';

const clickOption = async (box: HTMLElement, value: string): Promise<void> => {
  fireEvent.mouseDown(box);
  const option = await screen.findByRole('option', { name: value });
  fireEvent.click(option);
};

describe('components/PublicWarningsForm/PublicWarningsForm/ProbabilityField', () => {
  it('should render with default props', async () => {
    render(
      <WarningsThemeProvider>
        <ReactHookFormProvider>
          <ProbabilityField />
        </ReactHookFormProvider>
      </WarningsThemeProvider>,
    );

    expect(
      screen.getByText(translateKeyOutsideComponents('warning-probability')),
    ).toBeTruthy();

    const box = await screen.findByRole('combobox', {
      name: /Probability/,
    });

    expect(box).toHaveTextContent(`${defaultProbability} %`);

    fireEvent.mouseDown(box);

    const optionElements = await screen.findAllByRole('option');

    // check whether all expected options are rendered
    probablityOptions.forEach((option, index) => {
      expect(optionElements[index]).toHaveTextContent(`${option} %`);
    });
  });

  it('should show an error when the level is moderate and probability is lower than 30', async () => {
    render(
      <WarningsThemeProvider>
        <ReactHookFormProvider
          options={{
            mode: 'onChange',
            defaultValues: {
              level: 'moderate',
              [HIDDEN_INPUT_HELPER_IS_DRAFT]: false,
            },
          }}
        >
          <ProbabilityField />
        </ReactHookFormProvider>
      </WarningsThemeProvider>,
    );

    const box = await screen.findByRole('combobox', {
      name: /Probability/,
    });

    await clickOption(box, '20 %');

    await screen.findByText(
      translateKeyOutsideComponents(
        'warning-probability-moderate-level-invalid',
      ),
    );

    await clickOption(box, '30 %');

    await waitForElementToBeRemoved(() => {
      return screen.queryByText(
        translateKeyOutsideComponents(
          'warning-probability-moderate-level-invalid',
        ),
      );
    });
  });
});
