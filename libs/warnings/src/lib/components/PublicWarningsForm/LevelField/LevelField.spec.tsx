/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import { render, screen } from '@testing-library/react';
import React from 'react';
import { LevelField, levelOptions, levelOptionsDict } from './LevelField';
import { WarningsThemeProvider } from '../../Providers/Providers';
import { translateKeyOutsideComponents } from '../../../utils/i18n';

describe('components/PublicWarningsForm/PublicWarningsForm/LevelField', () => {
  it('should render with default props', () => {
    render(
      <WarningsThemeProvider>
        <ReactHookFormProvider>
          <LevelField />
        </ReactHookFormProvider>
      </WarningsThemeProvider>,
    );

    expect(
      screen.getByText(translateKeyOutsideComponents('warning-level')),
    ).toBeTruthy();
    const button = screen.getByRole('combobox', {
      name: /Level/i,
      hidden: true,
    });
    expect(button).toBeTruthy();
    expect(button.getAttribute('disabled')).toBeNull();
  });

  it('should render as disabled', () => {
    render(
      <WarningsThemeProvider>
        <ReactHookFormProvider>
          <LevelField isDisabled />
        </ReactHookFormProvider>
      </WarningsThemeProvider>,
    );

    const button = screen.getByRole('combobox', {
      name: /Level/i,
      hidden: true,
    });
    expect(button).toBeTruthy();
    expect(button.getAttribute('disabled')).toBeDefined();
  });

  it('should render as readonly', () => {
    render(
      <WarningsThemeProvider>
        <ReactHookFormProvider>
          <LevelField isReadOnly />
        </ReactHookFormProvider>
      </WarningsThemeProvider>,
    );

    const button = screen.getByRole('combobox', {
      name: /Level/i,
      hidden: true,
    });
    expect(button).toBeTruthy();
    expect(button.getAttribute('readonly')).toBeDefined();
  });

  it('should make a dict of leveloptions list', () => {
    const dict = levelOptionsDict;
    const dictKeys = Object.keys(dict);
    dictKeys.forEach((key) => {
      expect(levelOptions.includes(dict[key])).toBeTruthy();
    });
    expect(dictKeys).toHaveLength(levelOptions.length);
  });
});
