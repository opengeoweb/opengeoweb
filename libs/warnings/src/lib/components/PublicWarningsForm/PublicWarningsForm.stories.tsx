/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Paper } from '@mui/material';
import * as React from 'react';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import type { Meta, StoryObj } from '@storybook/react';
import { FormProps, PublicWarningsForm } from './PublicWarningsForm';
import { WarningsThemeProvider } from '../Providers/Providers';
import { DrawingListItem } from '../../store/warningsDrawings/types';
import { PublicWarningDetail } from '../../store/publicWarningForm/types';
import { fakePostError, testGeoJSON } from '../../storybookUtils/testUtils';
import {
  fakeAirmetInWarningList,
  fakeSigmetInWarningList,
} from '../../utils/fakeSigmetAirmet';
import {
  transformAirmetToProductFormat,
  transformSigmetToProductFormat,
} from '../../utils/sig-airmet-util';

const object: DrawingListItem = {
  id: '923723984872338768743',
  objectName: 'Drawing object 101',
  lastUpdatedTime: '2022-06-01T12:34:27Z',
  scope: 'user',
  geoJSON: testGeoJSON,
};
const publicWarningDetails: PublicWarningDetail = {
  id: '923723984872338768743',
  phenomenon: 'coastalEvent',
  validFrom: '2022-06-01T15:00:00Z',
  validUntil: '2022-06-01T18:00:00Z',
  level: 'extreme',
  probability: 80,
  descriptionOriginal: 'Some pretty intense coastal weather is coming our way',
  descriptionTranslation: 'And this would be the translation',
  areas: [{ geoJSON: testGeoJSON, objectName: 'test name' }],
};

const baseProps = {
  onPublishForm: async (formValues: PublicWarningDetail): Promise<null> => {
    // eslint-disable-next-line no-console
    console.log('publish form with values', formValues);
    return null;
  },
  onSaveForm: async (formValues: PublicWarningDetail): Promise<null> => {
    // eslint-disable-next-line no-console
    console.log('save form with values', formValues);
    return null;
  },
};

const props: FormProps = {
  object,
  publicWarningDetails,
  ...baseProps,
};

interface DemoProps {
  formProps: FormProps;
  darkMode: boolean;
  isReadOnly: boolean;
  error?: string;
}
export const PublicWarningsFormDemo = ({
  formProps = props,
  darkMode = false,
  isReadOnly = false,
  error,
}: DemoProps): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkMode ? darkTheme : lightTheme}>
      <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
        <PublicWarningsForm
          {...formProps}
          isReadOnly={isReadOnly}
          error={error ? { message: error } : undefined}
        />
      </Paper>
    </WarningsThemeProvider>
  );
};

type Story = StoryObj<typeof PublicWarningsFormDemo>;

const meta: Meta<typeof PublicWarningsFormDemo> = {
  title: 'components/PublicWarningsForm',
  component: PublicWarningsFormDemo,
};
export default meta;

export const LightMode: Story = {
  name: 'PublicWarningsForm light',
};

export const DarkMode: Story = {
  name: 'PublicWarningsForm dark',
  args: { darkMode: true },
};

export const EmptyForm: Story = {
  name: 'PublicWarningsForm empty light',
  args: { formProps: baseProps },
};
export const EmptyFormDark: Story = {
  name: 'PublicWarningsForm empty dark',
  args: { formProps: baseProps, darkMode: true },
};

export const ErrorForm: Story = {
  name: 'PublicWarningsForm error',
  args: { formProps: baseProps, error: fakePostError },
};

export const ErrorFormDark: Story = {
  name: 'PublicWarningsForm error dark',
  args: { formProps: baseProps, darkMode: true, error: fakePostError },
};

export const ReadOnlyForm: Story = {
  name: 'PublicWarningsForm readOnly',
  args: { isReadOnly: true },
};

export const ReadOnlyFormDark: Story = {
  name: 'PublicWarningsForm readOnly dark',
  args: { darkMode: true, isReadOnly: true },
};

const draftAirmetProps: FormProps = {
  ...baseProps,
  selectedAviationProduct: transformAirmetToProductFormat(
    fakeAirmetInWarningList[0],
  ),
  mode: 'edit',
  warningType: 'airmet',
};

export const PublicWarningsFormAirmetDraftLight = (): React.ReactElement => {
  return (
    <WarningsThemeProvider>
      <Paper sx={{ height: 'calc(100vh)' }}>
        <PublicWarningsForm {...draftAirmetProps} />
      </Paper>
    </WarningsThemeProvider>
  );
};

export const PublicWarningsFormAirmetDraftDark = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Paper sx={{ height: 'calc(100vh)' }}>
        <PublicWarningsForm {...draftAirmetProps} />
      </Paper>
    </WarningsThemeProvider>
  );
};

const cancelsAirmetProps: FormProps = {
  ...baseProps,
  selectedAviationProduct: transformAirmetToProductFormat(
    fakeAirmetInWarningList[1],
  ),
  isReadOnly: true,
  mode: 'view',
  warningType: 'airmet',
};

export const PublicWarningsFormAirmetCancels = (): React.ReactElement => {
  return (
    <WarningsThemeProvider>
      <Paper sx={{ height: 'calc(100vh)' }}>
        <PublicWarningsForm {...cancelsAirmetProps} />
      </Paper>
    </WarningsThemeProvider>
  );
};

const cancelledAirmetProps: FormProps = {
  ...baseProps,
  selectedAviationProduct: transformAirmetToProductFormat(
    fakeAirmetInWarningList[2],
  ),
  isReadOnly: true,
  mode: 'view',
  warningType: 'airmet',
};

export const PublicWarningsFormAirmetCancelled = (): React.ReactElement => {
  return (
    <WarningsThemeProvider>
      <Paper sx={{ height: 'calc(100vh)' }}>
        <PublicWarningsForm {...cancelledAirmetProps} />
      </Paper>
    </WarningsThemeProvider>
  );
};

const cancelsSigmetProps: FormProps = {
  ...baseProps,
  selectedAviationProduct: transformSigmetToProductFormat(
    fakeSigmetInWarningList[1],
  ),
  isReadOnly: true,
  mode: 'view',
  warningType: 'sigmet',
};

export const PublicWarningsFormSigmetCancels = (): React.ReactElement => {
  return (
    <WarningsThemeProvider>
      <Paper sx={{ height: 'calc(100vh)' }}>
        <PublicWarningsForm {...cancelsSigmetProps} />
      </Paper>
    </WarningsThemeProvider>
  );
};

const cancelledSigmetProps: FormProps = {
  ...baseProps,
  selectedAviationProduct: transformSigmetToProductFormat(
    fakeSigmetInWarningList[2],
  ),
  isReadOnly: true,
  mode: 'view',
  warningType: 'sigmet',
};

export const PublicWarningsFormSigmetCancelled = (): React.ReactElement => {
  return (
    <WarningsThemeProvider>
      <Paper sx={{ height: 'calc(100vh)' }}>
        <PublicWarningsForm {...cancelledSigmetProps} />
      </Paper>
    </WarningsThemeProvider>
  );
};
