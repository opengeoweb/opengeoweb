/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import i18n from 'i18next';
import { dateUtils } from '@opengeoweb/shared';
import {
  WARN_NAMESPACE,
  initWarnI18n,
  translateKeyOutsideComponents,
} from '../../../utils/i18n';
import {
  VALID_FROM_MAX_HOURS_AFTER_CURRENT,
  isMaxHoursAfterCurrentTime,
  isValueBeforeCurrentTime,
} from './ValidFromField';

describe('components/PublicWarningsForm/PublicWarningsForm/ValidFromField', () => {
  beforeEach(() => {
    const now = '2023-12-20T14:00:00Z';
    jest.spyOn(dateUtils, 'getCurrentTimeAsString').mockReturnValue(now);
  });
  afterEach(() => {
    jest.restoreAllMocks();
  });
  describe('isValueBeforeCurrentTime', () => {
    initWarnI18n();
    it('should return error when value is before current time', () => {
      expect(isValueBeforeCurrentTime('2023-12-20T13:59:00Z', i18n.t)).toEqual(
        translateKeyOutsideComponents(
          'warning-valid-from-error-before-current',
        ),
      );
    });
    it('should return true when value is equal to current time', () => {
      expect(isValueBeforeCurrentTime('2023-12-20T14:00:00Z', i18n.t)).toEqual(
        true,
      );
    });
    it('should return true when value is after current time', () => {
      expect(isValueBeforeCurrentTime('2023-12-20T14:01:00Z', i18n.t)).toEqual(
        true,
      );
    });
  });

  describe('isMaxHoursAfterCurrentTime', () => {
    it('should return error when value is more than 168 hours after current time', () => {
      expect(
        isMaxHoursAfterCurrentTime('2023-12-27T14:01:00Z', i18n.t),
      ).toEqual(
        i18n.t('warning-valid-from-error-max-hours', {
          ns: WARN_NAMESPACE,
          MAX_HOURS: VALID_FROM_MAX_HOURS_AFTER_CURRENT,
        }),
      );
    });
    it('should return true when value is exactly 168 hours after current time', () => {
      expect(
        isMaxHoursAfterCurrentTime('2023-12-27T14:00:00Z', i18n.t),
      ).toEqual(true);
    });
    it('should return true when value less than 168 hours after current time', () => {
      expect(
        isMaxHoursAfterCurrentTime('2023-12-27T13:59:00Z', i18n.t),
      ).toEqual(true);
    });
  });
});
