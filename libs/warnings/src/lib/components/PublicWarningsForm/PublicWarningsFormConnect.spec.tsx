/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { emptyGeoJSON } from '@opengeoweb/webmap-react';
import { Airmet, AviationPhenomenaCode } from '@opengeoweb/sigmet-airmet';
import { setupServer } from 'msw/node';
import { QueryClient } from '@tanstack/react-query';
import {
  PublicWarningsFormConnect,
  PublicWarningsFormConnectProps,
} from './PublicWarningsFormConnect';

import {
  TAKEOVER_MESSAGE,
  publicWarningFormActions,
} from '../../store/publicWarningForm/reducer';
import { formatDate } from './AreaField/AreaField';
import { DrawingListItem } from '../../store/warningsDrawings/types';
import {
  PublicWarningDetail,
  PublicWarningStatus,
  PublicWarning,
} from '../../store/publicWarningForm/types';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { MOCK_USERNAME } from '../../utils/fakeApi';
import {
  makeWarningsHandler,
  requestHandlers,
} from '../../utils/fakeApi/index';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store/store';
import { PublicWarningsFormDialogConnect } from '../PublicWarningsFormDialog/PublicWarningsFormDialogConnect';
import { PublicWarningListConnect } from '../PublicWarningList';

const testObject: DrawingListItem = {
  id: 'test',
  lastUpdatedTime: '2022-06-01T12:34:27Z',
  objectName: 'my test area',
  scope: 'user',
  geoJSON: emptyGeoJSON,
};

const testPublicWarningDetail: PublicWarningDetail = {
  id: '923723984872338768743',
  phenomenon: 'coastalEvent',
  validFrom: '2022-06-01T15:00:00Z',
  validUntil: '2022-06-01T18:00:00Z',
  level: 'extreme',
  probability: 80,
  descriptionOriginal: 'Some pretty intense coastal weather is coming our way',
  descriptionTranslation: 'And this would be the translation',
  areas: [{ geoJSON: emptyGeoJSON, objectName: testObject.objectName }],
};

const server = setupServer(...requestHandlers);
describe('components/PublicWarningsFormConnect', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });
  it('should render with default props', () => {
    render(
      <WarningsThemeStoreProvider>
        <PublicWarningsFormConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-no-object-selected'),
      ),
    ).toBeTruthy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('object-manager-options-menu'),
      }),
    ).toBeFalsy();
  });

  it('should render prefilled object values', () => {
    const store = createMockStore();

    store.dispatch(
      publicWarningFormActions.setFormValues({
        object: testObject,
        warning: {
          warningDetail: testPublicWarningDetail,
          status: PublicWarningStatus.DRAFT,
          type: 'public',
        },
      }),
    );

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(store.getState().publicWarningForm.object).toEqual(testObject);
    expect(store.getState().publicWarningForm.warning?.warningDetail).toEqual(
      testPublicWarningDetail,
    );
    expect(screen.getByText(testObject.objectName)).toBeTruthy();
    expect(
      screen.getByText(formatDate(testObject.lastUpdatedTime)),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('object-manager-options-menu'),
      }),
    ).toBeTruthy();
  });

  it('should publish a form', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2022-06-01T12:00:00Z'));
    const store = createMockStore();

    store.dispatch(
      publicWarningFormActions.setFormValues({
        object: testObject,
        warning: {
          warningDetail: testPublicWarningDetail,
          status: PublicWarningStatus.DRAFT,
          editor: MOCK_USERNAME,
          type: 'public',
        },
      }),
    );

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormConnect editor={MOCK_USERNAME} />
      </WarningsThemeStoreProvider>,
    );
    expect(store.getState().publicWarningForm.formState).toBeUndefined();

    const publishButton = screen.getByRole('button', {
      name: translateKeyOutsideComponents('warning-button-publish'),
    });

    fireEvent.click(publishButton);

    await waitFor(() => {
      expect(
        screen.getByRole('button', {
          name: translateKeyOutsideComponents('warning-button-publishing'),
        }),
      ).toBeTruthy();
    });

    await waitFor(() => {
      expect(
        screen.queryByRole('dialog', {
          name: 'Public warning',
        }),
      ).toBeFalsy();
    });

    jest.useRealTimers();
  });

  it('should save a form', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2022-06-01T12:00:00Z'));
    const store = createMockStore();

    store.dispatch(
      publicWarningFormActions.setFormValues({
        object: testObject,
        warning: {
          warningDetail: testPublicWarningDetail,
          status: PublicWarningStatus.DRAFT,
          editor: MOCK_USERNAME,
          type: 'public',
        },
      }),
    );

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormConnect editor={MOCK_USERNAME} />
      </WarningsThemeStoreProvider>,
    );
    expect(store.getState().publicWarningForm.formState).toBeUndefined();

    const saveButton = screen.getByRole('button', {
      name: translateKeyOutsideComponents('warning-button-save'),
    });

    fireEvent.click(saveButton);

    await waitFor(() => {
      expect(
        screen.getByRole('button', {
          name: translateKeyOutsideComponents('warning-button-saving'),
        }),
      ).toBeTruthy();
    });
    await waitFor(() => {
      expect(store.getState().publicWarningForm.warning?.editor).toEqual(
        MOCK_USERNAME,
      );
    });
    jest.useRealTimers();
  });

  it('should clear areas when clearing form', () => {
    const store = createMockStore();

    store.dispatch(
      publicWarningFormActions.setFormValues({
        warning: {
          warningDetail: testPublicWarningDetail,
          status: PublicWarningStatus.DRAFT,
          editor: MOCK_USERNAME,
          type: 'public',
        },
      }),
    );

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormConnect editor={MOCK_USERNAME} />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().publicWarningForm.warning?.warningDetail.areas,
    ).toEqual(testPublicWarningDetail.areas);

    const clearButton = screen.getByRole('button', {
      name: translateKeyOutsideComponents('warning-button-clear'),
    });

    fireEvent.click(clearButton);
    expect(
      store.getState().publicWarningForm.warning?.warningDetail.areas,
    ).toEqual([]);
  });

  it('should show error', async () => {
    const store = createMockStore();

    store.dispatch(
      publicWarningFormActions.setFormValues({
        object: testObject,
      }),
    );
    const testError = 'something went wrong';
    store.dispatch(
      publicWarningFormActions.setFormError({ message: testError }),
    );

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormConnect />
      </WarningsThemeStoreProvider>,
    );
    expect(screen.getByText(testError)).toBeTruthy();
  });

  it('should render readonly if you are not the editor', () => {
    const store = createMockStore();

    store.dispatch(
      publicWarningFormActions.setFormValues({
        object: testObject,
        warning: {
          warningDetail: testPublicWarningDetail,
          status: PublicWarningStatus.DRAFT,
          editor: 'phil.collins',
          type: 'public',
        },
      }),
    );

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeFalsy();
  });
  it('should render in edit mode if you are creating a new warning (you are not yet set as editor)', () => {
    const store = createMockStore();

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeTruthy();
  });

  it('should show a warning when other user takes over', async () => {
    const store = createMockStore();

    const testPublicWarning: PublicWarning = {
      id: testPublicWarningDetail.id,
      warningDetail: testPublicWarningDetail,
      status: PublicWarningStatus.DRAFT,
      editor: MOCK_USERNAME,
      type: 'public',
    };
    const queryClient = new QueryClient({
      defaultOptions: { queries: { retry: false } },
    });
    server.use(makeWarningsHandler([testPublicWarning]));
    void queryClient.setQueryData(['public-warning-list'], [testPublicWarning]);

    store.dispatch(
      publicWarningFormActions.openPublicWarningFormDialog({
        object: testObject,
        warning: testPublicWarning,
      }),
    );

    render(
      <WarningsThemeStoreProvider
        store={store}
        optionalQueryClient={queryClient}
      >
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
        <PublicWarningsFormConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(screen.queryByText(TAKEOVER_MESSAGE)).toBeFalsy();
    expect(store.getState().publicWarningForm.error).toBeUndefined();

    server.use(
      makeWarningsHandler([{ ...testPublicWarning, editor: 'take.over' }]),
    );

    void queryClient.refetchQueries({ queryKey: ['public-warning-list'] });

    await waitFor(() => {
      expect(store.getState().publicWarningForm.error).toEqual({
        message: TAKEOVER_MESSAGE,
        severity: 'warning',
      });
    });
    expect(screen.getByText(TAKEOVER_MESSAGE)).toBeTruthy();
    server.resetHandlers();
  });

  it('should show readonly for a published airmet', () => {
    const store = createMockStore();
    const props: PublicWarningsFormConnectProps = {
      selectedAviationProduct: {
        id: 'airmet-1',
        phenomenon: 'SFC_WIND' as AviationPhenomenaCode,
        validDateStart: '2023-12-20T14:00:00Z',
        validDateEnd: '2023-12-20T18:00:00Z',
        level: { value: 500, unit: 'FL' },
        areas: [{ geoJSON: {}, objectName: 'test area' }],
        status: 'PUBLISHED',
        isObservationOrForecast: 'OBS',
        type: 'TEST',
        change: 'NC',
        movementType: 'STATIONARY',
        issueDate: '2023-12-20T13:00:00Z',
        firName: 'Amsterdam FIR',
        locationIndicatorATSR: 'EHAA',
        locationIndicatorATSU: 'EHAA',
        locationIndicatorMWO: 'EHDB',
        startGeometry: { type: 'FeatureCollection', features: [] },
        startGeometryIntersect: { type: 'FeatureCollection', features: [] },
      } as Airmet,
      mode: 'view',
      warningType: 'airmet',
    };

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormConnect {...props} />
      </WarningsThemeStoreProvider>,
    );

    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeFalsy();
  });

  it('should show in edit mode for a draft airmet', () => {
    const store = createMockStore();
    const props: PublicWarningsFormConnectProps = {
      selectedAviationProduct: {
        id: 'airmet-1',
        phenomenon: 'SFC_WIND' as AviationPhenomenaCode,
        validDateStart: '2023-12-20T14:00:00Z',
        validDateEnd: '2023-12-20T18:00:00Z',
        level: { value: 500, unit: 'FL' },
        areas: [{ geoJSON: {}, objectName: 'test area' }],
        status: 'DRAFT',
        isObservationOrForecast: 'OBS',
        type: 'TEST',
        change: 'NC',
        movementType: 'STATIONARY',
        issueDate: '2023-12-20T13:00:00Z',
        firName: 'Amsterdam FIR',
        locationIndicatorATSR: 'EHAA',
        locationIndicatorATSU: 'EHAA',
        locationIndicatorMWO: 'EHDB',
        startGeometry: { type: 'FeatureCollection', features: [] },
        startGeometryIntersect: { type: 'FeatureCollection', features: [] },
      } as Airmet,
      mode: 'edit',
      warningType: 'airmet',
    };

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormConnect {...props} />
      </WarningsThemeStoreProvider>,
    );

    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeTruthy();
  });

  it('should show in edit mode for a new airmet', () => {
    const store = createMockStore();
    const props: PublicWarningsFormConnectProps = {
      mode: 'new',
      warningType: 'airmet',
    };

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormConnect {...props} />
      </WarningsThemeStoreProvider>,
    );

    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeTruthy();
  });
  it('should show in edit mode for a new sigmet', () => {
    const store = createMockStore();
    const props: PublicWarningsFormConnectProps = {
      mode: 'new',
      warningType: 'sigmet',
    };

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormConnect {...props} />
      </WarningsThemeStoreProvider>,
    );

    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeTruthy();
  });
  it('should show in edit mode for a renewed sigmet', () => {
    const store = createMockStore();
    const props: PublicWarningsFormConnectProps = {
      mode: 'view',
      warningType: 'sigmet',
    };

    // Dispatch necessary actions to set up the state
    store.dispatch(
      publicWarningFormActions.setFormValues({
        warning: {
          status: PublicWarningStatus.DRAFT,
          warningDetail: {
            id: '',
            phenomenon: '',
            validFrom: '',
            validUntil: '',
            level: '',
            probability: 0,
            descriptionOriginal: '',
            descriptionTranslation: '',
            areas: [],
          },
          type: 'public',
        },
        formState: 'edit',
      }),
    );

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormConnect {...props} />
      </WarningsThemeStoreProvider>,
    );

    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeTruthy();
  });
});
