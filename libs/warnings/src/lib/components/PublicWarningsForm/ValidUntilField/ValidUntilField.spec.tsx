/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import i18n from 'i18next';
import { dateUtils } from '@opengeoweb/shared';
import {
  VALID_UNTIL_MAX_HOURS_AFTER_CURRENT,
  VALID_UNTIL_MIN_HOURS_AFTER_VALID_FROM,
  isMaxHoursAfterCurrentTime,
  isMinHoursAfterValidFrom,
  isValueBeforeCurrentTime,
} from './ValidUntilField';
import {
  WARN_NAMESPACE,
  initWarnI18n,
  translateKeyOutsideComponents,
} from '../../../utils/i18n';

describe('components/PublicWarningsForm/PublicWarningsForm/ValidUntilField', () => {
  const now = '2023-12-20T14:00:00Z';
  beforeEach(() => {
    jest.spyOn(dateUtils, 'getCurrentTimeAsString').mockReturnValue(now);
  });
  afterEach(() => {
    jest.restoreAllMocks();
  });
  describe('isValueBeforeCurrentTime', () => {
    initWarnI18n();
    it('should return error when value is before current time', () => {
      expect(isValueBeforeCurrentTime('2023-12-20T13:59:00Z', i18n.t)).toEqual(
        translateKeyOutsideComponents(
          'warning-valid-until-error-before-current',
        ),
      );
    });
    it('should return true when value is equal to current time', () => {
      expect(isValueBeforeCurrentTime(now, i18n.t)).toEqual(true);
    });
    it('should return true when value is after current time', () => {
      expect(isValueBeforeCurrentTime('2023-12-20T14:01:00Z', i18n.t)).toEqual(
        true,
      );
    });
  });

  describe('isMaxHoursAfterCurrentTime', () => {
    it('should return error when value is more than 180 hours after current time', () => {
      expect(
        isMaxHoursAfterCurrentTime('2023-12-28T02:01:00Z', i18n.t),
      ).toEqual(
        i18n.t('warning-valid-until-error-max-hours', {
          ns: WARN_NAMESPACE,
          MAX_HOURS: VALID_UNTIL_MAX_HOURS_AFTER_CURRENT,
        }),
      );
    });
    it('should return true when value is exactly 180 hours after current time', () => {
      expect(
        isMaxHoursAfterCurrentTime('2023-12-28T02:00:00Z', i18n.t),
      ).toEqual(true);
    });
    it('should return true when value less than 180 hours after current time', () => {
      expect(
        isMaxHoursAfterCurrentTime('2023-12-28T01:59:00Z', i18n.t),
      ).toEqual(true);
    });
  });

  describe('isOneHourAfterValidFrom', () => {
    it('should return true when exactly 1 hour difference', () => {
      const validFrom = '2020-09-17T13:00:00Z';
      const validUntil = '2020-09-17T14:00:00Z';
      expect(isMinHoursAfterValidFrom(validUntil, validFrom, i18n.t)).toEqual(
        true,
      );
    });

    it('should return true when more than 1 hour difference', () => {
      const validFrom = '2020-09-17T13:00:00Z';
      const validUntil = '2020-09-17T14:01:00Z';
      expect(isMinHoursAfterValidFrom(validUntil, validFrom, i18n.t)).toEqual(
        true,
      );
    });

    it('should return error when less than 1 hour difference', () => {
      const validFrom = '2020-09-17T13:00:00Z';
      const validUntil = '2020-09-17T13:59:00Z';
      expect(isMinHoursAfterValidFrom(validUntil, validFrom, i18n.t)).toEqual(
        i18n.t('warning-valid-until-error-min-hours', {
          ns: WARN_NAMESPACE,
          MIN_HOURS: VALID_UNTIL_MIN_HOURS_AFTER_VALID_FROM,
        }),
      );
    });

    it('should return true when values are empty', () => {
      const validFrom = '2020-09-17T13:00:00Z';
      const validUntil = '2020-09-17T14:00:00Z';
      expect(isMinHoursAfterValidFrom('', '', i18n.t)).toEqual(true);
      expect(isMinHoursAfterValidFrom(validUntil, '', i18n.t)).toEqual(true);
      expect(isMinHoursAfterValidFrom('', validFrom, i18n.t)).toEqual(true);
    });
  });
});
