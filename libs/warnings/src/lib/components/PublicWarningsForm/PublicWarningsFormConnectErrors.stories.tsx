/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Paper } from '@mui/material';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { dateUtils } from '@opengeoweb/shared';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { PublicWarningsFormConnect } from './PublicWarningsFormConnect';
import { publicWarningFormActions } from '../../store/publicWarningForm/reducer';
import {
  testGeoJSON,
  fakeErrorRequest,
  fakePostError,
} from '../../storybookUtils/testUtils';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WarningsApi } from '../../utils/api';
import { PublicWarningStatus } from '../../store/publicWarningForm/types';
import { createMockStore } from '../../store/store';

export default {
  title: 'components/PublicWarningsFormConnect/errors',
};

const Demo: React.FC = () => {
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(
      publicWarningFormActions.setFormValues({
        object: {
          id: '923723984872338768743',
          objectName: 'Drawing object 101',
          lastUpdatedTime: '2022-06-01T12:34:27Z',
          scope: 'user',
          geoJSON: testGeoJSON,
        },
        warning: {
          warningDetail: {
            id: '923723984872338768743',
            phenomenon: 'coastalEvent',
            validFrom: dateUtils.dateToString(
              dateUtils.add(new Date(), { hours: 1 }),
            )!,
            validUntil: dateUtils.dateToString(
              dateUtils.add(new Date(), { hours: 2 }),
            )!,
            level: 'extreme',
            probability: 80,
            descriptionOriginal:
              'Some pretty intense coastal weather is coming our way',
            descriptionTranslation: 'And this would be the translation',
            areas: [{ geoJSON: testGeoJSON, objectName: 'test name' }],
          },
          status: PublicWarningStatus.TODO,
          type: 'public',
        },
      }),
    );
  }, [dispatch]);
  return <PublicWarningsFormConnect />;
};

export const SaveError = (): React.ReactElement => {
  const fakeApiWithErrors = (): WarningsApi => ({
    ...createFakeApi(),
    updateWarning: () => fakeErrorRequest<void>(fakePostError),
  });

  return (
    <WarningsThemeStoreProvider
      store={createMockStore()}
      createApi={fakeApiWithErrors}
    >
      <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
        <Demo />
      </Paper>
    </WarningsThemeStoreProvider>
  );
};

export const PublishError = (): React.ReactElement => {
  const fakeApiWithErrors = (): WarningsApi => ({
    ...createFakeApi(),
    saveWarningAs: () => fakeErrorRequest<string>(fakePostError),
  });

  return (
    <WarningsThemeStoreProvider
      store={createMockStore()}
      createApi={fakeApiWithErrors}
    >
      <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
        <Demo />
      </Paper>
    </WarningsThemeStoreProvider>
  );
};
