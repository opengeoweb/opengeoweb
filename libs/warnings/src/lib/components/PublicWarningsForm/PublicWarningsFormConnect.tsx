/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useAuthenticationContext } from '@opengeoweb/authentication';
import { AviationProduct, FormMode } from '@opengeoweb/sigmet-airmet';
import { MapPreset, uiActions, uiTypes } from '@opengeoweb/store';
import { PublicWarningsForm } from './PublicWarningsForm';
import { selectPublicWarningState } from '../../store/publicWarningForm/selectors';
import { publicWarningFormActions } from '../../store/publicWarningForm/reducer';
import {
  Area,
  PublicWarningDetail,
  PublicWarningStatus,
  WarningType,
} from '../../store/publicWarningForm/types';
import { usePublishWarning, useSaveWarning } from '../../api/warning-api/hooks';

export interface PublicWarningsFormConnectProps {
  editor?: string;
  selectedAviationProduct?: AviationProduct;
  warningType?: WarningType;
  mode?: FormMode;
  defaultMapPreset?: MapPreset;
}

export const PublicWarningsFormConnect: React.FC<
  PublicWarningsFormConnectProps
> = ({
  editor,
  defaultMapPreset,
  selectedAviationProduct,
  warningType = 'public',
  mode,
}: PublicWarningsFormConnectProps) => {
  const { warning, object, formState, error, panelId } = useSelector(
    selectPublicWarningState,
  );
  const { mutateAsync: publishWarning } = usePublishWarning();
  const { mutateAsync: saveWarning } = useSaveWarning();

  const { auth } = useAuthenticationContext();
  const isEditor = editor === auth?.username || false;

  const dispatch = useDispatch();

  const publishForm = async (
    newFormValues: PublicWarningDetail,
  ): Promise<Headers | null> => {
    return publishWarning({
      id: newFormValues.id || newFormValues.linked_to_id || '',
      data: {
        warningDetail: newFormValues.id
          ? newFormValues
          : { ...newFormValues, id: newFormValues.linked_to_id },
        status: PublicWarningStatus.PUBLISHED,
        type: 'public',
      },
    });
  };

  const clearForm = (): void => {
    dispatch(publicWarningFormActions.removeAllAreas());
  };

  const saveForm = async (
    newFormValues: PublicWarningDetail,
  ): Promise<Headers | null> => {
    const status = newFormValues.linked_to_id
      ? PublicWarningStatus.DRAFT_PUBLISHED
      : PublicWarningStatus.DRAFT;
    return saveWarning({
      id: newFormValues.id!,
      data: {
        warningDetail: newFormValues,
        status,
        editor: auth?.username,
        type: 'public',
      },
      panelId,
    });
  };

  const onFormDirty = (isFormDirty: boolean): void => {
    dispatch(
      publicWarningFormActions.setFormDirty({
        isFormDirty,
      }),
    );
  };

  const getIsReadOnly = (): boolean => {
    // public warning related
    if (warningType === 'public') {
      if (formState === 'readonly') {
        return true;
      }
      if (warning?.status === 'TODO') {
        return false;
      }
      // Readonly in case you are not creating a new warning and you are not set as the editor
      return !isEditor && warning?.status !== undefined;
    }
    // aviation related
    if (selectedAviationProduct?.status === 'DRAFT') {
      return false;
    }
    if (formState === 'edit') {
      return false;
    }
    if (mode === 'new') {
      return false;
    }
    return true;
  };

  const isReadOnly = getIsReadOnly();
  const formMode = formState === 'edit' ? 'edit' : mode;

  const onAddObject = (): void => {
    dispatch(
      uiActions.setToggleOpenDialog({
        type: uiTypes.DialogTypes.AreaObjectLoader,
        setOpen: true,
      }),
    );
  };

  const onSetViewModeResetArea = (area: Area[]): void => {
    dispatch(
      publicWarningFormActions.addArea({
        area: area[0],
      }),
    );
  };

  return (
    <PublicWarningsForm
      object={object}
      publicWarningDetails={warning?.warningDetail}
      onPublishForm={publishForm}
      onSaveForm={saveForm}
      onClearForm={clearForm}
      isReadOnly={isReadOnly}
      error={error}
      onFormDirty={onFormDirty}
      defaultMapPreset={defaultMapPreset}
      onAddObject={onAddObject}
      selectedAviationProduct={selectedAviationProduct}
      warningType={warningType}
      mode={formMode}
      onSetViewModeResetArea={onSetViewModeResetArea}
    />
  );
};
