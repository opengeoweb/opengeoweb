/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { mapActions, uiActions } from '@opengeoweb/store';
import { fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ObjectManagerConnect } from './ObjectManagerConnect';
import { ObjectManagerMapButtonConnect } from './ObjectManagerMapButtonConnect';
import DrawingToolMapButtonConnect from '../DrawingTool/DrawingToolMapButtonConnect';
import { drawingDialogType } from '../../store/warningsDrawings/utils';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { createMockStore } from '../../store';

describe('src/components/ObjectManager/ObjectManagerMapButtonConnect', () => {
  it('should open and close object manager for two maps', async () => {
    const store = createMockStore();
    store.dispatch(mapActions.registerMap({ mapId: 'mapId1' }));
    store.dispatch(mapActions.registerMap({ mapId: 'mapId2' }));
    store.dispatch(
      uiActions.registerDialog({
        type: drawingDialogType,
      }),
    );

    render(
      <WarningsThemeStoreProvider store={store}>
        <div>
          <ObjectManagerMapButtonConnect mapId="mapId1" />
          <ObjectManagerMapButtonConnect mapId="mapId2" />
          <ObjectManagerConnect />
        </div>
      </WarningsThemeStoreProvider>,
    );

    expect(
      screen.queryByRole('heading', { name: /object manager/i }),
    ).not.toBeInTheDocument();

    // open object manager for map1
    const buttonMap1 = /object manager button for mapId1/i;
    const titleMap1 = /object manager for mapId1/i;
    fireEvent.click(screen.getByRole('button', { name: buttonMap1 }));
    screen.getByRole('heading', { name: titleMap1 });
    await screen.findByText(/wednesday, june 01/i);
    screen.getByText(/drawing object 101/i);

    const optionsButton = screen.getAllByRole('button', {
      name: /options/i,
    })[0];
    fireEvent.click(optionsButton);
    screen.getByRole('menuitem', { name: /edit/i });
    await userEvent.keyboard('{Escape}');

    // open object manager for map2
    const buttonMap2 = /object manager button for mapId2/i;
    const titleMap2 = /object manager for mapId2/i;
    fireEvent.click(screen.getByRole('button', { name: buttonMap2 }));
    screen.getByRole('heading', { name: titleMap2 });
    expect(
      screen.queryByRole('heading', { name: titleMap1 }),
    ).not.toBeInTheDocument();

    // close object manager by clicking on button again
    fireEvent.click(screen.getByRole('button', { name: buttonMap2 }));
    expect(
      screen.queryByRole('heading', { name: titleMap2 }),
    ).not.toBeInTheDocument();

    // open and close object manager for map2
    fireEvent.click(screen.getByRole('button', { name: buttonMap2 }));
    screen.getByRole('heading', { name: titleMap2 });
    fireEvent.click(screen.getByRole('button', { name: /close/i }));
    expect(
      screen.queryByRole('heading', { name: titleMap2 }),
    ).not.toBeInTheDocument();
  });

  it('should close the drawdialog when opening the object manager', async () => {
    const mapId = 'mapId1';
    const store = createMockStore();
    store.dispatch(mapActions.registerMap({ mapId: 'mapId1' }));
    store.dispatch(mapActions.registerMap({ mapId: 'mapId2' }));
    store.dispatch(
      uiActions.registerDialog({
        type: drawingDialogType,
      }),
    );

    // register drawingTool
    store.dispatch(
      uiActions.registerDialog({
        type: drawingDialogType,
        setOpen: false,
        source: 'app',
      }),
    );
    store.dispatch(
      uiActions.setActiveMapIdForDialog({
        type: drawingDialogType,
        mapId,
        setOpen: true,
        source: 'app',
      }),
    );
    render(
      <WarningsThemeStoreProvider store={store}>
        <div>
          <ObjectManagerMapButtonConnect mapId={mapId} />
          <DrawingToolMapButtonConnect mapId={mapId} />
          <ObjectManagerConnect />
        </div>
      </WarningsThemeStoreProvider>,
    );

    expect(
      screen.queryByRole('heading', { name: /object manager/i }),
    ).not.toBeInTheDocument();

    // check active
    const drawingButton = await screen.findByRole('button', {
      name: 'Drawing Toolbox',
    });
    expect(drawingButton.classList).toContain('Mui-selected');

    // open object manager for map1
    const buttonMap1 = /object manager button for mapId1/i;
    const titleMap1 = /object manager for mapId1/i;
    await userEvent.click(screen.getByRole('button', { name: buttonMap1 }));
    screen.getByRole('heading', { name: titleMap1 });
    await screen.findByText(/wednesday, june 01/i);
    screen.getByText(/drawing object 101/i);

    const optionsButton = screen.getAllByRole('button', {
      name: /options/i,
    })[0];
    await userEvent.click(optionsButton);
    screen.getByRole('menuitem', { name: /edit/i });
    await userEvent.keyboard('{Escape}');

    expect(drawingButton.classList).not.toContain('Mui-selected');
  });
});
