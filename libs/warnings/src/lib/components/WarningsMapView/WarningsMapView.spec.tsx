/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  WMBBOX,
  WMJSMap,
  getWMJSMapById,
  registerWMJSMap,
  unRegisterAllWMJSLayersAndMaps,
} from '@opengeoweb/webmap';
import { render, screen } from '@testing-library/react';
import { defaultBbox } from '@opengeoweb/core';
import { WarningsMapView, zoomToFeature } from './WarningsMapView';
import { WarningsI18nProvider } from '../Providers/Providers';

const simplePolygonGeoJSONHalfOfNL: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {},
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [4.921875, 51.767839887322154],
            [4.7900390625, 49.97242235423708],
            [7.679443359375, 49.79544988802771],
            [7.888183593749999, 51.74743863117572],
            [5.86669921875, 51.984880139916626],
            [4.921875, 51.767839887322154],
          ],
        ],
      },
    },
  ],
};

const simplePolygonGeoJSONModified: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {},
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [4.921875, 51.767839887322154],
            [4.7900390625, 49.97242235423708],
            [27.679443359375, 59.79544988802771],
            [7.888183593749999, 51.74743863117572],
            [5.86669921875, 51.984880139916626],
            [4.921875, 51.767839887322154],
          ],
        ],
      },
    },
  ],
};

describe('components/PublicWarningsForm/WarningsMapView', () => {
  describe('zoomToFeature', () => {
    it('should zoom to the feature when the map is in mercator projection', () => {
      unRegisterAllWMJSLayersAndMaps();
      const map = new WMJSMap(document.createElement('div'));
      map.setSize(512, 256);
      map.setProjection(
        'EPSG:3857',
        new WMBBOX({
          left: -1010048.1871574474,
          bottom: 6176666.999248354,
          right: 1967368.0267185564,
          top: 8020268.370069409,
        }),
      );
      const mapId = 'testmap-1';
      registerWMJSMap(map, mapId);
      const spy = jest.spyOn(map, 'zoomTo');
      zoomToFeature(mapId, simplePolygonGeoJSONHalfOfNL);
      expect(spy).toHaveBeenCalledTimes(2);
      expect(spy).toHaveBeenNthCalledWith(
        1,
        expect.objectContaining({
          bottom: 6410926.436334303,
          left: 533224.7093173895,
          right: 878108.5809401047,
          top: 6797392.0513441535,
        }),
      );
    });

    it('should zoom to the feature when the map is in latlon projection', () => {
      unRegisterAllWMJSLayersAndMaps();
      const map = new WMJSMap(document.createElement('div'));
      map.setSize(256, 256);
      map.setProjection(
        'EPSG:4326',
        new WMBBOX({
          left: -50,
          bottom: -40,
          right: 30,
          top: 70,
        }),
      );
      const mapId = 'testmap-1';
      registerWMJSMap(map, mapId);
      const spy = jest.spyOn(map, 'zoomTo');
      zoomToFeature(mapId, simplePolygonGeoJSONHalfOfNL);
      expect(spy).toHaveBeenCalledTimes(2);
      expect(spy).toHaveBeenNthCalledWith(
        1,
        expect.objectContaining({
          bottom: 49.79544988802771,
          left: 4.7900390625,
          right: 7.888183593749999,
          top: 51.984880139916626,
        }),
      );
    });
  });

  describe('WarningsMapView', () => {
    it('should zoom to provided geojson if no defaultBbox', () => {
      unRegisterAllWMJSLayersAndMaps();
      const { rerender } = render(
        <WarningsI18nProvider>
          <WarningsMapView geojsonFeature={simplePolygonGeoJSONHalfOfNL} />
        </WarningsI18nProvider>,
      );
      const baselayer = screen.getAllByTestId('mapViewLayer')[0].textContent;
      const idArray = baselayer?.split('_baseLayer');
      const mapId = idArray![0];
      const map = getWMJSMapById(mapId);
      expect(map).toBeDefined();
      const spy = jest.spyOn(map!, 'zoomTo');

      rerender(
        <WarningsI18nProvider>
          <WarningsMapView geojsonFeature={simplePolygonGeoJSONModified} />
        </WarningsI18nProvider>,
      );
      expect(spy).toHaveBeenCalledTimes(2);
      expect(spy).toHaveBeenNthCalledWith(
        1,
        expect.objectContaining({
          bottom: 6441501.247648372,
          left: 533224.7093173895,
          right: 3081261.5402068826,
          top: 8354337.18939323,
        }),
      );
    });
    it('should not zoom to provided geojson if defaultBbox provided', () => {
      unRegisterAllWMJSLayersAndMaps();
      const { rerender } = render(
        <WarningsI18nProvider>
          <WarningsMapView
            geojsonFeature={simplePolygonGeoJSONHalfOfNL}
            defaultMapPreset={{ proj: defaultBbox }}
          />
        </WarningsI18nProvider>,
      );
      const baselayer = screen.getAllByTestId('mapViewLayer')[0].textContent;
      const idArray = baselayer?.split('_baseLayer');
      const mapId = idArray![0];
      const map = getWMJSMapById(mapId);
      expect(map).toBeDefined();
      const spy = jest.spyOn(map!, 'zoomTo');

      rerender(
        <WarningsI18nProvider>
          <WarningsMapView
            geojsonFeature={simplePolygonGeoJSONModified}
            defaultMapPreset={{ proj: defaultBbox }}
          />
        </WarningsI18nProvider>,
      );
      expect(spy).not.toHaveBeenCalled();
    });
  });
});
