/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { currentlySupportedDrawModes } from '@opengeoweb/webmap-react';
import DrawingToolDialog from './DrawingToolDialog';
import { WarningsThemeProvider } from '../Providers/Providers';
import { DrawingToolForm } from '../DrawingToolForm/DrawingToolForm';

describe('src/components/MapDraw/DrawingToolDialog/DrawingToolDialog', () => {
  const props = {
    isOpen: true,
    onClose: (): void => {},
    showTitle: false,
    onMouseDown: jest.fn(),
  };

  const formProps = {
    onSubmitForm: jest.fn(),
    objectName: '23/08/2023 07:00 UTC',
    drawModes: currentlySupportedDrawModes,
    onChangeDrawMode: jest.fn(),
    activeDrawModeId: '',
    onDeactivateTool: jest.fn(),
    isInEditMode: false,
    geoJSONProperties: {},
    onChangeProperties: jest.fn(),
    onChangeName: jest.fn(),
    geoJSON: undefined,
  };

  it('should render required component', async () => {
    render(
      <WarningsThemeProvider>
        <DrawingToolDialog {...props}>
          <DrawingToolForm {...formProps} />
        </DrawingToolDialog>
      </WarningsThemeProvider>,
    );

    expect(screen.getByTestId('drawingToolWindow')).toBeTruthy();

    fireEvent.mouseDown(screen.getByTestId('drawingToolWindow')!);
    expect(props.onMouseDown).toHaveBeenCalled();
  });

  it('should show the loading indicator when loading', async () => {
    const props = {
      mapId: 'mapId_3',
      isOpen: true,
      onClose: (): void => {},
      showTitle: false,
      onMouseDown: jest.fn(),
      isLoading: true,
    };

    render(
      <WarningsThemeProvider>
        <DrawingToolDialog {...props}>
          <DrawingToolForm {...formProps} />
        </DrawingToolDialog>
      </WarningsThemeProvider>,
    );
    expect(await screen.findByTestId('loading-bar-drawingTool')).toBeTruthy();
  });

  it('should show the alert banner when error given', async () => {
    const props = {
      mapId: 'mapId_3',
      isOpen: true,
      onClose: (): void => {},
      showTitle: false,
      onMouseDown: jest.fn(),
    };

    render(
      <WarningsThemeProvider>
        <DrawingToolDialog {...props}>
          <DrawingToolForm {...formProps} dialogError="Test error message." />
        </DrawingToolDialog>
      </WarningsThemeProvider>,
    );

    expect(await screen.findByText('Test error message.')).toBeTruthy();
    expect(await screen.findByTestId('alert-banner')).toBeTruthy();
  });

  it('should show default title', async () => {
    render(
      <WarningsThemeProvider>
        <DrawingToolDialog {...props}>
          <DrawingToolForm {...formProps} />
        </DrawingToolDialog>
      </WarningsThemeProvider>,
    );

    expect(
      screen.getByRole('heading', { name: /Drawing Toolbox/i }).innerHTML,
    ).toMatch(/Drawing Toolbox/);
  });
  it('should show custom title', async () => {
    render(
      <WarningsThemeProvider>
        <DrawingToolDialog {...props} title="Test title">
          <DrawingToolForm {...formProps} />
        </DrawingToolDialog>
      </WarningsThemeProvider>,
    );
    expect(
      screen.getByRole('heading', { name: /Test title/i }).innerHTML,
    ).toMatch(/Test title/);
  });

  it('should include form', async () => {
    render(
      <WarningsThemeProvider>
        <DrawingToolDialog {...props} title="Test title">
          <DrawingToolForm {...formProps} />
        </DrawingToolDialog>
      </WarningsThemeProvider>,
    );
    expect(screen.getByText('Tools')).toBeTruthy();
  });
});
