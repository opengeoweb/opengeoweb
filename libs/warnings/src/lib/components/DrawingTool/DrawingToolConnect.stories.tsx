/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { ConfigurableMapConnect, LayerManagerConnect } from '@opengeoweb/core';
import DrawingToolConnect from './DrawingToolConnect';
import DrawingToolMapButtonConnect from './DrawingToolMapButtonConnect';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { createMockStore } from '../../store/store';

export default {
  title: 'components/DrawingToolConnect',
};

interface MapWithDrawingToolProps {
  mapId: string;
}

const MapWithDrawingTool: React.FC<MapWithDrawingToolProps> = ({
  mapId,
}: MapWithDrawingToolProps) => {
  return (
    <div style={{ height: '100vh' }}>
      <DrawingToolConnect mapId={mapId} />
      <LayerManagerConnect mapId={mapId} />
      <ConfigurableMapConnect
        id={mapId}
        shouldDisplayDrawControls
        layers={[]}
        mapControls={<DrawingToolMapButtonConnect mapId={mapId} />}
      />
    </div>
  );
};

export const DrawingToolConnectLight = (): React.ReactElement => {
  return (
    <WarningsThemeStoreProvider store={createMockStore()}>
      <MapWithDrawingTool mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};
