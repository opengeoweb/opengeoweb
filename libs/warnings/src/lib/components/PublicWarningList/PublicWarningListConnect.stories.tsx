/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { Box } from '@mui/material';
import { http, HttpResponse } from 'msw';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { PublicWarningListConnect } from './PublicWarningListConnect';
import { fakeErrorRequest } from '../../storybookUtils/testUtils';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WarningsApi } from '../../utils/api';
import { PublicWarningsFormDialogConnect } from '../PublicWarningsFormDialog';
import { createMockStore } from '../../store/store';
import {
  makeWarningsHandler,
  requestHandlers,
  warningList,
} from '../../utils/fakeApi/index';
import { Warning } from '../../store/publicWarningForm/types';

const PublicWarningListComponent = ({
  description,
}: {
  description?: string;
}): React.ReactElement => {
  return (
    <WarningsThemeStoreProvider store={createMockStore()}>
      <Box>{description}</Box>
      <Box sx={{ backgroundColor: '#f5f5f5', padding: 1, height: '100vh' }}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </Box>
    </WarningsThemeStoreProvider>
  );
};

export default {
  title: 'components/PublicWarningListConnect',
  component: PublicWarningListComponent,
};

export const LightMode = {};

export const ListError = {
  parameters: {
    msw: {
      handlers: {
        warnings: [
          http.get(/^.*\/warnings$/, () => {
            // respond using a mocked JSON body
            return HttpResponse.json(
              {
                message:
                  'Something went wrong while fetching, please try again.',
              },
              { status: 500 },
            );
          }),
          ...requestHandlers,
        ],
      },
    },
  },
};

let warningsLoadedCounter = 0;
export const EditorChanged = {
  args: {
    description:
      'Open an editable warning, change the editor to yourself, then refresh until you get the editor change message.',
  },
  parameters: {
    msw: {
      handlers: {
        warnings: [
          http.get(/^.*\/warnings$/, () => {
            warningsLoadedCounter += 1;
            // Return a different editor after 3 refetches
            return HttpResponse.json(
              warningsLoadedCounter < 3
                ? warningList
                : warningList.map((warning) => ({
                    ...warning,
                    editor: 'taken.over',
                  })),
            );
          }),
          ...requestHandlers,
        ],
      },
    },
  },
};

export const PublicWarningListErrorDeleteWarning = (): React.ReactElement => {
  const createFakeApiWithError = (): WarningsApi => ({
    ...createFakeApi(),
    deleteWarning: () => fakeErrorRequest<void>('Oops, something went wrong'),
  });

  return (
    <WarningsThemeStoreProvider
      createApi={createFakeApiWithError}
      store={createMockStore()}
    >
      <Box sx={{ height: '100vh' }}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </Box>
    </WarningsThemeStoreProvider>
  );
};

const amount = 15;
const indexList = Array.from(Array(amount), (_, x) => x);
const data = indexList.reduce((list: Warning[], rowIndex) => {
  const warnings = warningList.map((warning, index) => ({
    ...warning,
    id: `${rowIndex}-${index}`,
  }));
  return list.concat(warnings);
}, []);

export const ExtraLongList = {
  args: {
    description:
      'To test performance select a warning and close the form, response should be instant',
  },
  parameters: {
    msw: {
      handlers: {
        warnings: [makeWarningsHandler(data), ...requestHandlers],
      },
    },
  },
};
