/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import {
  fireEvent,
  render,
  renderHook,
  screen,
  waitFor,
} from '@testing-library/react';
import { getLastUpdateTitle } from '@opengeoweb/shared';
import { TFunction } from 'i18next';
import { WarningsThemeProvider } from '../Providers/Providers';
import { PublicWarningList, PublicWarningListProps } from './PublicWarningList';
import {
  airmetList,
  combinedWarnings,
  sigmetList,
  warningList,
} from '../../utils/fakeApi/index';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import {
  getWarningWithProposalId,
  getWarningWithoutIdStatusEditor,
  getWarningWithLinkedToId,
} from '../../utils/warningUtils';
import {
  BUTTON_AIRMET,
  BUTTON_SIGMET,
} from './NewWarningButton/NewWarningButton';
import { useWarningFilters } from '../FilterList/filter-hooks';
import {
  emptyFilterId,
  initialFilters,
  Phenomenon,
} from '../FilterList/filter-utils';

jest.mock(
  'react-virtualized-auto-sizer',
  () =>
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ({ children }: any): React.ReactNode =>
      children({ height: 1800, width: 600 }),
);

const getDefaultProps = (
  warnings = combinedWarnings,
): PublicWarningListProps => {
  const savedFilters = {
    ...initialFilters,
    phenomenon: {
      unspecified: true,
      [Phenomenon.wind]: true,
      [Phenomenon.highTemp]: true,
      [Phenomenon.lowTemp]: true,
      [Phenomenon.fog]: true,
      [Phenomenon.coastalEvent]: true,
      [Phenomenon.snowIce]: true,
      [Phenomenon.thunderstorm]: true,
    },
    source: { unspecified: true, 'PASCAL-ECMWF-EPS': true },
  };
  const { result } = renderHook(() =>
    useWarningFilters(warnings, () => {}, savedFilters),
  );

  const { byFilterState } = result.current;

  return {
    warnings: warnings.filter(byFilterState),
    onRefetchWarnings: jest.fn(),
    filterHookState: result.current,
    expandedSections: {
      TODO: true,
      DRAFT: true,
      PUBLISHED: true,
      EXPIRED: true,
    },
    setExpandedSections: jest.fn(),
  };
};

describe('components/PublicWarningList/PublicWarningList', () => {
  it('should show component', () => {
    const defaultProps = getDefaultProps();
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} warnings={[]} />
      </WarningsThemeProvider>,
    );
    expect(
      screen.getByText(
        translateKeyOutsideComponents('warning-list-button_create'),
      ),
    ).toBeTruthy();
    expect(screen.getByText('0 Reviews')).toBeTruthy();
    expect(screen.getByText('0 Drafts')).toBeTruthy();
    expect(screen.getByText('0 Published')).toBeTruthy();
    expect(
      screen.getByText(
        `0 ${translateKeyOutsideComponents('warning-list-status-expired')}`,
      ),
    ).toBeTruthy();
    expect(screen.queryByTestId('refresh-button')).toBeFalsy();
  });

  it('should show warnings sorted by type', () => {
    const defaultProps = getDefaultProps();
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} />
      </WarningsThemeProvider>,
    );

    expect(screen.getAllByTestId('warning-DRAFT')).toHaveLength(5);
    expect(screen.getAllByTestId('warning-PUBLISHED')).toHaveLength(3);
    expect(screen.getAllByTestId('warning-TODO')).toHaveLength(5);
    expect(screen.getAllByTestId('warning-EXPIRED')).toHaveLength(2);
  });

  it('should format dates correctly', () => {
    const defaultProps = getDefaultProps();
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} warnings={[warningList[0]]} />
      </WarningsThemeProvider>,
    );
    expect(screen.getByText('Wed 06 Dec 06:56')).toBeTruthy();
    expect(screen.getByText('Tue 19 Dec 00:00')).toBeTruthy();
    expect(screen.getByText('Wed 20 Dec 00:00')).toBeTruthy();
  });

  it('should sort warnings by date when clicking on the column header', () => {
    const defaultProps = getDefaultProps();
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} />
      </WarningsThemeProvider>,
    );

    fireEvent.click(
      screen.getAllByText(
        `${translateKeyOutsideComponents('warning-list-header-start-time')} UTC`,
      )[0],
    );

    // Descending order
    const sortedWarningsDesc = screen.getAllByTestId(/^warning-/);
    expect(sortedWarningsDesc[0]).toHaveTextContent('Tue 19 Dec 00:00');
    expect(sortedWarningsDesc[sortedWarningsDesc.length - 1]).toHaveTextContent(
      'Tue 05 Dec 14:47',
    );

    // Click on the "Start Time" column header again to sort by date in acending order
    fireEvent.click(
      screen.getAllByText(
        `${translateKeyOutsideComponents('warning-list-header-start-time')} UTC`,
      )[0],
    );

    // The selector "getAllByTestId will get all the different statuses, and certain statuses are always first
    // that's why the first element here is not the same as the last element of descending order
    const sortedWarningsAsc = screen.getAllByTestId(/^warning-/);
    expect(sortedWarningsAsc[0]).toHaveTextContent('Mon 11 Dec 00:00');
    expect(sortedWarningsAsc[sortedWarningsAsc.length - 1]).toHaveTextContent(
      'Thu 07 Dec 14:47',
    );
  });

  it('should show error', () => {
    const defaultProps = getDefaultProps();
    const errorMessage = 'this is a test';
    const testError = new Error(errorMessage);
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} error={testError} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(errorMessage)).toBeTruthy();
    expect(screen.queryAllByTestId('warning-DRAFT')).toHaveLength(0);
    expect(screen.queryAllByTestId('warning-PUBLISHED')).toHaveLength(0);
    expect(screen.queryAllByTestId('warning-TODO')).toHaveLength(0);
    expect(screen.queryAllByTestId('warning-EXPIRED')).toHaveLength(0);
  });

  it('should show as initial loading', () => {
    const defaultProps = getDefaultProps();
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} warnings={[]} isLoading />
      </WarningsThemeProvider>,
    );

    expect(screen.getByTestId('loading-bar')).toBeTruthy();
    expect(screen.queryAllByTestId('warning-DRAFT')).toHaveLength(0);
    expect(screen.queryAllByTestId('warning-PUBLISHED')).toHaveLength(0);
    expect(screen.queryAllByTestId('warning-TODO')).toHaveLength(0);
    expect(screen.queryAllByTestId('warning-EXPIRED')).toHaveLength(0);
  });

  it('should show as loading when warnings are present', () => {
    const defaultProps = getDefaultProps();
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} isLoading />
      </WarningsThemeProvider>,
    );

    expect(screen.getByTestId('loading-bar')).toBeTruthy();
    expect(screen.getByLabelText('warninglist DRAFT')).toBeTruthy();
    expect(screen.getByLabelText('warninglist PUBLISHED')).toBeTruthy();
    expect(screen.getByLabelText('warninglist TODO')).toBeTruthy();
    expect(screen.getByLabelText('warninglist EXPIRED')).toBeTruthy();
  });

  it('should be able to select a warning', () => {
    const defaultProps = getDefaultProps();
    const onSelectWarning = jest.fn();

    render(
      <WarningsThemeProvider>
        <PublicWarningList
          {...defaultProps}
          onSelectWarning={onSelectWarning}
        />
      </WarningsThemeProvider>,
    );

    const listItem = screen.getByRole('button', {
      name: `warning item for ${warningList[0].id}`,
    });
    fireEvent.click(listItem);
    expect(onSelectWarning).toHaveBeenCalledWith(warningList[0]);

    const listItem2 = screen.getByRole('button', {
      name: `warning item for ${warningList[1].id}`,
    });
    fireEvent.click(listItem2);
    expect(onSelectWarning).toHaveBeenCalledWith(warningList[1]);
  });

  it('should be able to create a new public warning', () => {
    const defaultProps = getDefaultProps();
    const onCreateWarning = jest.fn();
    render(
      <WarningsThemeProvider>
        <PublicWarningList
          {...defaultProps}
          onCreateWarning={onCreateWarning}
        />
      </WarningsThemeProvider>,
    );

    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-list-button_create'),
      ),
    );
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('public-warning-title').toUpperCase(),
      ),
    );

    expect(onCreateWarning).toHaveBeenCalledWith('public');
  });

  it('should be able to create a new sigmet warning', () => {
    const defaultProps = getDefaultProps();
    const onCreateWarning = jest.fn();
    render(
      <WarningsThemeProvider>
        <PublicWarningList
          {...defaultProps}
          onCreateWarning={onCreateWarning}
        />
      </WarningsThemeProvider>,
    );

    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-list-button_create'),
      ),
    );
    fireEvent.click(screen.getByText(BUTTON_SIGMET));

    expect(onCreateWarning).toHaveBeenCalledWith('sigmet');
  });
  it('should be able to create a new airmet warning', () => {
    const defaultProps = getDefaultProps();
    const onCreateWarning = jest.fn();
    render(
      <WarningsThemeProvider>
        <PublicWarningList
          {...defaultProps}
          onCreateWarning={onCreateWarning}
        />
      </WarningsThemeProvider>,
    );

    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-list-button_create'),
      ),
    );
    fireEvent.click(screen.getByText(BUTTON_AIRMET));

    expect(onCreateWarning).toHaveBeenCalledWith('airmet');
  });
  it('should show active warning', () => {
    const defaultProps = getDefaultProps();
    const props = {
      onCreateWarning: jest.fn(),
      selectedWarningId: warningList[0].id,
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} {...props} />
      </WarningsThemeProvider>,
    );

    expect(
      screen.getByRole('button', {
        name: `warning item for ${warningList[0].id}`,
      }).classList,
    ).toContain('Mui-selected');
  });

  it('should be able to edit a draft warning', () => {
    const defaultProps = getDefaultProps();
    const onEditWarning = jest.fn();
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} onEditWarning={onEditWarning} />
      </WarningsThemeProvider>,
    );

    const optionsButton = screen.getAllByRole('button', {
      name: translateKeyOutsideComponents('warning-list-button-options'),
    })[7];
    fireEvent.click(optionsButton);
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    );
    expect(onEditWarning).toHaveBeenCalledWith(warningList[7]);
  });

  it('should not show edit option if warning is not a draft', () => {
    const defaultProps = getDefaultProps();
    const onSelectWarning = jest.fn();
    render(
      <WarningsThemeProvider>
        <PublicWarningList
          {...defaultProps}
          onSelectWarning={onSelectWarning}
        />
      </WarningsThemeProvider>,
    );

    const optionsButton = screen.getAllByRole('button', {
      name: translateKeyOutsideComponents('warning-list-button-options'),
    })[0];
    fireEvent.click(optionsButton);

    expect(
      screen.queryByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    ).toBeFalsy();
  });

  it('should be able to delete a draft warning', () => {
    const defaultProps = getDefaultProps();
    const onDeleteWarning = jest.fn();
    render(
      <WarningsThemeProvider>
        <PublicWarningList
          {...defaultProps}
          onDeleteWarning={onDeleteWarning}
        />
      </WarningsThemeProvider>,
    );

    const optionsButton = screen.getAllByRole('button', {
      name: translateKeyOutsideComponents('warning-list-button-options'),
    })[7];
    fireEvent.click(optionsButton);
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-delete'),
      ),
    );
    expect(onDeleteWarning).toHaveBeenCalledWith(
      warningList[7].id,
      warningList[7].editor,
    );
  });

  it('should not show delete option if warning is published', () => {
    const defaultProps = getDefaultProps();
    const onDeleteWarning = jest.fn();
    render(
      <WarningsThemeProvider>
        <PublicWarningList
          {...defaultProps}
          onDeleteWarning={onDeleteWarning}
        />
      </WarningsThemeProvider>,
    );

    const optionsButton = screen.getAllByRole('button', {
      name: translateKeyOutsideComponents('warning-list-button-options'),
    })[10];
    fireEvent.click(optionsButton);

    expect(
      screen.queryByText(
        translateKeyOutsideComponents('object-manager-button-delete'),
      ),
    ).toBeFalsy();
  });

  it('should show avatar if editor for that warning', () => {
    const defaultProps = getDefaultProps([warningList[7]]);
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText('UN')).toBeTruthy();
  });

  it('should show no avatar if no editor for that warning', () => {
    const defaultProps = getDefaultProps();
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} warnings={[warningList[9]]} />
      </WarningsThemeProvider>,
    );

    expect(screen.queryByTestId('avatar')).toBeFalsy();
  });

  it('be able to see last updated time and trigger a refetch', () => {
    const defaultProps = getDefaultProps();
    const props = {
      warnings: [warningList[9]],
      onRefetchWarnings: jest.fn(),
      lastUpdatedTime: '10:23',
    };
    const t: TFunction = jest.fn((key: string, options?: { time?: string }) => {
      if (key === 'shared-last-updated' && options?.time) {
        return `Last updated ${options.time}`;
      }
      return key;
    }) as unknown as TFunction;
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} {...props} />
      </WarningsThemeProvider>,
    );
    expect(props.onRefetchWarnings).toHaveBeenCalledTimes(0);
    expect(
      screen.getByText(getLastUpdateTitle(props.lastUpdatedTime, t)),
    ).toBeInTheDocument();

    fireEvent.click(screen.getByTestId('refresh-button'));
    expect(props.onRefetchWarnings).toHaveBeenCalledTimes(1);
  });

  it('should be able to use a review warning', () => {
    const defaultProps = getDefaultProps();
    const props = {
      onRefetchWarnings: jest.fn(),
      lastUpdatedTime: '10:23',
      onEditWarning: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} {...props} />
      </WarningsThemeProvider>,
    );
    const optionsButton = screen.getAllByRole('button', {
      name: translateKeyOutsideComponents('warning-list-button-options'),
    })[0];
    fireEvent.click(optionsButton);
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-list-button-use'),
      ),
    );

    expect(props.onEditWarning).toHaveBeenCalledWith(
      getWarningWithProposalId(warningList[0]),
    );
  });

  it('should be able to delete a review warning', () => {
    const defaultProps = getDefaultProps();
    const props = {
      onRefetchWarnings: jest.fn(),
      lastUpdatedTime: '10:23',
      onDeleteReviewWarning: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} {...props} />
      </WarningsThemeProvider>,
    );
    const optionsButton = screen.getAllByRole('button', {
      name: translateKeyOutsideComponents('warning-list-button-options'),
    })[0];
    fireEvent.click(optionsButton);
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-delete'),
      ),
    );

    expect(props.onDeleteReviewWarning).toHaveBeenCalledWith(warningList[0]);
  });

  it('should be able to duplicate a draft warning', () => {
    const defaultProps = getDefaultProps();
    const onEditWarning = jest.fn();
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} onEditWarning={onEditWarning} />
      </WarningsThemeProvider>,
    );

    const optionsButton = screen.getAllByRole('button', {
      name: translateKeyOutsideComponents('warning-list-button-options'),
    })[7];
    fireEvent.click(optionsButton);
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-list-button-duplicate'),
      ),
    );
    expect(onEditWarning).toHaveBeenCalledWith(
      getWarningWithoutIdStatusEditor(warningList[7]),
    );
  });

  it('should be able to duplicate a published warning', () => {
    const defaultProps = getDefaultProps();
    const onEditWarning = jest.fn();
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} onEditWarning={onEditWarning} />
      </WarningsThemeProvider>,
    );

    const optionsButton = screen.getAllByRole('button', {
      name: translateKeyOutsideComponents('warning-list-button-options'),
    })[10];
    fireEvent.click(optionsButton);
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-list-button-duplicate'),
      ),
    );
    expect(onEditWarning).toHaveBeenCalledWith(
      getWarningWithoutIdStatusEditor(warningList[10]),
    );
  });

  it('should be able to duplicate an expired warning', () => {
    const defaultProps = getDefaultProps();
    const onEditWarning = jest.fn();
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} onEditWarning={onEditWarning} />
      </WarningsThemeProvider>,
    );

    const optionsButton = screen.getAllByRole('button', {
      name: translateKeyOutsideComponents('warning-list-button-options'),
    })[14];
    fireEvent.click(optionsButton);
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-list-button-duplicate'),
      ),
    );
    expect(onEditWarning).toHaveBeenCalledWith(
      getWarningWithoutIdStatusEditor(warningList[14]),
    );
  });

  it('should be able to duplicate a withdrawn warning', () => {
    const defaultProps = getDefaultProps();
    const onEditWarning = jest.fn();
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} onEditWarning={onEditWarning} />
      </WarningsThemeProvider>,
    );

    const optionsButton = screen.getAllByRole('button', {
      name: translateKeyOutsideComponents('warning-list-button-options'),
    })[13];
    fireEvent.click(optionsButton);
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-list-button-duplicate'),
      ),
    );
    expect(onEditWarning).toHaveBeenCalledWith(
      getWarningWithoutIdStatusEditor(warningList[13]),
    );
  });

  it('should not show duplicate option if warning is a proposal', () => {
    const defaultProps = getDefaultProps();
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} />
      </WarningsThemeProvider>,
    );

    const optionsButton = screen.getAllByRole('button', {
      name: translateKeyOutsideComponents('warning-list-button-options'),
    })[0];
    fireEvent.click(optionsButton);

    expect(
      screen.queryByText(
        translateKeyOutsideComponents('warning-list-button-duplicate'),
      ),
    ).toBeFalsy();
  });

  it('should be able to update a published warning', () => {
    const defaultProps = getDefaultProps();
    const onEditWarning = jest.fn();
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} onEditWarning={onEditWarning} />
      </WarningsThemeProvider>,
    );

    const optionsButton = screen.getAllByRole('button', {
      name: translateKeyOutsideComponents('warning-list-button-options'),
    })[10];
    fireEvent.click(optionsButton);
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-list-button-update'),
      ),
    );
    expect(onEditWarning).toHaveBeenCalledWith(
      getWarningWithLinkedToId(warningList[10]),
    );
  });

  it('should be able to withdraw a published warning', () => {
    const defaultProps = getDefaultProps();
    const onWithdrawWarning = jest.fn();
    render(
      <WarningsThemeProvider>
        <PublicWarningList
          {...defaultProps}
          onWithdrawWarning={onWithdrawWarning}
        />
      </WarningsThemeProvider>,
    );

    const optionsButton = screen.getAllByRole('button', {
      name: translateKeyOutsideComponents('warning-list-button-options'),
    })[10];
    fireEvent.click(optionsButton);
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-list-button-withdraw'),
      ),
    );
    expect(onWithdrawWarning).toHaveBeenCalledWith(warningList[10]);
  });

  it('should show expire option for a published warning as disabled', () => {
    const defaultProps = getDefaultProps();
    const onEditWarning = jest.fn();
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...defaultProps} onEditWarning={onEditWarning} />
      </WarningsThemeProvider>,
    );

    const optionsButton = screen.getAllByRole('button', {
      name: translateKeyOutsideComponents('warning-list-button-options'),
    })[10];
    fireEvent.click(optionsButton);
    expect(
      screen
        .getByRole('menuitem', {
          name: translateKeyOutsideComponents('warning-list-button-expire'),
        })
        .getAttribute('aria-disabled'),
    ).toBeTruthy();
  });

  it('should unexpand and expand list sections', async () => {
    const defaultProps = getDefaultProps();
    const setExpandedSections = jest.fn((newState) => {
      defaultProps.expandedSections = newState;
    });

    const { rerender } = render(
      <WarningsThemeProvider>
        <PublicWarningList
          {...defaultProps}
          setExpandedSections={setExpandedSections}
        />
      </WarningsThemeProvider>,
    );

    expect(screen.getAllByTestId('warning-TODO')).toHaveLength(5);
    const header = screen.getByRole('button', { name: '5 Reviews' });
    fireEvent.click(header);
    expect(setExpandedSections).toHaveBeenCalledWith({
      ...defaultProps.expandedSections,
      TODO: false,
    });

    rerender(
      <WarningsThemeProvider>
        <PublicWarningList
          {...defaultProps}
          expandedSections={{ ...defaultProps.expandedSections, TODO: false }}
          setExpandedSections={setExpandedSections}
        />
      </WarningsThemeProvider>,
    );

    await waitFor(() =>
      expect(screen.queryAllByTestId('warning-TODO')).toHaveLength(0),
    );

    const header2 = screen.getByRole('button', { name: '5 Reviews' });
    fireEvent.click(header2);
    expect(setExpandedSections).toHaveBeenCalledWith({
      ...defaultProps.expandedSections,
      TODO: true,
    });

    // Re-render with updated expandedSections state
    rerender(
      <WarningsThemeProvider>
        <PublicWarningList
          {...defaultProps}
          expandedSections={{ ...defaultProps.expandedSections, TODO: true }}
          setExpandedSections={setExpandedSections}
        />
      </WarningsThemeProvider>,
    );

    await waitFor(() =>
      expect(screen.getAllByTestId('warning-TODO')).toHaveLength(5),
    );
  });

  it('should first select public, then aviation warnings when domain filter is used', () => {
    const defaultProps = getDefaultProps();
    const onSelectWarning = jest.fn();

    const { rerender } = render(
      <WarningsThemeProvider>
        <PublicWarningList
          {...defaultProps}
          onSelectWarning={onSelectWarning}
        />
      </WarningsThemeProvider>,
    );

    const listItem = screen.getByRole('button', {
      name: `warning item for ${warningList[0].id}`,
    });
    fireEvent.click(listItem);
    expect(onSelectWarning).toHaveBeenLastCalledWith(warningList[0]);

    const listItem2 = screen.getByRole('button', {
      name: `warning item for ${warningList[1].id}`,
    });
    fireEvent.click(listItem2);
    expect(onSelectWarning).toHaveBeenLastCalledWith(warningList[1]);

    const { result } = renderHook(() =>
      useWarningFilters(combinedWarnings, () => {}, {
        ...initialFilters,
        domain: { aviation: true, public: false, maritime: false },
        incident: { [emptyFilterId]: true, '-1': true },
      }),
    );

    const updatedProps = {
      ...defaultProps,
      warnings: combinedWarnings.filter(result.current.byFilterState),
      filterHookState: result.current,
    };

    rerender(
      <WarningsThemeProvider>
        <PublicWarningList
          {...updatedProps}
          onSelectWarning={onSelectWarning}
        />
      </WarningsThemeProvider>,
    );

    const aviationListItem = screen.getByRole('button', {
      name: `warning item for ${sigmetList[0].id}`,
    });
    fireEvent.click(aviationListItem);
    expect(onSelectWarning).toHaveBeenLastCalledWith(sigmetList[0]);

    const aviationListItem2 = screen.getByRole('button', {
      name: `warning item for ${airmetList[0].id}`,
    });
    fireEvent.click(aviationListItem2);
    expect(onSelectWarning).toHaveBeenLastCalledWith(airmetList[0]);
  });

  it('should show only airmetList objects when aviation domain is true and only AIR level is true', () => {
    const defaultProps = getDefaultProps();
    const onSelectWarning = jest.fn();

    const { result } = renderHook(() =>
      useWarningFilters(combinedWarnings, () => {}, {
        ...initialFilters,
        domain: { aviation: true, public: false, maritime: false },
        level: {
          ...initialFilters.level,
          SIG: false,
          AIR: true,
          unspecified: false,
        },
        incident: { unspecified: true, '-1': true, T01: true, Z01: true },
      }),
    );

    const updatedProps = {
      ...defaultProps,
      warnings: combinedWarnings.filter(result.current.byFilterState),
      filterHookState: result.current,
    };

    updatedProps.warnings.forEach((warning) => {
      expect(warning.warningDetail.level).toEqual('AIR');
    });

    render(
      <WarningsThemeProvider>
        <PublicWarningList
          {...updatedProps}
          onSelectWarning={onSelectWarning}
        />
      </WarningsThemeProvider>,
    );

    const aviationListItem = screen.getByRole('button', {
      name: `warning item for ${airmetList[0].id}`,
    });
    fireEvent.click(aviationListItem);
    expect(onSelectWarning).toHaveBeenCalledWith(airmetList[0]);
  });

  it('should show only sigmetList objects when aviation domain is true and only SIG level is true', () => {
    const defaultProps = getDefaultProps();
    const onSelectWarning = jest.fn();

    const { result } = renderHook(() =>
      useWarningFilters(combinedWarnings, () => {}, {
        ...initialFilters,
        domain: { aviation: true, public: false, maritime: false },
        level: {
          ...initialFilters.level,
          SIG: true,
          AIR: false,
          unspecified: false,
        },
        incident: { unspecified: true, '-1': true, T01: true, Z01: true },
      }),
    );

    const updatedProps = {
      ...defaultProps,
      warnings: combinedWarnings.filter(result.current.byFilterState),
      filterHookState: result.current,
    };

    updatedProps.warnings.forEach((warning) => {
      expect(warning.warningDetail.level).toEqual('SIG');
    });

    render(
      <WarningsThemeProvider>
        <PublicWarningList
          {...updatedProps}
          onSelectWarning={onSelectWarning}
        />
      </WarningsThemeProvider>,
    );

    const aviationListItem = screen.getByRole('button', {
      name: `warning item for ${sigmetList[0].id}`,
    });
    fireEvent.click(aviationListItem);
    expect(onSelectWarning).toHaveBeenCalledWith(sigmetList[0]);
  });

  it('should find the correct index for the hoveredFeatureId', () => {
    const defaultProps = getDefaultProps();

    jest.spyOn(React, 'useMemo').mockReturnValueOnce([
      { id: 'b6daea16-46db-497a-865e-9e7320ad8d12', status: 'TODO' },
      { id: 'bcbb6f28-235c-495f-b1a3-976c49079ca0', status: 'TODO' },
    ]);

    render(
      <WarningsThemeProvider>
        <PublicWarningList
          {...defaultProps}
          hoveredFeatureId="b6daea16-46db-497a-865e-9e7320ad8d12"
        />
      </WarningsThemeProvider>,
    );

    const items = [
      { id: 'b6daea16-46db-497a-865e-9e7320ad8d12', status: 'TODO' },
      { id: 'bcbb6f28-235c-495f-b1a3-976c49079ca0', status: 'TODO' },
    ];
    const hoveredIndex = items.findIndex(
      (item) => item.id === 'b6daea16-46db-497a-865e-9e7320ad8d12',
    );

    expect(hoveredIndex).toBe(0);
  });
});
