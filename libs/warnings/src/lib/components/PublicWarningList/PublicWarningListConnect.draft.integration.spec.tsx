/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import React from 'react';
import userEvent from '@testing-library/user-event';
import { dateUtils } from '@opengeoweb/shared';
import { setupServer } from 'msw/node';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WarningsApi } from '../../utils/api';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import {
  drawingList,
  requestHandlers,
  warningList,
} from '../../utils/fakeApi/index';
import { DrawingListItem } from '../../store/warningsDrawings/types';
import { PublicWarningListConnect } from './PublicWarningListConnect';
import { PublicWarningsFormDialogConnect } from '../PublicWarningsFormDialog/PublicWarningsFormDialogConnect';
import { publicWarningDialogType } from '../../store/publicWarningForm/utils';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store';

jest.mock(
  'react-virtualized-auto-sizer',
  () =>
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ({ children }: any): React.ReactNode =>
      children({ height: 1800, width: 600 }),
);

const server = setupServer(...requestHandlers);

const createApi = (): WarningsApi => {
  return {
    ...createFakeApi(),
    getDrawings: (): Promise<{ data: DrawingListItem[] }> => {
      return new Promise((resolve) => {
        resolve({ data: drawingList });
      });
    },
  };
};

describe('src/components/PublicWarningList/PublicWarningListConnectIntegration', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });
  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('should open a draft public warning in readonly mode', async () => {
    const store = createMockStore();
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });
    // open DRAFT warning dialog
    const warningDRAFT = await screen.findByRole('button', {
      name: `warning item for ${warningList[7].id}`,
    });
    await userEvent.click(warningDRAFT);

    expect(screen.getByLabelText('Switch mode')).toBeTruthy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeFalsy();
  });

  it('should open a draft warning in edit mode if you are editor and allow making an update', async () => {
    const store = createMockStore();
    // Mock date so validity validation succeeds
    const now = '2023-12-05T14:00:00Z';
    jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());
    jest.spyOn(dateUtils, 'getNowUtc').mockReturnValue(new Date(now));
    jest.spyOn(dateUtils, 'utc').mockReturnValue(new Date(now));
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });
    await screen.findByRole('button', {
      name: `warning item for ${warningList[7].id}`,
    });

    const options = await screen.findAllByLabelText('Options');
    fireEvent.click(options[7]);
    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    ).toBeTruthy();
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    );

    expect(screen.getByLabelText('Switch mode')).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeTruthy();

    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    );

    // Snackbar should be shown
    await screen.findByText(
      translateKeyOutsideComponents('warning-save-succes'),
    );

    // Dialog should still be open as we've only saved
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
  });

  it('should open a draft warning in view mode if you not are editor', async () => {
    const store = createMockStore();
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    await screen.findByRole('button', {
      name: `warning item for ${warningList[8].id}`,
    });

    const options = await screen.findAllByLabelText('Options');
    fireEvent.click(options[8]);
    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    ).toBeTruthy();
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
    expect(screen.getByLabelText('Switch mode')).toBeTruthy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeFalsy();
  });
});
