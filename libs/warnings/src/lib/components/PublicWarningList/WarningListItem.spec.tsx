/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { CancelSigmet, ProductStatus } from '@opengeoweb/api';
import WarningListItem from './WarningListItem';
import {
  Warning,
  PublicWarningStatus,
  SigmetWarning,
} from '../../store/publicWarningForm/types';

describe('WarningListItem', () => {
  const warning: Warning = {
    id: '1',
    warningDetail: {
      level: 'moderate',
      phenomenon: 'phenomenon',
      areas: [],
      validFrom: '',
      validUntil: '',
      probability: 0,
      descriptionOriginal: '',
      descriptionTranslation: '',
    },
    status: PublicWarningStatus.PUBLISHED,
    type: 'public',
  };

  it('should change background color if highlighted is true', () => {
    render(
      <WarningListItem
        warning={warning}
        activeWarningId=""
        highlightedItem={true}
        onSelectWarning={jest.fn()}
        onEditWarning={jest.fn()}
        onDeleteWarning={jest.fn()}
        onDeleteReviewWarning={jest.fn()}
        onExpireWarning={jest.fn()}
        onWithdrawWarning={jest.fn()}
      />,
    );

    const listItem = screen.getByTestId(
      `warning-${PublicWarningStatus.PUBLISHED}`,
    );
    expect(listItem).toHaveStyle(
      'background-color: geowebColors.cards.cardContainerMouseOver',
    );
  });

  it('should not change background color if highlighted is false', () => {
    render(
      <WarningListItem
        warning={warning}
        activeWarningId=""
        highlightedItem={false}
        onSelectWarning={jest.fn()}
        onEditWarning={jest.fn()}
        onDeleteWarning={jest.fn()}
        onDeleteReviewWarning={jest.fn()}
        onExpireWarning={jest.fn()}
        onWithdrawWarning={jest.fn()}
      />,
    );

    const listItem = screen.getByTestId(
      `warning-${PublicWarningStatus.PUBLISHED}`,
    );
    expect(listItem).toHaveStyle(
      'background-color: geowebColors.cards.cardContainer',
    );
  });

  it('should call onSelectWarning when clicked', () => {
    const onSelectWarning = jest.fn();
    render(
      <WarningListItem
        warning={warning}
        activeWarningId=""
        highlightedItem={false}
        onSelectWarning={onSelectWarning}
        onEditWarning={jest.fn()}
        onDeleteWarning={jest.fn()}
        onDeleteReviewWarning={jest.fn()}
        onExpireWarning={jest.fn()}
        onWithdrawWarning={jest.fn()}
      />,
    );

    const listItem = screen.getByRole('button', {
      name: `warning item for ${warning.id}`,
    });
    fireEvent.click(listItem);
    expect(onSelectWarning).toHaveBeenCalledWith(warning);
  });

  describe('renderStatusIcon', () => {
    const mockBaseWarning = {
      id: '1',
      productStatus: ProductStatus.PUBLISHED,
      warningDetail: {
        phenomenon: '',
        areas: [],
        validFrom: '',
        validUntil: '',
        level: '',
        probability: 0,
        descriptionOriginal: '',
        descriptionTranslation: '',
      },
    };
    const mockPublicWarning: Warning = {
      ...mockBaseWarning,
      type: 'public',
    };

    it('should render CNL status tag when productStatus is CANCELLED', () => {
      const warning: SigmetWarning = {
        ...mockBaseWarning,
        productStatus: ProductStatus.CANCELLED,
        sigmetAirmetDetails: {
          validDateStartOfSigmetToCancel: '',
          validDateEndOfSigmetToCancel: '',
          status: ProductStatus.PUBLISHED,
          validDateStart: '',
          validDateEnd: '',
          firName: '',
          locationIndicatorMWO: '',
          locationIndicatorATSU: '',
          locationIndicatorATSR: '',
        } as CancelSigmet,
        type: 'sigmet',
      };
      render(<WarningListItem warning={warning} activeWarningId="" />);
      expect(screen.getByText('Cnld')).toBeInTheDocument();
    });

    it('should render CNL status tag when sigmetAirmetDetails.cancelsSigmetSequenceId is present', () => {
      const warning: SigmetWarning = {
        ...mockBaseWarning,
        sigmetAirmetDetails: {
          cancelsSigmetSequenceId: '123',
          validDateStartOfSigmetToCancel: '',
          validDateEndOfSigmetToCancel: '',
          status: ProductStatus.PUBLISHED,
          validDateStart: '',
          validDateEnd: '',
          firName: '',
          locationIndicatorMWO: '',
          locationIndicatorATSU: '',
          locationIndicatorATSR: '',
        } as CancelSigmet,
        type: 'sigmet',
      };
      render(<WarningListItem warning={warning} activeWarningId="" />);
      expect(screen.getByText('Cnls 123')).toBeInTheDocument();
    });

    it('should render Pub status tag when status is PUBLISHED and not cancelled', () => {
      render(
        <WarningListItem warning={mockPublicWarning} activeWarningId="" />,
      );
      expect(screen.getByText('Pub')).toBeInTheDocument();
    });

    it('should render Wdn status tag when status is WITHDRAWN', () => {
      const warning = {
        ...mockPublicWarning,
        status: PublicWarningStatus.WITHDRAWN,
      };
      render(<WarningListItem warning={warning} activeWarningId="" />);
      expect(screen.getByText('Wdn')).toBeInTheDocument();
    });

    it('should render Exp status tag when status is EXPIRED', () => {
      const warning = {
        ...mockPublicWarning,
        status: PublicWarningStatus.EXPIRED,
      };
      render(<WarningListItem warning={warning} activeWarningId="" />);
      expect(screen.getByText('Exp')).toBeInTheDocument();
    });

    it('should render Edit icon when status is DRAFT', () => {
      const warning = {
        ...mockPublicWarning,
        status: PublicWarningStatus.DRAFT,
      };
      render(<WarningListItem warning={warning} activeWarningId="" />);
      expect(screen.getByTestId('EditPencilIcon')).toBeInTheDocument();
    });
  });
});
