/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, screen, waitFor } from '@testing-library/react';
import React from 'react';
import userEvent from '@testing-library/user-event';
import i18n from 'i18next';
import { setupServer } from 'msw/node';
import { http, HttpResponse } from 'msw';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WarningsApi } from '../../utils/api';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import {
  lengthWarningListWithoutExpiredWithdrawn,
  requestHandlers,
  warningList,
} from '../../utils/fakeApi/index';
import { PublicWarningListConnect } from './PublicWarningListConnect';
import { PublicWarningsFormDialogConnect } from '../PublicWarningsFormDialog/PublicWarningsFormDialogConnect';
import { publicWarningDialogType } from '../../store/publicWarningForm/utils';
import { PublicWarningStatus } from '../../store/publicWarningForm/types';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store';

jest.mock(
  'react-virtualized-auto-sizer',
  () =>
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ({ children }: any): React.ReactNode =>
      children({ height: 1800, width: 600 }),
);

const server = setupServer(...requestHandlers);

describe('src/components/PublicWarningList/PublicWarningListConnect - confirmation dialog', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });
  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('should allow deleting a review warning and show confirmation dialog', async () => {
    const store = createMockStore();
    const updateWarning = jest.fn();
    const createApi = (): WarningsApi => {
      return {
        ...createFakeApi(),
        updateWarning,
      };
    };
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );
    expect(
      screen.queryByText(translateKeyOutsideComponents('public-warning-title')),
    ).toBeFalsy();

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    // wait until all warnings have loaded
    await waitFor(() => {
      const options = screen.getAllByLabelText('Options');
      // Expired and withdrawn warnings are hidden by default
      expect(options.length).toEqual(lengthWarningListWithoutExpiredWithdrawn);
    });

    // try to delete the warning via the list
    const options = await screen.findAllByLabelText('Options');

    await userEvent.click(options[0]);
    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-delete'),
      ),
    ).toBeTruthy();
    await userEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-delete'),
      ),
    );

    expect(
      screen.getByText(
        translateKeyOutsideComponents('warning-dialog-delete-title'),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('warning-dialog-delete-description'),
      ),
    ).toBeTruthy();

    // Confirm deletion
    await userEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-dialog-delete-confirm'),
      ),
    );

    // Snackbar should be shown
    await waitFor(() => {
      expect(
        screen.getByText(
          translateKeyOutsideComponents('warning-dialog-delete-succes'),
        ),
      ).toBeTruthy();
    });

    // Warnings dialog should be closed
    await waitFor(() => {
      expect(
        store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
      ).toBeFalsy();
    });
    await waitFor(() => {
      expect(screen.queryByText(i18n.t('public-warning-title'))).toBeFalsy();
    });

    // correct api call should be made
    expect(updateWarning).toHaveBeenCalledWith(warningList[0].id, {
      ...warningList[0],
      status: PublicWarningStatus.IGNORED,
      type: 'public',
    });
  });

  it('should allow deleting a draft warning and show confirmation dialog', async () => {
    const store = createMockStore();

    const newWarningList = [...warningList];
    const deleteWarning = jest
      .fn()
      .mockImplementation((warningId: string): Promise<void> => {
        newWarningList.splice(
          newWarningList.findIndex((warning) => warning.id === warningId),
          1,
        );
        return new Promise((resolve) => {
          resolve();
        });
      });

    server.use(
      http.get('warnings', () => {
        // respond using a mocked JSON body
        return HttpResponse.json(newWarningList);
      }),
    );
    const createApi = (): WarningsApi => {
      return {
        ...createFakeApi(),
        deleteWarning,
      };
    };
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );
    expect(
      screen.queryByText(translateKeyOutsideComponents('public-warning-title')),
    ).toBeFalsy();

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    await screen.findByText('5 Drafts');
    await screen.findByText('3 Published');

    // try to delete the warning via the list
    const options = await screen.findAllByLabelText('Options');
    await userEvent.click(options[7]);
    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-delete'),
      ),
    ).toBeTruthy();
    await userEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-delete'),
      ),
    );
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('warning-dialog-delete-title'),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('warning-dialog-delete-description'),
      ),
    ).toBeTruthy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('warning-dialog-delete-islocked'),
      ),
    ).toBeFalsy();

    // Confirm deletion
    await userEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-dialog-delete-confirm'),
      ),
    );

    // Snackbar should be shown
    await waitFor(() => {
      expect(
        screen.getByText(
          translateKeyOutsideComponents('warning-dialog-delete-succes'),
        ),
      ).toBeTruthy();
    });

    // Warnings dialog should be closed
    await waitFor(() => {
      expect(
        store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
      ).toBeFalsy();
    });
    await waitFor(() => {
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('public-warning-title'),
        ),
      ).toBeFalsy();
    });
    // List should be updated
    await waitFor(() => {
      expect(screen.getByText('4 Drafts')).toBeTruthy();
    });

    expect(deleteWarning).toHaveBeenCalledWith(warningList[7].id);
  });

  it('should allow deleting a locked draft warning and show alert in confirmation dialog', async () => {
    const store = createMockStore();

    const newWarningList = [...warningList];
    const deleteWarning = jest
      .fn()
      .mockImplementation((warningId: string): Promise<void> => {
        newWarningList.splice(
          newWarningList.findIndex((warning) => warning.id === warningId),
          1,
        );
        return new Promise((resolve) => {
          resolve();
        });
      });

    server.use(
      http.get('warnings', () => {
        // respond using a mocked JSON body
        return HttpResponse.json(newWarningList);
      }),
    );
    const createApi = (): WarningsApi => {
      return {
        ...createFakeApi(),
        deleteWarning,
      };
    };
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );
    expect(
      screen.queryByText(translateKeyOutsideComponents('public-warning-title')),
    ).toBeFalsy();

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    await screen.findByText('5 Drafts');

    // try to delete the warning via the list
    const options = await screen.findAllByLabelText('Options');
    await userEvent.click(options[8]);
    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-delete'),
      ),
    ).toBeTruthy();
    await userEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-delete'),
      ),
    );
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('warning-dialog-delete-title'),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('warning-dialog-delete-description'),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('warning-dialog-delete-islocked'),
      ),
    ).toBeTruthy();

    // Confirm deletion
    await userEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-dialog-delete-confirm'),
      ),
    );

    // Snackbar should be shown
    await waitFor(() => {
      expect(
        screen.getByText(
          translateKeyOutsideComponents('warning-dialog-delete-succes'),
        ),
      ).toBeTruthy();
    });

    // Warnings dialog should be closed
    await waitFor(() => {
      expect(
        store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
      ).toBeFalsy();
    });
    await waitFor(() => {
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('public-warning-title'),
        ),
      ).toBeFalsy();
    });
    // List should be updated
    await waitFor(() => {
      expect(screen.getByText('4 Drafts')).toBeTruthy();
    });

    expect(deleteWarning).toHaveBeenCalledWith(warningList[8].id);
  });
});
