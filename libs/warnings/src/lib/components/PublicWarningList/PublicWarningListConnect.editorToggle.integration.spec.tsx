/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  fireEvent,
  render,
  screen,
  waitFor,
  within,
} from '@testing-library/react';
import React from 'react';
import { setupServer } from 'msw/node';
import userEvent from '@testing-library/user-event';
import { http, HttpResponse } from 'msw';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WarningsApi } from '../../utils/api';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import {
  lengthWarningListWithoutExpiredWithdrawn,
  makeWarningsHandler,
  requestHandlers,
  warningList,
} from '../../utils/fakeApi/index';
import { PublicWarningListConnect } from './PublicWarningListConnect';
import { PublicWarningsFormDialogConnect } from '../PublicWarningsFormDialog/PublicWarningsFormDialogConnect';
import { publicWarningDialogType } from '../../store/publicWarningForm/utils';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store/store';

jest.mock(
  'react-virtualized-auto-sizer',
  () =>
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ({ children }: any): React.ReactNode =>
      children({ height: 1800, width: 600 }),
);

const server = setupServer(...requestHandlers);

describe('src/components/PublicWarningList/PublicWarningListConnectIntegration  - editorToggle', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });
  it('should switch from view mode to editor mode and back, and reset form if changes made', async () => {
    const store = createMockStore();
    const createMockApi = (): WarningsApi => {
      return {
        ...createFakeApi(),
      };
    };
    const selectedWarning = warningList.find(
      (w) => w.id === '078e6676-937d-11ee-b619-0ab3cf8a98c1',
    )!;

    server.use(makeWarningsHandler([selectedWarning]));

    render(
      <WarningsThemeStoreProvider createApi={createMockApi} store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    // wait until all warnings have loaded
    await waitFor(() => {
      const options = screen.getAllByLabelText('Options');
      // Expired and withdrawn warnings are hidden by default
      expect(options.length).toEqual(1);
    });

    const options = await screen.findAllByLabelText('Options');

    fireEvent.click(options[0]);
    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    ).toBeTruthy();
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
    expect(screen.getByLabelText('Switch mode')).toBeTruthy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeFalsy();

    const mode = within(screen.getByLabelText('Switch mode')).getByRole(
      'checkbox',
    );
    await userEvent.click(mode);

    // confirm to take over
    await userEvent.click(
      await screen.findByTestId('confirmationDialog-confirm'),
    );

    // check snackbar
    await screen.findByText(
      translateKeyOutsideComponents('warning-editor-add-editor'),
    );
    server.use(
      makeWarningsHandler([{ ...selectedWarning, editor: 'user.name' }]),
    );

    // check edit mode of the form
    await screen.findByRole('button', {
      name: translateKeyOutsideComponents('warning-button-clear'),
    });
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeTruthy();
    expect(screen.getAllByText('Coastal event').length).toBe(2);

    // Make some changes
    await userEvent.click(await screen.findByLabelText('Phenomenon'));
    await userEvent.click(await screen.findByText('Rain'));

    expect(screen.getAllByText('Coastal event').length).toBe(1);

    expect(screen.getByText('Rain')).toBeTruthy();

    // switch back
    await userEvent.click(mode);

    expect(
      await screen.findByText(
        translateKeyOutsideComponents('warning-view-mode-description'),
      ),
    ).toBeTruthy();

    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-view-mode-confirm'),
      ),
    );

    // check snackbar
    await screen.findByText(
      translateKeyOutsideComponents('warning-editor-remove-editor'),
    );
    server.use(
      makeWarningsHandler([{ ...selectedWarning, editor: undefined }]),
    );

    // check view mode of the form
    expect(screen.queryByText('Rain')).toBeFalsy();
    expect(screen.getAllByText('Coastal event').length).toBe(2);
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeFalsy();
  });

  it('should show error message when editor toggle fails', async () => {
    const store = createMockStore();
    server.use(makeWarningsHandler(warningList));

    server.use(
      http.post(/^.*\/warnings\/(.*)$/, () => {
        return HttpResponse.json({ message: 'toggle error' }, { status: 500 });
      }),
    );

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    // wait until all warnings have loaded
    await waitFor(() => {
      const options = screen.getAllByLabelText('Options');
      // Expired and withdrawn warnings are hidden by default
      expect(options.length).toEqual(lengthWarningListWithoutExpiredWithdrawn);
    });

    const options = await screen.findAllByLabelText('Options');
    fireEvent.click(options[7]);
    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    ).toBeTruthy();
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
    expect(screen.getByLabelText('Switch mode')).toBeTruthy();
    // check that form is in edit mode
    await screen.findByRole('button', {
      name: translateKeyOutsideComponents('warning-button-clear'),
    });

    const mode = within(screen.getByLabelText('Switch mode')).getByRole(
      'checkbox',
    );
    await userEvent.click(mode);

    // check error messsage
    await waitFor(
      () => {
        screen.getByText('toggle error');
      },
      { timeout: 5000 },
    );

    // check that form is still in edit mode
    await screen.findByRole('button', {
      name: translateKeyOutsideComponents('warning-button-clear'),
    });
  });
});
