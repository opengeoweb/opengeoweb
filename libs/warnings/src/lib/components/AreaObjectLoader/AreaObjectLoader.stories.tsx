/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { Box } from '@mui/material';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { WarningsThemeProvider } from '../Providers/Providers';
import { drawingList } from '../../utils/fakeApi/index';
import { DrawingListItem } from '../../store/warningsDrawings/types';
import { AreaObjectLoader } from './AreaObjectLoader';
import { areaObjectLoaderType } from '../../store/warningsDrawings/utils';

export default {
  title: 'components/AreaObjectLoader',
};

const extraLongItem: DrawingListItem = {
  id: 'longest292002',
  objectName: 'Reallylongnonclippablenameovermultiplelines',
  lastUpdatedTime: '2023-09-20T12:34:27Z',
  scope: 'global',
  geoJSON: {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        properties: {
          fill: '#ff7800',
          'fill-opacity': 0.2,
          stroke: '#ff7800',
          'stroke-width': 2,
          'stroke-opacity': 1,
          selectionType: 'poly',
        },
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [3.357673379394467, 54.021903403974555],
              [6.600428193305415, 54.583753881732186],
              [6.642909697417786, 50.7929736738295],
              [3.357673379394467, 54.021903403974555],
            ],
          ],
        },
      },
    ],
  },
  keywords: 'Objects',
};

const filterProps = {
  searchQuery: '',
  filters: [
    {
      label: 'Coastal Districts',
      id: 'Coastal Districts',
      isSelected: false,
      isDisabled: false,
    },
    {
      label: 'Objects',
      id: 'Objects',
      isSelected: false,
      isDisabled: false,
    },
    {
      label: 'Countries',
      id: 'Countries',
      isSelected: false,
      isDisabled: false,
    },
  ],
};

export const AreaObjectLoaderSmall = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '300px', height: '500px' }}>
        <AreaObjectLoader
          onClose={(): void => {}}
          {...filterProps}
          isOpen
          drawingListItems={[extraLongItem, ...drawingList]}
          size={{ width: 160, height: 300 }}
        />
      </Box>
    </WarningsThemeProvider>
  );
};

AreaObjectLoaderSmall.tags = ['snapshot'];

export const AreaObjectLoaderLight = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <AreaObjectLoader
          onClose={(): void => {}}
          {...filterProps}
          isOpen
          drawingListItems={drawingList}
        />
      </Box>
    </WarningsThemeProvider>
  );
};

AreaObjectLoaderLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/668fc6410244e5e7cb10b221',
    },
  ],
};

AreaObjectLoaderLight.tags = ['snapshot'];

export const AreaObjectLoaderDark = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <AreaObjectLoader
          onClose={(): void => {}}
          {...filterProps}
          isOpen
          drawingListItems={drawingList}
        />
      </Box>
    </WarningsThemeProvider>
  );
};

AreaObjectLoaderDark.tags = ['snapshot'];

export const AreaObjectLoaderLightNoResults = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <AreaObjectLoader
          onClose={(): void => {}}
          {...filterProps}
          isOpen
          drawingListItems={[]}
        />
      </Box>
    </WarningsThemeProvider>
  );
};

AreaObjectLoaderLightNoResults.tags = ['snapshot'];

export const AreaObjectLoaderDarkNoResults = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <AreaObjectLoader
          onClose={(): void => {}}
          {...filterProps}
          isOpen
          drawingListItems={[]}
        />
      </Box>
    </WarningsThemeProvider>
  );
};

AreaObjectLoaderDarkNoResults.tags = ['snapshot'];

export const AreaObjectLoaderLoading = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <AreaObjectLoader
          onClose={(): void => {}}
          {...filterProps}
          isOpen
          drawingListItems={drawingList}
          isLoading
        />
      </Box>
    </WarningsThemeProvider>
  );
};

export const AreaObjectLoaderErrorLight = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <AreaObjectLoader
          onClose={(): void => {}}
          {...filterProps}
          isOpen
          drawingListItems={drawingList}
          error={{ error: 'Something went wrong', type: areaObjectLoaderType }}
        />
      </Box>
    </WarningsThemeProvider>
  );
};

export const AreaObjectLoaderErrorDark = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <AreaObjectLoader
          onClose={(): void => {}}
          {...filterProps}
          isOpen
          drawingListItems={drawingList}
          error={{ error: 'Something went wrong', type: areaObjectLoaderType }}
        />
      </Box>
    </WarningsThemeProvider>
  );
};

AreaObjectLoaderErrorLight.tags = ['snapshot'];
AreaObjectLoaderErrorDark.tags = ['snapshot'];
