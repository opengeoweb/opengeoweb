/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { useDispatch } from 'react-redux';
import { uiActions } from '@opengeoweb/store';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { AreaObjectLoaderConnect } from './AreaObjectLoaderConnect';
import { areaObjectLoaderType } from '../../store/warningsDrawings/utils';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WarningsApi } from '../../utils/api';
import { fakeErrorRequest } from '../../storybookUtils/testUtils';

export default {
  title: 'components/AreaObjectLoaderConnect',
};

const AreaObjectLoaderComponentDemo: React.FC = () => {
  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(
      uiActions.setActiveMapIdForDialog({
        type: areaObjectLoaderType,
        mapId: '',
        setOpen: true,
      }),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <AreaObjectLoaderConnect />;
};

export const AreaObjectLoaderDemo = (): React.ReactElement => {
  return (
    <WarningsThemeStoreProvider>
      <AreaObjectLoaderComponentDemo />
    </WarningsThemeStoreProvider>
  );
};

export const AreaObjectLoaderDemoWithError = (): React.ReactElement => {
  const createApiWithErrorOnAreas = (): WarningsApi => ({
    ...createFakeApi(),
    getAreaKeywords: () =>
      fakeErrorRequest<{ data: string[] }>(
        'Something went wrong while getting areas, please try again.',
      ),
  });
  return (
    <WarningsThemeStoreProvider createApi={createApiWithErrorOnAreas}>
      <AreaObjectLoaderComponentDemo />
    </WarningsThemeStoreProvider>
  );
};
