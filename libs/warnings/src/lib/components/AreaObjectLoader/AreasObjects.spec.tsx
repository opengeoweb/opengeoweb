/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen } from '@testing-library/react';
import { TFunction } from 'i18next';
import { DrawingListItem } from '../../store/warningsDrawings/types';
import { WarningsThemeProvider } from '../Providers/Providers';
import {
  AreasObjects,
  getDisplayDate,
  getListInclHeaders,
} from './AreasObjects';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { testGeoJSON } from '../../storybookUtils/testUtils';

describe('src/components/AreaObjectLoader/AreasObjects', () => {
  it('should show the list of Objects and Areas', async () => {
    const props = {
      drawingListItems: [
        {
          id: '923723984872338768743',
          objectName: 'Drawing object 101',
          lastUpdatedTime: '2022-06-01T12:34:27Z',
          scope: 'user',
          keywords: 'Objects',
        },
        {
          id: '934834893283',
          objectName: 'object 89',
          lastUpdatedTime: '2022-05-26T12:34:27Z',
          scope: 'user',
          keywords: 'Objects',
        },
        {
          id: '345346456345',
          objectName: 'object 88',
          lastUpdatedTime: '2022-04-26T12:34:27Z',
          scope: 'global',
        },
      ] as DrawingListItem[],
    };

    render(
      <WarningsThemeProvider>
        <AreasObjects {...props} />
      </WarningsThemeProvider>,
    );

    // should show headers
    await screen.findByText('2 Objects');
    await screen.findByText('1 Areas');

    // should show date for objects
    await screen.findByText(props.drawingListItems[0].objectName);
    await screen.findByText(
      getDisplayDate(props.drawingListItems[0].lastUpdatedTime),
    );
    await screen.findByText(props.drawingListItems[1].objectName);
    await screen.findByText(
      getDisplayDate(props.drawingListItems[1].lastUpdatedTime),
    );

    // should not show date for areas
    await screen.findByText(props.drawingListItems[2].objectName);
    expect(
      screen.queryByText(
        getDisplayDate(props.drawingListItems[2].lastUpdatedTime),
      ),
    ).toBeFalsy();

    // should render add/remove buttons as disabled
    screen
      .getAllByText(translateKeyOutsideComponents('add-button'))
      .forEach((button) => {
        expect(button.getAttribute('disabled')).toBeDefined();
      });
  });

  it('should show no results message', async () => {
    const props = {
      drawingListItems: [] as DrawingListItem[],
    };

    render(
      <WarningsThemeProvider>
        <AreasObjects {...props} />
      </WarningsThemeProvider>,
    );

    expect(
      await screen.findByText(
        translateKeyOutsideComponents('area-object-loader-no-areas-objects'),
      ),
    );
  });

  it('should not show no results message while loading', async () => {
    const props = {
      drawingListItems: [] as DrawingListItem[],
      isLoading: true,
    };

    render(
      <WarningsThemeProvider>
        <AreasObjects {...props} />
      </WarningsThemeProvider>,
    );

    expect(
      screen.queryByText(
        translateKeyOutsideComponents('area-object-loader-no-areas-objects'),
      ),
    ).toBeFalsy();
  });

  it('should show be able to add an area', async () => {
    const props = {
      drawingListItems: [
        {
          id: '923723984872338768743',
          objectName: 'Drawing object 101',
          lastUpdatedTime: '2022-06-01T12:34:27Z',
          scope: 'user',
          keywords: 'Objects',
        },
        {
          id: '934834893283',
          objectName: 'object 89',
          lastUpdatedTime: '2022-05-26T12:34:27Z',
          scope: 'user',
          keywords: 'Objects',
        },
        {
          id: '345346456345',
          objectName: 'object 88',
          lastUpdatedTime: '2022-04-26T12:34:27Z',
          scope: 'global',
          geoJSON: testGeoJSON,
        },
      ] as DrawingListItem[],
      onAddArea: jest.fn(),
      isChangeAreaDisabled: false,
    };

    render(
      <WarningsThemeProvider>
        <AreasObjects {...props} />
      </WarningsThemeProvider>,
    );

    // should show headers
    await screen.findByText('2 Objects');
    await screen.findByText('1 Areas');

    fireEvent.click(
      screen.getAllByText(translateKeyOutsideComponents('add-button'))[2],
    );

    expect(props.onAddArea).toHaveBeenCalledWith(props.drawingListItems[2]);
  });

  it('should show be able to remove an area', async () => {
    const props = {
      drawingListItems: [
        {
          id: '923723984872338768743',
          objectName: 'Drawing object 101',
          lastUpdatedTime: '2022-06-01T12:34:27Z',
          scope: 'user',
          keywords: 'Objects',
        },
        {
          id: '934834893283',
          objectName: 'object 89',
          lastUpdatedTime: '2022-05-26T12:34:27Z',
          scope: 'user',
          keywords: 'Objects',
        },
        {
          id: '345346456345',
          objectName: 'object 88',
          lastUpdatedTime: '2022-04-26T12:34:27Z',
          scope: 'global',
          geoJSON: testGeoJSON,
        },
      ] as DrawingListItem[],
      onRemoveArea: jest.fn(),
      isChangeAreaDisabled: false,
      selectedAreas: [{ objectName: 'object 88', geoJSON: testGeoJSON }],
    };

    render(
      <WarningsThemeProvider>
        <AreasObjects {...props} />
      </WarningsThemeProvider>,
    );

    // should show headers
    await screen.findByText('2 Objects');
    await screen.findByText('1 Areas');

    fireEvent.click(
      screen.getByText(translateKeyOutsideComponents('remove-button')),
    );

    expect(props.onRemoveArea).toHaveBeenCalledWith(props.drawingListItems[2]);
  });

  it('should show added areas', async () => {
    const props = {
      drawingListItems: [
        {
          id: '923723984872338768743',
          objectName: 'Drawing object 101',
          lastUpdatedTime: '2022-06-01T12:34:27Z',
          scope: 'user',
          keywords: 'Objects',
          geoJSON: testGeoJSON,
        },
        {
          id: '934834893283',
          objectName: 'object 89',
          lastUpdatedTime: '2022-05-26T12:34:27Z',
          scope: 'user',
          keywords: 'Objects',
        },
        {
          id: '345346456345',
          objectName: 'object 88',
          lastUpdatedTime: '2022-04-26T12:34:27Z',
          scope: 'global',
          geoJSON: testGeoJSON,
        },
      ] as DrawingListItem[],
      onRemoveArea: jest.fn(),
      isChangeAreaDisabled: false,
      selectedAreas: [
        { objectName: 'object 88', geoJSON: testGeoJSON },
        { objectName: 'Drawing object 101', geoJSON: testGeoJSON },
      ],
    };

    render(
      <WarningsThemeProvider>
        <AreasObjects {...props} />
      </WarningsThemeProvider>,
    );

    // should show headers
    await screen.findByText('2 Objects');
    await screen.findByText('1 Areas');

    expect(
      screen.getAllByText(translateKeyOutsideComponents('remove-button')),
    ).toHaveLength(2);

    expect(
      screen.getAllByText(translateKeyOutsideComponents('add-button')),
    ).toHaveLength(1);
  });

  describe('getListInclHeaders', () => {
    const drawingListItems = [
      {
        id: '923723984872338768743',
        objectName: 'Drawing object 101',
        lastUpdatedTime: '2022-06-01T12:34:27Z',
        scope: 'user',
        keywords: 'Objects',
      },
      {
        id: '934834893283',
        objectName: 'object 89',
        lastUpdatedTime: '2022-05-26T12:34:27Z',
        scope: 'user',
        keywords: 'Objects',
      },
      {
        id: '345346456345',
        objectName: 'object 88',
        lastUpdatedTime: '2022-04-26T12:34:27Z',
        scope: 'global',
      },
    ] as DrawingListItem[];

    it('should create a list of objects and areas with headers', async () => {
      const t = ((key: string) => key) as unknown as TFunction;

      expect(getListInclHeaders([], t)).toEqual({
        areas: { items: [], title: '0 areas' },
        objects: { items: [], title: '0 objects' },
      });
      expect(getListInclHeaders(drawingListItems, t)).toEqual({
        areas: { items: [drawingListItems[2]], title: '1 areas' },
        objects: {
          items: [drawingListItems[0], drawingListItems[1]],
          title: '2 objects',
        },
      });
    });
  });
});
