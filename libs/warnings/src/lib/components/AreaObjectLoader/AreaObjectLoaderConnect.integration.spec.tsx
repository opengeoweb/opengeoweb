/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  fireEvent,
  render,
  screen,
  waitFor,
  within,
} from '@testing-library/react';
import { uiActions, uiTypes } from '@opengeoweb/store';
import { DEBOUNCE_TIMEOUT } from '@opengeoweb/shared';
import { setupServer } from 'msw/node';
import { AreaObjectLoaderConnect } from './AreaObjectLoaderConnect';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WarningsApi } from '../../utils/api';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { drawingList, requestHandlers } from '../../utils/fakeApi/index';
import { DrawingListItem } from '../../store/warningsDrawings/types';
import { PublicWarningsFormDialogConnect } from '../PublicWarningsFormDialog/PublicWarningsFormDialogConnect';
import { areaObjectLoaderType } from '../../store/warningsDrawings/utils';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { publicWarningDialogType } from '../../store/publicWarningForm/utils';
import { getDisplayDate } from './AreasObjects';
import { createMockStore } from '../../store/store';
import { PublicWarningStatus } from '../../store/publicWarningForm/types';

const server = setupServer(...requestHandlers);
describe('src/components/AreaObjectLoader/AreaObjectLoaderConnect', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });
  const createApi = (): WarningsApi => {
    return {
      ...createFakeApi(),
    };
  };

  const store = createMockStore();
  it('should open the loader from the public warning form and close it when closing the form', async () => {
    const mapId = 'map123';

    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      // open form
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: publicWarningDialogType,
          mapId,
          setOpen: true,
        }),
      );
    });
    await screen.findByText(
      translateKeyOutsideComponents('public-warning-title'),
    );

    fireEvent.click(
      await screen.findByText(
        translateKeyOutsideComponents('warning-button-add-object'),
      ),
    );

    await screen.findByText(`8 ${translateKeyOutsideComponents('objects')}`);
    await screen.findByText(`2 ${translateKeyOutsideComponents('areas')}`);

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    expect(
      await screen.findByText(getDisplayDate(drawingList[0].lastUpdatedTime)),
    ).toBeTruthy();

    fireEvent.click(
      within(
        screen.getByLabelText(
          translateKeyOutsideComponents('public-warning-title'),
        ),
      ).getByRole('button', {
        name: translateKeyOutsideComponents('shared-close'),
      }),
    );

    expect(
      screen.queryByText(translateKeyOutsideComponents('public-warning-title')),
    ).toBeFalsy();
    expect(
      screen.queryByText(`8 ${translateKeyOutsideComponents('objects')}`),
    ).toBeFalsy();
  });

  it('should show an error when getting the list fails', async () => {
    const mapId = 'map124';

    const createApiWithErrorOnList = (): WarningsApi => {
      return {
        ...createFakeApi(),
        getDrawings: (): Promise<{ data: DrawingListItem[] }> => {
          return new Promise((_, reject) => {
            reject(new Error(`Error fetching drawings.`));
          });
        },
      };
    };

    render(
      <WarningsThemeStoreProvider
        createApi={createApiWithErrorOnList}
        store={store}
      >
        <AreaObjectLoaderConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      // open form
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: areaObjectLoaderType,
          mapId,
          setOpen: true,
        }),
      );
    });
    // Error should be shown
    expect(await screen.findByText('Error fetching drawings.')).toBeTruthy();
    // No results message should be shown
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('area-object-loader-no-areas-objects'),
      ),
    ).toBeTruthy();
  });

  it('should show an error when getting the areas fails', async () => {
    const mapId = 'map124';

    const createApiWithErrorOnList = (): WarningsApi => {
      return {
        ...createFakeApi(),
        getAreaKeywords: (): Promise<{ data: string[] }> => {
          return new Promise((_, reject) => {
            reject(new Error(`Error fetching areas.`));
          });
        },
      };
    };

    render(
      <WarningsThemeStoreProvider
        createApi={createApiWithErrorOnList}
        store={store}
      >
        <AreaObjectLoaderConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      // open form
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: areaObjectLoaderType,
          mapId,
          setOpen: true,
        }),
      );
    });
    // Error should be shown
    expect(await screen.findByText('Error fetching areas.')).toBeTruthy();
  });

  it('should close when closing the dialog', async () => {
    const mapId = 'map124';

    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <AreaObjectLoaderConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      // open form
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: areaObjectLoaderType,
          mapId,
          setOpen: true,
        }),
      );
    });
    // List should be shown
    await screen.findByText(
      translateKeyOutsideComponents('area-object-loader-title'),
    );

    fireEvent.click(
      within(
        screen.getByLabelText(
          translateKeyOutsideComponents('area-object-loader-title'),
        ),
      ).getByRole('button', {
        name: translateKeyOutsideComponents('shared-close'),
      }),
    );

    expect(
      screen.queryByText(
        translateKeyOutsideComponents('area-object-loader-title'),
      ),
    ).toBeFalsy();
  });

  it('should be able to filter on search query and keywords', async () => {
    const mapId = 'map123';
    jest.useFakeTimers();

    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      // open form
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: publicWarningDialogType,
          mapId,
          setOpen: true,
        }),
      );
    });
    await screen.findByText(
      translateKeyOutsideComponents('public-warning-title'),
    );

    fireEvent.click(
      await screen.findByText(
        translateKeyOutsideComponents('warning-button-add-object'),
      ),
    );

    await screen.findByText(`8 ${translateKeyOutsideComponents('objects')}`);
    await screen.findByText(`2 ${translateKeyOutsideComponents('areas')}`);

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    expect(
      await screen.findByText(getDisplayDate(drawingList[0].lastUpdatedTime)),
    ).toBeTruthy();

    const searchBox = within(
      screen.getByLabelText(
        translateKeyOutsideComponents('area-object-loader-title'),
      ),
    ).getByRole('textbox', {
      name: translateKeyOutsideComponents('shared-filter-search'),
    });

    // search
    fireEvent.change(searchBox, {
      target: {
        value: 'test',
      },
    });

    await waitFor(() => jest.advanceTimersByTime(DEBOUNCE_TIMEOUT));

    await screen.findByText(`1 ${translateKeyOutsideComponents('objects')}`);
    await screen.findByText(`0 ${translateKeyOutsideComponents('areas')}`);

    // reset search
    fireEvent.change(searchBox, {
      target: {
        value: '',
      },
    });
    await waitFor(() => jest.advanceTimersByTime(DEBOUNCE_TIMEOUT));

    await screen.findByText(`8 ${translateKeyOutsideComponents('objects')}`);
    await screen.findByText(`2 ${translateKeyOutsideComponents('areas')}`);

    // toggle filter
    fireEvent.click(screen.getByText('NL'));
    await screen.findByText(`0 ${translateKeyOutsideComponents('objects')}`);
    await screen.findByText(`2 ${translateKeyOutsideComponents('areas')}`);

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should add and remove areas', async () => {
    const mapId = 'map123';

    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      // open form
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: publicWarningDialogType,
          mapId,
          setOpen: true,
        }),
      );
    });
    await screen.findByText(
      translateKeyOutsideComponents('public-warning-title'),
    );

    fireEvent.click(
      await screen.findByText(
        translateKeyOutsideComponents('warning-button-add-object'),
      ),
    );

    await screen.findByText(`8 ${translateKeyOutsideComponents('objects')}`);
    await screen.findByText(`2 ${translateKeyOutsideComponents('areas')}`);

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();

    expect(
      screen.queryAllByText(translateKeyOutsideComponents('remove-button')),
    ).toHaveLength(0);

    // add first area
    fireEvent.click(
      screen.getAllByText(translateKeyOutsideComponents('add-button'))[0],
    );
    expect(
      screen.queryAllByText(translateKeyOutsideComponents('remove-button')),
    ).toHaveLength(1);

    // add second area
    fireEvent.click(
      screen.getAllByText(translateKeyOutsideComponents('add-button'))[1],
    );
    expect(
      screen.queryAllByText(translateKeyOutsideComponents('remove-button')),
    ).toHaveLength(1);

    // remove area
    fireEvent.click(
      screen.getAllByText(translateKeyOutsideComponents('remove-button'))[0],
    );
    expect(
      screen.queryAllByText(translateKeyOutsideComponents('remove-button')),
    ).toHaveLength(0);
  });

  it('should add and remove areas for status DRAFT', async () => {
    const mapId = 'map123';
    const store = createMockStore({
      publicWarningForm: {
        warning: {
          id: '20f54a8a-937d-11ee-b619-0ab3cf8a98c1',
          lastUpdatedTime: '2023-12-05T14:46:52Z',
          status: PublicWarningStatus.DRAFT,
          warningDetail: {
            phenomenon: 'lowTemp',
            areas: [],
            validFrom: '2023-12-05T14:46:39Z',
            validUntil: '2023-12-07T20:48:39Z',
            level: 'moderate',
            probability: 80,
            descriptionOriginal: 'description original',
            descriptionTranslation: 'description translation',
          },
          type: 'public',
        },
      },
      ui: {
        dialogs: {
          [uiTypes.DialogTypes.AreaObjectLoader]: {
            isOpen: true,
            type: uiTypes.DialogTypes.AreaObjectLoader,
            activeMapId: mapId,
          },
        },
        order: [uiTypes.DialogTypes.AreaObjectLoader],
      },
    });

    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      // open form
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: publicWarningDialogType,
          mapId,
          setOpen: true,
        }),
      );
    });
    await screen.findByText(
      translateKeyOutsideComponents('public-warning-title'),
    );

    const addButtons = await screen.findAllByText(
      translateKeyOutsideComponents('warning-button-add-object'),
    );

    fireEvent.click(addButtons[0]);

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();

    expect(
      screen.queryAllByText(translateKeyOutsideComponents('remove-button')),
    ).toHaveLength(0);

    // add first area
    fireEvent.click(
      screen.getAllByText(translateKeyOutsideComponents('add-button'))[0],
    );
    expect(
      screen.queryAllByText(translateKeyOutsideComponents('remove-button')),
    ).toHaveLength(1);

    // add second area
    fireEvent.click(
      screen.getAllByText(translateKeyOutsideComponents('add-button'))[1],
    );
    expect(
      screen.queryAllByText(translateKeyOutsideComponents('remove-button')),
    ).toHaveLength(1);

    // remove area
    fireEvent.click(
      screen.getAllByText(translateKeyOutsideComponents('remove-button'))[0],
    );
    expect(
      screen.queryAllByText(translateKeyOutsideComponents('remove-button')),
    ).toHaveLength(0);
  });

  it('should add and remove areas for status DRAFT_PUBLISHED', async () => {
    const mapId = 'map123';
    const store = createMockStore({
      publicWarningForm: {
        warning: {
          id: '20f54a8a-937d-11ee-b619-0ab3cf8a98c1',
          lastUpdatedTime: '2023-12-05T14:46:52Z',
          status: PublicWarningStatus.DRAFT_PUBLISHED,
          warningDetail: {
            phenomenon: 'lowTemp',
            areas: [],
            validFrom: '2023-12-05T14:46:39Z',
            validUntil: '2023-12-07T20:48:39Z',
            level: 'moderate',
            probability: 80,
            descriptionOriginal: 'description original',
            descriptionTranslation: 'description translation',
          },
          type: 'public',
        },
      },
      ui: {
        dialogs: {
          [uiTypes.DialogTypes.AreaObjectLoader]: {
            isOpen: true,
            type: uiTypes.DialogTypes.AreaObjectLoader,
            activeMapId: mapId,
          },
        },
        order: [uiTypes.DialogTypes.AreaObjectLoader],
      },
    });

    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      // open form
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: publicWarningDialogType,
          mapId,
          setOpen: true,
        }),
      );
    });
    await screen.findByText(
      translateKeyOutsideComponents('public-warning-title'),
    );

    const addButtons = await screen.findAllByText(
      translateKeyOutsideComponents('warning-button-add-object'),
    );

    fireEvent.click(addButtons[0]);

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();

    expect(
      screen.queryAllByText(translateKeyOutsideComponents('remove-button')),
    ).toHaveLength(0);

    // add first area
    fireEvent.click(
      screen.getAllByText(translateKeyOutsideComponents('add-button'))[0],
    );
    expect(
      screen.queryAllByText(translateKeyOutsideComponents('remove-button')),
    ).toHaveLength(1);

    // add second area
    fireEvent.click(
      screen.getAllByText(translateKeyOutsideComponents('add-button'))[1],
    );
    expect(
      screen.queryAllByText(translateKeyOutsideComponents('remove-button')),
    ).toHaveLength(1);

    // remove area
    fireEvent.click(
      screen.getAllByText(translateKeyOutsideComponents('remove-button'))[0],
    );
    expect(
      screen.queryAllByText(translateKeyOutsideComponents('remove-button')),
    ).toHaveLength(0);
  });

  it('should change area for a new public warning', async () => {
    const mapId = 'map123';

    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      // open form
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: publicWarningDialogType,
          mapId,
          setOpen: true,
        }),
      );
    });
    await screen.findByText(
      translateKeyOutsideComponents('public-warning-title'),
    );

    // select phenom
    const phenomenonButton = screen.getByRole('combobox', {
      name: translateKeyOutsideComponents('warning-phenomenon'),
      hidden: true,
    });

    fireEvent.mouseDown(phenomenonButton);
    const menuItem = await screen.findByText(
      translateKeyOutsideComponents('warning-phenomenon-fog'),
    );

    fireEvent.click(menuItem);

    await waitFor(() => {
      expect(phenomenonButton.textContent).toEqual(
        translateKeyOutsideComponents('warning-phenomenon-fog'),
      );
    });

    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-no-object-selected'),
      ),
    ).toBeTruthy();

    expect(
      screen.queryByRole('dialog', {
        name: translateKeyOutsideComponents('area-object-loader-title'),
      }),
    ).toBeFalsy();

    expect(screen.queryByText(drawingList[0].objectName)).toBeFalsy();

    // select area
    fireEvent.click(
      await screen.findByText(
        translateKeyOutsideComponents('warning-button-add-object'),
      ),
    );

    expect(
      screen.getByRole('dialog', {
        name: translateKeyOutsideComponents('area-object-loader-title'),
      }),
    ).toBeTruthy();

    await screen.findByText(`8 ${translateKeyOutsideComponents('objects')}`);
    await screen.findByText(`2 ${translateKeyOutsideComponents('areas')}`);

    // click first object
    fireEvent.click(
      screen.getAllByText(translateKeyOutsideComponents('add-button'))[0],
    );

    // check if the form is updated with new area
    await screen.findAllByText(drawingList[0].objectName);
    // check phenomenon is still selected
    await waitFor(() => {
      expect(phenomenonButton.textContent).toEqual(
        translateKeyOutsideComponents('warning-phenomenon-fog'),
      );
    });
  });
});
