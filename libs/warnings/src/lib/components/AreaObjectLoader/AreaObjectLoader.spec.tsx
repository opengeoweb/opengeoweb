/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen } from '@testing-library/react';
import { WarningsThemeProvider } from '../Providers/Providers';
import { DrawingListItem } from '../../store/warningsDrawings/types';
import { areaErrorType, AreaObjectLoader } from './AreaObjectLoader';
import { areaObjectLoaderType } from '../../store/warningsDrawings/utils';
import { translateKeyOutsideComponents } from '../../utils/i18n';

const filterProps = {
  searchQuery: '',
  filters: [
    {
      label: 'Coastal Districts',
      id: 'Coastal Districts',
      isSelected: false,
      isDisabled: false,
    },
    {
      label: 'Objects',
      id: 'Objects',
      isSelected: false,
      isDisabled: false,
    },
    {
      label: 'Countries',
      id: 'Countries',
      isSelected: false,
      isDisabled: false,
    },
  ],
};

describe('src/components/AreaObjectLoader/AreaObjectLoader', () => {
  it('should show error if passed', async () => {
    const props = {
      onClose: jest.fn(),
      isOpen: true,
      drawingListItems: [] as DrawingListItem[],
      error: {
        type: areaObjectLoaderType,
        error: 'Woops, something went wrong!',
      },
      ...filterProps,
    };

    render(
      <WarningsThemeProvider>
        <AreaObjectLoader {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(props.error.error)).toBeTruthy();
  });

  it('should show error for areas and be able to retry', async () => {
    const props = {
      onClose: jest.fn(),
      isOpen: true,
      drawingListItems: [] as DrawingListItem[],
      error: {
        type: areaErrorType,
        error: 'Woops, something went wrong!',
      },
      onRetryRequest: jest.fn(),
      ...filterProps,
    };

    render(
      <WarningsThemeProvider>
        <AreaObjectLoader {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(props.error.error)).toBeTruthy();

    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('error-title-retry').toUpperCase(),
      ),
    );

    expect(props.onRetryRequest).toHaveBeenCalledWith(areaErrorType);
  });

  it('should show loading bar if loading', async () => {
    const props = {
      onClose: jest.fn(),
      isOpen: true,
      drawingListItems: [] as DrawingListItem[],
      isLoading: true,
      ...filterProps,
    };

    render(
      <WarningsThemeProvider>
        <AreaObjectLoader {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByRole('progressbar')).toBeTruthy();
  });
});
