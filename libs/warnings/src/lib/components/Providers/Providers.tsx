/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Theme } from '@mui/material';
import { Provider } from 'react-redux';
import i18n from 'i18next';
import { I18nextProvider } from 'react-i18next';
import { lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import { ConfirmationServiceProvider } from '@opengeoweb/shared';
import { Store } from '@reduxjs/toolkit';
import { ApiProvider } from '@opengeoweb/api';
import { SnackbarWrapperConnect } from '@opengeoweb/snackbar';
import { AuthenticationProvider } from '@opengeoweb/authentication';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { createApi as createFakeApi, MOCK_USERNAME } from '../../utils/fakeApi';
import { API_NAME, WarningsApi } from '../../utils/api';
import { initWarnI18n } from '../../utils/i18n';
import { createMockStore } from '../../store/store';

const appStore = createMockStore();

export interface WarningsThemeProviderProps {
  children: React.ReactNode;
  theme?: Theme;
  optionalQueryClient?: QueryClient;
}

export const WarningsThemeProvider: React.FC<WarningsThemeProviderProps> = ({
  children,
  optionalQueryClient,
  theme = lightTheme,
}: WarningsThemeProviderProps) => {
  const queryClient = new QueryClient({
    defaultOptions: { queries: { retry: false }, mutations: { retry: false } },
  });
  return (
    <QueryClientProvider client={optionalQueryClient || queryClient}>
      <WarningsI18nProvider>
        <ThemeWrapper theme={theme}>{children}</ThemeWrapper>
      </WarningsI18nProvider>
    </QueryClientProvider>
  );
};

const WarningsWrapperProvider: React.FC<WarningsThemeProviderProps> = ({
  children,
  ...props
}: WarningsThemeProviderProps) => (
  <WarningsThemeProvider {...props}>{children}</WarningsThemeProvider>
);

export interface WarningsThemeStoreProviderProps
  extends WarningsThemeProviderProps {
  store?: Store;
  createApi?: () => WarningsApi;
  optionalQueryClient?: QueryClient;
}

export const WarningsThemeStoreProvider: React.FC<
  WarningsThemeStoreProviderProps
> = ({
  children,
  theme = lightTheme,
  store = appStore,
  createApi = createFakeApi,
  optionalQueryClient,
}: WarningsThemeStoreProviderProps) => {
  return (
    <Provider store={store}>
      <AuthenticationProvider
        value={{
          isLoggedIn: true,
          auth: {
            username: MOCK_USERNAME,
            token: '1223344',
            refresh_token: '33455214',
          },
          onLogin: (): void => null!,
          onSetAuth: (): void => null!,
          sessionStorageProvider: null!,
        }}
      >
        <ApiProvider createApi={() => ({ ...createApi() })} name={API_NAME}>
          <WarningsWrapperProvider
            theme={theme}
            optionalQueryClient={optionalQueryClient}
          >
            <SnackbarWrapperConnect>
              <ConfirmationServiceProvider>
                {children as React.ReactElement}
              </ConfirmationServiceProvider>
            </SnackbarWrapperConnect>
          </WarningsWrapperProvider>
        </ApiProvider>
      </AuthenticationProvider>
    </Provider>
  );
};

interface WarningsTranslationsWrapperProps {
  children?: React.ReactNode;
}

export const WarningsI18nProvider: React.FC<
  WarningsTranslationsWrapperProps
> = ({ children }) => {
  initWarnI18n();
  return <I18nextProvider i18n={i18n}>{children}</I18nextProvider>;
};
