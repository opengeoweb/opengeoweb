/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { QueryClient } from '@tanstack/query-core';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { setupServer } from 'msw/node';
import { FC } from 'react';
import { testPublicWarning } from '../storybookUtils/testUtils';
import {
  PublicWarning,
  PublicWarningStatus,
} from '../store/publicWarningForm/types';
import { usePublicWarnings, usePublishWarning } from './warning-api/hooks';
import { WarningsThemeStoreProvider } from '../components/Providers/Providers';
import { requestHandlers } from '../utils/fakeApi/index';

const draftId = testPublicWarning.warningDetail.id;
const publishedId = 'publishedId';

const testWarning: PublicWarning = {
  ...testPublicWarning,
  id: draftId,
  status: PublicWarningStatus.DRAFT_PUBLISHED,
  warningDetail: {
    ...testPublicWarning.warningDetail,
    areas: undefined,
    linked_to_id: publishedId,
  },
};
const originalWarning = {
  ...testWarning,
  id: publishedId,
  status: PublicWarningStatus.PUBLISHED,
  warningDetail: {
    ...testWarning.warningDetail,
    level: 'moderate',
    id: publishedId,
  },
};

const server = setupServer(...requestHandlers);
const WarningHookTestComponent: FC = () => {
  const { mutate } = usePublishWarning();
  const { data } = usePublicWarnings();

  const publishingWarning: PublicWarning = {
    ...testWarning,
    status: PublicWarningStatus.PUBLISHED,
  };

  return (
    <div>
      <button
        type="button"
        onClick={() =>
          mutate({ data: publishingWarning, id: publishingWarning.id })
        }
      >
        publish
      </button>
      <div data-testid="result">
        {data?.map((warning) => <div key={warning.id}>{warning.id}</div>)}
      </div>
    </div>
  );
};

describe('warning-api/hooks.ts', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });
  it('should optimistically update the warning list when publishing a warning', async () => {
    const queryClient = new QueryClient();
    queryClient.setQueryData(
      ['public-warning-list'],
      [testWarning, originalWarning],
    );
    render(
      <WarningsThemeStoreProvider optionalQueryClient={queryClient}>
        <WarningHookTestComponent />
      </WarningsThemeStoreProvider>,
    );
    fireEvent.click(screen.getByText('publish'));
    await waitFor(() => {
      expect(
        queryClient.getQueryData<PublicWarning[]>(['public-warning-list'])!
          .length,
      ).toBe(1);
    });
    const publishedWarning = queryClient.getQueryData<PublicWarning[]>([
      'public-warning-list',
    ])![0];
    expect(publishedWarning.id).toBe(publishedId);
    expect(publishedWarning.warningDetail).toEqual({
      ...testWarning.warningDetail,
      id: publishedId,
    });
  });
});
