/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  optimisticUpdate,
  useAuthenticationContext,
  useAuthQuery,
} from '@opengeoweb/api';
import {
  useMutation,
  UseMutationResult,
  useQueryClient,
  UseQueryResult,
} from '@tanstack/react-query';
import { useDispatch } from 'react-redux';
import { snackbarActions, snackbarTypes } from '@opengeoweb/snackbar';
import { uiActions } from '@opengeoweb/store';
import {
  getPublicWarningList,
  postPublicWarning,
  updatePublicWarning,
  updatePublicWarningEditor,
} from './api';
import {
  PublicWarning,
  PublicWarningStatus,
} from '../../store/publicWarningForm/types';
import { publicWarningFormActions } from '../../store/publicWarningForm/reducer';
import { publicWarningDialogType } from '../../store/publicWarningForm/utils';
import { getIdFromUrl } from '../../utils/api';

/**
 * @param disabled - Disables fetching, but still returns cached values.
 * https://tanstack.com/query/v4/docs/framework/react/guides/disabling-queries
 */
export const usePublicWarnings = (
  disabled = false,
): UseQueryResult<PublicWarning[]> =>
  useAuthQuery(
    ['public-warning-list'],
    getPublicWarningList,
    disabled,
    1000 * 60 * 5,
  );

type MutationReturnType<T> = UseMutationResult<
  Headers | null,
  Error,
  T,
  { oldData: PublicWarning[] | undefined }
>;

interface MutateEditorProps {
  id: string;
  isEditor: boolean;
}
export const useMutatePublicWarningEditor =
  (): MutationReturnType<MutateEditorProps> => {
    const queryClient = useQueryClient();
    const { auth } = useAuthenticationContext();
    const dispatch = useDispatch();

    return useMutation({
      mutationFn: ({ id, isEditor }) => {
        if (!auth) {
          throw new Error('Authentication context not found');
        }
        return updatePublicWarningEditor(id, isEditor, auth);
      },
      ...optimisticUpdate<PublicWarning, MutateEditorProps>(
        ['public-warning-list'],
        queryClient,
        (warning, { isEditor }) => ({
          ...warning,
          editor: isEditor ? auth!.username : undefined,
        }),
        (error) => {
          dispatch(
            publicWarningFormActions.setFormError({ message: error.message }),
          );
        },
      ),
      onSuccess: async (data, { isEditor }) => {
        dispatch(
          snackbarActions.openSnackbar({
            type: snackbarTypes.SnackbarMessageType.TRANSLATABLE_MESSAGE,
            key: isEditor
              ? 'warning-editor-add-editor'
              : 'warning-editor-remove-editor',
          }),
        );
      },
    });
  };
interface UpdateWarningProps {
  id?: string;
  data: PublicWarning;
  panelId?: string;
}

export const useMutateWarning = (
  snackBarMessage: string,
): MutationReturnType<UpdateWarningProps> => {
  const queryClient = useQueryClient();
  const { auth } = useAuthenticationContext();
  const dispatch = useDispatch();

  return useMutation({
    mutationFn: ({ id, data }) => {
      if (!auth) {
        throw new Error('Authentication context not found');
      }
      if (id) {
        return updatePublicWarning(id, auth, data);
      }
      return postPublicWarning(auth, data);
    },
    ...optimisticUpdate<PublicWarning, UpdateWarningProps>(
      ['public-warning-list'],
      queryClient,
      (_, { data, id }) => ({
        ...data,
        id,
        lastUpdatedTime: new Date().toISOString(),
      }),
      (error) => {
        dispatch(
          publicWarningFormActions.setFormError({ message: error.message }),
        );
      },
    ),
    onSuccess: async (data: Headers | null, variables) => {
      const location = data?.get('location');
      const { warningDetail, status } = variables.data;
      if (
        // DRAFT_PUBLISHED warnings have  linked_to_id to the published warning it is based on
        // When publishing a DRAFT_PUBLISHED warning we need to update the published warning and delete the draft
        warningDetail.linked_to_id &&
        status === PublicWarningStatus.PUBLISHED
      ) {
        queryClient.setQueryData<PublicWarning[]>(
          ['public-warning-list'],
          (old) => {
            return old?.reduce((acc, warning) => {
              if (warning.id === warningDetail.linked_to_id) {
                return acc;
              }
              const id =
                warning.id === warningDetail.id
                  ? warningDetail.linked_to_id
                  : warningDetail.id;
              return [
                ...acc,
                {
                  ...warning,
                  id,
                  warningDetail: {
                    ...warningDetail,
                    id,
                  },
                },
              ];
            }, [] as PublicWarning[]);
          },
        );
      }
      if (location) {
        // When making a new warning we get it's ID from the location header
        const newId = getIdFromUrl(location);
        const newWarning = {
          ...variables.data,
          id: newId,
          lastUpdatedTime: new Date().toISOString(),
          warningDetail: {
            ...warningDetail,
            id: newId,
            domain: 'public',
          },
        } as PublicWarning;
        queryClient.setQueryData<PublicWarning[]>(
          ['public-warning-list'],
          (old) => [...(old || []), newWarning],
        );
        dispatch(
          publicWarningFormActions.openPublicWarningFormDialog({
            warning: newWarning,
            formState: '',
            panelId: variables.panelId,
          }),
        );
      }
      dispatch(
        snackbarActions.openSnackbar({
          type: snackbarTypes.SnackbarMessageType.TRANSLATABLE_MESSAGE,
          key: snackBarMessage,
        }),
      );
      if (variables.data.status === PublicWarningStatus.PUBLISHED) {
        dispatch(
          uiActions.setToggleOpenDialog({
            type: publicWarningDialogType,
            setOpen: false,
          }),
        );
      }
    },
  });
};

export const usePublishWarning = (): MutationReturnType<UpdateWarningProps> =>
  useMutateWarning('warning-publish-succes');

export const useSaveWarning = (): MutationReturnType<UpdateWarningProps> =>
  useMutateWarning('warning-save-succes');
