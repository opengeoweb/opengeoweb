/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { usePrevious } from '@opengeoweb/shared';
import * as React from 'react';

export const useFormDirty = ({
  isDirty,
  isLoading,
  error,
  onFormDirty,
  onSuccess,
}: {
  isDirty: boolean;
  isLoading: boolean;
  error: string | undefined;
  onFormDirty: (isFormDirty: boolean) => void;
  onSuccess: () => void;
}): void => {
  const isDirtyPrevious = usePrevious(isDirty);
  React.useEffect(() => {
    // trigger form dirty when value has changed
    if (isDirtyPrevious !== undefined && isDirtyPrevious !== isDirty) {
      onFormDirty(isDirty);
    }
  }, [isDirty, onFormDirty, isDirtyPrevious]);

  const previousIsLoading = usePrevious(isLoading);
  React.useEffect(() => {
    if (previousIsLoading !== undefined && isLoading !== previousIsLoading) {
      // done loading and no error? trigger success
      if (!isLoading && !error) {
        onSuccess();
      }
    }
  }, [isLoading, error, previousIsLoading, onSuccess]);
};
