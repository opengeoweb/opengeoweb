/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  SigmetFromBackend,
  SigmetPhenomena,
  ProductStatus,
  LevelUnits,
  AirmetFromBackend,
  AirmetPhenomena,
  MovementUnit,
  WindUnit,
  Direction,
} from '@opengeoweb/api';
import { dateUtils } from '@opengeoweb/shared';
import { getFir, sigmetConfig, airmetConfig } from '@opengeoweb/sigmet-airmet';

export const fakeSigmetInWarningList: SigmetFromBackend[] = [
  {
    uuid: 'someuniqueidprescibedbyBE',
    creationDate: '2020-09-17T12:00:00Z',
    lastUpdateDate: '2020-09-17T12:00:00Z',
    canbe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
    sigmet: {
      uuid: 'someuniqueidprescibedbyBE',
      phenomenon: 'OBSC_TS' as SigmetPhenomena.OBSC_TS,
      sequence: 'A01',
      validDateStart: dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 1 }),
      )!,

      validDateEnd: dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 3 }),
      )!,

      firName: 'AMSTERDAM FIR',
      locationIndicatorATSU: 'EHAA',
      locationIndicatorATSR: 'EHAA',
      locationIndicatorMWO: 'EHDB',
      isObservationOrForecast: 'OBS',
      observationOrForecastTime: dateUtils.dateToString(
        dateUtils.sub(dateUtils.utc(), { hours: 2 }),
      ),

      movementType: 'FORECAST_POSITION',
      change: 'WKN',
      type: 'NORMAL',
      status: ProductStatus.DRAFT,
      levelInfoMode: 'AT',

      level: {
        value: 1000,
        unit: 'FT' as LevelUnits,
      },
      firGeometry: getFir(sigmetConfig),
      startGeometry: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [5.0, 55.0],
                  [4.331914, 55.332644],
                  [3.368817, 55.764314],
                  [2.761908, 54.379261],
                  [3.15576, 52.913554],
                  [2.000002, 51.500002],
                  [3.370001, 51.369722],
                  [3.370527, 51.36867],
                  [3.362223, 51.320002],
                  [3.36389, 51.313608],
                  [3.373613, 51.309999],
                  [3.952501, 51.214441],
                  [4.397501, 51.452776],
                  [5.078611, 51.391665],
                  [5.848333, 51.139444],
                  [5.651667, 50.824717],
                  [6.011797, 50.757273],
                  [5.934168, 51.036386],
                  [6.222223, 51.361666],
                  [5.94639, 51.811663],
                  [6.405001, 51.830828],
                  [7.053095, 52.237764],
                  [7.031389, 52.268885],
                  [7.063612, 52.346109],
                  [7.065557, 52.385828],
                  [7.133055, 52.888887],
                  [7.14218, 52.898244],
                  [7.191667, 53.3],
                  [6.5, 53.666667],
                  [6.500002, 55.000002],
                  [5.0, 55.0],
                ],
              ],
            },
            properties: {
              selectionType: 'fir',
            },
          },
        ],
      },
      startGeometryIntersect: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [5.0, 55.0],
                  [4.331914, 55.332644],
                  [3.368817, 55.764314],
                  [2.761908, 54.379261],
                  [3.15576, 52.913554],
                  [2.000002, 51.500002],
                  [3.370001, 51.369722],
                  [3.370527, 51.36867],
                  [3.362223, 51.320002],
                  [3.36389, 51.313608],
                  [3.373613, 51.309999],
                  [3.952501, 51.214441],
                  [4.397501, 51.452776],
                  [5.078611, 51.391665],
                  [5.848333, 51.139444],
                  [5.651667, 50.824717],
                  [6.011797, 50.757273],
                  [5.934168, 51.036386],
                  [6.222223, 51.361666],
                  [5.94639, 51.811663],
                  [6.405001, 51.830828],
                  [7.053095, 52.237764],
                  [7.031389, 52.268885],
                  [7.063612, 52.346109],
                  [7.065557, 52.385828],
                  [7.133055, 52.888887],
                  [7.14218, 52.898244],
                  [7.191667, 53.3],
                  [6.5, 53.666667],
                  [6.500002, 55.000002],
                  [5.0, 55.0],
                ],
              ],
            },
            properties: {
              selectionType: 'fir',
            },
          },
        ],
      },
      endGeometry: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'poly',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [6.406626021876513, 52.98697212658787],
                  [7.1354804185898635, 53.68061155113826],
                  [7.955441614892378, 52.82212063293559],
                  [6.406626021876513, 52.98697212658787],
                ],
              ],
            },
          },
        ],
      },
      endGeometryIntersect: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'poly',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [6.406626, 52.986972],
                  [6.898724319383813, 53.45529498136169],
                  [7.191667, 53.3],
                  [7.14344908001551, 52.90854691815449],
                  [6.406626, 52.986972],
                ],
              ],
            },
          },
        ],
      },
    },
  },
  {
    uuid: 'someuniqueidprescibedbyBE2',
    lastUpdateDate: '2020-09-17T12:00:00Z',
    creationDate: '2020-09-17T12:00:00Z',
    canbe: [],
    sigmet: {
      uuid: 'someuniqueidprescibedbyBE2',
      validDateStart: '2020-09-17T14:30:00Z',
      validDateEnd: '2020-09-17T17:00:00Z',
      sequence: '4',
      issueDate: '2020-09-17T14:30:00Z',
      firName: 'AMSTERDAM FIR',
      locationIndicatorATSU: 'EHAA',
      locationIndicatorATSR: 'EHAA',
      locationIndicatorMWO: 'EHDB',
      status: ProductStatus.PUBLISHED,
      cancelsSigmetSequenceId: '114',
      validDateStartOfSigmetToCancel: '2020-09-17T13:00:00Z',
      validDateEndOfSigmetToCancel: '2020-09-17T17:00:00Z',
      type: 'NORMAL',
    },
  },
  {
    uuid: 'someuniqueidprescibedbyBE3',
    lastUpdateDate: '2020-09-17T12:00:00Z',
    creationDate: '2020-09-17T12:00:00Z',
    canbe: [],
    sigmet: {
      uuid: 'someuniqueidprescibedbyBE3',
      phenomenon: 'OBSC_TS' as SigmetPhenomena.OBSC_TS,
      sequence: '113',
      issueDate: '2020-09-17T12:00:00Z',
      validDateStart: '2020-09-17T13:00:00Z',
      validDateEnd: '2020-09-17T17:00:00Z',
      firName: 'AMSTERDAM FIR',
      locationIndicatorATSU: 'EHAA',
      locationIndicatorATSR: 'EHAA',
      locationIndicatorMWO: 'EHDB',
      isObservationOrForecast: 'OBS',
      observationOrForecastTime: '2020-09-17T12:00:00Z',
      levelInfoMode: 'AT',
      level: {
        value: 300,
        unit: 'FL' as LevelUnits,
      },
      movementType: 'FORECAST_POSITION',
      change: 'WKN',
      type: 'NORMAL',
      status: ProductStatus.CANCELLED,
      firGeometry: getFir(sigmetConfig),
      startGeometry: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'box',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [8.451700286501511, 52.063884260285],
                  [0.4518849127926449, 52.063884260285],
                  [0.4518849127926449, 53.280640580981604],
                  [8.451700286501511, 53.280640580981604],
                  [8.451700286501511, 52.063884260285],
                ],
              ],
            },
          },
        ],
      },
      startGeometryIntersect: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'poly',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [6.77617041058054, 52.063884],
                  [2.461047035878412, 52.063884],
                  [3.15576, 52.913554],
                  [3.0571195833792144, 53.280641],
                  [7.189282421218352, 53.280641],
                  [7.14218, 52.898244],
                  [7.133055, 52.888887],
                  [7.065557, 52.385828],
                  [7.063612, 52.346109],
                  [7.031389, 52.268885],
                  [7.053095, 52.237764],
                  [6.77617041058054, 52.063884],
                ],
              ],
            },
          },
        ],
      },
      endGeometry: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'poly',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [6.406626021876513, 52.98697212658787],
                  [7.1354804185898635, 53.68061155113826],
                  [7.955441614892378, 52.82212063293559],
                  [6.406626021876513, 52.98697212658787],
                ],
              ],
            },
          },
        ],
      },
      endGeometryIntersect: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'poly',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [6.406626, 52.986972],
                  [6.898724319383813, 53.45529498136169],
                  [7.191667, 53.3],
                  [7.14344908001551, 52.90854691815449],
                  [6.406626, 52.986972],
                ],
              ],
            },
          },
        ],
      },
    },
  },
];

export const fakeAirmetInWarningList: AirmetFromBackend[] = [
  {
    uuid: 'someuniqueidprescibedbyBE',
    lastUpdateDate: '2020-09-17T12:00:00Z',
    creationDate: '2020-09-17T12:00:00Z',
    canbe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
    airmet: {
      uuid: 'someuniqueidprescibedbyBE',
      phenomenon: 'ISOL_TSGR' as AirmetPhenomena.ISOL_TSGR,
      sequence: 'A01',
      validDateStart: dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 1 }),
      )!,
      validDateEnd: dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 3 }),
      )!,
      firName: 'AMSTERDAM FIR',
      locationIndicatorATSU: 'EHAA',
      locationIndicatorATSR: 'EHAA',
      locationIndicatorMWO: 'EHDB',
      isObservationOrForecast: 'OBS',
      observationOrForecastTime: dateUtils.dateToString(
        dateUtils.sub(dateUtils.utc(), { hours: 2 }),
      ),
      movementType: 'STATIONARY',
      change: 'WKN',
      type: 'NORMAL',
      status: ProductStatus.DRAFT,
      levelInfoMode: 'ABV',
      level: {
        value: 1000,
        unit: 'FT' as LevelUnits,
      },
      firGeometry: getFir(airmetConfig),
      startGeometry: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [5.0, 55.0],
                  [4.331914, 55.332644],
                  [3.368817, 55.764314],
                  [2.761908, 54.379261],
                  [3.15576, 52.913554],
                  [2.000002, 51.500002],
                  [3.370001, 51.369722],
                  [3.370527, 51.36867],
                  [3.362223, 51.320002],
                  [3.36389, 51.313608],
                  [3.373613, 51.309999],
                  [3.952501, 51.214441],
                  [4.397501, 51.452776],
                  [5.078611, 51.391665],
                  [5.848333, 51.139444],
                  [5.651667, 50.824717],
                  [6.011797, 50.757273],
                  [5.934168, 51.036386],
                  [6.222223, 51.361666],
                  [5.94639, 51.811663],
                  [6.405001, 51.830828],
                  [7.053095, 52.237764],
                  [7.031389, 52.268885],
                  [7.063612, 52.346109],
                  [7.065557, 52.385828],
                  [7.133055, 52.888887],
                  [7.14218, 52.898244],
                  [7.191667, 53.3],
                  [6.5, 53.666667],
                  [6.500002, 55.000002],
                  [5.0, 55.0],
                ],
              ],
            },
            properties: {
              selectionType: 'fir',
            },
          },
        ],
      },
      startGeometryIntersect: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [5.0, 55.0],
                  [4.331914, 55.332644],
                  [3.368817, 55.764314],
                  [2.761908, 54.379261],
                  [3.15576, 52.913554],
                  [2.000002, 51.500002],
                  [3.370001, 51.369722],
                  [3.370527, 51.36867],
                  [3.362223, 51.320002],
                  [3.36389, 51.313608],
                  [3.373613, 51.309999],
                  [3.952501, 51.214441],
                  [4.397501, 51.452776],
                  [5.078611, 51.391665],
                  [5.848333, 51.139444],
                  [5.651667, 50.824717],
                  [6.011797, 50.757273],
                  [5.934168, 51.036386],
                  [6.222223, 51.361666],
                  [5.94639, 51.811663],
                  [6.405001, 51.830828],
                  [7.053095, 52.237764],
                  [7.031389, 52.268885],
                  [7.063612, 52.346109],
                  [7.065557, 52.385828],
                  [7.133055, 52.888887],
                  [7.14218, 52.898244],
                  [7.191667, 53.3],
                  [6.5, 53.666667],
                  [6.500002, 55.000002],
                  [5.0, 55.0],
                ],
              ],
            },
            properties: {
              selectionType: 'fir',
            },
          },
        ],
      },
    },
  },
  {
    uuid: 'someuniqueidprescibedbyBE3',
    lastUpdateDate: '2020-09-17T12:00:00Z',
    creationDate: '2020-09-17T12:00:00Z',
    canbe: [],
    airmet: {
      uuid: 'someuniqueidprescibedbyBE3',
      validDateStart: '2020-09-17T14:30:00Z',
      validDateEnd: '2020-09-17T17:00:00Z',
      sequence: '4',
      issueDate: '2020-09-17T14:30:00Z',
      firName: 'AMSTERDAM FIR',
      locationIndicatorATSU: 'EHAA',
      locationIndicatorATSR: 'EHAA',
      locationIndicatorMWO: 'EHDB',
      status: ProductStatus.PUBLISHED,
      cancelsAirmetSequenceId: '113',
      validDateStartOfAirmetToCancel: '2020-09-17T13:00:00Z',
      validDateEndOfAirmetToCancel: '2020-09-17T17:00:00Z',
      type: 'NORMAL',
    },
  },
  {
    uuid: 'someuniqueidprescibedbyBE4',
    lastUpdateDate: '2020-09-17T12:00:00Z',
    creationDate: '2020-09-17T12:00:00Z',
    canbe: [],
    airmet: {
      uuid: 'someuniqueidprescibedbyBE4',
      phenomenon: 'SFC_WIND' as AirmetPhenomena.SFC_WIND,
      sequence: '113',
      issueDate: '2020-09-17T12:00:00Z',
      validDateStart: '2020-09-17T13:00:00Z',
      validDateEnd: '2020-09-17T17:00:00Z',
      firName: 'AMSTERDAM FIR',
      locationIndicatorATSU: 'EHAA',
      locationIndicatorATSR: 'EHAA',
      locationIndicatorMWO: 'EHDB',
      isObservationOrForecast: 'OBS',
      observationOrForecastTime: '2020-09-17T12:00:00Z',
      movementType: 'MOVEMENT',
      movementSpeed: 20.0,
      movementUnit: 'KT' as MovementUnit.KT,
      movementDirection: Direction.NNE,
      change: 'WKN',
      type: 'NORMAL',
      status: ProductStatus.CANCELLED,
      windSpeed: 120,
      windUnit: 'KT' as WindUnit.KT,
      windDirection: 220,
      firGeometry: getFir(airmetConfig),
      startGeometry: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'box',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [8.451700286501511, 52.063884260285],
                  [0.4518849127926449, 52.063884260285],
                  [0.4518849127926449, 53.280640580981604],
                  [8.451700286501511, 53.280640580981604],
                  [8.451700286501511, 52.063884260285],
                ],
              ],
            },
          },
        ],
      },
      startGeometryIntersect: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              selectionType: 'poly',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [6.77617041058054, 52.063884],
                  [2.461047035878412, 52.063884],
                  [3.15576, 52.913554],
                  [3.0571195833792144, 53.280641],
                  [7.189282421218352, 53.280641],
                  [7.14218, 52.898244],
                  [7.133055, 52.888887],
                  [7.065557, 52.385828],
                  [7.063612, 52.346109],
                  [7.031389, 52.268885],
                  [7.053095, 52.237764],
                  [6.77617041058054, 52.063884],
                ],
              ],
            },
          },
        ],
      },
    },
  },
];
