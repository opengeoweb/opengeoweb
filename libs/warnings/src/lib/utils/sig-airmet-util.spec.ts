/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  Airmet,
  CancelAirmet,
  CancelSigmet,
  ProductStatus,
  Sigmet,
} from '@opengeoweb/api';
import { airmetConfig, sigmetConfig } from '@opengeoweb/sigmet-airmet';
import {
  duplicateAviationProduct,
  isCancelAirmet,
  isCancelSigmet,
  renewAviationProduct,
  transformAirmetFromBackendToProductFormat,
  transformSigmetFromBackendToProductFormat,
} from './sig-airmet-util';
import {
  AirmetWarning,
  SigmetWarning,
  Warning,
} from '../store/publicWarningForm/types';
import {
  fakeAirmetInWarningList,
  fakeSigmetInWarningList,
} from './fakeSigmetAirmet';

describe('sig-airmet-util', () => {
  describe('isCancelAirmet', () => {
    it('should return true for a CancelAirmet object', () => {
      const cancelAirmet: CancelAirmet = {
        cancelsAirmetSequenceId: '123',
        validDateStartOfAirmetToCancel: '2023-01-01T00:00:00Z',
        validDateEndOfAirmetToCancel: '2023-01-01T06:00:00Z',
        status: ProductStatus.PUBLISHED,
        validDateStart: '2023-01-01T00:00:00Z',
        validDateEnd: '2023-01-01T06:00:00Z',
        firName: 'FIR',
        locationIndicatorMWO: 'MWO',
        locationIndicatorATSU: 'ATSU',
        locationIndicatorATSR: 'ATSR',
      };
      expect(isCancelAirmet(cancelAirmet)).toBe(true);
    });
  });

  describe('isCancelSigmet', () => {
    it('should return true for a CancelSigmet object', () => {
      const cancelSigmet: CancelSigmet = {
        cancelsSigmetSequenceId: '123',
        validDateStartOfSigmetToCancel: '2023-01-01T00:00:00Z',
        validDateEndOfSigmetToCancel: '2023-01-01T06:00:00Z',
        status: ProductStatus.PUBLISHED,
        validDateStart: '2023-01-01T00:00:00Z',
        validDateEnd: '2023-01-01T06:00:00Z',
        firName: 'FIR',
        locationIndicatorMWO: 'MWO',
        locationIndicatorATSU: 'ATSU',
        locationIndicatorATSR: 'ATSR',
      };
      expect(isCancelSigmet(cancelSigmet)).toBe(true);
    });
  });
  describe('renewWarning', () => {
    it('should renew an Airmet', () => {
      const oldAirmet: AirmetWarning = {
        id: '1',
        productStatus: ProductStatus.PUBLISHED,
        warningDetail: {
          phenomenon: 'TS',
          areas: [],
          validFrom: '2023-01-01T00:00:00Z',
          validUntil: '2023-01-01T06:00:00Z',
          level: '',
          probability: 0,
          descriptionOriginal: '',
          descriptionTranslation: '',
        },
        sigmetAirmetDetails: {
          phenomenon: 'FRQ_TS',
          status: ProductStatus.PUBLISHED,
          validDateStart: '2023-01-01T00:00:00Z',
          validDateEnd: '2023-01-01T06:00:00Z',
          firName: 'FIR',
          type: 'NORMAL',
          isObservationOrForecast: 'OBSERVATION',
          observationOrForecastTime: '2023-01-01T00:00:00Z',
          change: 'NO_CHANGE',
          startGeometry: { type: 'FeatureCollection', features: [] },
          startGeometryIntersect: { type: 'FeatureCollection', features: [] },
          endGeometry: { type: 'FeatureCollection', features: [] },
          endGeometryIntersect: { type: 'FeatureCollection', features: [] },
          firGeometry: { type: 'FeatureCollection', features: [] },
          levelInfoMode: 'SFC',
          level: 'SFC',
          lowerLevel: 'SFC',
          movementType: 'STATIONARY',
          movementDirection: 'N',
          movementSpeed: 10,
          movementUnit: 'KT',
          visibilityValue: 10,
          visibilityCause: 'FOG',
          visibilityUnit: 'KM',
          windSpeed: 10,
          windUnit: 'KT',
          windDirection: 'N',
          cloudLevelInfoMode: 'SFC',
          cloudLevel: 'SFC',
          cloudLowerLevel: 'SFC',
          locationIndicatorMWO: 'MWO',
          locationIndicatorATSU: 'ATSU',
          locationIndicatorATSR: 'ATSR',
        } as unknown as Airmet,
        lastUpdatedTime: '2023-01-01T00:00:00Z',
        type: 'airmet',
      };

      const renewedAirmet = renewAviationProduct(oldAirmet, airmetConfig);

      expect(renewedAirmet.sigmetAirmetDetails?.validDateStart).toEqual(
        oldAirmet.sigmetAirmetDetails?.validDateEnd,
      );
      expect(renewedAirmet.sigmetAirmetDetails?.status).toEqual(
        ProductStatus.DRAFT,
      );
      expect(renewedAirmet.sigmetAirmetDetails?.sequence).toEqual('-1');
    });

    it('should renew a Sigmet', () => {
      const oldSigmet: SigmetWarning = {
        id: '1',
        productStatus: ProductStatus.PUBLISHED,
        warningDetail: {
          phenomenon: 'TS',
          areas: [],
          validFrom: '2023-01-01T00:00:00Z',
          validUntil: '2023-01-01T06:00:00Z',
          level: '',
          probability: 0,
          descriptionOriginal: '',
          descriptionTranslation: '',
        },
        sigmetAirmetDetails: {
          phenomenon: 'TS',
          status: ProductStatus.PUBLISHED,
          validDateStart: '2023-01-01T00:00:00Z',
          validDateEnd: '2023-01-01T06:00:00Z',
          firName: 'FIR',
          type: 'NORMAL',
          isObservationOrForecast: 'OBSERVATION',
          observationOrForecastTime: '2023-01-01T00:00:00Z',
          change: 'NO_CHANGE',
          startGeometry: { type: 'FeatureCollection', features: [] },
          startGeometryIntersect: { type: 'FeatureCollection', features: [] },
          endGeometry: { type: 'FeatureCollection', features: [] },
          endGeometryIntersect: { type: 'FeatureCollection', features: [] },
          firGeometry: { type: 'FeatureCollection', features: [] },
          levelInfoMode: 'SFC',
          level: 'SFC',
          lowerLevel: 'SFC',
          movementType: 'STATIONARY',
          movementDirection: 'N',
          movementSpeed: 10,
          movementUnit: 'KT',
          vaSigmetMoveToFIR: 'FIR',
          vaSigmetVolcanoName: 'Volcano',
          vaSigmetVolcanoCoordinates: '10N 10E',
          locationIndicatorMWO: 'MWO',
          locationIndicatorATSU: 'ATSU',
          locationIndicatorATSR: 'ATSR',
        } as unknown as Sigmet,
        lastUpdatedTime: '2023-01-01T00:00:00Z',
        type: 'sigmet',
      };

      const renewedSigmet = renewAviationProduct(oldSigmet, sigmetConfig);

      expect(renewedSigmet.sigmetAirmetDetails?.validDateStart).toEqual(
        oldSigmet.sigmetAirmetDetails?.validDateEnd,
      );
      expect(renewedSigmet.sigmetAirmetDetails?.status).toEqual(
        ProductStatus.DRAFT,
      );
      expect(renewedSigmet.sigmetAirmetDetails?.sequence).toEqual('-1');
    });

    describe('duplicateAviationProduct', () => {
      it('should duplicate an Airmet', () => {
        const oldAirmet: AirmetWarning = {
          id: '1',
          productStatus: ProductStatus.PUBLISHED,
          warningDetail: {
            phenomenon: 'TS',
            areas: [],
            validFrom: '2023-01-01T00:00:00Z',
            validUntil: '2023-01-01T06:00:00Z',
            level: '',
            probability: 0,
            descriptionOriginal: '',
            descriptionTranslation: '',
          },
          sigmetAirmetDetails: {
            phenomenon: 'FRQ_TS',
            status: ProductStatus.PUBLISHED,
            validDateStart: '2023-01-01T00:00:00Z',
            validDateEnd: '2023-01-01T06:00:00Z',
            firName: 'FIR',
            type: 'NORMAL',
            isObservationOrForecast: 'OBSERVATION',
            observationOrForecastTime: '2023-01-01T00:00:00Z',
            change: 'NO_CHANGE',
            startGeometry: { type: 'FeatureCollection', features: [] },
            startGeometryIntersect: { type: 'FeatureCollection', features: [] },
            endGeometry: { type: 'FeatureCollection', features: [] },
            endGeometryIntersect: { type: 'FeatureCollection', features: [] },
            firGeometry: { type: 'FeatureCollection', features: [] },
            levelInfoMode: 'SFC',
            level: 'SFC',
            lowerLevel: 'SFC',
            movementType: 'STATIONARY',
            movementDirection: 'N',
            movementSpeed: 10,
            movementUnit: 'KT',
            visibilityValue: 10,
            visibilityCause: 'FOG',
            visibilityUnit: 'KM',
            windSpeed: 10,
            windUnit: 'KT',
            windDirection: 'N',
            cloudLevelInfoMode: 'SFC',
            cloudLevel: 'SFC',
            cloudLowerLevel: 'SFC',
            locationIndicatorMWO: 'MWO',
            locationIndicatorATSU: 'ATSU',
            locationIndicatorATSR: 'ATSR',
          } as unknown as Airmet,
          lastUpdatedTime: '2023-01-01T00:00:00Z',
          type: 'airmet',
        };

        const duplicatedAirmet = duplicateAviationProduct(oldAirmet);

        const duplicatedAirmetWarning = duplicatedAirmet as AirmetWarning;
        expect(duplicatedAirmetWarning.sigmetAirmetDetails?.status).toEqual(
          ProductStatus.DRAFT,
        );
        expect(
          duplicatedAirmetWarning.sigmetAirmetDetails?.uuid,
        ).toBeUndefined();
        expect(duplicatedAirmetWarning.sigmetAirmetDetails?.sequence).toEqual(
          '-1',
        );
      });

      it('should duplicate a Sigmet', () => {
        const oldSigmet: Warning = {
          id: '1',
          productStatus: ProductStatus.PUBLISHED,
          warningDetail: {
            phenomenon: 'TS',
            areas: [],
            validFrom: '2023-01-01T00:00:00Z',
            validUntil: '2023-01-01T06:00:00Z',
            level: '',
            probability: 0,
            descriptionOriginal: '',
            descriptionTranslation: '',
          },
          sigmetAirmetDetails: {
            phenomenon: 'TS',
            status: ProductStatus.PUBLISHED,
            validDateStart: '2023-01-01T00:00:00Z',
            validDateEnd: '2023-01-01T06:00:00Z',
            firName: 'FIR',
            type: 'NORMAL',
            isObservationOrForecast: 'OBSERVATION',
            observationOrForecastTime: '2023-01-01T00:00:00Z',
            change: 'NO_CHANGE',
            startGeometry: { type: 'FeatureCollection', features: [] },
            startGeometryIntersect: { type: 'FeatureCollection', features: [] },
            endGeometry: { type: 'FeatureCollection', features: [] },
            endGeometryIntersect: { type: 'FeatureCollection', features: [] },
            firGeometry: { type: 'FeatureCollection', features: [] },
            levelInfoMode: 'SFC',
            level: 'SFC',
            lowerLevel: 'SFC',
            movementType: 'STATIONARY',
            movementDirection: 'N',
            movementSpeed: 10,
            movementUnit: 'KT',
            vaSigmetMoveToFIR: 'FIR',
            vaSigmetVolcanoName: 'Volcano',
            vaSigmetVolcanoCoordinates: '10N 10E',
            locationIndicatorMWO: 'MWO',
            locationIndicatorATSU: 'ATSU',
            locationIndicatorATSR: 'ATSR',
          } as unknown as Sigmet,
          lastUpdatedTime: '2023-01-01T00:00:00Z',
          type: 'sigmet',
        };

        const duplicatedSigmet = duplicateAviationProduct(
          oldSigmet,
        ) as SigmetWarning;

        expect(duplicatedSigmet.sigmetAirmetDetails?.status).toEqual(
          ProductStatus.DRAFT,
        );
        expect(duplicatedSigmet.sigmetAirmetDetails?.uuid).toBeUndefined();
        expect(duplicatedSigmet.sigmetAirmetDetails?.sequence).toEqual('-1');
      });
    });
  });

  describe('transformSigmetFromBackendToProductFormat', () => {
    const selectedSigmet = fakeSigmetInWarningList[0];
    it('should transform selectedSigmet to product format', () => {
      const transformedProduct =
        transformSigmetFromBackendToProductFormat(selectedSigmet);
      expect(transformedProduct).toBeDefined();
      expect(transformedProduct).toHaveProperty(
        'uuid',
        selectedSigmet.sigmet.uuid,
      );
    });
  });
  describe('transformAirmetFromBackendToProductFormat', () => {
    const selectedAirmet = fakeAirmetInWarningList[0];
    it('should transform selectedAirmet to product format', () => {
      const transformedProduct =
        transformAirmetFromBackendToProductFormat(selectedAirmet);
      expect(transformedProduct).toBeDefined();
      expect(transformedProduct).toHaveProperty(
        'uuid',
        selectedAirmet.airmet.uuid,
      );
    });
  });
});
