/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import {
  LayerManagerConnect,
  MultiMapDimensionSelectConnect,
} from '@opengeoweb/core';
import Box from '@mui/material/Box';
import type { Meta, StoryObj } from '@storybook/react';
import MetInfoWrapper from './components/MetInfoWrapper/MetInfoWrapper';
import { createFakeApiWithErrors } from './utils/testUtils';
import { sigmetConfig } from './utils/config';
import { ProductConfig } from './types';
import { DownloadSigmetAirmetConfigWrapper } from './components/MetInfoWrapper';

const meta: Meta<typeof MetInfoWrapper> = {
  title: 'demo/Sigmet',
  component: MetInfoWrapper,
  parameters: {
    docs: {
      description: {
        component: 'Demo of Sigmet module',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof MetInfoWrapper>;

export const Component: Story = {
  args: {
    productConfig: sigmetConfig,
    productType: 'sigmet',
  },
};

interface SigmetDemoComponentProps {
  productConfig?: ProductConfig;
}

const SigmetDemoComponent = ({
  productConfig = sigmetConfig,
}: SigmetDemoComponentProps): React.ReactElement => {
  return (
    <Box
      sx={{
        height: '100vh',
        backgroundColor: 'geowebColors.background.surfaceApp',
      }}
    >
      <MetInfoWrapper productType="sigmet" productConfig={productConfig} />
      <LayerManagerConnect bounds="parent" showMapIdInTitle />
      <MultiMapDimensionSelectConnect />
    </Box>
  );
};

const DownloadConfigDemoComponent: React.FC = (): React.ReactElement => {
  return (
    <Box
      sx={{
        height: '100vh',
        backgroundColor: 'geowebColors.background.surfaceApp',
      }}
    >
      <DownloadSigmetAirmetConfigWrapper productType="sigmet" />
      <LayerManagerConnect bounds="parent" showMapIdInTitle />
      <MultiMapDimensionSelectConnect />
    </Box>
  );
};

export const SigmetDemo: Story = {
  render: () => <SigmetDemoComponent />,
  parameters: {
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a4934891039316017011cd/version/6213a41da08de61393dadfca',
      },
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a4935476031e13fb0f3ab6/version/6213a4443c444d10d9e17536',
      },
    ],
  },
};

export const SigmetDemoDark: Story = {
  ...SigmetDemo,
  tags: ['dark'],
};

export const SigmetDemoErrorApi: Story = {
  ...SigmetDemo,
  parameters: {
    createApi: createFakeApiWithErrors,
  },
  tags: ['dark'],
};

export const SigmetConfigIsLoading: Story = {
  render: () => <DownloadConfigDemoComponent />,
};

export const SigmetConfigIsLoadingWithError: Story = {
  render: () => <DownloadConfigDemoComponent />,
  parameters: {
    createApi: createFakeApiWithErrors,
  },
};
