/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import {
  darkTheme,
  lightTheme,
  ThemeWrapper,
  ThemeProviderProps,
} from '@opengeoweb/theme';
import { ApiProvider, CreateApiFn } from '@opengeoweb/api';
import { Provider } from 'react-redux';
import { Store } from '@reduxjs/toolkit';
import { AxiosError, AxiosResponseHeaders } from 'axios';
import {
  genericListener,
  mapListener,
  storeReducerMap,
  uiTypes,
  WebMapStateModuleState,
} from '@opengeoweb/store';
import { Box } from '@mui/material';
import { createToolkitMockStore } from '@opengeoweb/shared';
import { createApi as createFakeApi } from './fakeApi';
import { SigmetAirmetI18nProvider } from '../components/Providers';
import { AviationPhenomenon } from '../types';

// TODO: move these wrappers to a Providers file, and see if we can remove some
export const ThemeWrapperWithModules: React.FC<ThemeProviderProps> = ({
  theme,
  children,
}: ThemeProviderProps) => <ThemeWrapper theme={theme}>{children}</ThemeWrapper>;

interface StoryWrapperProps {
  children: React.ReactNode;
  // eslint-disable-next-line react/no-unused-prop-types
  isDarkTheme?: boolean;
}

export interface TestWrapperProps extends StoryWrapperProps {
  store?: Store;
  createApi?: CreateApiFn;
}

export const TestWrapper: React.FC<TestWrapperProps> = ({
  children,
  store,
  createApi = null!,
}: TestWrapperProps) => {
  return (
    <SigmetAirmetI18nProvider>
      <Provider store={store || createMockStore({})}>
        <ThemeWrapperWithModules theme={lightTheme}>
          <ApiProvider createApi={createApi || createFakeApi}>
            {children}
          </ApiProvider>
        </ThemeWrapperWithModules>
      </Provider>
    </SigmetAirmetI18nProvider>
  );
};

export const StoryWrapperFakeApi: React.FC<
  StoryWrapperProps & { createApi?: CreateApiFn }
> = ({ children, isDarkTheme = false, createApi = createFakeApi }) => {
  return (
    <SigmetAirmetI18nProvider>
      <Provider store={createMockStore({})}>
        <ThemeWrapperWithModules theme={isDarkTheme ? darkTheme : lightTheme}>
          <ApiProvider createApi={createApi}>{children}</ApiProvider>
        </ThemeWrapperWithModules>
      </Provider>
    </SigmetAirmetI18nProvider>
  );
};

interface SnapshotStoryWrapperProps {
  children: React.ReactNode;
}

export const SnapshotStoryWrapper: React.FC<SnapshotStoryWrapperProps> = ({
  children,
}) => (
  <Box
    sx={{
      width: '600px',
      padding: '10px',
      position: 'relative',
      backgroundColor: 'geowebColors.background.surfaceApp',
    }}
  >
    {children}
  </Box>
);

export const fakeBackendError: AxiosError = {
  isAxiosError: true,
  config: undefined!,
  toJSON: undefined!,
  name: 'API error',
  message: '',
  response: {
    data: 'Unable to store data',
    status: 400,
    statusText: '',
    config: undefined!,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

export const fakeBackendNestedError: AxiosError<{ message: string }> = {
  isAxiosError: true,
  config: undefined!,
  toJSON: undefined!,
  name: 'API error',
  message: '',
  response: {
    data: { message: 'Unable to store nested data' },
    status: 400,
    statusText: '',
    config: undefined!,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

export const fakeBackendDifferentError: AxiosError = {
  isAxiosError: true,
  config: undefined!,
  toJSON: undefined!,
  name: 'API error',
  message: '',
  response: {
    data: { errorMessage: { innerErrorMessage: 'Unable to store data' } },
    status: 400,
    statusText: '',
    config: undefined!,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

export const testFir: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [5.0, 55.0],
            [4.331914, 55.332644],
            [3.368817, 55.764314],
            [2.761908, 54.379261],
            [3.15576, 52.913554],
            [2.000002, 51.500002],
            [3.370001, 51.369722],
            [3.370527, 51.36867],
            [3.362223, 51.320002],
            [3.36389, 51.313608],
            [3.373613, 51.309999],
            [3.952501, 51.214441],
            [4.397501, 51.452776],
            [5.078611, 51.391665],
            [5.848333, 51.139444],
            [5.651667, 50.824717],
            [6.011797, 50.757273],
            [5.934168, 51.036386],
            [6.222223, 51.361666],
            [5.94639, 51.811663],
            [6.405001, 51.830828],
            [7.053095, 52.237764],
            [7.031389, 52.268885],
            [7.063612, 52.346109],
            [7.065557, 52.385828],
            [7.133055, 52.888887],
            [7.14218, 52.898244],
            [7.191667, 53.3],
            [6.5, 53.666667],
            [6.500002, 55.000002],
            [5.0, 55.0],
          ],
        ],
      },
      properties: { selectionType: 'fir' },
    },
  ],
};

export const createFakeApiWithErrors =
  (): // eslint-disable-next-line @typescript-eslint/no-explicit-any
  any => {
    return {
      // dummy calls
      postSigmet: (): Promise<void> => {
        return Promise.reject(fakeBackendError);
      },
      postAirmet: (): Promise<void> => {
        return Promise.reject(fakeBackendError);
      },
      getSigmetConfiguration: async (): Promise<void> => {
        await new Promise((resolve) => {
          setTimeout(resolve, 2000);
        });
        return Promise.reject(fakeBackendError);
      },
      getAirmetConfiguration: async (): Promise<void> => {
        await new Promise((resolve) => {
          setTimeout(resolve, 2000);
        });
        return Promise.reject(fakeBackendError);
      },
    };
  };

export const fakeAirmetPhenomenaList: AviationPhenomenon[] = [
  { code: 'SFC_VIS', description: 'Surface visibility' },
  { code: 'SFC_WIND', description: 'Surface wind' },
  { code: 'OVC_CLD', description: 'Overcast cloud' },
  { code: 'BKN_CLD', description: 'Broken cloud' },
  { code: 'ISOL_CB', description: 'Isolated cumulonimbus cloud' },
  { code: 'OCNL_CB', description: 'Occasional cumulonimbus cloud' },
  { code: 'FRQ_CB', description: 'Frequent cumulonimbus cloud' },
  { code: 'ISOL_TS', description: 'Isolated thunderstorm(s)' },
  { code: 'OCNL_TS', description: 'Occasional thunderstorm(s)' },
  { code: 'ISOL_TSGR', description: 'Isolated thunderstorm(s) with hail' },
  { code: 'OCNL_TSGR', description: 'Occasional thunderstorm(s) with hail' },
  { code: 'ISOL_TCU', description: 'Isolated towering cumulus cloud' },
  { code: 'OCNL_TCU', description: 'Occasional towering cumulus cloud' },
  { code: 'FRQ_TCU', description: 'Frequent towering cumulus cloud' },
  { code: 'MOD_ICE', description: 'Moderate icing' },
  { code: 'MOD_MTW', description: 'Moderate mountain wave' },
  { code: 'MOD_TURB', description: 'Moderate turbulence' },
  { code: 'MT_OBSC', description: 'Mountains obscured' },
];

export const fakeSigmetPhenomenaList: AviationPhenomenon[] = [
  { code: 'EMBD_TS', description: 'Embedded thunderstorm(s)' },
  { code: 'EMBD_TSGR', description: 'Embedded thunderstorm(s) with hail' },
  { code: 'FRQ_TS', description: 'Frequent thunderstorm(s)' },
  { code: 'FRQ_TSGR', description: 'Frequent thunderstorm(s) with hail' },
  { code: 'HVY_DS', description: 'Heavy duststorm' },
  { code: 'HVY_SS', description: 'Heavy sandstorm' },
  { code: 'OBSC_TS', description: 'Obscured thunderstorm(s)' },
  { code: 'OBSC_TSGR', description: 'Obscured thunderstorm(s) with hail' },
  { code: 'RDOACT_CLD', description: 'Radioactive cloud' },
  { code: 'SEV_ICE', description: 'Severe icing' },
  { code: 'SEV_ICE_FRZ_RN', description: 'Severe icing due to freezing rain' },
  { code: 'SEV_MTW', description: 'Severe mountain wave' },
  { code: 'SEV_TURB', description: 'Severe turbulence' },
  { code: 'SQL_TS', description: 'Squall line thunderstorm(s)' },
  { code: 'SQL_TSGR', description: 'Squall line thunderstorm(s) with hail' },
  { code: 'VA_CLD', description: 'Volcanic ash cloud' },
];

const reducer = {
  ...storeReducerMap,
};

export const createMockStore = (
  mockState: uiTypes.UIModuleState & WebMapStateModuleState,
): Store =>
  createToolkitMockStore({
    reducer,
    preloadedState: mockState,
    middleware: [genericListener.middleware, mapListener.middleware],
  });
