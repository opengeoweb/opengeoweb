/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import i18n from 'i18next';
import {
  initReactI18next,
  useTranslation,
  UseTranslationResponse,
} from 'react-i18next';
import { SHARED_NAMESPACE, sharedTranslations } from '@opengeoweb/shared';
import { CORE_NAMESPACE, coreTranslations } from '@opengeoweb/core';
import {
  FORM_FIELDS_NAMESPACE,
  formFieldsTranslations,
} from '@opengeoweb/form-fields';
import sigmetAirmetTranslations from '../../../locales/sigmet-airmet.json';

export const SA_NAMESPACE = 'sigmetairmet';

export const initSigmetAirmetI18n = (): void => {
  void i18n.use(initReactI18next).init({
    lng: 'en',
    fallbackLng: 'en',
    ns: SA_NAMESPACE,

    interpolation: {
      escapeValue: false,
    },
    resources: {
      en: {
        [SA_NAMESPACE]: sigmetAirmetTranslations.en,
        [SHARED_NAMESPACE]: sharedTranslations.en,
        [CORE_NAMESPACE]: coreTranslations.en,
        [FORM_FIELDS_NAMESPACE]: formFieldsTranslations.en,
      },
      fi: {
        [SA_NAMESPACE]: sigmetAirmetTranslations.fi,
        [SHARED_NAMESPACE]: sharedTranslations.fi,
        [CORE_NAMESPACE]: coreTranslations.fi,
        [FORM_FIELDS_NAMESPACE]: formFieldsTranslations.fi,
      },
      no: {
        [SA_NAMESPACE]: sigmetAirmetTranslations.no,
        [SHARED_NAMESPACE]: sharedTranslations.no,
        [CORE_NAMESPACE]: coreTranslations.no,
        [FORM_FIELDS_NAMESPACE]: formFieldsTranslations.no,
      },
      nl: {
        [SA_NAMESPACE]: sigmetAirmetTranslations.nl,
        [SHARED_NAMESPACE]: sharedTranslations.nl,
        [CORE_NAMESPACE]: coreTranslations.nl,
        [FORM_FIELDS_NAMESPACE]: formFieldsTranslations.nl,
      },
    },
  });
};

const ns = [SA_NAMESPACE, FORM_FIELDS_NAMESPACE];

export const translateKeyOutsideComponents = (
  key: string,
  params: Record<string, string | number> | undefined = undefined,
): string => {
  if (!i18n.isInitialized) {
    initSigmetAirmetI18n();
  }
  return i18n.t(key, { ns, ...params });
};

export const useSigmetAirmetTranslation = (): UseTranslationResponse<
  typeof SA_NAMESPACE,
  typeof i18n
> => {
  if (!i18n.isInitialized) {
    initSigmetAirmetI18n();
  }
  return useTranslation(ns);
};

export { i18n };
