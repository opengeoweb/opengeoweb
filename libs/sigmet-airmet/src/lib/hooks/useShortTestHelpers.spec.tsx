/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { act, renderHook } from '@testing-library/react';
import { useShortTestHelpers } from './useShortTestHelpers';

const mockGetValues = jest.fn();
const mockSetValue = jest.fn();
const mockClearErrors = jest.fn();

jest.mock('react-hook-form', () => ({
  useFormContext: (): unknown => ({
    getValues: mockGetValues,
    setValue: mockSetValue,
    clearErrors: mockClearErrors,
  }),
}));

describe('hooks/useShortTestHelpers/useShortTestHelpers', () => {
  it('should return true when type is SHORT_TEST or SHORT_VA_TEST', () => {
    mockGetValues.mockReturnValueOnce('SHORT_TEST');
    const { result } = renderHook(() => useShortTestHelpers());
    expect(result.current.isShortTest()).toBe(true);

    mockGetValues.mockReturnValueOnce('SHORT_VA_TEST');
    const { result: vaResult } = renderHook(() => useShortTestHelpers());
    expect(vaResult.current.isShortTest()).toBe(true);
  });

  it('should return false when type is NORMAL', () => {
    mockGetValues.mockReturnValueOnce('NORMAL');
    const { result } = renderHook(() => useShortTestHelpers());
    expect(result.current.isShortTest()).toBe(false);
  });

  it('should set phenomenon to EMBD_TS when type is SHORT_TEST', () => {
    mockGetValues.mockReturnValue('SHORT_TEST');
    const { result } = renderHook(() => useShortTestHelpers());
    act(() => {
      result.current.onTypeChange();
    });
    expect(mockSetValue).toHaveBeenCalledWith('phenomenon', 'EMBD_TS');
  });

  it('should set phenomenon to VA_CLD when type is SHORT_VA_TEST', () => {
    mockGetValues.mockReturnValue('SHORT_VA_TEST');
    const { result } = renderHook(() => useShortTestHelpers());
    act(() => {
      result.current.onTypeChange();
    });
    expect(mockSetValue).toHaveBeenCalledWith('phenomenon', 'VA_CLD');
  });

  it('should reset unused fields when type is SHORT_TEST', () => {
    mockGetValues.mockReturnValue('SHORT_TEST');
    const { result } = renderHook(() => useShortTestHelpers());
    act(() => {
      result.current.onTypeChange();
    });
    expect(mockSetValue).toHaveBeenCalledWith('isObservationOrForecast', null);
    expect(mockClearErrors).toHaveBeenCalled();
  });

  it('should reset unused fields when type is SHORT_VA_TEST', () => {
    mockGetValues.mockReturnValue('SHORT_VA_TEST');
    const { result } = renderHook(() => useShortTestHelpers());
    act(() => {
      result.current.onTypeChange();
    });
    expect(mockSetValue).toHaveBeenCalledWith('isObservationOrForecast', null);
    expect(mockClearErrors).toHaveBeenCalled();
  });

  it('should not reset unused fields when type is NORMAL', () => {
    mockGetValues.mockReturnValue('NORMAL');
    const { result } = renderHook(() => useShortTestHelpers());
    act(() => {
      result.current.onTypeChange();
    });
    expect(mockSetValue).not.toHaveBeenCalled();
    expect(mockClearErrors).not.toHaveBeenCalled();
  });
});
