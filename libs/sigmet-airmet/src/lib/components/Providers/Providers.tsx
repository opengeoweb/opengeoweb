/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { I18nextProvider } from 'react-i18next';
import i18n from 'i18next';
import {
  lightTheme,
  ThemeProviderProps,
  ThemeWrapper,
} from '@opengeoweb/theme';
import { Theme } from '@mui/material';
import { Store } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { initSigmetAirmetI18n } from '../../utils/i18n';

interface SATranslationWrapperProps {
  children?: React.ReactNode;
}

interface BaseProviderProps {
  children?: React.ReactNode;
  theme?: Theme;
}

export const SigmetAirmetI18nProvider: React.FC<SATranslationWrapperProps> = ({
  children,
}) => {
  initSigmetAirmetI18n();
  return <I18nextProvider i18n={i18n}>{children}</I18nextProvider>;
};

export const ThemeProvider: React.FC<BaseProviderProps> = ({
  children,
  theme = lightTheme,
}) => <ThemeWrapper theme={theme}>{children}</ThemeWrapper>;

export const DemoWrapper: React.FC<BaseProviderProps> = ({
  children,
  theme,
}) => {
  return (
    <SigmetAirmetI18nProvider>
      <ThemeProvider theme={theme}>{children}</ThemeProvider>
    </SigmetAirmetI18nProvider>
  );
};

export const TestThemeStoreProvider: React.FC<
  ThemeProviderProps & {
    store: Store;
  }
> = ({ children, theme = lightTheme, store }) => (
  <Provider store={store}>
    <SigmetAirmetI18nProvider>
      <ThemeProvider theme={theme}>
        {children as React.ReactElement}
      </ThemeProvider>
    </SigmetAirmetI18nProvider>
  </Provider>
);
