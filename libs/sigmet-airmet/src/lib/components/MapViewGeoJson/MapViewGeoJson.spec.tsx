/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';

import { defaultLayers, genericActions, mapUtils } from '@opengeoweb/store';
import { LayerType, webmapUtils } from '@opengeoweb/webmap';
import { MapViewLayerProps } from '@opengeoweb/webmap-react';
import MapViewGeoJson from './MapViewGeoJson';
import { srsAndBboxDefault } from './constants';
import { createMockStore, TestWrapper } from '../../utils/testUtils';

describe('MapViewGeoJson', () => {
  afterEach(() => {
    webmapUtils.unRegisterAllWMJSLayersAndMaps();
  });
  it('should render successfully with default layers', () => {
    render(
      <TestWrapper>
        <MapViewGeoJson geoJSONLayers={[]} />
      </TestWrapper>,
    );
    const layerList = screen.queryAllByTestId('mapViewLayer');
    expect(layerList[0].textContent).toContain('countryborders');
  });

  it('should set correct default baselayers', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            ...mapUtils.createMap({ id: 'main-map' }),
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
      syncGroups: {
        ...genericActions.initialSyncState,
      },
    };
    const store = createMockStore(mockState);
    jest.spyOn(webmapUtils, 'generateMapId').mockReturnValueOnce('mapid_1');

    const generatedId = 'countryborders-sigmet-mapid_1';
    expect(store.getState().layers.byId[generatedId]).toBeUndefined();

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson geoJSONLayers={[]} />
      </TestWrapper>,
    );

    const result = store.getState().layers.byId[generatedId];

    expect(result).toBeDefined();
    expect(result.service).toEqual(defaultLayers.overLayer.service);
    expect(result.name).toEqual(defaultLayers.overLayer.name);
    expect(result.layerType).toEqual('overLayer');
  });

  it('should set correct given baselayers', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            ...mapUtils.createMap({ id: 'main-map' }),
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {},
        allIds: [],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
      syncGroups: {
        ...genericActions.initialSyncState,
      },
    };
    const store = createMockStore(mockState);
    const testLayers = [
      {
        id: 'baseLayer-airmet',
        name: 'WorldMap_Light_Grey_Canvas',
        type: 'twms',
        layerType: LayerType.baseLayer,
      },
      {
        id: 'countryborder-airmet',
        name: 'countryborders',
        layerType: LayerType.overLayer,
        format: 'image/png',
        enabled: true,
        service: 'https://geoservices.knmi.nl/wms?DATASET=baselayers&',
      },
      {
        id: 'northseastations-airmet',
        name: 'northseastations',
        layerType: LayerType.overLayer,
        service:
          'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserverdataset=OVL',
        format: 'image/png',
        enabled: true,
      },
      {
        id: 'airports-airmet',
        name: 'airports',
        layerType: LayerType.overLayer,
        service:
          'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserverdataset=OVL',
        format: 'image/png',
        enabled: true,
      },
    ];

    expect(store.getState().layers.allIds).toHaveLength(0);

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson baseLayers={testLayers} geoJSONLayers={[]} />
      </TestWrapper>,
    );

    const layerStoreResult = store.getState().layers;
    expect(layerStoreResult.allIds).toHaveLength(testLayers.length);

    testLayers.forEach((layer, index) => {
      expect(layerStoreResult.byId[layer.id]).toBeDefined();
      expect(layerStoreResult.byId[layer.id].id).toEqual(testLayers[index].id);
      expect(layerStoreResult.byId[layer.id].name).toEqual(
        testLayers[index].name,
      );
      expect(layerStoreResult.byId[layer.id].layerType).toEqual(
        testLayers[index].layerType,
      );
      expect(layerStoreResult.byId[layer.id].format).toEqual(
        testLayers[index].format,
      );
    });
  });

  it('should render layers successfully with fir', () => {
    const testLayers: MapViewLayerProps[] = [
      {
        id: 'geojsonlayer-fir',
        geojson: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [5, 55],
                    [4.331914, 55.332644],
                    [3.368817, 55.764314],
                    [2.761908, 54.379261],
                    [3.15576, 52.913554],

                    [7.133055, 52.888887],
                    [7.14218, 52.898244],
                    [7.191667, 53.3],
                    [6.5, 53.666667],
                    [6.500002, 55.000002],
                    [5, 55],
                  ],
                ],
              },
              properties: {
                selectionType: 'fir',
                stroke: '#0075a9',
                'stroke-width': 1.5,
                'stroke-opacity': 1,
                fill: '#0075a9',
                'fill-opacity': 0,
              },
            },
          ],
        },
        isInEditMode: false,
      },
      {
        id: 'geojsonlayer-end',
        geojson: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                selectionType: 'point',
                stroke: '#6e1e91',
                'stroke-width': 1.5,
                'stroke-opacity': 1,
                fill: '#6e1e91',
                'fill-opacity': 0.25,
              },
              geometry: {
                type: 'Point',
                coordinates: [5, 51.5],
              },
            },
          ],
        },
        isInEditMode: false,
        drawMode: '',
        selectedFeatureIndex: 0,
      },
      {
        id: 'geojsonlayer-start',
        geojson: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                selectionType: 'box',
                stroke: '#f24a00',
                'stroke-width': 1.5,
                'stroke-opacity': 1,
                fill: '#f24a00',
                'fill-opacity': 0.25,
              },
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [8.451700286501511, 52.063884260285],
                    [0.4518849127926449, 52.063884260285],
                    [0.4518849127926449, 53.280640580981604],
                    [8.451700286501511, 53.280640580981604],
                    [8.451700286501511, 52.063884260285],
                  ],
                ],
              },
            },
          ],
        },
        isInEditMode: false,
        drawMode: '',
        selectedFeatureIndex: 0,
      },
      {
        id: 'geojsonlayer-intersection-end',
        geojson: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                selectionType: 'point',
                stroke: '#6e1e91',
                'stroke-width': 1.5,
                'stroke-opacity': 1,
                fill: '#6e1e91',
                'fill-opacity': 0.5,
              },
              geometry: {
                type: 'Point',
                coordinates: [5, 51.5],
              },
            },
          ],
        },
        isInEditMode: false,
      },
      {
        id: 'geojsonlayer-intersection-start',
        geojson: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                selectionType: 'poly',
                stroke: '#f24a00',
                'stroke-width': 1.5,
                'stroke-opacity': 1,
                fill: '#f24a00',
                'fill-opacity': 0.5,
              },
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [6.77617041058054, 52.063884],
                    [2.461047035878412, 52.063884],
                    [3.15576, 52.913554],
                    [3.0571195833792144, 53.280641],
                    [7.189282421218352, 53.280641],
                    [7.14218, 52.898244],
                    [7.133055, 52.888887],
                    [7.065557, 52.385828],
                    [7.063612, 52.346109],
                    [7.031389, 52.268885],
                    [7.053095, 52.237764],
                    [6.77617041058054, 52.063884],
                  ],
                ],
              },
            },
          ],
        },
        isInEditMode: false,
      },
    ];
    render(
      <TestWrapper>
        <MapViewGeoJson geoJSONLayers={testLayers} />
      </TestWrapper>,
    );
    const layerList = screen.queryAllByTestId('mapViewLayer');
    expect(layerList[1].textContent).toContain('geojsonlayer-fir');
    expect(layerList[2].textContent).toContain('geojsonlayer-end');
    expect(layerList[3].textContent).toContain('geojsonlayer-start');
    expect(layerList[4].textContent).toContain('geojsonlayer-intersection-end');
    expect(layerList[5].textContent).toContain(
      'geojsonlayer-intersection-start',
    );
  });

  it('should set layers of main map if they exist', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            ...mapUtils.createMap({ id: 'main-map' }),
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
      syncGroups: {
        ...genericActions.initialSyncState,
      },
    };
    const store = createMockStore(mockState);

    expect(store.getState().layers.allIds).toHaveLength(1);
    expect(store.getState().webmap.allIds).toHaveLength(1);

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson geoJSONLayers={[]} />
      </TestWrapper>,
    );

    expect(store.getState().webmap.allIds).toHaveLength(2);
    expect(store.getState().layers.allIds).toHaveLength(3);
  });

  it('should set mapDimensions of the main map if they exist', () => {
    const dimensions = [
      {
        currentTime: new Date().toString(),
        name: 'time',
        currentValue: new Date().toString(),
      },
    ];
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            ...mapUtils.createMap({ id: 'main-map' }),
            mapLayers: ['layerid_2'],
            dimensions,
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
      syncGroups: {
        ...genericActions.initialSyncState,
      },
    };
    const store = createMockStore(mockState);

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson geoJSONLayers={[]} />
      </TestWrapper>,
    );

    expect(store.getState().webmap.byId['main-map'].dimensions).toEqual(
      dimensions,
    );
  });

  it('should contain the layermanager, legend and multidimensionselect buttons', async () => {
    const dimensions = [{ currentValue: '9000', name: 'elevation' }];
    const mapId = 'main-map';
    const layerId = 'layerid_1';

    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            ...mapUtils.createMap({ id: mapId }),
            mapLayers: [layerId],
            dimensions,
          },
        },
        allIds: [mapId],
      },
      layers: {
        byId: {
          [layerId]: {
            mapId,
            id: layerId,
            dimensions: [
              {
                name: 'elevation',
                currentValue: '9000',
                units: 'meters',
                values: '1000,5000,9000',
                synced: false,
              },
            ],
          },
        },
        allIds: [layerId],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
      syncGroups: {
        ...genericActions.initialSyncState,
      },
    };
    const store = createMockStore(mockState);

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson geoJSONLayers={[]} />
      </TestWrapper>,
    );

    expect(screen.getByTestId('layerManagerButton')).toBeTruthy();
    expect(screen.getByTestId('open-Legend')).toBeTruthy();
    expect(screen.getByTestId('dimensionMapBtn-elevation')).toBeTruthy();
  });

  it('should include docked layer manager with source app', async () => {
    const dimensions = [{ currentValue: '9000', name: 'elevation' }];
    const mapId = 'main-map';
    const layerId = 'layerid_1';

    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            ...mapUtils.createMap({ id: mapId }),
            mapLayers: [layerId],
            dimensions,
          },
        },
        allIds: [mapId],
      },
      layers: {
        byId: {
          [layerId]: {
            mapId,
            id: layerId,
            dimensions: [
              {
                name: 'elevation',
                currentValue: '9000',
                units: 'meters',
                values: '1000,5000,9000',
                synced: false,
              },
            ],
          },
        },
        allIds: [layerId],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
      syncGroups: {
        ...genericActions.initialSyncState,
      },
    };
    const store = createMockStore(mockState);

    expect(
      store.getState().ui.dialogs['dockedLayerManager-sigmet-mapid_1'],
    ).toBeUndefined();
    jest.spyOn(webmapUtils, 'generateMapId').mockReturnValueOnce('mapid_1');

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson geoJSONLayers={[]} />
      </TestWrapper>,
    );

    const result =
      store.getState().ui.dialogs['dockedLayerManager-sigmet-mapid_1'];
    expect(result).toBeDefined();
    expect(result.setOpen).toBeFalsy();
    expect(result.source).toEqual('app');
    expect(result.type).toEqual(
      expect.stringContaining('dockedLayerManager-sigmet-mapid_'),
    );
  });

  it('should set correct default projection', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            ...mapUtils.createMap({ id: 'main-map' }),
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
      syncGroups: {
        ...genericActions.initialSyncState,
      },
    };
    const store = createMockStore(mockState);
    const generatedId = 'sigmet-mapid_1';
    jest.spyOn(webmapUtils, 'generateMapId').mockReturnValueOnce('mapid_1');

    expect(store.getState().webmap.byId[generatedId]).toBeUndefined();

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson geoJSONLayers={[]} />
      </TestWrapper>,
    );
    const result = store.getState().webmap.byId[generatedId];
    expect(result).toBeDefined();
    expect(result.bbox).toEqual(srsAndBboxDefault.bbox);
    expect(result.id).toEqual(generatedId);
    expect(result.srs).toEqual(srsAndBboxDefault.srs);
  });

  it('should set correct given projection', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            ...mapUtils.createMap({ id: 'main-map' }),
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
      syncGroups: {
        ...genericActions.initialSyncState,
      },
    };
    const store = createMockStore(mockState);
    const testProjection = {
      bbox: {
        left: -811501,
        right: 2738819,
        top: 12688874,
        bottom: 5830186,
      },
      srs: 'EPSG:3857',
    };

    const generatedId = 'sigmet-mapid_1';
    jest.spyOn(webmapUtils, 'generateMapId').mockReturnValueOnce('mapid_1');

    expect(store.getState().webmap.byId[generatedId]).toBeUndefined();

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson projection={testProjection} geoJSONLayers={[]} />
      </TestWrapper>,
    );

    const result = store.getState().webmap.byId[generatedId];
    expect(result).toBeDefined();
    expect(result.bbox).toEqual(testProjection.bbox);
    expect(result.id).toEqual(generatedId);
    expect(result.srs).toEqual(testProjection.srs);
  });
});
