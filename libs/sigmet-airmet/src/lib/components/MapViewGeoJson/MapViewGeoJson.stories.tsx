/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { produce } from 'immer';

import { intersectPolygonGeoJSONS } from '@opengeoweb/webmap-react';
import MapViewGeoJson from './MapViewGeoJson';
import {
  featurePropsFIR,
  featurePropsUnStyled,
  simplePolygonGeoJSON,
  simplePolygonGeoJSONHalfOfNL,
} from './constants';
import { sigmetConfig } from '../../utils/config';
import { getFir } from '../ProductForms/utils';

export default {
  title: 'components/Mapview',
  tags: ['!autodocs'],
};

export const mapViewGeoJson: React.FC = () => {
  return (
    <div style={{ height: '100vh' }}>
      <MapViewGeoJson
        geoJSONLayers={[
          { id: 'start', geojson: simplePolygonGeoJSON },
          { id: 'end', geojson: simplePolygonGeoJSON },
        ]}
      />
    </div>
  );
};

export const mapViewAmsterdamFir: React.FC = () => {
  const firArea = produce(getFir(sigmetConfig), (draft) => {
    draft.features[0].properties = {
      ...draft.features[0].properties,
      ...featurePropsFIR,
    };
  });
  return (
    <div style={{ height: '100vh' }}>
      <MapViewGeoJson
        geoJSONLayers={[
          { id: 'fir', geojson: firArea },
          { id: 'start', geojson: simplePolygonGeoJSON },
          { id: 'end', geojson: simplePolygonGeoJSON },
        ]}
      />
    </div>
  );
};

export const mapViewIntersect: React.FC = () => {
  const firArea = produce(getFir(sigmetConfig), (draft) => {
    draft.features[0].properties = {
      ...draft.features[0].properties,
      ...featurePropsFIR,
    };
  });
  return (
    <div style={{ height: '100vh' }}>
      <MapViewGeoJson
        geoJSONLayers={[
          { id: 'fir', geojson: firArea },
          {
            id: 'start',
            geojson: produce(simplePolygonGeoJSONHalfOfNL, (draft) => {
              draft.features[0].properties!['fill-opacity'] = 0;
              draft.features[0].properties!['stroke-width'] = 5;
            }),
          },
          {
            id: 'intersection',
            geojson: intersectPolygonGeoJSONS(
              simplePolygonGeoJSONHalfOfNL,
              firArea,
              featurePropsUnStyled,
            ),
          },
        ]}
      />
    </div>
  );
};
