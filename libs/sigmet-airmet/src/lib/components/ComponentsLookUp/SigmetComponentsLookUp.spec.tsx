/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Credentials } from '@opengeoweb/api';

import {
  SigmetComponentsLookUp,
  SigmetComponentsLookUpPayload,
} from './SigmetComponentsLookUp';

jest.mock('../../utils/api');

describe('components/ComponentsLookUp/componentsLookUp', () => {
  beforeEach(() => {
    jest.useFakeTimers();
    Element.prototype.scrollTo = jest.fn();
  });

  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  const testPayload = {
    componentType: 'SigmetModule',
    id: 'test',
    initialProps: {},
  } as SigmetComponentsLookUpPayload;

  const testConfig = {
    baseURL: 'test',
    appURL: 'test',
    authTokenURL: '',
    authClientId: 'test',
  };

  const testAuth: Credentials = {
    username: 'test user',
    token: '',
    refresh_token: '',
  };

  const testOnSetAuth = jest.fn();

  it('should return sigmet module', () => {
    const result = SigmetComponentsLookUp(testPayload, {
      config: testConfig,
      auth: testAuth,
      onSetAuth: testOnSetAuth,
      productType: 'sigmet',
    });

    expect(result!.props.config).toEqual(testConfig);
    expect(result!.props.auth).toEqual(testAuth);
  });
});
