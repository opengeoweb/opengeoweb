/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';

import ProductList from './ProductList';
import { fakeSigmetList } from '../../utils/mockdata/fakeSigmetList';
import { TestWrapper } from '../../utils/testUtils';
import { ProductType } from '../../types';
import { fakeAirmetList } from '../../utils/mockdata/fakeAirmetList';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { airmetConfig, sigmetConfig } from '../../utils/config';

describe('ProductList', () => {
  it('should create a new sigmet', async () => {
    jest.mock(
      'libs/authentication/src/lib/components/AuthenticationContext',
      () => {
        return {
          // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
          useAuthenticationContext: () => ({ isLoggedIn: true }),
        };
      },
    );

    const props = {
      productType: 'sigmet' as ProductType,
      productConfig: sigmetConfig,
      onClickProductRow: jest.fn(),
      onClickNewProduct: jest.fn(),
      productList: fakeSigmetList,
      isLoading: false,
      error: null!,
    };

    render(
      <TestWrapper>
        <ProductList {...props} />
      </TestWrapper>,
    );

    fireEvent.click(await screen.findByTestId('productListCreateButton'));
    expect(props.onClickNewProduct).toHaveBeenCalled();
    expect(props.onClickProductRow).not.toHaveBeenCalled();
  });

  it('should still show new aviation button when backend fails to load', async () => {
    const props = {
      productType: 'sigmet' as ProductType,
      productConfig: sigmetConfig,
      onClickProductRow: jest.fn(),
      onClickNewProduct: jest.fn(),
      productList: fakeSigmetList,
      isLoading: false,
      error: {
        name: translateKeyOutsideComponents('error-backend'),
        message: 'test error message',
      },
    };
    render(
      <TestWrapper>
        <ProductList {...props} />
      </TestWrapper>,
    );

    await screen.findAllByText('Issue time');

    expect(screen.getByTestId('productListCreateButton')).toBeTruthy();
  });

  it('should create a new airmet', async () => {
    const props = {
      productType: 'airmet' as ProductType,
      productConfig: airmetConfig,
      onClickProductRow: jest.fn(),
      onClickNewProduct: jest.fn(),
      productList: fakeAirmetList,
      isLoading: false,
      error: null!,
    };
    render(
      <TestWrapper>
        <ProductList {...props} />
      </TestWrapper>,
    );

    // Wait until sigmetlist has loaded
    await screen.findAllByText('Issue time');

    fireEvent.click(screen.getByTestId('productListCreateButton'));
    expect(props.onClickNewProduct).toHaveBeenCalled();
    expect(props.onClickProductRow).not.toHaveBeenCalled();
  });

  it('should open an existing sigmet', async () => {
    const props = {
      productType: 'sigmet' as ProductType,
      productConfig: sigmetConfig,
      onClickProductRow: jest.fn(),
      onClickNewProduct: jest.fn(),
      productList: fakeSigmetList,
      isLoading: false,
      error: null!,
    };
    render(
      <TestWrapper>
        <ProductList {...props} />
      </TestWrapper>,
    );

    // Wait until sigmetlist has loaded
    const issueTimes = await screen.findAllByText('Issue time');

    fireEvent.click(issueTimes[0]);
    expect(props.onClickNewProduct).not.toHaveBeenCalled();
    expect(props.onClickProductRow).toHaveBeenCalled();
  });

  it('should show a row for each sigmet in the list', async () => {
    const props = {
      productType: 'sigmet' as ProductType,
      productConfig: sigmetConfig,
      onClickProductRow: jest.fn(),
      onClickNewProduct: jest.fn(),
      productList: fakeSigmetList,
      isLoading: false,
      error: null!,
    };
    render(
      <TestWrapper>
        <ProductList {...props} />
      </TestWrapper>,
    );
    // Wait until sigmetlist has loaded
    await screen.findAllByText('Issue time');

    expect(screen.getAllByTestId('productListItem').length).toEqual(
      props.productList.length,
    );
    expect(screen.queryByTestId('productlist-loadingbar')).toBeFalsy();
    expect(screen.queryByTestId('productList-alert')).toBeFalsy();
  });

  it('should show a row for each airmet in the list', async () => {
    const props = {
      productType: 'airmet' as ProductType,
      productConfig: airmetConfig,
      onClickProductRow: jest.fn(),
      onClickNewProduct: jest.fn(),
      productList: fakeAirmetList,
      isLoading: false,
      error: null!,
    };
    render(
      <TestWrapper>
        <ProductList {...props} />
      </TestWrapper>,
    );
    // Wait until airmetlist has loaded
    await screen.findAllByText('Issue time');

    expect(screen.getAllByTestId('productListItem').length).toEqual(
      props.productList.length,
    );
    expect(screen.queryByTestId('productlist-loadingbar')).toBeFalsy();
    expect(screen.queryByTestId('productList-alert')).toBeFalsy();
  });

  it('should show an error when present', async () => {
    const testError = new Error('test sigmet list error');
    testError.name = 'SomeErrorName';

    const props = {
      productType: 'sigmet' as ProductType,
      productConfig: sigmetConfig,
      onClickProductRow: jest.fn(),
      onClickNewProduct: jest.fn(),
      productList: fakeSigmetList,
      isLoading: false,
      error: testError,
    };
    render(
      <TestWrapper>
        <ProductList {...props} />
      </TestWrapper>,
    );

    // Wait until sigmetlist has loaded
    await screen.findAllByText('Issue time');

    // Check for the presence of certain keywords in the error message
    expect(screen.getByTestId('productList-alert').textContent).toContain(
      'SomeErrorName',
    );
    expect(screen.queryByTestId('productlist-loadingbar')).toBeFalsy();
  });

  it('should show an error with name and message', async () => {
    const testError = new Error('test error message');

    const props = {
      productType: 'sigmet' as ProductType,
      productConfig: sigmetConfig,
      onClickProductRow: jest.fn(),
      onClickNewProduct: jest.fn(),
      productList: fakeSigmetList,
      isLoading: false,
      error: testError,
    };
    render(
      <TestWrapper>
        <ProductList {...props} />
      </TestWrapper>,
    );

    // Wait until sigmetlist has loaded
    await screen.findAllByText('Issue time');

    // Check for the presence of certain keywords in the error message
    expect(screen.getByTestId('productList-alert').textContent).toContain(
      'Errortest',
    );
    expect(screen.queryByTestId('productlist-loadingbar')).toBeFalsy();
  });

  it('should show a loadingbar when loading', async () => {
    const props = {
      productType: 'sigmet' as ProductType,
      productConfig: sigmetConfig,
      onClickProductRow: jest.fn(),
      onClickNewProduct: jest.fn(),
      productList: fakeSigmetList,
      isLoading: true,
      error: null!,
    };
    render(
      <TestWrapper>
        <ProductList {...props} />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(screen.getByTestId('productlist-loadingbar')).toBeTruthy();
    });
    expect(screen.queryByTestId('productList-alert')).toBeFalsy();
  });
});
