/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Paper } from '@mui/material';
import { Meta, StoryObj } from '@storybook/react/*';
import ProductList from './ProductList';
import {
  fakeSigmetList,
  fakeStaticSigmetList,
} from '../../utils/mockdata/fakeSigmetList';
import {
  fakeAirmetList,
  fakeStaticAirmetList,
} from '../../utils/mockdata/fakeAirmetList';
import { AirmetFromBackend, SigmetFromBackend } from '../../types';
import { airmetConfig, sigmetConfig } from '../../utils/config';

const meta: Meta<typeof ProductList> = {
  title: 'components/Product List',
  component: ProductList,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the ProductList',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof ProductList>;

export const Component: Story = {
  args: {
    productList: fakeSigmetList,
    productType: 'sigmet',
    productConfig: sigmetConfig,
    isLoading: false,
    error: undefined,
    onClickProductRow: (): void => {},
    onClickNewProduct: (): void => {},
  },
};

interface ListProps {
  productList: SigmetFromBackend[] | AirmetFromBackend[];
  maxWidth?: number;
  isLoading?: boolean;
  listError?: Error | null;
}

const SigmetListComponent: React.FC<ListProps> = ({
  productList,
  maxWidth = 1200,
  isLoading = false,
  listError = null,
}: ListProps) => {
  return (
    <Paper
      style={{
        maxWidth,
        padding: '24px',
      }}
    >
      <ProductList
        productList={productList}
        productType="sigmet"
        productConfig={sigmetConfig}
        isLoading={isLoading}
        error={listError!}
        onClickProductRow={(): void => {}}
        onClickNewProduct={(): void => {}}
      />
    </Paper>
  );
};

export const SigmetListDemo = (): React.ReactElement => {
  return <SigmetListComponent productList={fakeSigmetList} />;
};

export const SigmetListLoading = (): React.ReactElement => {
  return <SigmetListComponent productList={fakeSigmetList} isLoading={true} />;
};

export const SigmetListError = (): React.ReactElement => {
  return (
    <SigmetListComponent
      productList={fakeSigmetList}
      listError={{
        name: 'An error occurred while retrieving the list.',
        message: '',
      }}
    />
  );
};

const sigmetDesignLinks = [
  {
    name: 'Light theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a4934891039316017011cd/version/6213a41da08de61393dadfca',
  },
  {
    name: 'Dark theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a4935476031e13fb0f3ab6/version/6213a4443c444d10d9e17536',
  },
  {
    name: 'responsive',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5f4e0b5cef6167b007682172/version/6213a41831d996150da6af09',
  },
];

export const SigmetListDemoSnapshot = (): React.ReactElement => {
  return <SigmetListComponent productList={fakeStaticSigmetList} />;
};

SigmetListDemoSnapshot.storyName = 'Sigmet List Demo light theme';
SigmetListDemoSnapshot.tags = ['snapshot'];

SigmetListDemoSnapshot.parameters = {
  zeplinLink: sigmetDesignLinks,
};

export const SigmetListDemoSnapshotDarkTheme = (): React.ReactElement => {
  return <SigmetListComponent productList={fakeStaticSigmetList} />;
};

SigmetListDemoSnapshotDarkTheme.storyName = 'Sigmet List Demo dark theme';
SigmetListDemoSnapshotDarkTheme.tags = ['snapshot', 'dark'];

SigmetListDemoSnapshotDarkTheme.parameters = {
  zeplinLink: sigmetDesignLinks,
};

export const SigmetListDemoSnapshotSmall = (): React.ReactElement => {
  return (
    <SigmetListComponent maxWidth={320} productList={fakeStaticSigmetList} />
  );
};
SigmetListDemoSnapshotSmall.storyName = 'Sigmet List Small';
SigmetListDemoSnapshotSmall.tags = ['snapshot'];

const AirmetListComponent: React.FC<ListProps> = ({
  productList,
  maxWidth = 1200,
  isLoading = false,
  listError = null,
}: ListProps) => {
  return (
    <Paper
      style={{
        padding: '24px',
        maxWidth,
      }}
    >
      <ProductList
        productList={productList}
        productType="airmet"
        productConfig={airmetConfig}
        isLoading={isLoading}
        error={listError!}
        onClickProductRow={(): void => {}}
        onClickNewProduct={(): void => {}}
      />
    </Paper>
  );
};

export const AirmetListDemo = (): React.ReactElement => {
  return <AirmetListComponent productList={fakeAirmetList} />;
};

const airmetDesignLinks = [
  {
    name: 'Light theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a497a1065b071285afb24d/version/6213a1c471010412c9ee5b56',
  },
  {
    name: 'Dark theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a497a84cf218112a0c79ab/version/6213a1f1447afc122858f76e',
  },
  {
    name: 'responsive',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5f4e0b5cef6167b007682172/version/6213a41831d996150da6af09',
  },
];

export const AirmetListDemoSnapshot = (): React.ReactElement => {
  return <AirmetListComponent productList={fakeStaticAirmetList} />;
};

AirmetListDemoSnapshot.storyName = 'Airmet List Demo light theme';
AirmetListDemoSnapshot.tags = ['snapshot'];

AirmetListDemoSnapshot.parameters = {
  zeplinLink: airmetDesignLinks,
};

export const AirmetListDemoSnapshotDarkTheme = (): React.ReactElement => {
  return <AirmetListComponent productList={fakeStaticAirmetList} />;
};

AirmetListDemoSnapshotDarkTheme.storyName = 'Airmet List Demo dark theme';
AirmetListDemoSnapshotDarkTheme.tags = ['snapshot', 'dark'];

AirmetListDemoSnapshotDarkTheme.parameters = {
  zeplinLink: airmetDesignLinks,
};

export const AirmetListDemoSnapshotSmall = (): React.ReactElement => {
  return (
    <AirmetListComponent maxWidth={320} productList={fakeStaticAirmetList} />
  );
};

AirmetListDemoSnapshotSmall.storyName = 'Airmet List Small';
AirmetListDemoSnapshotSmall.tags = ['snapshot'];
