/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { Button } from '@mui/material';
import ContentDialog from './ContentDialog';
import { translateKeyOutsideComponents } from '../../utils/i18n';

describe('src/components/ContentDialog/ContentDialog', () => {
  it('should trigger toggleDialogStatus when clicking the back button', () => {
    const props = {
      open: true,
      title: 'Title',
      toggleDialogStatus: jest.fn(),
    };
    render(<ContentDialog {...props} />);

    expect(screen.getByRole('dialog')).toBeTruthy();
    fireEvent.click(screen.getByText(translateKeyOutsideComponents('back')));
    expect(props.toggleDialogStatus).toHaveBeenCalledTimes(1);
  });

  it('should show the given title and content', () => {
    const props = {
      open: true,
      title: 'Title',
      toggleDialogStatus: jest.fn(),
    };
    const testContent = 'some test content';
    render(
      <ContentDialog {...props}>
        <div data-testid="child">{testContent}</div>
      </ContentDialog>,
    );

    expect(screen.getByTestId('dialogTitle').textContent).toEqual(props.title);
    expect(screen.getByTestId('child')).toBeTruthy();
  });

  it('should show the given options in the dialog', () => {
    const props = {
      open: true,
      title: 'Title',
      toggleDialogStatus: jest.fn(),
      options: (
        <div>
          <Button data-testid="optionButton" color="primary">
            {translateKeyOutsideComponents('save')}
          </Button>
          <Button data-testid="optionButton" color="secondary">
            {translateKeyOutsideComponents('delete')}
          </Button>
        </div>
      ),
    };
    render(<ContentDialog {...props} />);
    expect(screen.getAllByTestId('optionButton').length).toEqual(2);
  });
});
