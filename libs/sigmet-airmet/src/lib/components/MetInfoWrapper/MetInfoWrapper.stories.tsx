/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { ApiProvider } from '@opengeoweb/api';
import { lightTheme } from '@opengeoweb/theme';
import { Provider } from 'react-redux';
import { sigmetConfig } from '../../utils/config';
import {
  createMockStore,
  ThemeWrapperWithModules,
} from '../../utils/testUtils';
import MetInfoWrapper from './MetInfoWrapper';
import { createApi as createFakeApi } from '../../utils/fakeApi';

export default { title: 'components/MetInfoWrapper', tags: ['!autodocs'] };

const Wrapper = (): React.ReactElement => {
  const auth1 = {
    username: 'test',
    token: '2094023',
    refresh_token: 'di98i3k',
  };
  const [auth, onSetAuth] = React.useState(auth1);

  setTimeout(() => {
    // eslint-disable-next-line no-console
    console.log('set new auth');
    onSetAuth({
      username: 'test',
      token: '23aserf',
      refresh_token: '324kjkj',
    });
  }, 6000);

  return (
    <ApiProvider auth={auth} onSetAuth={onSetAuth} createApi={createFakeApi}>
      <Provider store={createMockStore({})}>
        <ThemeWrapperWithModules theme={lightTheme}>
          <MetInfoWrapper productType="sigmet" productConfig={sigmetConfig} />
        </ThemeWrapperWithModules>
      </Provider>
    </ApiProvider>
  );
};

export const RerenderDemo = (): React.ReactElement => {
  return <Wrapper />;
};
