/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { act, render, screen, waitFor } from '@testing-library/react';
import { AuthenticationProvider } from '@opengeoweb/authentication';
import DownloadSigmetAirmetConfigWrapper from './DownloadSigmetAirmetConfigWrapper';
import { TestWrapper } from '../../utils/testUtils';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { SigmetAirmetApi } from '../../utils/api';

describe('components/MetInfoWrapper/DownloadSigmetAirmetConfigWrapper', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });
  it('should render correctly with config', async () => {
    render(
      <TestWrapper createApi={createFakeApi}>
        <AuthenticationProvider>
          <DownloadSigmetAirmetConfigWrapper productType="sigmet" />
        </AuthenticationProvider>
      </TestWrapper>,
    );
    expect(screen.getByTestId('loadingSpinner')).toBeTruthy();
    await act(async () => jest.advanceTimersByTime(2000));
    await waitFor(() => {
      expect(screen.queryByTestId('loadingSpinner')).toBeFalsy();
    });
    expect(screen.getByTestId('sigmetAirmetComponent')).toBeTruthy();
  });

  it('should fetch correct sigmet config from api', async () => {
    const sigmetRequest = jest.fn();
    const fakeApi = (): SigmetAirmetApi => ({
      ...createFakeApi(),
      getSigmetConfiguration: sigmetRequest,
    });

    render(
      <TestWrapper createApi={fakeApi}>
        <AuthenticationProvider>
          <DownloadSigmetAirmetConfigWrapper productType="sigmet" />
        </AuthenticationProvider>
      </TestWrapper>,
    );

    expect(screen.getByTestId('loadingSpinner')).toBeTruthy();
    await waitFor(() => {
      expect(screen.queryByTestId('sigmetAirmetComponent')).toBeFalsy();
    });

    expect(sigmetRequest).toHaveBeenCalled();
  });

  it('should fetch correct airmet config from api', async () => {
    const airmetRequest = jest.fn();
    const fakeApi = (): SigmetAirmetApi => ({
      ...createFakeApi(),
      getAirmetConfiguration: airmetRequest,
    });

    render(
      <TestWrapper createApi={fakeApi}>
        <AuthenticationProvider>
          <DownloadSigmetAirmetConfigWrapper productType="airmet" />
        </AuthenticationProvider>
      </TestWrapper>,
    );

    expect(screen.getByTestId('loadingSpinner')).toBeTruthy();
    await waitFor(() => {
      expect(screen.queryByTestId('sigmetAirmetComponent')).toBeFalsy();
    });
    expect(airmetRequest).toHaveBeenCalled();
  });

  it('should not render without config', async () => {
    const airmetRequest = jest.fn();
    const fakeApi = (): SigmetAirmetApi => ({
      ...createFakeApi(),
      getAirmetConfiguration: airmetRequest,
    });

    render(
      <TestWrapper createApi={fakeApi}>
        <AuthenticationProvider>
          <DownloadSigmetAirmetConfigWrapper productType="airmet" />
        </AuthenticationProvider>
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('sigmetAirmetComponent')).toBeFalsy();
    });
  });
});
