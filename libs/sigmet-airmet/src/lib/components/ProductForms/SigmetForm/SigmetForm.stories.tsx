/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import {
  defaultFormOptions,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';
import {
  CancelSigmet,
  LevelUnits,
  Sigmet,
  AviationPhenomenaCode,
} from '../../../types';
import { sigmetConfig } from '../../../utils/config';
import {
  fakeSigmetList,
  fakeVolcanicCancelSigmetWithMoveTo,
} from '../../../utils/mockdata/fakeSigmetList';

import { SnapshotStoryWrapper } from '../../../utils/testUtils';
import { getValidFromXHoursBeforeMessage } from '../ProductFormFields/ValidFrom';
import { getFir } from '../utils';
import SigmetForm from './SigmetForm';
import { translateKeyOutsideComponents } from '../../../utils/i18n';

export default {
  title: 'components/SigmetForm',
  tags: ['!autodocs'],
};

const zeplinThemeLinkLight = [
  {
    name: 'Light theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a4934891039316017011cd/version/63453b21d90f056999965f74',
  },
];

const zeplinThemeLinkDark = [
  {
    name: 'Dark theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a4935476031e13fb0f3ab6/version/63453b3bb0834e6a236fa210',
  },
];

// New sigmet
const NewSigmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateStart: '2022-01-01T12:00Z',
            validDateEnd: '2022-01-01T13:00Z',
          },
        }}
      >
        <SigmetForm mode="new" showMap={false} />
      </ReactHookFormProvider>
    </SnapshotStoryWrapper>
  );
};

export const NewSigmetLightTheme = (): React.ReactElement => <NewSigmet />;
NewSigmetLightTheme.storyName = 'New Sigmet light theme';
NewSigmetLightTheme.tags = ['snapshot'];
NewSigmetLightTheme.parameters = { zeplinLink: zeplinThemeLinkLight };

export const NewSigmetDarkTheme = (): React.ReactElement => <NewSigmet />;
NewSigmetDarkTheme.storyName = 'New Sigmet dark theme';
NewSigmetDarkTheme.tags = ['snapshot', 'dark'];
NewSigmetDarkTheme.parameters = { zeplinLink: zeplinThemeLinkDark };

// Error Sigmet
const DemoForm = (): React.ReactElement => {
  const { setError } = useFormContext();

  React.useEffect(() => {
    setTimeout(() => {
      setError('phenomenon', { message: 'this field is required' });
      setError('validDateStart', {
        message: translateKeyOutsideComponents(
          'valid-from-before-current-error',
        ),
      });
      setError('validDateEnd', {
        message: getValidFromXHoursBeforeMessage(4),
      });
    }, 1);
  }, [setError]);

  return <SigmetForm mode="new" showMap={false} />;
};

const ErrorSigmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateStart: '2022-01-01T13:00Z',
            validDateEnd: '2022-01-01T12:00Z',
          },
        }}
      >
        <DemoForm />
      </ReactHookFormProvider>
    </SnapshotStoryWrapper>
  );
};
export const ErrorSigmetLightTheme = (): React.ReactElement => <ErrorSigmet />;
ErrorSigmetLightTheme.storyName = 'Error Sigmet light theme';
ErrorSigmetLightTheme.tags = ['snapshot'];
ErrorSigmetLightTheme.parameters = { zeplinLink: zeplinThemeLinkLight };

export const ErrorSigmetDarkTheme = (): React.ReactElement => <ErrorSigmet />;
ErrorSigmetDarkTheme.storyName = 'Error Sigmet dark theme';
ErrorSigmetDarkTheme.tags = ['snapshot', 'dark'];
ErrorSigmetDarkTheme.parameters = { zeplinLink: zeplinThemeLinkDark };

// Edit Sigmet
const VASigmet = {
  uuid: 'someuniqueidprescibedbyBE3',
  phenomenon: 'VA_CLD' as AviationPhenomenaCode,
  sequence: 'A02V',
  issueDate: '2022-01-02T12:35Z',
  validDateStart: '2022-01-02T11:35Z',
  validDateEnd: '2022-01-02T12:35Z',
  firName: 'AMSTERDAM FIR',
  locationIndicatorATSU: 'EHAA',
  locationIndicatorATSR: 'EHAA',
  locationIndicatorMWO: 'EHDB',
  isObservationOrForecast: 'OBS',
  observationOrForecastTime: '2022-01-02T12:35Z',
  movementType: 'NO_VA_EXP',
  change: 'WKN',
  type: 'NORMAL',
  status: 'PUBLISHED',
  levelInfoMode: 'BETW',
  vaSigmetVolcanoName: 'EYJAFJALLAJOKULL',
  vaSigmetVolcanoCoordinates: { latitude: 63.62, longitude: -19.61 },
  level: {
    value: 1500,
    unit: 'FT' as LevelUnits,
  },
  lowerLevel: {
    value: 1000,
    unit: 'FT' as LevelUnits,
  },
  firGeometry: getFir(sigmetConfig),
  startGeometry: {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        properties: {
          selectionType: 'poly',
        },
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [3.7513346922388284, 52.93209131750574],
              [4.890169687103436, 52.26807619123409],
              [3.250247294498403, 51.960365357854286],
              [3.7513346922388284, 52.93209131750574],
            ],
          ],
        },
      },
    ],
  },
  startGeometryIntersect: {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        properties: {
          selectionType: 'poly',
        },
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [3.7513346922388284, 52.93209131750574],
              [4.890169687103436, 52.26807619123409],
              [3.250247294498403, 51.960365357854286],
              [3.7513346922388284, 52.93209131750574],
            ],
          ],
        },
      },
    ],
  },
};

const EditSigmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            ...VASigmet,
          },
        }}
      >
        <SigmetForm
          mode="edit"
          showMap={false}
          initialSigmet={VASigmet as Sigmet}
          product={VASigmet as Sigmet}
        />
      </ReactHookFormProvider>
    </SnapshotStoryWrapper>
  );
};

export const EditSigmetLightTheme = (): React.ReactElement => <EditSigmet />;
EditSigmetLightTheme.storyName = 'Edit Sigmet light theme';
EditSigmetLightTheme.tags = ['snapshot'];
EditSigmetLightTheme.parameters = { zeplinLink: zeplinThemeLinkLight };

export const EditSigmetDarkTheme = (): React.ReactElement => <EditSigmet />;
EditSigmetDarkTheme.storyName = 'Edit Sigmet dark theme';
EditSigmetDarkTheme.tags = ['snapshot', 'dark'];
EditSigmetDarkTheme.parameters = { zeplinLink: zeplinThemeLinkDark };

// View sigmet
const ViewSigmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            ...fakeSigmetList[7].sigmet,
          },
        }}
      >
        <SigmetForm
          mode="view"
          showMap={false}
          initialSigmet={fakeSigmetList[7].sigmet as Sigmet}
          product={fakeSigmetList[7].sigmet}
        />
      </ReactHookFormProvider>
    </SnapshotStoryWrapper>
  );
};

export const ViewSigmetLightTheme = (): React.ReactElement => <ViewSigmet />;
ViewSigmetLightTheme.storyName = 'View Sigmet light theme';
ViewSigmetLightTheme.tags = ['snapshot'];
ViewSigmetLightTheme.parameters = { zeplinLink: zeplinThemeLinkLight };

export const ViewSigmetDarkTheme = (): React.ReactElement => <ViewSigmet />;
ViewSigmetDarkTheme.storyName = 'View Sigmet dark theme';
ViewSigmetDarkTheme.tags = ['snapshot', 'dark'];
ViewSigmetDarkTheme.parameters = { zeplinLink: zeplinThemeLinkLight };

// Cancel sigmet
const CancelVASigmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            ...fakeVolcanicCancelSigmetWithMoveTo.sigmet,
          },
        }}
      >
        <SigmetForm
          mode="view"
          showMap={false}
          isCancelSigmet={true}
          initialCancelSigmet={
            fakeVolcanicCancelSigmetWithMoveTo.sigmet as CancelSigmet
          }
          product={fakeVolcanicCancelSigmetWithMoveTo.sigmet}
        />
      </ReactHookFormProvider>
    </SnapshotStoryWrapper>
  );
};

export const CancelVASigmetLightTheme = (): React.ReactElement => (
  <CancelVASigmet />
);
CancelVASigmetLightTheme.storyName = 'Cancel volcanic ashes Sigmet light theme';
CancelVASigmetLightTheme.tags = ['snapshot'];
CancelVASigmetLightTheme.parameters = { zeplinLink: zeplinThemeLinkLight };

export const CancelVASigmetDarkTheme = (): React.ReactElement => (
  <CancelVASigmet />
);
CancelVASigmetDarkTheme.storyName = 'Cancel volcanic ashes Sigmet dark theme';
CancelVASigmetDarkTheme.tags = ['snapshot', 'dark'];
CancelVASigmetDarkTheme.parameters = { zeplinLink: zeplinThemeLinkDark };
