/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { LayerType, webmapUtils } from '@opengeoweb/webmap';
import { genericActions, mapUtils } from '@opengeoweb/store';
import { TestWrapper, createMockStore, testFir } from '../../utils/testUtils';
import { sigmetConfig } from '../../utils/config';
import ProductFormLayout from './ProductFormLayout';
import { srsAndBboxDefault } from '../MapViewGeoJson/constants';

describe('components/ProductForms/ProductFormLayout', () => {
  it('should render with default props', () => {
    const TestChild = (): React.ReactElement => <div>test message</div>;
    render(
      <TestWrapper>
        <ProductFormLayout geoJSONLayers={[]} productConfig={sigmetConfig}>
          <TestChild />
        </ProductFormLayout>
      </TestWrapper>,
    );

    expect(screen.getByText('test message')).toBeTruthy();
  });

  it('should show given geoJSON layers', () => {
    const layers = [
      { id: 'test1', geoJSON: testFir },
      {
        id: 'test2',
        geoJSON: {
          type: 'Feature',
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [5.0, 55.0],
                [4.331914, 55.332644],
                [3.368817, 55.764314],
                [2.761908, 54.379261],
                [3.15576, 52.913554],
                [2.000002, 51.500002],
                [3.370001, 51.369722],
                [3.370527, 51.36867],
                [3.362223, 51.320002],
                [3.36389, 51.313608],
                [3.373613, 51.309999],
                [3.952501, 51.214441],
                [4.397501, 51.452776],
                [5.078611, 51.391665],
                [5.848333, 51.139444],
                [5.651667, 50.824717],
                [6.011797, 50.757273],
                [5.934168, 51.036386],
                [6.222223, 51.361666],
                [5.94639, 51.811663],
                [6.405001, 51.830828],
                [7.053095, 52.237764],
                [7.031389, 52.268885],
                [7.063612, 52.346109],
                [7.065557, 52.385828],
                [7.133055, 52.888887],
                [7.14218, 52.898244],
                [7.191667, 53.3],
                [6.5, 53.666667],
                [6.500002, 55.000002],
                [5.0, 55.0],
              ],
            ],
          },
          properties: { selectionType: 'fir' },
        },
      },
    ];
    const TestChild = (): React.ReactElement => <div>test message</div>;
    render(
      <TestWrapper>
        <ProductFormLayout geoJSONLayers={layers} productConfig={sigmetConfig}>
          <TestChild />
        </ProductFormLayout>
      </TestWrapper>,
    );

    const mapLayers = screen.getAllByTestId('mapViewLayer');

    expect(mapLayers[4].textContent).toContain(layers[0].id);
    expect(mapLayers[5].textContent).toContain(layers[1].id);
  });

  it('should set correct given baselayers', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            ...mapUtils.createMap({ id: 'main-map' }),
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
      syncGroups: {
        ...genericActions.initialSyncState,
      },
    };
    const store = createMockStore(mockState);
    const testLayers = [
      {
        id: 'baseLayer-airmet',
        name: 'WorldMap_Light_Grey_Canvas',
        type: 'twms',
        layerType: LayerType.baseLayer,
      },
      {
        id: 'northseastations-airmet',
        name: 'northseastations',
        layerType: LayerType.overLayer,
        service:
          'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserverdataset=OVL',
        format: 'image/png',
        enabled: true,
      },
    ];

    const productConfig = {
      ...sigmetConfig,
      mapPreset: { layers: testLayers },
    };

    testLayers.forEach((testLayer) => {
      expect(store.getState().layers.byId[testLayer.id]).toBeUndefined();
    });

    render(
      <TestWrapper store={store}>
        <ProductFormLayout productConfig={productConfig} geoJSONLayers={[]}>
          some child
        </ProductFormLayout>
      </TestWrapper>,
    );

    testLayers.forEach((testLayer) => {
      const result = store.getState().layers.byId[testLayer.id];
      expect(result).toBeDefined();
      expect(result.id).toEqual(testLayer.id);
      expect(result.name).toEqual(testLayer.name);
      expect(result.type).toEqual(testLayer.type);
      expect(result.layerType).toEqual(testLayer.layerType);
    });
  });

  it('should set correct projection from config', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            ...mapUtils.createMap({ id: 'main-map' }),
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
      syncGroups: {
        ...genericActions.initialSyncState,
      },
    };

    const generatedId = 'sigmet-mapid_1';
    jest.spyOn(webmapUtils, 'generateMapId').mockReturnValueOnce('mapid_1');
    const store = createMockStore(mockState);
    const testProjection = {
      bbox: {
        left: -811501,
        right: 2738819,
        top: 12688874,
        bottom: 5830186,
      },
      srs: 'EPSG:3857',
    };

    const productConfig = {
      ...sigmetConfig,
      mapPreset: { proj: testProjection },
    };

    expect(store.getState().webmap.byId[generatedId]).toBeUndefined();

    render(
      <TestWrapper store={store}>
        <ProductFormLayout productConfig={productConfig} geoJSONLayers={[]}>
          some child
        </ProductFormLayout>
      </TestWrapper>,
    );

    const result = store.getState().webmap.byId[generatedId];
    expect(result).toBeDefined();
    expect(result.srs).toEqual(testProjection.srs);
    expect(result.bbox).toEqual(testProjection.bbox);
  });

  it('should set correct default projection when config does not contain one', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            ...mapUtils.createMap({ id: 'main-map' }),
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
      syncGroups: {
        ...genericActions.initialSyncState,
      },
    };
    const store = createMockStore(mockState);
    const productConfig = {
      ...sigmetConfig,
      mapPreset: {},
    };

    const generatedId = 'sigmet-mapid_1';
    jest.spyOn(webmapUtils, 'generateMapId').mockReturnValueOnce('mapid_1');

    expect(store.getState().webmap.byId[generatedId]).toBeUndefined();

    render(
      <TestWrapper store={store}>
        <ProductFormLayout productConfig={productConfig} geoJSONLayers={[]}>
          some child
        </ProductFormLayout>
      </TestWrapper>,
    );

    const result = store.getState().webmap.byId[generatedId];
    expect(result).toBeDefined();
    expect(result.srs).toEqual(srsAndBboxDefault.srs);
    expect(result.bbox).toEqual(srsAndBboxDefault.bbox);
  });
});
