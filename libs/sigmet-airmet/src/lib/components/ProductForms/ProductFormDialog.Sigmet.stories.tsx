/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';

import { Meta, StoryObj } from '@storybook/react/*';
import {
  fakeSigmetList,
  fakeVolcanicCancelSigmetNoMoveTo,
  fakeVolcanicCancelSigmetWithMoveTo,
} from '../../utils/mockdata/fakeSigmetList';
import { ProductFormDialog, ProductFormDialogProps } from './ProductFormDialog';
import { sigmetConfig } from '../../utils/config';

const meta: Meta<typeof ProductFormDialog> = {
  title: 'components/ProductFormDialog/Sigmet',
  component: ProductFormDialog,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the ProductFormDialog',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof ProductFormDialog>;

const mockedProductFormDialogProps: ProductFormDialogProps = {
  isOpen: true, // SET TO TRUE
  toggleDialogStatus: (): void => {},
  handleRenewProductClick: (): void => {},
  productType: 'sigmet',
  productConfig: sigmetConfig,
};

export const Component: Story = {
  args: {
    ...mockedProductFormDialogProps,
    productListItem: fakeSigmetList[0],
    isOpen: false,
  },
  render: (props) => (
    <div style={{ height: 24 }}>
      <ProductFormDialog {...props} />
    </div>
  ),
  tags: ['!dev'],
};

export const NewSigmet: Story = {
  args: mockedProductFormDialogProps,
  tags: ['!autodocs'],
};

export const EditSigmet: Story = {
  args: { ...mockedProductFormDialogProps, productListItem: fakeSigmetList[0] },
  tags: ['!autodocs'],
};

export const ViewSigmet: Story = {
  args: { ...mockedProductFormDialogProps, productListItem: fakeSigmetList[4] },
  tags: ['!autodocs'],
};

export const ViewSigmetDark: Story = {
  args: { ...mockedProductFormDialogProps, productListItem: fakeSigmetList[4] },
  tags: ['dark', '!autodocs'],
};

export const ViewVolcanicSigmet: Story = {
  tags: ['!autodocs'],
  args: { ...mockedProductFormDialogProps, productListItem: fakeSigmetList[2] },
};

export const ViewEmptyOptionalFieldsSigmet: Story = {
  tags: ['!autodocs'],
  args: { ...mockedProductFormDialogProps, productListItem: fakeSigmetList[7] },
};

export const CancelledSigmet: Story = {
  tags: ['!autodocs'],
  args: { ...mockedProductFormDialogProps, productListItem: fakeSigmetList[5] },
};

export const CancelSigmet: Story = {
  tags: ['!autodocs'],
  args: { ...mockedProductFormDialogProps, productListItem: fakeSigmetList[3] },
};

export const CancelVolcanicSigmetNoMoveTo: Story = {
  tags: ['!autodocs'],
  args: {
    ...mockedProductFormDialogProps,
    productListItem: fakeVolcanicCancelSigmetNoMoveTo,
  },
};

export const CancelVolcanicSigmetInclMoveTo: Story = {
  tags: ['!autodocs'],
  args: {
    ...mockedProductFormDialogProps,
    productListItem: fakeVolcanicCancelSigmetWithMoveTo,
  },
};
