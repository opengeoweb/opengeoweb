/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import {
  fakeDataBox,
  fakeDataPoly,
  fakeSigmetList,
  fakeSigmetMultiPolygon,
} from '../../utils/mockdata/fakeSigmetList';
import { ProductFormDialog, ProductFormDialogProps } from './ProductFormDialog';
import { sigmetConfig } from '../../utils/config';

export default {
  title: 'components/ProductFormDialog/Sigmet/Map Draw Demos',
  tags: ['!autodocs'],
};
const sigmetFormDialogProps: ProductFormDialogProps = {
  isOpen: true,
  productType: 'sigmet',
  productConfig: sigmetConfig,
  toggleDialogStatus: (): void => {},
  handleRenewProductClick: (): void => {},
};
export const NewSigmetDemo = (): React.ReactElement => {
  return <ProductFormDialog {...sigmetFormDialogProps} />;
};

export const DraftSigmetFirPolygonDemo = (): React.ReactElement => {
  return (
    <ProductFormDialog
      {...sigmetFormDialogProps}
      productListItem={fakeSigmetList[0]}
    />
  );
};

export const DraftSigmetBoxPointDemo = (): React.ReactElement => {
  return (
    <ProductFormDialog
      {...sigmetFormDialogProps}
      productListItem={fakeDataBox}
    />
  );
};

export const DraftSigmetPolygonPolygonDemo = (): React.ReactElement => {
  return (
    <ProductFormDialog
      {...sigmetFormDialogProps}
      productListItem={fakeDataPoly}
    />
  );
};

export const DraftSigmetMultiIntersectionsDemo = (): React.ReactElement => {
  return (
    <ProductFormDialog
      {...sigmetFormDialogProps}
      productListItem={fakeSigmetMultiPolygon}
    />
  );
};
