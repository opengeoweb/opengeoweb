/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import {
  render,
  fireEvent,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';
import { dateUtils } from '@opengeoweb/shared';
import AirmetForm from './AirmetForm';
import { noTAC } from '../ProductFormTac';
import { TestWrapper } from '../../../utils/testUtils';
import {
  fakeDraftAirmet,
  fakeAirmetList,
} from '../../../utils/mockdata/fakeAirmetList';
import {
  Airmet,
  AviationPhenomenaCode,
  CloudLevelUnits,
  LevelUnits,
  VisibilityCause,
} from '../../../types';

import { getFir } from '../utils';
import { airmetConfig } from '../../../utils/config';
import { translateKeyOutsideComponents } from '../../../utils/i18n';

describe('components/ProductForms/AirmetForm/AirmetForm', () => {
  it('should show an error message when the start position drawing is removed', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeDraftAirmet },
          }}
        >
          <AirmetForm mode="edit" initialAirmet={fakeDraftAirmet} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const deleteButtonStartGeometry = within(
      within(screen.getByTestId('startGeometry')).getByTestId(
        'drawtools-delete',
      ),
    ).getByRole('radio');

    fireEvent.click(deleteButtonStartGeometry!);
    await screen.findByText(
      translateKeyOutsideComponents('geometry-coodinates-empty-message'),
    );
  });

  it('should remove the error message for start position when selecting fir', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeDraftAirmet },
          }}
        >
          <AirmetForm mode="edit" initialAirmet={fakeDraftAirmet} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const deleteButtonStartGeometry = within(
      within(screen.getByTestId('startGeometry')).getByTestId(
        'drawtools-delete',
      ),
    ).getByRole('radio');
    fireEvent.click(deleteButtonStartGeometry!);
    await screen.findByText(
      translateKeyOutsideComponents('geometry-coodinates-empty-message'),
    );

    const firButtonStartGeometry = within(
      within(screen.getByTestId('startGeometry')).getByTestId('drawtools-fir'),
    ).getByRole('radio');
    fireEvent.click(firButtonStartGeometry!);

    await waitFor(() =>
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('geometry-coodinates-empty-message'),
        ),
      ).toBeFalsy(),
    );
  });

  it('should show an error message for start position after selecting a FIR location', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeAirmetList[0].airmet },
          }}
        >
          <AirmetForm
            mode="edit"
            initialAirmet={fakeAirmetList[0].airmet as Airmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(
      screen.queryByText(
        translateKeyOutsideComponents('geometry-coodinates-empty-message'),
      ),
    ).toBeNull();

    // change fir location
    fireEvent.mouseDown(await screen.findByLabelText('Select FIR'));
    const menuItem = await screen.findAllByText('BRUSSEL FIR');
    fireEvent.click(menuItem[0]);

    await screen.findByText(
      translateKeyOutsideComponents('geometry-coodinates-empty-message'),
    );
  });

  it('should show an error message when choosing forecast and forecast time is before valid from time', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeDraftAirmet,
            },
          }}
        >
          <AirmetForm mode="edit" initialAirmet={fakeDraftAirmet} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const fcstField = screen.getByTestId('isObservationOrForecast-FCST');
    fireEvent.click(fcstField);

    await screen.findByText(
      translateKeyOutsideComponents('surface-visibility-between-time'),
    );

    expect(
      within(screen.getByTestId('obs-fcst-time'))
        .getByRole('textbox')
        .getAttribute('aria-invalid') === 'true',
    ).toBeTruthy();
  });

  it('should not show an error message when end time is automatically corrected within max hours after changing FIR', async () => {
    const testFirArea = getFir(airmetConfig);
    const mockConfigMultipleFIR = {
      location_indicator_mwo: 'EHDB',
      fir_areas: {
        EHAA: {
          fir_name: 'AMSTERDAM FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          area_preset: 'NL_FIR',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          level_min: {
            FT: 100,
            FL: 50,
          },
          level_max: {
            FT: 4900,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 5,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          phenomenon: [
            { code: 'SFC_VIS', description: 'Surface visibility' },
            { code: 'SFC_WIND', description: 'Surface wind' },
            {
              code: 'ISOL_TSGR',
              description: 'Isolated thunderstorm(s) with hail',
            },
            { code: 'OVC_CLD', description: 'Overcast cloud' },
            { code: 'BKN_CLD', description: 'Broken cloud' },
          ],
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'M'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
          ],
        },
        EBBU: {
          fir_name: 'OTHER FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EBBU',
          location_indicator_atsu: 'EBBU',
          area_preset: 'OTHER_FIR',
          max_hours_of_validity: 2,
          hours_before_validity: 1,
          level_min: {
            FT: 200,
            FL: 50,
          },
          level_max: {
            FT: 4000,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 15,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          phenomenon: [
            { code: 'SFC_VIS', description: 'Surface visibility' },
            { code: 'SFC_WIND', description: 'Surface wind' },
            {
              code: 'ISOL_TSGR',
              description: 'Isolated thunderstorm(s) with hail',
            },
            { code: 'OVC_CLD', description: 'Overcast cloud' },
            { code: 'BKN_CLD', description: 'Broken cloud' },
          ],
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'FL'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
          ],
        },
      },
      valid_from_delay_minutes: 45,
      default_validity_minutes: 90,
      active_firs: ['EHAA', 'EBBU'],
    };

    const fakeAirmet = {
      ...fakeAirmetList[0].airmet,
      validDateStart: dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 2 }),
      ),
      validDateEnd: dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 5 }),
      ),
      movementType: 'MOVEMENT',
      movementSpeed: 10,
      levelInfoMode: 'BETW',
      level: {
        value: 4500,
        unit: 'FT' as LevelUnits,
      },
      lowerLevel: {
        value: 150,
        unit: 'FT' as LevelUnits,
      },
    } as Airmet;

    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeAirmet },
          }}
        >
          <AirmetForm
            mode="edit"
            initialAirmet={fakeAirmet}
            productConfig={mockConfigMultipleFIR}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    // Change FIR location
    fireEvent.mouseDown(
      await screen.findByLabelText(
        translateKeyOutsideComponents('select-fir-label'),
      ),
    );
    const menuItem = await screen.findAllByText('OTHER FIR');
    fireEvent.click(menuItem[0]);

    // Since the form corrects automatically, there should be no error
    await waitFor(() => {
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('valid-until-time-error', { hours: 2 }),
        ),
      ).toBeFalsy();
    });
  });

  it('should show an error message for startdate, movementSpeed, upperLevel and lowerLevel after changing FIR location. Not for enddate', async () => {
    const testFirArea = getFir(airmetConfig);
    const mockConfigMultipleFIR = {
      location_indicator_mwo: 'EHDB',
      fir_areas: {
        EHAA: {
          fir_name: 'AMSTERDAM FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          area_preset: 'NL_FIR',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          level_min: {
            FT: 100,
            FL: 50,
          },
          level_max: {
            FT: 4900,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 5,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          phenomenon: [
            { code: 'SFC_VIS', description: 'Surface visibility' },
            { code: 'SFC_WIND', description: 'Surface wind' },
            {
              code: 'ISOL_TSGR',
              description: 'Isolated thunderstorm(s) with hail',
            },
            { code: 'OVC_CLD', description: 'Overcast cloud' },
            { code: 'BKN_CLD', description: 'Broken cloud' },
          ],
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'M'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
          ],
        },
        EBBU: {
          fir_name: 'OTHER FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EBBU',
          location_indicator_atsu: 'EBBU',
          area_preset: 'OTHER_FIR',
          max_hours_of_validity: 2,
          hours_before_validity: 1,
          level_min: {
            FT: 200,
            FL: 50,
          },
          level_max: {
            FT: 4000,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 15,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          phenomenon: [
            { code: 'SFC_VIS', description: 'Surface visibility' },
            { code: 'SFC_WIND', description: 'Surface wind' },
            {
              code: 'ISOL_TSGR',
              description: 'Isolated thunderstorm(s) with hail',
            },
            { code: 'OVC_CLD', description: 'Overcast cloud' },
            { code: 'BKN_CLD', description: 'Broken cloud' },
          ],
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'FL'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
          ],
        },
      },
      valid_from_delay_minutes: 45,
      default_validity_minutes: 90,
      active_firs: ['EHAA', 'EBBU'],
    };
    const fakeAirmet = {
      ...fakeAirmetList[0].airmet,
      validDateStart: dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 2 }),
      ),
      validDateEnd: dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 5 }),
      ),
      movementType: 'MOVEMENT',
      movementSpeed: 10,
      levelInfoMode: 'BETW',
      level: {
        value: 4500,
        unit: 'FT' as LevelUnits,
      },
      lowerLevel: {
        value: 150,
        unit: 'FT' as LevelUnits,
      },
    } as Airmet;

    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeAirmet },
          }}
        >
          <AirmetForm
            mode="edit"
            initialAirmet={fakeAirmet}
            productConfig={mockConfigMultipleFIR}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    // change fir location
    fireEvent.mouseDown(
      await screen.findByLabelText(
        translateKeyOutsideComponents('select-fir-label'),
      ),
    );
    const menuItem = await screen.findAllByText('OTHER FIR');
    fireEvent.click(menuItem[0]);

    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).not.toHaveLength(0);
    });

    expect(
      screen.getByText(
        translateKeyOutsideComponents('valid-from-time-error', { hours: 1 }),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents(
          'progress-the-minimum-level-in-movement',
          { movementUnitType: 'kt', movementUnit: 15 },
        ),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('levels-the-minimum-level', {
          levelUnit: 'ft',
          minLevelValue: 200,
        }),
      ),
    ).toBeTruthy();

    expect(
      screen.getByText(
        translateKeyOutsideComponents('levels-the-maximum-level', {
          levelUnit: 'ft',
          maxLevelValue: 4000,
        }),
      ),
    ).toBeTruthy();

    // change fir location back
    fireEvent.mouseDown(
      await screen.findByLabelText(
        translateKeyOutsideComponents('select-fir-label'),
      ),
    );
    const menuItem1 = await screen.findByText('AMSTERDAM FIR');
    fireEvent.click(menuItem1);

    await waitFor(() => {
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('valid-from-time-error', { hours: 1 }),
        ),
      ).toBeFalsy();
    });
    expect(
      screen.queryByText(
        translateKeyOutsideComponents(
          'progress-the-minimum-level-in-movement',
          { movementUnitType: 'kt', movementUnit: 15 },
        ),
      ),
    ).toBeFalsy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('levels-the-minimum-level', {
          levelUnit: 'ft',
          minLevelValue: 200,
        }),
      ),
    ).toBeFalsy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('levels-the-maximum-level', {
          levelUnit: 'ft',
          maxLevelValue: 4000,
        }),
      ),
    ).toBeFalsy();
  });

  it('should correctly update validUntil based on phenomenon changes in Airmet form', async () => {
    const testFirArea = getFir(airmetConfig);

    const mockConfigMultipleFIR = {
      location_indicator_mwo: 'EHDB',
      fir_areas: {
        EHAA: {
          fir_name: 'AMSTERDAM FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          area_preset: 'NL_FIR',
          max_hours_of_validity: 4, // Default max validity for other phenomena
          hours_before_validity: 4,
          phenomenon: [
            { code: 'SFC_VIS', description: 'Surface visibility' },
            { code: 'SFC_WIND', description: 'Surface wind' },
            { code: 'ISOL_TS', description: 'Isolated thunderstorm(s)' },
            {
              code: 'ISOL_TSGR',
              description: 'Isolated thunderstorm(s) with hail',
            },
            { code: 'OVC_CLD', description: 'Overcast cloud' },
            { code: 'BKN_CLD', description: 'Broken cloud' },
          ],
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'M'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
          ],
        },
      },
      valid_from_delay_minutes: 30, // Example delay for Airmet
      default_validity_minutes: 90,
      active_firs: ['EHAA'],
    };

    const fakeAirmet = {
      ...fakeAirmetList[0].airmet,
      validDateStart: dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 2 }),
      ),
      validDateEnd: dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 4 }),
      ),
      locationIndicatorATSR: 'EHAA',
      movementType: 'MOVEMENT',
      movementSpeed: 10,
      levelInfoMode: 'BETW',
      level: {
        value: 4500,
        unit: 'FT' as LevelUnits,
      },
      lowerLevel: {
        value: 150,
        unit: 'FT' as LevelUnits,
      },
    } as Airmet;

    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeAirmet },
          }}
        >
          <AirmetForm
            mode="edit"
            initialAirmet={fakeAirmet}
            productConfig={mockConfigMultipleFIR}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    // Simulate changing phenomenon to 'ISOL_TS' which has 2-hour max validity
    fireEvent.mouseDown(
      within(screen.getByTestId('phenomenon')).getByRole('combobox'),
    );
    const isolTsOption = await screen.findByText('Isolated thunderstorm(s)');
    fireEvent.click(isolTsOption);

    // Check validUntil after changing phenomenon to 'ISOL_TS'
    await waitFor(() => {
      const validUntilInput = within(
        screen.getByTestId('valid-until'),
      ).getByRole('textbox');
      const expectedValidUntilTime = dateUtils.add(
        new Date(fakeAirmet.validDateStart),
        {
          hours: 2,
        },
      );

      expect(validUntilInput.getAttribute('value')).toEqual(
        dateUtils.dateToString(
          expectedValidUntilTime,
          dateUtils.DATE_FORMAT_DATEPICKER,
        ),
      );
    });

    // Simulate changing to phenomenon with 4-hour validity
    fireEvent.mouseDown(
      within(screen.getByTestId('phenomenon')).getByRole('combobox'),
    );
    const vaCldOption = await screen.findByText('Broken cloud');
    fireEvent.click(vaCldOption);

    await waitFor(() => {
      const validUntilInput = within(
        screen.getByTestId('valid-until'),
      ).getByRole('textbox');
      const expectedValidUntilTime = dateUtils.add(
        new Date(fakeAirmet.validDateStart),
        {
          hours: 4,
        },
      );

      expect(validUntilInput.getAttribute('value')).toEqual(
        dateUtils.dateToString(
          expectedValidUntilTime,
          dateUtils.DATE_FORMAT_DATEPICKER,
        ),
      );
    });

    await waitFor(() => {
      const errorMessages = screen.queryAllByRole('alert');
      expect(errorMessages).toHaveLength(0);
    });
  });

  it('should show an error message for windDirection and windSpeed after changing FIR location', async () => {
    const testFirArea = getFir(airmetConfig);
    const mockConfigMultipleFIR = {
      location_indicator_mwo: 'EHDB',
      fir_areas: {
        EHAA: {
          fir_name: 'AMSTERDAM FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          area_preset: 'NL_FIR',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          level_min: {
            FT: 100,
            FL: 50,
          },
          level_max: {
            FT: 4900,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 5,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          phenomenon: [
            { code: 'SFC_VIS', description: 'Surface visibility' },
            { code: 'SFC_WIND', description: 'Surface wind' },
            {
              code: 'ISOL_TSGR',
              description: 'Isolated thunderstorm(s) with hail',
            },
            { code: 'OVC_CLD', description: 'Overcast cloud' },
            { code: 'BKN_CLD', description: 'Broken cloud' },
          ],
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'M'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
            {
              unit_type: 'surfacewind_unit',
              allowed_units: ['KT'],
            },
            {
              unit_type: 'cloud_level_unit',
              allowed_units: ['FT'],
            },
          ],
          wind_direction_rounding: 10,
          wind_speed_min: {
            KT: 31,
          },
          wind_speed_max: {
            KT: 199,
          },
        },
        EBBU: {
          fir_name: 'OTHER FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EBBU',
          location_indicator_atsu: 'EBBU',
          area_preset: 'OTHER_FIR',
          max_hours_of_validity: 2,
          hours_before_validity: 1,
          level_min: {
            FT: 200,
            FL: 50,
          },
          level_max: {
            FT: 4000,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 15,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          phenomenon: [
            { code: 'SFC_VIS', description: 'Surface visibility' },
            { code: 'SFC_WIND', description: 'Surface wind' },
            {
              code: 'ISOL_TSGR',
              description: 'Isolated thunderstorm(s) with hail',
            },
            { code: 'OVC_CLD', description: 'Overcast cloud' },
            { code: 'BKN_CLD', description: 'Broken cloud' },
          ],
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'FL'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
            {
              unit_type: 'surfacewind_unit',
              allowed_units: ['KT'],
            },
            {
              unit_type: 'cloud_level_unit',
              allowed_units: ['FT'],
            },
          ],
          wind_direction_rounding: 25,
          wind_speed_min: {
            KT: 35,
          },
          wind_speed_max: {
            KT: 199,
          },
        },
      },
      valid_from_delay_minutes: 45,
      default_validity_minutes: 90,
      active_firs: ['EHAA', 'EBBU'],
    };
    const fakeAirmet = {
      ...fakeAirmetList[0].airmet,
      phenomenon: 'SFC_WIND' as AviationPhenomenaCode,
      windDirection: 10,
      windSpeed: 31,
    } as Airmet;

    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeAirmet },
          }}
        >
          <AirmetForm
            mode="edit"
            initialAirmet={fakeAirmet}
            productConfig={mockConfigMultipleFIR}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });

    // change fir location
    fireEvent.mouseDown(await screen.findByLabelText('Select FIR'));
    const menuItem = await screen.findAllByText('OTHER FIR');
    fireEvent.click(menuItem[0]);

    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).not.toHaveLength(0);
    });

    expect(
      screen.getByText('Direction must be rounded to the nearest 25 deg.'),
    ).toBeTruthy();
    expect(screen.getByText('The minimum wind speed in kt is 35')).toBeTruthy();

    // change fir location back
    fireEvent.mouseDown(await screen.findByLabelText('Select FIR'));
    const menuItem1 = await screen.findByText('AMSTERDAM FIR');
    fireEvent.click(menuItem1);

    await waitFor(() => {
      expect(
        screen.queryByText('Direction must be rounded to the nearest 25 deg.'),
      ).toBeFalsy();
    });
    expect(
      screen.queryByText('The minimum wind speed in kt is 35'),
    ).toBeFalsy();
  });

  it('should show an error message for visiblity after changing FIR location', async () => {
    const testFirArea = getFir(airmetConfig);
    const mockConfigMultipleFIR = {
      location_indicator_mwo: 'EHDB',
      fir_areas: {
        EHAA: {
          fir_name: 'AMSTERDAM FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          area_preset: 'NL_FIR',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          level_min: {
            FT: 100,
            FL: 50,
          },
          level_max: {
            FT: 4900,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 5,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          phenomenon: [
            { code: 'SFC_VIS', description: 'Surface visibility' },
            { code: 'SFC_WIND', description: 'Surface wind' },
            {
              code: 'ISOL_TSGR',
              description: 'Isolated thunderstorm(s) with hail',
            },
            { code: 'OVC_CLD', description: 'Overcast cloud' },
            { code: 'BKN_CLD', description: 'Broken cloud' },
          ],
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'M'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
            {
              unit_type: 'surfacewind_unit',
              allowed_units: ['KT'],
            },
            {
              unit_type: 'cloud_level_unit',
              allowed_units: ['FT'],
            },
          ],
          wind_direction_rounding: 10,
          wind_speed_min: {
            KT: 31,
          },
          wind_speed_max: {
            KT: 199,
          },
          visibility_max: 4900,
          visibility_min: 0,
          visibility_rounding_below: 50,
          visibility_rounding_above: 100,
        },
        EBBU: {
          fir_name: 'OTHER FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EBBU',
          location_indicator_atsu: 'EBBU',
          area_preset: 'OTHER_FIR',
          max_hours_of_validity: 2,
          hours_before_validity: 1,
          level_min: {
            FT: 200,
            FL: 50,
          },
          level_max: {
            FT: 4000,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 15,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          phenomenon: [
            { code: 'SFC_VIS', description: 'Surface visibility' },
            { code: 'SFC_WIND', description: 'Surface wind' },
            {
              code: 'ISOL_TSGR',
              description: 'Isolated thunderstorm(s) with hail',
            },
            { code: 'OVC_CLD', description: 'Overcast cloud' },
            { code: 'BKN_CLD', description: 'Broken cloud' },
          ],
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'FL'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
            {
              unit_type: 'surfacewind_unit',
              allowed_units: ['KT'],
            },
            {
              unit_type: 'cloud_level_unit',
              allowed_units: ['FT'],
            },
          ],
          wind_direction_rounding: 25,
          wind_speed_min: {
            KT: 35,
          },
          wind_speed_max: {
            KT: 199,
          },
          visibility_max: 4900,
          visibility_min: 250,
          visibility_rounding_below: 50,
          visibility_rounding_above: 100,
        },
      },
      valid_from_delay_minutes: 45,
      default_validity_minutes: 90,
      active_firs: ['EHAA', 'EBBU'],
    };
    const fakeAirmet = {
      ...fakeAirmetList[0].airmet,
      phenomenon: 'SFC_VIS' as AviationPhenomenaCode,
      visibilityValue: 100,
      visibilityCause: 'DZ' as VisibilityCause,
    } as Airmet;

    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeAirmet },
          }}
        >
          <AirmetForm
            mode="edit"
            initialAirmet={fakeAirmet}
            productConfig={mockConfigMultipleFIR}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });

    // change fir location
    fireEvent.mouseDown(await screen.findByLabelText('Select FIR'));
    const menuItem = await screen.findAllByText('OTHER FIR');
    fireEvent.click(menuItem[0]);

    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).not.toHaveLength(0);
    });

    expect(
      screen.getByText('The minimum visibility in meters is 250'),
    ).toBeTruthy();

    // change fir location back
    fireEvent.mouseDown(await screen.findByLabelText('Select FIR'));
    const menuItem1 = await screen.findByText('AMSTERDAM FIR');
    fireEvent.click(menuItem1);

    await waitFor(() => {
      expect(
        screen.queryByText('The minimum visibility in meters is 250'),
      ).toBeFalsy();
    });
  });

  it('should show an error message for cloud upperLevel and cloud lowerLevel after changing FIR location', async () => {
    const testFirArea = getFir(airmetConfig);
    const mockConfigMultipleFIR = {
      location_indicator_mwo: 'EHDB',
      fir_areas: {
        EHAA: {
          fir_name: 'AMSTERDAM FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          area_preset: 'NL_FIR',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          level_min: {
            FT: 100,
            FL: 50,
          },
          level_max: {
            FT: 4900,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 5,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          phenomenon: [
            { code: 'SFC_VIS', description: 'Surface visibility' },
            { code: 'SFC_WIND', description: 'Surface wind' },
            {
              code: 'ISOL_TSGR',
              description: 'Isolated thunderstorm(s) with hail',
            },
            { code: 'OVC_CLD', description: 'Overcast cloud' },
            { code: 'BKN_CLD', description: 'Broken cloud' },
          ],
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'M'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
            {
              unit_type: 'surfacewind_unit',
              allowed_units: ['KT'],
            },
            {
              unit_type: 'cloud_level_unit',
              allowed_units: ['FT'],
            },
          ],
          cloud_lower_level_min: {
            FT: 100,
          },
          cloud_lower_level_max: {
            FT: 900,
          },
          cloud_level_min: {
            FT: 100,
          },
          cloud_level_max: {
            FT: 9900,
          },
        },
        EBBU: {
          fir_name: 'OTHER FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EBBU',
          location_indicator_atsu: 'EBBU',
          area_preset: 'OTHER_FIR',
          max_hours_of_validity: 2,
          hours_before_validity: 1,
          level_min: {
            FT: 200,
            FL: 50,
          },
          level_max: {
            FT: 4000,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 15,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          phenomenon: [
            { code: 'SFC_VIS', description: 'Surface visibility' },
            { code: 'SFC_WIND', description: 'Surface wind' },
            {
              code: 'ISOL_TSGR',
              description: 'Isolated thunderstorm(s) with hail',
            },
            { code: 'OVC_CLD', description: 'Overcast cloud' },
            { code: 'BKN_CLD', description: 'Broken cloud' },
          ],
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'FL'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
            {
              unit_type: 'surfacewind_unit',
              allowed_units: ['KT'],
            },
            {
              unit_type: 'cloud_level_unit',
              allowed_units: ['FT'],
            },
          ],
          cloud_lower_level_min: {
            FT: 200,
          },
          cloud_lower_level_max: {
            FT: 900,
          },
          cloud_level_min: {
            FT: 100,
          },
          cloud_level_max: {
            FT: 8000,
          },
        },
      },
      valid_from_delay_minutes: 45,
      default_validity_minutes: 90,
      active_firs: ['EHAA', 'EBBU'],
    };
    const fakeAirmet = {
      ...fakeAirmetList[0].airmet,
      phenomenon: 'OVC_CLD' as AviationPhenomenaCode,
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 8500,
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 100,
        unit: 'FT' as CloudLevelUnits,
      },
    } as Airmet;

    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeAirmet },
          }}
        >
          <AirmetForm
            mode="edit"
            initialAirmet={fakeAirmet}
            productConfig={mockConfigMultipleFIR}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });

    // change fir location
    fireEvent.mouseDown(await screen.findByLabelText('Select FIR'));
    const menuItem = await screen.findAllByText('OTHER FIR');
    fireEvent.click(menuItem[0]);

    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).not.toHaveLength(0);
    });

    expect(screen.getByText('The minimum level in ft is 200')).toBeTruthy();
    expect(screen.getByText('The maximum level in ft is 8000')).toBeTruthy();

    // change fir location back
    fireEvent.mouseDown(await screen.findByLabelText('Select FIR'));
    const menuItem1 = await screen.findByText('AMSTERDAM FIR');
    fireEvent.click(menuItem1);

    await waitFor(() => {
      expect(screen.queryByText('The minimum level in ft is 200')).toBeFalsy();
    });
    expect(screen.queryByText('The maximum level in ft is 8000')).toBeFalsy();
  });
});
