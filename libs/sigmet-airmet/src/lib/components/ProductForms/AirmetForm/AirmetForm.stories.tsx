/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import {
  defaultFormOptions,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';
import { Airmet, CancelAirmet } from '../../../types';
import { fakeAirmetList } from '../../../utils/mockdata/fakeAirmetList';

import { SnapshotStoryWrapper } from '../../../utils/testUtils';
import { getValidFromXHoursBeforeMessage } from '../ProductFormFields/ValidFrom';
import AirmetForm from './AirmetForm';
import { translateKeyOutsideComponents } from '../../../utils/i18n';

export default {
  title: 'components/AirmetForm',
  tags: ['!autodocs'],
};

const zeplinThemeLinkLight = [
  {
    name: 'Light theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6059eb82ce4a5a0774bbc476/version/6213a1b24935c915b0ac0ab2',
  },
];

const zeplinThemeLinkDark = [
  {
    name: 'Dark theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60b75c65eeebf325e9420a2c/version/6213a1deaafe6212a61c3677',
  },
];

// New airmet
const NewAirmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateStart: '2022-01-01T12:00Z',
            validDateEnd: '2022-01-01T13:00Z',
          },
        }}
      >
        <AirmetForm mode="new" showMap={false} />
      </ReactHookFormProvider>
    </SnapshotStoryWrapper>
  );
};

export const NewAirmetLightTheme = (): React.ReactElement => <NewAirmet />;
NewAirmetLightTheme.storyName = 'New Airmet light theme';
NewAirmetLightTheme.tags = ['snapshot'];
NewAirmetLightTheme.parameters = { zeplinLink: zeplinThemeLinkLight };

export const NewAirmetDarkTheme = (): React.ReactElement => <NewAirmet />;
NewAirmetDarkTheme.storyName = 'New Airmet dark theme';
NewAirmetDarkTheme.tags = ['snapshot', 'dark'];
NewAirmetDarkTheme.parameters = { zeplinLink: zeplinThemeLinkDark };

// Error airmet
const DemoForm = (): React.ReactElement => {
  const { setError } = useFormContext();

  React.useEffect(() => {
    setTimeout(() => {
      setError('phenomenon', { message: 'this field is required' });
      setError('validDateStart', {
        message: translateKeyOutsideComponents(
          'valid-from-before-current-error',
        ),
      });
      setError('validDateEnd', {
        message: getValidFromXHoursBeforeMessage(4),
      });
    }, 1);
  }, [setError]);

  return <AirmetForm mode="new" showMap={false} />;
};

const ErrorAirmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateStart: '2022-01-01T12:00Z',
            validDateEnd: '2022-01-01T13:00Z',
          },
        }}
      >
        <DemoForm />
      </ReactHookFormProvider>
    </SnapshotStoryWrapper>
  );
};

export const ErrorAirmetLightTheme = (): React.ReactElement => <ErrorAirmet />;
ErrorAirmetLightTheme.storyName = 'Error airmet light theme';
ErrorAirmetLightTheme.tags = ['snapshot'];
ErrorAirmetLightTheme.parameters = { zeplinLink: zeplinThemeLinkLight };

export const ErrorAirmetDarkTheme = (): React.ReactElement => <ErrorAirmet />;
ErrorAirmetDarkTheme.storyName = 'Error airmet dark theme';
ErrorAirmetDarkTheme.tags = ['snapshot', 'dark'];
ErrorAirmetDarkTheme.parameters = { zeplinLink: zeplinThemeLinkDark };

// Edit airmet
const EditAirmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            ...fakeAirmetList[5].airmet,
            validDateStart: '2020-09-17T11:00Z',
            validDateEnd: '2020-09-17T12:00Z',
            observationOrForecastTime: '2020-09-17T12:00Z',
          },
        }}
      >
        <AirmetForm
          mode="edit"
          showMap={false}
          initialAirmet={fakeAirmetList[5].airmet as Airmet}
          product={fakeAirmetList[5].airmet}
        />
      </ReactHookFormProvider>
    </SnapshotStoryWrapper>
  );
};
export const EditAirmetLightTheme = (): React.ReactElement => <EditAirmet />;
EditAirmetLightTheme.storyName = 'Edit Airmet light theme';
EditAirmetLightTheme.tags = ['snapshot'];
EditAirmetLightTheme.parameters = { zeplinLink: zeplinThemeLinkLight };

export const EditAirmetDarkTheme = (): React.ReactElement => <EditAirmet />;
EditAirmetDarkTheme.storyName = 'Edit Airmet dark theme';
EditAirmetDarkTheme.tags = ['snapshot', 'dark'];
EditAirmetDarkTheme.parameters = { zeplinLink: zeplinThemeLinkDark };

// View airmet
const ViewAirmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            ...fakeAirmetList[1].airmet,
            validDateStart: '2020-09-17T11:00Z',
            validDateEnd: '2020-09-17T12:00Z',
            observationOrForecastTime: '2020-09-17T12:00Z',
          },
        }}
      >
        <AirmetForm
          mode="view"
          showMap={false}
          initialAirmet={fakeAirmetList[1].airmet as Airmet}
          product={fakeAirmetList[1].airmet}
        />
      </ReactHookFormProvider>
    </SnapshotStoryWrapper>
  );
};
export const ViewAirmetLightTheme = (): React.ReactElement => <ViewAirmet />;
ViewAirmetLightTheme.storyName = 'View Airmet light theme';
ViewAirmetLightTheme.tags = ['snapshot'];
ViewAirmetLightTheme.parameters = { zeplinLink: zeplinThemeLinkLight };

export const ViewAirmetDarkTheme = (): React.ReactElement => <ViewAirmet />;
ViewAirmetDarkTheme.storyName = 'View Airmet dark theme';
ViewAirmetDarkTheme.tags = ['snapshot', 'dark'];
ViewAirmetDarkTheme.parameters = { zeplinLink: zeplinThemeLinkDark };

// Cancel airmet
const CancelledAirmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            ...fakeAirmetList[3].airmet,
            validDateStart: '2020-09-17T11:00Z',
            validDateEnd: '2020-09-17T12:00Z',
            observationOrForecastTime: '2020-09-17T12:00Z',
          },
        }}
      >
        <AirmetForm
          mode="view"
          showMap={false}
          isCancelAirmet={true}
          initialCancelAirmet={fakeAirmetList[3].airmet as CancelAirmet}
          product={fakeAirmetList[3].airmet}
        />
      </ReactHookFormProvider>
    </SnapshotStoryWrapper>
  );
};

export const CancelAirmetLightTheme = (): React.ReactElement => (
  <CancelledAirmet />
);
CancelAirmetLightTheme.storyName = 'Cancel Airmet light theme';
CancelAirmetLightTheme.tags = ['snapshot'];
CancelAirmetLightTheme.parameters = { zeplinLink: zeplinThemeLinkLight };

export const CancelAirmetDarkTheme = (): React.ReactElement => (
  <CancelledAirmet />
);
CancelAirmetDarkTheme.storyName = 'Cancel Airmet dark theme';
CancelAirmetDarkTheme.tags = ['snapshot', 'dark'];
CancelAirmetDarkTheme.parameters = { zeplinLink: zeplinThemeLinkDark };
