/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { airmetConfig } from '../../utils/config';
import { fakeAirmetList } from '../../utils/mockdata/fakeAirmetList';

import { createFakeApiWithErrors } from '../../utils/testUtils';
import { ProductFormDialog, ProductFormDialogProps } from './ProductFormDialog';

const meta: Meta<typeof ProductFormDialog> = {
  title: 'components/ProductFormDialog/Airmet',
  component: ProductFormDialog,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the ProductFormDialog',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof ProductFormDialog>;

const airmetFormDialogProps: ProductFormDialogProps = {
  isOpen: true,
  productType: 'airmet',
  productConfig: airmetConfig,
  toggleDialogStatus: (): void => {},
  handleRenewProductClick: (): void => {},
};

export const Component: Story = {
  args: {
    ...airmetFormDialogProps,
    productListItem: fakeAirmetList[2],
    isOpen: false,
  },
  render: (props) => (
    <div style={{ height: 24 }}>
      <ProductFormDialog {...props} />
    </div>
  ),
  tags: ['!dev'],
};

export const NewAirmet: Story = {
  args: { ...airmetFormDialogProps, productListItem: null! },
  tags: ['!autodocs'],
};

export const ViewAirmet: Story = {
  args: { ...airmetFormDialogProps, productListItem: fakeAirmetList[2] },
  tags: ['!autodocs'],
};

export const ViewAirmetSurfaceVisibility: Story = {
  args: { ...airmetFormDialogProps, productListItem: fakeAirmetList[1] },
  tags: ['!autodocs'],
};

export const ViewAirmetSurfaceWind: Story = {
  args: { ...airmetFormDialogProps, productListItem: fakeAirmetList[5] },
  tags: ['!autodocs'],
};

export const ViewBrokenCloud: Story = {
  args: { ...airmetFormDialogProps, productListItem: fakeAirmetList[7] },
  tags: ['!autodocs'],
};

export const ViewOvercastCloud: Story = {
  args: { ...airmetFormDialogProps, productListItem: fakeAirmetList[4] },
  tags: ['!autodocs'],
};

export const EditAirmet: Story = {
  args: { ...airmetFormDialogProps, productListItem: fakeAirmetList[0] },
  tags: ['!autodocs'],
};

export const CancelAirmet: Story = {
  args: { ...airmetFormDialogProps, productListItem: fakeAirmetList[3] },
  tags: ['!autodocs'],
};

export const BackendErrorMsgOnSaveOrPublish: Story = {
  args: { ...airmetFormDialogProps, productListItem: fakeAirmetList[0] },
  tags: ['!autodocs'],
  parameters: {
    createApi: createFakeApiWithErrors,
  },
};
