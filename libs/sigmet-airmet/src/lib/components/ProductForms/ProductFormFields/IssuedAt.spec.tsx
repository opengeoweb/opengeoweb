/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';
import IssuedAt from './IssuedAt';

describe('components/ProductForms/ProductFormFields/IssuedAt', () => {
  it('should render with default props', async () => {
    const props = {
      date: '2020/09/17 14:30 UTC',
    };
    render(<IssuedAt {...props} />);

    expect(await screen.findByText(props.date)).toBeTruthy();
  });
});
