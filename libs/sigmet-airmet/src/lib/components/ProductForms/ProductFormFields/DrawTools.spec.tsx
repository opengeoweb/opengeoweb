/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, screen, within } from '@testing-library/react';

import { defaultBox } from '@opengeoweb/webmap-react';
import DrawTools from './DrawTools';
import { StartOrEndDrawing } from '../../../types';
import { getDrawTools } from '../utils';
import { testFir } from '../../../utils/testUtils';
import { translateKeyOutsideComponents } from '../../../utils/i18n';

describe('components/ProductForms/ProductFormFields/DrawTools', () => {
  it('expect the selected drawMode to be highlighted', () => {
    const props = {
      activeTool: defaultBox.drawModeId,
      tools: getDrawTools(StartOrEndDrawing.start, testFir),
      onChangeTool: jest.fn(),
    };

    render(<DrawTools {...props} />);

    expect(screen.getByTestId('drawtools-box').getAttribute('class')).toContain(
      'Mui-checked',
    );
    expect(
      screen.getByTestId('drawtools-point').getAttribute('class'),
    ).not.toContain('Mui-checked');
    expect(
      screen.getByTestId('drawtools-polygon').getAttribute('class'),
    ).not.toContain('Mui-checked');
    expect(
      screen.getByTestId('drawtools-fir').getAttribute('class'),
    ).not.toContain('Mui-checked');
  });

  it('expect able to set tools', () => {
    const props = {
      activeTool: defaultBox.drawModeId,
      tools: getDrawTools(StartOrEndDrawing.start, testFir),
      onChangeTool: jest.fn(),
    };

    render(<DrawTools {...props} />);

    props.tools.forEach((tool) =>
      expect(screen.getByTestId(tool.drawModeId)).toBeTruthy(),
    );
  });

  it('should show the correct tooltips using the type passed in: start', async () => {
    const props = {
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.start, testFir),
      onChangeTool: jest.fn(),
    };

    render(<DrawTools {...props} />);

    fireEvent.mouseOver(screen.getByTestId('drawtools-point'));
    // Wait until tooltip appears
    const tooltippoint = await screen.findByText(
      translateKeyOutsideComponents('drop-a-point-as', { type: 'start' }),
    );
    expect(tooltippoint.textContent).toBeTruthy();

    fireEvent.mouseOver(screen.getByTestId('drawtools-box'));
    // Wait until tooltip appears
    const tooltipbox = await screen.findByText(
      translateKeyOutsideComponents('draw-a-box-as', { type: 'start' }),
    );

    expect(tooltipbox.textContent).toBeTruthy();

    fireEvent.mouseOver(screen.getByTestId('drawtools-polygon'));
    // Wait until tooltip appears
    const tooltippolygon = await screen.findByText(
      translateKeyOutsideComponents('draw-a-shape-as', { type: 'start' }),
    );
    expect(tooltippolygon.textContent).toBeTruthy();

    fireEvent.mouseOver(screen.getByTestId('drawtools-fir'));
    // Wait until tooltip appears
    const tooltipfir = await screen.findByText(
      translateKeyOutsideComponents('set-a-fir-as', { type: 'start' }),
    );

    expect(tooltipfir.textContent).toBeTruthy();

    fireEvent.mouseOver(screen.getByTestId('drawtools-delete'));
    // Wait until tooltip appears
    const tooltipdelete = await screen.findByText(
      translateKeyOutsideComponents('remove-geometry'),
    );
    expect(tooltipdelete.textContent).toBeTruthy();
  });

  it('should show the correct tooltips using the type passed in: end', async () => {
    const props = {
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
    };

    render(<DrawTools {...props} />);

    fireEvent.mouseOver(screen.getByTestId('drawtools-point'));
    // Wait until tooltip appears
    const tooltippoint = await screen.findByText(
      translateKeyOutsideComponents('drop-a-point-as', { type: 'end' }),
    );
    expect(tooltippoint.textContent).toBeTruthy();

    fireEvent.mouseOver(screen.getByTestId('drawtools-box'));
    // Wait until tooltip appears
    const tooltipbox = await screen.findByText(
      translateKeyOutsideComponents('draw-a-box-as', { type: 'end' }),
    );

    expect(tooltipbox.textContent).toBeTruthy();

    fireEvent.mouseOver(screen.getByTestId('drawtools-polygon'));
    // Wait until tooltip appears
    const tooltippolygon = await screen.findByText(
      translateKeyOutsideComponents('draw-a-shape-as', { type: 'end' }),
    );
    expect(tooltippolygon.textContent).toBeTruthy();

    fireEvent.mouseOver(screen.getByTestId('drawtools-fir'));
    // Wait until tooltip appears
    const tooltipfir = await screen.findByText(
      translateKeyOutsideComponents('set-a-fir-as', { type: 'end' }),
    );

    expect(tooltipfir.textContent).toBeTruthy();

    fireEvent.mouseOver(screen.getByTestId('drawtools-delete'));
    // Wait until tooltip appears
    const tooltipdelete = await screen.findByText(
      translateKeyOutsideComponents('remove-geometry'),
    );
    expect(tooltipdelete.textContent).toBeTruthy();
  });

  it('expect when clicking one of the buttons - onChangeTool is called with the right drawMode as parameter', async () => {
    const props = {
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.start, testFir),
      onChangeTool: jest.fn(),
    };

    render(<DrawTools {...props} />);

    const deleteButton = within(
      screen.getByTestId('drawtools-delete'),
    ).getByRole('radio');
    fireEvent.click(deleteButton!);
    expect(props.onChangeTool).toHaveBeenLastCalledWith(props.tools[4]);

    const pointButton = within(screen.getByTestId('drawtools-point')).getByRole(
      'radio',
    );
    fireEvent.click(pointButton!);
    expect(props.onChangeTool).toHaveBeenLastCalledWith(props.tools[0]);

    const polygonButton = within(
      screen.getByTestId('drawtools-polygon'),
    ).getByRole('radio');
    fireEvent.click(polygonButton!);
    expect(props.onChangeTool).toHaveBeenLastCalledWith(props.tools[2]);

    const firButton = within(screen.getByTestId('drawtools-fir')).getByRole(
      'radio',
    );
    fireEvent.click(firButton!);
    expect(props.onChangeTool).toHaveBeenLastCalledWith(props.tools[3]);

    const boxButton = within(screen.getByTestId('drawtools-box')).getByRole(
      'radio',
    );
    fireEvent.click(boxButton!);
    expect(props.onChangeTool).toHaveBeenLastCalledWith(props.tools[1]);
  });
});
