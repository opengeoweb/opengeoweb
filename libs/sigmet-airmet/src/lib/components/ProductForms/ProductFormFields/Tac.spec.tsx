/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';
import Tac from './Tac';
import { fakeSigmetTAC } from '../../../utils/fakeApi';

describe('components/ProductForms/ProductFormFields/Tac', () => {
  it('should render with default props and display sequence number styling when tac is valid', async () => {
    const props = {
      tac: fakeSigmetTAC,
    };

    render(
      <Tac {...props}>
        <p>testing</p>
      </Tac>,
    );

    expect(await screen.findByText('testing')).toBeTruthy();
    expect(await screen.findByText('1', { selector: 'span' })).toBeTruthy();
    expect(await screen.findByText(/121449/)).toBeTruthy();
  });

  it('should render without sequence styling when noTac is true', async () => {
    const props = {
      tac: fakeSigmetTAC,
    };

    render(
      <Tac {...props}>
        <p>testing</p>
      </Tac>,
    );

    expect(await screen.findByText('testing')).toBeTruthy();

    const sequenceNumber = screen.getByTestId('productform-tac-message-line');
    const style = window.getComputedStyle(sequenceNumber);
    if (sequenceNumber) {
      expect(style).not.toBe('font-weight: lighter');
    }
    expect(screen.queryByText(/VALID 121249\/121449 EHDB/)).not.toBeTruthy();
  });
});
