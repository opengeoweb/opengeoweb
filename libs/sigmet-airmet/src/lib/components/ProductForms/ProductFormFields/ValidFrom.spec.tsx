/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';

import { dateUtils } from '@opengeoweb/shared';

import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';

import ValidFrom, {
  getDefaultValidFromValue,
  getValidFromXHoursBeforeMessage,
} from './ValidFrom';
import { airmetConfig, sigmetConfig } from '../../../utils/config';
import { translateKeyOutsideComponents } from '../../../utils/i18n';
import { TestWrapper } from '../../../utils/testUtils';

describe('components/ProductForms/ProductFormFields/ValidFrom', () => {
  const delayTime = sigmetConfig.valid_from_delay_minutes;

  it('should show current time +Xmins if no date passed in', async () => {
    const now = new Date('2023-02-03T14:00:00Z');
    const spy = jest.spyOn(dateUtils, 'utc').mockReturnValue(now);
    const timeToShow = dateUtils.add(now, { minutes: delayTime });

    render(
      <ReactHookFormProvider>
        <ValidFrom
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled={false}
        />
      </ReactHookFormProvider>,
    );

    const field = screen.getByRole('textbox');
    await waitFor(() => {
      expect(field!.getAttribute('value')!).toEqual(
        dateUtils.dateToString(timeToShow, dateUtils.DATE_FORMAT_DATEPICKER),
      );
    });

    expect(
      screen.getByText(
        `${translateKeyOutsideComponents('select')} ${translateKeyOutsideComponents('date-and-time')}`,
        { exact: false },
      ),
    ).toBeTruthy();
    spy.mockRestore();
  });

  it('should show passed in date', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateStart: '2020-11-17T13:03:00Z',
          },
        }}
      >
        <ValidFrom
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled={false}
        />
      </ReactHookFormProvider>,
    );

    const field = screen.getByRole('textbox');

    await waitFor(() => {
      expect(field!.getAttribute('value')!).toEqual('17/11/2020 13:03');
    });
  });

  it('should be possible to set a date', async () => {
    const now = dateUtils.utc();

    const dateWithDelay = dateUtils.add(now, {
      minutes: delayTime,
    });

    const minutes = dateWithDelay.getUTCMinutes();
    const extraMinutes = minutes <= 30 ? 30 - minutes : 60 - minutes;
    const timeToShow = dateUtils.add(dateWithDelay, { minutes: extraMinutes });

    render(
      <ReactHookFormProvider>
        <ValidFrom
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled={false}
        />
      </ReactHookFormProvider>,
    );

    const field = screen.getByRole('textbox');
    await waitFor(() => {
      expect(field.getAttribute('value')!).toEqual(
        dateUtils.dateToString(timeToShow, dateUtils.DATE_FORMAT_DATEPICKER),
      );
    });

    fireEvent.change(field!, { target: { value: '17/11/2020 13:03' } });
    await waitFor(() => {
      expect(field!.getAttribute('value')!).toEqual('17/11/2020 13:03');
    });
  });

  it('should show correct error', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateStart: '2020-11-17T13:03:00Z',
          },
        }}
      >
        <ValidFrom
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled={false}
        />
      </ReactHookFormProvider>,
    );

    const field = screen.getByRole('textbox');

    await waitFor(() => {
      expect(field!.getAttribute('value')!).toEqual('17/11/2020 13:03');
    });

    // in past
    fireEvent.change(field!, { target: { value: '17/11/2000 13:03' } });

    await waitFor(() => {
      expect(
        screen.getByText(
          translateKeyOutsideComponents('valid-from-before-current-error'),
        ),
      ).toBeTruthy();
    });

    // before validity start
    fireEvent.change(field!, { target: { value: '17/11/2100 13:03' } });
    await waitFor(() => {
      expect(screen.getByText(getValidFromXHoursBeforeMessage(4))).toBeTruthy();
    });
  });

  it('should show the correct disabled input', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ValidFrom
            productType="sigmet"
            productConfig={sigmetConfig}
            isDisabled
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const field = screen.getByRole('textbox');

    await waitFor(() => {
      expect(field).toBeTruthy();
    });
    expect(field.getAttribute('disabled')).toBeDefined();
  });

  it('should show the correct readonly input', () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ValidFrom
            productType="sigmet"
            productConfig={sigmetConfig}
            isReadOnly
            isDisabled
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(
      screen.getByText(translateKeyOutsideComponents('date-and-time')),
    ).toBeTruthy();
  });

  describe('getDefaultValidFromValue', () => {
    it('should add the default delay', () => {
      const now = new Date('2023-02-03T14:00:00Z');
      const spy = jest.spyOn(dateUtils, 'utc').mockReturnValue(now);
      expect(getDefaultValidFromValue(sigmetConfig)).toBe(
        dateUtils.dateToString(
          dateUtils.add(now, {
            minutes: sigmetConfig.valid_from_delay_minutes,
          }),
        ),
      );
      expect(getDefaultValidFromValue(airmetConfig)).toBe(
        dateUtils.dateToString(
          dateUtils.add(now, {
            minutes: airmetConfig.valid_from_delay_minutes,
          }),
        ),
      );
      spy.mockRestore();
    });
  });
});
