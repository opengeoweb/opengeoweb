/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';

import Phenomenon from './Phenomenon';
import { AviationPhenomenaDescription } from '../../../types';
import {
  initSigmetAirmetI18n,
  translateKeyOutsideComponents,
} from '../../../utils/i18n';
import { airmetConfig, sigmetConfig } from '../../../utils/config';
import {
  fakeAirmetPhenomenaList,
  fakeSigmetPhenomenaList,
} from '../../../utils/testUtils';

beforeAll(() => {
  initSigmetAirmetI18n();
});

describe('components/ProductForms/ProductFormFields/Phenomenon', () => {
  it('should be possible to select a sigmet phenomenon', async () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            phenomenon: 'EMBD_TS',
          },
        }}
      >
        <Phenomenon
          productType="sigmet"
          isDisabled={false}
          productConfig={sigmetConfig}
          phenomenaList={fakeSigmetPhenomenaList}
        />
      </ReactHookFormProvider>,
    );
    fireEvent.mouseDown(screen.getByRole('combobox'));

    const menuItem = await screen.findByText('Radioactive cloud');
    fireEvent.click(menuItem);

    await waitFor(() => {
      expect(
        screen.getByLabelText(
          `${translateKeyOutsideComponents('select')} ${translateKeyOutsideComponents('phenomenon')}`,
          { exact: false },
        ).textContent,
      ).toEqual('Radioactive cloud');
    });
  });

  it('should be possible to select an airmet phenomenon', async () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            phenomenon: 'BKN_CLD',
          },
        }}
      >
        <Phenomenon
          productType="airmet"
          isDisabled={false}
          productConfig={airmetConfig}
          phenomenaList={fakeAirmetPhenomenaList}
        />
      </ReactHookFormProvider>,
    );
    fireEvent.mouseDown(screen.getByRole('combobox'));

    const menuItem = await screen.findByText('Surface wind');
    fireEvent.click(menuItem);

    await waitFor(() => {
      expect(
        screen.getByLabelText(
          `${translateKeyOutsideComponents('select')} ${translateKeyOutsideComponents('phenomenon')}`,
          { exact: false },
        ).textContent,
      ).toEqual('Surface wind');
    });
  });

  it('should show the phenomenon as disabled', () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            phenomenon: 'EMBD_TS',
          },
        }}
      >
        <Phenomenon
          productType="sigmet"
          isDisabled
          productConfig={sigmetConfig}
          phenomenaList={fakeSigmetPhenomenaList}
        />
      </ReactHookFormProvider>,
    );
    const phenomenon = screen.getByLabelText(
      `${translateKeyOutsideComponents('select')} ${translateKeyOutsideComponents('phenomenon')}`,
      { exact: false },
    );
    expect(phenomenon.textContent).toEqual(
      'Embedded thunderstorm(s)' as AviationPhenomenaDescription,
    );
    expect(phenomenon.getAttribute('aria-disabled') === 'true').toBeTruthy();
  });

  it('should show the phenomenon as readOnly', async () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            phenomenon: 'EMBD_TS',
          },
        }}
      >
        <Phenomenon
          productType="sigmet"
          isDisabled
          isReadOnly
          productConfig={sigmetConfig}
          phenomenaList={fakeSigmetPhenomenaList}
        />
      </ReactHookFormProvider>,
    );
    const phenomenon = screen.getByLabelText(
      translateKeyOutsideComponents('phenomenon'),
    );
    expect(phenomenon.textContent).toEqual(
      'Embedded thunderstorm(s)' as AviationPhenomenaDescription,
    );
    expect(phenomenon.getAttribute('aria-disabled') === 'true').toBeTruthy();
  });
});
