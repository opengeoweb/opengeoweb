/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';
import userEvent from '@testing-library/user-event';

import SurfaceWind, {
  invalidSurfaceWindDirectionStepsMessage,
  validateSurfaceWindDirection,
} from './SurfaceWind';
import { getMaxWindSpeedValue, getMinWindSpeedValue } from '../utils';
import { WindUnit } from '../../../types';
import { airmetConfig } from '../../../utils/config';
import { translateKeyOutsideComponents } from '../../../utils/i18n';
import { TestWrapper } from '../../../utils/testUtils';

describe('components/ProductForms/ProductFormFields/SurfaceWind', () => {
  const user = userEvent.setup();
  it('should not show any errors when entering valid wind values', async () => {
    const onChangeSpy = jest.fn();

    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            defaultValues: {
              windSpeed: '',
              windUnit: '',
              windDirection: '',
            },
          }}
        >
          <SurfaceWind
            isDisabled={false}
            onChange={onChangeSpy}
            productConfig={airmetConfig}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const windDirection = screen.getByLabelText(
      `${translateKeyOutsideComponents('set')} ${translateKeyOutsideComponents('surface-wind-label')}`,
    );
    const windUnit = screen.getByRole('combobox', {
      name: 'Unit',
    });
    const windSpeed = screen.getByLabelText(
      `${translateKeyOutsideComponents('set')} ${translateKeyOutsideComponents('surface-wind-speed-label')}`,
    );

    await user.type(windDirection, '200');
    await user.type(windSpeed, '80');
    await user.click(windUnit);
    await user.click(await screen.findByText('kt'));

    expect(onChangeSpy).toHaveBeenCalled();

    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });
  });

  it('should show correct labels for readonly', async () => {
    const onChangeSpy = jest.fn();

    render(
      <ReactHookFormProvider>
        <SurfaceWind
          isReadOnly
          isDisabled={false}
          onChange={onChangeSpy}
          productConfig={airmetConfig}
        />
      </ReactHookFormProvider>,
    );

    expect(
      screen.getByText(translateKeyOutsideComponents('surface-wind-label')),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('surface-wind-speed-label'),
      ),
    ).toBeTruthy();
  });

  it('should not show an error message when entering a decimal Wind Direction but convert it directly to integer', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            windSpeed: '',
            windUnit: 'KT' as WindUnit,
            windDirection: '',
          },
        }}
      >
        <SurfaceWind
          isDisabled={false}
          onChange={jest.fn()}
          productConfig={airmetConfig}
        />
      </ReactHookFormProvider>,
    );
    const directionInput = screen.getByLabelText(
      `${translateKeyOutsideComponents('set')} ${translateKeyOutsideComponents('surface-wind-label')}`,
    );
    await user.type(directionInput, '220.');

    await waitFor(() =>
      expect(
        directionInput.getAttribute('aria-invalid') === 'true',
      ).toBeFalsy(),
    );
    /* wait for the value to be converted to integer */
    await waitFor(() =>
      expect(directionInput.getAttribute('value')).toEqual('220'),
    );
  });
  it('should show an error message when invalid wind direction is entered', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            windSpeed: '',
            windUnit: 'KT' as WindUnit,
            windDirection: '',
          },
        }}
      >
        <SurfaceWind
          isDisabled={false}
          onChange={jest.fn()}
          productConfig={airmetConfig}
        />
      </ReactHookFormProvider>,
    );
    const directionInput = screen.getByLabelText(
      `${translateKeyOutsideComponents('set')} ${translateKeyOutsideComponents('surface-wind-label')}`,
    );
    fireEvent.change(directionInput, { target: { value: '390' } });

    await waitFor(() =>
      expect(
        directionInput.getAttribute('aria-invalid') === 'true',
      ).toBeTruthy(),
    );
    expect(
      screen.getByText(
        translateKeyOutsideComponents('surface-wind-rule-min-max-message'),
      ),
    ).toBeTruthy();

    fireEvent.change(directionInput, { target: { value: '100' } });

    await waitFor(() =>
      expect(
        directionInput.getAttribute('aria-invalid') === 'true',
      ).toBeFalsy(),
    );
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('surface-wind-rule-min-max-message'),
      ),
    ).toBeFalsy();

    fireEvent.change(directionInput, { target: { value: '5' } });

    await waitFor(() =>
      expect(
        directionInput.getAttribute('aria-invalid') === 'true',
      ).toBeTruthy(),
    );
    expect(
      screen.getByText(
        translateKeyOutsideComponents('surface-wind-rule-min-max-message'),
      ),
    ).toBeTruthy();

    fireEvent.change(directionInput, { target: { value: '16' } });
    await waitFor(() =>
      expect(
        directionInput.getAttribute('aria-invalid') === 'true',
      ).toBeTruthy(),
    );
    expect(
      screen.getByText(
        translateKeyOutsideComponents(
          'invalid-surface-wind-direction-message',
          { value: 10 },
        ),
      ),
    ).toBeTruthy();
  });
  it('should not show an error message when entering a decimal Wind Speed but convert it directly to integer', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            windSpeed: '',
            windUnit: 'KT' as WindUnit,
            windDirection: '',
          },
        }}
      >
        <SurfaceWind
          isDisabled={false}
          onChange={jest.fn()}
          productConfig={airmetConfig}
        />
      </ReactHookFormProvider>,
    );
    const speedInput = screen.getByLabelText(
      `${translateKeyOutsideComponents('set')} ${translateKeyOutsideComponents('surface-wind-speed-label')}`,
    );

    await user.type(speedInput, '31.');

    await waitFor(() =>
      expect(speedInput.getAttribute('aria-invalid') === 'true').toBeFalsy(),
    );
    /* wait for the value to be converted to integer */
    await waitFor(() => expect(speedInput.getAttribute('value')).toEqual('31'));
  });
  it('should show an error message when invalid wind speed is entered', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            windSpeed: '',
            windUnit: 'KT' as WindUnit,
            windDirection: '',
          },
        }}
      >
        <SurfaceWind
          isDisabled={false}
          onChange={jest.fn()}
          productConfig={airmetConfig}
        />
      </ReactHookFormProvider>,
    );
    const speedInput = screen.getByLabelText(
      `${translateKeyOutsideComponents('set')} ${translateKeyOutsideComponents('surface-wind-speed-label')}`,
    );
    const speedUnit = screen.getByRole('combobox', {
      name: 'Unit',
    });

    // Test with 400 kts
    await user.type(speedInput, '400');
    await screen.findByText(
      translateKeyOutsideComponents('surface-wind-maximum-wind-speed', {
        windUnit: 'kt',
        maxWindSpeedValue: getMaxWindSpeedValue('KT', 'EHAA', airmetConfig)!,
      }),
    );
    expect(speedInput.getAttribute('value')).toEqual('400');
    expect(speedInput.getAttribute('aria-invalid') === 'true').toBeTruthy();

    // Test with 100 kts
    await user.clear(speedInput);
    await user.type(speedInput, '100');
    await waitFor(() =>
      expect(speedInput.getAttribute('aria-invalid') === 'true').toBeFalsy(),
    );

    // Test with 10 kts
    await user.clear(speedInput);
    await user.type(speedInput, '10');
    await screen.findByText(
      translateKeyOutsideComponents('surface-wind-minimum-wind-speed', {
        windUnit: 'kt',
        minWindSpeedValue: getMinWindSpeedValue('KT', 'EHAA', airmetConfig)!,
      }),
    );

    // Test with 10 mps
    await user.click(speedUnit);
    await user.click(screen.getByText('mps'));

    await waitFor(() =>
      expect(speedInput.getAttribute('aria-invalid') === 'true').toBeFalsy(),
    );

    // Test with -10 mps
    await user.clear(speedInput);
    await user.type(speedInput, '-10');
    await screen.findByText(
      translateKeyOutsideComponents('surface-wind-minimum-wind-speed', {
        windUnit: 'mps',
        minWindSpeedValue: getMinWindSpeedValue('MPS', 'EHAA', airmetConfig)!,
      }),
    );

    // Test with 100 mps
    await user.clear(speedInput);
    await user.type(speedInput, '100');
    await screen.findByText(
      translateKeyOutsideComponents('surface-wind-maximum-wind-speed', {
        windUnit: 'mps',
        maxWindSpeedValue: getMaxWindSpeedValue('MPS', 'EHAA', airmetConfig)!,
      }),
    );
  });
  describe('validateSurfaceWindDirection', () => {
    it('should validate passed wind direction correctly with default values', () => {
      const currentFIR = 'EHAA';

      expect(
        validateSurfaceWindDirection('000', airmetConfig, currentFIR),
      ).toBe(true);
      expect(validateSurfaceWindDirection('0', airmetConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceWindDirection('50', airmetConfig, currentFIR)).toBe(
        true,
      );
      expect(
        validateSurfaceWindDirection('070', airmetConfig, currentFIR),
      ).toBe(true);
      expect(
        validateSurfaceWindDirection('007', airmetConfig, currentFIR),
      ).toBe(invalidSurfaceWindDirectionStepsMessage());
      expect(
        validateSurfaceWindDirection('057', airmetConfig, currentFIR),
      ).toBe(invalidSurfaceWindDirectionStepsMessage());
      expect(
        validateSurfaceWindDirection('278', airmetConfig, currentFIR),
      ).toBe(invalidSurfaceWindDirectionStepsMessage());
      expect(
        validateSurfaceWindDirection('', airmetConfig, currentFIR),
      ).toBeTruthy();
    });

    it('should validate passed wind direction correctly with values from config', () => {
      const currentFIR = 'EHAA';
      const testWindDirectionRounding = 25;
      const testConfig = {
        ...airmetConfig,
        fir_areas: {
          ...airmetConfig.fir_areas,
          EHAA: {
            ...airmetConfig.fir_areas.EHAA,
            wind_direction_rounding: testWindDirectionRounding,
          },
        },
      };

      expect(validateSurfaceWindDirection('0000', testConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceWindDirection('0', testConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceWindDirection('25', testConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceWindDirection('050', testConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceWindDirection('10', testConfig, currentFIR)).toBe(
        invalidSurfaceWindDirectionStepsMessage(testWindDirectionRounding),
      );
      expect(validateSurfaceWindDirection('010', testConfig, currentFIR)).toBe(
        invalidSurfaceWindDirectionStepsMessage(testWindDirectionRounding),
      );
      expect(validateSurfaceWindDirection('0010', testConfig, currentFIR)).toBe(
        invalidSurfaceWindDirectionStepsMessage(testWindDirectionRounding),
      );
      expect(validateSurfaceWindDirection('901', testConfig, currentFIR)).toBe(
        invalidSurfaceWindDirectionStepsMessage(testWindDirectionRounding),
      );
      expect(
        validateSurfaceWindDirection('', testConfig, currentFIR),
      ).toBeTruthy();
    });
  });
});
