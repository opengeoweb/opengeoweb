/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';

import Type from './Type';
import { translateKeyOutsideComponents } from '../../../utils/i18n';

describe('components/ProductForms/ProductFormFields/Type', () => {
  it('should be possible to select a type', async () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            type: 'TEST',
          },
        }}
      >
        <Type productType="sigmet" isDisabled={false} isReadOnly={false} />
      </ReactHookFormProvider>,
    );
    fireEvent.mouseDown(screen.getByRole('combobox'));

    const menuItem = await screen.findByText('Normal');
    fireEvent.click(menuItem);

    await waitFor(() => {
      expect(screen.getByTestId('type')!.textContent).toEqual('Normal');
    });

    expect(
      await screen.findByText(
        `${(translateKeyOutsideComponents('select'), translateKeyOutsideComponents('type-title'))}`,
      ),
    ).toBeTruthy();
  });
  it('should show type defaulting to "Normal"', async () => {
    render(
      <ReactHookFormProvider>
        <Type productType="sigmet" isDisabled={false} isReadOnly={false} />
      </ReactHookFormProvider>,
    );

    await waitFor(() => {
      expect(screen.getByTestId('type')!.textContent).toEqual('Normal');
    });

    expect(
      await screen.findByText(
        `${(translateKeyOutsideComponents('select'), translateKeyOutsideComponents('type-title'))}`,
      ),
    ).toBeTruthy();
  });
  it('should show type as disabled', async () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            type: 'TEST',
          },
        }}
      >
        <Type productType="sigmet" isDisabled isReadOnly={false} />
      </ReactHookFormProvider>,
    );
    const type = screen.getByTestId('type')!;
    expect(type.textContent).toEqual('Test');
    expect(type.getAttribute('class')).toContain('Mui-disabled');
  });
  it('should show type as readonly', async () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            type: 'TEST',
          },
        }}
      >
        <Type productType="sigmet" isDisabled isReadOnly />
      </ReactHookFormProvider>,
    );

    const type = screen.getByTestId('type')!;
    expect(type.textContent).toEqual('Test');
    expect(type.getAttribute('class')).toContain('Mui-disabled');
    expect(
      await screen.findAllByText(translateKeyOutsideComponents('type-title')),
    ).toBeTruthy();
  });
});
