/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  defaultFormOptions,
  errorMessages,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';

import CloudLevels, {
  DEFAULT_ROUNDING_CLOUD_LEVELS_FT,
  DEFAULT_ROUNDING_CLOUD_LEVELS_M_ABOVE,
  DEFAULT_ROUNDING_CLOUD_LEVELS_M_BELOW,
  getInvalidStepsForFeetUnitMessage,
  getInvalidStepsForMetersUnitAbove2970Message,
  getInvalidStepsForMetersUnitBelow2970Message,
  invalidUnitMessage,
  validateRoundedStep,
} from './CloudLevels';
import { AirmetConfig, CloudLevelUnits } from '../../../types';
import {
  getFir,
  getMaxCloudLevelValue,
  getMaxCloudLowerLevelValue,
  getMinCloudLevelValue,
  getMinCloudLowerLevelValue,
} from '../utils';
import { airmetConfig } from '../../../utils/config';
import {
  initSigmetAirmetI18n,
  translateKeyOutsideComponents,
} from '../../../utils/i18n';

beforeAll(() => {
  initSigmetAirmetI18n();
});

describe('components/ProductForms/ProductFormFields/CloudLevels', () => {
  const user = userEvent.setup();

  const config: AirmetConfig = {
    location_indicator_mwo: 'EHDB',
    active_firs: ['EHAA', 'TEST'],
    fir_areas: {
      EHAA: {
        fir_name: 'AMSTERDAM FIR',
        fir_location: getFir(airmetConfig, 'EHAA'),
        location_indicator_atsr: 'EHAA',
        location_indicator_atsu: 'EHAA',
        hours_before_validity: 5,
        max_hours_of_validity: 5,
        cloud_lower_level_min: {
          FT: 100,
          M: 30,
        },
        cloud_lower_level_max: {
          FT: 900,
          M: 300,
        },
        cloud_level_min: {
          FT: 100,
          M: 30,
        },
        cloud_level_max: {
          FT: 9900,
          M: 9900,
        },
        cloud_level_rounding_ft: 100,
        cloud_level_rounding_m_below: 30,
        cloud_level_rounding_m_above: 300,
        movement_rounding_kt: 5,
        movement_rounding_kmh: 10,
        wind_speed_min: {
          KT: 31,
        },
        wind_speed_max: {
          KT: 199,
        },
        visibility_max: 4900,
        visibility_min: 0,
        level_min: {
          FT: 100,
          FL: 50,
        },
        level_max: {
          FT: 4900,
          FL: 650,
        },
        phenomenon: airmetConfig.fir_areas.EHAA.phenomenon,
        units: [
          {
            unit_type: 'level_unit',
            allowed_units: ['FT', 'FL'],
          },
          {
            unit_type: 'movement_unit',
            allowed_units: ['KT'],
          },
          {
            unit_type: 'cloud_level_unit',
            allowed_units: ['FT', 'M'],
          },
        ],
      },
      TEST: {
        fir_name: 'TEST FIR',
        fir_location: getFir(airmetConfig, 'EHAA'),
        location_indicator_atsr: 'EHAA',
        location_indicator_atsu: 'EHAA',
        hours_before_validity: 5,
        max_hours_of_validity: 5,
        cloud_lower_level_min: {
          FT: 100,
          M: 30,
        },
        cloud_lower_level_max: {
          FT: 900,
          M: 300,
        },
        cloud_level_min: {
          FT: 100,
          M: 30,
        },
        cloud_level_max: {
          FT: 9900,
          M: 9900,
        },
        movement_rounding_kt: 5,
        movement_rounding_kmh: 10,
        cloud_level_rounding_ft: 777,
        cloud_level_rounding_m_below: 888,
        cloud_level_rounding_m_above: 999,
        wind_speed_min: {
          KT: 31,
        },
        wind_speed_max: {
          KT: 199,
        },
        visibility_max: 4900,
        visibility_min: 0,
        level_min: {
          FT: 100,
          FL: 50,
        },
        level_max: {
          FT: 4900,
          FL: 650,
        },
        phenomenon: airmetConfig.fir_areas.EHAA.phenomenon,
        units: [
          {
            unit_type: 'level_unit',
            allowed_units: ['FT', 'FL'],
          },
          {
            unit_type: 'movement_unit',
            allowed_units: ['KT'],
          },
          {
            unit_type: 'cloud_level_unit',
            allowed_units: ['FT', 'M'],
          },
        ],
      },
    },
    valid_from_delay_minutes: 60,
    default_validity_minutes: 180,
  };
  it('should show the correct input fields when selecting level BETW and BETW_ABV', async () => {
    render(
      <ReactHookFormProvider>
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const cloudLevelAbove: HTMLInputElement = within(
      screen.getByTestId('cloudLevels-Above'),
    ).getByRole('checkbox');
    const cloudLevelSurface: HTMLInputElement = within(
      screen.getByTestId('cloudLevels-SFC'),
    ).getByRole('checkbox');

    expect(cloudLevelAbove).toBeTruthy();
    expect(cloudLevelSurface).toBeTruthy();
    expect(screen.getAllByLabelText('Unit').length).toEqual(2);
    expect(screen.getByLabelText('Upper level')).toBeTruthy();
    expect(screen.getByLabelText('Lower level')).toBeTruthy();

    // select BETW_ABV
    fireEvent.click(cloudLevelAbove);
    await waitFor(() => expect(cloudLevelAbove.checked).toBeTruthy());

    expect(cloudLevelAbove).toBeTruthy();
    expect(cloudLevelSurface).toBeTruthy();
    expect(screen.getAllByLabelText('Unit').length).toEqual(2);
    expect(screen.getByLabelText('Upper level')).toBeTruthy();
    expect(screen.getByLabelText('Lower level')).toBeTruthy();
  });

  it('should show the correct input fields when selecting level BETW and BETW_SFC', async () => {
    render(
      <ReactHookFormProvider>
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const cloudLevelAbove: HTMLInputElement = within(
      screen.getByTestId('cloudLevels-Above'),
    ).getByRole('checkbox');
    const cloudLevelSurface: HTMLInputElement = within(
      screen.getByTestId('cloudLevels-SFC'),
    ).getByRole('checkbox');

    expect(cloudLevelAbove).toBeTruthy();
    expect(cloudLevelSurface).toBeTruthy();
    expect(screen.getAllByLabelText('Unit').length).toEqual(2);
    expect(screen.getByLabelText('Upper level')).toBeTruthy();
    expect(screen.getByLabelText('Lower level')).toBeTruthy();

    // select BETW_SFC
    fireEvent.click(cloudLevelSurface);
    await waitFor(() => expect(cloudLevelSurface.checked).toBeTruthy());
    expect(cloudLevelAbove.checked).toBeFalsy();
    expect(cloudLevelAbove).toBeTruthy();
    expect(cloudLevelSurface).toBeTruthy();
    expect(screen.getAllByLabelText('Unit').length).toEqual(1);
    expect(screen.getByLabelText('Upper level')).toBeTruthy();
    expect(screen.queryByLabelText('Lower level')).toBeFalsy();
  });
  it('should show the correct input fields when selecting level BETW and BETW_ABV_SFC', async () => {
    render(
      <ReactHookFormProvider>
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );

    const cloudLevelAbove: HTMLInputElement = within(
      screen.getByTestId('cloudLevels-Above'),
    ).getByRole('checkbox');
    const cloudLevelSurface: HTMLInputElement = within(
      screen.getByTestId('cloudLevels-SFC'),
    ).getByRole('checkbox');

    expect(cloudLevelAbove).toBeTruthy();
    expect(cloudLevelSurface).toBeTruthy();
    expect(screen.getAllByLabelText('Unit').length).toEqual(2);
    expect(screen.getByLabelText('Upper level')).toBeTruthy();
    expect(screen.getByLabelText('Lower level')).toBeTruthy();

    // select BETW_ABV_SFC
    fireEvent.click(cloudLevelAbove);
    fireEvent.click(cloudLevelSurface);
    await waitFor(() => expect(cloudLevelSurface.checked).toBeTruthy());
    expect(cloudLevelAbove.checked).toBeTruthy();

    expect(cloudLevelAbove).toBeTruthy();
    expect(cloudLevelSurface).toBeTruthy();
    expect(screen.getAllByLabelText('Unit').length).toEqual(1);
    expect(screen.getByLabelText('Upper level')).toBeTruthy();
    expect(screen.queryByLabelText('Lower level')).toBeFalsy();
  });
  it('should show the input fields as disabled with correct values', () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: '1000',
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: '800',
        unit: 'FT' as CloudLevelUnits,
      },
    };
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: testValues,
        }}
      >
        <CloudLevels isDisabled isReadOnly={false} productConfig={config} />
      </ReactHookFormProvider>,
    );
    const cloudLevelAbove: HTMLInputElement = within(
      screen.getByTestId('cloudLevels-Above'),
    ).getByRole('checkbox');
    const cloudLevelSurface: HTMLInputElement = within(
      screen.getByTestId('cloudLevels-SFC'),
    ).getByRole('checkbox');
    const cloudLevelUnit = screen.getAllByLabelText('Unit')[0];
    const cloudLevelValue: HTMLInputElement =
      screen.getByLabelText('Upper level');
    const cloudLowerLevelUnit = screen.getAllByLabelText('Unit')[1];
    const cloudLowerLevelValue: HTMLInputElement =
      screen.getByLabelText('Lower level');

    expect(cloudLevelAbove.disabled).toBeTruthy();
    expect(cloudLevelAbove.checked).toBeFalsy();
    expect(cloudLevelSurface.disabled).toBeTruthy();
    expect(cloudLevelSurface.checked).toBeFalsy();

    expect(
      cloudLevelUnit.getAttribute('aria-disabled') === 'true',
    ).toBeTruthy();
    expect(cloudLevelUnit.textContent).toEqual('ft');
    expect(cloudLevelValue.disabled).toBeTruthy();
    expect(cloudLevelValue.getAttribute('value')).toEqual(
      testValues.cloudLevel.value,
    );
    expect(
      cloudLowerLevelUnit.getAttribute('aria-disabled') === 'true',
    ).toBeTruthy();
    expect(cloudLowerLevelUnit.textContent).toEqual('ft');
    expect(cloudLowerLevelValue.disabled).toBeTruthy();
    expect(cloudLowerLevelValue.getAttribute('value')).toEqual(
      testValues.cloudLowerLevel.value,
    );
  });
  it('should show the input fields as readOnly with correct values', () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: '1000',
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: '800',
        unit: 'FT' as CloudLevelUnits,
      },
    };
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: testValues,
        }}
      >
        <CloudLevels isDisabled isReadOnly productConfig={config} />
      </ReactHookFormProvider>,
    );

    const cloudLevelUnit = screen.getAllByLabelText('Unit')[0];
    const cloudLevelValue: HTMLInputElement =
      screen.getByLabelText('Upper level');
    const cloudLowerLevelUnit = screen.getAllByLabelText('Unit')[1];
    const cloudLowerLevelValue: HTMLInputElement =
      screen.getByLabelText('Lower level');

    expect(
      cloudLevelUnit.getAttribute('aria-disabled') === 'true',
    ).toBeTruthy();
    expect(cloudLevelUnit.textContent).toEqual('ft');
    expect(cloudLevelValue.disabled).toBeTruthy();
    expect(cloudLevelValue.getAttribute('value')).toEqual(
      testValues.cloudLevel.value,
    );
    expect(
      cloudLowerLevelUnit.getAttribute('aria-disabled') === 'true',
    ).toBeTruthy();
    expect(cloudLowerLevelUnit.textContent).toEqual('ft');
    expect(cloudLowerLevelValue.disabled).toBeTruthy();
    expect(cloudLowerLevelValue.getAttribute('value')).toEqual(
      testValues.cloudLowerLevel.value,
    );
    // test other level fields are hidden
    expect(screen.queryByTestId('cloudLevels-Above')).toBeFalsy();
    expect(screen.queryByTestId('cloudLevels-SFC')).toBeFalsy();
  });
  it('should show error when level is below the min value in meters', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 18000,
        unit: 'M' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 60,
        unit: 'M' as CloudLevelUnits,
      },
    };
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const upperLevelValue: HTMLInputElement =
      screen.getByLabelText('Upper level');
    const lowerLevelValue: HTMLInputElement =
      screen.getByLabelText('Lower level');

    await waitFor(() => {
      expect(
        upperLevelValue.getAttribute('aria-invalid') === 'true',
      ).toBeFalsy();
    });
    fireEvent.change(upperLevelValue, { target: { value: 20 } });
    await screen.findByText(
      `The minimum level in m is ${getMinCloudLevelValue('M', 'EHAA', config)}`,
    );
    expect(
      upperLevelValue.getAttribute('aria-invalid') === 'true',
    ).toBeTruthy();

    fireEvent.change(lowerLevelValue, { target: { value: 10 } });
    await waitFor(() => {
      expect(
        screen.queryAllByText(
          `The minimum level in m is ${getMinCloudLowerLevelValue(
            'M',
            'EHAA',
            config,
          )}`,
        ).length,
      ).toEqual(2);
    });
    expect(
      lowerLevelValue.getAttribute('aria-invalid') === 'true',
    ).toBeTruthy();
  });
  it('should show error when level is below the min value in feet', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 9900,
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 100,
        unit: 'FT' as CloudLevelUnits,
      },
    };
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );

    const upperLevelValue: HTMLInputElement =
      screen.getByLabelText('Upper level');
    const lowerLevelValue: HTMLInputElement =
      screen.getByLabelText('Lower level');

    await waitFor(() => {
      expect(
        upperLevelValue.getAttribute('aria-invalid') === 'true',
      ).toBeFalsy();
    });

    fireEvent.change(upperLevelValue, { target: { value: 80 } });
    await screen.findByText(
      `The minimum level in ft is ${getMinCloudLevelValue(
        'FT',
        'EHAA',
        config,
      )}`,
    );

    expect(
      upperLevelValue.getAttribute('aria-invalid') === 'true',
    ).toBeTruthy();

    fireEvent.change(lowerLevelValue, { target: { value: 50 } });
    await waitFor(() => {
      expect(
        screen.queryAllByText(
          `The minimum level in ft is ${getMinCloudLowerLevelValue(
            'FT',
            'EHAA',
            config,
          )}`,
        ).length,
      ).toEqual(2);
    });

    expect(
      lowerLevelValue.getAttribute('aria-invalid') === 'true',
    ).toBeTruthy();
  });
  it('should show error when switching to a upperlevel unit with a lower max value', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 21000,
        unit: 'M' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 90,
        unit: 'M' as CloudLevelUnits,
      },
    };
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const upperLevelUnit = screen.getAllByLabelText('Unit')[0];
    const upperLevelValue: HTMLInputElement =
      screen.getByLabelText('Upper level');

    await waitFor(() => {
      expect(
        upperLevelValue.getAttribute('aria-invalid') === 'true',
      ).toBeFalsy();
    });

    await user.click(upperLevelUnit);
    await user.click(screen.getByText('ft'));

    await waitFor(() => {
      expect(
        screen.getByText(
          `The maximum level in ft is ${getMaxCloudLevelValue(
            'FT',
            'EHAA',
            config,
          )}`,
        ),
      ).toBeTruthy();
    });
    expect(
      upperLevelValue.getAttribute('aria-invalid') === 'true',
    ).toBeTruthy();
  });
  it('should show error when lowerlevel value is set higher than upper level value', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 800,
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 600,
        unit: 'FT' as CloudLevelUnits,
      },
    };
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const lowerLevelValueField: HTMLInputElement =
      screen.getByLabelText('Lower level');

    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });

    fireEvent.change(lowerLevelValueField, { target: { value: 900 } });
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(1);
    });
    expect(
      screen.getByText(
        translateKeyOutsideComponents(errorMessages.isLevelLower),
      ),
    ).toBeTruthy();
    fireEvent.change(lowerLevelValueField, { target: { value: 100 } });
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });
    expect(
      screen.queryByText(
        translateKeyOutsideComponents(errorMessages.isLevelLower),
      ),
    ).toBeFalsy();
  });
  it('should show error when upperlevel value is set below lowerlevel value', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 2000,
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 900,
        unit: 'FT' as CloudLevelUnits,
      },
    };
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const upperLevelValueField: HTMLInputElement =
      screen.getByLabelText('Upper level');
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });
    fireEvent.change(upperLevelValueField, { target: { value: 800 } });
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(1);
    });
    expect(
      screen.getByText(
        translateKeyOutsideComponents(errorMessages.isLevelLower),
      ),
    ).toBeTruthy();
    fireEvent.change(upperLevelValueField, { target: { value: 1100 } });
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });
    expect(
      screen.queryByText(
        translateKeyOutsideComponents(errorMessages.isLevelLower),
      ),
    ).toBeFalsy();
  });
  it('should show error when switching to a lowerlevel unit with a lower max value', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 100,
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 9900,
        unit: 'FT' as CloudLevelUnits,
      },
    };
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const lowerLevelUnit = screen.getAllByLabelText('Unit')[1];
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });
    await user.click(lowerLevelUnit);
    await user.click(screen.getByText('m'));

    await screen.findByText(
      `The maximum level in m is ${getMaxCloudLowerLevelValue(
        'M',
        'EHAA',
        config,
      )}`,
    );
    expect(
      screen
        .queryAllByRole('textbox')
        .filter((field) => field.getAttribute('aria-invalid') === 'true'),
    ).toHaveLength(2);
  });
  it('should show error when step is not correct for the cloud upperlevel', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 1000,
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 100,
        unit: 'FT' as CloudLevelUnits,
      },
    };
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );

    const upperLevelValueField: HTMLInputElement =
      screen.getByLabelText('Upper level');
    const upperLevelUnitField = screen.getAllByLabelText('Unit')[0];
    const lowerLevelValueField: HTMLInputElement =
      screen.getByLabelText('Lower level');

    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });

    fireEvent.change(upperLevelValueField, { target: { value: 1010 } });
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(1);
    });
    expect(screen.getByText(getInvalidStepsForFeetUnitMessage())).toBeTruthy();
    fireEvent.change(upperLevelValueField, { target: { value: 10 } });
    expect(screen.getByText(getInvalidStepsForFeetUnitMessage())).toBeTruthy();
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(1);
    });

    fireEvent.change(upperLevelValueField, { target: { value: 500 } });
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });
    expect(screen.queryByText(getInvalidStepsForFeetUnitMessage())).toBeFalsy();

    await user.click(upperLevelUnitField);
    await user.click(screen.getByText('m'));

    fireEvent.change(lowerLevelValueField, {
      target: { value: 30 },
    });
    fireEvent.change(upperLevelValueField, { target: { value: 310 } });
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(1);
    });
    expect(
      screen.getByText(getInvalidStepsForMetersUnitBelow2970Message()),
    ).toBeTruthy();
    fireEvent.change(upperLevelValueField, { target: { value: 300 } });
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });
    expect(
      screen.queryByText(getInvalidStepsForMetersUnitBelow2970Message()),
    ).toBeFalsy();
    fireEvent.change(upperLevelValueField, { target: { value: 500 } });
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(1);
    });
    expect(
      screen.getByText(getInvalidStepsForMetersUnitBelow2970Message()),
    ).toBeTruthy();
    fireEvent.change(upperLevelValueField, { target: { value: 2970 } });
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });
    expect(
      screen.queryByText(getInvalidStepsForMetersUnitBelow2970Message()),
    ).toBeFalsy();
    fireEvent.change(upperLevelValueField, { target: { value: 5000 } });
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(1);
    });
    expect(
      screen.getByText(getInvalidStepsForMetersUnitAbove2970Message()),
    ).toBeTruthy();
    fireEvent.change(upperLevelValueField, { target: { value: 5400 } });
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });
    expect(
      screen.queryByText(getInvalidStepsForMetersUnitAbove2970Message()),
    ).toBeFalsy();
  });
  it('should show error when step is not correct for the cloud lowerlevel', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 1000,
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 100,
        unit: 'FT' as CloudLevelUnits,
      },
    };
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );

    const upperLevelValueField: HTMLInputElement =
      screen.getByLabelText('Upper level');
    const lowerLevelValueField: HTMLInputElement =
      screen.getByLabelText('Lower level');
    const lowerLevelUnitField = screen.getAllByLabelText('Unit')[1];

    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });

    fireEvent.change(lowerLevelValueField, { target: { value: 810 } });
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(1);
    });
    expect(screen.getByText(getInvalidStepsForFeetUnitMessage())).toBeTruthy();
    fireEvent.change(lowerLevelValueField, { target: { value: 10 } });

    expect(
      screen
        .queryAllByRole('textbox')
        .filter((field) => field.getAttribute('aria-invalid') === 'true'),
    ).toHaveLength(1);

    await waitFor(() => {
      expect(
        screen.getByText(getInvalidStepsForFeetUnitMessage()),
      ).toBeTruthy();
    });
    fireEvent.change(lowerLevelValueField, { target: { value: 500 } });
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });
    expect(screen.queryByText(getInvalidStepsForFeetUnitMessage())).toBeFalsy();

    await user.click(lowerLevelUnitField);
    await user.click(screen.getByText('m'));

    fireEvent.change(lowerLevelValueField, { target: { value: 110 } });
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(2);
    });
    expect(
      screen.queryAllByText(getInvalidStepsForMetersUnitBelow2970Message())
        .length,
    ).toEqual(2);
    fireEvent.change(upperLevelValueField, { target: { value: 1500 } });
    fireEvent.change(lowerLevelValueField, { target: { value: 120 } });
    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });
    expect(
      screen.queryByText(getInvalidStepsForMetersUnitBelow2970Message()),
    ).toBeFalsy();
  });
  it('should change the unit of CloudLowerLevel if the unit of CloudUpperLevel is changed (and vice versa)', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 1000,
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 100,
        unit: 'FT' as CloudLevelUnits,
      },
    };
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );

    const lowerLevelUnitField = screen.getAllByLabelText('Unit')[1];
    const upperLevelUnitField = screen.getAllByLabelText('Unit')[0];

    // Change of lowerLevel unit automatically changes upperLevel unit
    await user.click(lowerLevelUnitField);
    await user.click(screen.getByText('m'));

    await waitFor(() => expect(upperLevelUnitField.textContent).toEqual('m'));

    await user.click(lowerLevelUnitField);
    await user.click(screen.getByText('ft'));

    await waitFor(() => expect(upperLevelUnitField.textContent).toEqual('ft'));

    // Change of upperLevel unit automatically changes lowerLevel unit
    await user.click(upperLevelUnitField);
    await user.click(screen.getByText('m'));

    await waitFor(() => expect(lowerLevelUnitField.textContent).toEqual('m'));
    await user.click(upperLevelUnitField);
    await user.click(screen.getByText('ft'));
    await waitFor(() => expect(lowerLevelUnitField.textContent).toEqual('ft'));
  });
  it('should the lower level unit be the same of upper level unit when toggling SFC', async () => {
    render(
      <ReactHookFormProvider>
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const cloudLevelSFC: HTMLInputElement = within(
      screen.getByTestId('cloudLevels-SFC'),
    ).getByRole('checkbox');

    // Checkbox SFC checked
    fireEvent.click(cloudLevelSFC);
    expect(cloudLevelSFC.checked).toBeTruthy();

    // Changed unit of Upper Level
    const upperLevelUnitField = screen.getAllByLabelText('Unit')[0];
    await user.click(upperLevelUnitField);
    await user.click(screen.getByText('m'));

    await waitFor(() => expect(upperLevelUnitField.textContent).toEqual('m'));
    // Checkbox SFC unchecked
    fireEvent.click(cloudLevelSFC);
    expect(cloudLevelSFC.checked).toBeFalsy();

    const lowerLevelUnitField = screen.getAllByLabelText('Unit')[1];

    expect(upperLevelUnitField.textContent).toEqual('m');
    expect(lowerLevelUnitField.textContent).toEqual('m');
  });

  describe('getInvalidStepsForFeetUnitMessage', () => {
    it('should return message with default value', () => {
      expect(getInvalidStepsForFeetUnitMessage()).toEqual(
        `A level must be rounded to the nearest ${DEFAULT_ROUNDING_CLOUD_LEVELS_FT} ft`,
      );
    });
    it('should return message with given value', () => {
      const testValue = 9999;
      expect(getInvalidStepsForFeetUnitMessage(testValue)).toEqual(
        `A level must be rounded to the nearest ${testValue} ft`,
      );
    });
  });

  describe('getInvalidStepsForMetersUnitBelow2970Message', () => {
    it('should return message with default value', () => {
      expect(getInvalidStepsForMetersUnitBelow2970Message()).toEqual(
        `A level must be rounded to the nearest ${DEFAULT_ROUNDING_CLOUD_LEVELS_M_BELOW} m`,
      );
    });
    it('should return message with given value', () => {
      const testValue = 9999;
      expect(getInvalidStepsForMetersUnitBelow2970Message(testValue)).toEqual(
        `A level must be rounded to the nearest ${testValue} m`,
      );
    });
  });

  describe('getInvalidStepsForMetersUnitAbove2970Message', () => {
    it('should return message with default value', () => {
      expect(getInvalidStepsForMetersUnitAbove2970Message()).toEqual(
        `A level must be rounded to the nearest ${DEFAULT_ROUNDING_CLOUD_LEVELS_M_ABOVE} m`,
      );
    });
    it('should return message with given value', () => {
      const testValue = 9999;
      expect(getInvalidStepsForMetersUnitAbove2970Message(testValue)).toEqual(
        `A level must be rounded to the nearest ${testValue} m`,
      );
    });
  });

  describe('validateRoundedStep', () => {
    it('should show error when level cloud unit is not defined', () => {
      expect(
        validateRoundedStep(
          '10',
          '',
          airmetConfig,
          airmetConfig.active_firs[0],
        ),
      ).toBe(invalidUnitMessage);
    });
    it('should show return true when value is empty', () => {
      expect(
        validateRoundedStep('', '', airmetConfig, airmetConfig.active_firs[0]),
      ).toBeTruthy();
    });

    it('should return correct error message for cloud_level_rounding_ft when no value is passed', () => {
      expect(
        validateRoundedStep(
          '15',
          'FT',
          airmetConfig,
          airmetConfig.active_firs[0],
        ),
      ).toEqual(
        getInvalidStepsForFeetUnitMessage(DEFAULT_ROUNDING_CLOUD_LEVELS_FT),
      );
    });

    it('should return correct error message for cloud_level_rounding_ft for given value', () => {
      expect(validateRoundedStep('15', 'FT', config, 'TEST')).toEqual(
        getInvalidStepsForFeetUnitMessage(
          config.fir_areas.TEST.cloud_level_rounding_ft,
        ),
      );
    });

    it('should return correct error message for cloud_level_rounding_m_below when no value is passed', () => {
      expect(
        validateRoundedStep(
          '15',
          'M',
          airmetConfig,
          airmetConfig.active_firs[0],
        ),
      ).toEqual(
        getInvalidStepsForMetersUnitBelow2970Message(
          DEFAULT_ROUNDING_CLOUD_LEVELS_M_BELOW,
        ),
      );
    });

    it('should return correct error message for cloud_level_rounding_m_below for given value', () => {
      expect(validateRoundedStep('15', 'M', config, 'TEST')).toEqual(
        getInvalidStepsForMetersUnitBelow2970Message(
          config.fir_areas.TEST.cloud_level_rounding_m_below,
        ),
      );
    });

    it('should return correct error message for cloud_level_rounding_m_above when no value is passed', () => {
      expect(
        validateRoundedStep(
          '3001',
          'M',
          airmetConfig,
          airmetConfig.active_firs[0],
        ),
      ).toEqual(
        getInvalidStepsForMetersUnitAbove2970Message(
          DEFAULT_ROUNDING_CLOUD_LEVELS_M_ABOVE,
        ),
      );
    });

    it('should return correct error message for cloud_level_rounding_m_above for given value', () => {
      expect(validateRoundedStep('3001', 'M', config, 'TEST')).toEqual(
        getInvalidStepsForMetersUnitAbove2970Message(
          config.fir_areas.TEST.cloud_level_rounding_m_above,
        ),
      );
    });
  });
});
