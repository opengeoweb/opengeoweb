/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';

import ObservationForecastTime from './ObservationForecastTime';
import { TestWrapper } from '../../../utils/testUtils';
import { translateKeyOutsideComponents } from '../../../utils/i18n';

describe('components/ProductForms/ProductFormFields/ObservationForecastTime', () => {
  it('should not be possible to set a date later then validDateStart when isObservationOrForecast = OBS', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              isObservationOrForecast: 'OBS',
              validDateStart: '2020-11-17T13:03+00:00',
            },
          }}
        >
          <ObservationForecastTime
            isDisabled={false}
            isReadOnly={false}
            helperText=""
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const field = screen.getByRole('textbox');
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('observation-forecast-error'),
      ),
    ).toBeFalsy();

    fireEvent.change(field, { target: { value: '20/11/2120 13:03' } });
    await screen.findByText(
      translateKeyOutsideComponents('observation-forecast-error'),
    );

    fireEvent.change(field, { target: { value: '16/11/2020 13:03' } });

    await waitFor(() => {
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('observation-forecast-error'),
        ),
      ).toBeFalsy();
    });
  });

  it('should not be possible to set a date ouside the valid time when isObservationOrForecast = FCST', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            isObservationOrForecast: 'FCST',
            validDateStart: '2020-11-17T13:03:00Z',
            validDateEnd: '2020-11-17T14:03:00Z',
          },
        }}
      >
        <ObservationForecastTime
          isDisabled={false}
          isReadOnly={false}
          helperText=""
        />
      </ReactHookFormProvider>,
    );

    const field = screen.getByRole('textbox');
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('surface-visibility-between-time'),
      ),
    ).toBeFalsy();

    fireEvent.change(field, { target: { value: '16/11/2020 13:03' } });
    await screen.findByText(
      translateKeyOutsideComponents('surface-visibility-between-time'),
    );

    fireEvent.change(field, { target: { value: '17/11/2020 13:33' } });
    await waitFor(() => {
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('surface-visibility-between-time'),
        ),
      ).toBeFalsy();
    });

    fireEvent.change(field, { target: { value: '18/11/2020 13:03' } });
    await screen.findByText(
      translateKeyOutsideComponents('surface-visibility-between-time'),
    );
  });

  it('should allow a date equal to start or end time when isObservationOrForecast = FCST', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            isObservationOrForecast: 'FCST',
            validDateStart: '2020-11-17T13:03:00Z',
            validDateEnd: '2020-11-17T14:03:00Z',
          },
        }}
      >
        <ObservationForecastTime
          isDisabled={false}
          isReadOnly={false}
          helperText=""
        />
      </ReactHookFormProvider>,
    );

    const field = screen.getByRole('textbox');
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('surface-visibility-between-time'),
      ),
    ).toBeFalsy();

    fireEvent.change(field, { target: { value: '16/11/2020 13:03' } });
    await screen.findByText(
      translateKeyOutsideComponents('surface-visibility-between-time'),
    );

    fireEvent.change(field, { target: { value: '17/11/2020 13:03' } });
    await waitFor(() => {
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('surface-visibility-between-time'),
        ),
      ).toBeFalsy();
    });

    fireEvent.change(field, { target: { value: '18/11/2020 13:03' } });
    await screen.findByText(
      translateKeyOutsideComponents('surface-visibility-between-time'),
    );

    fireEvent.change(field, { target: { value: '17/11/2020 14:03' } });
    await waitFor(() => {
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('surface-visibility-between-time'),
        ),
      ).toBeFalsy();
    });
  });

  it('should show the correct readOnly and disabled input', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            observationOrForecastTime: '2020-11-17T13:03+00:00',
          },
        }}
      >
        <ObservationForecastTime isDisabled isReadOnly helperText="" />
      </ReactHookFormProvider>,
    );

    const input: HTMLInputElement = screen.getByRole('textbox');

    expect(input).toBeTruthy();
    expect(input.disabled).toBeTruthy();
  });

  it('should hide the input when readOnly and no value', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            observationOrForecastTime: null,
          },
        }}
      >
        <ObservationForecastTime isDisabled={false} isReadOnly helperText="" />
      </ReactHookFormProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByRole('textbox')).toBeFalsy();
    });
  });

  it('should handle a helperText', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            observationOrForecastTime: null,
          },
        }}
      >
        <ObservationForecastTime
          isDisabled={false}
          isReadOnly={false}
          helperText="test-text"
        />
      </ReactHookFormProvider>,
    );

    await waitFor(() => {
      expect(screen.getByText('test-text')).toBeTruthy();
    });
  });
});
