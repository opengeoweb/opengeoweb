/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { Provider } from 'react-redux';
import { Theme } from '@mui/material';
import { lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import { Store } from '@reduxjs/toolkit';
import {
  TimeSliderLiteI18nProvider,
  TimeSliderLiteLanguage,
} from '../locales/i18n';

interface BaseProviderProps {
  children?: React.ReactNode;
  theme?: Theme;
}

interface ThemeI18nProviderProps extends BaseProviderProps {
  language?: TimeSliderLiteLanguage;
}

const ThemeProvider: React.FC<BaseProviderProps> = ({
  children,
  theme = lightTheme,
}) => <ThemeWrapper theme={theme}>{children}</ThemeWrapper>;

export const ThemeI18nProvider: React.FC<ThemeI18nProviderProps> = ({
  children,
  theme = lightTheme,
  language = 'en',
}) => {
  return (
    <ThemeProvider theme={theme}>
      <TimeSliderLiteI18nProvider language={language}>
        {children}
      </TimeSliderLiteI18nProvider>
    </ThemeProvider>
  );
};

interface ThemeStoreI18nProviderProps extends BaseProviderProps {
  store: Store;
  language?: TimeSliderLiteLanguage;
}

export const StoreProvider: React.FC<{
  children: React.ReactNode;
  store: Store;
}> = ({ children, store }) => (
  <Provider store={store}>{children as React.ReactElement}</Provider>
);

export const ThemeStoreI18nProvider: React.FC<ThemeStoreI18nProviderProps> = ({
  children,
  theme = lightTheme,
  store,
  language = 'en',
}) => (
  <StoreProvider store={store}>
    <ThemeProvider theme={theme}>
      <TimeSliderLiteI18nProvider language={language}>
        {children as React.ReactElement}
      </TimeSliderLiteI18nProvider>
    </ThemeProvider>
  </StoreProvider>
);
