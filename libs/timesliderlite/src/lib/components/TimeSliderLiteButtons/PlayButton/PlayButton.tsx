/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { Play, Pause } from '@opengeoweb/theme';
import { IconButton, Tooltip } from '@mui/material';
import { TimeSliderLiteCustomSettings } from '../../TimeSliderLite/timeSliderLiteUtils';
import { timeSliderLiteButtonDefaultProps as buttonDefaultProps } from '../TimeSliderLiteButtonUtils';
import { useTimesliderLiteTranslation } from '../../../locales/i18n';

export interface PlayButtonProps {
  isAnimating?: boolean;
  isDisabled?: boolean;
  onTogglePlayButton: () => void;
  settings?: TimeSliderLiteCustomSettings['playButton'];
  tooltipDelay?: number;
}

const PlayButton: React.FC<PlayButtonProps> = ({
  isAnimating,
  isDisabled,
  onTogglePlayButton,
  settings,
  tooltipDelay,
}) => {
  const { t } = useTimesliderLiteTranslation();

  const pauseIcon = settings?.icon?.pause ?? <Pause />;
  const playIcon = settings?.icon?.play ?? <Play />;

  return (
    <Tooltip
      title={isAnimating ? t('controlButtons.pause') : t('controlButtons.play')}
      placement="top"
      enterDelay={tooltipDelay}
      enterNextDelay={tooltipDelay}
    >
      <span>
        <IconButton
          {...buttonDefaultProps}
          sx={{
            ...(isAnimating && {
              backgroundColor:
                'geowebColors.timeSliderLite.selected.backgroundColor',
            }),
            ...buttonDefaultProps.sx,
          }}
          onClick={(): void => {
            onTogglePlayButton();
          }}
          disabled={isDisabled}
          data-testid="TimeSliderLite-PlayButton"
        >
          {isAnimating ? pauseIcon : playIcon}
        </IconButton>
      </span>
    </Tooltip>
  );
};

export default PlayButton;
