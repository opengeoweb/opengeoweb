/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { Options } from '@opengeoweb/theme';
import { IconButton, Tooltip } from '@mui/material';
import { TimeSliderLiteCustomSettings } from '../../TimeSliderLite/timeSliderLiteUtils';
import { timeSliderLiteButtonDefaultProps as buttonDefaultProps } from '../TimeSliderLiteButtonUtils';
import { useTimesliderLiteTranslation } from '../../../locales/i18n';

interface Props {
  menuOpen?: boolean;
  onClick?: () => void;
  settings?: TimeSliderLiteCustomSettings['menuButton'];
}

const MenuButton: React.FC<Props> = ({ menuOpen, onClick, settings }) => {
  const { t } = useTimesliderLiteTranslation();
  const icon = settings?.icon ?? <Options />;

  const Button = (
    <IconButton
      {...buttonDefaultProps}
      sx={{
        ...(menuOpen && {
          backgroundColor:
            'geowebColors.timeSliderLite.selected.backgroundColor',
        }),
        ...buttonDefaultProps.sx,
      }}
      aria-selected={menuOpen}
      onClick={onClick}
      data-testid="TimeSliderLite-MenuButton"
    >
      {icon}
    </IconButton>
  );

  if (menuOpen) {
    return Button;
  }
  return (
    <Tooltip placement="top-start" title={t('menu.title')}>
      {Button}
    </Tooltip>
  );
};

export default MenuButton;
