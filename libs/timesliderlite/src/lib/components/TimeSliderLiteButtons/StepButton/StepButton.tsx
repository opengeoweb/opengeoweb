/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { StepBackward, StepForward } from '@opengeoweb/theme';
import { IconButton, Tooltip } from '@mui/material';
import { TimeSliderLiteCustomSettings } from '../../TimeSliderLite/timeSliderLiteUtils';
import { timeSliderLiteButtonDefaultProps as buttonDefaultProps } from '../TimeSliderLiteButtonUtils';
import { useTimesliderLiteTranslation } from '../../../locales/i18n';

export interface StepButtonProps {
  forward?: boolean;
  isDisabled?: boolean;
  onClick?: () => void;
  settings?: TimeSliderLiteCustomSettings['stepButton'];
  tooltipDelay?: number;
}

const StepButton: React.FC<StepButtonProps> = ({
  forward,
  isDisabled,
  onClick = (): void => {},
  settings,
  tooltipDelay,
}: StepButtonProps) => {
  const { t } = useTimesliderLiteTranslation();
  const forwardTitle = t('controlButtons.forward');
  const backwardTitle = t('controlButtons.backward');

  const forwardIcon = settings?.icon?.forward ?? (
    <StepForward data-testid="step-forward-svg-path" />
  );
  const backwardIcon = settings?.icon?.backward ?? (
    <StepBackward data-testid="step-backward-svg-path" />
  );

  return (
    <Tooltip
      placement="top"
      title={forward ? forwardTitle : backwardTitle}
      enterDelay={tooltipDelay}
      enterNextDelay={tooltipDelay}
    >
      <span>
        <IconButton
          {...buttonDefaultProps}
          disabled={isDisabled}
          aria-label={forward ? forwardTitle : backwardTitle}
          onClick={onClick}
          data-testid={`TimeSliderLite-StepButton${
            forward ? 'Forward' : 'Backward'
          }`}
        >
          {forward ? forwardIcon : backwardIcon}
        </IconButton>
      </span>
    </Tooltip>
  );
};

export default StepButton;
