/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  mapSelectors,
  mapActions,
  genericActions,
  mapEnums,
  CoreAppStore,
} from '@opengeoweb/store';
import { handleDateUtilsISOString } from '@opengeoweb/webmap';
import { defaultAnimationDelayAtStart } from '@opengeoweb/timeslider';
import TimeSliderLite, { TimeSliderLiteProps } from './TimeSliderLite';
import {
  HOUR_TO_MINUTE,
  SECOND_TO_MILLISECOND,
  TimeSliderLiteHandleUpdateOptions,
  useAutoUpdate,
  useDefaultTimeRange,
  isWithinRange,
  MINUTE_TO_SECOND,
} from './timeSliderLiteUtils';
import TimeSliderLiteOptionsMenu, {
  TimeSliderLiteOptionsMenuProps,
} from '../TimeSliderLiteOptionsMenu/TimeSliderLiteOptionsMenu';
import {
  TimeZone,
  TimeZoneSwitchProps,
} from '../TimeSliderLiteOptionsMenu/TimeZoneSwitch/TimeZoneSwitch';
import { useOnToggleAnimation } from './useOnToggleAnimation';

const TWO_DAYS_IN_SECONDS = 2 * 24 * 60 * 60;
export const TIME_SLIDER_LITE_DEFAULT_TIME_RANGE = TWO_DAYS_IN_SECONDS;

export interface TimeSliderLiteConnectProps
  extends Partial<TimeSliderLiteProps> {
  overrideUseTimeRange?: [
    [number, number] | null,
    (timeRange: [number, number] | null) => void,
  ];
  overrideUseTimeStep?: [
    [number, number] | null,
    (timeStep: [number, number] | null) => void,
  ];
  overrideUseTimeZone?: [TimeZone, (timeZone: TimeZone) => void];
  overrideUseUpdateInterval?: [
    number | null,
    (updateInterval: number | null) => void,
  ];
  overrideHandleAutoUpdate?: (
    options: TimeSliderLiteHandleUpdateOptions,
  ) => void;
  /**
   * Function to customize selected time rounding
   * Default behaviour: Only cast to ISO string
   */
  preTimeDimensionSelect?: (mapId: string, selectedTime: number) => string;
  sourceId: string;
  mapId: string;
  isAlwaysVisible?: boolean;
  TimeZoneSwitch?: React.FC<TimeZoneSwitchProps>;
  TimeSliderLiteOptionsMenuProps?: Partial<
    Omit<
      TimeSliderLiteOptionsMenuProps,
      | 'useTimeRange'
      | 'useTimeStep'
      | 'useAnimationSpeed'
      | 'useTimeZone'
      | 'currentTime'
    >
  >;
}

const TimeSliderLiteConnect: React.FC<TimeSliderLiteConnectProps> = ({
  overrideUseTimeRange,
  overrideUseTimeStep,
  overrideUseTimeZone,
  overrideUseUpdateInterval,
  overrideHandleAutoUpdate,
  preTimeDimensionSelect = (_: string, selectedTime: number): string =>
    new Date(selectedTime * SECOND_TO_MILLISECOND).toISOString(),
  sourceId,
  mapId,
  isAlwaysVisible = false,
  TimeSliderLiteOptionsMenuProps,
  ...timeSliderProps
}: TimeSliderLiteConnectProps) => {
  const [menuOpen, setMenuOpen] = React.useState(false);
  const useTimeRange = React.useState<[number, number] | null>(null);
  const [timeRange, setTimeRange] = overrideUseTimeRange || useTimeRange;
  const useTimeStep = React.useState<[number, number] | null>(null);
  const [timeStep, setTimeStep] = overrideUseTimeStep || useTimeStep;
  const useTimeZone = React.useState<TimeZone>('LT');
  const [timeZone, setTimeZone] = overrideUseTimeZone || useTimeZone;

  const useUpdateInterval = React.useState<number | null>(null);
  const [updateInterval, setUpdateInterval] =
    overrideUseUpdateInterval || useUpdateInterval;

  const dispatch = useDispatch();

  const { currentTime } = useAutoUpdate(timeRange, setTimeRange, {
    ...TimeSliderLiteOptionsMenuProps,
    handleUpdate: overrideHandleAutoUpdate,
  });

  const isTimeSliderVisible = useSelector((store: CoreAppStore) =>
    mapSelectors.isTimeSliderVisible(store, mapId),
  );
  const isVisible = isAlwaysVisible || isTimeSliderVisible;

  const onToggleTimeSliderVisibility = (): void => {
    dispatch(
      mapActions.toggleTimeSliderIsVisible({
        mapId,
        isTimeSliderVisible: !isVisible,
        origin: mapEnums.MapActionOrigin.map,
      }),
    );
  };

  const animationDelay = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapAnimationDelay(store, mapId),
  );
  const animationSpeed = animationDelay
    ? SECOND_TO_MILLISECOND / animationDelay
    : defaultAnimationDelayAtStart;

  const handleAnimationSpeedChange = (animationSpeed: number): void => {
    const animationDelay = SECOND_TO_MILLISECOND / animationSpeed;
    dispatch(
      mapActions.setAnimationDelay({
        mapId,
        animationDelay,
        origin: mapEnums.MapActionOrigin.map,
      }),
    );
  };

  const defaultTimeStep: [number, number] =
    TimeSliderLiteOptionsMenuProps?.defaultTimeStep ?? [5, 15];

  const layers = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapLayers(store, mapId),
  );

  const [defaultStartTime, defaultEndTime] = useDefaultTimeRange(
    layers,
    HOUR_TO_MINUTE,
    currentTime,
  );
  const [startTime, endTime] = timeRange || [defaultStartTime, defaultEndTime];

  const zeroRange = isWithinRange(startTime, endTime, 30 * MINUTE_TO_SECOND);

  const isAnimating = useSelector((store: CoreAppStore) =>
    mapSelectors.isAnimating(store, mapId),
  );

  useEffect(() => {
    if (zeroRange) {
      dispatch(mapActions.mapStopAnimation({ mapId }));
    }
  }, [dispatch, mapId, zeroRange]);

  const selectedTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getSelectedTime(store, mapId),
  );

  const onToggleAnimation = useOnToggleAnimation(
    dispatch,
    mapId,
    selectedTime,
    timeStep,
    defaultTimeStep,
    startTime,
    endTime,
    isAnimating,
    {
      ...timeSliderProps,
      timeZone,
    },
  );

  // Restart animation if auto updated
  useEffect(() => {
    if (isAnimating) {
      onToggleAnimation(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentTime]);

  const handleTimeRangeChange = (
    newTimeRange: [number, number] | null,
  ): void => {
    if (isAnimating && newTimeRange) {
      dispatch(mapActions.mapStopAnimation({ mapId }));
    }
    setTimeRange(newTimeRange);
  };

  const handleTimeStepChange = (newTimeStep: [number, number] | null): void => {
    if (isAnimating) {
      dispatch(mapActions.mapStopAnimation({ mapId }));
    }
    setTimeStep(newTimeStep);
  };

  return (
    <>
      {isVisible && menuOpen && (
        <TimeSliderLiteOptionsMenu
          menuOpen={menuOpen}
          setMenuOpen={setMenuOpen}
          defaultTimeRange={[defaultStartTime, defaultEndTime]}
          defaultTimeStep={defaultTimeStep} // TODO: Calculate from layers
          useTimeRange={[timeRange, handleTimeRangeChange]}
          useTimeStep={[timeStep, handleTimeStepChange]}
          useAnimationSpeed={[animationSpeed, handleAnimationSpeedChange]}
          useUpdateInterval={[updateInterval, setUpdateInterval]}
          useTimeZone={[timeZone, setTimeZone]}
          closeButtonIcon={timeSliderProps.settings?.closeButton?.icon}
          dropdownButtonIcon={timeSliderProps.settings?.dropdownButton?.icon}
          TimeZoneSwitch={timeSliderProps?.settings?.TimeZoneSwitch}
          {...TimeSliderLiteOptionsMenuProps}
        />
      )}
      <TimeSliderLite
        currentTime={currentTime}
        selectedTime={selectedTime}
        timeStep={timeStep ?? defaultTimeStep}
        timeZone={timeZone}
        overrideAnimation
        isAnimating={!!isAnimating}
        isVisible={isVisible}
        needleDisabled={zeroRange}
        mapId={mapId}
        setSelectedTime={(newSelectedTime: number): void => {
          const newDate = preTimeDimensionSelect(mapId, newSelectedTime);
          if (isAnimating) {
            dispatch(mapActions.mapStopAnimation({ mapId }));
          }
          dispatch(
            genericActions.setTime({
              sourceId,
              value: handleDateUtilsISOString(newDate),
              origin: mapEnums.MapActionOrigin.map,
            }),
          );
        }}
        menuOpen={menuOpen}
        onToggleMenu={(): void => setMenuOpen(!menuOpen)}
        onToggleTimeSlider={onToggleTimeSliderVisibility}
        onToggleAnimation={onToggleAnimation}
        startTime={startTime}
        endTime={endTime}
        {...timeSliderProps}
      />
    </>
  );
};

export default TimeSliderLiteConnect;
