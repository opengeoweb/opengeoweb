/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { renderHook } from '@testing-library/react';
import { HOUR_TO_SECOND } from './timeSliderLiteUtils';
import { useOnToggleAnimation } from './useOnToggleAnimation';

describe('components/TimeSliderLite/onToggleAnimation', () => {
  let result: (forceAnimationStart?: boolean) => void;
  const dispatch = jest.fn();
  const mapId = 'mockMapId';
  const timeStep = [5, 15] as [number, number];
  const defaultTimeStep = [5, 15] as [number, number];
  const startTime = 1701907200;
  const endTime = 1701990000;

  it('should start animation when onToggleAnimation is called', async () => {
    const selectedTime = 1701935472;
    const isAnimating = false;
    const timeSliderProps = { currentTime: 1701935472 };

    renderHook(() => {
      result = useOnToggleAnimation(
        dispatch,
        mapId,
        selectedTime,
        timeStep,
        defaultTimeStep,
        startTime,
        endTime,
        isAnimating,
        timeSliderProps,
      );
    });

    result();
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch.mock.calls[0][0].payload.timeList[0].value).toBe(
      '2023-12-07T01:00:00.000Z',
    );
  });

  it('should not offset animation start if onToggleAnimation is called with UTC time zone', async () => {
    const selectedTime = 1701935472;
    const isAnimating = false;
    const timeSliderProps = {
      currentTime: 1701935472,
      timeZone: 'UTC',
    } as const;

    renderHook(() => {
      result = useOnToggleAnimation(
        dispatch,
        mapId,
        selectedTime,
        [360, 360],
        defaultTimeStep,
        startTime + HOUR_TO_SECOND * 4,
        endTime,
        isAnimating,
        timeSliderProps,
      );
    });

    result();
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch.mock.calls[0][0].payload.timeList[0].value).toBe(
      '2023-12-07T06:00:00.000Z',
    );
  });

  it('should stop animation when onToggleAnimation is called and isAnimating is true', async () => {
    const selectedTime = 1701935472;
    const isAnimating = true;
    const timeSliderProps = { currentTime: 1701935472 };

    renderHook(() => {
      result = useOnToggleAnimation(
        dispatch,
        mapId,
        selectedTime,
        timeStep,
        defaultTimeStep,
        startTime,
        endTime,
        isAnimating,
        timeSliderProps,
      );
    });

    result();
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch.mock.calls[0][0].payload).toStrictEqual({
      mapId: 'mockMapId',
      origin: 'map',
    });
  });

  it('should start animation when onToggleAnimation is called and stopped when called again', async () => {
    const selectedTime = 1701935472;
    let isAnimating = false;
    const timeSliderProps = { currentTime: 1701935472 };

    renderHook(() => {
      result = useOnToggleAnimation(
        dispatch,
        mapId,
        selectedTime,
        timeStep,
        defaultTimeStep,
        startTime,
        endTime,
        isAnimating,
        timeSliderProps,
      );
    });

    result();
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch.mock.calls[0][0].payload.timeList[0].value).toBe(
      '2023-12-07T01:00:00.000Z',
    );

    isAnimating = true;
    renderHook(() => {
      result = useOnToggleAnimation(
        dispatch,
        mapId,
        selectedTime,
        timeStep,
        defaultTimeStep,
        startTime,
        endTime,
        isAnimating,
        timeSliderProps,
      );
    });

    result();
    expect(dispatch).toHaveBeenCalledTimes(2);
    expect(dispatch.mock.calls[1][0].payload).toStrictEqual({
      mapId: 'mockMapId',
      origin: 'map',
    });
  });

  it('should only init startAnimation dispacth when onToggleAnimation is called and selectedTime is outside startTime and endTime range', async () => {
    let result = (): void => {};
    const dispatch = jest.fn();
    const selectedTime = 1701471600;
    let isAnimating = false;
    const timeSliderProps = { currentTime: 1701471600 };

    renderHook(() => {
      result = useOnToggleAnimation(
        dispatch,
        mapId,
        selectedTime,
        timeStep,
        defaultTimeStep,
        startTime,
        endTime,
        isAnimating,
        timeSliderProps,
      );
    });

    result();
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch.mock.calls[0][0].payload.timeList[0].value).toBe(
      '2023-12-02T00:00:00.000Z',
    );

    isAnimating = true;
    renderHook(() => {
      result = useOnToggleAnimation(
        dispatch,
        mapId,
        selectedTime,
        timeStep,
        defaultTimeStep,
        startTime,
        endTime,
        isAnimating,
        timeSliderProps,
      );
    });

    // Thos won't stop the animation since the selectedTime is outside the time range
    // and startAnimation is called again
    result();
    expect(dispatch.mock.calls[0][0].payload.timeList[0].value).toBe(
      '2023-12-02T00:00:00.000Z',
    );
  });

  it('should remove passing hour if start time is over current time', async () => {
    let result = (): void => {};
    const dispatch = jest.fn();
    const selectedTime = 1701471600;
    // Set current time to 2023-12-07T00:00:01.000Z
    const timeSliderProps = { currentTime: startTime + 1 };

    renderHook(() => {
      result = useOnToggleAnimation(
        dispatch,
        mapId,
        selectedTime,
        timeStep,
        defaultTimeStep,
        startTime,
        endTime,
        false,
        timeSliderProps,
      );
    });

    result();
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch.mock.calls[0][0].payload.timeList[0].value).toBe(
      '2023-12-07T01:00:00.000Z',
    );
  });

  it.only('should not play animation in forecast if only observed is selected', async () => {
    let result = (): void => {};
    const dispatch = jest.fn();
    const selectedTime = 1701471600;
    const halfHourBeforeRoundedEndTime = endTime + HOUR_TO_SECOND / 2;
    const timeSliderProps = { currentTime: halfHourBeforeRoundedEndTime };
    const lastAvailableObservedTime = new Date(
      timeSliderProps.currentTime * 1000,
    ).toISOString();

    renderHook(() => {
      result = useOnToggleAnimation(
        dispatch,
        mapId,
        selectedTime,
        timeStep,
        defaultTimeStep,
        startTime,
        endTime,
        false,
        timeSliderProps,
      );
    });

    result();
    expect(dispatch).toHaveBeenCalledTimes(1);
    const calledTimeList = dispatch.mock.calls[0][0].payload.timeList;
    expect(calledTimeList[calledTimeList.length - 1]).toStrictEqual({
      name: 'time',
      value: lastAvailableObservedTime,
    });
  });
});
