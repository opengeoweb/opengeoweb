/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { publicLayers, MapControls } from '@opengeoweb/webmap-react';
import { useDefaultMapSettings } from '@opengeoweb/webmap-redux';

import {
  LegendConnect,
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
  MapViewConnect,
  LegendMapButtonConnect,
} from '@opengeoweb/core';
import type { Meta, StoryObj } from '@storybook/react';
import TimeSliderLiteConnect from './TimeSliderLiteConnect';
import { StoreProvider } from '../../testUtils/Providers';
import { createMockStore } from '../../testUtils/utils';

const meta: Meta<typeof TimeSliderLiteConnect> = {
  title: 'components/TimeSliderLiteConnect',
  tags: ['!autodocs'],
};

type Story = StoryObj<typeof TimeSliderLiteConnect>;

export default meta;

const store = createMockStore({});

interface ExampleComponentProps {
  mapId: string;
}

const ExampleComponent: React.FC<ExampleComponentProps> = ({
  mapId,
}: ExampleComponentProps) => {
  const [updateInterval, setUpdateInterval] = React.useState<number | null>(
    null,
  );

  useDefaultMapSettings({
    mapId,
    layers: [
      {
        ...publicLayers.radarLayer,
        id: `precipitation-observed-${mapId}`,
      },
      {
        ...publicLayers.harmoniePrecipitation,
        id: `precipitation-forecast-${mapId}`,
      },
    ],
    baseLayers: [
      {
        ...publicLayers.defaultLayers.baseLayerGrey,
        id: `baseGrey-${mapId}`,
      },
      publicLayers.defaultLayers.overLayer,
    ],
  });

  return (
    <div style={{ height: '100vh' }}>
      <LegendConnect mapId={mapId} />
      <MapControls>
        <LegendMapButtonConnect mapId={mapId} />
        <LayerManagerMapButtonConnect mapId={mapId} />
      </MapControls>
      <div id="root" style={{ height: '100vh' }}>
        <MapViewConnect
          mapId={mapId}
          shouldAutoFetch={updateInterval ?? true}
        />
        <LayerManagerConnect bounds="#root" />
      </div>
      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          width: 'calc(100% - 16px)',
          zIndex: 10,
          margin: '8px',
        }}
      >
        <TimeSliderLiteConnect
          sourceId="timeslider-1"
          mapId={mapId}
          overrideUseUpdateInterval={[updateInterval, setUpdateInterval]}
          TimeSliderLiteOptionsMenuProps={{
            sx: {
              position: 'absolute',
              bottom: '40px',
            },
            subMenuSx: {
              position: 'absolute',
              bottom: '100px',
            },
          }}
        />
      </div>
    </div>
  );
};

export const DemoTimeSliderLiteConnect: Story = {
  render: () => (
    <StoreProvider store={store}>
      <ExampleComponent mapId="mapid_1" />
    </StoreProvider>
  ),
};

DemoTimeSliderLiteConnect.storyName = 'Time Slider Lite Connect';
