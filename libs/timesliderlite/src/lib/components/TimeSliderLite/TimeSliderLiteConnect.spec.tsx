/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { mapActions } from '@opengeoweb/store';
import { fireEvent, render, screen } from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';
import TimeSliderLiteConnect from './TimeSliderLiteConnect';
import {
  DAY_TO_SECOND,
  HOUR_TO_MINUTE,
  HOUR_TO_SECOND,
} from './timeSliderLiteUtils';
import { ThemeStoreI18nProvider } from '../../testUtils/Providers';
import { createMockStore } from '../../testUtils/utils';

describe('components/TimeSliderLite/TimeSliderLiteConnect', () => {
  it('should render the component when the map has a layer with time dimension', () => {
    const mapId = 'mapid_1';
    const store = createMockStore();
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <ThemeStoreI18nProvider theme={lightTheme} store={store}>
        <TimeSliderLiteConnect sourceId="timeslider-1" mapId={mapId} />
      </ThemeStoreI18nProvider>,
    );

    expect(screen.getByTestId('timeSliderLite-mapid_1')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-MenuButton')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-PlayButton')).toBeTruthy();
    expect(
      screen.getByTestId('TimeSliderLite-StepButtonBackward'),
    ).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-StepButtonForward')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-Background')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-HideButton')).toBeTruthy();
  });

  const currentTime = '2020-03-13T13:00:00Z';
  const currentTimeSeconds = new Date(currentTime).getTime() / 1000;

  const defaultTimeStep = [HOUR_TO_MINUTE, HOUR_TO_MINUTE] as [number, number];

  const expectedTooltipTimeStamp = (timeInSeconds: number): string =>
    new Date(timeInSeconds * 1000).toLocaleDateString('en', {
      weekday: 'short',
      month: 'numeric',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
    });

  it('should have modifiable default time step', () => {
    const mapId = 'mapid_1';
    const store = createMockStore();
    store.dispatch(mapActions.registerMap({ mapId }));
    store.dispatch(
      mapActions.mapChangeDimension({
        mapId,
        dimension: {
          name: 'time',
          currentValue: currentTime,
        },
        origin: 'map',
      }),
    );

    render(
      <ThemeStoreI18nProvider store={store}>
        <TimeSliderLiteConnect
          sourceId="timeslider-2"
          mapId={mapId}
          currentTime={currentTimeSeconds}
          selectedTime={currentTimeSeconds}
          startTime={currentTimeSeconds - DAY_TO_SECOND}
          endTime={currentTimeSeconds + DAY_TO_SECOND}
          TimeSliderLiteOptionsMenuProps={{
            defaultTimeStep,
          }}
        />
      </ThemeStoreI18nProvider>,
    );

    const tooltip = screen.getByText(
      expectedTooltipTimeStamp(currentTimeSeconds),
    );
    expect(tooltip).toBeTruthy();

    // Should increment by defaultTimeStep
    fireEvent.click(screen.getByTestId('TimeSliderLite-StepButtonForward'));

    const tooltipAfterIncrement = screen.getByText(
      expectedTooltipTimeStamp(currentTimeSeconds + HOUR_TO_SECOND),
    );
    expect(tooltipAfterIncrement).toBeTruthy();
  });

  it('should use prop preTimeDimensionSelect', () => {
    const preTimeDimensionSelect = jest.fn().mockReturnValue(currentTime);

    const mapId = 'mapid_2';
    const store = createMockStore();
    store.dispatch(mapActions.registerMap({ mapId }));
    store.dispatch(
      mapActions.mapChangeDimension({
        mapId,
        dimension: {
          name: 'time',
          currentValue: currentTime,
        },
        origin: 'map',
      }),
    );

    render(
      <ThemeStoreI18nProvider store={store}>
        <TimeSliderLiteConnect
          sourceId="timeslider-2"
          mapId={mapId}
          currentTime={currentTimeSeconds}
          selectedTime={currentTimeSeconds}
          preTimeDimensionSelect={preTimeDimensionSelect}
          startTime={currentTimeSeconds - DAY_TO_SECOND}
          endTime={currentTimeSeconds + DAY_TO_SECOND}
          TimeSliderLiteOptionsMenuProps={{
            defaultTimeStep,
          }}
        />
      </ThemeStoreI18nProvider>,
    );

    // Should increment by defaultTimeStep
    fireEvent.click(screen.getByTestId('TimeSliderLite-StepButtonForward'));

    expect(preTimeDimensionSelect).toHaveBeenCalled();
    expect(preTimeDimensionSelect).toHaveBeenCalledWith(
      mapId,
      currentTimeSeconds + HOUR_TO_SECOND,
    );
  });
});
