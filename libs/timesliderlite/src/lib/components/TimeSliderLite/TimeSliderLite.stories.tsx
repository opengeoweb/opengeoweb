/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { Box, Slider } from '@mui/material';

import { SpeedFactorType } from '@opengeoweb/timeslider';
import { isArray } from 'lodash';
import type { Meta, StoryObj } from '@storybook/react';
import { StorybookDocsWrapper } from '@opengeoweb/shared';
import TimeSliderLite, { TimeSliderLiteProps } from './TimeSliderLite';
import TimeSliderLiteOptionsMenu, {
  TimeSliderLiteOptionsMenuProps,
} from '../TimeSliderLiteOptionsMenu/TimeSliderLiteOptionsMenu';
import {
  HOUR_TO_SECOND,
  MILLISECOND_TO_SECOND,
  MINUTE_TO_SECOND,
  SECOND_TO_MILLISECOND,
  getTimeScaleDescription,
} from './timeSliderLiteUtils';
import { TimeZone } from '../TimeSliderLiteOptionsMenu/TimeZoneSwitch/TimeZoneSwitch';

const meta: Meta<typeof TimeSliderLite> = {
  title: 'components/TimeSliderLite',
  component: TimeSliderLite,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the TimeSliderLite',
      },
    },
  },
  argTypes: {
    controlButtons: { control: false },
    hideButton: { control: false },
    settings: { control: false },
  },
};
export default meta;

type Story = StoryObj<typeof TimeSliderLite>;

interface TimeScaleSliderProps {
  timeScale: number;
  setTimeScale: (timeScale: number) => void;
}

/**
 * Time scale slider for testing update interval
 */
const TimeScaleSlider: React.FC<TimeScaleSliderProps> = ({
  timeScale,
  setTimeScale,
}) => {
  const value = getTimeScaleDescription(timeScale);

  return (
    <div
      style={{
        width: '300px',
        position: 'absolute',
        right: '0px',
      }}
    >
      <span>Time scale: {value}</span>
      <Slider
        min={1}
        max={3 * HOUR_TO_SECOND}
        step={60}
        value={timeScale}
        onChange={(_event: Event, value: number | number[]): void => {
          const timeScale = isArray(value) ? value[0] : value;
          setTimeScale(timeScale);
        }}
      />
    </div>
  );
};

// Tue May 02 2023 07:17:06 GMT+0200 (Central European Summer Time)
const initCurrentTime = 1683003626;
const currentHour = new Date(
  initCurrentTime * SECOND_TO_MILLISECOND,
).getHours();
const currentMinutes = new Date(
  initCurrentTime * SECOND_TO_MILLISECOND,
).getMinutes();
const currentSeconds = new Date(
  initCurrentTime * SECOND_TO_MILLISECOND,
).getSeconds();
const currentMilliseconds = new Date(
  initCurrentTime * SECOND_TO_MILLISECOND,
).getMilliseconds();

const fourDaysAgo = 1682831826;
const fourDaysAgoDayStart = new Date(fourDaysAgo * 1000).setHours(
  currentHour,
  currentMinutes,
  currentSeconds,
  currentMilliseconds,
);

const fourDaysFromNow = 1683177426;
const fourDaysFromNowDayEnd = new Date(fourDaysFromNow * 1000).setHours(
  currentHour,
  currentMinutes,
  currentSeconds,
  currentMilliseconds,
);

const defaultTimeStep: [number, number] = [5, 15];
const defaultAnimationSpeed: SpeedFactorType = 4;
const customAnimationSpeedOptions: SpeedFactorType[] = [16, 8, 4, 2, 1];
const defaultUpdateInterval = 5;
const customUpdateIntervalOptions = [5, 10, 15, 30, 60];

const defaultOptionsMenuProps: Omit<
  TimeSliderLiteOptionsMenuProps,
  'defaultTimeRange'
> = {
  sx: { position: 'absolute', bottom: '48px' },
  subMenuSx: { position: 'absolute', bottom: '108px' },
  defaultTimeStep,
  defaultSpeedFactor: defaultAnimationSpeed,
  defaultUpdateInterval,
  speedOptions: customAnimationSpeedOptions,
  updateIntervalOptions: customUpdateIntervalOptions,
};

const defaultTimeSliderLiteProps: Omit<
  TimeSliderLiteProps,
  | 'isVisible'
  | 'onToggleTimeSlider'
  | 'onToggleMenu'
  | 'isAnimating'
  | 'onToggleAnimation'
  | 'selectedTime'
  | 'setSelectedTime'
  | 'startTime'
  | 'endTime'
> = {
  mapId: 'map_1',
  currentTime: initCurrentTime,
  height: 44,
  sx: {
    position: 'absolute',
    bottom: '4px',
  },
};

export const Component: Story = {
  args: {
    ...defaultTimeSliderLiteProps,
    isVisible: true,
    currentTime: initCurrentTime,
    menuOpen: true,
    isAnimating: false,
    selectedTime: initCurrentTime,
    timeStep: defaultTimeStep,
    animationSpeed: defaultAnimationSpeed,
    timeZone: 'LT',
    startTime: fourDaysAgoDayStart * MILLISECOND_TO_SECOND,
    endTime: fourDaysFromNowDayEnd * MILLISECOND_TO_SECOND,
    setSelectedTime: () => {},
    onToggleTimeSlider: () => {},
    onToggleMenu: () => {},
    onToggleAnimation: () => {},
    needleDisabled: false,
    hideMenuButton: false,
    hideStepButtons: false,
    hidePlayButton: false,
    mouseHoverNeedleOpacity: 0.5,
    draggableTimeStamp: false,
    needleWidth: 5,
    needleLabelZIndex: 1,
    needleLabelOffset: [0, -5],
    needleDragAreaWidth: 10,
    overrideAnimation: false,
  },

  render: (props) => (
    <Box
      sx={{
        margin: '10px',
        height: '80px',
        position: 'relative',
      }}
    >
      <TimeSliderLite {...props} />
    </Box>
  ),
};

export const ComponentDark: Story = {
  ...Component,
  tags: ['dark'],
  render: (props) => (
    <StorybookDocsWrapper isDark>
      {' '}
      <Box
        sx={{
          margin: '10px',
          height: '80px',
          position: 'relative',
        }}
      >
        <TimeSliderLite {...props} />
      </Box>
    </StorybookDocsWrapper>
  ),
};

const ExampleComponent: React.FC = () => {
  const [storyTimeScale, setStoryTimeScale] = React.useState<number>(1);

  const [isVisible, setIsVisible] = React.useState(true);
  const [isAnimating, setIsAnimating] = React.useState(false);
  const [selectedTime, setSelectedTime] =
    React.useState<number>(initCurrentTime);
  const [currentTime, setCurrentTime] = React.useState<number>(initCurrentTime);
  const [timeRange, setTimeRange] = React.useState<[number, number] | null>(
    null,
  );
  const [timeStep, setTimeStep] = React.useState<[number, number] | null>(null);
  const [animationSpeed, setAnimationSpeed] = React.useState<number>(
    defaultAnimationSpeed,
  );
  const [timeZone, setTimeZone] = React.useState<TimeZone>('LT');
  const [updateInterval, setUpdateInterval] = React.useState<number | null>(
    null,
  );
  const [updateIntervalInstance, setUpdateIntervalInstance] =
    React.useState<NodeJS.Timeout | null>(null);
  const [menuOpen, setMenuOpen] = React.useState(false);
  const [startTime, setStartTime] = React.useState(
    fourDaysAgoDayStart * MILLISECOND_TO_SECOND,
  );
  const [endTime, setEndTime] = React.useState(
    fourDaysFromNowDayEnd * MILLISECOND_TO_SECOND,
  );

  // Mock current time and update interval for testing
  React.useEffect(() => {
    const updateIntervalSeconds =
      (updateInterval ?? defaultUpdateInterval) * MINUTE_TO_SECOND;
    clearInterval(updateIntervalInstance as NodeJS.Timeout);

    const interval = setInterval(
      () => {
        setStartTime((prevStartTime) => prevStartTime + updateIntervalSeconds);
        setEndTime((prevEndTime) => prevEndTime + updateIntervalSeconds);
        setTimeRange((prevTimeRange) =>
          prevTimeRange
            ? [
                prevTimeRange[0] + updateIntervalSeconds,
                prevTimeRange[1] + updateIntervalSeconds,
              ]
            : null,
        );
        setCurrentTime(
          (prevCurrentTime) => prevCurrentTime + updateIntervalSeconds,
        );
      },
      (updateIntervalSeconds * SECOND_TO_MILLISECOND) / storyTimeScale,
    );
    setUpdateIntervalInstance(updateIntervalInstance);

    return (): void => clearInterval(interval);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [updateInterval, storyTimeScale]);

  const animationSpeedValue = animationSpeed ?? defaultAnimationSpeed;

  const onToggleTimeSlider = (): void => {
    setIsVisible(!isVisible);
  };

  const onToggleAnimation = (): void => {
    setIsAnimating(!isAnimating);
  };

  const onToggleMenu = (): void => {
    setMenuOpen(!menuOpen);
  };

  return (
    <Box
      sx={{
        margin: '10px',
        height: '370px',
        position: 'relative',
      }}
    >
      {isVisible && menuOpen && (
        <>
          <TimeScaleSlider
            timeScale={storyTimeScale}
            setTimeScale={setStoryTimeScale}
          />
          <TimeSliderLiteOptionsMenu
            {...defaultOptionsMenuProps}
            menuOpen={menuOpen}
            setMenuOpen={setMenuOpen}
            currentTime={currentTime}
            defaultTimeRange={[startTime, endTime]}
            useTimeRange={[timeRange, setTimeRange]}
            useTimeStep={[timeStep, setTimeStep]}
            useAnimationSpeed={[animationSpeed, setAnimationSpeed]}
            useUpdateInterval={[updateInterval, setUpdateInterval]}
            useTimeZone={[timeZone, setTimeZone]}
          />
        </>
      )}
      <TimeSliderLite
        {...defaultTimeSliderLiteProps}
        currentTime={currentTime}
        menuOpen={menuOpen}
        isVisible={isVisible}
        onToggleTimeSlider={onToggleTimeSlider}
        onToggleMenu={onToggleMenu}
        isAnimating={isAnimating}
        onToggleAnimation={onToggleAnimation}
        selectedTime={selectedTime}
        timeStep={timeStep ?? defaultTimeStep}
        timeZone={timeZone}
        startTime={timeRange ? timeRange[0] : startTime}
        endTime={timeRange ? timeRange[1] : endTime}
        setSelectedTime={setSelectedTime}
        animationSpeed={animationSpeedValue}
        needleDisabled={!!timeRange && timeRange[0] === timeRange[1]}
      />
    </Box>
  );
};

export const DemoTimeSliderLite: Story = {
  render: () => <ExampleComponent />,
};

// Snapshot generated using Central European Summer Time (UTC+2)
DemoTimeSliderLite.tags = ['snapshot', '!autodocs'];
