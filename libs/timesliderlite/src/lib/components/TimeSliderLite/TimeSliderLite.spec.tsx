/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

import { colorsLight } from '@opengeoweb/theme';
import TimeSliderLite, { TimeSliderLiteProps } from './TimeSliderLite';
import {
  MILLISECOND_TO_SECOND,
  DAY_TO_SECOND,
  HOUR_TO_SECOND,
} from './timeSliderLiteUtils';
import { ThemeI18nProvider } from '../../testUtils/Providers';

const defaultCurrentDate = new Date('2023-06-12T00:00:00Z');
defaultCurrentDate.setHours(23);
const defaultCurrentTime = defaultCurrentDate.getTime() * MILLISECOND_TO_SECOND;

describe('src/components/TimeSliderLite/TimeSliderLite', () => {
  const defaultProps: TimeSliderLiteProps = {
    mapId: 'map_1',
    currentTime: defaultCurrentTime,
    selectedTime: defaultCurrentTime,
    startTime: Math.floor(defaultCurrentTime - 2 * DAY_TO_SECOND),
    endTime: defaultCurrentTime + 2 * DAY_TO_SECOND,
    timeStep: [60, 60],
    height: 44,
    isVisible: true,
    onToggleAnimation: (): void => {},
    onToggleMenu: (): void => {},
    onToggleTimeSlider: (): void => {},
    setSelectedTime: (): void => {},
  };

  it('should render control buttons', () => {
    render(
      <ThemeI18nProvider>
        <TimeSliderLite {...defaultProps} />
      </ThemeI18nProvider>,
    );
    expect(screen.getByTestId('TimeSliderLite-HideButton')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-MenuButton')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-PlayButton')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-StepButtonForward')).toBeTruthy();
    expect(
      screen.getByTestId('TimeSliderLite-StepButtonBackward'),
    ).toBeTruthy();
  });

  it('should render slider background with observed and forecast days', () => {
    render(
      <ThemeI18nProvider>
        <TimeSliderLite {...defaultProps} />
      </ThemeI18nProvider>,
    );
    expect(screen.getByTestId('TimeSliderLite-Background')).toBeTruthy();

    const observedDays = screen.queryAllByTestId(
      'TimeSliderLite-BackgroundRectangle-Observed',
    );
    expect(observedDays.length).toBeGreaterThan(0);

    const forecastDays = screen.queryAllByTestId(
      'TimeSliderLite-BackgroundRectangle-Forecast',
    );
    expect(forecastDays.length).toBeGreaterThan(0);
  });

  it('should truncate observed days if only forecast days selected', () => {
    render(
      <ThemeI18nProvider>
        <TimeSliderLite {...defaultProps} startTime={defaultCurrentTime + 1} />
      </ThemeI18nProvider>,
    );

    const truncatedObservedDates = screen.queryAllByTestId(
      'TimeSliderLite-BackgroundRectangle-Observed',
    );
    expect(truncatedObservedDates.length).toBe(0);
  });

  it('should call onToggleMenu on menu button click', () => {
    const onToggleMenu = jest.fn();
    render(
      <ThemeI18nProvider>
        <TimeSliderLite {...defaultProps} onToggleMenu={onToggleMenu} />
      </ThemeI18nProvider>,
    );
    fireEvent.click(screen.getByTestId('TimeSliderLite-MenuButton'));
    expect(onToggleMenu).toHaveBeenCalled();
  });

  it('should call onToggleAnimation on play button click', () => {
    const onToggleAnimation = jest.fn();
    render(
      <ThemeI18nProvider>
        <TimeSliderLite
          {...defaultProps}
          onToggleAnimation={onToggleAnimation}
        />
      </ThemeI18nProvider>,
    );
    fireEvent.click(screen.getByTestId('TimeSliderLite-PlayButton'));
    expect(onToggleAnimation).toHaveBeenCalled();
  });

  it('should call onToggleTimeSlider on hide button click', () => {
    const onToggleTimeSlider = jest.fn();
    render(
      <ThemeI18nProvider>
        <TimeSliderLite
          {...defaultProps}
          onToggleTimeSlider={onToggleTimeSlider}
        />
      </ThemeI18nProvider>,
    );
    fireEvent.click(screen.getByTestId('TimeSliderLite-HideButton'));
    expect(onToggleTimeSlider).toHaveBeenCalled();
  });

  it('should call setSelectedTime after clicking step button', async () => {
    const setSelectedTime = jest.fn();
    render(
      <ThemeI18nProvider>
        <TimeSliderLite {...defaultProps} setSelectedTime={setSelectedTime} />
      </ThemeI18nProvider>,
    );
    // After initial render, setSelectedTime is called
    const setSelectedTimeInitialCalls = setSelectedTime.mock.calls.length;
    expect(setSelectedTimeInitialCalls).toBeGreaterThan(0);

    fireEvent.click(screen.getByTestId('TimeSliderLite-StepButtonForward'));
    expect(setSelectedTime.mock.calls.length).toBe(
      setSelectedTimeInitialCalls + 1,
    );

    fireEvent.click(screen.getByTestId('TimeSliderLite-StepButtonBackward'));
    expect(setSelectedTime.mock.calls.length).toBe(
      setSelectedTimeInitialCalls + 2,
    );
  });

  it('should not step below start time (time step 1h)', async () => {
    const setSelectedTime = jest.fn();
    render(
      <ThemeI18nProvider>
        <TimeSliderLite
          {...defaultProps}
          timeStep={[5, 60]}
          startTime={Math.floor(defaultCurrentTime)}
          setSelectedTime={setSelectedTime}
        />
      </ThemeI18nProvider>,
    );

    const setSelectedTimeInitialCalls = setSelectedTime.mock.calls.length;

    fireEvent.click(screen.getByTestId('TimeSliderLite-StepButtonBackward'));

    expect(setSelectedTime.mock.calls.length).toBe(setSelectedTimeInitialCalls);

    fireEvent.click(screen.getByTestId('TimeSliderLite-StepButtonBackward'));

    expect(setSelectedTime.mock.calls.length).toBe(setSelectedTimeInitialCalls);
  });

  it('should not step below start time (time step 12h)', async () => {
    const setSelectedTime = jest.fn();
    const twelveHours = 60 * 12;
    render(
      <ThemeI18nProvider>
        <TimeSliderLite
          {...defaultProps}
          timeStep={[twelveHours, twelveHours]}
          startTime={Math.floor(defaultCurrentTime)}
          setSelectedTime={setSelectedTime}
        />
      </ThemeI18nProvider>,
    );

    const setSelectedTimeInitialCalls = setSelectedTime.mock.calls.length;

    fireEvent.click(screen.getByTestId('TimeSliderLite-StepButtonBackward'));

    expect(setSelectedTime.mock.calls.length).toBe(setSelectedTimeInitialCalls);

    fireEvent.click(screen.getByTestId('TimeSliderLite-StepButtonBackward'));

    expect(setSelectedTime.mock.calls.length).toBe(setSelectedTimeInitialCalls);
  });

  it('should disable forecast if currentTime is at the end', async () => {
    const currentTime = defaultCurrentTime - HOUR_TO_SECOND / 2; // 2023-06-12T22:30:00

    const { baseElement } = render(
      <ThemeI18nProvider>
        <TimeSliderLite
          {...defaultProps}
          timeStep={[5, 15]}
          startTime={Math.floor(defaultCurrentTime - 2 * HOUR_TO_SECOND)}
          currentTime={currentTime}
          endTime={currentTime + HOUR_TO_SECOND / 2} // 2023-06-12T23:00:00
        />
      </ThemeI18nProvider>,
    );

    const forecastBackgroundColor =
      colorsLight.timeSliderLite.timelineBackground;
    const forecastBackgroundColorAlternative =
      colorsLight.timeSliderLite.timelineBackgroundAlternative;

    expect(baseElement.innerHTML).not.toContain(
      `fill="${forecastBackgroundColor.fill}"`,
    );
    expect(baseElement.innerHTML).not.toContain(
      `fill="${forecastBackgroundColorAlternative.fill}"`,
    );

    const forecastDisabledBackgroundColor =
      colorsLight.timeSliderLite.timelineBackgroundDisabled;
    const forecastDisabledBackgroundColorAlternative =
      colorsLight.timeSliderLite.timelineBackgroundDisabledAlternative;

    expect(baseElement.innerHTML).toContain(
      `fill="${forecastDisabledBackgroundColor.fill}"`,
    );
    expect(baseElement.innerHTML).toContain(
      `fill="${forecastDisabledBackgroundColorAlternative.fill}"`,
    );
  });
});
