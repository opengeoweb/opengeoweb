/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { ElementType, ReactNode, useEffect, useState } from 'react';
import { layerTypes } from '@opengeoweb/store';
import { usePrevious } from '@opengeoweb/shared';
import { TimeZoneSwitchProps } from '../TimeSliderLiteOptionsMenu/TimeZoneSwitch/TimeZoneSwitch';

export const DAY_TO_MILLISECOND = 24 * 60 * 60 * 1000;
export const DAY_TO_SECOND = 86400;
export const HOUR_TO_SECOND = 3600;
export const HOUR_TO_MINUTE = 60;
export const MILLISECOND_TO_DAY = 1 / 86400000;
export const MILLISECOND_TO_SECOND = 1 / 1000;
export const MILLISECOND_TO_MINUTE = 1 / 60000;
export const MINUTE_TO_SECOND = 60;
export const MINUTE_TO_MILLISECOND = 60000;
export const SECOND_TO_DAY = 1 / 86400;
export const SECOND_TO_HOUR = 1 / 3600;
export const SECOND_TO_MILLISECOND = 1000;
export const SEVEN_DAYS_IN_SECONDS = 7 * 24 * 60 * 60;
const ONE_DAY_IN_SECONDS = DAY_TO_SECOND;

export interface TimeSliderLiteHandleUpdateOptions {
  currentTime: number;
  setCurrentTime: (newCurrentTime: number) => void;
  timeRange: [number, number] | null;
  setTimeRange: (timeRange: [number, number]) => void;
}

/**
 * Default behaviour for time slider lite to update
 *  - Update current time
 *  - Update time range
 *
 * Layers are updated using the native auto update functionality implemented in ReactMapView.tsx
 *
 * @param mapId
 * @param options
 */
export const defaultTimeSliderLiteUpdate = (
  options: TimeSliderLiteHandleUpdateOptions,
): void => {
  if (document.visibilityState !== 'visible') {
    return;
  }

  const { setCurrentTime, timeRange, currentTime, setTimeRange } = options;

  const newCurrentTime = Math.floor(Date.now() * MILLISECOND_TO_SECOND);
  const delta = newCurrentTime - currentTime;

  // Update current time and trigger update in useDefaultTimeRange hook
  setCurrentTime(currentTime + delta);
  if (timeRange && setTimeRange) {
    setTimeRange([timeRange[0] + delta, timeRange[1] + delta]);
  }
};

/**
 * Connected hook for updating time range and current time every 10 seconds
 * @param updateInterval Update interval in minutes
 * @param timeRange [startTime, endTime] in seconds
 * @param setTimeRange Sets the time range
 * @param options Options
 */
export const useAutoUpdate = (
  timeRange: [number, number] | null,
  setTimeRange: (timeRange: [number, number]) => void,
  options?: {
    defaultUpdateInterval?: number;
    handleUpdate?: (options: TimeSliderLiteHandleUpdateOptions) => void;
  },
): { currentTime: number } => {
  const [currentTime, setCurrentTime] = useState<number>(
    Math.floor(Date.now() * MILLISECOND_TO_SECOND),
  );

  useEffect(() => {
    const intervalCallback = (): void =>
      (options?.handleUpdate ?? defaultTimeSliderLiteUpdate)({
        setCurrentTime,
        currentTime,
        timeRange,
        setTimeRange,
      });

    const interval = setInterval(intervalCallback, 10 * SECOND_TO_MILLISECOND);
    document.addEventListener('visibilitychange', intervalCallback);

    return (): void => {
      clearInterval(interval);
      document.removeEventListener('visibilitychange', intervalCallback);
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    timeRange,
    setTimeRange,
    options?.handleUpdate,
    currentTime,
    setCurrentTime,
  ]);

  return { currentTime };
};

/**
 * Reformats a date that has capitalized first letter and colon separator for time
 * @param dateString Date string from Date.toLocaleDateString
 * @returns Reformatted date string
 */
export const reformatFinnishDateString = (dateString: string): string => {
  const charPositionOfTimestamp = -4;
  return (
    dateString.charAt(0).toUpperCase() +
    dateString.slice(1, charPositionOfTimestamp) +
    dateString.slice(charPositionOfTimestamp).replace('.', ':')
  );
};

/**
 * Marks UTC time with Z after hh:mm or hh.mm or appends UTC to the end of the date string
 * If two matches of time are found, the second one is marked
 */
export const shortUTCTime = (dateString: string): string => {
  const timeRegex = /(\d[:.]\d{2})/g;
  const match = dateString.match(timeRegex);

  if (!match) {
    return `${dateString} UTC`;
  }

  const [first, second] = match;

  if (second) {
    const secondIndex = dateString.lastIndexOf(second);
    return `${dateString.substring(
      0,
      secondIndex,
    )}${second}Z${dateString.substring(secondIndex + second.length)}`;
  }

  return dateString.replace(first, `${first}Z`);
};

export const getMonthChanges = (startDate: Date, endDate: Date): number[] => {
  const result: number[] = [];
  const currentDate = new Date(startDate.getTime());
  const lastDate = new Date(endDate.getTime());

  currentDate.setDate(1);
  currentDate.setHours(0, 0, 0, 0);

  while (currentDate <= lastDate) {
    result.push(currentDate.getTime() * MILLISECOND_TO_SECOND);
    currentDate.setMonth(currentDate.getMonth() + 1);
  }

  return result;
};

export const isWithinRange = (
  time: number,
  target: number,
  range: number,
): boolean => Math.abs(time - target) <= range;

export const getSelectedTimePx = (
  startTime: number,
  endTime: number,
  selectedTime: number,
  width: number,
): number => {
  const startDate = new Date(startTime * SECOND_TO_MILLISECOND);
  const startDateX = startDate.getTime() * MILLISECOND_TO_SECOND;
  const selectedTimeX = selectedTime - startDateX;
  const needleX = Math.floor((selectedTimeX / (endTime - startTime)) * width);
  return needleX;
};

export const boundedValue = (
  value: number,
  bounds: [number, number],
): number => {
  const [min, max] = bounds;
  return Math.max(min, Math.min(max, value));
};

export const getSelectedTimeFromPx = (
  startTime: number,
  endTime: number,
  selectedTimePx: number,
  width: number,
): number => {
  const startDate = new Date(startTime * SECOND_TO_MILLISECOND);
  const startDateX = startDate.getTime() * MILLISECOND_TO_SECOND;
  const selectedTimeX = Math.floor(
    (selectedTimePx / width) * (endTime - startTime),
  );
  const selectedTime = selectedTimeX + startDateX;
  return selectedTime;
};

export const getNearestSelectableTime = (
  timeStep: [number, number],
  startTime: number,
  endTime: number,
  x: number,
  width: number,
  currentTime: number,
  disableForecast?: boolean,
  timeZoneOffsetSeconds?: number,
): number => {
  const observedTimeStepSeconds = timeStep[0];
  const forecastTimeStepSeconds = timeStep[1];

  const hoveredTime = getSelectedTimeFromPx(startTime, endTime, x, width);
  const lastObservedTime = floorLocalSeconds(
    currentTime,
    observedTimeStepSeconds,
    timeZoneOffsetSeconds,
  );
  if (disableForecast && hoveredTime >= lastObservedTime) {
    return lastObservedTime;
  }

  const timeStepSeconds =
    hoveredTime >= currentTime
      ? forecastTimeStepSeconds
      : observedTimeStepSeconds;

  const firstForecastTime =
    floorLocalSeconds(
      currentTime,
      forecastTimeStepSeconds,
      timeZoneOffsetSeconds,
    ) + forecastTimeStepSeconds;

  const isBetweenObservedAndForecast =
    hoveredTime >= lastObservedTime && hoveredTime < firstForecastTime;

  const roundedCurrentTime =
    hoveredTime >= (lastObservedTime + firstForecastTime) / 2
      ? firstForecastTime
      : lastObservedTime;

  const result = isBetweenObservedAndForecast
    ? roundedCurrentTime
    : floorLocalSeconds(
        hoveredTime + timeStepSeconds / 2,
        timeStepSeconds,
        timeZoneOffsetSeconds,
      );
  return result;
};

export const floorLocalMinutes = (
  selectedTimeMinutes: number,
  timeStepMinutes: number,
  timeZoneOffsetMinutes?: number,
): number => {
  const localTimeZoneOffset =
    timeZoneOffsetMinutes ?? new Date().getTimezoneOffset();
  const unFlooredTimeSteps =
    (selectedTimeMinutes - localTimeZoneOffset) / timeStepMinutes;
  return Math.floor(unFlooredTimeSteps) * timeStepMinutes + localTimeZoneOffset;
};

export const floorLocalSeconds = (
  selectedTimeSeconds: number,
  timeStepSeconds: number,
  timeZoneOffsetSeconds?: number,
): number => {
  if (timeZoneOffsetSeconds === 0) {
    return Math.floor(selectedTimeSeconds / timeStepSeconds) * timeStepSeconds;
  }
  const localTimeZoneOffset =
    timeZoneOffsetSeconds ?? new Date().getTimezoneOffset() * 60;
  const unFlooredTimeSteps =
    (selectedTimeSeconds - localTimeZoneOffset) / timeStepSeconds;
  return Math.floor(unFlooredTimeSteps) * timeStepSeconds + localTimeZoneOffset;
};

export const secondsToDaysAndHours = (
  seconds: number,
): { days: number; hours: number } => {
  const days = Math.floor(seconds * SECOND_TO_DAY);
  const hours = Math.round((seconds % DAY_TO_SECOND) * SECOND_TO_HOUR);
  if (hours === 24) {
    return { days: days + 1, hours: 0 };
  }
  return { days, hours };
};

export const hoursAndDaysToSeconds = (hours: number, days: number): number => {
  return hours * HOUR_TO_SECOND + days * DAY_TO_SECOND;
};

export const TIME_SLIDER_LITE_DEFAULT_MAX_FORECAST_RANGE =
  2 * SEVEN_DAYS_IN_SECONDS;
export const TIME_SLIDER_LITE_DEFAULT_MAX_OBSERVED_RANGE =
  SEVEN_DAYS_IN_SECONDS;

/**
 * Checks which layers have observed/forecasted data and returns a default time range
 * @param layers layerTypes.ReduxLayer[] Map layers
 * @param currentTime Current time in seconds
 * @param overrideDefaultTimeRange [observedTimeRange, forecastTime] in seconds from current time
 * @returns [startTime, endTime] in seconds
 */
export const defaultTimeRange = (
  layers: layerTypes.ReduxLayer[],
  currentTime = Math.floor(Date.now() * MILLISECOND_TO_SECOND),
  overrideDefaultTimeRange?: [number, number],
): [number, number] => {
  const defaultBounds: [number, number] = [
    currentTime - TIME_SLIDER_LITE_DEFAULT_MAX_OBSERVED_RANGE,
    currentTime + TIME_SLIDER_LITE_DEFAULT_MAX_FORECAST_RANGE,
  ];
  const bounds: [number, number] = overrideDefaultTimeRange
    ? [
        currentTime - overrideDefaultTimeRange[0],
        currentTime + overrideDefaultTimeRange[1],
      ]
    : defaultBounds;

  const timeDimensions: { minValue: string; maxValue: string }[] = [];
  layers.forEach((layer) => {
    const timeDimension = layer.dimensions?.find(
      (dimension) => dimension.name === 'time',
    );
    if (timeDimension?.minValue && timeDimension?.maxValue) {
      timeDimensions.push(
        timeDimension as { minValue: string; maxValue: string },
      );
    }
  });

  if (timeDimensions.length === 0) {
    return bounds;
  }

  const minMinValue = Math.min(
    ...timeDimensions.map((dimension) => {
      return new Date(dimension.minValue).valueOf() * MILLISECOND_TO_SECOND;
    }),
  );
  const maxMaxValue = Math.max(
    ...timeDimensions.map((dimension) => {
      return new Date(dimension.maxValue).valueOf() * MILLISECOND_TO_SECOND;
    }),
  );

  return [Math.max(bounds[0], minMinValue), Math.min(bounds[1], maxMaxValue)];
};

export const useDefaultTimeRange = (
  layers: layerTypes.ReduxLayer[],
  updateThreshold: number,
  currentTime = Math.floor(Date.now() * MILLISECOND_TO_SECOND),
): [number, number] => {
  const [[defaultStartTime, defaultEndTime], setDefaultTimeRange] = useState<
    [number, number]
  >([0, 0]);

  const updateThresholdInMinutes = updateThreshold * MINUTE_TO_SECOND;

  const updateDefaultTimeRange = (): void => {
    const newTimeRange = defaultTimeRange(layers, currentTime);
    const defaultStartTimeChanged =
      Math.abs(newTimeRange[0] - defaultStartTime) > updateThresholdInMinutes;
    const defaultEndTimeChanged =
      Math.abs(newTimeRange[1] - defaultEndTime) > updateThresholdInMinutes;
    if (defaultStartTimeChanged || defaultEndTimeChanged) {
      setDefaultTimeRange(newTimeRange);
    }
  };

  useEffect(() => {
    updateDefaultTimeRange();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [layers, currentTime]);

  return [defaultStartTime, defaultEndTime];
};

interface TimeList {
  name: string;
  value: string;
}

export const generateCustomTimeList = (
  currentTime: number,
  startTime: number,
  endTime: number,
  timeStep: [number, number],
  timeZoneOffsetSeconds?: number,
): TimeList[] => {
  const localTimeZoneOffset =
    timeZoneOffsetSeconds !== undefined
      ? timeZoneOffsetSeconds
      : new Date().getTimezoneOffset() * MINUTE_TO_SECOND;
  const observedTimeStep = timeStep[0] * MINUTE_TO_SECOND;
  const forecastTimeStep = timeStep[1] * MINUTE_TO_SECOND;

  let firstObservedTimeStep = floorLocalSeconds(
    startTime,
    observedTimeStep,
    localTimeZoneOffset,
  );
  if (firstObservedTimeStep < startTime) {
    firstObservedTimeStep += observedTimeStep;
  }
  const lastObservedTimeStep = floorLocalSeconds(
    currentTime,
    observedTimeStep,
    localTimeZoneOffset,
  );
  const truncatePassingHour = currentTime - startTime < 0;
  const firstForecastTimeStep = truncatePassingHour
    ? floorLocalSeconds(
        currentTime + HOUR_TO_SECOND,
        HOUR_TO_SECOND,
        localTimeZoneOffset,
      ) - 1
    : floorLocalSeconds(currentTime, forecastTimeStep, localTimeZoneOffset);
  const lastForecastTimeStep = floorLocalSeconds(
    endTime,
    forecastTimeStep,
    localTimeZoneOffset,
  );

  const observedTimeList: TimeList[] = [];
  const forecastTimeList: TimeList[] = [];

  let time = firstObservedTimeStep;
  while (time <= lastObservedTimeStep) {
    const observedTimeListItem = {
      name: 'time',
      value: new Date(time * SECOND_TO_MILLISECOND).toISOString(),
    };
    observedTimeList.push(observedTimeListItem);
    time += observedTimeStep;
  }

  time = lastForecastTimeStep;
  while (time > firstForecastTimeStep) {
    const forecastTimeListItem = {
      name: 'time',
      value: new Date(time * SECOND_TO_MILLISECOND).toISOString(),
    };
    forecastTimeList.push(forecastTimeListItem);
    time -= forecastTimeStep;
  }
  forecastTimeList.reverse();

  return [...observedTimeList, ...forecastTimeList];
};

enum TickMode {
  DAY = 1,
  HALF_A_DAY = 2,
  SIX_HOURS = 4,
  THREE_HOURS = 8,
  ONE_HOUR = 24,
  HALF_AN_HOUR = 48,
  FIFTEEN_MINUTES = 96,
}

export const subTicksPerDay = (dayWidthInPixels: number): number => {
  if (dayWidthInPixels < 40) {
    return TickMode.DAY;
  }
  if (dayWidthInPixels < 75) {
    return TickMode.HALF_A_DAY;
  }
  if (dayWidthInPixels < 400) {
    return TickMode.SIX_HOURS;
  }
  if (dayWidthInPixels < 800) {
    return TickMode.THREE_HOURS;
  }
  if (dayWidthInPixels < 1600) {
    return TickMode.ONE_HOUR;
  }
  if (dayWidthInPixels < 3200) {
    return TickMode.HALF_AN_HOUR;
  }
  return TickMode.FIFTEEN_MINUTES;
};

export const maxSubTicksPerDay = (
  dayWidthInPixels: number,
  timeStep: number,
): number =>
  Math.min(
    subTicksPerDay(dayWidthInPixels),
    Math.floor(ONE_DAY_IN_SECONDS / timeStep),
  );

export const getRemainingHours = (
  maxHours: number | undefined,
  days: number,
): number => {
  const hourDifference = maxHours && maxHours - days * 24;
  const result =
    maxHours && hourDifference && hourDifference > 0 ? hourDifference : 0;
  return result;
};

export const secondsSinceLocalMidnight = (
  currentTimeInSeconds: number,
): number => {
  return (
    new Date(currentTimeInSeconds * 1000).getHours() * HOUR_TO_SECOND +
    new Date(currentTimeInSeconds * 1000).getMinutes() * MINUTE_TO_SECOND +
    new Date(currentTimeInSeconds * 1000).getSeconds()
  );
};

export const getTimeScaleDescription = (timeScale: number): string => {
  if (timeScale !== 1 && timeScale < HOUR_TO_SECOND) {
    return `${Math.round(timeScale / MINUTE_TO_SECOND)} minutes is ~1 second`;
  }
  if (timeScale > 60) {
    const hours = Math.floor(timeScale / HOUR_TO_SECOND);
    const mins = Math.round(
      (timeScale - hours * HOUR_TO_SECOND) / MINUTE_TO_SECOND,
    );
    return `${hours} h ${mins} min is ~1 second`;
  }
  return 'Real-time';
};

export interface TimeSliderLiteCustomSettings {
  menuButton?: {
    icon?: ReactNode;
  };
  playButton?: {
    icon?: {
      play?: ReactNode;
      pause?: ReactNode;
    };
  };
  stepButton?: {
    label?: {
      forward?: string;
      backward?: string;
    };
    icon?: {
      forward?: ReactNode;
      backward?: ReactNode;
    };
  };
  TimeZoneSwitch?: React.FC<TimeZoneSwitchProps>;
  hideButton?: {
    icon?: {
      hide?: ReactNode;
      show?: ReactNode;
    };
  };
  closeButton?: {
    icon?: ReactNode;
  };
  dropdownButton?: {
    icon?: ElementType;
  };
}

/**
 * Keeps selected time in sync with default time
 * @returns True if selected time sync to default time is enabled
 */
export const useAutoUpdateSelectedTime = (
  defaultTime: number,
  selectedTime: number,
  setSelectedTime: (newCurrentTime: number) => void,
): boolean => {
  const defaultTimeBeforeChange = usePrevious(defaultTime);
  const [previousDefaultTime, setPreviousDefaultTime] =
    useState<number>(defaultTime);

  useEffect(() => {
    if (defaultTime !== defaultTimeBeforeChange) {
      setPreviousDefaultTime(defaultTimeBeforeChange);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [defaultTime]);

  useEffect(() => {
    if (previousDefaultTime === selectedTime) {
      setSelectedTime(defaultTime);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [previousDefaultTime]);

  return defaultTime === selectedTime;
};
