/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { act, renderHook } from '@testing-library/react';
import { layerTypes } from '@opengeoweb/store';
import {
  HOUR_TO_MINUTE,
  HOUR_TO_SECOND,
  MILLISECOND_TO_MINUTE,
  MILLISECOND_TO_SECOND,
  MINUTE_TO_SECOND,
  SECOND_TO_MILLISECOND,
  TIME_SLIDER_LITE_DEFAULT_MAX_FORECAST_RANGE,
  TIME_SLIDER_LITE_DEFAULT_MAX_OBSERVED_RANGE,
  defaultTimeRange,
  floorLocalMinutes,
  floorLocalSeconds,
  getNearestSelectableTime,
  generateCustomTimeList,
  getSelectedTimeFromPx,
  getSelectedTimePx,
  hoursAndDaysToSeconds,
  secondsToDaysAndHours,
  useDefaultTimeRange,
  shortUTCTime,
  useAutoUpdate,
  MINUTE_TO_MILLISECOND,
  TimeSliderLiteHandleUpdateOptions,
  useAutoUpdateSelectedTime,
} from './timeSliderLiteUtils';

jest.mock('@opengeoweb/webmap-react', () => ({
  ...jest.requireActual('@opengeoweb/webmap-react'),
  parseWMJSLayer: (): string => {
    return 'parseWMJSLayer return value';
  },
  getLayerUpdateInfo: (): string => {
    return 'getLayerUpdateInfo return value';
  },
}));

describe('components/TimeSliderLite/timeSliderLiteUtils', () => {
  jest.useFakeTimers();

  describe('useAutoUpdate', () => {
    it('updates current time if default time range is used', () => {
      const updateIntervalMinutes = 1;
      const timeRange = null;

      const setTimeRange = jest.fn();
      const { result } = renderHook(() =>
        useAutoUpdate(timeRange, setTimeRange, {}),
      );
      const currentTimeBefore: number = result.current.currentTime;

      act(() => {
        jest.advanceTimersByTime(updateIntervalMinutes * MINUTE_TO_MILLISECOND);
      });
      const currentTimeAfter: number = result.current.currentTime;

      expect(setTimeRange).not.toHaveBeenCalled();
      expect(currentTimeAfter).toBeGreaterThan(currentTimeBefore);
    });

    it('updates current time and time range after the specified update interval', () => {
      const updateIntervalMinutes = 10;
      const timeRange = [0, 1000] as [number, number];

      const setTimeRange = jest.fn();
      const { result } = renderHook(() =>
        useAutoUpdate(timeRange, setTimeRange, {}),
      );
      const currentTimeBefore: number = result.current.currentTime;

      act(() => {
        jest.advanceTimersByTime(updateIntervalMinutes * MINUTE_TO_MILLISECOND);
      });
      const currentTimeAfter: number = result.current.currentTime;

      expect(setTimeRange).toHaveBeenCalled();
      expect(currentTimeAfter).toBeGreaterThan(currentTimeBefore);
    });

    it('should override default behaviour with custom update function', () => {
      const updateIntervalMinutes = 1;
      const setTimeRange = jest.fn();
      const timeRange = null;
      const customUpdateFunction = (
        options: TimeSliderLiteHandleUpdateOptions,
      ): void => {
        const { setCurrentTime, setTimeRange } = options;
        setCurrentTime(-3);
        setTimeRange([-1, 1]);
      };

      const { result } = renderHook(() =>
        useAutoUpdate(timeRange, setTimeRange, {
          handleUpdate: customUpdateFunction,
        }),
      );

      act(() => {
        jest.advanceTimersByTime(updateIntervalMinutes * MINUTE_TO_MILLISECOND);
      });

      // Should update values according to custom update function
      const { currentTime } = result.current;
      expect(currentTime).toBe(-3);

      expect(setTimeRange).toHaveBeenCalled();
      const setTimeRangeArgs = setTimeRange.mock.calls[0];
      expect(setTimeRangeArgs[0]).toEqual([-1, 1]);
    });
  });

  describe('getSelectedTimePx', () => {
    it('should return half of width when selected time is in the middle of time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const selectedTime = (startTime + endTime) / 2;
      const width = 1000;
      const result = getSelectedTimePx(startTime, endTime, selectedTime, width);
      expect(result).toEqual(width / 2);
    });

    it('should return 0 for a selected time at the beginning of the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const selectedTime = 1618366800;
      const width = 1000;
      const result = getSelectedTimePx(startTime, endTime, selectedTime, width);
      expect(result).toEqual(0);
    });

    it('should return full width for a selected time at the end of the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const selectedTime = 1618453200;
      const width = 1000;
      const result = getSelectedTimePx(startTime, endTime, selectedTime, width);
      expect(result).toEqual(width);
    });

    it('should return the correct value for a selected time after of the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const timeRange = endTime - startTime;
      const selectedTime = endTime + timeRange;
      const width = 1000;
      const result = getSelectedTimePx(startTime, endTime, selectedTime, width);
      expect(result).toEqual(width * 2);
    });

    it('should return the correct value for a selected time before of the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const timeRange = endTime - startTime;
      const selectedTime = startTime - timeRange;
      const width = 1000;
      const result = getSelectedTimePx(startTime, endTime, selectedTime, width);
      expect(result).toEqual(-width);
    });
  });

  describe('getSelectedTimeFromPx', () => {
    it('should return middle of start and end time for a selected position in the middle of the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const width = 1000;
      const selectedTimePx = width / 2;
      const result = getSelectedTimeFromPx(
        startTime,
        endTime,
        selectedTimePx,
        width,
      );
      expect(result).toEqual((startTime + endTime) / 2);
    });

    it('should return start time for a selected position at the beginning of the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const width = 1000;
      const selectedTimePx = 0;
      const result = getSelectedTimeFromPx(
        startTime,
        endTime,
        selectedTimePx,
        width,
      );
      expect(result).toEqual(startTime);
    });

    it('should return the end time for a selected position at the end of the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const width = 1000;
      const selectedTimePx = width;
      const result = getSelectedTimeFromPx(
        startTime,
        endTime,
        selectedTimePx,
        width,
      );
      expect(result).toEqual(endTime);
    });

    it('should return the time after end time for a selected position after the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const timeRange = endTime - startTime;
      const width = 1000;
      const selectedTimePx = width * 2;
      const result = getSelectedTimeFromPx(
        startTime,
        endTime,
        selectedTimePx,
        width,
      );
      expect(result).toEqual(endTime + timeRange);
    });

    it('should return the correct value for a selected time before of the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const timeRange = endTime - startTime;
      const width = 1000;
      const selectedTimePx = -width;
      const result = getSelectedTimeFromPx(
        startTime,
        endTime,
        selectedTimePx,
        width,
      );
      expect(result).toEqual(startTime - timeRange);
    });
  });

  describe('getNearestSelectableTime', () => {
    const observedTimeStep = 30;
    const forecastTimeStep = 60;
    const timeStep = [observedTimeStep, forecastTimeStep] as [number, number];
    const startTime = 1500;
    const endTime = 3000;
    const width = 300;
    const currentTime = 2000;

    it('should value rounded by observed time step', () => {
      const expected = 1590;
      const result = getNearestSelectableTime(
        timeStep,
        startTime,
        endTime,
        20,
        width,
        currentTime,
      );

      // Shoud round by observed time step
      expect(result % observedTimeStep).toBe(0);
      expect(result % forecastTimeStep).not.toBe(0);

      expect(result).toEqual(expected);
    });

    it('should value rounded by forecast time step', () => {
      const expected = 2280;
      const result = getNearestSelectableTime(
        [180, forecastTimeStep],
        startTime,
        endTime,
        160,
        width,
        currentTime,
      );

      // Shoud round by forecast time step
      expect(result % 180).not.toBe(0);
      expect(result % forecastTimeStep).toBe(0);

      expect(result).toEqual(expected);
    });

    it('should round current time to the nearest time step', () => {
      const lastObservedTime = floorLocalSeconds(currentTime, observedTimeStep);
      const firstForecastTime =
        floorLocalSeconds(currentTime, forecastTimeStep) + forecastTimeStep;

      const currentTimeBorder = (lastObservedTime + firstForecastTime) / 2;
      const currentTimeBorderX = getSelectedTimePx(
        startTime,
        endTime,
        currentTimeBorder,
        width,
      );

      const lastObservedResult = getNearestSelectableTime(
        timeStep,
        startTime,
        endTime,
        currentTimeBorderX - 1,
        width,
        currentTime,
      );
      const firstForecastResult = getNearestSelectableTime(
        timeStep,
        startTime,
        endTime,
        currentTimeBorderX + 1,
        width,
        currentTime,
      );

      // currentTimeBorderX should be surrounded by lastObservedTime and firstForecastTime
      expect(lastObservedResult).toEqual(lastObservedTime);
      expect(firstForecastResult).toEqual(firstForecastTime);
    });
  });

  describe('floorLocalMinutes', () => {
    const easternEuropeanSummerTime = -3 * HOUR_TO_MINUTE;
    const centralEuropeanSummerTime = -2 * HOUR_TO_MINUTE;
    const westernEuropeanSummerTime = -1 * HOUR_TO_MINUTE;

    it('should floor to the nearest 15 minutes independent from timezone', () => {
      const selectedTime =
        new Date('2021-04-14T02:24:40Z').valueOf() * MILLISECOND_TO_MINUTE;
      const timeStep = 15;
      const expected =
        new Date('2021-04-14T02:15:00Z').valueOf() * MILLISECOND_TO_MINUTE;

      const resultInEasternEurope = floorLocalMinutes(
        selectedTime,
        timeStep,
        easternEuropeanSummerTime,
      );
      expect(resultInEasternEurope).toEqual(expected);

      const resultInCentralEurope = floorLocalMinutes(
        selectedTime,
        timeStep,
        centralEuropeanSummerTime,
      );
      expect(resultInCentralEurope).toEqual(expected);

      const resultInWesternEurope = floorLocalMinutes(
        selectedTime,
        timeStep,
        westernEuropeanSummerTime,
      );
      expect(resultInWesternEurope).toEqual(expected);
    });

    it('should floor to the nearest one hour independent from timezone', () => {
      const selectedTime =
        new Date('2021-04-14T02:24:40Z').valueOf() * MILLISECOND_TO_MINUTE;
      const timeStep = 60;
      const expected =
        new Date('2021-04-14T02:00:00Z').valueOf() * MILLISECOND_TO_MINUTE;

      const resultInEasternEurope = floorLocalMinutes(
        selectedTime,
        timeStep,
        easternEuropeanSummerTime,
      );
      expect(resultInEasternEurope).toEqual(expected);

      const resultInCentralEurope = floorLocalMinutes(
        selectedTime,
        timeStep,
        centralEuropeanSummerTime,
      );
      expect(resultInCentralEurope).toEqual(expected);

      const resultInWesternEurope = floorLocalMinutes(
        selectedTime,
        timeStep,
        westernEuropeanSummerTime,
      );
      expect(resultInWesternEurope).toEqual(expected);
    });

    it('should floor to the nearest 3 hours depending on timezone', () => {
      const selectedTime =
        new Date('2021-04-14T02:24:40Z').valueOf() * MILLISECOND_TO_MINUTE;
      const timeStep = 180;

      const expectedInEasternEurope =
        new Date('2021-04-14T00:00:00Z').valueOf() * MILLISECOND_TO_MINUTE;
      const resultInEasternEurope = floorLocalMinutes(
        selectedTime,
        timeStep,
        easternEuropeanSummerTime,
      );
      expect(resultInEasternEurope).toEqual(expectedInEasternEurope);

      const expectedInCentralEurope =
        new Date('2021-04-14T01:00:00Z').valueOf() * MILLISECOND_TO_MINUTE;
      const resultInCentralEurope = floorLocalMinutes(
        selectedTime,
        timeStep,
        centralEuropeanSummerTime,
      );
      expect(resultInCentralEurope).toEqual(expectedInCentralEurope);

      const expectedInWesternEurope =
        new Date('2021-04-14T02:00:00Z').valueOf() * MILLISECOND_TO_MINUTE;
      const resultInWesternEurope = floorLocalMinutes(
        selectedTime,
        timeStep,
        westernEuropeanSummerTime,
      );
      expect(resultInWesternEurope).toEqual(expectedInWesternEurope);
    });

    it('should floor to the nearest 6 hours depending on timezone', () => {
      const selectedTime =
        new Date('2021-04-14T02:24:40Z').valueOf() * MILLISECOND_TO_MINUTE;
      const timeStep = 360;

      const expectedInEasternEurope =
        new Date('2021-04-13T21:00:00Z').valueOf() * MILLISECOND_TO_MINUTE;
      const resultInEasternEurope = floorLocalMinutes(
        selectedTime,
        timeStep,
        easternEuropeanSummerTime,
      );
      expect(resultInEasternEurope).toEqual(expectedInEasternEurope);

      const expectedInCentralEurope =
        new Date('2021-04-13T22:00:00Z').valueOf() * MILLISECOND_TO_MINUTE;
      const resultInCentralEurope = floorLocalMinutes(
        selectedTime,
        timeStep,
        centralEuropeanSummerTime,
      );
      expect(resultInCentralEurope).toEqual(expectedInCentralEurope);

      const expectedInWesternEurope =
        new Date('2021-04-13T23:00:00Z').valueOf() * MILLISECOND_TO_MINUTE;
      const resultInWesternEurope = floorLocalMinutes(
        selectedTime,
        timeStep,
        westernEuropeanSummerTime,
      );
      expect(resultInWesternEurope).toEqual(expectedInWesternEurope);
    });
  });

  describe('floorLocalSeconds', () => {
    const easternEuropeanSummerTime = -3 * HOUR_TO_SECOND;
    const centralEuropeanSummerTime = -2 * HOUR_TO_SECOND;
    const westernEuropeanSummerTime = -1 * HOUR_TO_SECOND;

    it('should floor to the nearest 15 minutes independent from timezone', () => {
      const selectedTime =
        new Date('2021-04-14T02:24:40Z').valueOf() * MILLISECOND_TO_SECOND;

      const timeStep = 15 * MINUTE_TO_SECOND;
      const expected =
        new Date('2021-04-14T02:15:00Z').valueOf() * MILLISECOND_TO_SECOND;

      const resultInEasternEurope = floorLocalSeconds(
        selectedTime,
        timeStep,
        easternEuropeanSummerTime,
      );
      expect(resultInEasternEurope).toEqual(expected);

      const resultInCentralEurope = floorLocalSeconds(
        selectedTime,
        timeStep,
        centralEuropeanSummerTime,
      );
      expect(resultInCentralEurope).toEqual(expected);

      const resultInWesternEurope = floorLocalSeconds(
        selectedTime,
        timeStep,
        westernEuropeanSummerTime,
      );
      expect(resultInWesternEurope).toEqual(expected);
    });

    it('should floor to the nearest 30 minutes independent from timezone', () => {
      const selectedTime =
        new Date('2021-04-14T02:24:40Z').valueOf() * MILLISECOND_TO_SECOND;

      const timeStep = 30 * MINUTE_TO_SECOND;
      const expected =
        new Date('2021-04-14T02:00:00Z').valueOf() * MILLISECOND_TO_SECOND;

      const resultInEasternEurope = floorLocalSeconds(
        selectedTime,
        timeStep,
        easternEuropeanSummerTime,
      );
      expect(resultInEasternEurope).toEqual(expected);

      const resultInCentralEurope = floorLocalSeconds(
        selectedTime,
        timeStep,
        centralEuropeanSummerTime,
      );
      expect(resultInCentralEurope).toEqual(expected);

      const resultInWesternEurope = floorLocalSeconds(
        selectedTime,
        timeStep,
        westernEuropeanSummerTime,
      );
      expect(resultInWesternEurope).toEqual(expected);
    });

    it('should floor to the nearest 3 hours depending on timezone', () => {
      const selectedTime =
        new Date('2021-04-14T02:24:40Z').valueOf() * MILLISECOND_TO_SECOND;

      const timeStep = 180 * MINUTE_TO_SECOND;

      const expectedInEasternEurope =
        new Date('2021-04-14T00:00:00Z').valueOf() * MILLISECOND_TO_SECOND;
      const resultInEasternEurope = floorLocalMinutes(
        selectedTime,
        timeStep,
        easternEuropeanSummerTime,
      );
      expect(resultInEasternEurope).toEqual(expectedInEasternEurope);

      const expectedInCentralEurope =
        new Date('2021-04-14T01:00:00Z').valueOf() * MILLISECOND_TO_SECOND;
      const resultInCentralEurope = floorLocalMinutes(
        selectedTime,
        timeStep,
        centralEuropeanSummerTime,
      );
      expect(resultInCentralEurope).toEqual(expectedInCentralEurope);

      const expectedInWesternEurope =
        new Date('2021-04-14T02:00:00Z').valueOf() * MILLISECOND_TO_SECOND;
      const resultInWesternEurope = floorLocalMinutes(
        selectedTime,
        timeStep,
        westernEuropeanSummerTime,
      );
      expect(resultInWesternEurope).toEqual(expectedInWesternEurope);
    });
  });

  const hour = 3600;
  const day = 86400;

  describe('secondsToDaysAndHours', () => {
    it('should convert seconds to days and hours', () => {
      expect(secondsToDaysAndHours(hour)).toEqual({ days: 0, hours: 1 });
      expect(secondsToDaysAndHours(day)).toEqual({ days: 1, hours: 0 });
      expect(secondsToDaysAndHours(day + 3 * hour)).toEqual({
        days: 1,
        hours: 3,
      });
      expect(secondsToDaysAndHours(4 * day + 4 * hour)).toEqual({
        days: 4,
        hours: 4,
      });
    });

    const minute = 60;

    it('should round to closest hour', () => {
      expect(secondsToDaysAndHours(hour - minute)).toEqual({
        days: 0,
        hours: 1,
      });
      expect(secondsToDaysAndHours(hour + minute)).toEqual({
        days: 0,
        hours: 1,
      });
      expect(secondsToDaysAndHours(day + 3 * hour - minute)).toEqual({
        days: 1,
        hours: 3,
      });
      expect(secondsToDaysAndHours(day + minute)).toEqual({
        days: 1,
        hours: 0,
      });
      expect(secondsToDaysAndHours(day + 3 * hour - minute)).toEqual({
        days: 1,
        hours: 3,
      });
      expect(secondsToDaysAndHours(4 * day + 4 * hour - minute)).toEqual({
        days: 4,
        hours: 4,
      });
    });

    it('should round to closest day', () => {
      expect(secondsToDaysAndHours(day - 29 * minute)).toEqual({
        days: 1,
        hours: 0,
      });
      expect(secondsToDaysAndHours(day + 29 * minute)).toEqual({
        days: 1,
        hours: 0,
      });
      expect(secondsToDaysAndHours(3 * day - 29 * minute)).toEqual({
        days: 3,
        hours: 0,
      });
      expect(secondsToDaysAndHours(5 * day + 29 * minute)).toEqual({
        days: 5,
        hours: 0,
      });
    });
  });

  describe('hoursAndDaysToSeconds', () => {
    it('should convert hours and days to seconds', () => {
      expect(hoursAndDaysToSeconds(0, 1)).toBe(day);
      expect(hoursAndDaysToSeconds(2, 3)).toBe(2 * hour + 3 * day);
      expect(hoursAndDaysToSeconds(5, 0)).toBe(5 * hour);
      expect(hoursAndDaysToSeconds(12, 7)).toBe(12 * hour + 7 * day);
    });
  });

  describe('defaultTimeRange', () => {
    it('should return max time range if no layers given', () => {
      const currentTime = Math.floor(Date.now() * MILLISECOND_TO_SECOND);
      const result = defaultTimeRange([]);
      expect(result).toEqual([
        currentTime - TIME_SLIDER_LITE_DEFAULT_MAX_OBSERVED_RANGE,
        currentTime + TIME_SLIDER_LITE_DEFAULT_MAX_FORECAST_RANGE,
      ]);
    });

    it('should accept injecting current time', () => {
      const currentTime = Math.floor(
        new Date('Wed May 24 2023').valueOf() * MILLISECOND_TO_SECOND,
      );
      const result = defaultTimeRange([], currentTime);
      expect(result).toEqual([
        currentTime - TIME_SLIDER_LITE_DEFAULT_MAX_OBSERVED_RANGE,
        currentTime + TIME_SLIDER_LITE_DEFAULT_MAX_FORECAST_RANGE,
      ]);
    });

    it('should accept custom bounds', () => {
      const currentTime = Math.floor(
        new Date('Wed May 24 2023').valueOf() * MILLISECOND_TO_SECOND,
      );
      const customBounds: [number, number] = [1000, 2000];
      const result = defaultTimeRange([], currentTime, customBounds);
      expect(result).toEqual([
        currentTime - customBounds[0],
        currentTime + customBounds[1],
      ]);
    });

    it('should return range with forecast and observed union', () => {
      const currentTime = Math.floor(
        new Date('Wed May 24 2023').valueOf() * MILLISECOND_TO_SECOND,
      );
      const currentTimeISO = new Date(
        currentTime * SECOND_TO_MILLISECOND,
      ).toISOString();

      const threeDaysBefore =
        new Date('Wed May 21 2023').valueOf() * MILLISECOND_TO_SECOND;
      const threeDaysBeforeISO = new Date('Wed May 21 2023').toISOString();
      const mockLayerWithThreeObservedDays: layerTypes.ReduxLayer = {
        dimensions: [
          {
            name: 'time',
            currentValue: currentTimeISO,
            minValue: threeDaysBeforeISO,
            maxValue: currentTimeISO,
          },
        ],
      };

      const oneDayAfter =
        new Date('Wed May 25 2023').valueOf() * MILLISECOND_TO_SECOND;
      const oneDayAfterISO = new Date('Wed May 25 2023').toISOString();
      const mockLayerWithOneForecastDay: layerTypes.ReduxLayer = {
        dimensions: [
          {
            name: 'time',
            currentValue: currentTimeISO,
            minValue: currentTimeISO,
            maxValue: oneDayAfterISO,
          },
        ],
      };

      const mockLayers = [
        mockLayerWithThreeObservedDays,
        mockLayerWithOneForecastDay,
      ];

      const observedAndForecastUnion = [threeDaysBefore, oneDayAfter];
      const bounds = defaultTimeRange(mockLayers, currentTime);
      expect(bounds).toEqual(observedAndForecastUnion);
    });
  });

  describe('useDefaultTimeRange', () => {
    const secondsNow = Math.floor(
      new Date('2022-01-02T12:00:00Z').valueOf() * MILLISECOND_TO_SECOND,
    );
    const defaultTimeRange = [
      secondsNow - TIME_SLIDER_LITE_DEFAULT_MAX_OBSERVED_RANGE,
      secondsNow + TIME_SLIDER_LITE_DEFAULT_MAX_FORECAST_RANGE,
    ];

    const observedLayer = {
      name: 'observed layer',
      dimensions: [
        {
          name: 'time',
          minValue: '2022-01-01T00:00:00Z',
          maxValue: '2022-01-02T00:00:00Z',
        },
      ],
    };

    const forecastLayer = {
      dimensions: [
        {
          name: 'time',
          minValue: '2022-01-03T00:00:00Z',
          maxValue: '2022-01-04T00:00:00Z',
        },
      ],
    };
    const updateThreshold = 5;

    it('should update time range if observed layer is added', () => {
      let layers: layerTypes.ReduxLayer[] = [];

      const { result, rerender } = renderHook(() =>
        useDefaultTimeRange(layers, updateThreshold, secondsNow),
      );

      expect(result.current).toEqual(defaultTimeRange);

      layers = [observedLayer as layerTypes.ReduxLayer];
      rerender();

      const observedMinValue =
        new Date(observedLayer.dimensions[0].minValue).valueOf() *
        MILLISECOND_TO_SECOND;
      const observedMaxValue =
        new Date(observedLayer.dimensions[0].maxValue).valueOf() *
        MILLISECOND_TO_SECOND;
      expect(result.current).toEqual([observedMinValue, observedMaxValue]);
    });

    it('should update time range if observed layer is added', () => {
      let layers: layerTypes.ReduxLayer[] = [];

      const { result, rerender } = renderHook(() =>
        useDefaultTimeRange(layers, updateThreshold, secondsNow),
      );

      expect(result.current).toEqual(defaultTimeRange);

      layers = [forecastLayer as layerTypes.ReduxLayer];
      rerender();

      const forecastMinValue =
        new Date(forecastLayer.dimensions[0].minValue).valueOf() *
        MILLISECOND_TO_SECOND;
      const forecastMaxValue =
        new Date(forecastLayer.dimensions[0].maxValue).valueOf() *
        MILLISECOND_TO_SECOND;
      expect(result.current).toEqual([forecastMinValue, forecastMaxValue]);
    });

    it('should update time range if observed and forecast layer is added', () => {
      let layers: layerTypes.ReduxLayer[] = [];

      const { result, rerender } = renderHook(() =>
        useDefaultTimeRange(layers, updateThreshold, secondsNow),
      );

      expect(result.current).toEqual(defaultTimeRange);

      layers = [observedLayer, forecastLayer] as layerTypes.ReduxLayer[];
      rerender();

      const observedMinValue =
        new Date(observedLayer.dimensions[0].minValue).valueOf() *
        MILLISECOND_TO_SECOND;
      const forecastMaxValue =
        new Date(forecastLayer.dimensions[0].maxValue).valueOf() *
        MILLISECOND_TO_SECOND;
      expect(result.current).toEqual([observedMinValue, forecastMaxValue]);
    });
  });

  describe('generateCustomTimeList', () => {
    it('should generate a list of times with the correct time step', () => {
      const currentTime =
        new Date('2021-04-14T00:00:00.000Z').valueOf() * MILLISECOND_TO_SECOND;
      const startTime =
        new Date('2021-04-13T22:00:00.000Z').valueOf() * MILLISECOND_TO_SECOND;
      const endTime =
        new Date('2021-04-14T06:00:00.000Z').valueOf() * MILLISECOND_TO_SECOND;
      const timeStep: [number, number] = [30, 60];
      const expectedTimeList = [
        { name: 'time', value: '2021-04-13T22:00:00.000Z' },
        { name: 'time', value: '2021-04-13T22:30:00.000Z' },
        { name: 'time', value: '2021-04-13T23:00:00.000Z' },
        { name: 'time', value: '2021-04-13T23:30:00.000Z' },
        { name: 'time', value: '2021-04-14T00:00:00.000Z' }, // current time
        { name: 'time', value: '2021-04-14T01:00:00.000Z' },
        { name: 'time', value: '2021-04-14T02:00:00.000Z' },
        { name: 'time', value: '2021-04-14T03:00:00.000Z' },
        { name: 'time', value: '2021-04-14T04:00:00.000Z' },
        { name: 'time', value: '2021-04-14T05:00:00.000Z' },
        { name: 'time', value: '2021-04-14T06:00:00.000Z' },
      ];
      const result = generateCustomTimeList(
        currentTime,
        startTime,
        endTime,
        timeStep,
      );
      expect(result).toEqual(expectedTimeList);
    });

    it('should floor the list of times to the nearest local six and three hours', () => {
      const easternEuropeanSummerTime = -3 * HOUR_TO_SECOND;
      const centralEuropeanSummerTime = -2 * HOUR_TO_SECOND;
      const westernEuropeanSummerTime = -1 * HOUR_TO_SECOND;

      const startTime =
        new Date('2021-04-14T00:00:00.000Z').valueOf() * MILLISECOND_TO_SECOND;
      const currentTime =
        new Date('2021-04-15T00:00:00.000Z').valueOf() * MILLISECOND_TO_SECOND;
      const endTime =
        new Date('2021-04-15T09:00:00.000Z').valueOf() * MILLISECOND_TO_SECOND;
      const timeStep: [number, number] = [6 * 60, 3 * 60];

      const expectedTimeListForEasterEuropeanSummerTime = [
        { name: 'time', value: '2021-04-14T03:00:00.000Z' },
        { name: 'time', value: '2021-04-14T09:00:00.000Z' },
        { name: 'time', value: '2021-04-14T15:00:00.000Z' },
        { name: 'time', value: '2021-04-14T21:00:00.000Z' },
        { name: 'time', value: '2021-04-15T03:00:00.000Z' }, // current local time
        { name: 'time', value: '2021-04-15T06:00:00.000Z' },
        { name: 'time', value: '2021-04-15T09:00:00.000Z' },
      ];
      const easternEuropeanSummerTimeResult = generateCustomTimeList(
        currentTime,
        startTime,
        endTime,
        timeStep,
        easternEuropeanSummerTime,
      );
      expect(easternEuropeanSummerTimeResult).toEqual(
        expectedTimeListForEasterEuropeanSummerTime,
      );

      const expectedTimeListForCentralEuropeanSummerTime = [
        { name: 'time', value: '2021-04-14T04:00:00.000Z' },
        { name: 'time', value: '2021-04-14T10:00:00.000Z' },
        { name: 'time', value: '2021-04-14T16:00:00.000Z' },
        { name: 'time', value: '2021-04-14T22:00:00.000Z' },
        { name: 'time', value: '2021-04-15T01:00:00.000Z' },
        { name: 'time', value: '2021-04-15T04:00:00.000Z' },
        { name: 'time', value: '2021-04-15T07:00:00.000Z' },
      ];
      const centralEuropeanSummerTimeResult = generateCustomTimeList(
        currentTime,
        startTime,
        endTime,
        timeStep,
        centralEuropeanSummerTime,
      );
      expect(centralEuropeanSummerTimeResult).toEqual(
        expectedTimeListForCentralEuropeanSummerTime,
      );

      const expectedTimeListForWesternEuropeanSummerTime = [
        { name: 'time', value: '2021-04-14T05:00:00.000Z' },
        { name: 'time', value: '2021-04-14T11:00:00.000Z' },
        { name: 'time', value: '2021-04-14T17:00:00.000Z' },
        { name: 'time', value: '2021-04-14T23:00:00.000Z' },
        { name: 'time', value: '2021-04-15T02:00:00.000Z' },
        { name: 'time', value: '2021-04-15T05:00:00.000Z' },
        { name: 'time', value: '2021-04-15T08:00:00.000Z' },
      ];
      const westernEuropeanSummerTimeResult = generateCustomTimeList(
        currentTime,
        startTime,
        endTime,
        timeStep,
        westernEuropeanSummerTime,
      );
      expect(westernEuropeanSummerTimeResult).toEqual(
        expectedTimeListForWesternEuropeanSummerTime,
      );
    });
  });

  describe('shortUTCTime', () => {
    it('should append UTC if Z cannot be added', () => {
      const dateString = 'Fri, 11/24, 1 PM';
      const expectedDateString = 'Fri, 11/24, 1 PM UTC';
      expect(shortUTCTime(dateString)).toEqual(expectedDateString);
    });

    it('should append Z in English', () => {
      const dateString = 'Fri, 11/24, 1:00';
      const expectedDateString = 'Fri, 11/24, 1:00Z';
      expect(shortUTCTime(dateString)).toEqual(expectedDateString);
    });

    it('should append Z in German', () => {
      const dateString = 'tor. 23.11., 09:00';
      const expectedDateString = 'tor. 23.11., 09:00Z';
      expect(shortUTCTime(dateString)).toEqual(expectedDateString);
    });

    it('should append Z in Swedish', () => {
      const dateString = 'tors 23/11 09.00';
      const expectedDateString = 'tors 23/11 09.00Z';
      expect(shortUTCTime(dateString)).toEqual(expectedDateString);
    });

    it('should append Z in Finnish', () => {
      const dateString = 'To 23.11. 09.00';
      const expectedDateString = 'To 23.11. 09.00Z';
      expect(shortUTCTime(dateString)).toEqual(expectedDateString);
    });

    it('should append Z in Norwegian', () => {
      const dateString = 'tor. 23.11., 09.00';
      const expectedDateString = 'tor. 23.11., 09.00Z';
      expect(shortUTCTime(dateString)).toEqual(expectedDateString);
    });

    it('should append Z in Danish', () => {
      const dateString = 'tors. 23.11 09.00';
      const expectedDateString = 'tors. 23.11 09.00Z';
      expect(shortUTCTime(dateString)).toEqual(expectedDateString);
    });
  });

  describe('useAutoUpdateSelectedTime', () => {
    it('should sync default selected time', () => {
      const timeStep = 1;
      let defaultTime = 1618366800;
      const selectedTime = defaultTime;
      const setSelectedTime = jest.fn();

      const { rerender, result } = renderHook(() =>
        useAutoUpdateSelectedTime(defaultTime, selectedTime, setSelectedTime),
      );

      expect(result.current).toEqual(true);

      defaultTime += timeStep;

      rerender();

      expect(setSelectedTime).toHaveBeenCalledWith(defaultTime);
    });

    it('should not sync if selected time is smaller than default time', () => {
      const timeStep = 1;
      let defaultTime = 1618366800;
      const selectedTime = defaultTime - timeStep;
      const setSelectedTime = jest.fn();

      const { rerender, result } = renderHook(() =>
        useAutoUpdateSelectedTime(defaultTime, selectedTime, setSelectedTime),
      );

      expect(result.current).toEqual(false);

      defaultTime += timeStep;

      rerender();

      expect(setSelectedTime).not.toHaveBeenCalled();
    });

    it('should not sync if selected time is greater than default time', () => {
      const timeStep = 1;
      let defaultTime = 1618366800;
      const selectedTime = defaultTime + timeStep;
      const setSelectedTime = jest.fn();

      const { rerender, result } = renderHook(() =>
        useAutoUpdateSelectedTime(defaultTime, selectedTime, setSelectedTime),
      );

      expect(result.current).toEqual(false);

      defaultTime += timeStep;

      rerender();

      expect(setSelectedTime).not.toHaveBeenCalled();
    });
  });
});
