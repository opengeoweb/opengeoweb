/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  MenuItem,
  Select,
  SelectChangeEvent,
  BaseSelectProps,
} from '@mui/material';
import React from 'react';
import { useTimesliderLiteTranslation } from '../../../locales/i18n';

type TimeStepOptions = Record<string, string>;

type CustomSelectProps = BaseSelectProps & {
  setValue: (value: number) => void;
  dropdownButtonIcon?: React.ElementType;
  defaultTimeStep: number;
  timeStepOptions?: TimeStepOptions;
};

const TimeStepSelector: React.FC<CustomSelectProps> = ({
  dropdownButtonIcon,
  value,
  setValue,
  defaultTimeStep,
  timeStepOptions,
  ...props
}) => {
  const { t } = useTimesliderLiteTranslation();
  const defaultTimeStepOptions: TimeStepOptions = {
    5: `5 ${t('units.minutes')}`,
    15: `15 ${t('units.minutes')}`,
    30: `30 ${t('units.minutes')}`,
    60: `1 ${t('units.hours')}`,
    180: `3 ${t('units.hours')}`,
    360: `6 ${t('units.hours')}`,
    720: `12 ${t('units.hours')}`,
  } as const;
  const options = timeStepOptions ?? defaultTimeStepOptions;
  const allOptionsKeys = Object.keys(options);

  const availableOptionKeys: string[] = allOptionsKeys.filter(
    (option) => Number(option) >= defaultTimeStep,
  );

  return (
    <Select
      {...props}
      className="TimeSliderLite-timeStepSelect"
      data-testid="TimeSliderLite-timeStepSelect"
      inputProps={{
        'data-testid': 'TimeSliderLite-timeStepSelect-input',
      }}
      sx={{
        height: '40px',
        width: '100%',
      }}
      value={String(
        availableOptionKeys.includes(String(value))
          ? value
          : availableOptionKeys[availableOptionKeys.length - 1],
      )}
      onChange={(event: SelectChangeEvent<unknown>): void => {
        const newValue = Number(event?.target?.value);
        setValue(newValue);
      }}
      IconComponent={dropdownButtonIcon}
    >
      {availableOptionKeys.map((option) => (
        <MenuItem key={String(option)} value={String(option)}>
          {options[option]}
        </MenuItem>
      ))}
    </Select>
  );
};

export default TimeStepSelector;
