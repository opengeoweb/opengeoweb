/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

import { ThemeI18nProvider } from '../../testUtils/Providers';
import TimeSliderLiteOptionsMenu from './TimeSliderLiteOptionsMenu';

describe('components/TimeSliderLiteOptionsMenu/TimeSliderLiteOptionsMenu', () => {
  const currentTime = 1682590012;
  const startTime = 1682424000;
  const endTime = 1682769600;
  const defaultTimeRange: [number, number] = [startTime, endTime];
  const defaultTimeStep: [number, number] = [5, 15];
  const setTimeRange = jest.fn();
  const useTimeRange: [
    [number, number],
    (value: [number, number] | null) => void,
  ] = [defaultTimeRange, setTimeRange];
  const defaultProps = {
    sx: {
      position: 'absolute',
      bottom: '48px',
    },
    submenuSx: {
      position: 'absolute',
      bottom: '108px',
    },
    menuOpen: false,
    setMenuOpen: jest.fn(),
    currentTime,
    defaultTimeRange,
    defaultTimeStep,
    useTimeRange,
  };

  it('should render closable menu', () => {
    render(
      <ThemeI18nProvider>
        <TimeSliderLiteOptionsMenu {...defaultProps} />
      </ThemeI18nProvider>,
    );

    expect(screen.getByTestId('TimeSliderLite-optionsMenu')).toBeTruthy();

    fireEvent.click(
      screen.getByTestId('TimeSliderLite-optionsMenu-closeButton'),
    );

    expect(defaultProps.setMenuOpen).toHaveBeenCalledTimes(1);
  });

  it('should open submenu for time range', () => {
    render(
      <ThemeI18nProvider>
        <TimeSliderLiteOptionsMenu {...defaultProps} />
      </ThemeI18nProvider>,
    );

    expect(screen.queryByTestId('TimeSliderLite-timeRangeOptions')).toBeFalsy();
    fireEvent.click(screen.getByText('Time range'));
    expect(screen.getByTestId('TimeSliderLite-timeRangeOptions')).toBeTruthy();
    fireEvent.click(
      screen.getByTestId('TimeSliderLite-submenuWrapper-closeButton'),
    );
    expect(screen.queryByTestId('TimeSliderLite-timeRangeOptions')).toBeFalsy();
  });

  it('should open submenu for time step', () => {
    render(
      <ThemeI18nProvider>
        <TimeSliderLiteOptionsMenu {...defaultProps} />
      </ThemeI18nProvider>,
    );

    expect(screen.queryByTestId('TimeSliderLite-timeStepOptions')).toBeFalsy();
    fireEvent.click(screen.getByText('Time step'));
    expect(screen.getByTestId('TimeSliderLite-timeStepOptions')).toBeTruthy();
    fireEvent.click(
      screen.getByTestId('TimeSliderLite-submenuWrapper-closeButton'),
    );
    expect(screen.queryByTestId('TimeSliderLite-timeStepOptions')).toBeFalsy();
  });

  it('should open submenu for animation speed', () => {
    render(
      <ThemeI18nProvider>
        <TimeSliderLiteOptionsMenu {...defaultProps} />
      </ThemeI18nProvider>,
    );

    expect(
      screen.queryByTestId('TimeSliderLite-animationSpeedOptions'),
    ).toBeFalsy();
    fireEvent.click(screen.getByText('Speed'));
    expect(
      screen.getByTestId('TimeSliderLite-animationSpeedOptions'),
    ).toBeTruthy();
    fireEvent.click(
      screen.getByTestId('TimeSliderLite-submenuWrapper-closeButton'),
    );
    expect(
      screen.queryByTestId('TimeSliderLite-animationSpeedOptions'),
    ).toBeFalsy();
  });
});
