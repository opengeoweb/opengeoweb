/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  CardContent,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import { SpeedFactorType, speedFactors } from '@opengeoweb/timeslider';
import React from 'react';
import { useTimesliderLiteTranslation } from '../../../locales/i18n';

export interface AnimationSpeedSelectorProps {
  dropdownButtonIcon?: React.ElementType;
  speedOptions?: SpeedFactorType[];
  speedOptionItemPrefix?: string;
  speedOptionItemPostfix?: string;
  defaultSpeedOptionPostfix?: string;
  defaultSpeedFactor?: SpeedFactorType;
  useAnimationSpeed?: [number, (animationSpeed: number) => void] | [number];
}

const AnimationSpeedOptions: React.FC<AnimationSpeedSelectorProps> = ({
  dropdownButtonIcon,
  speedOptions = speedFactors.sort((a, b) => b - a),
  speedOptionItemPrefix = 'x ',
  speedOptionItemPostfix = '',
  defaultSpeedOptionPostfix,
  defaultSpeedFactor = speedFactors[2],
  useAnimationSpeed,
}) => {
  const { t } = useTimesliderLiteTranslation();
  const translatedSpeedOptionPostfix =
    defaultSpeedOptionPostfix ??
    ` (${t('common.default').toLocaleLowerCase()})`;

  const animationSpeed = useAnimationSpeed?.[0] ?? defaultSpeedFactor;
  const setAnimationSpeed = useAnimationSpeed?.[1];
  const readOnly = !setAnimationSpeed || speedOptions.length < 2;

  return (
    <CardContent
      className="TimeSliderLite-animationSpeedOptions"
      data-testid="TimeSliderLite-animationSpeedOptions"
      sx={{
        paddingTop: '12px',
        marginBottom: '-8px',
        minWidth: '200px',
      }}
    >
      <Select
        disabled={readOnly}
        className="TimeSliderLite-animationSpeedSelect"
        data-testid="TimeSliderLite-animationSpeedSelect"
        inputProps={{
          'data-testid': 'TimeSliderLite-animationSpeedSelect-input',
        }}
        sx={{ height: '40px' }}
        value={String(animationSpeed)}
        onChange={(event: SelectChangeEvent): void => {
          const newValue = Number(event?.target?.value);
          setAnimationSpeed && setAnimationSpeed(newValue);
        }}
        IconComponent={dropdownButtonIcon}
      >
        {speedOptions.map((option) => {
          const postfix =
            option === defaultSpeedFactor
              ? translatedSpeedOptionPostfix
              : speedOptionItemPostfix;
          const label = speedOptionItemPrefix + option + postfix;
          return (
            <MenuItem key={String(option)} value={String(option)}>
              {label}
            </MenuItem>
          );
        })}
      </Select>
    </CardContent>
  );
};

export default AnimationSpeedOptions;
