/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { fireEvent, render, screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import DayHourSelector, { CustomSelect } from './DayHourSelector';
import { ThemeI18nProvider } from '../../../testUtils/Providers';

const getSelectButton = (label: string): HTMLElement =>
  within(screen.getByTestId(`TimeSliderLite-customSelect-${label}`)).getByRole(
    'combobox',
    { hidden: true },
  );

const getSelectInput = (label: string): HTMLElement =>
  screen.getByTestId(`TimeSliderLite-customSelect-${label}-input`);

describe('components/TimeSliderLiteOptionsMenu/DayHourSelector', () => {
  const defaultProps = {
    label: 'label',
    maxHours: 50,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    useDays: [2, jest.fn()] as any,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    useHours: [2, jest.fn()] as any,
  };
  const user = userEvent.setup();

  it('should render day and hour select inputs', async () => {
    render(
      <ThemeI18nProvider>
        <DayHourSelector {...defaultProps} />
      </ThemeI18nProvider>,
    );
    expect(screen.getByText('label')).toBeTruthy();
    expect(screen.getByText('2 days')).toBeTruthy();
    expect(screen.getByText('2 hours')).toBeTruthy();

    await user.click(getSelectButton('days'));
    const dayListBox = within(screen.getByRole('presentation')).getByRole(
      'listbox',
    );
    // eslint-disable-next-line testing-library/no-node-access
    await user.click(within(dayListBox).getByText('0 days').parentElement!);
    expect(screen.getByText('0 days')).toBeTruthy();

    await user.click(getSelectButton('hours'));
    const hourListBox = within(screen.getByRole('presentation')).getByRole(
      'listbox',
    );
    // eslint-disable-next-line testing-library/no-node-access
    await user.click(within(hourListBox).getByText('0 hours').parentElement!);
    expect(screen.getByText('0 hours')).toBeTruthy();
  });

  it('should limit days by max hours', async () => {
    render(
      <ThemeI18nProvider>
        <DayHourSelector {...defaultProps} />
      </ThemeI18nProvider>,
    );

    await user.click(getSelectButton('days'));

    const listbox = within(screen.getByRole('presentation')).getByRole(
      'listbox',
    );

    within(listbox).getByText('0 days');
    within(listbox).getByText('1 days'); // TODO: Translate singular
    within(listbox).getByText('2 days');
    expect(within(listbox).queryByText('3 days')).toBeFalsy();
  });

  it('should limit hours by remaining max hours', async () => {
    render(
      <ThemeI18nProvider>
        <DayHourSelector {...defaultProps} />
      </ThemeI18nProvider>,
    );

    await user.click(getSelectButton('hours'));

    const listbox = within(screen.getByRole('presentation')).getByRole(
      'listbox',
    );

    within(listbox).getByText('0 hours');
    within(listbox).getByText('1 hours'); // TODO: Translate singular
    within(listbox).getByText('2 hours');
    expect(within(listbox).queryByText('3 hours')).toBeFalsy();
  });

  it('should limit hours to 23', async () => {
    render(
      <ThemeI18nProvider>
        <DayHourSelector
          {...defaultProps}
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          useDays={[1, jest.fn()] as any}
        />
      </ThemeI18nProvider>,
    );

    await user.click(getSelectButton('hours'));

    const listbox = within(screen.getByRole('presentation')).getByRole(
      'listbox',
    );

    within(listbox).getByText('0 hours');
    within(listbox).getByText('1 hours'); // TODO: Translate singular
    within(listbox).getByText('2 hours');
    within(listbox).getByText('23 hours');
    expect(within(listbox).queryByText('24 hours')).toBeFalsy();
  });

  it('should disable days if none are available', async () => {
    render(
      <ThemeI18nProvider>
        <DayHourSelector
          {...defaultProps}
          useDays={[0, jest.fn()]}
          useHours={[0, jest.fn()]}
          maxHours={23}
        />
      </ThemeI18nProvider>,
    );

    expect(getSelectButton('days')).toHaveClass('Mui-disabled');
    expect(getSelectButton('hours')).not.toHaveClass('Mui-disabled');
  });

  it('should disable hours if none is remaining', async () => {
    render(
      <ThemeI18nProvider>
        <DayHourSelector
          {...defaultProps}
          useDays={[1, jest.fn()]}
          useHours={[0, jest.fn()]}
          maxHours={24}
        />
      </ThemeI18nProvider>,
    );

    expect(getSelectButton('days')).not.toHaveClass('Mui-disabled');
    expect(getSelectButton('hours')).toHaveClass('Mui-disabled');
  });

  describe('CustomSelect', () => {
    const defaultProps = {
      amountOfOptions: 5,
      dropdownButtonIcon: (): React.ReactElement => <div>Dropdown Icon</div>,
      itemPostFix: 'items',
      value: 3,
      setValue: jest.fn(),
    };

    it('renders with correct options, selected, and item post-fix', async () => {
      render(<CustomSelect {...defaultProps} />);
      await user.click(getSelectButton('items'));

      const itemListBox = within(screen.getByRole('presentation')).getByRole(
        'listbox',
      );

      const menuItems = within(itemListBox).getAllByRole('option');
      expect(menuItems).toHaveLength(5);
      expect(menuItems[3]).toHaveAttribute('aria-selected', 'true');

      menuItems.forEach((menuItem, index) => {
        expect(menuItem).toHaveTextContent(`${index} items`);
      });
    });

    it('calls setValue when an available option is selected', () => {
      render(<CustomSelect {...defaultProps} />);
      const selectElement = getSelectInput('items');

      fireEvent.change(selectElement, { target: { value: '1' } });
      expect(defaultProps.setValue).toHaveBeenCalledWith(1);
    });

    it('does not call setValue when an unavailable option is selected', () => {
      render(<CustomSelect {...defaultProps} />);
      const selectElement = getSelectInput('items');

      fireEvent.change(selectElement, { target: { value: '5' } });
      expect(defaultProps.setValue).not.toHaveBeenCalled();
    });
  });
});
