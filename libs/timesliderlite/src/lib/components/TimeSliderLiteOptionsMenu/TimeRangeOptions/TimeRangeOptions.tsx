/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  Box,
  CardContent,
  FormControlLabel,
  Radio,
  RadioGroup,
} from '@mui/material';
import React, { useEffect, useLayoutEffect } from 'react';
import DayHourSelector from './DayHourSelector';
import {
  MILLISECOND_TO_SECOND,
  SECOND_TO_HOUR,
  hoursAndDaysToSeconds,
  secondsToDaysAndHours,
} from '../../TimeSliderLite/timeSliderLiteUtils';
import { useTimesliderLiteTranslation } from '../../../locales/i18n';

export interface TimeRangeOptionsProps {
  currentTime?: number;
  useTimeRange?: [
    [number, number] | null,
    (timeRange: [number, number] | null) => void,
  ];
  defaultTimeRange: [number, number];
  dropdownButtonIcon?: React.ElementType;
}

const TIME_SELECTOR_MIN_WIDTH = '21em';

const TimeRangeOptions: React.FC<TimeRangeOptionsProps> = ({
  currentTime = Date.now().valueOf() * MILLISECOND_TO_SECOND,
  useTimeRange,
  defaultTimeRange,
  dropdownButtonIcon,
}) => {
  const { t } = useTimesliderLiteTranslation();

  const isReadOnly = useTimeRange === undefined;
  const isDefault = !useTimeRange || useTimeRange[0] === null;
  const handleIsDefaultChange = (
    event: React.ChangeEvent<HTMLInputElement>,
  ): void => {
    if (event.target.value === 'default' && useTimeRange?.[1]) {
      useTimeRange[1](null);
    }
    if (event.target.value === 'custom' && useTimeRange?.[1]) {
      useTimeRange[1](defaultTimeRange);
    }
  };

  const [forecastDays, setForecastDays] = React.useState<number>(0);
  const [forecastHours, setForecastHours] = React.useState<number>(0);
  const [observedDays, setObservedDays] = React.useState<number>(0);
  const [observedHours, setObservedHours] = React.useState<number>(0);

  useEffect(() => {
    if (!isDefault && !isReadOnly) {
      const observedSeconds = Math.min(
        hoursAndDaysToSeconds(observedHours, observedDays),
        currentTime - defaultTimeRange[0],
      );
      const forecastSeconds = Math.min(
        hoursAndDaysToSeconds(forecastHours, forecastDays),
        defaultTimeRange[1] - currentTime,
      );

      if (useTimeRange?.[1]) {
        useTimeRange[1]([
          currentTime - observedSeconds,
          currentTime + forecastSeconds,
        ]);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [forecastDays, forecastHours, observedDays, observedHours]);

  useLayoutEffect(() => {
    if (useTimeRange) {
      const [timeRange] = useTimeRange;
      const [timeStart, timeEnd] = timeRange || defaultTimeRange;

      const timeToEndSeconds =
        timeEnd - currentTime > 0 ? timeEnd - currentTime : 0;
      const timeToStartSeconds =
        currentTime - timeStart > 0 ? currentTime - timeStart : 0;

      const { days: forecastDays, hours: forecastHours } =
        secondsToDaysAndHours(timeToEndSeconds);
      const { days: observedDays, hours: observedHours } =
        secondsToDaysAndHours(timeToStartSeconds);

      setForecastDays(forecastDays);
      setForecastHours(forecastHours);
      setObservedDays(observedDays);
      setObservedHours(observedHours);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isDefault]);

  const customTimeRange = useTimeRange?.[0] || defaultTimeRange;

  const minStartTime = Math.min(defaultTimeRange[0], customTimeRange[0]);
  const observedHoursToCurrentTime =
    (currentTime - minStartTime) * SECOND_TO_HOUR;
  const maxObservedHours =
    observedHoursToCurrentTime > 0 ? Math.floor(observedHoursToCurrentTime) : 0;

  const maxEndTime = Math.max(defaultTimeRange[1], customTimeRange[1]);
  const forecastHoursToCurrentTime =
    (maxEndTime - currentTime) * SECOND_TO_HOUR;
  const maxForecastHours =
    forecastHoursToCurrentTime > 0 ? Math.floor(forecastHoursToCurrentTime) : 0;

  return (
    <CardContent
      className="TimeSliderLite-timeRangeOptions"
      data-testid="TimeSliderLite-timeRangeOptions"
      sx={{
        paddingTop: '4px',
        '& label': {
          height: '36px',
        },
        '&:last-child': {
          paddingBottom: '16px',
        },
      }}
    >
      {t('menu.timeRange.description')}
      <RadioGroup
        defaultValue={isDefault ? 'default' : 'custom'}
        name="time-range-radio-button-group"
        row
        sx={{ marginTop: 1 }}
        value={isDefault ? 'default' : 'custom'}
        onChange={handleIsDefaultChange}
      >
        <FormControlLabel
          disabled={isReadOnly}
          value="default"
          control={<Radio />}
          label={t('common.default')}
        />
        <FormControlLabel
          disabled={isReadOnly}
          value="custom"
          control={<Radio />}
          label={t('common.custom')}
        />
      </RadioGroup>
      <Box minWidth={TIME_SELECTOR_MIN_WIDTH}>
        <DayHourSelector
          isDisabled={isDefault}
          dropdownButtonIcon={dropdownButtonIcon}
          label={t('common.observed')}
          maxHours={maxObservedHours}
          sx={{ marginY: 1 }}
          useDays={[observedDays, setObservedDays]}
          useHours={[observedHours, setObservedHours]}
        />
        <DayHourSelector
          isDisabled={isDefault}
          dropdownButtonIcon={dropdownButtonIcon}
          label={t('common.forecast')}
          maxHours={maxForecastHours}
          useDays={[forecastDays, setForecastDays]}
          useHours={[forecastHours, setForecastHours]}
        />
      </Box>
    </CardContent>
  );
};

export default TimeRangeOptions;
