/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Card, CardProps, IconButton, Typography } from '@mui/material';
import { Close } from '@opengeoweb/theme';
import React from 'react';

interface SubmenuWrapperProps extends CardProps {
  closeButtonIcon?: React.ReactNode;
  setSubmenu?: (value: string | null) => void;
}

const SubmenuWrapper: React.FC<SubmenuWrapperProps> = ({
  children,
  closeButtonIcon,
  sx,
  setSubmenu,
  title,
  ...props
}) => {
  return (
    <Card
      {...props}
      sx={{
        position: 'relative',
        ...sx,
      }}
    >
      <Typography
        sx={{
          fontWeight: '500',
          padding: '16px',
          paddingBottom: '0px',
        }}
      >
        {title}
      </Typography>
      <IconButton
        data-testid="TimeSliderLite-submenuWrapper-closeButton"
        size="small"
        sx={{ position: 'absolute', top: 4, right: 4 }}
        onClick={(): void => setSubmenu && setSubmenu(null)}
      >
        {closeButtonIcon ?? (
          <Close data-testid="TimeSliderLite-submenuWrapper-defaultCloseIcon" />
        )}
      </IconButton>
      {children}
    </Card>
  );
};

export default SubmenuWrapper;
