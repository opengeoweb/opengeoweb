/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import TimeSliderLiteClickableTimeline from './TimeSliderLiteClickableTimeline';
import { ThemeI18nProvider } from '../../testUtils/Providers';

describe('components/TimeSliderLiteBackground/TimeSliderClickableTimeline', () => {
  const defaultProps = {
    endTime: 1000,
    setSelectedTime: jest.fn(),
    startTime: 50,
    timeStep: [60, 3600] as [number, number],
    currentTime: 100,
    width: 100,
  };

  it('sets the selected time when clicked', () => {
    const setSelectedTime = jest.fn();
    render(
      <ThemeI18nProvider>
        <TimeSliderLiteClickableTimeline
          {...defaultProps}
          setSelectedTime={setSelectedTime}
        />
      </ThemeI18nProvider>,
    );
    const timeline = screen.getByTestId('TimeSliderLite-clickableTimeline');
    fireEvent.mouseEnter(timeline);
    const mouseMove = new MouseEvent('mouseMove', {
      bubbles: true,
      cancelable: true,
    });
    fireEvent(timeline, mouseMove);
    fireEvent.click(timeline);
    expect(setSelectedTime).toHaveBeenCalledWith(60);
  });
});
