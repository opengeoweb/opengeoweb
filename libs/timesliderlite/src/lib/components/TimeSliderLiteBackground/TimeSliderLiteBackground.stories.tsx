/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { Box } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import { StorybookDocsWrapper } from '@opengeoweb/shared';
import TimeSliderLiteBackground from './TimeSliderLiteBackground';

const meta: Meta<typeof TimeSliderLiteBackground> = {
  title: 'components/TimeSliderLiteBackground',
  component: TimeSliderLiteBackground,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the TimeSliderLiteBackground',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof TimeSliderLiteBackground>;

const currentTime = 1683000000;
const fourHours = 14400;
const halfADay = 43200;
const day = 86400;
const week = 604800;
const month = 2592000 + 4 * day;

const commonStoryProps = {
  height: 44,
  currentTime,
  timeStep: [15, 15] as [number, number],
};

export const Component: Story = {
  args: {
    ...commonStoryProps,
    startTime: currentTime - fourHours,
    endTime: currentTime + fourHours,
    selectedTime: currentTime,
    setSelectedTime: () => {},
    draggableTimeStamp: true,
    currentTimeSelected: true,
    timeZone: 'LT',
    needleWidth: 5,
    mouseHoverNeedleOpacity: 0.9,
    needleDragAreaWidth: 10,
    needleLabelZIndex: 1,
    disableForecast: false,
    zerorange: false,
    needleLabelOffset: [0, -5],
    resetSelectedTime: () => {},
  },
  render: (props) => (
    <div style={{ padding: 40, height: '100px' }}>
      <TimeSliderLiteBackground {...props} />
    </div>
  ),
};

export const ComponentDark: Story = {
  ...Component,
  tags: ['dark'],
  render: (props) => (
    <StorybookDocsWrapper isDark>
      <div style={{ padding: 40, height: '100px' }}>
        <TimeSliderLiteBackground {...props} />
      </div>
    </StorybookDocsWrapper>
  ),
};

const TimeSliderLegendDisplay = ({
  isDemo = true,
}: {
  isDemo?: boolean;
}): React.ReactElement => {
  const [selectedTime, setSelectedTime] = React.useState(currentTime);

  const style = isDemo ? { overflow: 'scroll', height: '100dvh' } : {};

  return (
    <Box {...style}>
      <Box m={4}>
        <TimeSliderLiteBackground
          {...commonStoryProps}
          zerorange
          startTime={currentTime}
          endTime={currentTime}
          selectedTime={selectedTime}
          setSelectedTime={setSelectedTime}
        />
      </Box>
      <Box m={4}>
        <TimeSliderLiteBackground
          {...commonStoryProps}
          startTime={currentTime - fourHours}
          endTime={currentTime + fourHours}
          selectedTime={selectedTime}
          setSelectedTime={setSelectedTime}
        />
      </Box>
      <Box m={4}>
        <TimeSliderLiteBackground
          {...commonStoryProps}
          startTime={currentTime - halfADay}
          endTime={currentTime + halfADay}
          selectedTime={selectedTime}
          setSelectedTime={setSelectedTime}
        />
      </Box>
      <Box m={4}>
        <TimeSliderLiteBackground
          {...commonStoryProps}
          startTime={currentTime - day}
          endTime={currentTime + day}
          selectedTime={selectedTime}
          setSelectedTime={setSelectedTime}
        />
      </Box>
      <Box m={4}>
        <TimeSliderLiteBackground
          {...commonStoryProps}
          startTime={currentTime - week}
          endTime={currentTime + week}
          selectedTime={selectedTime}
          setSelectedTime={setSelectedTime}
        />
      </Box>
      <Box m={4}>
        <TimeSliderLiteBackground
          {...commonStoryProps}
          startTime={currentTime - month}
          endTime={currentTime + month}
          selectedTime={selectedTime}
          setSelectedTime={setSelectedTime}
        />
      </Box>
    </Box>
  );
};

const parameters = {
  docs: {
    description: {
      story: 'Variations of TimeSliderLiteBackground',
    },
  },
};

export const TimesliderLegendExamplesLight: Story = {
  render: () => <TimeSliderLegendDisplay isDemo={false} />,
  tags: ['!dev'],
  parameters,
};

export const TimesliderLegendExamplesDark: Story = {
  render: () => (
    <StorybookDocsWrapper isDark>
      <TimeSliderLegendDisplay isDemo={false} />
    </StorybookDocsWrapper>
  ),
  tags: ['dark', '!dev'],
  parameters,
};

export const TimeSliderLegendDemoLight: Story = {
  render: () => <TimeSliderLegendDisplay />,
  tags: ['snapshot', '!autodocs'],
};

export const TimeSliderLegendDemoDark: Story = {
  render: () => <TimeSliderLegendDisplay />,
  tags: ['snapshot', 'dark', '!autodocs'],
};
