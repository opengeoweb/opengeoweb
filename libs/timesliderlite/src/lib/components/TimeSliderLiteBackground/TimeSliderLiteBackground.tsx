/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { useTheme, Box } from '@mui/material';
import React, { useLayoutEffect, useRef } from 'react';

import TimeSliderLiteBackgroundSvg, {
  TIME_SLIDER_LITE_PADDING_RIGHT,
} from './TimeSliderLiteBackgroundSvg';
import TimeSliderDraggableNeedle from './TimeSliderDraggableNeedle';
import TimeSliderLiteClickableTimeline from './TimeSliderLiteClickableTimeline';
import { MILLISECOND_TO_SECOND } from '../TimeSliderLite/timeSliderLiteUtils';
import { TimeZone } from '../TimeSliderLiteOptionsMenu/TimeZoneSwitch/TimeZoneSwitch';
import TimeSliderZeroTimestamp from './TimeSliderZeroTimestamp';

interface TimeSliderLiteBackgroundProps {
  height: number;
  draggableTimeStamp?: boolean;
  startTime: number;
  currentTime?: number;
  currentTimeSelected?: boolean;
  mouseHoverNeedleOpacity?: number;
  timeZone?: TimeZone;
  needleWidth?: number;
  needleDragAreaWidth?: number;
  needleLabelZIndex?: number;
  needleLabelOffset?: [number, number];
  endTime: number;
  disableForecast?: boolean;
  timeStep: [number, number];
  selectedTime: number;
  setSelectedTime: (time: number) => void;
  resetSelectedTime?: React.MouseEventHandler<HTMLDivElement>;
  zerorange?: boolean;
}

const TimeSliderLiteBackground: React.FC<TimeSliderLiteBackgroundProps> = ({
  height,
  draggableTimeStamp,
  timeZone = 'LT',
  startTime,
  endTime,
  disableForecast,
  currentTime = Date.now() * MILLISECOND_TO_SECOND,
  currentTimeSelected,
  mouseHoverNeedleOpacity,
  needleWidth,
  needleDragAreaWidth,
  needleLabelZIndex,
  needleLabelOffset,
  timeStep,
  selectedTime,
  setSelectedTime,
  resetSelectedTime,
  zerorange,
}) => {
  const {
    palette: {
      geowebColors: { timeSliderLite },
    },
  } = useTheme();
  const sliderAreaRef = useRef<HTMLDivElement>(null);
  const [areaWidth, setAreaWidth] = React.useState(0);
  const drawNeedle = selectedTime && areaWidth && startTime <= endTime;
  useLayoutEffect(() => {
    const sliderArea = sliderAreaRef.current as Element;
    setAreaWidth(sliderArea.clientWidth - TIME_SLIDER_LITE_PADDING_RIGHT);

    const observer = new ResizeObserver(() => {
      setAreaWidth(sliderArea.clientWidth - TIME_SLIDER_LITE_PADDING_RIGHT);
    });
    observer.observe(sliderArea);

    return (): void => {
      observer.disconnect();
    };
  }, [sliderAreaRef]);

  return (
    <Box
      ref={sliderAreaRef}
      sx={{
        position: 'relative',
        height: `${height}px`,
        overflow: 'visible',
        width: '100%',
        backgroundColor: timeSliderLite.timelineBackgroundObserved.fill,
      }}
      data-testid="TimeSliderLite-Background"
    >
      <TimeSliderLiteBackgroundSvg
        style={{ position: 'absolute', top: 0 }}
        timeStep={timeStep}
        width={areaWidth}
        height={height}
        startTime={startTime}
        endTime={endTime}
        disableForecast={disableForecast}
        currentTime={currentTime}
        timeZone={timeZone}
      />
      <TimeSliderLiteClickableTimeline
        currentTime={currentTime}
        opacity={mouseHoverNeedleOpacity}
        width={areaWidth}
        startTime={startTime}
        endTime={endTime}
        disableForecast={disableForecast}
        timeStep={timeStep}
        setSelectedTime={setSelectedTime}
        timeZone={timeZone}
      />
      {drawNeedle &&
        (!zerorange ? (
          <TimeSliderDraggableNeedle
            currentTimeSelected={currentTimeSelected}
            onDoubleClick={resetSelectedTime}
            needleWidth={needleWidth}
            draggableLabel={draggableTimeStamp}
            style={{ position: 'absolute', top: 0 }}
            timeZone={timeZone}
            disableForecast={disableForecast}
            width={areaWidth}
            needleDragAreaWidth={needleDragAreaWidth}
            needleLabelZIndex={needleLabelZIndex}
            needleLabelOffset={needleLabelOffset}
            height={height}
            startTime={startTime}
            endTime={endTime}
            timeStep={timeStep}
            currentTime={currentTime}
            selectedTime={selectedTime}
            setSelectedTime={setSelectedTime}
          />
        ) : (
          <TimeSliderZeroTimestamp
            timeZone={timeZone}
            timeStep={timeStep}
            currentTime={currentTime}
            timestampZIndex={needleLabelZIndex}
          />
        ))}
    </Box>
  );
};

export default TimeSliderLiteBackground;
