/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

import { lightTheme } from '@opengeoweb/theme';
import { ThemeI18nProvider } from '../../testUtils/Providers';
import TimeSliderDraggableNeedle, {
  DRAG_AREA_WIDTH,
} from './TimeSliderDraggableNeedle';
import { getSelectedTimePx } from '../TimeSliderLite/timeSliderLiteUtils';

const startTime = 1682424000;
const endTime = 1682769600;

describe('components/TimeSliderLiteBackground/TimeSliderDraggableNeedle', () => {
  const defaultProps = {
    style: {
      top: 0,
    },
    locale: 'en',
    width: 1907,
    height: 44,
    startTime,
    endTime,
    timeStep: [3600, 3600] as [number, number],
    selectedTime: 1682590012,
    setSelectedTime: jest.fn(),
  };

  it('should render needle', () => {
    render(
      <ThemeI18nProvider>
        <TimeSliderDraggableNeedle {...defaultProps} />
      </ThemeI18nProvider>,
    );
    expect(screen.getByTestId('TimeSliderLite-NeedleShadow')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-NeedleLine')).toBeTruthy();
  });

  it('should place needle according to selected time', () => {
    render(
      <ThemeI18nProvider>
        <TimeSliderDraggableNeedle {...defaultProps} />
      </ThemeI18nProvider>,
    );
    const draggableNeedle = screen.getByTestId(
      'TimeSliderLite-DraggableNeedle',
    );

    const expectedX =
      getSelectedTimePx(
        defaultProps.startTime,
        defaultProps.endTime,
        defaultProps.selectedTime,
        defaultProps.width,
      ) -
      DRAG_AREA_WIDTH / 2;
    const expectedTransform = `translate(${expectedX}px,0px)`;

    expect(draggableNeedle.style.transform).toMatch(expectedTransform);
  });

  it('should call setSelectedTime after drag end', async () => {
    const setSelectedTime = jest.fn();
    render(
      <ThemeI18nProvider>
        <TimeSliderDraggableNeedle
          {...{
            ...defaultProps,
            setSelectedTime,
          }}
        />
      </ThemeI18nProvider>,
    );
    const draggableNeedle = screen.getByTestId(
      'TimeSliderLite-DraggableNeedle',
    );

    fireEvent.mouseDown(draggableNeedle, {
      x: 0,
      y: 0,
    });

    fireEvent.mouseMove(draggableNeedle, {
      buttons: 1,
      x: 100,
      y: 0,
    });

    expect(setSelectedTime).toHaveBeenCalled();
    const selectedTime = setSelectedTime.mock.calls[0][0];
    expect(selectedTime).toBeGreaterThan(defaultProps.startTime);
    expect(selectedTime).toBeLessThan(defaultProps.endTime);
  });

  it('should render needle to the start if selected time is outside of bounds', () => {
    render(
      <ThemeI18nProvider>
        <TimeSliderDraggableNeedle
          {...{
            ...defaultProps,
            selectedTime: startTime - 1000,
          }}
        />
      </ThemeI18nProvider>,
    );
    expect(
      screen.queryByTestId('TimeSliderLite-DraggableNeedle'),
    ).toHaveAttribute('style', 'transform: translate(-5px,0px);');
  });

  it('should render default needle if current time is selected', () => {
    render(
      <ThemeI18nProvider>
        <TimeSliderDraggableNeedle
          {...{
            ...defaultProps,
            currentTimeSelected: true,
          }}
        />
      </ThemeI18nProvider>,
    );

    expect(screen.getByTestId('TimeSliderLite-NeedleLine')).toHaveAttribute(
      'stroke',
      lightTheme.palette.geowebColors.timeSliderLite.needleDefault.stroke,
    );
  });
});
