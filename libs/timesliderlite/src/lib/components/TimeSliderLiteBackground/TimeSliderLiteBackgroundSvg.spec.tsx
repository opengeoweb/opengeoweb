/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';

import { lightTheme } from '@opengeoweb/theme';
import TimeSliderLiteBackgroundSvg from './TimeSliderLiteBackgroundSvg';
import { ThemeI18nProvider } from '../../testUtils/Providers';

describe('components/TimeSliderLiteBackground/TimeSliderLiteBackgroundSvg', () => {
  const defaultProps = {
    width: 1498,
    height: 44,
    startTime: 1682856000,
    endTime: 1683201600,
    locale: 'en',
  };

  it('should render background', () => {
    const numberOfDays =
      (defaultProps.endTime - defaultProps.startTime) / (60 * 60 * 24);
    const numberOfVisibleDays = numberOfDays + 4; // Renders two day padding each side
    const numberOfTicks = numberOfVisibleDays * 5; // Four ticks per day plus end tick
    const { baseElement } = render(
      <ThemeI18nProvider>
        <TimeSliderLiteBackgroundSvg {...defaultProps} />
      </ThemeI18nProvider>,
    );

    // eslint-disable-next-line testing-library/no-node-access
    expect(baseElement.querySelectorAll('rect').length).toBe(
      numberOfVisibleDays,
    );
    // eslint-disable-next-line testing-library/no-node-access
    expect(baseElement.querySelectorAll('line').length).toBe(numberOfTicks);
    // eslint-disable-next-line testing-library/no-node-access
    expect(baseElement.querySelectorAll('text').length).toBe(
      numberOfVisibleDays,
    );
  });

  it('should render observed and forecast background in alternating colors', () => {
    const currentTime = (defaultProps.startTime + defaultProps.endTime) / 2;
    const { baseElement } = render(
      <ThemeI18nProvider>
        <TimeSliderLiteBackgroundSvg
          {...defaultProps}
          currentTime={currentTime}
        />
      </ThemeI18nProvider>,
    );
    // eslint-disable-next-line testing-library/no-node-access
    const backgroundRectangles = baseElement.querySelectorAll('rect');
    expect(backgroundRectangles.length).toBe(9);

    const first = backgroundRectangles[0];
    const second = backgroundRectangles[1];
    const third = backgroundRectangles[2];
    const fourth = backgroundRectangles[3];
    const partial = backgroundRectangles[4];
    const thirdLast = backgroundRectangles[5];
    const secondLast = backgroundRectangles[6];
    const last = backgroundRectangles[7];

    const observedFill =
      lightTheme.palette.geowebColors.timeSliderLite.timelineBackgroundObserved
        .fill;
    const observedFillAlternative =
      lightTheme.palette.geowebColors.timeSliderLite
        .timelineBackgroundObservedAlternative.fill;
    const forecastFill =
      lightTheme.palette.geowebColors.timeSliderLite.timelineBackground.fill;
    const forecastFillAlternative =
      lightTheme.palette.geowebColors.timeSliderLite
        .timelineBackgroundAlternative.fill;

    expect(first.getAttribute('fill')).toBe(observedFill);
    expect(second.getAttribute('fill')).toBe(observedFillAlternative);

    expect(third.getAttribute('fill')).toBe(observedFill);

    // Middle should consist of forecast background and observed partial rectangle
    expect(fourth.getAttribute('fill')).toBe(forecastFill);
    expect(partial.getAttribute('fill')).toBe(observedFillAlternative);
    expect(Number(partial.getAttribute('width'))).toBeLessThan(
      Number(first.getAttribute('width')),
    );
    expect(Number(partial.getAttribute('x'))).toBe(
      Number(fourth.getAttribute('x')),
    );

    expect(thirdLast.getAttribute('fill')).toBe(forecastFillAlternative);
    expect(secondLast.getAttribute('fill')).toBe(forecastFill);
    expect(last.getAttribute('fill')).toBe(forecastFillAlternative);
  });

  it('should localize day labels', async () => {
    render(
      <ThemeI18nProvider language="fi">
        <TimeSliderLiteBackgroundSvg {...defaultProps} />
      </ThemeI18nProvider>,
    );

    // Expect Finnish labels
    expect(await screen.findByText('Su 30.4.')).toBeTruthy();
    expect(await screen.findByText('Ma 1.5.')).toBeTruthy();
    expect(await screen.findByText('Ti 2.5.')).toBeTruthy();
    expect(await screen.findByText('Ke 3.5.')).toBeTruthy();
    expect(await screen.findByText('To 4.5.')).toBeTruthy();
    expect(await screen.findByText('Pe 5.5.')).toBeTruthy();
  });
});
