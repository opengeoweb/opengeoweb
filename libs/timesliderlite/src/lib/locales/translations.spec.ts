/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import translations from './translations.json';

const availableTranslations = Object.keys(translations);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const compareKeysDeep = (obj1: any, obj2: any, parentKey?: string): void => {
  const keys1 = Object.keys(obj1);
  const keys2 = Object.keys(obj2);

  let difference: string | null = null;
  if (keys1.length !== keys2.length || String(keys1) !== String(keys2)) {
    difference = `Keys are not equal for ${
      parentKey || 'root'
    }: ${keys1} !== ${keys2}`;
  }

  expect(difference).toBe(null);

  keys1.forEach((key) => {
    if (typeof obj1[key] !== typeof obj2[key]) {
      difference = `Types are not equal for ${
        parentKey || 'root'
      }: ${typeof obj1[key]} !== ${typeof obj2[key]}`;
    }
    expect(difference).toBe(null);
    if (typeof obj1[key] === 'object') {
      compareKeysDeep(
        obj1[key],
        obj2[key],
        parentKey ? `${parentKey}.${key}` : key,
      );
    }
  });
};

describe('src/lib/locales/translations', () => {
  test('should have same keys', () => {
    const baseTranslation =
      translations[availableTranslations[0] as keyof typeof translations];

    // All translations should have same keys as baseTranslation
    availableTranslations.forEach((_, index) => {
      if (index === 0) {
        return;
      }
      compareKeysDeep(
        baseTranslation,
        translations[availableTranslations[index] as keyof typeof translations],
      );
    });
  });
});
