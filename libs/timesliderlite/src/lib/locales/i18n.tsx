/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React, { PropsWithChildren } from 'react';
import {
  I18nextProvider,
  initReactI18next,
  useTranslation,
  UseTranslationResponse,
} from 'react-i18next';
import i18n from 'i18next';
import { CORE_NAMESPACE, coreTranslations } from '@opengeoweb/core';
import translations from './translations.json';

export type TimeSliderLiteLanguage = keyof typeof translations;
export const TIME_SLIDER_LITE_NAMESPACE = 'timeSliderLite';

export const initTimeSliderLiteI18n = (
  language?: TimeSliderLiteLanguage,
): void => {
  void i18n.use(initReactI18next).init({
    lng: language ?? 'en',
    ns: TIME_SLIDER_LITE_NAMESPACE,
    resources: {
      en: {
        [TIME_SLIDER_LITE_NAMESPACE]: translations.en,
        [CORE_NAMESPACE]: coreTranslations.en,
      },
      fi: {
        [TIME_SLIDER_LITE_NAMESPACE]: translations.fi,
        [CORE_NAMESPACE]: coreTranslations.fi,
      },
      'sv-FI': {
        [TIME_SLIDER_LITE_NAMESPACE]: translations['sv-FI'],
      },
    },
  });
};

interface TimeSliderLiteI18nProviderProps extends PropsWithChildren {
  language?: TimeSliderLiteLanguage;
}

/**
 * This provider is limited to TimeSliderLite translations.
 *
 * If you need more translations, install i18next and react-i18next packages and create your own I18nextProvider as multiple providers are not supported.
 *
 * You can then import TIME_SLIDER_LITE_NAMESPACE and timeSliderLiteTranslations and use it to build your own provider.
 * @language TimeSliderLiteLanguage - optional - default 'en'
 * @returns I18nextProvider
 */
export const TimeSliderLiteI18nProvider: React.FC<
  TimeSliderLiteI18nProviderProps
> = ({ children, language }) => {
  initTimeSliderLiteI18n(language);
  return <I18nextProvider i18n={i18n}>{children}</I18nextProvider>;
};

export const useTimesliderLiteTranslation = (): UseTranslationResponse<
  typeof TIME_SLIDER_LITE_NAMESPACE,
  typeof i18n
> => useTranslation(TIME_SLIDER_LITE_NAMESPACE);
