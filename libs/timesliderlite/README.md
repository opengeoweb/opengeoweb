# Time Slider Lite

This library is a simple time slider component that renders timeline according to layer time dimensions.

Time Slider Lite can be used separately in a single map components such as `ConfigurableMap` or `MapViewConnect`. How to connect Time Slider Lite to a map component, see [Time Slider Lite Connect story](src/lib/components/TimeSliderLite/TimeSliderLiteConnect.stories.tsx).

## Running unit tests

Run `nx test timesliderlite` to execute the unit tests via [Jest](https://jestjs.io).

### TypeScript Documentation

- [TypeScript Docs](https://opengeoweb.gitlab.io/opengeoweb/typescript-docs/timesliderlite/)
